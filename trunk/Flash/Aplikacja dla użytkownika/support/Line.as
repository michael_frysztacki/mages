﻿package support
{
	import flash.display.MovieClip;
	import flash.utils.*;
	import flash.display.Shape;
	import support.*;

	public class Line
	{				
		private var line:MovieClip;
		
		public function Line(mapa:MovieClip):void
		{			
			line = new MovieClip();						
			mapa.addChild(line);
		}
		public function drawLine(spot1:Spot, spot2:Spot, color:Number):void  //0xff00ff
		{						
			var alfa:Number = Transl.getAlfa(spot1, spot2);				
			var fromX:Number = Transl.longitDegToPix(spot1.getLongitude());
			var fromY:Number = Transl.latDegToPix(spot1.getLatitude());
			var toX:Number;
			var toY:Number;
			var temp:Spot;
			
			line.graphics.lineStyle(1, color, 100, true, "none", "round", "miter", 1);			
			line.graphics.moveTo(fromX, fromY);						
			
			for(var beta=0; beta<alfa; beta+=0.001)
			{							
				fromX = toX;
				fromY = toY;				
				temp = Transl.getPointBetween(spot1, spot2, beta);
				toX = Transl.longitDegToPix(temp.getLongitude());
				toY = Transl.latDegToPix(temp.getLatitude());
				if(Math.abs(fromX-toX)<Transl.getMapWidth()*0.9)
				{					
					line.graphics.lineTo(toX, toY);
				}
				else
				line.graphics.moveTo(toX, toY);										
			}			
		}		
		public function eraseLine():void
		{
			line.graphics.clear();
		}		
	}		
}


