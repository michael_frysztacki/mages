﻿package support.Drawing
{
	import flash.display.MovieClip;
	import flash.utils.*;
	import flash.display.Shape;
	import support.*;
	import flash.sampler.Sample;


	public class Square
	{				
		private var line:MovieClip;
		private var line1:MovieClip;
		
		public function Square(mapa:MovieClip):void
		{			
			line = new MovieClip();						 			
			mapa.addChild(line);
		}
		
		public function drawSquare(centerSpot:Spot, km:Number, color:Number):void  //0xff00ff
		{																											
			var toX:Number;
			var toY:Number;
			var cenX:Number = centerSpot.getLongitude();
			var cenY:Number = centerSpot.getLatitude();
			var R:Number = Transl.kmToDegrees(km)/Math.cos(Transl.degToRad(centerSpot.getLatitude()));
			var fromX:Number = Transl.longitDegToPix(cenX-R);
			var fromY:Number = Transl.latDegToPix(cenY-R);			
			
			line.graphics.moveTo(fromX, fromY);			
			line.graphics.lineStyle(1, color, 100, true, "none", "round", "miter", 1);			
			
			
			var i:Number;			
			toY = Transl.latDegToPix(cenY-R);
			for(i=cenX-R; i<cenX+R; i+=0.1)
			{										
				toX = Transl.longitDegToPix(i);				
				if(Math.abs(fromX-toX)<Transl.getMapWidth()*0.9)
				{					
					line.graphics.lineTo(toX, toY);
				}
				else
				line.graphics.moveTo(toX, toY);
				fromX = toX;
			}			
			toX = Transl.latDegToPix(cenX-R);
			for(i=cenY-R; i<cenY+R; i+=0.1)
			{										
				toY = Transl.longitDegToPix(i);				
				if(Math.abs(fromY-toY)<Transl.getMapHeight()*0.9)
				{					
					line.graphics.lineTo(toX, toY);
				}
				else
				line.graphics.moveTo(toX, toY);
				fromX = toY;
			}			
			toY = Transl.latDegToPix(cenY+R);
			for(i=cenX-R; i<cenX+R; i+=0.1)
			{										
				toX = Transl.longitDegToPix(i);				
				if(Math.abs(fromX-toX)<Transl.getMapWidth()*0.9)
				{					
					line.graphics.lineTo(toX, toY);
				}
				else
				line.graphics.moveTo(toX, toY);
				fromX = toX;
			}			
			toX = Transl.latDegToPix(cenX+R);
			for(i=cenY-R; i<cenY+R; i+=0.1)
			{										
				toY = Transl.longitDegToPix(i);				
				if(Math.abs(fromY-toY)<Transl.getMapHeight()*0.9)
				{					
					line.graphics.lineTo(toX, toY);
				}
				else
				line.graphics.moveTo(toX, toY);
				fromX = toY;
			}		
		}		
		
		public function eraseSquare():void
		{
			line.graphics.clear();
		}		
	}		
}



