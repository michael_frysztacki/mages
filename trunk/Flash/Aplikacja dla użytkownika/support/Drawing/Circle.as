﻿package support.Drawing
{
	import flash.display.MovieClip;
	import flash.utils.*;
	import flash.display.Shape;
	import support.*;
	import flash.sampler.Sample;

	public class Circle
	{				
		private var line:MovieClip;
		private var line1:MovieClip;
		
		public function Circle(mapa:MovieClip):void
		{			
			line = new MovieClip();						 			
			mapa.addChild(line);
		}
		public function drawCircle(centerSpot:Spot, km:Number, color:Number):void  //0xff00ff
		{						
			var t:Number = 0; //parametr
			var a:Number = Transl.degToRad(90-centerSpot.getLatitude());//-(centerSpot.getLatitude()+90);    // obrot wokol osi x
			var b:Number = Transl.degToRad(centerSpot.getLongitude());   // obrot wokol osi z
			//var a:Number = 0;   // obrot wokol osi x
			//var b:Number = 0;   // obrot wokol osi z
			
			var r:Number = Math.sin(km/(Transl.earthRadiusLength))*Transl.earthRadiusLength;
			var R = Math.sqrt(Transl.earthRadiusLength*Transl.earthRadiusLength-r*r);
			var coordX:Number;
			var coordY:Number; 			
			var coordZ:Number;
			
			//{Cos[b] rCos[t] + (Cos[a] rSin[t] + R Sin[a]) Sin[b], 
// 			Cos[b] (Cos[a] rSin[t] + R Sin[a]) - rCos[t] Sin[b], 
// 			R Cos[a] - rSin[t] Sin[a]}
			
			
			coordX = r*Math.cos(b)*Math.cos(t)+(r*Math.cos(a)*Math.sin(t)+R*Math.sin(a))*Math.sin(b);
			coordY = Math.cos(b)*(r*Math.cos(a)*Math.sin(t)+R*Math.sin(a))-r*Math.cos(t)*Math.sin(b);
			coordZ = R*Math.cos(a)-r*Math.sin(t)*Math.sin(a);
			
			var tempPoint:Spot = new Spot();						
			tempPoint.setLatitude(Transl.xyzToLat(coordZ));
			tempPoint.setLongitude(Transl.xyzToLongit(coordX, coordY));
			line.addChild(tempPoint);								   
								   
			var fromX:Number = Transl.longitDegToPix(tempPoint.getLongitude());
			var fromY:Number = Transl.latDegToPix(tempPoint.getLatitude());			
			var toX:Number;
			var toY:Number;								
			
			line.graphics.lineStyle(1, color, 100, true, "none", "round", "miter", 1);			
			line.graphics.moveTo(fromX, fromY);						
			
			for(t=0; t<2*Math.PI; t+=0.01)
			{											
				coordX = r*Math.cos(b)*Math.cos(t)+(r*Math.cos(a)*Math.sin(t)+R*Math.sin(a))*Math.sin(b);				
				coordY = Math.cos(b)*(r*Math.cos(a)*Math.sin(t)+R*Math.sin(a))-r*Math.cos(t)*Math.sin(b);
				coordZ = R*Math.cos(a)-r*Math.sin(t)*Math.sin(a);																	
				
				fromX = toX;
				fromY = toY;								
				tempPoint.setLongitude(Transl.xyzToLongit(coordX, coordY));
				tempPoint.setLatitude(Transl.xyzToLat(coordZ));
				toX = Transl.longitDegToPix(tempPoint.getLongitude());
				toY = Transl.latDegToPix(tempPoint.getLatitude());
				if(Math.abs(fromX-toX)<Transl.getMapWidth()*0.9)
				{					
					line.graphics.lineTo(toX, toY);
				}
				else
				line.graphics.moveTo(toX, toY);														
			}			
		}		
		
		public function eraseCircle():void
		{
			line.graphics.clear();
		}		
	}		
}



