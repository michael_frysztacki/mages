﻿package support
{
	
    import flash.display.MovieClip;
	import flash.events.*;
	import fl.controls.*;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.*;
	import fl.data.DataProvider;
	import flash.sampler.NewObjectSample;		
	import support.*;
	import support.UserData.TravellingUnits;

	public class Menu extends Spot
	{
		var shell:MovieClip;
		var mapa:MovieClip;
		var menuContainer:MovieClip;
		var unitsContainer:MovieClip;
		var showMenu:Button;
		var acceptBut:Button;		
		var cBoxCapital:CheckBox;
		var cBoxColony:CheckBox;
		var modeChoice:ComboBox;
		var cityChoice:ComboBox;
		var tempTrack:Track;
		var userData:UserData;
		var distWind:TextField;		
		
		public function Menu(shell:MovieClip, mapa:MovieClip, userData:UserData)
		{
			this.shell = shell;
			this.mapa = mapa;
			this.userData = userData;
			menuContainer = new MovieClip();
			menuContainer.graphics.beginFill(0x8B8B83);
			menuContainer.graphics.drawRoundRect(0, 0, 600, 50, 25, 25);
			menuContainer.graphics.endFill();
			menuContainer.x = 0 - 300;
			menuContainer.y = Transl.startingHeight / 2 - 130 + 1000;
			menuContainer.alpha = 0.95;
			shell.addChild(menuContainer);
			
			var modeChoices:Array = new Array(
			{label:"atak", data:"atak"},
			{label:"handel", data:"atak"},
			{label:"transport", data:"transport"});
			modeChoice = new ComboBox();
			modeChoice.dropdownWidth = 100;
			modeChoice.width = 100;
			modeChoice.move( 10, 10);
			//modeChoice.prompt = "Choose Mode";
			modeChoice.dataProvider = new DataProvider(modeChoices);						
			modeChoice.selectedIndex = 0;  //default value = "atak"
			menuContainer.addChild(modeChoice);
			

			var cityChoices:Array = new Array();
			cityChoices[0] = {label:userData.myCities[0].city.getName(),data:"0"};
			for (var i=1; i<userData.myCities.length; i++)
			{
				cityChoices[i] = {label:"Colony no" + i.toString(),data:i.toString()};
			}
			cityChoice = new ComboBox();
			cityChoice.dropdownWidth = 100;
			cityChoice.width = 100;
			cityChoice.move( 120, 10);
			//cityChoice.prompt = "Choose City";
			cityChoice.dataProvider = new DataProvider(cityChoices);
			cityChoice.selectedIndex = 0;  //default value is Capital
			menuContainer.addChild(cityChoice);

			distWind = new TextField();
			distWind.x = 230;
			distWind.y = 10;
			distWind.width = 50;
			var format:TextFormat = new TextFormat();
			format.color = 0x000000;
			format.size = 45;
			format.bold = true;
			distWind.setTextFormat(format);
			menuContainer.addChild(distWind);

			tempTrack = new Track(mapa);			
			
			
			showMenu = new Button();
			showMenu.width = 100;
			showMenu.height = 20;
			showMenu.label = "Pokaz Menu";
			showMenu.x =  -  showMenu.width / 2;
			showMenu.y = Transl.startingHeight / 2 - 100;
			shell.addChild(showMenu);
			
			acceptBut = new Button();
			acceptBut.width = 80;
			acceptBut.height = 20;
			acceptBut.label = "Zatwierdź";
			acceptBut.x =  500;
			acceptBut.y = 10;
			menuContainer.addChild(acceptBut);

			cBoxColony = new CheckBox();
			cBoxColony.label = "Colony";
			cBoxColony.x = 340;
			cBoxColony.y = 10;
			menuContainer.addChild(cBoxColony);

			cBoxCapital = new CheckBox();
			cBoxCapital.label = "City";
			cBoxCapital.x = 420;
			cBoxCapital.y = 10;
			menuContainer.addChild(cBoxCapital);
			
			unitsContainer = new MovieClip();
			unitsContainer.graphics.beginFill(0x8B8B83);
			unitsContainer.graphics.drawRoundRect(0, 0, 120,500, 25, 25);
			unitsContainer.graphics.endFill();
			unitsContainer.x = -Transl.startingWidth/2;
			unitsContainer.y = -200 + 2000;
			unitsContainer.alpha = 0.95;
			shell.addChild(unitsContainer);
			var unitsButArray:Array = new Array();
			for(i=0; i<userData.myCities[cityChoice.selectedIndex].units.length; i++)
			{
				unitsButArray[i] = new Button();
				unitsButArray[i].width = 100;
				unitsButArray[i].height = 20;
				unitsButArray[i].x = 10;
				unitsButArray[i].y = 25 + 25*i;
				unitsButArray[i].label = userData.myCities[cityChoice.selectedIndex].units[i].getUnitName();
				unitsContainer.addChild(unitsButArray[i]);
			}
			
			showMenu.addEventListener(MouseEvent.CLICK, moveMenu);
			acceptBut.addEventListener(MouseEvent.CLICK, sendUnits);
			//cityChoice.addEventListener("change", chooseCity);
			cBoxCapital.addEventListener(MouseEvent.CLICK, drawRemoveCapital);
			cBoxColony.addEventListener(MouseEvent.CLICK, drawRemoveColony);
			
			drawMyCities()			
			
			/*
			var dlafrika:Button;
			dlafrika = new Button();
			dlafrika.width = 200;
			dlafrika.height = 200;
			dlafrika.label = "Dla frika";
			dlafrika.x =  -100;
			dlafrika.y = -100;
			shell.addChild(dlafrika);
			dlafrika.addEventListener(MouseEvent.CLICK, probaFrika);
			*/
		}
		public function probaFrika(event:MouseEvent):void
		{
			DBAccess.getDataFromJava();
		}
		public function sendUnits(event:MouseEvent):void
		{			
			if(!tempTrack.ifCitiesNull())
			{				
				var spot1:Spot = new Spot();
				var spot2:Spot = new Spot();
				var trasa:Track = new Track(mapa);
				spot1.setSpot(tempTrack.getStartCity().getLongitude(),
							  tempTrack.getStartCity().getLatitude(), tempTrack.getStartCity().getID(), mapa);
				spot2.setSpot(tempTrack.getEndCity().getLongitude(), 
							  tempTrack.getEndCity().getLatitude(), tempTrack.getEndCity().getID(), mapa);
				trasa.setCities(spot1, spot2);
				var currTime:Date = new Date();
				var T:Number = currTime.time;				
				var travellingUnits:TravellingUnits = new TravellingUnits(mapa, getChosenUnits(), T, trasa);										
				userData.travUnits[userData.travUnits.length] = travellingUnits;				
			}
		}
		public function getChosenUnits():Array  //wersja tymczasowa
		{
			return userData.myCities[cityChoice.selectedIndex].units;
		}
		public function drawWalkingUnits(event:TimerEvent):void		
		{						
			userData.travUnits[userData.travUnits.length-1].drawUnitsPosition();
		}
		public function moveMenu(evt: MouseEvent)
		{
			if (showMenu.label == "Pokaz Menu")
			{
				menuContainer.y = menuContainer.y - 1000;
				unitsContainer.y = unitsContainer.y - 2000;
				showMenu.label = "Ukryj Menu";
				showMenu.y -=  50;
			}
			else
			{
				showMenu.y;
				menuContainer.y = menuContainer.y + 1000;
				unitsContainer.y = unitsContainer.y + 2000;
				showMenu.label = "Pokaz Menu";
				showMenu.y +=  50;
			}
		}
		public function getUnits():Array
		{
			var availUnits:Array = new Array();
			return availUnits
		}
		
		public function chooseCity(event:Event):void
		{
			if (cityChoice.selectedIndex != -1)
			{
				for (var i=0; i<userData.allSpots.length; i++)
				{
					//userData.allSpots[i].addEventListener(MouseEvent.CLICK, drawTempTrack);
				}
			}
		}		
		function drawMyCities()
		{
			for (var i=0; i<userData.myCities.length; i++)
			{			
				userData.myCities[i].city.drawCirc();
				userData.myCities[i].city.addEventListener(MouseEvent.CLICK, drawTempTrack);
			}
		}
		function drawRemoveCapital(event:MouseEvent):void
		{			
			for (var i=0; i<userData.allSpots.length; i++)
			{
				if (userData.allSpots[i].ifIsCity())
				{
					if (cBoxCapital.selected == false)
					{
						if(userData.allSpots[i].getID() == tempTrack.getStartCity().getID() ||
						   userData.allSpots[i].getID() == tempTrack.getEndCity().getID())						
						tempTrack.eraseTrack();						
						userData.allSpots[i].removeCircle();						
						userData.allSpots[i].removeEventListener(MouseEvent.CLICK, drawTempTrack);
					}
					if (cBoxCapital.selected == true)
					{
						userData.allSpots[i].drawCirc();
						userData.allSpots[i].addEventListener(MouseEvent.CLICK, drawTempTrack);
					}
				}				
			}
			drawMyCities();
		}
		function drawRemoveColony(event:MouseEvent):void
		{
			//tempTrack.eraseTrack();
			for (var i=0; i<userData.allSpots.length; i++)
			{
				if (userData.allSpots[i].ifIsColony())
				{
					if (cBoxColony.selected == false)
					{
						if(userData.allSpots[i].getID() == tempTrack.getStartCity().getID() ||
						   userData.allSpots[i].getID() == tempTrack.getEndCity().getID())
						tempTrack.eraseTrack();						
						userData.allSpots[i].removeCircle();						
						userData.allSpots[i].removeEventListener(MouseEvent.CLICK, drawTempTrack);
					}
					if (cBoxColony.selected == true)
					{
						userData.allSpots[i].drawCirc();
						userData.allSpots[i].addEventListener(MouseEvent.CLICK, drawTempTrack);						
					}
				}
			}
			drawMyCities();
		}
		function drawTempTrack(evt:MouseEvent)
		{						
			tempTrack.setCities(userData.myCities[cityChoice.selectedIndex].city, evt.target.parent)
			tempTrack.drawLandTrack();
			distWind.text = Math.round(tempTrack.calcTrackLength()).toString();
		}
	}
}