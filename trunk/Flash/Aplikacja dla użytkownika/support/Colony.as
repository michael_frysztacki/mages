﻿package support
{
	import flash.display.MovieClip;
	import flash.display.Shape;
	import support.Transl;
	public class Colony extends Spot
	{				
		override public function ifIsColony():Boolean
		{
			return true;
		}
		override public function ifIsCity():Boolean
		{
			return false;
		}		
		public function Colony()
		{				
			super();				
		}
		public function setColony(lat:Number, longit:Number, id:Number, obj:MovieClip)
		{
			setIfHasSeaAccess(false);
			setLatitude(lat);
			setLongitude(longit);
			setId(id);
			
			var temp:MovieClip=new MovieClip();
			//temp.graphics.beginFill(0x00EE00);
			//temp.graphics.drawCircle(Transl.longitDegToPix(getLongitude()),
//									   Transl.latDegToPix(getLatitude()), 10);			
//			temp.graphics.endFill();
			setCircle(temp);
			obj.addChild(this);
			this.addChild(getCircle());
		}
		
		public override function getName():String
		{
			return null;
		}
	}
}