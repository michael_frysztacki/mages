﻿package support.UserData
{	
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.*;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.*;
	import support.UserData.*;
	import support.*
		
	public class TravellingUnits
	{			
		public var path:Track;
		public var ifFinished:Boolean;
		public var travUnits:Array;   //array of Unit		
		public var time:Number;
		public var spot1:Spot;
		public var spot2:Spot;
		public var travelTimer:Timer;
		public static var interval:Number = 100;
		
		public function TravellingUnits(mapa:MovieClip, travUnits:Array, time:Number, path:Track):void
		{									
			this.path = new Track(mapa);
			this.path = path;
			this.ifFinished = false;
			this.travUnits = travUnits;
			this.time = time;
			this.spot1 = new Spot();
			this.spot2 = new Spot();
			this.spot1 = spot1;
			this.spot2 = spot2;
			this.travelTimer = new Timer(interval, calcTimeOfTravelLeft()/Transl.timeCoef*(1000/interval));			
			travelTimer.addEventListener(TimerEvent.TIMER, drawUnitsPosition);
			travelTimer.addEventListener(TimerEvent.TIMER_COMPLETE, finished);				
			travelTimer.start();			
		}
		private function finished(evt:TimerEvent)
		{
			ifFinished = true;			
		}
		public function calcIterationsForTimer():Number
		{
			return Math.round(calcTimeOfTravelLeft()/Transl.timeCoef*(1000/interval));
		}
		public function calcTimeOfTravelLeft():Number // in seconds, in real speed
		{			
			var currTim:Date = new Date();
			var currTime:Number = currTim.time;
			var distTravelled:Number = (currTime-time)/1000/3600*getTheMinSpeed()*Transl.timeCoef;
			var distToTravel:Number = path.calcTrackLength();			
			return (distToTravel - distTravelled)/getTheMinSpeed()*3600;						
		}
		public function getTheMinSpeed():Number
		{
			var minSpeed:Number=10000;
			for(var i=0; i<travUnits.length; i++)
			{			
				if(travUnits[i].speed<minSpeed)
				{
					minSpeed=travUnits[i].speed;
				}				
			}			
			return minSpeed;
		}	
		private function drawUnitsPosition(evt:TimerEvent):void
		{
			path.removeUnits();
			var currTim:Date = new Date();
			var currTime:Number = currTim.time;			
			var angleDist:Number = path.getAngleDist();  
			var distTravelled:Number = (currTime-time)/1000/3600*Transl.timeCoef*getTheMinSpeed();   // in km
			var distTravRad:Number = Transl.kmToRadians(distTravelled);	  //  dist in rad;
			if(Transl.kmToRadians(distTravelled) > angleDist)
			{
				trace("jednostki doszly do konca");
			}
			else
			{
				path.drawUnits(path.angleToPosition(distTravRad));
			}
		}
	}
}







