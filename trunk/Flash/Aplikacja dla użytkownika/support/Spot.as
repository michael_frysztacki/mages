﻿package support
{
	import flash.display.MovieClip;
	import flash.display.Shape;
	import support.Transl;
	public class Spot extends MovieClip
	{		
		private var latitude:Number;
		private var longitude:Number;
		private var ifHasSeaAccess:Boolean;				
		private var circle:MovieClip=new MovieClip();
		private var id:Number;		
		public function Spot()
		{
			addChild(circle);
		}		
		public function setId(id:Number)
		{
			this.id = id;
		}
		public function getID():Number
		{
			return this.id
		}
		public function setSpot(longit:Number, lat:Number, id:Number, obj: MovieClip)
		{			
			ifHasSeaAccess = false;
			latitude = lat;
			longitude = longit;									
			this.id = id;
			
		//	circle.graphics.beginFill(0x888888);
//			circle.graphics.drawCircle(Transl.longitDegToPix(longitude),
//									   Transl.latDegToPix(latitude), 10);
			//circle.graphics.endFill();
			obj.addChild(this);
			this.addChild(circle);			
		}
		public function getName():String
		{
			return null;
		}
		public function removeCircle():void
		{
			circle.graphics.clear();
		}
		public function drawCirc()
		{			
			var color:Number;			
			if(this.ifIsCity())
			color = 0xFF4040;
			if(this.ifIsColony())
			color = 0x00EE00;
			if(!this.ifIsCity()&&!this.ifIsColony())
			color = 0x888888;
			if(this.ifHasSeaAccess)
			color = 0x009ACD;
			circle.graphics.clear();
			circle.graphics.beginFill(color);
			circle.graphics.drawCircle(Transl.longitDegToPix(longitude),
									   Transl.latDegToPix(latitude), 10);
			circle.graphics.endFill();			
			this.addChild(circle);
		}
		
		
		public function ifWaterSpot():Boolean
		{
			return ifHasSeaAccess;
		}
		public function setIfHasSeaAccess(ifHas:Boolean):void
		{
			ifHasSeaAccess=ifHas;
		}
		public function getCircle():MovieClip
		{
			return circle;
		}
		public function setCircle(circleToSet:MovieClip):void
		{
			this.circle=circleToSet;
		}
		public function getLatitude():Number
		{
			return latitude;
		}
		public function getLongitude():Number
		{
			return longitude;
		}	
		public function setLatitude(lat:Number):void
		{
			latitude = lat;
		}
		public function setLongitude(longit:Number):void
		{
			longitude = longit;
		}		
		public function ifIsColony():Boolean
		{
			return false;
		}	
		public function ifIsCity():Boolean
		{
			return false;
		}
		
		public function showSpot():void
		{						
			trace(this.getLatitude());			
			trace(this.getLongitude());
		}
		public function setCoordinates(lat:Number, longit:Number):void
		{		
			latitude=lat;
			longitude=longit;		
		}
	}		
}