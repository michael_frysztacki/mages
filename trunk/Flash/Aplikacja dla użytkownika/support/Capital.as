﻿package support
{	
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.*;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import support.Transl;	
	
	public class Capital extends Colony	
	{					
		private var cityName:String;
		//private var cName=new TextField();				
		override public function ifIsColony():Boolean
		{
			return false;
		}
		override public function ifIsCity():Boolean
		{
			return true;
		}		
		public function Capital()
		{	
			super();
		}
		public function setCapital(longit:Number, lat:Number, id:Number, cName:String, obj:MovieClip)
		{				
			setIfHasSeaAccess(false);
			setLatitude(lat);
			setLongitude(longit);
			setId(id);
			cityName=cName;
			
			var temp:MovieClip=new MovieClip;
			//temp.graphics.beginFill(0xFF4040);
			//temp.graphics.drawCircle(Transl.longitDegToPix(getLongitude()),
//									   Transl.latDegToPix(getLatitude()), 10);
//			temp.graphics.endFill();
			setCircle(temp);
			obj.addChild(this);
			this.addChild(getCircle());
			
			this.getCircle().addEventListener(MouseEvent.MOUSE_OVER, showCityName);
							// function(evt:MouseEvent)
							  //{ showCityName(this);});
		}
		
        public function showCityName(evt:MouseEvent):void
		{				
			var cName=new TextField();				
			cName.text=getName();
			cName.x=Transl.longitDegToPix(getLongitude())-40;
			cName.y=Transl.latDegToPix(getLatitude())-300;
			cName.width=10000;				
			var format:TextFormat=new TextFormat();				
			format.color=0xFF0000;
			format.size=45;
			format.bold=true;
			cName.setTextFormat(format);			
			addChild(cName);			
			this.getCircle().addEventListener(MouseEvent.MOUSE_OUT,
						function(evt:MouseEvent){removeChild(cName);
						evt.currentTarget.removeEventListener
						(MouseEvent.MOUSE_OUT,arguments.callee);});												
		}
		public override function getName():String
		{
			return cityName;
		}
	}
}