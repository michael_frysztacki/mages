﻿package support
{
	import flash.display.MovieClip;
	import flash.utils.*;
	import flash.display.Shape;
	import support.*;

	public class Track
	{
		private var startCity:Spot;
		private var endCity:Spot;
		private var track:Line;
		private var MovingUnits:MovieClip;
		private var ifLandTrack:Boolean;

		public function getStartCity():Spot
		{
			return startCity;
		}
		public function getEndCity():Spot
		{
			return endCity;
		}
		public function ifCitiesNull():Boolean
		{
			return (startCity == null || endCity == null);
		}
		public function Track(mapa:MovieClip):void
		{
			startCity = new Spot();
			endCity = new Spot();
			//startCity = null;
			//endCity = null;
			track = new Line(mapa);
			MovingUnits = new MovieClip();
			mapa.addChild(MovingUnits);
		}
		public function setCities(startCity:Spot, endCity:Spot)
		{
			this.startCity = startCity;
			this.endCity = endCity;
		}
		public function getIfLandTrack():Boolean
		{
			return ifLandTrack;
		}
		public function setIfLandTrack(ifLandTrack:Boolean):void
		{
			this.ifLandTrack = ifLandTrack;
		}
		public function angleToPosition(beta:Number):Spot
		{
			var spotArray:Array = DBAccess.getTrackArray(startCity,endCity);
			var spotOnThePosition:Spot = new Spot();
			var distTravelled:Number = 0;//in radians
			if (beta<=getAngleDist() && beta>=0)
			{

				for (var i=0; i<spotArray.length-1; i++)
				{
					if (distTravelled<beta)
					{
						distTravelled +=  Transl.getAlfa(spotArray[i],spotArray[i + 1]);
					}
					if (distTravelled>=beta)
					{
						spotOnThePosition = Transl.getPointBetween(spotArray[i], spotArray[i+1], 
						beta-(distTravelled-Transl.getAlfa(spotArray[i], spotArray[i+1])));
						break;
					}
				}
			}
			else
			{
				trace("Beta - nieodpowiedni argument wejsciowy");
			}
			return spotOnThePosition;
		}
		public function drawUnits(where:Spot):void
		{
			var color:Number;
			color = 0xCD7F32;
			MovingUnits.graphics.beginFill(color);
			MovingUnits.graphics.drawCircle(Transl.longitDegToPix(where.getLongitude()),
			   Transl.latDegToPix(where.getLatitude()), 10);
			MovingUnits.graphics.endFill();
		}
		public function removeUnits():void
		{
			MovingUnits.graphics.clear();
		}
		public function getAngleDist():Number
		{
			var spotArray:Array = DBAccess.getTrackArray(startCity,endCity);
			var angleDist:Number = 0;
			for (var i=0; i<spotArray.length-1; i++)
			{
				angleDist +=  Transl.getAlfa(spotArray[i],spotArray[i + 1]);
			}
			return angleDist;
		}
		public function drawTrack(color):void
		{
			var spotArray:Array = DBAccess.getTrackArray(startCity,endCity);
			track.eraseLine();
			for (var i=0; i<spotArray.length-1; i++)
			{
				track.drawLine(spotArray[i], spotArray[i+1], color);
			}
		}
		public function calcTrackLength():Number
		{
			var spotArray:Array = DBAccess.getTrackArray(startCity,endCity);
			var distance:Number = 0;
			for (var i=0; i<spotArray.length-1; i++)
			{
				distance +=  Transl.getDistance(spotArray[i],spotArray[i + 1]);
			}
			return distance;
		}
		public function drawWaterTrack():void
		{
			var spotArray = DBAccess.getTrackArray(startCity,endCity);
			drawTrack(0x0000ff);
		}
		public function drawLandTrack():void
		{
			drawTrack(0xff0000);
		}
		public function eraseTrack():void
		{
			//startCity = endCity = null;
			track.eraseLine();
		}
	}
}