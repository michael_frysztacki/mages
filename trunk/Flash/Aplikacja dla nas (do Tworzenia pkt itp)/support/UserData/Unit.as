﻿package support.UserData
{	
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.*;
	import flash.text.TextField;
	import flash.text.TextFormat;	
	import support.*;	
	import support.UserData.*;
	
	public class Unit
	{					
		public var id:Number;		
		public var speed:Number;
		public var consumption:Number;
		public var quantity:Number;
		public var attack:Number;
				
		public function Unit(id:Number, speed:Number, consumption:Number, quantity:Number, attack:Number):void
		{
			this.id = id;
			this.speed = speed;
			this.consumption = consumption;
			this.quantity = quantity;			
			this.attack = attack;
		}
		public function getUnitName():String
		{
			var unitName:String;
			if(id == 1)
			unitName = "Turtle Ship";
			if(id == 2)
			unitName = "Hwatcha";
			return unitName;
		}
	}
}