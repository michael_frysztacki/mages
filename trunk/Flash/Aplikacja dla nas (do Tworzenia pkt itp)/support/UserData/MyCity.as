﻿package support.UserData
{	
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.*;
	import flash.text.TextField;
	import flash.text.TextFormat;	
	import support.*;	
	import support.UserData.*;
	
	public class MyCity
	{					
		public var units:Array;   // array of Unit
		public var city:Spot;
		//var foodSet:			
		public function MyCity(city:Spot, units:Array):void
		{			
			this.city = new Spot();			
			this.city = city;
			this.units = units;
		}
	}
}