﻿package support
{
	import flash.display.MovieClip;
	import flash.display.Shape;
	import support.Transl;	
	import support.Drawing.SpotShape;

	public class Spot extends MovieClip
	{		
		private var latitude:Number;
		private var longitude:Number;
		private var ifWater:Boolean;				
		private var ifPolar:Boolean;				
		private var spotShape:SpotShape;
		private var id:Number;		
		
		public function Spot()
		{
			
		}		
		public function setId(id:Number)
		{
			this.id = id;
		}
		public function getID():Number
		{
			return this.id
		}
		public function setSpot(longit:Number, lat:Number, id:Number, mapa: MovieClip)
		{			
			spotShape = new SpotShape();
			ifWater = false;
			ifPolar = false;
			latitude = lat;
			longitude = longit;									
			this.id = id;		
			mapa.addChild(this);
			this.addChild(getShape());			
		}
		public function getName():String
		{
			return null;
		}
		public function getShape():SpotShape
		{
			return spotShape;
		}
		public function removeShape():void
		{
			spotShape.erase();
		}
		public function drawSpot():void
		{			
			var color:Number;
			var size:Number = 1.5; //size of a shape
			if(this.getIfPolar() && this.getIfWater())
				color = 0x03B4C8;
			if(this.getIfPolar() && !this.getIfWater())
				color = 0x5959AB;
			if(!this.getIfPolar() && this.getIfWater())
				color = 0x0000EE;				
			if(!this.getIfWater() && !this.getIfPolar())
				color = 0xFF4040;
			
			if(this.ifCity())
			{				
				spotShape.drawSquare(Transl.longitDegToPix(longitude), Transl.latDegToPix(latitude), color, size);		
			}				
			if(this.ifColony())
				spotShape.drawCirc(Transl.longitDegToPix(longitude), Transl.latDegToPix(latitude), color, size);		
			if(!this.ifCity() && !this.ifColony())
				spotShape.drawCross(Transl.longitDegToPix(longitude), Transl.latDegToPix(latitude), color, size);		
		}
				
		
		public function setIfWater(ifHas:Boolean):void
		{
			ifWater = ifHas;
		}				
		public function setIfPolar(ifPolar:Boolean):void
		{
			this.ifPolar = ifPolar;
		}				
		public function getLatitude():Number
		{
			return latitude;
		}
		public function getLongitude():Number
		{
			return longitude;
		}	
		public function setLatitude(lat:Number):void
		{
			latitude = lat;
		}
		public function setLongitude(longit:Number):void
		{
			longitude = longit;
		}		
		public function ifColony():Boolean
		{
			return false;
		}	
		public function ifCity():Boolean
		{
			return false;
		}
		public function getIfPolar():Boolean
		{
			return ifPolar;
		}
		public function getIfWater():Boolean
		{
			return ifWater;
		}
		
		public function showSpot():void
		{						
			trace(this.getLatitude());			
			trace(this.getLongitude());
		}
		public function setCoordinates(lat:Number, longit:Number):void
		{		
			latitude=lat;
			longitude=longit;		
		}
	}		
}