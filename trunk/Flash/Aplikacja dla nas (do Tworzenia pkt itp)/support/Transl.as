﻿package support
{
	import flash.display.*;
	import flash.system.Capabilities;
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequestMethod;
	import flash.geom.Rectangle;
	import flash.display.MovieClip;
	import flash.text.TextField;
	import support.*;
	
	public class Transl
	{				
		public static var earthRadiusLength:Number = 6371.009;
		public static var timeCoef:Number = 100000.0;
		public static var mapWidth:Number = 4096;
		public static var mapHeight:Number = 2048;
		public static var startingWidth = Capabilities.screenResolutionX;
		public static var startingHeight = Capabilities.screenResolutionY;		
		
		public static function degToRad(deg:Number)
		{
			var rad:Number = deg/360*2*Math.PI;
			return rad;
		}
		public static function radToDeg(rad:Number)
		{
			var deg:Number = rad/(2*Math.PI)*360;
			return deg;
		}
		public static function kmToDegrees(km:Number):Number
		{
			return 360/(earthRadiusLength*2*Math.PI)*km;			
		}
		public static function kmToRadians(km:Number):Number
		{
			return km/(earthRadiusLength*2*Math.PI)*2*Math.PI;
		}
		public static function getMapWidth():Number
		{
			return mapWidth;
		}
		public static function getMapHeight():Number
		{
			return mapHeight;
		}
		public static function ERadius():Number
		{
			return earthRadiusLength;
		}
		public static function getTimeCoef():Number
		{
			return timeCoef;
		}
		public static function kmToPixels(km:Number):Number
		{
			return mapWidth/(6371.009*2*Math.PI)*km;
		}		
		public static function latPixToDeg(pix:Number):Number		// lekka niescislosc, moze do naprawy
		{
			return  (pix-mapHeight/2)/mapHeight*180;
		}
		public static function longitPixToDeg(pix:Number):Number	// lekka niescislosc, moze do naprawy	
		{
			return  (pix-mapWidth/2)/mapWidth*360;
		}
		public static function latDegToPix(degrees:Number):Number
		{
			//return mapHeight/2+degrees/180*mapHeight;	
			return degrees/180*mapHeight;	
		}
		public static function longitDegToPix(degrees:Number):Number
		{		
			//return mapWidth/2+degrees/360*mapWidth;
			return degrees/360*mapWidth;
		}
		public static function getDistance(spot1:Spot, spot2:Spot):Number
		{	
			var a:Number;
			var c:Number;
			var d:Number;
			var dLat:Number = (spot1.getLatitude()* Math.PI/180-spot2.getLatitude()* Math.PI/180);
			var dLon:Number = (spot1.getLongitude()* Math.PI/180-spot2.getLongitude()* Math.PI/180);
			var lat1:Number = spot1.getLatitude()* Math.PI/180;
			var lat2:Number = spot2.getLatitude()* Math.PI/180;
	
			a = Math.sin(dLat/2) * Math.sin(dLat/2) +
					Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
			c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
			d = earthRadiusLength * c;
			return d;
		}	

		public static function coordinatesToCartX(longitude:Number, latitude:Number):Number  // polnoc to Z, wschod to X, Y w kierunku oczu
		{
			var x:Number=earthRadiusLength*Math.sin(longitude* Math.PI/180)*Math.cos(latitude* Math.PI/180);
			return x;
		}
		public static function coordinatesToCartY(longitude:Number, latitude:Number):Number  // polnoc to Z, wschod to X, Y w kierunku oczu
		{
			var y:Number=earthRadiusLength*Math.cos(longitude* Math.PI/180)*Math.cos(latitude* Math.PI/180);
			return y;
		}
		
		public static function coordinatesToCartZ(latitude:Number):Number  // polnoc to Z, wschod to X, Y w kierunku oczu
		{
			var z:Number=earthRadiusLength*Math.sin(latitude* Math.PI/180);
			return z;
		}
			// nie jestem do konca pewny czy ponizej nie ma jakichs bugow
		public static function xyzToLongit(x:Number, y:Number):Number  // polnoc to Z, wschod to X, Y w kierunku oczu
		{		
			var longit:Number=0;
			if(y>=0)
			longit=Math.atan(x/y)* 180/Math.PI;
			else
			{
				if(x<=0)
					longit=-90-(90-Math.atan(x/y)* 180/Math.PI);
				if(x>0)
					longit=180+Math.atan(x/y)* 180/Math.PI;
			}		
			return longit;
		}
		
		
		public static function xyzToLat(z:Number):Number  // polnoc to Z, wschod to X, Y w kierunku oczu
		{
			var longit:Number=Math.asin(z/earthRadiusLength)* 180/Math.PI;
			return longit;
		}
		public static function getAlfa(spot1:Spot, spot2:Spot):Number //zwraca kat miedzy punktami
		{
			var xa:Number = Transl.coordinatesToCartX(spot1.getLongitude(), spot1.getLatitude());
			var ya:Number = Transl.coordinatesToCartY(spot1.getLongitude(), spot1.getLatitude());
			var za:Number = Transl.coordinatesToCartZ(spot1.getLatitude());
			var xb:Number = Transl.coordinatesToCartX(spot2.getLongitude(), spot2.getLatitude());
			var yb:Number = Transl.coordinatesToCartY(spot2.getLongitude(), spot2.getLatitude());
			var zb:Number = Transl.coordinatesToCartZ(spot2.getLatitude());
			var alfa:Number = Math.acos((xa*xb + ya*yb + za*zb ) 
										/(Transl.ERadius()*Transl.ERadius()));
			return alfa;
		}
		public static function getPointBetween(spot1:Spot, spot2:Spot, beta:Number):Spot // zwraca punkt na luku miedzy punktami oddalony o kat beta od punktu spot1
		{			
			var xa:Number=Transl.coordinatesToCartX(spot1.getLongitude(), spot1.getLatitude());
			var ya:Number=Transl.coordinatesToCartY(spot1.getLongitude(), spot1.getLatitude());
			var za:Number=Transl.coordinatesToCartZ(spot1.getLatitude());
			var xb:Number=Transl.coordinatesToCartX(spot2.getLongitude(), spot2.getLatitude());
			var yb:Number=Transl.coordinatesToCartY(spot2.getLongitude(), spot2.getLatitude());
			var zb:Number=Transl.coordinatesToCartZ(spot2.getLatitude());
			var alfa = getAlfa(spot1, spot2);												
			var t:Number;
			var s:Number;
			var xp:Number;
			var yp:Number;
			var zp:Number;								

			t = Math.sin(alfa-beta)/Math.sin(alfa);
			s = Math.sin(beta)/Math.sin(alfa);
			xp = xa*t+xb*s;
			yp = ya*t+yb*s;
			zp = za*t+zb*s;
			var spotBetween:Spot = new Spot();
			spotBetween.setLongitude(Transl.xyzToLongit(xp, yp));
			spotBetween.setLatitude(Transl.xyzToLat(zp));				
			
			return spotBetween;		
		}				
	}		
}

//////////////////   TTTTEEEESSSSTTTTYYYY
/*
//		private static var mapWidth:Number=4096;
//		private static var mapHeight:Number=2048;
		var a:Spot=new Spot(0,0);
		var b:Spot=new Spot(0,45);
		var c:Spot=new Spot(0,90);
		var d:Spot=new Spot(90,45);
		trace("test no 1");
		trace(Transl.kmToDegrees(1000));   
		trace("8.993203");
		trace("test no 2");
		trace(Transl.kmToDegrees(50000));
		trace("449.6602");
		trace("test no 3");
		trace(Transl.kmToPixels(0));
		trace("0");
		trace("test no 4");
		trace(Transl.kmToPixels(6371.009*2*Math.PI));
		trace(4096);			  
		trace("test no 5");
		trace(Transl.latPixToDeg(2047));			  
		trace("90");			
		trace("test no 6");
		trace(Transl.latPixToDeg(0));			
		trace("-90");			
		trace("test no 7");
		trace(Transl.longitPixToDeg(4095));			
		trace("180");			
		trace("test no 8");
		trace(Transl.longitPixToDeg(0));			
		trace("-180");		
		trace("test no 9");
		trace(Transl.getDistance(a,b));
		trace(6371.009*Math.PI*2/8);
		trace("test no 10");
		trace(Transl.getDistance(a,c));
		trace(6371.009*Math.PI*2/4);
		trace("test no 11");
		trace(Transl.getDistance(b,c));
		trace(6371.009*Math.PI*2/8);
		trace("test no 12");
		trace(Transl.getDistance(a,d));
		trace(6371.009*Math.PI*2/4);
		trace("test no 13");
		trace(Transl.getDistance(c,d));
		trace(6371.009*Math.PI*2/8);
		trace("test no 14");
		trace(Transl.coordinatesToCartX(0,0))
		trace(0);
		trace("test no 15");
		trace(Transl.coordinatesToCartX(90,0))
		trace(Transl.ERadius());
		trace("test no 16");
		trace(Transl.coordinatesToCartX(-90,0))
		trace(-Transl.ERadius());
		trace("test no 17");
		trace(Transl.coordinatesToCartX(180,0))
		trace(0);
		trace("test no 18");
		trace(Transl.coordinatesToCartX(-180,0))
		trace(0);
		trace("test no 19");
		trace(Transl.coordinatesToCartX(0,90))
		trace(0);
		trace("test no 20");
		trace(Transl.coordinatesToCartX(90,90))
		trace(0);
		trace("test no 21");
		trace(Transl.coordinatesToCartX(-90,90))
		trace(0);
		trace("test no 22");
		trace(Transl.coordinatesToCartX(180,90))
		trace(0);
		trace("test no 23");
		trace(Transl.coordinatesToCartX(-180,90))
		trace(0);
		trace("test no 24");		
		trace(Transl.coordinatesToCartY(0,0))
		trace(6371.009);
		trace("test no 25");
		trace(Transl.coordinatesToCartY(90,0))
		trace(0);
		trace("test no 26");
		trace(Transl.coordinatesToCartY(-90,0))
		trace(0);
		trace("test no 27");
		trace(Transl.coordinatesToCartY(180,0))
		trace(-Transl.ERadius());
		trace("test no 28");
		trace(Transl.coordinatesToCartY(-180,0))
		trace(-Transl.ERadius());
		trace("test no 29");
		trace(Transl.coordinatesToCartY(0,90))
		trace(0);
		trace("test no 30");
		trace(Transl.coordinatesToCartY(90,90))
		trace(0);
		trace("test no 31");
		trace(Transl.coordinatesToCartY(-90,90))
		trace(0);
		trace("test no 32");
		trace(Transl.coordinatesToCartY(180,90))
		trace(0);
		trace("test no 33");
		trace(Transl.coordinatesToCartY(-180,90))
		trace(0);
		trace("test no 34");
		trace(Transl.coordinatesToCartZ(0));
		trace(0);
		trace("test no 35");
		trace(Transl.coordinatesToCartZ(90));
		trace(Transl.ERadius());
		trace("test no 36");
		trace(Transl.coordinatesToCartZ(-90));
		trace(-Transl.ERadius());
		trace("test no 37");
		trace(Transl.coordinatesToCartZ(-45));
		trace(-Transl.ERadius()*Math.sqrt(2)/2);
		trace("test no 38");
		trace(Transl.coordinatesToCartZ(45));
		trace(Transl.ERadius()*Math.sqrt(2)/2);
		trace("test no 39");
		trace(Transl.xyzToLongit(Transl.ERadius(),0));
		trace("90");
		trace("test no 40");
		trace(Transl.xyzToLongit(Transl.ERadius()*Math.sqrt(2)/2,Transl.ERadius()*Math.sqrt(2)/2));
		trace("45");
		trace("test no 41");
		trace(Transl.xyzToLongit(-Transl.ERadius(),0));
		trace("-90");
		trace("test no 42");
		trace(Transl.xyzToLongit(-Transl.ERadius()*Math.sqrt(2)/2,Transl.ERadius()*Math.sqrt(2)/2));
		trace("-45");
		trace("test no 43");
		trace(Transl.xyzToLongit(0,Transl.ERadius()));
		trace("0");
		trace("test no 44");
		trace(Transl.xyzToLongit(-Transl.ERadius()*Math.sqrt(2)/2,-Transl.ERadius()*Math.sqrt(2)/2));
		trace("-135");
		trace("test no 45");
		trace(Transl.xyzToLongit(0,-Transl.ERadius()));
		trace("-180");
		trace("test no 46");
		trace(Transl.xyzToLongit(Transl.ERadius()*Math.sqrt(2)/2,-Transl.ERadius()*Math.sqrt(2)/2));
		trace("135");
		trace("test no 47");
		trace(Transl.xyzToLat(Transl.ERadius()));
		trace(90);
		trace("test no 48");
		trace(Transl.xyzToLat(Transl.ERadius()*Math.sqrt(2)/2));
		trace(45);
		trace("test no 49");
		trace(-Transl.xyzToLat(Transl.ERadius()));
		trace(-90);
		trace("test no 50");
		trace(-Transl.xyzToLat(Transl.ERadius()*Math.sqrt(2)/2));
		trace(-45);
		
		*/