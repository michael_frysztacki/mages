﻿package support
{
	import flash.display.MovieClip;
	import flash.display.Shape;
	import support.Transl;
	public class Colony extends Spot
	{				
		override public function ifColony():Boolean
		{
			return true;
		}
		override public function ifCity():Boolean
		{
			return false;
		}		
		public function Colony()
		{				
			super();				
		}
		public function setColony(longit:Number, lat:Number, id:Number, obj:MovieClip)
		{
			setSpot(longit, lat, id, obj);						
		}
		
		public override function getName():String
		{
			return null;
		}
	}
}