﻿package support
{
  import  flash.display.MovieClip;
  import  flash.net.URLRequest;
  import  flash.net.URLLoader;
  import  flash.text.TextField;
  import  flash.events.Event;
  import  flash.events.IOErrorEvent;
  import flash.filesystem.*;
  
  public  class  TxtLoader  extends  MovieClip
  {
    public var textArray:Array;
  
    var myLoader:URLLoader;
    var filePath:String;
    var myReq:URLRequest;
  
    public  function  TxtLoader(path:String ):void
    {				  

		filePath  =  path;		  
		myReq  =  new  URLRequest(filePath);		  
		myLoader  =  new  URLLoader();		  
		myLoader.addEventListener(Event.COMPLETE,  fileLoaded);
		myLoader.addEventListener(IOErrorEvent.IO_ERROR,  ioErrorHandler);		    
		try
		{				
			myLoader.load(myReq);
		}
		catch(error:Error)
		{
			trace("Just  catched  the  error  :(");
		}
    }    
    public  function  fileLoaded(e:Event):void
    {     	
      	var temp:Array = new Array;
		temp = e.target.data.split(/\n/);
		addToTextArray(temp);		
    }
	public function addToTextArray(temp:Array)
	{		
		this.textArray = temp;
	}
    public  function  ioErrorHandler(e:IOErrorEvent):void
    {
      	//myTextField.text  =  "There  was  a  problem  loading  a  "+filePath+"  file:  "+e;
    }
  }
}