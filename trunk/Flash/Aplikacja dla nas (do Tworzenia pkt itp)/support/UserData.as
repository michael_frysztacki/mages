﻿package support
{
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.*;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import support.*;
	import support.UserData.*;
	import flash.utils.Timer;

	public class UserData	
	{
		public var myCities:Array;	//Array of MyCity
		public var allSpots:Array;	//Array of Spot
		public var travUnits:Array	//Array of TravellingUnits;		
		private var checkingTimer:Timer;

		public function UserData(myCities:Array, allSpots:Array, travUnits:Array):void
		{
			this.myCities = myCities;
			this.allSpots = allSpots;
			this.travUnits = travUnits;
			checkingTimer = new Timer(1000);
			checkingTimer.addEventListener(TimerEvent.TIMER, handleTravUnits);				
			checkingTimer.start();			
		}
		private function handleTravUnits(evt:TimerEvent)
		{			
			for(var i=0; i<travUnits.length; i++)
			{
				if(travUnits[i].ifFinished)
				{					
					unloadUnits();
					unloadUnits(); 					
					travUnits.splice(i, 1);					
				}
			}
		}
		public function unloadUnits():void    // do dokonczenia
		{
			
		}		
	}
}