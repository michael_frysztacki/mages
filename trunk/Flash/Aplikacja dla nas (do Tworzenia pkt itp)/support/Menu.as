﻿package support
{	
    import flash.display.MovieClip;
	import flash.events.*;
	import fl.controls.*;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.*;
	import fl.data.DataProvider;
	import flash.sampler.NewObjectSample;		
	import support.*;	
	import flash.filesystem.*;

	public class Menu
	{
		var shell:MovieClip;
		var mapa:MovieClip;
		var menuContainer:MovieClip;		
		var showMenu:Button;
		var createFiles:Button;
		
		var showCitiesChBox:CheckBox;
		var showColoniesChBox:CheckBox;
		var showInterSpotsChBox:CheckBox;
		var showWaterChBox:CheckBox;
		var showPolarChBox:CheckBox;
		
		var addWaterChBox:CheckBox;
		var addPolarChBox:CheckBox;
		
		var markWaterChBox:CheckBox;
		var markPolarChBox:CheckBox;
				
		var Mode:ComboBox;				
		var typeComBox:ComboBox;	// miasto/kolonia/punkt do przechodzenia		
		
		var swiat:World;
		
		
		public function Menu(shell:MovieClip, mapa:MovieClip)
		{
			swiat = new World(mapa);
			
			this.shell = shell;
			this.mapa = mapa;			
			menuContainer = new MovieClip();
			menuContainer.graphics.beginFill(0x8B8B83);
			menuContainer.graphics.drawRoundRect(0, 0, 600, 100, 25, 25);			
			menuContainer.graphics.endFill();
			menuContainer.x = 0 - 300;
			menuContainer.y = Transl.startingHeight / 2 - 230 + 1000;
			menuContainer.alpha = 0.95;
			shell.addChild(menuContainer);
			
			var modeChoices:Array = new Array()
			modeChoices[0] = {label:"Show", data:"Show"};
			modeChoices[1] = {label:"Add", data:"Add"};
			modeChoices[2] = {label:"Delete", data:"Delete"};
			modeChoices[3] = {label:"Move", data:"Move"};
			modeChoices[4] = {label:"Mark", data:"Mark"};
			modeChoices.prompt = "Choose Mode";
			Mode = new ComboBox();
			Mode.dropdownWidth = 100;
			Mode.width = 100;
			Mode.move( 10, 10);		
			Mode.selectedIndex = -1;			
			Mode.dataProvider = new DataProvider(modeChoices);						
			Mode.selectedIndex = -1;  
			Mode.prompt = "Choose Mode";
			menuContainer.addChild(Mode);
			
			var addTypes:Array = new Array();
			addTypes[0] = {label:"Spot",data:"Spot"};
			addTypes[1] = {label:"Colony",data:"Colony"};		
			addTypes[2] = {label:"City",data:"City"};		
			typeComBox = new ComboBox();
			typeComBox.dropdownWidth = 100;
			typeComBox.width = 100;
			typeComBox.move( 120, 10+1000);			
			typeComBox.dataProvider = new DataProvider(addTypes);
			typeComBox.selectedIndex = 0;
			menuContainer.addChild(typeComBox);
			
			showMenu = new Button();
			showMenu.width = 100;
			showMenu.height = 20;
			showMenu.label = "Pokaz Menu";
			showMenu.x =  -  showMenu.width / 2;
			showMenu.y = Transl.startingHeight / 2 - 200;
			shell.addChild(showMenu);
			
			createFiles = new Button();
			createFiles.width = 80;
			createFiles.height = 20;
			createFiles.label = "Create Files";
			createFiles.x =  500;
			createFiles.y = 10+1000;
			menuContainer.addChild(createFiles);

			showInterSpotsChBox = new CheckBox();
			showInterSpotsChBox.width = 150;
			showInterSpotsChBox.label = "Show Inter Spots";
			showInterSpotsChBox.x = 120;
			showInterSpotsChBox.y = 10+1000;
			menuContainer.addChild(showInterSpotsChBox);
			
			showColoniesChBox = new CheckBox();
			showColoniesChBox.width = 150;
			showColoniesChBox.label = "Show Colonies";
			showColoniesChBox.x = 120;
			showColoniesChBox.y = 40+1000;
			menuContainer.addChild(showColoniesChBox);

			showCitiesChBox = new CheckBox();
			showCitiesChBox.width = 150;
			showCitiesChBox.label = "Show Cities";
			showCitiesChBox.x = 120;
			showCitiesChBox.y = 70+1000;
			menuContainer.addChild(showCitiesChBox);			
			
			showPolarChBox = new CheckBox();
			showPolarChBox.width = 150;
			showPolarChBox.label = "Show Polar";
			showPolarChBox.x = 300;
			showPolarChBox.y = 10+1000;
			menuContainer.addChild(showPolarChBox);			
			
			showWaterChBox = new CheckBox();
			showWaterChBox.width = 150;
			showWaterChBox.label = "Show Water";
			showWaterChBox.x = 300;
			showWaterChBox.y = 40+1000;
			menuContainer.addChild(showWaterChBox);			
					
			addWaterChBox = new CheckBox();
			addWaterChBox.width = 150;
			addWaterChBox.label = "Water";
			addWaterChBox.x = 235;
			addWaterChBox.y = 40+1000;
			menuContainer.addChild(addWaterChBox);								
			
			addPolarChBox = new CheckBox();
			addPolarChBox.width = 150;
			addPolarChBox.label = "Polar";
			addPolarChBox.x = 235;
			addPolarChBox.y = 10+1000;
			menuContainer.addChild(addPolarChBox);			
			
			markWaterChBox = new CheckBox();
			markWaterChBox.width = 150;
			markWaterChBox.label = "mark Water";
			markWaterChBox.x = 110;
			markWaterChBox.y = 40+1000;
			menuContainer.addChild(markWaterChBox);								
			
			markPolarChBox = new CheckBox();
			markPolarChBox.width = 150;
			markPolarChBox.label = "mark Polar";
			markPolarChBox.x = 110;
			markPolarChBox.y = 10+1000;
			menuContainer.addChild(markPolarChBox);			
			
			showMenu.addEventListener(MouseEvent.CLICK, moveMenu);
			createFiles.addEventListener(MouseEvent.CLICK, saveFiles);			
			Mode.addEventListener("change", chooseMode);		
		}
		
		
		public function remAllSpots()
		{
			for(var i=0; i<swiat.cities.length; i++)
			{				
				(Spot)(swiat.cities[i]).removeShape();										
			}
			for(i=0; i<swiat.colonies.length; i++)
			{
				(Spot)(swiat.colonies[i]).removeShape();										
			}
			for(i=0; i<swiat.interSpots.length; i++)
			{
				(Spot)(swiat.interSpots[i]).removeShape();										
			}			
		}
		public function drawAllSpots()
		{
			if(showCitiesChBox.selected == true)
			{				
				for(var i=0; i<swiat.cities.length; i++)
				{											
					if (showWaterChBox.selected == (Spot)(swiat.cities[i]).getIfWater() && 
						showPolarChBox.selected == (Spot)(swiat.cities[i]).getIfPolar())
					{
						(Spot)(swiat.cities[i]).drawSpot();
					}
				}
			}
			if(showColoniesChBox.selected == true)
			{				
				for(i=0; i<swiat.colonies.length; i++)
				{							
					if (showWaterChBox.selected == (Spot)(swiat.colonies[i]).getIfWater() && 
						showPolarChBox.selected == (Spot)(swiat.colonies[i]).getIfPolar())
					{
						(Spot)(swiat.colonies[i]).drawSpot();
					}
				}
			}
			if(showInterSpotsChBox.selected == true)
			{				
				for(i=0; i<swiat.interSpots.length; i++)
				{							
					if (showWaterChBox.selected == (Spot)(swiat.interSpots[i]).getIfWater() && 
						showPolarChBox.selected == (Spot)(swiat.interSpots[i]).getIfPolar())
					{
						(Spot)(swiat.interSpots[i]).drawSpot();
					}
				}
			}			
		}
		//metoda wyrysowywujaca buttony, dziala tak samo jak jedna metoda nizej, ale ta jest wywolywana podczas eventu
		public function updateShownSpotsOnEvent(event:MouseEvent):void
		{
			updateShownSpots();
		}
		//metoda do wyrysowywania buttonow
		public function updateShownSpots():void
		{					
			remAllSpots();
			drawAllSpots();						
		}
		//metoda obslugujaca zmiane trybow pracy
		public function chooseMode(evt: Event)
		{		
			var i:Number;
			
			drawButtons(); // ustawienie przyciskow po kazdym zmianie trybu obslugi
			
			// usuwanie wszystkich eventow, ktore funkcja "chooseMode" moze wlaczyc			
			// usuwanie eventow odpowiedzialnych za SHOW
			showCitiesChBox.removeEventListener(MouseEvent.CLICK, updateShownSpotsOnEvent);
			showColoniesChBox.removeEventListener(MouseEvent.CLICK, updateShownSpotsOnEvent);
			showInterSpotsChBox.removeEventListener(MouseEvent.CLICK, updateShownSpotsOnEvent);
			showPolarChBox.removeEventListener(MouseEvent.CLICK, updateShownSpotsOnEvent);
			showWaterChBox.removeEventListener(MouseEvent.CLICK, updateShownSpotsOnEvent);			
			
			// usuwanie eventow odpowiedzialnych za ADD
			mapa.removeEventListener(MouseEvent.CLICK, addSpotToTheWorld);
			
			// usuwanie eventow odpowiedzalnych za DELETE
			for(i=0; i<swiat.cities.length; i++)
			{
				swiat.cities[i].getShape().getDrawing().removeEventListener(MouseEvent.CLICK, deleteCity);
			}
			for(i=0; i<swiat.colonies.length; i++)
			{
				swiat.colonies[i].getShape().getDrawing().removeEventListener(MouseEvent.CLICK, deleteColony);
			}
			for(i=0; i<swiat.interSpots.length; i++)
			{
				swiat.interSpots[i].getShape().getDrawing().removeEventListener(MouseEvent.CLICK, deleteInterSpots);
			}
					
			// usuwanie eventow odpowiedzialnych za MOVE
			for(i=0; i<swiat.cities.length; i++)
			{
				swiat.cities[i].getShape().getDrawing().removeEventListener(MouseEvent.MOUSE_DOWN, moveSpot);
			}
			for(i=0; i<swiat.colonies.length; i++)
			{
				swiat.colonies[i].getShape().getDrawing().removeEventListener(MouseEvent.MOUSE_DOWN, moveSpot);
			}
			for(i=0; i<swiat.interSpots.length; i++)
			{
				swiat.interSpots[i].getShape().getDrawing().removeEventListener(MouseEvent.MOUSE_DOWN, moveSpot);
			}
			
			// usuwanie eventow odpowiedzialnych za MARK
			for(i=0; i<swiat.cities.length; i++)
			{
				swiat.cities[i].getShape().getDrawing().removeEventListener(MouseEvent.CLICK, markSpot);
			}
			for(i=0; i<swiat.colonies.length; i++)
			{
				swiat.colonies[i].getShape().getDrawing().removeEventListener(MouseEvent.CLICK, markSpot);
			}
			for(i=0; i<swiat.interSpots.length; i++)
			{
				swiat.interSpots[i].getShape().getDrawing().removeEventListener(MouseEvent.CLICK, markSpot);
			}
			// tutaj konczy sie usuwanie wszystkich mozliwych eventow, ktore moze wlaczyc ta metoda			
			
			switch(Mode.selectedIndex)
			{
				case 0:   //show mode
				{
					showCitiesChBox.addEventListener(MouseEvent.CLICK, updateShownSpotsOnEvent);
					showColoniesChBox.addEventListener(MouseEvent.CLICK, updateShownSpotsOnEvent);
					showInterSpotsChBox.addEventListener(MouseEvent.CLICK, updateShownSpotsOnEvent);
					showPolarChBox.addEventListener(MouseEvent.CLICK, updateShownSpotsOnEvent);
					showWaterChBox.addEventListener(MouseEvent.CLICK, updateShownSpotsOnEvent);			
					break;
				}
				case 1: 	//add mode
				{
					mapa.addEventListener(MouseEvent.CLICK, addSpotToTheWorld);
					break;
				}
				case 2:		//delete mode
				{
					for(i=0; i<swiat.cities.length; i++)
					{
						swiat.cities[i].getShape().getDrawing().addEventListener(MouseEvent.CLICK, deleteCity);
					}
					for(i=0; i<swiat.colonies.length; i++)
					{
						swiat.colonies[i].getShape().getDrawing().addEventListener(MouseEvent.CLICK, deleteColony);
					}
					for(i=0; i<swiat.interSpots.length; i++)
					{
						swiat.interSpots[i].getShape().getDrawing().addEventListener(MouseEvent.CLICK, deleteInterSpots);
					}
					break;
				}
				case 3:		//move mode
				{
					for(i=0; i<swiat.cities.length; i++)
					{
						swiat.cities[i].getShape().getDrawing().addEventListener(MouseEvent.MOUSE_DOWN, moveSpot);
					}
					for(i=0; i<swiat.colonies.length; i++)
					{
						swiat.colonies[i].getShape().getDrawing().addEventListener(MouseEvent.MOUSE_DOWN, moveSpot);
					}
					for(i=0; i<swiat.interSpots.length; i++)
					{
						swiat.interSpots[i].getShape().getDrawing().addEventListener(MouseEvent.MOUSE_DOWN, moveSpot);
					}
					break;
				}
				case 4:		//mark mode
				{			
					for(i=0; i<swiat.cities.length; i++)
					{
						swiat.cities[i].getShape().getDrawing().addEventListener(MouseEvent.CLICK, markSpot);
					}
					for(i=0; i<swiat.colonies.length; i++)
					{
						swiat.colonies[i].getShape().getDrawing().addEventListener(MouseEvent.CLICK, markSpot);
					}
					for(i=0; i<swiat.interSpots.length; i++)
					{
						swiat.interSpots[i].getShape().getDrawing().addEventListener(MouseEvent.CLICK, markSpot);
					}
					break;
				}
				default:
				{
					break;
				}
			}
		}
		//metoda dodawajaca spoty do miasta
		public function addSpotToTheWorld(evt:Event)
		{
			var longit:Number = Transl.longitPixToDeg(mapa.mouseX+2048);
			var lat:Number = Transl.latPixToDeg(mapa.mouseY+1024);
						
			switch(typeComBox.selectedIndex)
			{								
				case 0:				
				{					
					var tempSpot:Spot = new Spot();
					tempSpot.setSpot(longit, lat, swiat.interSpots.length, mapa);
					tempSpot.setIfPolar(addPolarChBox.selected);
					tempSpot.setIfWater(addWaterChBox.selected);
					swiat.interSpots.push(tempSpot);															
					updateShownSpots();
					break;
				}
				case 1:
				{
					var tempColony:Colony = new Colony();
					tempColony.setSpot(longit, lat, swiat.colonies.length, mapa);
					tempColony.setIfPolar(addPolarChBox.selected);
					tempColony.setIfWater(addWaterChBox.selected);
					swiat.cities.push(tempColony);															
					updateShownSpots();
					break;
				}
				default:
				{
					break;
				}
			}			
		}
		// metoda do usuwania miast
		function deleteCity(evt:Event):void   
		{						
			var startVector:Vector.<Spot> = new Vector.<Spot>;  //poczatkowa czesc wektora
			var endVector:Vector.<Spot> = new Vector.<Spot>;	//koncowa czesc wektora
			var index:Number = swiat.cities.indexOf(evt.currentTarget.parent.parent, 0);   //szukamy element wektora, ktory wybralismy mysza
			swiat.cities[index].removeShape();   // usuwamy grafike zaznaczonego przez nas spota
			swiat.cities[index] = null;		
			
			startVector = swiat.cities.slice(0, index);  
			endVector = swiat.cities.slice(index+1, swiat.cities.length);					
			swiat.cities = startVector.concat(endVector);  // laczymy dwa wektory ktore utworzylismy poprzez przeciecie w miejscu "index"
			
			// po zmianach trzeba zaaktualizowac wyrysowane spoty
			updateShownSpots();			
		}
		// metoda do usuwania kolonii
		function deleteColony(evt:Event):void
		{						
			var startVector:Vector.<Spot> = new Vector.<Spot>;  //poczatkowa czesc wektora
			var endVector:Vector.<Spot> = new Vector.<Spot>;	//koncowa czesc wektora
			var index:Number = swiat.colonies.indexOf(evt.currentTarget.parent.parent, 0);   //szukamy element wektora, ktory wybralismy mysza
			swiat.colonies[index].removeShape();   // usuwamy grafike zaznaczonego przez nas spota
			swiat.colonies[index] = null;		
			
			startVector = swiat.colonies.slice(0, index);  
			endVector = swiat.colonies.slice(index+1, swiat.cities.length);					
			swiat.colonies = startVector.concat(endVector);  // laczymy dwa wektory ktore utworzylismy poprzez przeciecie w miejscu "index"
			
			// po zmianach trzeba zaaktualizowac wyrysowane spoty
			updateShownSpots();			
		}
		// metoda do usuwania interSpots
		function deleteInterSpots(evt:Event):void
		{						
			var startVector:Vector.<Spot> = new Vector.<Spot>;  //poczatkowa czesc wektora
			var endVector:Vector.<Spot> = new Vector.<Spot>;	//koncowa czesc wektora
			var index:Number = swiat.interSpots.indexOf(evt.currentTarget.parent.parent, 0);   //szukamy element wektora, ktory wybralismy mysza
			swiat.interSpots[index].removeShape();   // usuwamy grafike zaznaczonego przez nas spota
			swiat.interSpots[index] = null;		
			
			startVector = swiat.interSpots.slice(0, index);  
			endVector = swiat.interSpots.slice(index+1, swiat.cities.length);					
			swiat.interSpots = startVector.concat(endVector);  // laczymy dwa wektory ktore utworzylismy poprzez przeciecie w miejscu "index"
			
			// po zmianach trzeba zaaktualizowac wyrysowane spoty
			updateShownSpots();			
		}
		//metoda do przesuwania spotow, sluzy do aktywowania dragu
		function moveSpot(evt:Event):void
		{			
			evt.currentTarget.startDrag();
			evt.currentTarget.addEventListener(MouseEvent.MOUSE_UP, stopDragSpot);
		}
		//metoda do przesuwania spotow, sluzy do dezaktywowania dragu
		function stopDragSpot(evt:Event):void
		{
			evt.currentTarget.parent.parent.setLongitude(Transl.longitPixToDeg(mapa.mouseX+2048));
			evt.currentTarget.parent.parent.setLatitude(Transl.latPixToDeg(mapa.mouseY+1024));			
			evt.currentTarget.removeEventListener(MouseEvent.MOUSE_UP, stopDragSpot);
			evt.currentTarget.stopDrag();
		}		
		//metoda ta zaznacza dany punkt na morski lub polarny
		public function markSpot(evt:Event):void
		{
			evt.currentTarget.parent.parent.setIfPolar(markPolarChBox.selected);
			evt.currentTarget.parent.parent.setIfWater(markWaterChBox.selected);			
			updateShownSpots();
		}
		//funkcja rysuje buttony w odpowiednich miejscach menu
		public function drawButtons():void
		{
			butPosDefault();	// przed wyrysowaniem przyciskow, trzeba je ustawic na pozycje defaultowe
			switch(Mode.selectedIndex)
			{
				case 0:  //show mode
				{
					createFiles.y -= 1000;
					showInterSpotsChBox.y -= 1000;
					showColoniesChBox.y -= 1000;
					showCitiesChBox.y -= 1000;
					showPolarChBox.y -= 1000;
					showWaterChBox.y -= 1000;		
					break;
				}
				case 1:	//add mode
				{
					createFiles.y -= 1000;
					typeComBox.y -= 1000;					
					addPolarChBox.y -= 1000;
					addWaterChBox.y -= 1000;									
					break;
				}				
				case 4: //mark mode
				{
					createFiles.y -= 1000;
					markPolarChBox.y -= 1000;
					markWaterChBox.y -= 1000;					
					break;
				}
				default:  
				{
					createFiles.y -= 1000;
					break;
				}
			}
		}
		// metoda ustawiajaca wszystkie buttony w niewidoczne miejsca
		public function butPosDefault():void   // funkcja chowajaca przyciski
		{
				showInterSpotsChBox.y = 10+1000;
				showColoniesChBox.y = 40+1000;
				showCitiesChBox.y = 70+1000;				
				showPolarChBox.y = 10+1000;
				showWaterChBox.y = 40+1000;
				addPolarChBox.y = 10+1000;
				addWaterChBox.y = 40+1000;
				markPolarChBox.y = 10+1000;
				markWaterChBox.y = 40+1000;
				typeComBox.move( 120, 10+1000);
				createFiles.y = 10+1000;
		}
		// metoda przesuwajaca menu oraz przycisk aktywujacy menu
		public function moveMenu(evt: MouseEvent)
		{
			if (showMenu.label == "Pokaz Menu")
			{
				menuContainer.y = menuContainer.y - 1000;				
				showMenu.label = "Ukryj Menu";
				showMenu.y -= 50;
			}
			else
			{
				showMenu.y;
				menuContainer.y = menuContainer.y + 1000;				
				showMenu.label = "Pokaz Menu";
				showMenu.y += 50;
			}
		}
		// metoda zapisuja spoty do plikow, wywolywana na wcisniecie przycisku "create files"
		public function saveFiles(evt: MouseEvent)
		{
			var i;
			
			///////////////////////////////////////////////////////////////////////////
			// MIASTA (STOLICE)
			var longitFile:File = File.userDirectory.resolvePath(swiat.directory+"Cities/Longitude1.txt");				
			var latFile:File = File.userDirectory.resolvePath(swiat.directory+"Cities/Latitude1.txt");				
			var waterFile:File = File.userDirectory.resolvePath(swiat.directory+"Cities/Water1.txt");				
			var polarFile:File = File.userDirectory.resolvePath(swiat.directory+"Cities/Polar1.txt");				
			
			var waterFileStream:FileStream = new FileStream();
			var polarFileStream:FileStream = new FileStream();			
			var longitFileStream:FileStream = new FileStream();			
			var latFileStream:FileStream = new FileStream();
			
			latFileStream.open(latFile, FileMode.WRITE);    //wykorzystanie trybu zapisu
			longitFileStream.open(longitFile, FileMode.WRITE);	 //wykorzystanie trybu zapisu
			waterFileStream.open(waterFile, FileMode.WRITE);	 //wykorzystanie trybu zapisu
			polarFileStream.open(polarFile, FileMode.WRITE);	 //wykorzystanie trybu zapisu
			for(i=0; i<swiat.cities.length; i++)
			{	
				//drukowanie longitude do pliku					
				longitFileStream.writeUTFBytes((Spot)(swiat.cities[i]).getLongitude());
				if(i != swiat.cities.length-1)	// jesli nie ostatnia linijka to dodaj znak nowej linii
					longitFileStream.writeUTFBytes("\r\n");
					
				//drukowanie latitude do pliku
				latFileStream.writeUTFBytes((Spot)(swiat.cities[i]).getLatitude());
				if(i != swiat.cities.length-1)	// jesli nie ostatnia linijka to dodaj znak nowej linii
					latFileStream.writeUTFBytes("\r\n");				
					
				//drukowanie ifWater do pliku					
				waterFileStream.writeUTFBytes((Spot)(swiat.cities[i]).getIfWater());
				if(i != swiat.cities.length-1)	// jesli nie ostatnia linijka to dodaj znak nowej linii
					waterFileStream.writeUTFBytes("\r\n");				
				
				//drukowanie ifPolar do pliku
				polarFileStream.writeUTFBytes((Spot)(swiat.cities[i]).getIfPolar());
				if(i != swiat.cities.length-1)	// jesli nie ostatnia linijka to dodaj znak nowej linii
					polarFileStream.writeUTFBytes("\r\n");				
			}
			longitFileStream.close();
			latFileStream.close();			
			waterFileStream.close();
			polarFileStream.close();
			
			///////////////////////////////////////////////////////////////////////////
			//KOLONIE
			longitFile = File.userDirectory.resolvePath(swiat.directory+"Colonies/Longitude1.txt");				
			latFile = File.userDirectory.resolvePath(swiat.directory+"Colonies/Latitude1.txt");		
			waterFile = File.userDirectory.resolvePath(swiat.directory+"Colonies/Water1.txt");				
			polarFile = File.userDirectory.resolvePath(swiat.directory+"Colonies/Polar1.txt");				
			
			longitFileStream = new FileStream();			
			latFileStream = new FileStream();					
			waterFileStream = new FileStream();
			polarFileStream = new FileStream();			
			
			longitFileStream.open(longitFile, FileMode.WRITE);	 //wykorzystanie trybu zapisu
			latFileStream.open(latFile, FileMode.WRITE);    //wykorzystanie trybu zapisu
			waterFileStream.open(waterFile, FileMode.WRITE);	 //wykorzystanie trybu zapisu
			polarFileStream.open(polarFile, FileMode.WRITE);	 //wykorzystanie trybu zapisu			
			for(i=0; i<swiat.colonies.length; i++)
			{	
				//drukowanie longitude do pliku					
				longitFileStream.writeUTFBytes((Spot)(swiat.cities[i]).getLongitude());
				if(i != swiat.colonies.length-1)	// jesli nie ostatnia linijka to dodaj znak nowej linii
					longitFileStream.writeUTFBytes("\r\n");
					
				//drukowanie latitude do pliku
				latFileStream.writeUTFBytes((Spot)(swiat.cities[i]).getLatitude());
				if(i != swiat.colonies.length-1)	// jesli nie ostatnia linijka to dodaj znak nowej linii
					latFileStream.writeUTFBytes("\r\n");				
					
				//drukowanie ifWater do pliku					
				waterFileStream.writeUTFBytes((Spot)(swiat.colonies[i]).getIfWater());
				if(i != swiat.colonies.length-1)	// jesli nie ostatnia linijka to dodaj znak nowej linii
					waterFileStream.writeUTFBytes("\r\n");				
					
				//drukowanie ifPolar do pliku
				polarFileStream.writeUTFBytes((Spot)(swiat.colonies[i]).getIfPolar());
				if(i != swiat.colonies.length-1)	// jesli nie ostatnia linijka to dodaj znak nowej linii
					polarFileStream.writeUTFBytes("\r\n");		
			}
			longitFileStream.close();
			latFileStream.close();			
			waterFileStream.close();
			polarFileStream.close();
			
			///////////////////////////////////////////////////////////////////////////
			// INTERSPOTS
			longitFile = File.userDirectory.resolvePath(swiat.directory+"InterSpots/Longitude.txt");				
			latFile = File.userDirectory.resolvePath(swiat.directory+"InterSpots/Latitude.txt");				
			waterFile = File.userDirectory.resolvePath(swiat.directory+"InterSpots/Water.txt");				
			polarFile = File.userDirectory.resolvePath(swiat.directory+"InterSpots/Polar.txt");						
			
			longitFileStream = new FileStream();			
			latFileStream = new FileStream();			
			waterFileStream = new FileStream();
			polarFileStream = new FileStream();			
			
			longitFileStream.open(longitFile, FileMode.WRITE);	 //wykorzystanie trybu zapisu
			latFileStream.open(latFile, FileMode.WRITE);    //wykorzystanie trybu zapisu
			waterFileStream.open(waterFile, FileMode.WRITE);	 //wykorzystanie trybu zapisu
			polarFileStream.open(polarFile, FileMode.WRITE);	 //wykorzystanie trybu zapisu			
			for(i=0; i<swiat.interSpots.length; i++)
			{	
				//drukowanie longitude do pliku					
				longitFileStream.writeUTFBytes((Spot)(swiat.interSpots[i]).getLongitude());
				if(i != swiat.interSpots.length-1)	// jesli nie ostatnia linijka to dodaj znak nowej linii
					longitFileStream.writeUTFBytes("\r\n");
					
				//drukowanie latitude do pliku
				latFileStream.writeUTFBytes((Spot)(swiat.interSpots[i]).getLatitude());
				if(i != swiat.interSpots.length-1)	// jesli nie ostatnia linijka to dodaj znak nowej linii
					latFileStream.writeUTFBytes("\r\n");				
				
				//drukowanie ifWater do pliku
				waterFileStream.writeUTFBytes((Spot)(swiat.interSpots[i]).getIfWater());
				if(i != swiat.interSpots.length-1)	// jesli nie ostatnia linijka to dodaj znak nowej linii
					waterFileStream.writeUTFBytes("\r\n");				
					
				//drukowanie ifPolar do pliku
				polarFileStream.writeUTFBytes((Spot)(swiat.interSpots[i]).getIfPolar());
				if(i != swiat.interSpots.length-1)	// jesli nie ostatnia linijka to dodaj znak nowej linii
					polarFileStream.writeUTFBytes("\r\n");		
			}
			longitFileStream.close();
			latFileStream.close();			
			waterFileStream.close();
			polarFileStream.close();
		}		
	}
}