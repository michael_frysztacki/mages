﻿package support
{	
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.*;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import support.Transl;	
	
	public class Capital extends Colony	
	{					
		private var cityName:String;
		
		override public function ifColony():Boolean
		{
			return false;
		}
		override public function ifCity():Boolean
		{
			return true;
		}		
		public function Capital()
		{	
			super();
		}
		public function setCapital(longit:Number, lat:Number, id:Number, cName:String, obj:MovieClip)
		{				
			setSpot(longit, lat, id, obj);
			cityName = cName;			
			this.getShape().addEventListener(MouseEvent.MOUSE_OVER, showCityName);
		}
		
        public function showCityName(evt:MouseEvent):void
		{				
			var cName = new TextField();				
			cName.text = getName();
			cName.x = Transl.longitDegToPix(getLongitude())+2;
			cName.y = Transl.latDegToPix(getLatitude())-10;
			cName.width = 150;				
			var format:TextFormat = new TextFormat();				
			format.color = 0x03B4C8;
			format.size = 5;
			format.bold = true;
			cName.setTextFormat(format);			
			addChild(cName);			
			this.getShape().getDrawing().addEventListener(MouseEvent.MOUSE_OUT,
						function(evt:MouseEvent){removeChild(cName);
						evt.currentTarget.removeEventListener
						(MouseEvent.MOUSE_OUT,arguments.callee);});												
		}
		public override function getName():String
		{
			return cityName;
		}
	}
}