﻿package support.Drawing
{
	import flash.display.MovieClip;
	import flash.utils.*;
	import flash.display.Shape;
	import support.*;
	import flash.sampler.Sample;


	public class SpotShape extends MovieClip
	{				
		private var drawing:MovieClip;		
		
		public function SpotShape():void
		{
			drawing = new MovieClip;			
			this.addChild(drawing);
		}		
		public function getDrawing():MovieClip
		{
			return drawing;
		}
		public function drawSquare(x:Number, y:Number, color:Number, radius:Number):void
		{						
			drawing.graphics.beginFill(color);
			drawing.graphics.drawRect(x-radius, y-radius, 2*radius, 2*radius);						
			drawing.graphics.endFill();			
		}
		public function drawCirc(x:Number, y:Number, color:Number, radius:Number):void
		{
			drawing.graphics.beginFill(color);
			drawing.graphics.drawCircle(x,y,radius);
			drawing.graphics.endFill();			
		}
		public function drawCross(x:Number, y:Number, color:Number, radius:Number):void
		{
			drawing.graphics.beginFill(color);
			drawing.graphics.lineStyle(4, color, 100, true, "none", "round", "miter", 1);			
			drawing.graphics.moveTo(x, y-radius);			
			drawing.graphics.lineTo(x, y+radius)
			drawing.graphics.moveTo(x-radius, y);			
			drawing.graphics.lineTo(x+radius, y)
			drawing.graphics.endFill();			
		}
		public function erase():void
		{
			drawing.graphics.clear();
		}		
	}		
}



