﻿package support
{
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.*;		
	import support.*;
	import flash.filesystem.*;	
	


	public class World
	{		
		//public var cities:Array;
		public var cities:Vector.<Spot>;
		//public var colonies:Array;
		public var colonies:Vector.<Spot>;
		//public var interSpots:Array;
		public var interSpots:Vector.<Spot>;
		public var directory:String;

		public function World(mapa:MovieClip):void
		{			
			//cities = new Array;
			cities = new Vector.<Spot>;
			//colonies = new Array;
			colonies = new Vector.<Spot>;
			//interSpots = new Array;
			interSpots = new Vector.<Spot>;
			// ustawienie sciezki do plikow txt, na ktorych program bedize pracowac
			directory = "C:/Users/Kamil/Desktop/Middle Ages/Implementacja by GA1/Flash/Aplikacja dla nas (do Tworzenia pkt itp)/txt Files/";
			
			// WGRYWANIE MIAST
			var longitFile:File = File.userDirectory.resolvePath(directory+"Cities/Longitude.txt");							
			var latFile:File = File.userDirectory.resolvePath(directory+"Cities/Latitude.txt");				
			var waterFile:File = File.userDirectory.resolvePath(directory+"Cities/Water1.txt");							
			var polarFile:File = File.userDirectory.resolvePath(directory+"Cities/Polar1.txt");							
			var cityNamesFile:File = File.userDirectory.resolvePath(directory+"Cities/NamesOfCities.txt");				
			var longitFileStream:FileStream = new FileStream();
			var latFileStream:FileStream = new FileStream();
			var waterFileStream:FileStream = new FileStream();
			var polarFileStream:FileStream = new FileStream();
			var cityNamesFileStream:FileStream = new FileStream();
						
			longitFileStream.open(longitFile, FileMode.READ);
			latFileStream.open(latFile, FileMode.READ);
			waterFileStream.open(latFile, FileMode.READ);
			polarFileStream.open(longitFile, FileMode.READ);
			cityNamesFileStream.open(cityNamesFile, FileMode.READ);
						
			var longitStr:String = longitFileStream.readUTFBytes(longitFile.size);
			var latStr:String = latFileStream.readUTFBytes(latFile.size);
			var waterStr:String = waterFileStream.readUTFBytes(waterFile.size);
			var polarStr:String = polarFileStream.readUTFBytes(polarFile.size);
			var cityNamesStr:String = cityNamesFileStream.readUTFBytes(cityNamesFile.size);
			
			latFileStream.close();
			longitFileStream.close();
			waterFileStream.close();
			polarFileStream.close();
			cityNamesFileStream.close()		
			
			var latArray:Array = new Array;		
			var longitArray:Array = new Array;
			var waterArray:Array = new Array;		
			var polarArray:Array = new Array;
			var cityNamesArray:Array = new Array;
			
			// splitowanie stringow
			latArray = latStr.split(/\n/);
			longitArray = longitStr.split(/\n/);
			waterArray = latStr.split(/\n/);
			polarArray = longitStr.split(/\n/);
			cityNamesArray = cityNamesStr.split(/\n/);
			
			var tempCity:Capital;
			// zapisanie miast w tablicy cities
			for(var i=0; i<latArray.length; i++)
			{				
				tempCity = new Capital();				
				tempCity.setCapital(longitArray[i], latArray[i], i, cityNamesArray[i], mapa);				
				//tempCity.setIfWater(waterArray[i]);
				//tempCity.setIfPolar(polarArray[i]);
				cities[i] = tempCity;	
			}
			
			// WGRYWANIE KOLONII
			longitFile = File.userDirectory.resolvePath(directory+"Colonies/Longitude.txt");							
			latFile = File.userDirectory.resolvePath(directory+"Colonies/Latitude.txt");							
			latFileStream = new FileStream();
			longitFileStream = new FileStream();			
			
			latFileStream.open(latFile, FileMode.READ);
			longitFileStream.open(longitFile, FileMode.READ);			
			
			latStr = latFileStream.readUTFBytes(latFile.size);
			longitStr = longitFileStream.readUTFBytes(longitFile.size);		
			
			latFileStream.close();
			longitFileStream.close();			
			
			latArray = new Array;		
			longitArray = new Array;			
			
			// splitowanie stringow
			latArray = latStr.split(/\n/);
			longitArray = longitStr.split(/\n/);
			
			var tempColony:Colony;
			// zapisanie punktow posrednich w tabeli colonies
			for(i=0; i<latArray.length; i++)
			{				
				tempColony = new Colony();				
				tempColony.setColony(longitArray[i], latArray[i], i, mapa);				
				colonies[i] = tempColony;	
			}		
			
			// WGRYWANIE INTERSPOTS
			longitFile = File.userDirectory.resolvePath(directory+"InterSpots/Longitude.txt");							
			latFile = File.userDirectory.resolvePath(directory+"InterSpots/Latitude.txt");							
			waterFile = File.userDirectory.resolvePath(directory+"InterSpots/Water.txt");							
			polarFile = File.userDirectory.resolvePath(directory+"InterSpots/Polar.txt");							
			latFileStream = new FileStream();
			longitFileStream = new FileStream();			
			waterFileStream = new FileStream();			
			polarFileStream = new FileStream();			
			
			latFileStream.open(latFile, FileMode.READ);
			longitFileStream.open(longitFile, FileMode.READ);			
			waterFileStream.open(waterFile, FileMode.READ);
			polarFileStream.open(polarFile, FileMode.READ);			
			
			latStr = latFileStream.readUTFBytes(latFile.size);
			longitStr = longitFileStream.readUTFBytes(longitFile.size);		
			waterStr = waterFileStream.readUTFBytes(waterFile.size);
			polarStr = polarFileStream.readUTFBytes(polarFile.size);		
						
			longitFileStream.close();			
			latFileStream.close();
			waterFileStream.close();
			polarFileStream.close();			
			
			longitArray = new Array;			
			latArray = new Array;		
			waterArray = new Array;
			polarArray = new Array;
			
			// splitowanie stringow			
			longitArray = longitStr.split("\n");
			latArray = latStr.split("\n");
			waterArray = waterStr.split("\n");
			polarArray = polarStr.split("\n");
			//polarArray = polarStr.split(/\n/);
			
			var tempInterSpot:Spot;
			// zapisanie punktow posrednich w tabeli interspots
			for(i=0; i<latArray.length; i++)
			{				
				tempInterSpot = new Spot();				
				tempInterSpot.setSpot(longitArray[i], latArray[i], i, mapa);				
				tempInterSpot.setIfWater(waterArray[i]=='true');
				trace(waterArray[i]);
				trace(waterArray[i]=='true');
				tempInterSpot.setIfPolar(polarArray[i]=='true');
				interSpots.push(tempInterSpot);
			}		
			
			
		}
	}
}