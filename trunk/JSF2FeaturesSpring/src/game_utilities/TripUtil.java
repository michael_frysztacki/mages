package game_utilities;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import org.springframework.stereotype.Service;


import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Mission;
import entities.RobberyTrip;
import entities.Storage;
import entities.Trip;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.stocks.CityFood;
import entities.stocks.CityStock;
import entities.stocks.CityStockSet;
import entities.stocks.StockDoubleCountComparator;
import entities.stocks.TripStockSet;
import entities.units.IUnitGroup;
import entities.units.TripArmy;
import framework.IStock;
import framework.IntegerStock;
import game_model_interfaces.IUnitModel;
import helpers.TimeTranslation;


@Service("tripComp")
public class TripUtil {

	public class NotEnoughCapacityForFood extends Exception{
		@Override
		public String toString(){
			return "wysy�ana armia nie ma wystarczaj�cej ilo�ci miejsca na jedzenie";
		}
	}
	public class NotEnoughFoodInCity extends Exception{
		@Override
		public String toString(){
			return "brak wystarczaj�cej ilo�ci jedzenia na podr�";
		}
	}
	public Trip sendTrip(Set<? extends IStock<Integer>> stocks, Set<? extends IUnitGroup<Integer>> army, UsersCityConfig startAfter, UsersCityConfig targetAfter, Mission mission, long msOneWayTravelTime) throws NotEnoughCapacityForFood
	{
		Trip trip = null;
		
		TripStockSet tripStock = TripStockSet.createTripStockSet(startAfter.getCityStocks(), stocks);
		/*
		 * TODO - throw exception here, if tried to send trip while having inifucient stocks,
		 * player could hack http request.
		 */
		if(tripStock == null)return null;
		TripArmy tripArmy = TripArmy.createTripArmy(startAfter.getMyArmy(), army);
		/*
		 * TODO - throw Exception below, reason same as above.
		 */
		if(tripArmy == null) return null;
		Set<IntegerStock> armyFood = this.getNeededFoodForTrip(tripArmy, startAfter.getCityStocks() , msOneWayTravelTime*2);
		/*
		 * TODO- tak samo, wyrzuc wyjatek, jesli nie ma wystarczajacej ilosci jedzenia.
		 */
		if(armyFood == null) return null;
		/*
		 * je�li wszystko ok, pobierz jedzenie potrzebne na droge z surowc�w miasta.
		 */
		TripStockSet.createTripStockSet(startAfter.getCityStocks(), armyFood);
		switch(mission)
		{
			case robbery:
				trip = new RobberyTrip(startAfter,targetAfter,nsNow_,tripArmy,tripStock,msOneWayTravelTime);
				break;
		}
		if(trip != null)
		{
			
			System.out.print("jedzenia na droge: " + armyFood);
			startAfter.getOutcomeTrips().addEdgeAndReversedEdge(trip);
			targetAfter.getIncomeTrips().addEdgeAndReversedEdge(trip);
		}
		return trip;
	}
	/*
	 * metoda zwraca ile jednostek jedzenie wojsko potrzebuje na wypraw�
	 * kt�ra trwa msTravelTime ( w milisekundach ). msTravelTime musi by� wi�c ca�kowitym czasem
	 * wyprawy tzn w obie strony i razem z czasem obl�enia.
	 */
	public int getFoodCountNeedForTravel(long msTravelTime,Set<? extends IUnitGroup<Integer>> army, UserConfig owner)
	{
		double needFood = 0.0d;
		UsersCityConfig myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(owner.getCivil());
		for(IUnitGroup<Integer> ug : army)
		{
			IUnitModel unit = myCivil.getByIdInInternalMap(ug.getUnitId(),IUnitModel.class);
			needFood += TimeTranslation.msToHours(msTravelTime) * unit.getEatPerHour( owner ) * ug.getCount();
		}
		return ((int)Math.floor(needFood) + 1) ;
	}
	/*
	 * metoda zwraca jakie jedzenie jest potrzebne na wyprawe w zaleznosci od tego jakie jedzeinie mamy 
	 * dost�pne w kraju
	 * 
	 * Army - armia jaka chcemy wys�a�
	 * hTravleTime - czas wyprawy w obiestrony w godzinach
	 * providedFood - jedzenie ktore mamy dostepne w miescie
	 * 
	 * zwraca null jesli w kraju nie mamy wystarczajacej ilosc jedzenia
	 */
	public Set<IntegerStock> getNeededFoodForTrip(TripArmy army,CityStockSet cityStocks, long msTravelTime)
	{
		//Set<Stock<Integer>> ret = new HashSet<Stock<Integer>>();
		/*
		 * zliczamy ile mamy dostepnego pozywienia ( wieksze od zera )
		 */
		int foodTypes = 0;
		for(CityFood cf : cityStocks.getFood() )
		{
			if(cf.getStorage() == Storage.tak)foodTypes++;
		}
		if(foodTypes == 0)return null;
		/*
		 * zliczamy ile jedzenia w sumie potrzebuje wojsko
		 * na wyprawe w obie strony
		 */
		int needFood = this.getFoodCountNeedForTravel(msTravelTime,army.getUnits(), army.getOwner());
		//availableFood - jedzenie ktore mamy dostepne w miescie
		int availableFood = 0;
		for(CityFood cf : cityStocks.getFood())
		{
			if(cf.getStorage() == Storage.tak)
			{
				if(cf.getVisibleAmount() >= 1.0d)availableFood += (int)Math.floor(cf.getVisibleAmount());
				else availableFood += 1; // je�li jedzenie jest pomi�dzy 0.0 a 1.0, to mamy 1 porcji dostepnych tego jedzenia.
			}
		}
		/*
		 * je�li armia potrzebuje wiecej jedzenia niz jest w miescie zwroc null
		 */
		if(needFood > availableFood)return null;
		
		Set<IntegerStock> ret1 = getFoodAndResourcesFromProvider(cityStocks.getFood(), needFood);
		return ret1;
		
	}
	/*
	 * algorytm w funkcji zabiera wszystkich surowcow po r�wno w miar� mo�liwo�ci tzn. uwzgledniaj�c miejsce
	 *  na zabranie surowcow jak i ilosc poszczegolnych surowcow do zabrania.
	 * argument howMuch mowi nam, ile chcemy w sumie zabra� surowc�w z nfs i fs.
	 * Mo�emy to wykorzyta� np. wyruszaniu armi na wyprawe.
	 * jako fs podajemy ilosc dostepnego pozywienia w mie�cie, a howMuch podajemy ile potrzebuja tego jedzenia
	 * na wypraw�. Nfs zostawiamy puste czyli null, dlatego ze aby wyruszyc na wyprawe wojsko niekoniecznie
	 * musi zabiera� surowce - potrzebuje tylko jedzenie.
	 * w efekcie dostajemy wynik w outputNfs.
	 * 
	 * Drugi przypadek u�ycia tej funkcji to podczas grabie�y, czy obl�enia.
	 * Tam rownie� nale�y zabra� surowc�w w miar� mo�liwo�ci r�wnomiernie. Jako nfs i fs podajemy
	 * surowce dost�pne w mie�cie atakowanym a howMuch podajemy ile surowc�w mozemy maksymalnie zgarn��
	 * tzn. bedzie to liczba : surowce przeciwnika * np. 0.3 ( bo podczas grabiezy zabieramy 30%)
	 */
	public Set<IntegerStock> getFoodAndResourcesFromProvider(List<? extends CityStock> provider, int howMuch)
	{
		int foodTypes = 0;
		Set<IntegerStock> helper = new HashSet<IntegerStock>();
		for(CityStock cf : provider )
		{
			if(cf.getVisibleAmount() >= 1.0d)foodTypes++;
		}
		List stockList = Arrays.asList(provider.toArray());
		Collections.sort(stockList, new StockDoubleCountComparator());
		
		//double perFoodNeed = howMuch / (double)(foodTypes - i);
		int i=0;
		for(CityStock cs : provider )
		{
			if(cs.getVisibleAmount() < 1.0d)continue;
			/*
			 * �rednia ilo�� jak� chcemy zabra� z ka�dego surowca. 
			 */
			double mediumCount = (double)howMuch / (double)(foodTypes - i);
			/*
			 * najmniejsza ilo�� surowca jak� chcemy zabra�.
			 */
			int lowerBoundValue = (int)Math.floor( mediumCount );
			/*
			 * najwi�ksza ilo�� surowca jak� chcemy zabra�.
			 */
			int upperBoundValue = lowerBoundValue + 1;
			/*
			 * ilo�� g�rnych surowc�w jak� wzmiemy
			 */
			int upperBoundAmount = (int) ((mediumCount - Math.floor(mediumCount)) *(double)foodTypes);
			/*
			 * ilo�� dolnych surowc�w jak� wzmiemy. Je�li wszytkie ilo�� surowca s� takie same to przechowuje je w�asnie lowerBoundAmount
			 */
			int lowerBoundAmount = foodTypes - upperBoundAmount;
			i++;
			boolean upperBoundUse = upperBoundAmount == 0 ? false : true;
			if(upperBoundUse)
			{
				int floorCount = (int)Math.floor(cs.getVisibleAmount());
				if(floorCount < upperBoundValue)
				{
					upperBoundUse = false;
				}
			}
			int countToSub = upperBoundUse ? upperBoundValue : lowerBoundValue;
			if(upperBoundUse)
				upperBoundValue -= 1 ;
			else lowerBoundValue -= 1;
			int rest = countToSub - (int)Math.floor(cs.getVisibleAmount());
			if(rest > 0)
			{
				howMuch += rest;
				IntegerStock local = new IntegerStock(cs.getStockType(), (int)Math.floor(cs.getVisibleAmount()));
				helper.add(local);
			}
			else
			{
				IntegerStock local = new IntegerStock(cs.getStockType(), countToSub );
				helper.add(local);
			}
			
		}
		return helper;
	}
	
	
}
