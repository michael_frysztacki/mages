package tests;

public class CloneableClass implements Cloneable{

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private int id;
	@Override
	public Object clone()
	{
		CloneableClass ret;
		try {
			ret = (CloneableClass)super.clone();
			return ret;
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
