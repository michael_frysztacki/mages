package tests;

public class ExtendedCloneableClass extends CloneableClass{

	public int field;
	@Override
	public Object clone()
	{
		ExtendedCloneableClass ret = (ExtendedCloneableClass)super.clone();
		return ret;
	}
}
