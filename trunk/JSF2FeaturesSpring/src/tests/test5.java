package tests;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;




import economycomputation.ConvertBuildingUtil;
import economycomputation.CityCompUtil;
import entities.Civil;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.stocks.CityResource;
import entities.stocks.CityStockSet;
import entities.stocks.TripStock;
import game_model_impl.ConvertBuilding;
import game_model_impl.StockType;

public class test5 {

	/**
	 * @param args
	 * @throws NoSuchFieldException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 */
	public static void main(String[] args) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchFieldException {
		// TODO Auto-generated method stub
		UserConfig uc = new UserConfig();
		uc.setCivil(Civil.korea);
		UsersCityConfig ucc = new UsersCityConfig();
		ucc.setUserConfig(uc);
		CityStockSet fs = new CityStockSet();
		fs.addConcreteFood(new TripStock(StockType.water,10.0d));
		fs.addConcreteFood(new TripStock(StockType.rice,0.0d));
		fs.addConcreteFood(new TripStock(StockType.pork,0.0d));
		NotFoodSet_dead resources = new NotFoodSet_dead();
		resources.addConcreteResource(new CityResource(StockType.peopleCount, 100));
		ucc.setMyResources(resources);
		ucc.setMyFood(fs);
		ucc.getLevelMap().put(203, 1);//woda
		ucc.getLevelMap().put(204, 1);//rice
		ucc.getLevelMap().put(207, 1);//pork
		ucc.getPopulationMap().put(203, 33);//woda
		ucc.getPopulationMap().put(204, 33);//rice
		ucc.getPopulationMap().put(207, 34);//pork
		
		CityCompUtil fc = new CityCompUtil();
		ModelHolder mh = new ModelHolder();
		BuildingController ec = new BuildingController();
		ConvertBuildingUtil cbc = new ConvertBuildingUtil();
		PeopleComp pc = new PeopleComp();
		mh.build();
		fc.setModelHolder(mh);
		fc.setEconomyController(ec);
		fc.setConvertBuildingComp(cbc);
		fc.setPeopleComp(pc);
		HashMap<String, Object> ret = fc.getSN_SOP2(ucc);
		System.out.print("SN: " + ret.get("SN") + "\n");
		System.out.print("SOP: " + ret.get("SOP") );
	}

}
