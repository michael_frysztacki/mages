package tests;

import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;

public class cloneTest {

	/**
	 * @param args
	 * @throws CloneNotSupportedException 
	 */
	public static void main(String[] args) throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		ExtendedCloneableClass c = new ExtendedCloneableClass();
		c.setId(2);
		c.field = 5;
		ExtendedCloneableClass c2 = (ExtendedCloneableClass) c.clone();
		c2.setId(10);
		c2.field = 20;
		System.out.print("c:" + c.getId() + "," + c.field);
		System.out.print("\nc2:" + c2.getId() + "," + c2.field);
	}

}
