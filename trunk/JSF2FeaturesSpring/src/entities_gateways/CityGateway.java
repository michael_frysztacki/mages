package entities_gateways;

import entities.UsersCityConfig;

public interface CityGateway {

	UsersCityConfig fetchCity(int cityId);
}
