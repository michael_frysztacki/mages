package world_definition;

import java.util.List;


import entities.Civil;
import entities.UserConfig;
import game_model_impl.BoosterPrototype;
import game_model_impl.ExtractBuilding;
import game_model_impl.GameModel;
import game_model_impl.PriceStockSetImpl;
import game_model_impl.Requirement;
import game_model_impl.Research;
import game_model_impl.StockType;
import game_model_impl.Text.LanguagePair;
import game_model_impl.Text.Text;
import game_model_impl.Text.Text.Language;
import game_model_impl.boosters.convert_building.ConvertBuildingSubstractor;
import game_model_impl.city_model.CityModelBooster;
import game_model_interfaces.PriceStock;
import game_model_interfaces.IUnitModel.ArmorType;
import game_model_interfaces.IUnitModel.AttackType;
import game_model_interfaces.civilization_defining_interfaces.ICivilizationConfigProvider;


public class Config_CommonCivilization implements ICivilizationConfigProvider{

	@Override
	public Integer getSpongingByPattern(int productionOfOutcomeResource,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PriceStockSetImpl getCostByPattern(PriceStockSetImpl baseCost,
			int patternId, int lvl, UserConfig uc) {
		switch( patternId )
		{
			case 0:
				return new PriceStockSetImpl(baseCost, Math.pow(1.32d, lvl) );
			default:
				return null;
		}
	}

	@Override
	public Integer getExtractionByPattern(Integer peopleCount,
			Integer lodeFactor, int patternId) {
		
		switch(patternId)
		{
			case 0:
				return (int)((double)peopleCount * (double)lodeFactor * (25.0d/24.0d)); // w czasie gry cz�owiek wydobywa 0.25 drzewa na dob�. Zatem wydobywa 25 drewna na dzie� w czasie rzeczywistym, zatem 25/24 na godzin� w czasie rzeczyw.
			default:
				return null;
		}
	}

	@Override
	public Integer getProductionByPattern(int peopleCount, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMaxPeopleByPattern(int baseAmount, int buildingLvl,
			int patternId) {
		
		switch(patternId)
		{
			case 0:
				//WydobMet = 30 * Poziom * 1,1 ^ Poziom
				double a = 10.0d / ( Math.pow(1.1d, 1) );
				return (int) (a * buildingLvl * Math.pow(1.1d, buildingLvl));
			default:
				return null;
		}
	}

	@Override
	public Integer getResearchBuildTimeByPattern(Long timeFactor, int rlvl,
			int blvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getUnitBuildTimeByPattern(Long baseBuildTime, int blevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getBuildingBuildTimeByPattern(int patternId, int currLvl,
			Long baseBuildTime) {
		switch( patternId )
		{
			case 0:
				return (int) (Math.pow(1.3d, currLvl)* baseBuildTime);
			default:
				return null;
		}
	}

	@Override
	public Integer getMaxLvlByPattern(Integer myLevel, int patternId) {
		
		switch( patternId)
		{
			case 0:
				return 0;
			case 1:
				return 1;
			default:
				return null;
		}
	}

	@Override
	public Integer boostBuildTimePattern(int currentTime, int brLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getImprovedProductionByPattern(int currentProduction,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getImprovedInputByPattern(int currentInput,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer maxPeopleBoostPattern(int currentMaxPeople,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PriceStockSetImpl PriceBoostPattern(PriceStockSetImpl current,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer ArmorBoostPattern(int current, int improvementLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean boostAntarcticaSettlemantAbility(int reserachLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostSatisfaction(int currentSatisf, int objectLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostCityRadius(int currentRadius, int objectLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostMoralFactor(int currentMoral, int objectLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostFortificationRepairPerHour(int currentRepairPerHour,
			int objectLvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostHP(int currentHP, int objectLvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostDirectionsAmount(int objectLvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getPercentRobberySteal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Civil getCivilization() {
		return Civil.common;
	}

	@Override
	public Civil extendsCivilization() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StockType getMainResource() {
		return null;
	}

	@Override
	public Double getPeopleFallRate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean canSettleAntarctica() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer mapDirectionsAmount() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Text[] getNames() {
		// TODO Auto-generated method stub
		return new Text[]
		{
			new Text(0,0,0,new LanguagePair(Language.PL, "Wieki Ciemne"),
					       new LanguagePair(Language.ENG, "Dark Age")),
					 
			new Text(0,0,1,new LanguagePair(Language.PL, "Epoka Feudalna"),
						   new LanguagePair(Language.ENG, "Feudal Age")),
			
			new Text(0,0,2,new LanguagePair(Language.PL,"Epoka Zamk�w"),
						   new LanguagePair(Language.ENG, "Castle Age")),
								
			new Text(0,0,3,new LanguagePair(Language.PL,"Epoka Imperialna"),
					  	   new LanguagePair(Language.ENG, "Imperial Age")),
			
			new Text(1,  new LanguagePair(Language.PL,"Tartak"),
				  	     new LanguagePair(Language.ENG, "Sawmill")),
			
			new Text(2,  new LanguagePair(Language.PL,"Kamienio�om"),
			  	     new LanguagePair(Language.ENG, "Stone-pit")),
			
			new Text(3,  new LanguagePair(Language.PL,"Kopalnia �elaza"),
			  	     new LanguagePair(Language.ENG, "Ironmine")),
			
			new Text(4,  new LanguagePair(Language.PL,"Dostawca wody"),
			  	     new LanguagePair(Language.ENG, "Water provider"))
			
		};
	}
	
	@Override
	public GameModel[] gameModels() {
		return new GameModel[]
		{
			new Research(0, 3, 0,
					new PriceStockSetImpl(
						
					), 1000, 0),
			/*
			 * @maxPeople
		     *    jeden cz�owiek wydobywa 0.25 drewna na godzine w czasie gry. To znaczy �e w rzeczywistej godzinie
		     *    b�dzie wydobywa� 25/h. maksymalna ilo�� ludzi na 1 lvl tartaku b�dzie 10, czyli maksymalnie 
		     *    tartak b�dzie wydobywa� 250/h w czasie rzeczywistym.
			 * 
			 */
			new ExtractBuilding(1, 20, 0, 0, 0, 0,
					new PriceStockSetImpl(
						new PriceStock(StockType.wood, 500),
						new PriceStock(StockType.stone, 200)), 
				5l*60l*1000l, StockType.wood),
				
			new ExtractBuilding(2, 10, 0, 0, 0, 0, 
					new PriceStockSetImpl(
							new PriceStock(StockType.wood,400),
							new PriceStock(StockType.stone,400)
							)
					, 5l*60l*1000l, StockType.stone),
					
			new ExtractBuilding(3, 50, 0, 0, 0, 0,
					new PriceStockSetImpl(
						new PriceStock(StockType.wood, 5000),
						new PriceStock(StockType.stone, 5000)
							)
					, 30l*60l*1000l, StockType.iron),
					
			new ExtractBuilding(4, 30, 0, 0, 0, 0,
					new PriceStockSetImpl(
						new PriceStock(StockType.stone, 400),
						new PriceStock(StockType.wood, 200)
							)
					, 5l*60l*1000l, StockType.water)
			
		};
	}

	@Override
	public Requirement[] getRequirements() {
		
		return new Requirement[]
		{
			new Requirement(1, 0, false, 0),
			new Requirement(2, 0, false, 0),
			new Requirement(3, 1, false, 0),
			new Requirement(4, 0, false, 0)
		};
	}
	
	@Override
	public ConvertBuildingSubstractor[] convertBuildingSubstractors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BoosterPrototype[] boosters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Text[] getDescriptions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getPeopleLimitInHouses(int housesLevel, int basePeopleLimit,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArmorType boostArmorType(int patternId, int rLevel, ArmorType current) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AttackType boostAttackType(int patternId, int rLevel,
			AttackType current) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostAttack(int patternId, int rLevel, int current) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostCapacity(int current, int improvementLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostSpeed(int current, int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getFortyfiactionHP(int baseHP, Integer fortyficationLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getFortyficationRepairRate(int baseRepairRate,
			Integer fortyficationLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getRadius(int baseRadius, Integer radiusResearchLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostRadius(int current, int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostFortyficationHP(int current, int improvementLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostFortyficationRepairRate(int current,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StockType getMajorMeat() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StockType getMajorCarbo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StockType getMajorAddition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CityModelBooster[] cityModelBoosters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationProtectionLevel(AttackType aType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationProtectionLevel(int unitId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationArmorBoost(int fortyficationLevel,
			int currentArmor) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationAttackBoost(int fortyficationLevel,
			int currentAttack) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getSpongingPatternById(String convBuildId_stockType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getCostPatternById(int gameModelId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getExtractionPatternById(int extractBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getProductionPatternById(int convertBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMaxPeoplePatternById(int extractBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getResearchBuildTimePatternById(int reseachId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getUnitBuildTimePatternById(int unitId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getBuildingBuildTimePatternById(int buildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getRadiusPatternById(int radiusReseach) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Integer> excludedObjects() {
		// TODO Auto-generated method stub
		return null;
	}

}
