package world_definition;

import java.util.List;


import entities.UsersCityConfig;
import game_model_impl.MyClassCastException;
import game_model_impl.RequirementNode;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.IResearchModel;



public class DumpUtility {

	public static void dumpCivilizationBuildings(UsersCityConfig civil, int idOfTierResearch, UsersCityConfig ucc)
	{
		System.out.print("\nCywilizacja " + civil.getMyCivilization() + "\n");
		IResearchModel tier = civil.getById(idOfTierResearch, IResearchModel.class);
		System.out.print("   Budynki Tier 1: \n");
		List<RequirementNode> list = tier.getRequirementNode().whatChildrenCanBeUpgradedForFollowingArguments(0, 0, IBuildingModel.class, ucc.getUserConfig().getCivil());
		for(RequirementNode children : list)
		{
			System.out.print("       -" + children.getMyGameModel().getName(ucc) + "\n");
		}
		System.out.print("   Budynki Tier 2: \n");
		list = tier.getRequirementNode().whatChildrenCanBeUpgradedForFollowingArguments(1, 0, IBuildingModel.class, ucc.getUserConfig().getCivil());
		for(RequirementNode children : list)
		{
			System.out.print("       -" + children.getMyGameModel().getName(ucc) + "\n");
		}
		System.out.print("   Budynki Tier 3: \n");
		list = tier.getRequirementNode().whatChildrenCanBeUpgradedForFollowingArguments(2, 0, IBuildingModel.class, ucc.getUserConfig().getCivil());
		for(RequirementNode children : list)
		{
			System.out.print("       -" + children.getMyGameModel().getName(ucc) + "\n");
		}
		System.out.print("   Budynki Tier 4: \n");
		list = tier.getRequirementNode().whatChildrenCanBeUpgradedForFollowingArguments(3, 0, IBuildingModel.class, ucc.getUserConfig().getCivil());
		for(RequirementNode children : list)
		{
			System.out.print("       -" + children.getMyGameModel().getName(ucc) + "\n");
		}
		
	}
	
	public static void dumpCivilizationResearches(UsersCityConfig civil, int idOfTierResearch, UsersCityConfig ucc)
	{
		System.out.print("\nCywilizacja " + civil.getMyCivilization() + "\n");
		IResearchModel tier = civil.getById(idOfTierResearch, IResearchModel.class);
		System.out.print("   Badania Tier 1: \n");
		List<RequirementNode> list = tier.getRequirementNode().whatChildrenCanBeUpgradedForFollowingArguments(0, 0, IBuildingModel.class, ucc.getUserConfig().getCivil());
		for(RequirementNode children : list)
		{
			List<IResearchModel> researchesInBuilding = children.getChildren(IResearchModel.class);
			for(IResearchModel researchInBuilding : researchesInBuilding)
			{
				if( researchInBuilding.getCivilizationHolder().findCivilizationInChain(researchInBuilding.getMyCivilization().getCivilization(), ucc.getUserConfig().getMyCivilization().getCivilization()) == null)
				{
					continue;
				}
				RequirementNode tierReqNode = researchInBuilding.getRequirementNode().getParentById(idOfTierResearch);
				if(tierReqNode == null)
				{
					System.out.print("       -" + researchInBuilding.getName(ucc) + "\n");
				}
			}
		}
		
		System.out.print("   Badania Tier 2: \n");
		list = tier.getRequirementNode().whatChildrenCanBeUpgradedForFollowingArguments(1, 0, IBuildingModel.class, ucc.getUserConfig().getCivil());
		for(RequirementNode children : list)
		{
			List<IResearchModel> researchesInBuilding = children.getChildren(IResearchModel.class);
			for(IResearchModel researchInBuilding : researchesInBuilding)
			{
				if( researchInBuilding.getCivilizationHolder().findCivilizationInChain(researchInBuilding.getMyCivilization().getCivilization(), ucc.getUserConfig().getMyCivilization().getCivilization()) == null)
				{
					continue;
				}
				RequirementNode tierReqNode = researchInBuilding.getRequirementNode().getParentById(idOfTierResearch);
				if(tierReqNode == null)
				{
					System.out.print("       -" + researchInBuilding.getName(ucc) + "\n");
				}
			}
		}
		
		System.out.print("   Badania Tier 3: \n");
		list = tier.getRequirementNode().whatChildrenCanBeUpgradedForFollowingArguments(2, 0, IBuildingModel.class, ucc.getUserConfig().getCivil());
		for(RequirementNode children : list)
		{
			List<IResearchModel> researchesInBuilding = children.getChildren(IResearchModel.class);
			for(IResearchModel researchInBuilding : researchesInBuilding)
			{
				if( researchInBuilding.getCivilizationHolder().findCivilizationInChain(researchInBuilding.getMyCivilization().getCivilization(), ucc.getUserConfig().getMyCivilization().getCivilization()) == null)
				{
					continue;
				}
				RequirementNode tierReqNode = researchInBuilding.getRequirementNode().getParentById(idOfTierResearch);
				if(tierReqNode == null)
				{
					System.out.print("       -" + researchInBuilding.getName(ucc) + "\n");
				}
			}
		}
		
		System.out.print("   Badania Tier 4: \n");
		list = tier.getRequirementNode().whatChildrenCanBeUpgradedForFollowingArguments(3, 0, IBuildingModel.class, ucc.getUserConfig().getCivil());
		for(RequirementNode children : list)
		{
			List<IResearchModel> researchesInBuilding = children.getChildren(IResearchModel.class);
			for(IResearchModel researchInBuilding : researchesInBuilding)
			{
				if( researchInBuilding.getCivilizationHolder().findCivilizationInChain(researchInBuilding.getMyCivilization().getCivilization(), ucc.getUserConfig().getMyCivilization().getCivilization()) == null)
				{
					continue;
				}
				RequirementNode tierReqNode = researchInBuilding.getRequirementNode().getParentById(idOfTierResearch);
				if(tierReqNode == null)
				{
					System.out.print("       -" + researchInBuilding.getName(ucc) + "\n");
				}
			}
		}
		
		
	}
}
