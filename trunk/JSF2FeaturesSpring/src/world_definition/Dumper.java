package world_definition;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import testing_component.world_definition_for_testing.PointInitializator;

import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Civil;
import entities.Colony;
import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IResearchModel;


public class Dumper {

	/**
	 * @param args
	 * @throws BuildModelException 
	 */
	public static void main(String[] args) throws BuildModelException {
		
		SpringJSFUtil su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_CommonCivilization(),new CommonParameters() );
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
		UserConfig uc = new UserConfig("friko", "pass", wd , Civil.common, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		DumpUtility.dumpCivilizationBuildings(uc.getMyCivilization(), 0, ucc);
		
		DumpUtility.dumpCivilizationResearches(uc.getMyCivilization(), 0, ucc);
		
		UsersCityConfig commonCivil = aw.getWorldModel(0).getCivilization(Civil.common);
		IExtractBuildingModel dostawcaWody = commonCivil.getById(1, IExtractBuildingModel.class);
		System.out.print(dostawcaWody.dump(Language.PL , Civil.common ));
		
		
		
	}

}
