package world_definition;

import game_model_interfaces.civilization_defining_interfaces.ICommonParameters;

public class CommonParameters implements ICommonParameters{

	@Override
	public int getFurtherAdditionSatisfaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getFurtherCarboSatisfaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getFurtherMeatSatisfaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMajorCarboSatisfaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getMajorMeatSatisfaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getWaterSatisfaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int satisfactionCooldownSec() {
		return 0;
	}

	@Override
	public int getFirstCitizensAmount() {
		return 100;
	}

	@Override
	public int getMaxFightRounds() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getBirthMultiplier() {
		return 0;
	}

	/*
	 * Ustalilismy, ze jeden czlowiek je 3 posilki na dzien W CZASIE GRY. W zwiazku z tym, 
	 * ze tempo gry jest przyspieszone oko�o 100 razy, cz�owiek b�dzie jad� 300 jednostek jedzenia na dzie� (czas rzeczywisty).
	 *  100 (tyle chyba ustalamy jako liczba pocz�tkowa) pocz�tkowych ludzi b�dzie wi�c je�� 
	 *  100*300* /3600/24 = 0,3472 zarcia na sekunde w poczatkowej fazie.(non-Javadoc)
	 *  na godzin� jedz� wi�c 0,3472 * 3600 = 1249,92
	 *  Jeden cz�owiek b�dzie wi�c jad� 1250/100 = 12,5
	 */
	@Override
	public int getEatPerHour() {
		return 12.5d;
	}

	@Override
	public int getMajorAdditionSatisfaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double chanceForTakingOverExtractBuilding() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
