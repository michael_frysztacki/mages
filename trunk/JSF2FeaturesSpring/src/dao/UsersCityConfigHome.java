package dao;
// default package
// Generated 2012-09-13 22:47:10 by Hibernate Tools 3.4.0.CR1


import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import entities.UserConfig;
import entities.UsersCityConfig;


/**
 * Home object for domain model class UsersCityConfig.
 * @see .UsersCityConfig
 * @author Hibernate Tools
 */
public class UsersCityConfigHome implements Serializable{

	private static final Log log = LogFactory.getLog(UsersCityConfigHome.class);

	private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();
	
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext()
					.lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(UsersCityConfig transientInstance) {
		log.debug("persisting UsersCityConfig instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}
	
	public Integer save(UsersCityConfig instance) {
		log.debug("attaching transient UsersCityConfig instance");
		try {
			Integer assignedId = (Integer)sessionFactory.getCurrentSession().save(instance);
			log.debug("attach successful");
			return assignedId;
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachDirty(UsersCityConfig instance) {
		log.debug("attaching dirty UsersCityConfig instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsersCityConfig instance) {
		log.debug("attaching clean UsersCityConfig instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(UsersCityConfig persistentInstance) {
		log.debug("deleting UsersCityConfig instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsersCityConfig merge(UsersCityConfig detachedInstance) {
		log.debug("merging UsersCityConfig instance");
		try {
			UsersCityConfig result = (UsersCityConfig) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public UsersCityConfig findById(int id) {
		log.debug("getting UsersCityConfig instance with id: " + id);
		try {
			UsersCityConfig instance = (UsersCityConfig) sessionFactory
					.getCurrentSession().get("pojo.UsersCityConfig", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	
	public Set<UsersCityConfig> getUserCities(UserConfig userConfig)
	{
		log.debug("finding UsersCityConfig instance by example");
		try {
			Session session = sessionFactory.getCurrentSession();
			//userConfig = (UserConfig)session.merge(userConfig);
//			List cities = session.createCriteria(UserConfig.class)
//				    .createAlias("usersCityConfigs", "uccs")
//				    .createAlias("mate", "mt")
//				    .add( Restrictions.eqProperty("kt.name", "mt.name") )
//				    .list();
			
			userConfig = (UserConfig)session.get("pojo.UserConfig", userConfig.getId());
			Hibernate.initialize(userConfig.getUsersCityConfigs());
			Set<UsersCityConfig> ret = userConfig.getUsersCityConfigs();
			return ret;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByExample(UsersCityConfig instance) {
		log.debug("finding UsersCityConfig instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("pojo.UsersCityConfig")
					.addEdgeAndReversedEdge(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	

}
