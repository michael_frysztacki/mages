package dao;
// default package
// Generated 2012-09-13 22:47:10 by Hibernate Tools 3.4.0.CR1


import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import entities.Point;


/**
 * Home object for domain model class Point.
 * @see .Point
 * @author Hibernate Tools
 */
public class PointHome {

	private static final Log log = LogFactory.getLog(PointHome.class);

	private SessionFactory sessionFactory;// = getSessionFactory();

	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
//	protected SessionFactory getSessionFactory() {
//		try {
//			return sessionFactory;
//		} catch (Exception e) {
//			log.error("Could not locate SessionFactory in JNDI", e);
//			throw new IllegalStateException(
//					"Could not locate SessionFactory in JNDI");
//		}
//	}

	public void persist(Point transientInstance) {
		log.debug("persisting Point instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Point instance) {
		log.debug("attaching dirty Point instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Point instance) {
		log.debug("attaching clean Point instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Point persistentInstance) {
		log.debug("deleting Point instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Point merge(Point detachedInstance) {
		log.debug("merging Point instance");
		try {
			Point result = (Point) sessionFactory.getCurrentSession().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Point findById(java.lang.Integer id) {
		log.debug("getting Point instance with id: " + id);
		try {
			Point instance = (Point) sessionFactory.getCurrentSession().get(
					"Point", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Point instance) {
		log.debug("finding Point instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("Point").addEdgeAndReversedEdge(Example.create(instance))
					.list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
