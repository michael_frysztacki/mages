package dao;

import java.util.List;
import java.util.Set;

import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;

import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.UnitBuild;


public class UnitBuildHome {

	private static final Log log = LogFactory.getLog(UnitBuildHome.class);

	private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();
	
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) new InitialContext()
					.lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(UnitBuild transientInstance) {
		log.debug("persisting UnitBuild instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(UnitBuild instance) {
		log.debug("attaching dirty UnitBuild instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UnitBuild instance) {
		log.debug("attaching clean UnitBuild instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(UnitBuild persistentInstance) {
		log.debug("deleting UnitBuild instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UnitBuild merge(UnitBuild detachedInstance) {
		log.debug("merging UnitBuild instance");
		try {
			UnitBuild result = (UnitBuild) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public UnitBuild findById(int id) {
		log.debug("getting UnitBuild instance with id: " + id);
		try {
			UnitBuild instance = (UnitBuild) sessionFactory
					.getCurrentSession().get("pojo.UnitBuild", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(UnitBuild instance) {
		log.debug("finding UnitBuild instance by example");
		try {
			List results = sessionFactory.getCurrentSession()
					.createCriteria("pojo.UnitBuild")
					.addEdgeAndReversedEdge(Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
}
