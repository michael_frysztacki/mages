package dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import entities.UserConfig;


public interface IUserConfigDao {

	UserConfig findByName(String nick);
	void setSessionFactory(SessionFactory sessionFactory);
	public void persist(UserConfig transientInstance);
	public void attachDirty(UserConfig instance);
	public void attachClean(UserConfig instance);
	public void delete(UserConfig persistentInstance) ;
	public UserConfig merge(UserConfig detachedInstance);
	public UserConfig findById(java.lang.Integer id);
	public List findByExample(UserConfig instance);
}
