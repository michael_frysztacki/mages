package dao;

import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;

import entities.UserConfig;
import entities.UsersCityConfig;


public interface IUsersCityConfigDao {

	void setSessionFactory(SessionFactory sessionFactory);
	public void persist(UsersCityConfig transientInstance);
	public void attachDirty(UsersCityConfig instance);
	public void attachClean(UsersCityConfig instance);
	public void delete(UsersCityConfig persistentInstance) ;
	public UsersCityConfig merge(UsersCityConfig detachedInstance);
	public UsersCityConfig findById(int id);
	public List findByExample(UsersCityConfig instance);
	public Set getUserCities(UserConfig userConfig);
	
}
