package dao;
// default package
// Generated 2012-09-13 22:47:10 by Hibernate Tools 3.4.0.CR1


import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.annotation.Aspect;
import org.hibernate.Hibernate;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entities.UserConfig;
//import org.hibernate.criterion.Example;



/**
 * Home object for domain model class UserConfig.
 * @see .UserConfig
 * @author Hibernate Tools
 */

//@Repository("ucdao")
public class UserConfigHomeImpl implements IUserConfigDao{

	//private static final Log log = LogFactory.getLog(UserConfigHome.class);
	
	
	private SessionFactory sessionFactory;// = HibernateUtil.getSessionFactory();
	
	public void setSessionFactory(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
    public SessionFactory getSessionFactory()
    {
    	return sessionFactory;
    }
	public void persist(UserConfig transientInstance) {
		//log.debug("persisting UserConfig instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			//log.debug("persist successful");
		} catch (RuntimeException re) {
			//log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(UserConfig instance) {
	//	log.debug("attaching dirty UserConfig instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			//log.debug("attach successful");
		} catch (RuntimeException re) {
			//log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UserConfig instance) {
		//log.debug("attaching clean UserConfig instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
		//	log.debug("attach successful");
		} catch (RuntimeException re) {
			//log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(UserConfig persistentInstance) {
	//	log.debug("deleting UserConfig instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			//log.debug("delete successful");
		} catch (RuntimeException re) {
			//log.error("delete failed", re);
			throw re;
		}
	}

	public UserConfig merge(UserConfig detachedInstance) {
		//log.debug("merging UserConfig instance");
		try {
			UserConfig result = (UserConfig) sessionFactory.getCurrentSession()
					.merge(detachedInstance);
			//log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			//log.error("merge failed", re);
			throw re;
		}
	}
	public UserConfig findById(java.lang.Integer id) {
		//log.debug("getting UserConfig instance with id: " + id);
		try {
			UserConfig instance = (UserConfig) sessionFactory
					.getCurrentSession().get("pojo.UserConfig", id);
			//Hibernate.initialize(instance.getUsersCityConfigs());
			if (instance == null) {
				//log.debug("get successful, no instance found");
			} else {
				//log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			//log.error("get failed", re);
			throw re;
		}
	}
	public UserConfig findByName(String nick)
	{
		//log.debug("getting UserConfig instance with nick: " + nick);
		try {
			Session session = sessionFactory.getCurrentSession();
			Query q = session.createQuery(
		                "Select uc from UserConfig uc " +
		                "where uc.nick = :nick"
		            );
            q.setParameter("nick", nick);
            Object obj  = (Object) q.uniqueResult();
            UserConfig uc = (UserConfig)obj;
            if(uc != null)
            Hibernate.initialize(uc.getUserRoles());
			if (uc == null) {
			//	log.debug("get successful, no instance found");
			} else {
			//	log.debug("get successful, instance found");
			}
			return uc;
		} catch (RuntimeException re) {
			//log.error("get failed", re);
			throw re;
		}
	}
	public List findByExample(UserConfig instance) {
		//log.debug("finding UserConfig instance by example");
		try {
//			List results = sessionFactory.getCurrentSession()
//					.createCriteria("UserConfig").add(Example.create(instance))
//					.list();
			List results = null;
			//log.debug("find by example successful, result size: "
	//				+ results.size());
			return results;
		} catch (RuntimeException re) {
			//log.error("find by example failed", re);
			throw re;
		}
	}


}
