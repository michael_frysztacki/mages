//package dao;
//// default package
//// Generated 2012-09-13 22:47:10 by Hibernate Tools 3.4.0.CR1
//
//
//import java.util.List;
//import javax.naming.InitialContext;
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.hibernate.LockMode;
//import org.hibernate.SessionFactory;
//import org.hibernate.criterion.Example;
//
//import pojo.LodesTypes;
//
///**
// * Home object for domain model class LodesTypes.
// * @see .LodesTypes
// * @author Hibernate Tools
// */
//public class LodesTypesHome {
//
//	private static final Log log = LogFactory.getLog(LodesTypesHome.class);
//
//	private final SessionFactory sessionFactory = getSessionFactory();
//
//	protected SessionFactory getSessionFactory() {
//		try {
//			return (SessionFactory) new InitialContext()
//					.lookup("SessionFactory");
//		} catch (Exception e) {
//			log.error("Could not locate SessionFactory in JNDI", e);
//			throw new IllegalStateException(
//					"Could not locate SessionFactory in JNDI");
//		}
//	}
//
//	public void persist(LodesTypes transientInstance) {
//		log.debug("persisting LodesTypes instance");
//		try {
//			sessionFactory.getCurrentSession().persist(transientInstance);
//			log.debug("persist successful");
//		} catch (RuntimeException re) {
//			log.error("persist failed", re);
//			throw re;
//		}
//	}
//
//	public void attachDirty(LodesTypes instance) {
//		log.debug("attaching dirty LodesTypes instance");
//		try {
//			sessionFactory.getCurrentSession().saveOrUpdate(instance);
//			log.debug("attach successful");
//		} catch (RuntimeException re) {
//			log.error("attach failed", re);
//			throw re;
//		}
//	}
//
//	public void attachClean(LodesTypes instance) {
//		log.debug("attaching clean LodesTypes instance");
//		try {
//			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
//			log.debug("attach successful");
//		} catch (RuntimeException re) {
//			log.error("attach failed", re);
//			throw re;
//		}
//	}
//
//	public void delete(LodesTypes persistentInstance) {
//		log.debug("deleting LodesTypes instance");
//		try {
//			sessionFactory.getCurrentSession().delete(persistentInstance);
//			log.debug("delete successful");
//		} catch (RuntimeException re) {
//			log.error("delete failed", re);
//			throw re;
//		}
//	}
//
//	public LodesTypes merge(LodesTypes detachedInstance) {
//		log.debug("merging LodesTypes instance");
//		try {
//			LodesTypes result = (LodesTypes) sessionFactory.getCurrentSession()
//					.merge(detachedInstance);
//			log.debug("merge successful");
//			return result;
//		} catch (RuntimeException re) {
//			log.error("merge failed", re);
//			throw re;
//		}
//	}
//
//	public LodesTypes findById(java.lang.Integer id) {
//		log.debug("getting LodesTypes instance with id: " + id);
//		try {
//			LodesTypes instance = (LodesTypes) sessionFactory
//					.getCurrentSession().get("LodesTypes", id);
//			if (instance == null) {
//				log.debug("get successful, no instance found");
//			} else {
//				log.debug("get successful, instance found");
//			}
//			return instance;
//		} catch (RuntimeException re) {
//			log.error("get failed", re);
//			throw re;
//		}
//	}
//
//	public List findByExample(LodesTypes instance) {
//		log.debug("finding LodesTypes instance by example");
//		try {
//			List results = sessionFactory.getCurrentSession()
//					.createCriteria("LodesTypes").add(Example.create(instance))
//					.list();
//			log.debug("find by example successful, result size: "
//					+ results.size());
//			return results;
//		} catch (RuntimeException re) {
//			log.error("find by example failed", re);
//			throw re;
//		}
//	}
//}
