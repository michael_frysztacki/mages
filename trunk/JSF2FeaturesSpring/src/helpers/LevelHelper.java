package helpers;

import entities.UsersCityConfig;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.IGameModel;
import game_model_interfaces.IResearchModel;

public class LevelHelper {
	/*
	 * metoda zwraca lvl budynku lub badania.
	 * Je�li gm jest Unitem, zwr�ci null.
	 */
	public static Integer getBuildingOrResearchLvl(UsersCityConfig ucc, IGameModel gm)
	{
		if(gm.canCast(IBuildingModel.class))
			return ucc.getLevel(gm.getId());
		if(gm.canCast(IResearchModel.class))
			return ucc.getUserConfig().getLevel(gm.getId());
		return null;
	}
}
