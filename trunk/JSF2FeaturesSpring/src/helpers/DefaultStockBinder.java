package helpers;

import entities.stocks.CityStock;
import framework.IStock;
import game_model_interfaces.PriceStock;
import helpers.CollectionUtil.Binder;

public class DefaultStockBinder implements Binder<IStock>
{
	@Override
	public Object getJoinProperty(IStock obj) {
		return obj.getStockId();
	}
}
