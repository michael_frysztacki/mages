package helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//import org.apache.commons.collections4.comparators.NullComparator;

import org.apache.commons.collections4.comparators.NullComparator;


public class CollectionUtil {

	
	@SuppressWarnings("unchecked")
	public static <T> List<T> findMaxMinObjects(Collection<T> coll, Comparator<T> objectComparator, boolean max)
	{
		List<T> ret = new ArrayList<T>();
		if(coll.size() > 0){
			T foundObject;
			if(max)foundObject = (T) Collections.max(coll, new NullComparator(objectComparator, false));
			else foundObject = (T) Collections.min(coll, new NullComparator(objectComparator, true));
			if(foundObject != null){
				for(T obj : coll){
					if(obj != null && objectComparator.compare(foundObject, obj) == 0){
						ret.add(obj);
					}
				}
			}
		}
		return ret;
	}
	
	public interface Binder<T>
	{
		Object getJoinProperty(T obj);
	}

	/*
	 * je�li kolekcja "E" invokers inn� ilo�� obiekt�w ni� arguments, join wyst�puje do tablicy invokers, tzn. je�li w tablicy arguments
	 * jest obiekt, kt�rego nie ma w invokers, nie zostanie on dodany do setu zwracanego. Je�li w argument nie ma obiektu, a jest taki w 
	 * invokers, invokers b�dzie mia� Pare <obiekt,null>.
	 */
	public static <N,E extends N,V extends N> Set<Pair<E,V>> joinCollections(Collection<E> invokers, Collection<V> arguments, Binder<N> binder)
	{
		Set<Pair<E,V>> ret = new HashSet<Pair<E,V>>();
		Map<Object,V> helper = new HashMap<Object,V>();
		for(V arg : arguments) {
			helper.put(binder.getJoinProperty(arg) , arg);
		}
		for(E arg2 : invokers)
		{
			V local = helper.get( binder.getJoinProperty(arg2) );
			Pair<E,V> pair = new Pair<E, V>(arg2,local);
			ret.add(pair);
		}
		return ret;
	}
	
	public static <E, T extends E> List<T> filterWhichMatchSubclass(Collection<E> coll,  Class<T> clazz) {
		List<T> ret = new  ArrayList<T>();
		for(E instance : coll ){
			if(clazz.isAssignableFrom(instance.getClass()))ret.add((T) instance);
		}
		return ret;
	}
}
