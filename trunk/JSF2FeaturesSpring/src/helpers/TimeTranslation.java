package helpers;

public class TimeTranslation {

	public static double msToHours(long msTime) {
		double temp = (double)msTime;
		double hours = (double)(temp / 1000.0d);
		hours /= 60.0d;
		hours /= 60.0d;
		return hours;
	}
	
	public static long msToNs(double ms) {
		return (long)(ms*1000.0d*1000.0d);
	}
	
	public static long msToNs(long ms) {
		return ms*1000l*1000l;
	}
	
	public static double nsToHours(long nsTime) {
		double temp = nsTime;
		double hours = (temp / 1000.0d);
		hours /= 1000.0d;
		hours /= 1000.0d;
		hours /= 60.0d;
		hours /= 60.0d;
		return hours;
	}
	
	public static long hoursToMs(double hTime) {
		hTime *= 1000;
		hTime *= 60;
		hTime *= 60;
		return (long)hTime;
	}
	
	public static long hoursToNs(double hTime) {
		hTime *= 1000;
		hTime *= 1000;
		hTime *= 1000;
		hTime *= 60;
		hTime *= 60;
		return (long)hTime;
	}
	
	public static long secToNs(int sec) {
		return (long)sec*1000l*1000l*1000l;
	}
}
