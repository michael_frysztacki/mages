package helpers;

public class Pair<V,K> {
	public Pair(V first,K second){
		this.first=first;
		this.second=second;
	}
	public V first;
	public K second;
}
