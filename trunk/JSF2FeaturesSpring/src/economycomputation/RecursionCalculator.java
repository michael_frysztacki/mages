package economycomputation;

import entities.UsersCityConfig;
import entities.stocks.CityFood;
import game_model_impl.StockType;

/*
 * intefejs wyliczaj�ca czas do nast�pnej rekurencji dla r�nych zdarzen np. koniec jedzenia, osi�gni�cie paska przez ludzi, spadek wszystkich wolnych ludzi.
 */
public interface RecursionCalculator {

	public Double getFoodRaiseTime(CityFood cs);
	/*
	 * GetFoodFallTime method is invoked only on food which is in non-lack state. It means that If we already don't have
	 * particular food, this method should be never invoked with thid food as parameter.
	 */
	public Double getFoodFallTime(CityFood cs);
	/*
	 * zwraca czas po jakim zmieni si� jedzenie
	 */
	public Double getFoodChangeTime(UsersCityConfig ucc, StockType rt,double A,double B, double C,boolean isFalling);
	/*
	 * zwraca czas po jakim odejd� wszyscy wolni ludzie ( zostan� tylko Ci, kt�rzy pracuj� w fabrykach).
	 */
	public Double freePeopleFall(UsersCityConfig ucc);
	/*
	 * czas po kt�rym ludzie osi�gn� pasek/domki. Ilosc ludzi mo�e by� wieksza lub mniejsza od paska.
	 */
	public Double timeWhenPeopleArriveLimit(UsersCityConfig ucc);

}
