package economycomputation;

import entities.UsersCityConfig;
import game_model_impl.StockType;

/*
 * Klasa wyliczaj�ca ilo�ci r�nych rzeczy po czasie, np. ilosc ludzi, ilosc jedzenia.
 */
public abstract class EcoCalculator {

	public abstract double getPeopleCount(long nsTime);
	public abstract double getFood(String foodId, long nsTime);
	public abstract double getResourceCount(long timeNs, String resourceId);
	public abstract double getTaxCount(double hTime, String taxId);
}
