package economycomputation.recursion_events;

import entities.Trip;

public class IncommingTripRecursionEvent extends RecursionEvent {

	public IncommingTripRecursionEvent(Trip trip)
	{
		this.nsTime = trip.timeLeftToNextInteraction(trip.getEndCity().getNsTime_());
		this.trip = trip;
	}
	private Trip trip;
	private Long nsTime;
	@Override
	public Long recursionTimeNs() {
		// TODO Auto-generated method stub
		return nsTime;
	}

	@Override
	public boolean process() {
		// TODO Auto-generated method stub
		return false;
	}

}
