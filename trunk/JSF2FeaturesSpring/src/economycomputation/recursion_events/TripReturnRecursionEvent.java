package economycomputation.recursion_events;

import java.util.List;

import entities.Report;
import entities.Trip;


public class TripReturnRecursionEvent extends RecursionEvent {

	public TripReturnRecursionEvent(Trip trip)
	{
		this.timeNs = trip.getReturnTime_() - trip.getStartCity().getNsTime_();
		this.trip = trip;
	}
	private Trip trip;
	private Long timeNs;
	@Override
	public Long recursionTimeNs() {
		// TODO Auto-generated method stub
		return timeNs;
	}

	@Override
	public boolean process() {
		// TODO Auto-generated method stub
		long uccNsTime = trip.getStartCity().getNsTime_();
		try {
			trip.createTripAfter(uccNsTime , false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(trip.canRemove(false, uccNsTime))
		{
			List<Report> reports = trip.getTripReports();
			trip.getStartCity().getUserConfig().getMessages().addAll(reports);
			trip.getStartCity().getIncomeTrips().remove(trip);
			return true;
		}
		return false;
	}

}
