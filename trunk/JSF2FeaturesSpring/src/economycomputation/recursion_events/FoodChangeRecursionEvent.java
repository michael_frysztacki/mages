package economycomputation.recursion_events;

import helpers.TimeTranslation;
import economycomputation.RecursionCalculator;
import entities.Storage;
import entities.UsersCityConfig;
import entities.stocks.CityFood;
import entities.stocks.StockChanger;


public class FoodChangeRecursionEvent extends RecursionEvent {
	
	/*
	 * nie mo�emy stworzy� pustego FoodChangeRecursionEvent...
	 */
	@SuppressWarnings("unused")
	private FoodChangeRecursionEvent(){};
	/*
	 * konstruktor potrzebny tylko do por�wnywania obiekt�w
	 */
	public FoodChangeRecursionEvent(CityFood ccf)
	{
		this.ccf = ccf;
	}
	public FoodChangeRecursionEvent(RecursionCalculator recCalc, UsersCityConfig ucc, CityFood ccf, double A, double B, double C)
	{
		if(ccf.getStorage() == Storage.nastyk)
		{
			this.nsChangeTime = null;
		}
		else
		{
			this.ccf = ccf;
			Double ret = recCalc.getFoodChangeTime(this.ucc, ccf.getStockType(), A, B, C, ccf.getStorage() == Storage.tak ? true : false);
			if(ret != null)this.nsChangeTime = TimeTranslation.hoursToNs(ret);
		}
	}
	private CityFood ccf = null;
	private UsersCityConfig ucc;
	private Long nsChangeTime = null;
	@Override
	public Long recursionTimeNs() {
		return nsChangeTime;
	}

	@Override
	public boolean process() {
		if(ccf == null || nsChangeTime == null)return false;
		else
		{
			StockChanger freeHand = new StockChanger();
			/*
			 * u�ywam tutaj freeHand do ustawiania �ywno�ci na 0.0 w czasie gdy si� sko�czy jedzenie lub zaczyna si� podno�i�.
			 */
			freeHand.freeHand(ccf, 0.0d);
			return true;
		}

	}
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		if(!super.equals(o))return false;
		CityFood ccf1 = (CityFood)o;
		return ccf1.getStockType() == this.ccf.getStockType();
	}

}
