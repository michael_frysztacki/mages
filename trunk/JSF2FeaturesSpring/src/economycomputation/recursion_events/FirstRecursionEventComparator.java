package economycomputation.recursion_events;

import java.util.Comparator;


public class FirstRecursionEventComparator implements Comparator<RecursionEvent>{

	@Override
	public int compare(RecursionEvent o1, RecursionEvent o2) {
		// TODO Auto-generated method stub
		return (int) (o1.recursionTimeNs() - o2.recursionTimeNs());
	}

}
