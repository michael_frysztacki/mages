package economycomputation.recursion_events;

public abstract class RecursionEvent{

	public enum RecursionContext
	{
		normal,
		wobble
	}
	public abstract Long recursionTimeNs();
	public abstract void process();
	/*
	 * dwa RecursionEvents s� sobie r�wne, je�li liczenie dotyczy tego samego np.
	 * w przypadku FoodRecursionEvent dwa Eventy s� sobie r�wne je�li przedstawiaj� liczenie tego samego surowca np ry�u.
	 */
	@Override
	public boolean equals(Object o)
	{
		if(this.getClass().isAssignableFrom(o.getClass()))return true;
		else return false;
	}
}
