package economycomputation.recursion_events;

import helpers.TimeTranslation;
import economycomputation.RecursionCalculator;
import entities.UsersCityConfig;
import entities.stocks.StockChanger;

public class FreePeopleFallRecursionEvent extends RecursionEvent {

	public FreePeopleFallRecursionEvent(RecursionCalculator recCalc, UsersCityConfig ucc)
	{
		this.ucc = ucc;
		Double recH = recCalc.freePeopleFall(ucc);
		if(recH != null)
		{
			this.recursionTimeNs = TimeTranslation.hoursToNs(recH);
		}
	}
	private Long recursionTimeNs = null;
	private UsersCityConfig ucc;
	@Override
	public Long recursionTimeNs() {
		// TODO Auto-generated method stub
		return recursionTimeNs;
	}

	@Override
	public boolean process() {
		// TODO Auto-generated method stub
		if(this.recursionTimeNs != null)
		{
			StockChanger freeHand = new StockChanger();
			freeHand.freeHand(ucc.getCityStocks().getPeopleStock(),(double)ucc.getCityStocks().getPeopleStock().getHiredPeople());
			return true;
		}
		return false;
	}
}
