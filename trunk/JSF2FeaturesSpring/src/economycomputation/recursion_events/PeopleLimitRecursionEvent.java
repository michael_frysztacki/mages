package economycomputation.recursion_events;

import helpers.TimeTranslation;
import economycomputation.RecursionCalculator;
import entities.UsersCityConfig;
import entities.stocks.StockChanger;

public class PeopleLimitRecursionEvent extends RecursionEvent{

	public PeopleLimitRecursionEvent(RecursionCalculator recCalc, UsersCityConfig ucc)
	{
		this.ucc = ucc;
		Double time = recCalc.timeWhenPeopleArriveLimit(ucc);
		this.recTime = TimeTranslation.hoursToNs(time);
	}
	private UsersCityConfig ucc;
	private Long recTime;
	@Override
	public Long recursionTimeNs() {
		return this.recTime;
	}

	@Override
	public boolean process() {
		StockChanger freeHand = new StockChanger();
		freeHand.freeHand(ucc.getCityStocks().getPeopleStock(),(double)ucc.getMaxLudzi());
		return true;
	}

}
