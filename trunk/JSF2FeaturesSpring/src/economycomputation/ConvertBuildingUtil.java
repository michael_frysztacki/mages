package economycomputation;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entities.UsersCityConfig;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import game_model_interfaces.IConvertBuildingModel;
import game_model_interfaces.IExtractBuildingModel;



@Service("convertBuildingUtil")
public class ConvertBuildingUtil implements Serializable{
	
	/*
	 * Metoda zwraca dla budynku konwertuj�cego ilo�� ludzi jak� maksymalnie mo�e ustawi�, je�li w spichlerzu brakuje surowc�w
	 * tzn. dopasowuje ilo�� ludzi do produkcji budynku kt�ry dostarcza surowiec.
	 * 
	 */
	@Autowired private WorldDefinition ch;
	
	public void setFoodComp(CityCompUtil foodComp) {
		System.out.print("cityCompUtil set in ConvertBuildingUtil\n");
		this.foodComp = foodComp;
	}
//	public void setEconomyController(BuildingController economyController) {
//		this.economyController = economyController;
//	}
	@Autowired private CityCompUtil foodComp;
	//@Autowired private BuildingController economyController;
//	public int getConvertBuildingMaxPeople(UsersCityConfig ucc, Boostable convertBuilding) throws Exception
//	{
//		if(ConvertBuilding.class.isAssignableFrom(modelHolder.getObjectsClass(ucc.getUserConfig(), convertBuilding.getId())))
//		{
//			HashMap<ResourceType, Double> productionRate = getProductionRates(ucc);
//			ArrayList<ResourceType> input = convertBuilding.getInputTypes();
//			input.removeAll(ucc.getMyFood().getStoredFood());
//			if(input.size() == 0)return modelHolder.getMaxPeople(ucc, convertBuilding.getId());
//			ResourceType minInput = input.get(0);
//			double min = productionRate.get(minInput);
//			for(int i=1 ; i<input.size() ; i++)
//			{
//				double next = productionRate.get(input.get(i));
//				if(next < min)
//				{
//					min = next;
//					minInput = input.get(i);
//				}
//			}
//			int temp = ucc.getPopulation(convertBuilding.getId());
//			ucc.getPopulationMap().put(convertBuilding.getId(), 1);
//			double sponging = -1 * modelHolder.getOutputPerHour(ucc, minInput, convertBuilding.getId());
//			ucc.getPopulationMap().put(convertBuilding.getId(), temp);
//			int peopleC = economyController.getPeopleCountInBuilding(ucc, convertBuilding.getId());
//			/*
//			 * Je�li aktualnie pobieranie surowca kt�ry dostarcza najmniej surowca dla extractionBuilding
//			 * jest wi�ksze ni� extraction buildong mo�e przetworzy�, zmniejsz odpowiednio ilo�� ludzi
//			 */
//			double rate  = min / sponging;
//			peopleC = (int) (Math.floor(rate));
//			return peopleC;
////				if(EconomyFacade.setPeopleCountInBuilding(ucc, convertBuilding, peopleC))
////				{
////					throw new Exception("metoda setConvertBuilding nie mog�a ustawi� ludzi");
////				}
//
//			/*
//			 * w innym przypadku mo�e pracowa� tyle ludzi ile aktualnie pracuje
//			 */
//			//else return peopleC;
//		}
//		return 0;
//	}
	public HashMap<StockType,Double> getProductionRates(UsersCityConfig ucc)
	{
		ArrayList<StockType> notStoredFood = ucc.getCityStocks().getNotStoredFood();
		notStoredFood.addAll(ucc.getCityStocks().getNastykStoredFood());
		HashMap<StockType,Double> productionRate = new HashMap<StockType,Double>();
		UsersCityConfig civil = ch.getCivilization(ucc.getUserConfig().getCivil());
		for(StockType rt: notStoredFood)
		{
			List<IConvertBuildingModel> spongers = civil.getSpongers(ucc.getUserConfig(), rt);
			
			if(spongers == null)continue;
			//test
			//spongers.remove(0);
			//spongers = ModelFacade.getSpongers(ucc.getUserConfig(), rt);
			//end test
			IExtractBuildingModel producerBuilding = civil.getProducer(ucc.getUserConfig(), rt);
			//spongers.remove(producer);
			double foodPerConvertBuilding = producerBuilding.getOutputPerHour(ucc) / (spongers.size());
			//double foodPerConvertBuilding = modelHolder.getOutputPerHour(ucc, rt, producerBuilding.getId()) / (spongers.size());
			productionRate.put(rt, foodPerConvertBuilding);
		}
		return productionRate;
	}
//	public void setConvertBuildings(UsersCityConfig ucc) throws Exception
//	{
//		//HashMap<ResourceType, Double> productionRates = getProductionRates(ucc);
//		//ArrayList<Boostable> buildings = modelHolder.getBuildings(ucc.getUserConfig(),ConvertBuilding.class,false);
//		Iterator i = ucc.getBuildings().iterator();
//		while(i.hasNext())
//		{
//			CityBuilding b = (CityBuilding)i.next();
//			if(b instanceof ConvertCityBuilding)
//			{
//				ConvertCityBuilding cb = (ConvertCityBuilding)b;
//				int maxp = cb.getMaxPeopleInConvertBuilding();
//				int actPeople = cb.getPeople();
//				if(maxp < actPeople)cb.setPeople(maxp);
//			}
//		}
//	}
	public WorldDefinition getCh() {
		return ch;
	}
	public void setCh(WorldDefinition ch) {
		this.ch = ch;
	}
}
