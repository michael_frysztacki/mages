package economycomputation;

public interface TimeChangeable {

	/*
	 * metoda wyliczaj�ca stan zasobu po nsTime nanosekundach.
	 * Uwaga! bezpowrotnie zmienia stan(ilosc) tego zasobu.
	 * arg1 - argument jakim kalkulatorem ma liczyc surowce(standardowy czy hu�tawkowy(wobble))
	 * arg2 - po jakim czasie ma by� policzony surowiec
	 */
	public void calculateAfter(EcoCalculator ecoCalc, long nsTime);
}
