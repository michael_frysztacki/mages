package economycomputation;

import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import game_model_interfaces.IConvertBuildingModel;
import game_model_interfaces.IExtractBuildingModel;
import helpers.CollectionUtil;
import helpers.TimeTranslation;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import java.util.GregorianCalendar;
import java.util.HashMap;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import comparators.BuildingBuildFinishComparator;
import comparators.ObjectBuildQueueComparator;
import comparators.TripInteractionComparator;
import comparators.TripReturnComparator;

import economycomputation.recursion_events.ExtractBuildingBuildRecursionEvent;
import economycomputation.recursion_events.FirstRecursionEventComparator;
import economycomputation.recursion_events.FoodChangeRecursionEvent;
import economycomputation.recursion_events.FreePeopleFallRecursionEvent;
import economycomputation.recursion_events.IncommingTripRecursionEvent;
import economycomputation.recursion_events.ObjectBuildQueueRecursionEvent;
import economycomputation.recursion_events.PeopleLimitRecursionEvent;
import economycomputation.recursion_events.RecursionEvent;
import economycomputation.recursion_events.TripReturnRecursionEvent;
import entities.Storage;
import entities.Trip;
import entities.UsersCityConfig;
import entities.buildings.BuildingBuild;
import entities.buildings.CityBuilding;
import entities.buildings.ObjectBuildQueue;
import entities.buildings.QueueBuild;
import entities.stocks.CityFood;
import entities.stocks.CityFoodInitializer;
import entities.stocks.CityResource;
import entities.stocks.CityStock;


@Component
public class CityCompUtil implements Serializable{
	
	@Autowired private WorldDefinition ch;
	@Autowired private ProductionUtil pu;
	
	@Autowired private ConvertBuildingUtil convertBuildingUtil;
	
	
//	private ArrayList<ResearchBuild> getExtractResearchBuilds(UsersCityConfig ucc) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
//	{
//		ArrayList<ResearchBuild> extractResearches = new ArrayList<ResearchBuild>();
//		Civilization civil = ch.getCivilization(ucc.getUserConfig().getCivil());
//		Iterator i = ucc.getUserConfig().getResearchBuilds().iterator();
//		while(i.hasNext())
//		{
//			ResearchBuild extractResearch = (ResearchBuild)i.next();
//			Boostable bExtractResearch = ch.getCivilization(ucc.getUserConfig().getCivil()).getById(extractResearch.getObjectId() );
//			//Boostable bExtractBuilding = modelHolder.getById(extractBuilding.getBuildingId())
//			Class buildingClass = bExtractResearch.getObjectsClass();
//			
//			if(bExtractResearch.isExtractionImprovement() != null)
//			{
//				if(bExtractResearch.isExtractionImprovement().equals(true))
//				{
//					extractResearches.add(extractResearch);
//				}
//			}
//		}
//		return extractResearches;
//	}
	
	private ArrayList<BuildingBuild> getExtractBuildingBuilds(UsersCityConfig ucc)
	{
		
		ArrayList<BuildingBuild> extractBuildings = new ArrayList<BuildingBuild>();
		UsersCityConfig civil = ch.getCivilization(ucc.getUserConfig().getCivil());
		Set test = ucc.getBuildingBuilds();
		Iterator i = ucc.getBuildingBuilds().iterator();
		while(i.hasNext())
		{
			BuildingBuild extractBuilding = (BuildingBuild)i.next();
			try{
			IExtractBuildingModel bExtractBuilding = ch.getCivilization(ucc.getUserConfig().getCivil()).getByIdInInternalMap(extractBuilding.getObjectId(), IExtractBuildingModel.class );
			}
			catch(ClassCastException e)
			{
				continue;
			}
			extractBuildings.add(extractBuilding);
			
		}
		return extractBuildings;
	}
//	private List<Trip> getFirstIndcommingTrips(UsersCityConfig ucc) throws Exception
//	{
//		List<Trip> firstTrips = new ArrayList<Trip>();
//		Set<Trip> income = ucc.getIncomeTrips();
//		//Collections.sort(income, new NearestInteractionTripComparator(ucc.getDate()));
//		Long minTime = null;
//		for(Trip trip : income)
//		{
//			Long tempTime = trip.timeLeftToNextInteraction(ucc.getNsTime_());
//			if(tempTime == null)continue;
//			if(minTime == null)minTime = tempTime;
//			if(minTime > tempTime)
//			{
//				minTime = tempTime;
//			}
//		}
//		for(Trip trip : income)
//		{
//			Long tempTime = trip.timeLeftToNextInteraction(ucc.getNsTime_());
//			if(tempTime != null)
//			{
//				if(tempTime.equals(minTime))
//				{
//					//Trip afterTrip = trip.createTripAfter(now,false);
//					firstTrips.add(trip);
//				}
//			}
//		}
//		return firstTrips;
//	}
//	/*
//	 * metoda zwraca trip, ktory powroci z misji najwczensiej.
//	 * Zwraca liste , bo moze sie trafic taki przypdaek ze kilka tripow wraca rownoczensie , tym samym czasie.
//	 */
//	private List<Trip> getFirstRedturnedTrip(UsersCityConfig ucc) throws Exception
//	{
//		List<Trip> firstTrips = new ArrayList<Trip>();
//		Set<Trip> outcome = ucc.getOutcomeTrips();
//		//Collections.sort(outcome, new ReturnTripComparator());
//		Long _minTime = null;
//		for(Trip trip : outcome)
//		{
//			Long tempTime_ = trip.getReturnTime_();
//			if(tempTime_ == null)continue;
//			if(_minTime == null)_minTime = tempTime_;
//			if(_minTime > tempTime_)
//			{
//				_minTime = tempTime_;
//			}
//		}
//		for(Trip trip : outcome)
//		{
//			Long tempTime_ = trip.getReturnTime_();
//			if(tempTime_ != null)
//			{
//				if(tempTime_.equals( _minTime))
//				{
//					//Trip afterTrip = trip.createTripAfter(now,true);
//					firstTrips.add(trip);
//				}
//			}
//		}
//		return firstTrips;
//	}
	enum ChangeType
	{
		people, // pasek ludzi
		freePeopleFall, // wszyscy niepracujacy ludzie odeszli( glod trwa nadal, jednak ludzie z budynkow nie odchodza)
		food,
		extractBuilding,
		firstExtractImprovementTime,
		firstReturnTime, // dotyczy czasu kiedy nasza wyslana wczesniej wyprawa wraca, trzeba przeliczyc rekurencje
		firstIncomeTime, // dotyczy czasu kiedy ktos do nas przychodzi, np atak
		objectBuildQueue
	}
	public Long shortest(Long... times)
	{
		Long shortest = times[0];
		for(int i = 1; i<times.length ; i++)
		{
			if(shortest == null)shortest = times[i];
			else if(times[i] != null)
			{
				if(times[i] < shortest)shortest = times[i];
			}
		}
		return shortest;
	}
	/*
	 * metoda zwraca kolejki budowy jednostek,
	 * ktore zakoncza budowe jedngo typu jednstki najszybciej.
	 * Przyklad:
	 * Mamy dwie kolejki budowy:
	 * ubq : 10 wlocznikow , 12 rycerzy , 3 balisty
	 * ubq2 : 12 wlocznikow , 2 rycerzy.
	 * W liscie zwrocona zostanie pierwsza kolejka ubq, dlatego ze pierwsza skoczy budowac najblizszy typ jednostki.
	 * Zwracana jest lista dlatego ze moze byc kilka kolejek ktore koncza budowac jeden typ jednostki
	 * w tym samym czasie.
	 */
	private List<ObjectBuildQueue> getFirstFinishedObjectBuildQueues(UsersCityConfig ucc,Map<Integer,Integer> obqId_buildCount)
	{
		List<ObjectBuildQueue> ret = null;
		Long minTime = null; 
		for(CityBuilding cb : ucc.getBuildings())
		{
			ObjectBuildQueue obq = cb.getObjectBuildQueue();
			Long compTime = obq.timeToNextRecur(null);
			if(minTime == null || (compTime != null && minTime > compTime))
			{
				minTime = compTime;
			}
		}
		if(minTime != null)
		{
			ret = new ArrayList<ObjectBuildQueue>();
			for(CityBuilding cb : ucc.getBuildings())
			{
				ObjectBuildQueue obq = cb.getObjectBuildQueue();
				int[] buildCount = new int[1];
				Long compTime = obq.timeToNextRecur(buildCount);
				if(compTime != null && compTime.equals(minTime))
				{
					ret.add(obq);
					obqId_buildCount.put(obq.getCityBuilding().getBuildingId() , buildCount[0]);
				}
			}
		}
		return ret;
		
		
	}
	
	
	/*
	 * g��wna metoda rekurencyjna licz�ca stan miasta po nsTime nanosekundach.
	 */
	public Map<String,Object> getFoodAT(UsersCityConfig uccFinal, long nsTime, Integer recurs)
	{
		RecursionCalculator recCalc = new DefaultRecursionCdddalculator();
		List<RecursionEvent> recEvents = new ArrayList<RecursionEvent>();
		if(nsTime < 0l)
		{
			uccFinal = null;
		}
		boolean isDebug = java.lang.management.ManagementFactory.getRuntimeMXBean().
			    getInputArguments().toString().indexOf("-agentlib:jdwp") > 0;
		
		//UsersCityConfig ucc = new UsersCityConfig(uccFinal);
		UsersCityConfig ucc = uccFinal;
		EcoCalculator ecoCalc = new DefaultEcoCalculator(ucc);
		UsersCityConfig myCivil = ch.getCivilization(ucc.getUserConfig().getCivil());
		if(isDebug)
	    {
			System.out.print("-------------------------------------------------------------------------------------------------\n");
			GregorianCalendar time = new GregorianCalendar();
			time.setTimeInMillis(ucc.getNsTime_()/1000l/1000l);
			System.out.print("chcemy wyliczyc miasto po: " + nsTime/1000l/1000l/1000l + "sekundach(" + nsTime +" nanosek)\n");
			System.out.print("czas miasta: " + time.getTime().getHours() + ":"+time.getTime().getMinutes()+":"+time.getTime().getSeconds() + "\n");
	    	System.out.print("ilosc ludzi: " + ucc.getCityStocks().getPeopleStock().getVisibleAmount() + "\n");
	    	System.out.print("birthRate: " + ucc.getBirthRatePerHour() + "\n");
	    	System.out.print("pasek ludzi: " + ucc.getMaxLudzi() + "\n");
	    	System.out.print("ilosc wolnych ludzi: " + ucc.getCityStocks().getPeopleStock().getFreePeople()+"\n");
	    	System.out.print("ilosc zatrudnionych ludzi: " + ucc.getCityStocks().getPeopleStock().getHiredPeople()+"\n");
	    	System.out.print("ilosc szkolonych ludzi: " + ucc.getTrainedPeople()+"\n");
	    	System.out.print("jedzenie:" + "\n");
	    	for(CityStock cs : ucc.getCityStocks().getFood())
	    	{
	    		IExtractBuildingModel b = myCivil.getProducer(ucc.getUserConfig(), cs.getStockType() );
	    		System.out.print(cs.getStockType() + ":" + cs.getVisibleAmount() + ",produkcja: " + b.getOutputPerHour(ucc) + "\n");
	    	}
	    	System.out.print("\nliczymy miasto: " + ucc.getId() + ",rekursja:" + recurs + "\n");
	    }
		long now_ = ucc.getNsTime_() + nsTime;
		recurs++;
		
		//GregorianCalendar now = new GregorianCalendar();
		//now.setTimeInMillis(ucc.getDate().getTimeInMillis() + msTime);
		CityFoodInitializer evi = new CityFoodInitializer();
		ucc.initCityEcoVars(evi);
		
//		Long freePeopleFallNs = null;
//		Double freePeopleFallH = freePeopleFall(ucc);
//		if(freePeopleFallH != null)freePeopleFallNs = TimeTranslation.hoursToNs(freePeopleFallH);
		recEvents.add(new FreePeopleFallRecursionEvent(recCalc, ucc));
		/*
		 *  obliczamy kiedy ludzie osi�gn� pasek
		 */
//		Long chPeopleNs = null;
//		Double chPeopleH = timeWhenPeopleArriveLimit(ucc);
//		if(chPeopleH != null)chPeopleNs = TimeTranslation.hoursToNs(chPeopleH);
		recEvents.add(new PeopleLimitRecursionEvent(recCalc, ucc));
		
		/*
		 * w tablicy 'czasy' b�dziemy trzyma� wszystkie nadchodz�ce zmiany dotycz�ce jedzenie, a nast�pnie 
		 * b�dziemy sprawdza� kt�ry z tych czas�w jest najkr�tszy tzn. kt�re jedzenie najszybciej ulegnie zmianie.
		 * Oboj�tne, czy jedzenie staje si� nadmiarowe czy niedomiarowe, trzymane jest to w tej samej tablicy
		 */
		ArrayList<BuildingBuild> extractBuildings = getExtractBuildingBuilds(ucc);
		//Long firstFinishTimeNs = null;
		//List<BuildingBuild> firstEBuildings  = null;
		if(extractBuildings.size() > 0)
		{
			List<BuildingBuild> firstEBuildings = CollectionUtil.findMaxMinObjects(extractBuildings, new BuildingBuildFinishComparator(ucc), false);
			//firstEBuildings = getFirstFinishedBuildingBuilds(extractBuildings, ucc);
			for(BuildingBuild bb : firstEBuildings)
			{
				recEvents.addEdgeAndReversedEdge(new ExtractBuildingBuildRecursionEvent(bb, ucc));
			}
			//long nsFinishTime = firstEBuildings.get(0).getFinishTime_() - ucc.getNsTime_();
			//firstFinishTimeNs = nsFinishTime;
		}
		
//		ArrayList<ResearchBuild> extractResearches = getExtractResearchBuilds(ucc);
//		Double firstExtractImprovementTimeMs = null;
//		List<ResearchBuild> firstEImprovements = null;
//		if(extractResearches.size() > 0)
//		{
//			firstEImprovements = getFirstFinishedImprovementsBuilds(extractResearches, ucc);
//			long msFinishTime = firstEImprovements.get(0).getFinishTime_().getTimeInMillis() - ucc.getDate().getTimeInMillis();
//			
//			firstExtractImprovementTimeMs = (double)msFinishTime;
//		}
		
		/*
		 * obliczamy, keidy konczy sie budowac najblizsza kolejka jednostek.
		 * Potrzebne nam to do rekurencji, dlatego �e zjadanie spozywanie jedzenie przez ka�da jednostke
		 * w kolejce budowy moze byc r�ne. a tym samym zmienia sie spozywanie jedzenie przez kolejke budowy. 
		 */
		//List<ObjectBuildQueue> firstFinishedObjectBuilds = null;
		//List<ObjectBuildQueue> unitBuilds = ucc.getUnitBuildQueues();
		//Map<Integer,Integer> obqId_buildCount = new HashMap<Integer,Integer>();
		//List<ObjectBuildQueue> firstFinishedObjectBuilds = getFirstFinishedObjectBuildQueues(ucc, obqId_buildCount);
		List<CityBuilding> firstFinishedObjectBuilds = CollectionUtil.findMaxMinObjects(ucc.getBuildings(), new ObjectBuildQueueComparator(), false);
		for(CityBuilding cb : firstFinishedObjectBuilds)
		{
			recEvents.addEdgeAndReversedEdge(new ObjectBuildQueueRecursionEvent(cb));
		}
		//Long firstFinishedObqNs = null;
		//if(firstFinishedObjectBuilds != null)
		//	firstFinishedObqNs = firstFinishedObjectBuilds.get(0).timeToNextRecur( null);
		
		//Set<Trip> testT = ucc.getOutcomeTrips();
		List<Trip> firstReturnedTrips = CollectionUtil.findMaxMinObjects(ucc.getOutcomeTrips(), new TripReturnComparator(), false);
		//List<Trip> firstReturnedTrips = getFirstReturnedTrip(ucc);
		//Long firstReturnTimeNs = null;
		for(Trip trip : firstReturnedTrips)
		{
			recEvents.add(new TripReturnRecursionEvent(trip));
		}
//		if(firstReturnedTrips.size() > 0)
//		{
//			Long nsFirstReturnTime = ((Trip)firstReturnedTrips.iterator().next()).getReturnTime_();
//			//long temp_ms = msFirstReturnTime - ucc.getDate().getTimeInMillis();
//			firstReturnTimeNs = nsFirstReturnTime - ucc.getNsTime_();
//			/*
//			 * je�eli czas przybycia wojska 
//			 */
//			//if(firstReturnTime.equals(hTime))firstReturnTime=null;
//		}
		
		Long firstIncomingTripTimeNs = null;
		List<Trip> firstIncommingTrips = CollectionUtil.findMaxMinObjects(ucc.getIncomeTrips(), new TripInteractionComparator(ucc), false);
		//List<Trip> firstIncommingTrips = getFirstIncommingTrips(ucc);
		for(Trip inTrip : firstIncommingTrips)
		{
			recEvents.add(new IncommingTripRecursionEvent(inTrip));
		}
//		if(firstIncommingTrips.size() > 0)
//		{
//			firstIncomingTripTimeNs = ((Trip)firstIncommingTrips.iterator().next()).timeLeftToNextInteraction(ucc.getNsTime_());
//			//firstIncomingTripTime = TimeTranslation.msToHours(msFirstArriveTime);
//			/*
//			 * je�li czas przybycia obcej armi do naszego miasta jest rowny
//			 * czasowi dla ktorego chcemy wyliczyc stan miasta (hTime),
//			 * ustaw firstIncommingTripTime na null. Robie tak dlatego, ze jak ktos inny chce wyliczy�
//			 * stan tego miasta tuz przed walka, to poda czas hTime, wlasnie rowny czasowi zetkniecia
//			 * sie armi. Ten 'ktos inny' nie chce zeby do rekurencji miasta, ktorego wylicza
//			 * dosta� sie wlasnie ten incomming trip - ma sie policzyc caly stan miasta, ale bez tego
//			 * incomming tripu.
//			 * 
//			 */
//			if(firstIncomingTripTimeNs!=null)
//			{
//				if(firstIncomingTripTimeNs.equals(nsTime))firstIncomingTripTimeNs=null;
//			}
//		}
		HashMap<StockType,Long> czasy = new HashMap<StockType,Long>();
		
		Map<String,Double> abcFactors = evi.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		
		/*
		 * sprawdzamy czasy zmiany jedze�
		 */
		for(CityFood ccf: ucc.getCityStocks().getFood() )
		{
			recEvents.add(new FoodChangeRecursionEvent(recCalc, ucc, ccf, A, B, C));
		}
		
		/*
		 * bierzemy najkr�tszy czas z tablicy czasy
		 */
		Long firstChange = getTimeWhenFirstFoodChange(czasy);
		ArrayList<StockType> changeFood = null;
		if(czasy != null && firstChange != null)
		changeFood = getChangedFoodAtTime(czasy,firstChange);
		
		@SuppressWarnings("unchecked")
		List<RecursionEvent> firstEvents = CollectionUtil.findMaxMinObjects(recEvents, new FirstRecursionEventComparator(), false);
		//CollectionUtil.findMaxMinObjects(coll, objectComparator, max)
//				
//				shortest( 
//									firstChange,
//									chPeopleNs, 
//									firstFinishTimeNs,
//									freePeopleFallNs, 
//									firstReturnTimeNs, 
//									firstIncomingTripTimeNs,
//									firstFinishedObqNs
//								  );
//		ArrayList<ChangeType> changes = new ArrayList<ChangeType>();
//		if(firstChange != null && firstChange.equals(nsShortest))changes.add(ChangeType.food);
//		if(chPeopleNs != null && chPeopleNs.equals(nsShortest))changes.add(ChangeType.people);
//		if(firstFinishTimeNs != null && firstFinishTimeNs.equals(nsShortest))changes.add(ChangeType.extractBuilding);
//		if(freePeopleFallNs != null && freePeopleFallNs.equals(nsShortest))changes.add(ChangeType.freePeopleFall);
//		if(firstReturnTimeNs != null && firstReturnTimeNs.equals(nsShortest))changes.add(ChangeType.firstReturnTime);
//		if(firstIncomingTripTimeNs != null && firstIncomingTripTimeNs.equals(nsShortest))changes.add(ChangeType.firstIncomeTime);
//		if(firstFinishedObqNs != null && firstFinishedObqNs.equals(nsShortest))changes.add(ChangeType.objectBuildQueue);
		//if(firstExtractImprovementTimeMs != null && firstExtractImprovementTimeMs.equals(nsShortest))changes.add(ChangeType.firstExtractImprovementTime);
		
//		if(isDebug)
//		{
//			if(firstChange != null)
//			{
//				System.out.print("zmiana jedzenia:\n");
//				for(ResourceType chf : changeFood)
//				{
//					System.out.print(chf + "\n");
//				}
//				System.out.print("po" + firstChange/1000l/1000l/1000l + " sekundach("+firstChange+"nanosek)\n");
//			}
//			if(chPeopleNs != null)
//			{
//				System.out.print("osiagniecie paska ludzi po " + chPeopleNs/1000l/1000l/1000l + "sekundach(" + chPeopleNs+"nanosek)\n");
//			}
//			if(freePeopleFallNs != null)
//			{
//				System.out.print("odejscie wszystkich wolnych ludzi po " + freePeopleFallNs/1000l/1000l/1000l + "sekundach("+freePeopleFallNs+"nanosek)\n");
//			}
//			System.out.print("najszybsza zmiana: \n");
//			for(ChangeType cht : changes)
//			{
//				System.out.print(cht + "\n");
//			}
//			
//		}
		Long nsShortest = firstEvents.get(0).recursionTimeNs();
		if(nsShortest == null || nsTime < nsShortest ) // je�li czas zmiany najszybszego surowca jest nan, to juz nie bedzie wiecej checkpointow
		{
			double br = ucc.getBirthRatePerHour();
			//UsersCityConfig ret = new UsersCityConfig(ucc);
			UsersCityConfig ret = ucc;
			//double retbr = ret.getBirthRatePerHour();
			Map<String,Object> retv = new HashMap<String,Object>();
			
			/*
			 * shortcut(hTime,
				ucc.getMyFood().getFood(rt).getCount(),
				getFoodProduction(rt, ucc),
				ccb.getEatPerHour(),
				ucc.getMyResources().getResource(ResourceType.peopleCount).getCount(),
				ucc.getTrainedPeople(),
				ucc.getBirthRatePerHour(),
				SN,
				SOP,
				A,
				B,
				C,
				rt
				);
			 */
			UsersCityConfig config = ch.getCivilization(ucc.getUserConfig().getCivil());
			//CivilConfigBoostable ccb = config.getCivilConfig();
			
			Map<StockType,Double> startFoodCount = new HashMap<StockType,Double>();
			Map<StockType,Double> startFoodOph = new HashMap<StockType,Double>();
			Map<StockType,Storage> startFoodStorage = new HashMap<StockType,Storage>();
			retv.put("nextRec", nsShortest == null ?null : TimeTranslation.nsToHours(nsShortest));
			retv.put("hTime", TimeTranslation.nsToHours(nsTime));
			retv.put("startFoodCount",startFoodCount);
			retv.put("startFoodOph",startFoodOph);
			retv.put("startFoodStorage",startFoodStorage);
			retv.put("eph", config.getEatPerHour() );
			retv.put("A", A);
			retv.put("B", B);
			retv.put("C", C);
			retv.put("p", ucc.getCityStocks().getPeopleStock().getVisibleAmount() );
			retv.put("sz", ucc.getTrainedPeople());
			retv.put("br", ucc.getBirthRatePerHour());
			retv.put("SN", ucc.getSN());
			retv.put("SOP", ucc.getSOP());
			
			for(CityFood cf : ret.getCityStocks().getFood())
			{
				startFoodCount.put(cf.getStockType(), cf.getVisibleAmount());
				startFoodOph.put(cf.getStockType(), pu.getFoodProduction(cf.getStockType(), ret));
				startFoodStorage.put(cf.getStockType(), cf.getStorage());
				cf.calculateAfter(ecoCalc, nsTime);
//				Double f = ecoCalc.getFood(cf.getStockType(),ret,TimeTranslation.nsToHours(nsTime),A,B,C);
//				if(f != null)
//				{
//					cf.setCount(f);
//				}
			}
			for(TimeChangeable cr : ret.getCityStocks().getResources() )
			{
				cr.calculateAfter(ecoCalc, nsTime);
			}
			
			/*
			 * upgradujemy budynki, ktore nie trzeba wkladac do rekurencji np. koszary, uniwersytet.
			 */
			Iterator i = ret.getBuildingBuilds().iterator();
			while(i.hasNext())
			{
				BuildingBuild bb = (BuildingBuild)i.next();
				if(bb.getFinishTime_() - ret.getNsTime_() <= nsTime)
				{
					ret.getBuilding(CityBuilding.class, bb.getObjectId() ).setLevel(bb.getDestLevel());
					i.remove();
				}
			}
			
			/*
			 * upgradujemy badania oraz budujemy unity, ktore nie trzeba wkladac do rekurencji np kartografia, nawigacja, badz unity 
			 * ktore sie jeszcze
			 */
			for(CityBuilding cb : ret.getBuildings())
			{
				ObjectBuildQueue obq = cb.getObjectBuildQueue();
				for(int k=0 ; k < obq.getBuildsAmount() ; k++)
				{
					QueueBuild qb = obq.get(k);
					if(!qb.processBuild(now_))break;
				}
			}
			
//			i = ret.getUserConfig().getResearchBuilds().iterator();
//			while(i.hasNext())
//			{
//				ResearchBuild rb = (ResearchBuild)i.next();
//				if(rb.getFinishTime_().getTimeInMillis() - ret.getDate().getTimeInMillis() <= msTime)
//				{
//					ret.getUserConfig().getResearch(rb.getUpgradeableObjectId()).setLevel(rb.getDestLevel());
//					i.remove();
//				}
//			}
			
			TimeChangeable people = ret.getCityStocks().getPeopleStock();
			people.calculateAfter(ecoCalc, nsTime);
			ret.addNsTime(nsTime);
			
			/*
			 * 
			 * TODO
			 * 
			 * ustawianie rapor�w dla miasta wysylajacego atak.
			 * Nie ma sensu wkladania tego do nowej rekurencji, walka w miescie broniacego nie 
			 * ma wplywu na to miasto, wystarczy tylko sprawdzic kiedy powinna sie odbyc walka i 
			 * mozna zasymulowac bitwy i wygenerowac raporty.
			 * 
			 */
			Set<Trip> outcomeTrips = ret.getOutcomeTrips();
			i = outcomeTrips.iterator();
			while(i.hasNext())
			{
				Trip outcome = (Trip)i.next();
				Trip newTrip = outcome.createTripAfter(ret.getNsTime_(), true);
				i.remove();
				outcomeTrips.add(newTrip);
			}
			
			
			return retv;
		}
		
		//UsersCityConfig checkPointConfig = new UsersCityConfig(ucc);
		//UsersCityConfig checkPointConfig = ucc;
		
		List<IConvertBuildingModel> convertBuildings = myCivil.getGameModelList(IConvertBuildingModel.class,false,true);
		/*
		 * najpierw ustawiamy budynki konwertuj�ce, dlatego �e metoda getProductionRate() w metodzie getOutputPerHouer w klasie model.ConvertBuilding,
		 * b�dzie dzia�a� niepoprawnie, je�li najpierw ustawimy wod� lub ry� na 0.0.
		 */
		for(IConvertBuildingModel convert : convertBuildings)
		{
			StockType rt = convert.getResourceType();
			CityFood ccf = (CityFood) ucc.getCityStocks().getStockByType(rt);
			FoodChangeRecursionEvent temp = new FoodChangeRecursionEvent(ccf);
			if(!recEvents.contains(temp))
			{
				ccf.calculateAfter(ecoCalc, nsShortest);
				//ccf.setCount(ecoCalc.getFood(rt,ucc,TimeTranslation.nsToHours(nsShortest),A,B,C));
			}
		}
		//List<IExtractBuildingModel> extBuildings = myCivil.getGameModelList(IExtractBuildingModel.class, false);
		for(CityFood ccf : ucc.getCityStocks().getFood())
		{
			FoodChangeRecursionEvent temp = new FoodChangeRecursionEvent(ccf);
			if(!recEvents.contains(temp))
			{
				ccf.calculateAfter(ecoCalc, nsShortest);
				//ccf.setCount(ecoCalc.getFood(ccf.getTyp(), ucc, TimeTranslation.nsToHours(nsShortest),A,B,C));
			}
		}
		for(CityResource cr : ucc.getCityStocks().getResources() )
		{
			//double count = this.getResourceCount(ucc, TimeTranslation.nsToHours(nsShortest), cr.getTyp());
			//ucc.getMyResources().getResource(cr.getTyp()).setCount(count);
			cr.calculateAfter(ecoCalc, nsShortest);
		
		}
		FreePeopleFallRecursionEvent temp1 = new FreePeopleFallRecursionEvent(recCalc, ucc);
		PeopleLimitRecursionEvent temp2 = new PeopleLimitRecursionEvent(recCalc, ucc);
		if(!recEvents.contains(temp1) && !recEvents.contains(temp2))
		{
			ucc.getCityStocks().getPeopleStock().calculateAfter(ecoCalc, nsShortest);
			//ucc.getMyResources().getResource(StockType.peopleCount).setCount(ecoCalc.getPeopleCount(ucc, TimeTranslation.nsToHours(nsShortest)));
		}
		/*
		 * objectBuildQueue musi byc ustawiony przed 
		 * if(changes.indexOf(ChangeType.food) != -1)
		 */
		for(CityBuilding cb : ucc.getBuildings())
		{
			ObjectBuildQueueRecursionEvent temp = new ObjectBuildQueueRecursionEvent();
			temp.setCityBuilding(cb);
			if(!recEvents.contains(temp))
			{
				Iterator<QueueBuild> i = cb.getObjectBuildQueue().getIterator();
				while(i.hasNext())
				{
					QueueBuild qb = (QueueBuild)i.next();
					if(!qb.processBuild(ucc.getNsTime_() + nsShortest))break;
				}
			}
		}
		ucc.addNsTime(nsShortest);	
		for(RecursionEvent event : recEvents)
		{
			event.process();
		}
//		if(changes.indexOf(ChangeType.food) != -1)
//		{
//			for(ResourceType rt_ : changeFood)
//			{
//				checkPointConfig.getMyFood().getFood(rt_).setCount(0.0d);
//			}
//		}
//		if(changes.indexOf(ChangeType.people) != -1)
//		{
//			checkPointConfig.getMyResources().getResource(ResourceType.peopleCount).setCount(ucc.getMaxLudzi());
//		}
//		if(changes.indexOf(ChangeType.freePeopleFall) != -1)
//		{
//			checkPointConfig.getMyResources().getResource(ResourceType.peopleCount).setCount(ucc.getHiredPeople());
//		}
//		if(changes.indexOf(ChangeType.extractBuilding) != -1)
//		{
//			//List food = Arrays.asList(checkPointConfig.getMyFood().getFood().toArray());
//			checkPointConfig.getBuildingBuilds().removeAll(firstEBuildings);
//			for(BuildingBuild bb : firstEBuildings)
//			{
//				CityBuilding cb = checkPointConfig.getBuilding(bb.getObjectId());
//				cb.setLevel(bb.getDestLevel());
//			}
//		}
//		if(changes.indexOf(ChangeType.firstExtractImprovementTime) != -1)
//		{
//			//List food = Arrays.asList(checkPointConfig.getMyFood().getFood().toArray());
//			checkPointConfig.getUserConfig().getResearchBuilds().removeAll(firstEImprovements);
//			for(ResearchBuild rb : firstEImprovements)
//			{
//				UserResearch ur = checkPointConfig.getUserConfig().getResearch(rb.getUpgradeableObjectId());
//				ur.setLevel(rb.getDestLevel());
//			}
//		}
		
		
//		else
//		{
//			this.addArmyFromUnitBuildQueues(checkPointConfig,shortest,A,null);
//		}
		/*
		 * TODO
		 * 
		 * dodac do petli rekurencyjnej badania.
		 */
		/*
		 * nad tym komentarzem dodawaj nowe zdarzenia (2*)
		 */
//		if(changes.indexOf(ChangeType.firstReturnTime) != -1)
//		{
//			for(Trip returned : firstReturnedTrips)
//			{	
//				Trip currentTrip = checkPointConfig.getOutComeTripById(returned.getId());
//				currentTrip.createTripAfter(checkPointConfig.getNsTime_(), true);
//				currentTrip.syncReturnedTripWithCity();
//				if(currentTrip.canRemove(true, checkPointConfig.getNsTime_()))
//				{
//					List<Report> reports = currentTrip.getTripReports();
//					checkPointConfig.getUserConfig().getMessages().addAll(reports);
//					checkPointConfig.getOutcomeTrips().remove(currentTrip);
//				}
//			}
//		}
//		if(changes.indexOf(ChangeType.firstIncomeTime) != -1)
//		{
//			for(Trip incomming : firstIncommingTrips)
//			{	
//				/*
//				 * korzystamy z currentTrip a nie incomming, dlatego ze incomming nie zawiera
//				 * referencji do checkPointConfig, tylko do ucc. Referencje do checkPointConfig zawiera
//				 * trip w checkPointConfig  i jego szukamy po id.
//				 */
//				//Trip currentTrip = checkPointConfig.getInComeTripById(incomming.getId());
//				incomming.createTripAfter(ucc.getNsTime_(), false);
//				if(incomming.canRemove(false, ucc.getNsTime_()))
//				{
//					List<Report> reports = incomming.getTripReports();
//					ucc.getUserConfig().getMessages().addAll(reports);
//					ucc.getIncomeTrips().remove(incomming);
//				}
//			}
//		}
		/*
		 * (2*)
		 * 					UWAGA
		 *   - nie dokonuj wiecej zdarzen ponizej 
		 * jakiekolwiek nowe zdarzenie dodawaj przed 
		 * if(changes.indexOf(ChangeType.firstIncomeTime) != -1)
		 * 
		 * if(changes.indexOf(ChangeType.firstReturnTime) != -1)
		 */
		
	//System.out.print("r�nica czasu msTime - changeTime: " + (nsTime - nsShortest) + "\n");
		//convertBuildingUtil.setConvertBuildings(checkPointConfig);
		return getFoodAT(ucc,nsTime - nsShortest,recurs);
	}


	private ArrayList<StockType> getChangedFoodAtTime(HashMap<StockType,Long> czasy,double hTime)
	{
		Iterator<Map.Entry<StockType, Long>> it = czasy.entrySet().iterator();
		ArrayList<StockType> decreaseFood = new ArrayList<StockType>();
		//decreaseFood.add(surowiecNaStyk.getKey());
		//double sekunda = 1.0d / 3600.0d;
		/*
		 * szukamy jeszcze innych surowcow ktore byc moze tez ko�cz� sie w czasie +/- 1 sekunda...
		 */
		while(it.hasNext())
		{
			Map.Entry<StockType, Long> next = it.next();
			if(next.getValue() == hTime)
			{
				decreaseFood.add(next.getKey());
			}
		}
		return decreaseFood;
	}
	
	public Long getTimeWhenFirstFoodChange(HashMap<StockType,Long> czasy)
	{
		if(czasy.size() == 0)return null;
		Iterator<Map.Entry<StockType, Long>> it = czasy.entrySet().iterator();
		
		Map.Entry<StockType, Long> surowiecNaStyk = it.next();
		while(it.hasNext())
		{
			Map.Entry<StockType, Long> next = it.next();
			if( next.getValue() < surowiecNaStyk.getValue() )
			{
				surowiecNaStyk = next;
			}
		}
		return surowiecNaStyk.getValue();
	}
	
	
//	private double getOneEatenFood(UsersCityConfig ucc, double SN, double SOP, double hTime) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
//	{
//		return ( getEatenFoodAT(ucc,hTime) - SN*hTime) / SOP;
//	}
	public double getEatenFoodAT(UsersCityConfig ucc, double minTime) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	{
		UsersCityConfig config = ch.getCivilization(ucc.getUserConfig().getCivil());
		//CivilConfigBoostable ccb = config.getCivilConfig();
		double br = (double)config.getCityModel().getBirthRatePerHour(ucc);
		double people = ucc.getCityStocks().getPeopleStock().getVisibleAmount();
		double eatPerH = (double)config.getEatPerHour();
		double x = (double)people*minTime;
		double c = (double)minTime*(double)minTime;
		double y = (double)(br/2.0d) *c;
		double a = (double)eatPerH * ( x + y );
		return a;
	}
	
//	public ArrayList<ResourceType> getHighestInfucientsFoodResources(UsersCityConfig ucc) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
//	{
//		ArrayList<ResourceType> ret = new ArrayList<ResourceType>();
//		if(ucc.getMyFood().getNotStoredFood().size() > 0 )
//		{
//			ResourceType maxProdType = ucc.getMyFood().getNotStoredFood().get(0);
//			double maxProd = pu.getFoodProduction(maxProdType, ucc);
//			for(int i=1 ; i < ucc.getMyFood().getNotStoredFood().size(); i++)
//			{
//				if(maxProd < pu.getFoodProduction(ucc.getMyFood().getNotStoredFood().get(i), ucc))
//				{
//					maxProdType = ucc.getMyFood().getNotStoredFood().get(i);
//					maxProd = pu.getFoodProduction(maxProdType, ucc);
//				}
//				//CheckPoint.getNotStoredFood(ucc).get(i);
//			}
//			for(ResourceType rt : ucc.getMyFood().getNotStoredFood())
//			{
//				if(pu.getFoodProduction(rt, ucc) == maxProd)
//				{
//					ret.add(rt);
//				}
//			}
//		}
//		return ret;
//	}
//	public boolean isStarvation(UsersCityConfig ucc)
//	{
//		if(getStoredFood(ucc).size() == 0)return true;
//		else return false;
//	}
	
//	public Double getChangeTime(UsersCityConfig ucc, ResourceType rt,Integer SOP, Double SN,double A,double B, double C,boolean isFalling) throws InstantiationException, IllegalAccessException
//	{
//		if(!isFalling)
//		{
//			List<ResourceType> maxInfResources = CollectionUtil.findMaxMinObjects(ucc.getMyFood().getNotStoredFood(), new ResourceProductionComparator(ucc, pu), true);
//			//ArrayList<ResourceType> maxInfResources = getHighestInfucientsFoodResources(ucc);
//			if(maxInfResources.size() > 0)
//			{
//				SOP +=  maxInfResources.size();
//				SN -= pu.sumOfFacotoryProductions(maxInfResources, ucc);
//			}
//			else return null;	
//		}
//		Civilization config = ch.getCivilization(ucc.getUserConfig().getCivil());
//		CivilConfigBoostable ccb = config.getCivilConfig();
//		
//		double currentPeople = ucc.getMyResources().getResource(ResourceType.peopleCount).getCount();
//		double a = (-1.0d)*0.5d*(B + (double)ccb.getEatPerHour()*(ucc.getBirthRatePerHour() + A));
//		double b = SOP * pu.getFoodProduction(rt, ucc) - (double)ccb.getEatPerHour()*(currentPeople + ucc.getTrainedPeople()) - C + SN;
//		double c = (ucc.getMyFood().getFood(rt).getCount() * SOP);
//		/*
//		 * jesli wspolczynnik przy a=0 to znaczy ze bedzie to rownanie liniowe
//		 */
//		if(a == 0)
//		{
//			//double ret = (SOP - SOP * ucc.myFood.getFood(rt).count) / (SOP * ModelFacade.getFoodProduction(rt, ucc) - ccb.getEatPerHour() * ucc.myResources.getResource(ResourceType.freePeople).count + SN);
//			Double ret = ((-1.0d)*c / b);
//			if(ret <= 0 || ret.isNaN())return null;
//			return ret;
//		}
//	
//		
//		
//		
//		
////		double a = (double) ((-1.0d)*0.5d * (double)ucc.getBirthRatePerHour() * (double)ccb.getEatPerHour());
////		double b = SOP * getFoodProduction(rt, ucc) +((-1.0d)*ccb.getEatPerHour() * ucc.getMyResources().getResource(ResourceType.peopleCount).getCount())  + SN;
////		double c = (ucc.getMyFood().getFood(rt).getCount() * SOP);// - SOP;
//		double delta = b*b - 4.0d*a*c;
//		if(delta < 0.0d )
//		{
//			return null; // ten food na pewno nigdy sie nie zmieni, sprawdzaj nast�pne jedzenie
//		}
//		else
//		{
//			Double t1 = (double) ( ((-1.0d) * b - Math.sqrt(delta) ) / (2.0d *a) );
//			Double t2 = (double) ( ((-1.0d) * b + Math.sqrt(delta) ) / (2.0d *a) );
//			if(this.getPeopleCount(ucc, !isFalling ? t1/2.0d : t1) < 0.0d)t1=null;
//			if(this.getPeopleCount(ucc, !isFalling ? t2/2.0d : t2) < 0.0d)t2=null;
//			Double ret = getNajmniejszyDodatniCzas(t1, t2);
//			if(!isFalling && ret != null)ret /= 2.0d;
//			return ret;
//		}
//	}
//	public Double timeWhenPeopleArriveLimit(UsersCityConfig ucc)
//	{
//		int maxPeople = ucc.getMaxLudzi();
//		double br = ucc.getBirthRatePerHour();
//		if(br != 0 && !ucc.isStarvation())
//		{
//			double time = (maxPeople - ucc.getMyResources().getResource(ResourceType.peopleCount).getCount()) / 
//			br;
//			if(time < 0 )return null;
//			return time;
//		}
//		else return null;
//		
//	}
	
//	private ArrayList<ResourceType> getLowestProdudctions(ArrayList<ResourceType> resources, UsersCityConfig ucc) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
//	{
//		ArrayList<ResourceType> ret = new ArrayList<ResourceType>();
//		if(resources.size() > 0)
//		{
//			double min = pu.getFoodProduction(resources.get(0), ucc);
//			ResourceType minr = resources.get(0);
//			for(ResourceType rt : resources)
//			{
//				if(pu.getFoodProduction(rt, ucc) < min)
//				{
//					min = pu.getFoodProduction(rt, ucc);
//					minr = rt;
//				}
//			}
//			for(ResourceType rt : resources)
//			{
//				if(pu.getFoodProduction(rt, ucc) == min)
//				{
//					ret.add(rt);
//				}
//			}
//		}
//		return ret;
//	}

	
//	public double getPeopleCgount(UsersCityConfig cc,double destTime) 
//	{
//		double current = cc.getMyResources().getResource(ResourceType.peopleCount).getCount();
//		//if(main2.debug)destTime = main2.diff;
//		current += destTime * cc.getBirthRatePerHour();
//		return current;
//	}
	public Double getNajmniejszyNieujemnyCzas(Double t1, Double t2)
	{
		//swap if t1 > t2
		if(t1 == null && t2 == null)return null;
		else if(t1 == null)
		{
			if(t2 < 0)return null;
			else return t2;
		}
		else if(t2 == null)
		{
			if(t1 < 0)return null;
			else return t1;
		}
		if(t1 > t2)
		{
			double temp = t1;
			t1 = t2;
			t2 = temp;
		}
		if(t1 >= 0)
		{
			return t1;
		}
		else if(t2 >= 0)
		{
			return t2;
		}
		else return null;
	}
	public Double getNajmniejszyDodatniCzas(Double t1, Double t2)
	{
		//swap if t1 > t2
		if(t1 == null && t2 == null)return null;
		else if(t1 == null)
		{
			if(t2 <= 0)return null;
			else return t2;
		}
		else if(t2 == null)
		{
			if(t1 <= 0)return null;
			else return t1;
		}
		if(t1 > t2)
		{
			double temp = t1;
			t1 = t2;
			t2 = temp;
		}
		if(t1 > 0)
		{
			return t1;
		}
		else if(t2 > 0)
		{
			return t2;
		}
		else return null;
	}
//	private Boolean peopleOrFoodChange(Double foodChange, Double chPeople)
//	{
//		if(foodChange == null && chPeople == null)return null;
//		if(foodChange == null)return true;
//		if(chPeople == null)return false;
//		return foodChange >= chPeople ? true : false;
//	}
	public ProductionUtil getPu() {
		return pu;
	}
	public void setPu(ProductionUtil pu) {
		this.pu = pu;
	}
	public void setCh(WorldDefinition ch) {
		this.ch = ch;
	}
	public ConvertBuildingUtil getConvertBuildingUtil() {
		return convertBuildingUtil;
	}
	public void setConvertBuildingUtil(ConvertBuildingUtil convertBuildingUtil) {
		this.convertBuildingUtil = convertBuildingUtil;
	}
	
	
}
