package economycomputation;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import entities.UsersCityConfig;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import game_model_interfaces.IConvertBuildingModel;
import game_model_interfaces.IExtractBuildingModel;


/*
 * klasa odpowiedziala za wszystkie obliczenia zwi�zane z produkcj� fabryk.
 * Mo�na sprawdzi� np. sumaryczn� produkcj� uwzgl�diaj�� fabryki "paso�yty",
 * sprawdzic kt�ra fabryka produkuje najmniej, kt�ra najwi�cej itp.
 */

public class ProductionUtil {

	@Autowired private WorldDefinition ch;
	
	public WorldDefinition getCh() {
		return ch;
	}

	public void setCh(WorldDefinition ch) {
		this.ch = ch;
	}

	public double sumOfFacotoryProductions(List<StockType> maxInfResources, UsersCityConfig ucc)
	{
		double productionOfFactories = 0;
		for(StockType rt : maxInfResources)
		{
			productionOfFactories += this.getFoodProduction(rt,ucc);
		}
		return productionOfFactories;
	}
	
	public Double getFoodProduction(StockType rtype, UsersCityConfig ucc) 
	{
		if(ucc.getCityStocks().getStockByType(rtype) == null)return null;
		UsersCityConfig config = ch.getCivilization(ucc.getUserConfig().getCivil());
		//if(ucc.myFood.getFood(rtype).storage == Storage.nastyk)return 0.0;
		double current = 0;
		IExtractBuildingModel eb = config.getProducer(ucc.getUserConfig(), rtype);
		current = eb.getOutputPerHour(ucc);
		//current = modelHolder.getOutputPerHour(ucc, rtype, eb.getId());
		List<IConvertBuildingModel> list = config.getSpongers(ucc.getUserConfig(), rtype);
		if(list != null)
		{
			for(IConvertBuildingModel rp : list)
			{
				current += rp.getOutputPerHour(ucc);
			}
		}
		return current;
	}
}
