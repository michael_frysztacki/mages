package unit_testing.mapcom.mapprojections;

import unit_testing.mapcom.testing_utils.TestHelper;
import mapcom.mapprojections.Equirectangular;

public class EquirectangularTest extends MapProjectionTest
{
	@Override
	public Equirectangular createInstance()
	{
		return new Equirectangular(TestHelper.tinyMapPath);
	}
}
