package unit_testing.mapcom.mapprojections;

import static org.junit.Assert.assertTrue;
import mapcom.imgproc.Colors;
import mapcom.mapprojections.MapProjection;
import mapcom.math.PointOnWorldSphere;
import mapcom.worldstructure.TerrainType;

import org.junit.Before;
import org.junit.Test;

import unit_testing.mapcom.testing_utils.TestHelper;

public abstract class MapProjectionTest
{
	double delta = 0.00000000001;  // 10 trylionowych, o tyle conajwyzej moze byc blad w double'ach
	
	public MapProjection proj;
	int color;	
		
	PointOnWorldSphere spotOfBuraydahCity = new PointOnWorldSphere(TestHelper.buraydahCityLongitude, TestHelper.buraydahCityLatitude);
	PointOnWorldSphere spotOfWarsawCity = new PointOnWorldSphere(TestHelper.warsawLongitude, TestHelper.warsawLatitude);
	PointOnWorldSphere spotOfManausCity = new PointOnWorldSphere(TestHelper.manausCityLongitude, TestHelper.manausCityLatitude);
	PointOnWorldSphere spotOfHargheisaCity = new PointOnWorldSphere(TestHelper.hargheisaCityLongitude, TestHelper.hargheisaCityLatitude);
	PointOnWorldSphere spotOfCuscoCity = new PointOnWorldSphere(TestHelper.cuscoCityLongitude, TestHelper.cuscoCityLatitude);
	PointOnWorldSphere spotOfCenterOfTheMap = new PointOnWorldSphere(0, 0);
	  
	@Before
	public void initialize(){
		proj = createInstance();
	}	
	
	public abstract MapProjection createInstance();	

	
	@Test
	public void apprStep_Test_0010()
	{
		MapProjection proj = createInstance(pathToTestFiles + "Maps/terrains_2700x1350.png");
		Point start = new Point(-89, 0);
		Point end = new Point(0, 50);
		LineDrawing.drawGeodesic(proj, start, end, 0, Colors.violet);
		if( draw )
		proj.saveProjection(pathToTestFiles + "Results/0010.png");
		
		assertTrue( isEveryPointOnEdgeIsOfGivenColor(proj, start, end, Colors.violet));		
	}
	
	@Test
	public void apprStep_Test_0020()
	{
		MapProjection proj = createInstance(pathToTestFiles + "Maps/terrains_2700x1350.png");
		Point start = new Point(-89, 0);
		Point end = new Point(0, 84);
		LineDrawing.drawGeodesic(proj, start, end, 0, Colors.violet);
		if( draw )
		proj.saveProjection(pathToTestFiles + "Results/0020.png");
		
		assertTrue( isEveryPointOnEdgeIsOfGivenColor(proj, start, end, Colors.violet));		
	}
	
	@Test
	public void apprStep_Test_0030()
	{
		MapProjection proj = createInstance(pathToTestFiles + "Maps/terrains_2700x1350.png");
		Point start = new Point(-89, -60);
		Point end = new Point(90, 60);
		LineDrawing.drawGeodesic(proj, start, end, 0, Colors.violet);
		if( draw )
		proj.saveProjection(pathToTestFiles + "Results/0030.png");
		
		assertTrue( isEveryPointOnEdgeIsOfGivenColor(proj, start, end, Colors.violet));		
	}
	
	@Test
	public void apprStep_Test_0040()
	{
		MapProjection proj = createInstance(pathToTestFiles + "Maps/terrains_2700x1350.png");
		Point start = new Point(-90, 0);
		Point end = new Point(0, 89);
		LineDrawing.drawGeodesic(proj, start, end, 0, Colors.violet);
		if( draw )
		proj.saveProjection(pathToTestFiles + "Results/0040.png");
		
		assertTrue( isEveryPointOnEdgeIsOfGivenColor(proj, start, end, Colors.violet));		
	}
	
	@Test
	public void apprStep_Test_0050()
	{
		MapProjection proj = createInstance(pathToTestFiles + "Maps/terrains_2700x1350.png");
		Point start = new Point(-5, 87);
		Point end = new Point(5, 87);
		System.out.println(GeometryCalc.angleDistanceInRad(start, end));
		LineDrawing.drawGeodesic(proj, start, end, 0, Colors.violet);
		if( draw )
		proj.saveProjection(pathToTestFiles + "Results/0050.png");
		
		assertTrue( isEveryPointOnEdgeIsOfGivenColor(proj, start, end, Colors.violet));		
	}
	
	@Test
	public void getColorOfSpotOnWater(){
		color = proj.getRGB(spotOfCenterOfTheMap);		
		assertTrue(Colors.colorToTerrainType(color) == TerrainType.Water);
	}


}
