package unit_testing.mapcom.imgproc;

import static org.junit.Assert.assertTrue;
import mapcom.imgproc.Auxiliary;
import mapcom.imgproc.Colors;
import mapcom.imgproc.LineDrawing;
import mapcom.imgproc.PointDrawing;
import mapcom.mapprojections.Equirectangular;
import mapcom.mapprojections.MapProjection;
import mapcom.math.PointOnWorldSphere;
import mapcom.math.Transl;

import org.junit.Test;

public class LineDrawingTest
{
	String pathToTestFiles = "src/unit_testing/mapcom/imgproc/LineDrawingTest_Files/";	
	double PI = Math.PI;
	/******************************************************************************************************************
	 *  drawGeodesic Method TEST 
	 *****************************************************************************************************************/
	

	@Test
	public void drawGeodesic_Test_0010()
	{
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
		
		PointOnWorldSphere A = new PointOnWorldSphere(-89, 0.0);
		PointOnWorldSphere B = new PointOnWorldSphere(89, 0.0);
		LineDrawing.drawGeodesic(proj, A, B, 1, Colors.orange);		
		Auxiliary.saveImage(proj.getImg(), pathToTestFiles + "Results/0010.png");
		
		int orangeRGB = Colors.orange;		
		assertTrue( proj.getRGB(A) == orangeRGB );
		assertTrue( proj.getRGB(B) == orangeRGB );		
		PointOnWorldSphere C = new PointOnWorldSphere(0, 0);
		assertTrue( proj.getRGB(C) == orangeRGB );
	}
	
	@Test
	public void drawGeodesic_Test_0020()
	{
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
		
		PointOnWorldSphere A = new PointOnWorldSphere(-60, 30.0);
		PointOnWorldSphere B = new PointOnWorldSphere(60, 30.0);
		LineDrawing.drawGeodesic(proj, A, B, 1, Colors.orange);		
		Auxiliary.saveImage(proj.getImg(), pathToTestFiles + "Results/0020.png");
		
		int orangeRGB = Colors.orange;		
		assertTrue( proj.getRGB(A) == orangeRGB );
		assertTrue( proj.getRGB(B) == orangeRGB );	
		assertTrue( proj.getRGB(588, 512) == orangeRGB );		
	}
	
	
	
	/******************************************************************************************************************
	 *  drawNavCircle Method TEST 
	 *****************************************************************************************************************/
	
	// Ponizsze klasy testujemy w nastepujacy sposob:
	// sprawdzamy czy kolo przejezdza przez dwa punkty ( jeden na wschodzie, drugi na zachodzie od srodka nawigacji ) 
	// bedace na tym samym rownolezniku co centrum nawigacji
	
		
	@Test
	public void drawNavCircle_Test_0030()
	{
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
		
		PointOnWorldSphere center = new PointOnWorldSphere(0, 0);		
		double navRadiusKm = 1000;
		PointOnWorldSphere westPoint = new PointOnWorldSphere(center.getLongit() - Transl.kmToDegreesOnParallel(navRadiusKm, center.getLat()), center.getLat());
		PointOnWorldSphere eastPoint = new PointOnWorldSphere(center.getLongit() + Transl.kmToDegreesOnParallel(navRadiusKm, center.getLat()), center.getLat());
		LineDrawing.drawNavCircle(proj, center, navRadiusKm, 0, Colors.orange);		
		
		int orangeRGB = Colors.orange;
		
		PointDrawing.markWithCrossOfColor(center, 5, proj, Colors.violet);
		PointDrawing.markWithCrossOfColor(westPoint, 5, proj, Colors.violet);
		PointDrawing.markWithCrossOfColor(eastPoint, 5, proj, Colors.violet);
		Auxiliary.saveImage(proj.getImg(), pathToTestFiles + "Results/0030.png");		

		// dla testow trzeba zamalowac wyzej narysowane fioletowe krzyze
		PointDrawing.markWithCrossOfColor(westPoint, 5, proj, Colors.orange);
		PointDrawing.markWithCrossOfColor(eastPoint, 5, proj, Colors.orange);
		
		assertTrue( proj.getRGB(westPoint) == orangeRGB );
		assertTrue( proj.getRGB(eastPoint) == orangeRGB );	
	}
	
	@Test
	public void drawNavCircle_Test_0040()
	{
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
		
		PointOnWorldSphere center = new PointOnWorldSphere(160, 55);		
		double navRadiusKm = 1000;
		PointOnWorldSphere westPoint = new PointOnWorldSphere(center.getLongit() - Transl.kmToDegreesOnParallel(navRadiusKm, center.getLat()), center.getLat());
		PointOnWorldSphere eastPoint = new PointOnWorldSphere(center.getLongit() + Transl.kmToDegreesOnParallel(navRadiusKm, center.getLat()), center.getLat());
		LineDrawing.drawNavCircle(proj, center, navRadiusKm, 0, Colors.orange);	
		
		PointDrawing.markWithCrossOfColor(center, 5, proj, Colors.violet);
		PointDrawing.markWithCrossOfColor(westPoint, 5, proj, Colors.violet);
		PointDrawing.markWithCrossOfColor(eastPoint, 5, proj, Colors.violet);
		Auxiliary.saveImage(proj.getImg(), pathToTestFiles + "Results/0040.png");
		
		// dla testow trzeba zamalowac wyzej narysowane fioletowe krzyze
		PointDrawing.markWithCrossOfColor(westPoint, 5, proj, Colors.orange);
		PointDrawing.markWithCrossOfColor(eastPoint, 5, proj, Colors.orange);
		
		int orangeRGB = Colors.orange;	
		assertTrue( proj.getRGB(westPoint) == orangeRGB );
		assertTrue( proj.getRGB(eastPoint) == orangeRGB );		
		
		
	}

	@Test
	public void drawNavCircle_Test_0050()
	{
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
		
		PointOnWorldSphere center = new PointOnWorldSphere(-130, -35);		
		double navRadiusKm = 1000;
		PointOnWorldSphere westPoint = new PointOnWorldSphere(center.getLongit() - Transl.kmToDegreesOnParallel(navRadiusKm, center.getLat()), center.getLat());
		PointOnWorldSphere eastPoint = new PointOnWorldSphere(center.getLongit() + Transl.kmToDegreesOnParallel(navRadiusKm, center.getLat()), center.getLat());
		LineDrawing.drawNavCircle(proj, center, navRadiusKm, 0, Colors.orange);		

		// dla testow trzeba zamalowac wyzej narysowane fioletowe krzyze
		PointDrawing.markWithCrossOfColor(westPoint, 5, proj, Colors.violet);
		PointDrawing.markWithCrossOfColor(eastPoint, 5, proj, Colors.violet);
		PointDrawing.markWithCrossOfColor(center, 5, proj, Colors.violet);
		Auxiliary.saveImage(proj.getImg(), pathToTestFiles + "Results/0050.png");
		
		// dla testow trzeba zamalowac wyzej narysowane fioletowe krzyze
		PointDrawing.markWithCrossOfColor(westPoint, 5, proj, Colors.orange);
		PointDrawing.markWithCrossOfColor(eastPoint, 5, proj, Colors.orange);
		
		int orangeRGB = Colors.orange;						
		assertTrue( proj.getRGB(westPoint) == orangeRGB );
		assertTrue( proj.getRGB(eastPoint) == orangeRGB );		
		
	}
	
	

	/******************************************************************************************************************
	 *  drawGreatCircle Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void drawGreatCircle_Test_0060()
	{
		int color = Colors.orange;
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
				
		PointOnWorldSphere toCheck1 = new PointOnWorldSphere(0, 30.0);
		PointOnWorldSphere toCheck2 = new PointOnWorldSphere(90, 0.0);			
		
		PointOnWorldSphere center = new PointOnWorldSphere(180, -60);
		PointDrawing.markPointWithSquareOfColor(center, 7, proj, Colors.darkblue);
		
		PointDrawing.markPointWithSquareOfColor(toCheck1, 3, proj, Colors.violet);
		PointDrawing.markPointWithSquareOfColor(toCheck2, 3, proj, Colors.violet);
		LineDrawing.drawGreatCircle(proj, Math.PI/6, 0, 0, color);
		
		Auxiliary.saveImage(proj.getImg(), pathToTestFiles + "Results/0060.png");
		
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck1), color) );
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck2), color) );		
	}
	
	@Test
	public void drawGreatCircle_Test_0070()
	{
		int color = Colors.orange;
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
				
		PointOnWorldSphere toCheck1 = new PointOnWorldSphere(0, -30.0);
		PointOnWorldSphere toCheck2 = new PointOnWorldSphere(90, 0.0);		
				
		PointDrawing.markPointWithSquareOfColor(toCheck1, 3, proj, Colors.violet);
		PointDrawing.markPointWithSquareOfColor(toCheck2, 3, proj, Colors.violet);
		LineDrawing.drawGreatCircle(proj, -Math.PI/6, 0, 0, color);
		
		Auxiliary.saveImage(proj.getImg(), pathToTestFiles + "Results/0070.png");
		
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck1), color) );
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck2), color) );		
	}
	@Test
	public void drawGreatCircle_Test_0080()
	{
		int color = Colors.orange;
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
				
		PointOnWorldSphere toCheck1 = new PointOnWorldSphere(60, 30.0);
		PointOnWorldSphere toCheck2 = new PointOnWorldSphere(150, 0.0);			
		
		PointDrawing.markPointWithSquareOfColor(toCheck1, 3, proj, Colors.violet);
		PointDrawing.markPointWithSquareOfColor(toCheck2, 3, proj, Colors.violet);
		LineDrawing.drawGreatCircle(proj, Math.PI/6, Math.PI/3, 0, color);
		
		Auxiliary.saveImage(proj.getImg(), pathToTestFiles + "Results/0080.png");
		
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck1), color) );
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck2), color) );		
	}	
	@Test
	public void drawGreatCircle_Test_0090()
	{
		int color = Colors.orange;
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
				
		PointOnWorldSphere toCheck1 = new PointOnWorldSphere(60, -30.0);
		PointOnWorldSphere toCheck2 = new PointOnWorldSphere(150, 0.0);		
				
		PointDrawing.markPointWithSquareOfColor(toCheck1, 3, proj, Colors.violet);
		PointDrawing.markPointWithSquareOfColor(toCheck2, 3, proj, Colors.violet);
		LineDrawing.drawGreatCircle(proj, -Math.PI/6, Math.PI/3, 0, color);
		
		Auxiliary.saveImage(proj.getImg(), pathToTestFiles + "Results/0090.png");
		
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck1), color) );
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck2), color) );		
	}
	@Test
	public void drawGreatCircle_Test_0100()
	{
		int color = Colors.orange;
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
		
		PointOnWorldSphere A = new PointOnWorldSphere(90.0, 0.0);
		PointOnWorldSphere B = new PointOnWorldSphere(9.0, 0.0);
		
		PointOnWorldSphere toCheck1 = A;
		PointOnWorldSphere toCheck2 = B;		
				
		PointDrawing.markPointWithSquareOfColor(toCheck1, 3, proj, Colors.violet);
		PointDrawing.markPointWithSquareOfColor(toCheck2, 3, proj, Colors.violet);
		LineDrawing.drawGreatCircle(proj, A, B, 0, color);
		
		Auxiliary.saveImage(proj.getImg(), pathToTestFiles + "Results/0100.png");
		
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck1), color) );
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck2), color) );		
	}
	
	@Test
	public void drawGreatCircle_Test_0110()
	{
		int color = Colors.orange;
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
		
		PointOnWorldSphere A = new PointOnWorldSphere(90.0, -6.0);
		PointOnWorldSphere B = new PointOnWorldSphere(9.0, -16.0);
		
		PointOnWorldSphere toCheck1 = A;
		PointOnWorldSphere toCheck2 = B;		
				
		PointDrawing.markPointWithSquareOfColor(toCheck1, 3, proj, Colors.violet);
		PointDrawing.markPointWithSquareOfColor(toCheck2, 3, proj, Colors.violet);
		LineDrawing.drawGreatCircle(proj, A, B, 0, color);
		
		Auxiliary.saveImage(proj.getImg(), pathToTestFiles + "Results/0110.png");
		
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck1), color) );
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck2), color) );		
	}
	
	@Test
	public void drawGreatCircle_Test_0120()
	{
		int color = Colors.orange;
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
		
		PointOnWorldSphere A = new PointOnWorldSphere(90.0, -6.0);
		PointOnWorldSphere B = new PointOnWorldSphere(9.0, -70.0);
		
		PointOnWorldSphere toCheck1 = A;
		PointOnWorldSphere toCheck2 = B;		
				
		PointDrawing.markPointWithSquareOfColor(toCheck1, 3, proj, Colors.violet);
		PointDrawing.markPointWithSquareOfColor(toCheck2, 3, proj, Colors.violet);
		LineDrawing.drawGreatCircle(proj, A, B, 0, color);
		
		Auxiliary.saveImage(proj.getImg(), pathToTestFiles + "Results/0120.png");
		
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck1), color) );
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck2), color) );		
	}
	
	@Test
	public void drawGreatCircle_Test_0130()
	{
		int color = Colors.orange;
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
		
		PointOnWorldSphere A = new PointOnWorldSphere(-45, -15.0);
		PointOnWorldSphere B = new PointOnWorldSphere(45.0, -15.0);
		
		PointOnWorldSphere toCheck1 = A;
		PointOnWorldSphere toCheck2 = B;		
				
		PointDrawing.markPointWithSquareOfColor(toCheck1, 3, proj, Colors.violet);
		PointDrawing.markPointWithSquareOfColor(toCheck2, 3, proj, Colors.violet);
		LineDrawing.drawGreatCircle(proj, A, B, 0, color);
		
		Auxiliary.saveImage(proj.getImg(), pathToTestFiles + "Results/0130.png");
		
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck1), color) );
		assertTrue( Colors.areTheSameColors(proj.getRGB(toCheck2), color) );		
	}
}
