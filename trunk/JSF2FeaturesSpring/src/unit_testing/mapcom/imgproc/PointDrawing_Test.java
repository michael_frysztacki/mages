package unit_testing.mapcom.imgproc;

import static org.junit.Assert.*;
import mapcom.imgproc.Colors;
import mapcom.imgproc.PointDrawing;
import mapcom.mapprojections.Equirectangular;
import mapcom.mapprojections.MapProjection;

import org.junit.Test;

public class PointDrawing_Test
{
	String pathToTestFiles = "src/unit_testing/mapcom/imgproc/PointDrawingTest_Files/";	
	
	/******************************************************************************************************************
	 *  markPointWithSquareColor Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void markPointWithSquareColor_Test_0010()
	{
		// rysowanie kwadratu w rogu rysunku 
		// obrazek ma wymiary 12x8
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/testImg.png");		
		PointDrawing.markPointWithSquareOfColor(2, 2, 2, proj, Colors.getWaterColor());
		int waterRGB = Colors.getWaterColor(); 
		
		// sprawdzamy czy caly kwadrat jest zamalowany
		for(int i = 0; i<5; i++)
		{
			for(int j = 0; j<5; j++)
			{
				assertTrue( proj.getRGB(i, j) == waterRGB );	
			}
		}
	}	
	@Test
	public void markPointWithSquareColor_Test_0020()
	{  
		// rysowanie kwadratu wyjezdzajacego za rysunek (sprawdzamy, czy zabezpieczenia dzialaja)
		// obrazek ma wymiary 12x8
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/testImg.png");		
		PointDrawing.markPointWithSquareOfColor(1, 1, 2, proj, Colors.getWaterColor());
		int waterRGB = Colors.getWaterColor(); 
		
		// sprawdzamy czy caly kwadrat jest zamalowany
		for(int i = 0; i<4; i++)
		{
			for(int j = 0; j<4; j++)
			{
				assertTrue( proj.getRGB(i, j) == waterRGB );	
			}
		}
	}
	@Test
	public void markPointWithSquareColor_Test_0030()
	{  
		// rysowanie kwadratu wyjezdzajacego za rysunek (sprawdzamy, czy zabezpieczenia dzialaja)
		// obrazek ma wymiary 12x8
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/testImg.png");		
		PointDrawing.markPointWithSquareOfColor(0, 0, 2, proj, Colors.getWaterColor());
		int waterRGB = Colors.getWaterColor(); 
		
		// sprawdzamy czy caly kwadrat jest zamalowany
		for(int i = 0; i<3; i++)
		{
			for(int j = 0; j<3; j++)
			{
				assertTrue( proj.getRGB(i, j) == waterRGB );	
			}
		}
	}
	@Test
	public void markPointWithSquareColor_Test_0040()
	{  
		// rysowanie kwadratu wyjezdzajacego za rysunek (sprawdzamy, czy zabezpieczenia dzialaja)
		// obrazek ma wymiary 12x8
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/testImg.png");		
		PointDrawing.markPointWithSquareOfColor(11, 11, 2, proj, Colors.getWaterColor());
		int waterRGB = Colors.getWaterColor(); 
		
		// sprawdzamy czy caly kwadrat jest zamalowany
		for(int i = 9; i<12; i++)
		{
			for(int j = 5; j<8; j++)
			{
				assertTrue( proj.getRGB(i, j) == waterRGB );	
			}
		}
	}

	
	
	/******************************************************************************************************************
	 *  markPointWithXColor Method TEST 
	 *****************************************************************************************************************/
	@Test
	public void markPointWithXColor_Test_0050()
	{  
		// rysowanie krzyza niewyjezdzajacego za rysunek
		// obrazek ma wymiary 12x8
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/testImg.png");		
		PointDrawing.markPointWithXOfColor(2, 2, 2, proj, Colors.getWaterColor());
		int waterRGB = Colors.getWaterColor(); 
		
		assertTrue( proj.getRGB(0, 0) == waterRGB );
		assertTrue( proj.getRGB(1, 1) == waterRGB );
		assertTrue( proj.getRGB(2, 2) == waterRGB );
		assertTrue( proj.getRGB(3, 3) == waterRGB );
		assertTrue( proj.getRGB(4, 4) == waterRGB );
		
		assertTrue( proj.getRGB(4, 0) == waterRGB );
		assertTrue( proj.getRGB(3, 1) == waterRGB );
		assertTrue( proj.getRGB(2, 2) == waterRGB );
		assertTrue( proj.getRGB(1, 3) == waterRGB );
		assertTrue( proj.getRGB(0, 4) == waterRGB );
	}
	
	@Test
	public void markPointWithXColor_Test_0060()
	{  
		// rysowanie krzyza wyjezdzajacego na lewa gore
		// obrazek ma wymiary 12x8
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/testImg.png");		
		PointDrawing.markPointWithXOfColor(1, 1 , 2, proj, Colors.getWaterColor());
		int waterRGB = Colors.getWaterColor(); 
		
		assertTrue( proj.getRGB(0, 0) == waterRGB );
		assertTrue( proj.getRGB(1, 1) == waterRGB );
		assertTrue( proj.getRGB(2, 2) == waterRGB );
		assertTrue( proj.getRGB(3, 3) == waterRGB );
						
		assertTrue( proj.getRGB(2, 0) == waterRGB );
		assertTrue( proj.getRGB(1, 1) == waterRGB );
		assertTrue( proj.getRGB(0, 2) == waterRGB );		
	}
	
	@Test
	public void markPointWithXColor_Test_0070()
	{  
		// rysowanie krzyza wyjezdzajacego na lewa gore
		// obrazek ma wymiary 12x8
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/testImg.png");		
		PointDrawing.markPointWithXOfColor(1, 1 , 2, proj, Colors.getWaterColor());
		int waterRGB = Colors.getWaterColor(); 
		
		assertTrue( proj.getRGB(0, 0) == waterRGB );
		assertTrue( proj.getRGB(1, 1) == waterRGB );
		assertTrue( proj.getRGB(2, 2) == waterRGB );
		assertTrue( proj.getRGB(3, 3) == waterRGB );
						
		assertTrue( proj.getRGB(2, 0) == waterRGB );
		assertTrue( proj.getRGB(1, 1) == waterRGB );
		assertTrue( proj.getRGB(0, 2) == waterRGB );		
	}

	@Test
	public void markPointWithXColor_Test_0080()
	{  
		// rysowanie krzyza wyjezdzajacego na lewy dol
		// obrazek ma wymiary 12x8
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/testImg.png");		
		PointDrawing.markPointWithXOfColor(10, 6 , 2, proj, Colors.getWaterColor());
		int waterRGB = Colors.getWaterColor(); 
		
		assertTrue( proj.getRGB(10, 6) == waterRGB );
		assertTrue( proj.getRGB(9, 5) == waterRGB );
		assertTrue( proj.getRGB(8, 4) == waterRGB );
		assertTrue( proj.getRGB(7, 3) == waterRGB );
						
		assertTrue( proj.getRGB(9, 7) == waterRGB );
		assertTrue( proj.getRGB(10, 6) == waterRGB );
		assertTrue( proj.getRGB(11, 7) == waterRGB );				
	}
	/******************************************************************************************************************
	 *  markPointWithCrossColor Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void markPointWithCrossColor_Test_0100()
	{  
		// rysowanie krzyza wyjezdzajacego na lewa gore
		// obrazek ma wymiary 12x8
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/testImg.png");		
		PointDrawing.markPointWithCrossOfColor(1, 1 , 2, proj, Colors.getWaterColor());
		int waterRGB = Colors.getWaterColor(); 
		
		assertTrue( proj.getRGB(1, 0) == waterRGB );
		assertTrue( proj.getRGB(1, 1) == waterRGB );
		assertTrue( proj.getRGB(1, 2) == waterRGB );
		assertTrue( proj.getRGB(1, 3) == waterRGB );
						
		assertTrue( proj.getRGB(0, 1) == waterRGB );
		assertTrue( proj.getRGB(1, 1) == waterRGB );
		assertTrue( proj.getRGB(2, 1) == waterRGB );
		assertTrue( proj.getRGB(3, 1) == waterRGB );
	}

	@Test
	public void markPointWithCrossColor_Test_0110()
	{  
		// rysowanie krzyza wyjezdzajacego na lewy dol
		// obrazek ma wymiary 12x8
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/testImg.png");		
		PointDrawing.markPointWithCrossOfColor(10, 6 , 2, proj, Colors.getWaterColor());
		int waterRGB = Colors.getWaterColor(); 
		
		assertTrue( proj.getRGB(10, 4) == waterRGB );
		assertTrue( proj.getRGB(10, 5) == waterRGB );
		assertTrue( proj.getRGB(10, 6) == waterRGB );
		assertTrue( proj.getRGB(10, 7) == waterRGB );
						
		assertTrue( proj.getRGB(8, 6) == waterRGB );
		assertTrue( proj.getRGB(9, 6) == waterRGB );
		assertTrue( proj.getRGB(10, 6) == waterRGB );
		assertTrue( proj.getRGB(11, 6) == waterRGB );
	}
}

