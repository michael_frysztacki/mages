package unit_testing.mapcom.imgproc;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;

import mapcom.imgproc.Auxiliary;
import mapcom.imgproc.Colors;
import mapcom.worldstructure.TerrainType;

import org.junit.BeforeClass;
import org.junit.Test;

import unit_testing.mapcom.testing_utils.TestHelper;

public class ColorsTest {			
	
	static BufferedImage officialTinyMap;
	static BufferedImage officialSmallMap;
	static BufferedImage officialMediumMap;	
	
	int waterXOnTinyMap = 600;
	int waterYOnTinyMap = 300;
	
	int plainXOnTinyMap = 800;
	int plainYOnTinyMap = 400;
	
	int desertXOnTinyMap = 710;
	int desertYOnTinyMap = 210;
	
	int jungleXOnTinyMap = 750;
	int jungleYOnTinyMap = 350;
	
	int steppeXOnTinyMap = 800;
	int steppeYOnTinyMap = 150;
	
	int mountainXOnTinyMap = 850;
	int mountainYOnTinyMap = 200;
	
	@BeforeClass
	public static void setUp(){
		officialTinyMap = Auxiliary.loadImage(TestHelper.tinyMapPath);
		officialSmallMap = Auxiliary.loadImage(TestHelper.smallMapPath);
		officialMediumMap = Auxiliary.loadImage(TestHelper.mediumMapPath);
	}
	
	// ***************************************************************************************************************
	// *************************** GETTERS ***************************************************************************
	// ***************************************************************************************************************	
	@Test
	public void getNeutralColorTest(){
		assertTrue( Colors.getNeutralColor() == Colors.black );
	}
	@Test
	public void getPlainColorTestTest(){
		assertTrue( Colors.getPlainColor() == Colors.white );
	}
	@Test
	public void getWaterColorTest(){
		assertTrue( Colors.getWaterColor() == Colors.lightBlue );
	}
	@Test
	public void getSteppeColorTest(){
		assertTrue( Colors.getSteppeColor() == Colors.red );
	}
	@Test
	public void getJungleColorTest(){
		assertTrue( Colors.getJungleColor() == Colors.green );
	}
	@Test
	public void getMountainColorTest(){
		assertTrue( Colors.getMountainColor() == Colors.grey );
	}
	@Test
	public void getDesertColorTest(){
		assertTrue( Colors.getDesertColor() == Colors.yellow );
	}
	
	
	// ***************************************************************************************************************
	// *************************** COMPARING COLORS ******************************************************************
	// ***************************************************************************************************************	
	@Test
	public void compareSlighltyDifferentColorsTest(){
		assertFalse( Colors.areTheSameColors(0x3399CC, 0x3399CD) );		
	}
	@Test
	public void compareTheSameColorsTest(){
		assertTrue( Colors.areTheSameColors(0x3399CC, 0x3399CC) );		
	}
	@Test
	public void isJungleWaterTest(){
		assertFalse( Colors.isWaterColor(Colors.getJungleColor()) );		
	}
	@Test
	public void isWaterColorTest(){
		assertTrue( Colors.isWaterColor(Colors.getWaterColor()) );		
	}
	@Test
	public void isWaterLandTest(){
		assertFalse( Colors.isLandColor(Colors.getWaterColor()) );		
	}
	@Test
	public void isNeutralLandTest(){
		assertFalse( Colors.isLandColor(Colors.getNeutralColor()) );		
	}
	@Test
	public void isJungleLandTest(){
		assertTrue( Colors.isLandColor(Colors.getJungleColor()) );		
	}
	@Test
	public void isSteppeLandTest(){
		assertTrue( Colors.isLandColor(Colors.getSteppeColor()) );		
	}
	@Test
	public void isMountainLandTest(){
		assertTrue( Colors.isLandColor(Colors.getMountainColor()) );		
	}
	@Test
	public void isPlainLandTest(){
		assertTrue( Colors.isLandColor(Colors.getPlainColor()) );		
	}
	@Test
	public void isDesertLandTest(){
		assertTrue( Colors.isLandColor(Colors.getDesertColor()) );		
	}
	
	
	// ***************************************************************************************************************
	// *************************** COLORS TO TERRAINTYPES CONVERSIONS ************************************************
	// ***************************************************************************************************************
	@Test
	public void waterColorToWaterTerrainTest(){
		assertTrue( TerrainType.Water == Colors.colorToTerrainType(Colors.getWaterColor()) );
	}
	@Test
	public void desertColorToDesertTerrainTest(){
		assertTrue( TerrainType.Desert == Colors.colorToTerrainType(Colors.getDesertColor()) );
	}
	@Test
	public void steppeColorToSteppeTerrainTest(){
		assertTrue( TerrainType.Steppe == Colors.colorToTerrainType(Colors.getSteppeColor()) );
	}
	@Test
	public void plainColorToPlainTerrainTest(){
		assertTrue( TerrainType.Plain == Colors.colorToTerrainType(Colors.getPlainColor()) );
	}
	@Test
	public void jungleColorTojungleTerrainTest(){
		assertTrue( TerrainType.Jungle == Colors.colorToTerrainType(Colors.getJungleColor()) );
	}
	@Test
	public void mountainColorToMountainTerrainTest(){
		assertTrue( TerrainType.Mountain == Colors.colorToTerrainType(Colors.getMountainColor()) );
	}
	@Test
	public void ifNotALandColorThanMarkAsWater1Test(){
		assertTrue( TerrainType.Water == Colors.colorToTerrainType(Colors.getNeutralColor()) );
	}
	@Test
	public void ifNotALandColorThanMarkAsWater2Test(){
		assertTrue( TerrainType.Water == Colors.colorToTerrainType(0xAABBCCDD) );
	}
	
	
	
	// ***************************************************************************************************************
	// *************************** COLORS TO TERRAINTYPES CONVERSIONS ************************************************
	// ***************************************************************************************************************	
	@Test
	public void plainTerrainToPlainColorTest(){
		assertTrue( Colors.getPlainColor() == Colors.terrainTypeToColor(TerrainType.Plain));
	}
	@Test
	public void plainTerrainToWaterColorTest(){
		assertTrue( Colors.getWaterColor() == Colors.terrainTypeToColor(TerrainType.Water));
	}
	@Test
	public void plainTerrainToSteppeColorTest(){
		assertTrue( Colors.getSteppeColor() == Colors.terrainTypeToColor(TerrainType.Steppe));
	}
	@Test
	public void plainTerrainToDesertColorTest(){
		assertTrue( Colors.getDesertColor() == Colors.terrainTypeToColor(TerrainType.Desert));
	}
	@Test
	public void plainTerrainToMountainColorTest(){
		assertTrue( Colors.getMountainColor() == Colors.terrainTypeToColor(TerrainType.Mountain));
	}
	@Test
	public void plainTerrainToJungleColorTest(){
		assertTrue( Colors.getJungleColor() == Colors.terrainTypeToColor(TerrainType.Jungle));
	}
	
	// ***************************************************************************************************************
	// **************************************** TINY MAP *************************************************************
	// ***************************************************************************************************************
	@Test
	public void verifyPlainColorOnOfficialTinyMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getPlainColor(), officialTinyMap.getRGB(plainXOnTinyMap, plainYOnTinyMap)) );
	}
	@Test
	public void verifyWaterColorOnOfficialTinyMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getWaterColor(), officialTinyMap.getRGB(waterXOnTinyMap, waterYOnTinyMap)) );
	}
	@Test
	public void verifyDesertColorOnOfficialTinyMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getDesertColor(), officialTinyMap.getRGB(desertXOnTinyMap, desertYOnTinyMap)) );
	}
	@Test
	public void verifyJungleColorOnOfficialTinyMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getJungleColor(), officialTinyMap.getRGB(jungleXOnTinyMap, jungleYOnTinyMap)) );
	}
	@Test
	public void verifySteppeColorOnOfficialTinyMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getSteppeColor(), officialTinyMap.getRGB(steppeXOnTinyMap, steppeYOnTinyMap)) );
	}
	@Test
	public void verifyMountainColorOnOfficialTinyMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getMountainColor(), officialTinyMap.getRGB(mountainXOnTinyMap, mountainYOnTinyMap)) );
	}
	
	// ***************************************************************************************************************
	// **************************************** SMALL MAP ************************************************************
	// ***************************************************************************************************************
	@Test
	public void verifyPlainColorOnOfficialSmallMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getPlainColor(), officialSmallMap.getRGB(plainXOnTinyMap*2, plainYOnTinyMap*2)) );
	}
	@Test
	public void verifyWaterColorOnOfficialSmallMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getWaterColor(), officialSmallMap.getRGB(waterXOnTinyMap*2, waterYOnTinyMap*2)) );
	}
	@Test
	public void verifyDesertColorOnOfficialSmallMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getDesertColor(), officialSmallMap.getRGB(desertXOnTinyMap*2, desertYOnTinyMap*2)) );
	}
	@Test
	public void verifyJungleColorOnOfficialSmallMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getJungleColor(), officialSmallMap.getRGB(jungleXOnTinyMap*2, jungleYOnTinyMap*2)) );
	}
	@Test
	public void verifySteppeColorOnOfficialSmallMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getSteppeColor(), officialSmallMap.getRGB(steppeXOnTinyMap*2, steppeYOnTinyMap*2)) );
	}
	@Test
	public void verifyMountainColorOnOfficialSmallMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getMountainColor(), officialSmallMap.getRGB(mountainXOnTinyMap*2, mountainYOnTinyMap*2)) );
	}
	
	
	// ***************************************************************************************************************
	// **************************************** MEDIUM MAP ***********************************************************
	// ***************************************************************************************************************
	@Test
	public void verifyPlainColorOnOfficialMediumMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getPlainColor(), officialMediumMap.getRGB(plainXOnTinyMap*4, plainYOnTinyMap*4)) );
	}
	@Test
	public void verifyWaterColorOnOfficialMediumMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getWaterColor(), officialMediumMap.getRGB(waterXOnTinyMap*4, waterYOnTinyMap*4)) );
	}
	@Test
	public void verifyDesertColorOnOfficialMediumMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getDesertColor(), officialMediumMap.getRGB(desertXOnTinyMap*4, desertYOnTinyMap*4)) );
	}
	@Test
	public void verifyJungleColorOnOfficialMediumMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getJungleColor(), officialMediumMap.getRGB(jungleXOnTinyMap*4, jungleYOnTinyMap*4)) );
	}
	@Test
	public void verifySteppeColorOnOfficialMediumMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getSteppeColor(), officialMediumMap.getRGB(steppeXOnTinyMap*4, steppeYOnTinyMap*4)) );
	}
	@Test
	public void verifyMountainColorOnOfficialMediumMapTest(){
		assertTrue( Colors.areTheSameColors(Colors.getMountainColor(), officialMediumMap.getRGB(mountainXOnTinyMap*4, mountainYOnTinyMap*4)) );
	}
	
}
