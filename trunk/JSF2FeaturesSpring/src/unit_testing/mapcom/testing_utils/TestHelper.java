package unit_testing.mapcom.testing_utils;

import java.io.File;
import java.util.Iterator;


public class TestHelper {
	
	public static String testsDirectory = "src/unit_testing/mapcom/";
	
	public static String mapsDirectory = testsDirectory + "testing_utils/Official Maps/Equirectangular/";
	
	public static String tinyMapPath = mapsDirectory + "terrains_1350x675.png";
	public static String smallMapPath = mapsDirectory + "terrains_2700x1350.png"; 
	public static String mediumMapPath = mapsDirectory + "terrains_5400x2700.png";
	
	public static double warsawLongitude = 21.0166673;
	public static double warsawLatitude = -52.233333;
	
	// City in Amazonia, Brazil
	public static double manausCityLongitude = -59.941111;
	public static double manausCityLatitude = 3.044444;
	
	// City in Peru
	public static double cuscoCityLongitude = -71.971307;
	public static double cuscoCityLatitude = 13.526141;
	
	// City in Saudi Arabia
	public static double buraydahCityLongitude = 43.966667;   
	public static double buraydahCityLatitude = -26.333333;

	// City in Etiopia
	public static double hargheisaCityLongitude = 44.061455;  
	public static double hargheisaCityLatitude = -9.559422;
	
	
	public static int countTraversedByIterator(Iterator<? extends Object> iter)
	{
		int count = 0;
		while( iter.hasNext() ){
			iter.next();
			count++;
		}
		return count;
	}
	
	public static void deleteContentOfDirectoryExceptGitIgnoreFile(String directory){
		File index = new File(directory);
		String[]entries = index.list();
		for(String s: entries){
		    File currentFile = new File(index.getPath(),s);
		    if( !currentFile.toString().contains(".gitignore") ){
		    	currentFile.delete();
		    }
		}
	}
}
