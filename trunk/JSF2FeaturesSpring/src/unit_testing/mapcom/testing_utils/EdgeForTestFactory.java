package unit_testing.mapcom.testing_utils;

import mapcom.mapprojections.MapProjection;
import mapcom.worldconstruction.EdgeBuilder;
import mapcom.worldconstruction.EdgeBuilder.TerrainInformation;
import mapcom.worldstructure.Edge;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.TerrainType;

public class EdgeForTestFactory 
{	
	private static EdgeBuilder builder = new EdgeBuilder();
	public static Edge createEdge(Point start, Point end)
	{
		return new Edge(start, end, new TerrainType[]{TerrainType.Desert}, new float[]{0.1f});
	}
	public static Edge createEdge(int id1, int id2)
	{
		Point start = new Point(0, 0, id1, true, true);
		Point end = new Point(0, 0, id2, true, true);
		return new Edge(start, end, new TerrainType[]{TerrainType.Desert}, new float[]{0.1f});
	}
	
	public static Edge createEdge(Point A, Point B, MapProjection proj)
	{
		TerrainInformation ti = builder.getTerrainInfo(A, B, proj);
		return new Edge(A, B, ti.typesOfTerrainsTraversed, ti.percentagesOfTerrainsTraversed);
	}
}
