package unit_testing.mapcom.testing_utils;

import mapcom.worldstructure.Capital;
import mapcom.worldstructure.Colony;
import mapcom.worldstructure.Point;

public class PointForTestFactory {
	public static Point createPoint(int id){
		return new Point(0, 0, id, false, false);
	}
	
	public static Point createPoint(double longitude, double latitude){
		return new Point(longitude, latitude, 0, false, false);
	}
	
	public static Point createLandPoint(){
		boolean shouldBeWater = false;
		return new Point(0, 0, 0, shouldBeWater, false);
	}
	
	public static Point createWaterPoint(){
		boolean shouldBeWater = true;
		return new Point(0, 0, 0, shouldBeWater, true);
	}
	
	public static Colony createLandColony(){
		boolean shouldBeWater = false;
		return new Colony(0, 0, 0, shouldBeWater, false);
	}
	
	public static Colony createWaterColony(){
		boolean shouldBeWater = true;
		return new Colony(0, 0, 0, shouldBeWater, true);
	}
	
	public static Capital createLandCapital(){
		boolean shouldBeWater = false;
		return new Capital(0, 0, 0, "name", shouldBeWater, false);
	}
	
	public static Capital createWaterCapital(){
		boolean shouldBeWater = true;
		return new Capital(0, 0, 0, "name", shouldBeWater, true);
	}
	
	public static Point createLandPoint(double longitude, double latitude){
		boolean shouldBeWater = false;
		return new Point(longitude, latitude, 0, shouldBeWater, false);
	}
	
	public static Point createWaterPoint(double longitude, double latitude){
		boolean shouldBeWater = true;
		return new Point(longitude, latitude, 0, shouldBeWater, true);
	}
}
