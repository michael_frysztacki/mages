package unit_testing.mapcom.worldstructure;

import static org.junit.Assert.assertEquals;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.PointsStructure;

import org.junit.Before;
import org.junit.Test;

public class UserGraphTest
{
	
	@Before
	public void initialiaze()
	{
		ps = createInstance();
	}
	
	@Test
	public void doesRetrievalFromPointsStructureWork()
	{
		int id = 1;
		ps.addPoint( createPoint(0, 0, id) );
		Point temp = ps.getPointWithId(id);
		assertEquals(0, temp.getLat(), delta);	
		assertEquals(0, temp.getLongit(), delta);	
	}
}
