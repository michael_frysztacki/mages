package unit_testing.mapcom.worldstructure;

import mapcom.worldstructure.EdgesStructure;
import mapcom.worldstructure.EdgesStructureOnHashMaps;

public class EdgesHashMapOfHashMapsTest extends EdgesStructureTest
{	
	@Override
	public EdgesStructure createInstance()
	{
		return new EdgesStructureOnHashMaps();
	}
}
