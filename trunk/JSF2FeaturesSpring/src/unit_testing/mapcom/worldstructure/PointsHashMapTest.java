package unit_testing.mapcom.worldstructure;

import mapcom.worldstructure.PointsHashMap;
import mapcom.worldstructure.PointsStructure;

public class PointsHashMapTest extends PointsStructureTest
{
	@Override
	public PointsStructure createInstance()
	{
		return new PointsHashMap();
	}
}
