package unit_testing.mapcom.worldstructure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import mapcom.worldconstruction.PointsStructureExtractor;
import mapcom.worldconstruction.PointsStructureHelper;
import mapcom.worldconstruction.PointsStructureLoader;
import mapcom.worldconstruction.PointsStructureSaver;
import mapcom.worldstructure.Capital;
import mapcom.worldstructure.Colony;
import mapcom.worldstructure.InterPoint;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.PointsStructure;

import org.junit.Before;
import org.junit.Test;

public abstract class PointsStructureTest
{
	String testFilesDir = "src/unit_testing/mapcom/worldstructure/PointsStructure_Test_Files/";
	double delta = 0.00000000001;  // 10 trylionowych, o tyle conajwyzej moze byc blad w double'ach
	PointsStructure ps, ps1, ps2;
	PointsStructureHelper helper;
	PointsStructureSaver saver;
	PointsStructureLoader loader;
	
	/******************************************************************************************************************
	 *  PointsStructure TESTS 
	 *****************************************************************************************************************/	
	
	public abstract PointsStructure createInstance();
	
	@Before
	public void initialiaze()
	{		
		ps = createInstance();
		ps1 = createInstance();
		ps2 = createInstance();
		helper = new PointsStructureHelper();
		saver = new PointsStructureSaver();
		loader = new PointsStructureLoader();
	}
	
		private Point createPoint(double longit, double lat, int id)
		{
			return new Point(longit, lat, id, true, true);
		}
	
	@Test
	public void doesRetrievalFromPointsStructureWork()
	{
		int id = 1;
		ps.addPoint( createPoint(5, 6, id) );
		Point temp = ps.getPointWithId(id);			
		assertEquals(5, temp.getLongit(), delta);
		assertEquals(6, temp.getLat(), delta);
	}
	
	@Test
	public void returnNullIfPointDoesNotExist()
	{
		Point temp = ps.getPointWithId(0);
		assertNull(temp);
	}
	
	@Test
	public void removePointTest()
	{
		int id = 1;
		ps.addPoint( createPoint(0, 0, id));
		ps.remove(id);
		assertNull( ps.getPointWithId(id) );
		assertEquals(0, ps.size() );
	}
	
	@Test
	public void sizeEqualsOneAfterAddingOnePoint()
	{
		int id = 1;
		ps.addPoint( createPoint(0, 0, id));
		assertEquals( 1, ps.size() );
	}
	
	@Test
	public void sizeEqualsZeroWhenNoPointsWereAdded()
	{
		assertEquals( 0, ps.size() );
	}
	
	@Test
	public void sizeEqualsOneAfterAddingTwoPointsWithTheSameId()
	{
		int id = 1;
		ps.addPoint( createPoint(0, 0, id));
		ps.addPoint( createPoint(10, 10, id));
		assertEquals( 1, ps.size() );
	}
	
	@Test
	public void sizeTest() throws IOException
	{
		ps.addPoint(new Point(0, 0, 0, true, true));
		ps.addPoint(new Point(10, 10, 1, true, true));		
		ps.addPoint(new Point(20, 20, 2, true, true));		
		ps.addPoint(new Point(30, 30, 3, true, true));
		
		assertEquals(4, ps.size(), delta);		
	}
	
	@Test
	public void createArrayOfEmptyPointsStructuresTest()
	{
		PointsStructure[] psArray = ps.createArrayOfEmptyPointsStructures(5);
		assertEquals(5, psArray.length);
	}
	
	@Test
	public void waterPointsExtractionTest() throws IOException
	{
		ps.addPoint( createWaterPoint(0, 0, 0) );
		ps.addPoint( createLandPoint(10, 10, 1) );
		ps.addPoint( createLandPoint(20, 20, 2) );
		ps.addPoint( createWaterPoint(30, 30, 3) );
		ps.addPoint( createWaterPoint(40, 40, 4) );
		ps.addPoint( createLandPoint(50, 50, 5) );	
		
		PointsStructureExtractor extractor = new PointsStructureExtractor();
		PointsStructure waterPointsStructure = extractor.extractPointsForWaterGraph(ps);
		
		assertEquals(3, waterPointsStructure.size(), delta);
		assertEquals(0, waterPointsStructure.getPointWithId(0).getLongit(), delta );
		assertEquals(30, waterPointsStructure.getPointWithId(3).getLongit(), delta );
		assertEquals(40, waterPointsStructure.getPointWithId(4).getLongit(), delta );
	}	
	
	@Test
	public void landPointsExtractionTest() throws IOException
	{
		ps.addPoint( createWaterPoint(0, 0, 0) );
		ps.addPoint( createLandPoint(10, 10, 1) );
		ps.addPoint( createLandPoint(20, 20, 2) );
		ps.addPoint( createWaterPoint(30, 30, 3) );
		ps.addPoint( createWaterPoint(40, 40, 4) );
		ps.addPoint( createLandPoint(50, 50, 5) );			
		
		PointsStructureExtractor extractor = new PointsStructureExtractor();
		PointsStructure landPointsStructure = extractor.extractPointsForLandGraph(ps);
		
		assertEquals(3, landPointsStructure.size(), delta);
		assertEquals(10, landPointsStructure.getPointWithId(1).getLongit(), delta );
		assertEquals(20, landPointsStructure.getPointWithId(2).getLongit(), delta );
		assertEquals(50, landPointsStructure.getPointWithId(5).getLongit(), delta );
	}	
		private Point createWaterPoint(double longitude, double latitude, int id)
		{
			return new Point(longitude, latitude, id, true, false);
		}
		private Point createLandPoint(double longitude, double latitude, int id)
		{
			return new Point(longitude, latitude, id, false, false);
		}
	
	@Test
	public void extractCapitalsTest()
	{
		ps.addPoint(createCapital(0, 0, 0));
		ps.addPoint(createColony(10, 10, 1));
		ps.addPoint(createPoint(20, 20, 2));
		ps.addPoint(createCapital(30, 30, 3));
		ps.addPoint(createColony(40, 40, 4));
		ps.addPoint(createPoint(50, 50, 5));
		
		PointsStructureExtractor extractor = new PointsStructureExtractor();
		PointsStructure capitals = extractor.extractCapitals(ps);
		
		assertEquals(2, capitals.size());
		assertEquals(0, capitals.getPointWithId(0).getLongit(), delta );
		assertEquals(30, capitals.getPointWithId(3).getLongit(), delta );
	}
	@Test
	public void extractColoniesTest()
	{
		ps.addPoint(createCapital(0, 0, 0));
		ps.addPoint(createColony(10, 10, 1));
		ps.addPoint(createPoint(20, 20, 2));
		ps.addPoint(createCapital(30, 30, 3));
		ps.addPoint(createColony(40, 40, 4));
		ps.addPoint(createPoint(50, 50, 5));
		
		PointsStructureExtractor extractor = new PointsStructureExtractor();
		PointsStructure colonies = extractor.extractColonies(ps);
		
		assertEquals(2, colonies.size());
		assertEquals(10, colonies.getPointWithId(1).getLongit(), delta );
		assertEquals(40, colonies.getPointWithId(4).getLongit(), delta );
	}
	@Test
	public void extractInterPointsTest()
	{
		ps.addPoint(createCapital(0, 0, 0));
		ps.addPoint(createColony(10, 10, 1));
		ps.addPoint(createInterPoint(20, 20, 2));
		ps.addPoint(createCapital(30, 30, 3));
		ps.addPoint(createColony(40, 40, 4));
		ps.addPoint(createInterPoint(50, 50, 5));
		
		PointsStructureExtractor extractor = new PointsStructureExtractor();
		PointsStructure interPoints = extractor.extractInterPoints(ps);
		
		assertEquals(2, interPoints.size());
		assertEquals(20, interPoints.getPointWithId(2).getLongit(), delta );
		assertEquals(50, interPoints.getPointWithId(5).getLongit(), delta );
	}
		private Capital createCapital(double longitude, double latitude, int id)
		{
			return new Capital(longitude, latitude, id, "", true, true);
		}
		private Colony createColony(double longitude, double latitude, int id)
		{
			return new Colony(longitude, latitude, id, true, true);
		}
		private InterPoint createInterPoint(double longitude, double latitude, int id)
		{
			return new InterPoint(longitude, latitude, id, true, true);
		}
	
	@Test // 0010
	public void loadingInterPointsTest() throws IOException
	{
		ps = loader.load(ps, testFilesDir + "0010/");
		
		assertTrue( new InterPoint(4, -4, 4, false, true).equals(ps.getPointWithId(4)) );
		assertTrue( new InterPoint(5, -5, 5, false, true).equals(ps.getPointWithId(5)) );
		assertTrue( new InterPoint(6, -6, 6, false, true).equals(ps.getPointWithId(6)) );
	}
	
	@Test // 0020
	public void loadingAColonyAndInterPointsTest() throws IOException
	{
		ps = loader.load(ps, testFilesDir + "0020/");
		
		assertTrue( new Colony(3, -3, 3, false, true).equals(ps.getPointWithId(3)) );
		
		assertTrue( new InterPoint(4, -4, 4, false, true).equals(ps.getPointWithId(4)) );
		assertTrue( new InterPoint(5, -5, 5, false, true).equals(ps.getPointWithId(5)) );
		assertTrue( new InterPoint(6, -6, 6, false, true).equals(ps.getPointWithId(6)) );
	}
	
	@Test // 0030
	public void loadingCapitalsAColonyAndInterPointsTest() throws IOException
	{
		ps = loader.load(ps, testFilesDir + "0030/");
		
		assertTrue( new Capital(0, 0, 0, "a", false, true).equals(ps.getPointWithId(0)) );
		assertTrue( new Capital(1, -1, 1, "b", false, true).equals(ps.getPointWithId(1)) );
		assertTrue( new Capital(2, -2, 2, "c", false, true).equals(ps.getPointWithId(2)) );
		
		assertTrue( new Colony(3, -3, 3, false, true).equals(ps.getPointWithId(3)) );
		
		assertTrue( new InterPoint(4, -4, 4, false, true).equals(ps.getPointWithId(4)) );
		assertTrue( new InterPoint(5, -5, 5, false, true).equals(ps.getPointWithId(5)) );
		assertTrue( new InterPoint(6, -6, 6, false, true).equals(ps.getPointWithId(6)) );
	}	
	

	/******************************************************************************************************************
	 *  saving loading contract TESTS 
	 *****************************************************************************************************************/

	@Test // 0039
	public void checkingSavingLoadingTest0039() throws IOException
	{
		ps1 = loader.load(ps1, testFilesDir + "0039/Before/");		
		saver.save(ps1, testFilesDir + "0039/After/");				
		ps2 = loader.load(ps1, testFilesDir + "0039/After/");		
				
		assertTrue( helper.containTheSamePoints(ps1, ps2) );		
	}	

	@Test // 0040
	public void checkingSavingLoadingTest0040() throws IOException
	{
		ps1 = loader.load(ps1, testFilesDir + "0040/Before/");	
		saver.save(ps1, testFilesDir + "0040/After/");				
		ps2 = loader.load(ps1, testFilesDir + "0040/After/");		
		
		assertTrue( helper.containTheSamePoints(ps1, ps2) );		
	}	

	
}