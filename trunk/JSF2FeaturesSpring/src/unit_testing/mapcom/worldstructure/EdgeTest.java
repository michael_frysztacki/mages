package unit_testing.mapcom.worldstructure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import mapcom.imgproc.Colors;
import mapcom.mapprojections.Equirectangular;
import mapcom.worldstructure.Edge;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.TerrainType;

import org.junit.Test;

import unit_testing.mapcom.testing_utils.EdgeForTestFactory;
import unit_testing.mapcom.testing_utils.PointForTestFactory;

public class EdgeTest
{
	
	double delta = 0.00000000001;  // 10 trylionowych, o tyle conajwyzej moze byc blad w double'ach
	Edge e, e1, e2;
	Point p1, p2, p3, p4;
	
	
	String path = "src/unit_testing/mapcom/worldstructure/EdgeTest_Files/";
	
	
	@Test 
	public void equalsForTheSameEdges(){
		p1 = PointForTestFactory.createPoint(0);
		p2 = PointForTestFactory.createPoint(1);
		
		e1 = new Edge(p1, p2, new TerrainType[]{}, new float[]{});
		e2 = new Edge(p1, p2, new TerrainType[]{}, new float[]{});
		
		assertTrue( e1.equals(e2) );
		assertTrue( e2.equals(e1) );
	}
	
	// According to documentation, none of ending points of the Edge object can be set to NULL.
	@Test( expected = NullPointerException.class )
	public void endingPointsOfEdgesCannotBeNull(){
		p1 = PointForTestFactory.createPoint(0);
		p2 = PointForTestFactory.createPoint(1);
				
		e1 = new Edge(null, p2, new TerrainType[]{}, new float[]{});
		e2 = new Edge(p1, p2, new TerrainType[]{}, new float[]{});
		
		assertFalse( e1.equals(e2) );
		assertFalse( e2.equals(e1) );
	}
	
	@Test 
	public void edgesWithDifferentPointsAreNotEqual(){
		p1 = PointForTestFactory.createPoint(0);
		p2 = PointForTestFactory.createPoint(1);
		p3 = PointForTestFactory.createPoint(2);
				
		e1 = new Edge(p1, p2, new TerrainType[]{}, new float[]{});
		e2 = new Edge(p1, p3, new TerrainType[]{}, new float[]{});
		
		assertFalse( e1.equals(e2) );
		assertFalse( e2.equals(e1) );
	}
	
	@Test 
	public void edgesWithDifferentTerrainTypesAreNotEqual(){
		p1 = PointForTestFactory.createPoint(0);
		p2 = PointForTestFactory.createPoint(1);
				
		e1 = new Edge(p1, p2, new TerrainType[]{TerrainType.Steppe}, new float[]{1.0f});
		e2 = new Edge(p1, p2, new TerrainType[]{TerrainType.Desert}, new float[]{1.0f});
		
		assertFalse( e1.equals(e2) );
		assertFalse( e2.equals(e1) );
	}
	
	@Test 
	public void edgesWithReversedTerrainTypesAreNotEqual(){
		p1 = PointForTestFactory.createPoint(0);
		p2 = PointForTestFactory.createPoint(1);
				
		e1 = new Edge(p1, p2, new TerrainType[]{TerrainType.Steppe, TerrainType.Desert}, new float[]{0.5f, 0.5f});
		e2 = new Edge(p1, p2, new TerrainType[]{TerrainType.Desert, TerrainType.Steppe}, new float[]{0.5f, 0.5f});
		
		assertFalse( e1.equals(e2) );
		assertFalse( e2.equals(e1) );
	}
	
	@Test 
	public void edgesWithDifferentPercentagesArraysNotEqual(){
		p1 = PointForTestFactory.createPoint(0);
		p2 = PointForTestFactory.createPoint(1);
				
		e1 = new Edge(p1, p2, new TerrainType[]{TerrainType.Desert, TerrainType.Steppe}, new float[]{0.1f, 0.9f});
		e2 = new Edge(p1, p2, new TerrainType[]{TerrainType.Desert, TerrainType.Steppe}, new float[]{0.5f, 0.5f});
		
		assertFalse( e1.equals(e2) );
		assertFalse( e2.equals(e1) );
	}
	
	@Test 
	public void edgesWithReversedPercentagesArraysNotEqual(){
		p1 = PointForTestFactory.createPoint(0);
		p2 = PointForTestFactory.createPoint(1);
				
		e1 = new Edge(p1, p2, new TerrainType[]{TerrainType.Desert, TerrainType.Steppe}, new float[]{0.6f, 0.4f});
		e2 = new Edge(p1, p2, new TerrainType[]{TerrainType.Desert, TerrainType.Steppe}, new float[]{0.4f, 0.6f});
		
		assertFalse( e1.equals(e2) );
		assertFalse( e2.equals(e1) );
	}
	
	@Test 
	public void toStringTest(){
		p1 = new Point(1, 2, 0, true, true);
		p2 = new Point(4, 5, 3, true, true);
		TerrainType[] terrains = new TerrainType[]{TerrainType.Desert, TerrainType.Jungle};
		float[] percentages = new float[]{0.4f, 0.6f};
		e = new Edge(p1, p2, terrains, percentages);
		
		String expected = "Edge:" + "\n" +  
				          "start point: [Point: id=0; (1.0, 2.0); water; polar;]" + "\n" +
						  "end point: [Point: id=3; (4.0, 5.0); water; polar;]" + "\n" +
				          "terrain types: [Desert, Jungle]" + "\n" +
						  "percentages: [0.4, 0.6]" + "\n";
		
		assertEquals( expected, e.toString() );
	}
	
	@Test
	public void TestEdge0010()
	{
		// mapa 6x8 lewa polowa zamalowana na kolor wody, prawa na kolor bialy
		Equirectangular proj = new Equirectangular(	path + "0010/TwoColorMap(Water_Plain)6x8.png");
		Point A = new Point(-60, 0);
		Point B = new Point(60, 0);
		Edge edge = new Edge(0, 1);
		edge.getEdgeTypes(A, B, proj);
		
		assertEquals(2, edge.getTypesOfTerrTrav().length);
		assertEquals(2, edge.getPercentagesOfTerrTrav().length);
		assertEquals(0.5f, edge.getPercentagesOfTerrTrav()[0], delta);
		assertEquals(0.5f, edge.getPercentagesOfTerrTrav()[1], delta);
		assert(Colors.terrainTypeToRGB(edge.getTypesOfTerrTrav()[0]) == Colors.getWaterColor().getRGB());
		assert(Colors.terrainTypeToRGB(edge.getTypesOfTerrTrav()[1]) == Colors.getPlainColor().getRGB());		
	}
	@Test
	public void TestEdge0020()
	{
		// mapa 12x8 podzielona na 3 kwadraty o roznych kolorach
		Equirectangular proj = new Equirectangular(path + "0020/ThreeColorMap(Mountain_Water_Steppe)12x4.png");
		Point A = new Point(-75, 0);
		Point B = new Point(75, 0);
		Edge edge = new Edge(0, 1);
		edge.getEdgeTypes(A, B, proj);
		
		assertEquals(3, edge.getTypesOfTerrTrav().length);
		assertEquals(3, edge.getPercentagesOfTerrTrav().length);
		assertEquals(0.16666666666666f, edge.getPercentagesOfTerrTrav()[0], delta);
		assertEquals(0.66666666666666f, edge.getPercentagesOfTerrTrav()[1], delta);
		assertEquals(0.16666666666666f, edge.getPercentagesOfTerrTrav()[2], delta);
		assert(Colors.terrainTypeToRGB(edge.getTypesOfTerrTrav()[0]) == Colors.getMountainColor().getRGB());
		assert(Colors.terrainTypeToRGB(edge.getTypesOfTerrTrav()[1]) == Colors.getWaterColor().getRGB());
		assert(Colors.terrainTypeToRGB(edge.getTypesOfTerrTrav()[2]) == Colors.getSteppeColor().getRGB());
	}
	@Test
	public void TestEdge0030()
	{
		// mapa 12x8 podzielona na 3 kwadraty o roznych kolorach
		Equirectangular proj = new Equirectangular(path + "0030/ThreeColorMap(Mountain_Steppe(short strip)_Water_Steppe)12x4.png");
		Point A = new Point(-75, 0);
		Point B = new Point(75, 0);
		Edge edge = new Edge(0, 1);
		edge.getEdgeTypes(A, B, proj);
		
		assertEquals(5, edge.getTypesOfTerrTrav().length);
		assertEquals(5, edge.getPercentagesOfTerrTrav().length);
		assertEquals(0.16666666666666f, edge.getPercentagesOfTerrTrav()[0], delta);
		assertEquals(0.16666666666666f, edge.getPercentagesOfTerrTrav()[1], delta);
		assertEquals(0.16666666666666f, edge.getPercentagesOfTerrTrav()[2], delta);
		assertEquals(0.33333333333333f, edge.getPercentagesOfTerrTrav()[3], delta);
		assertEquals(0.16666666666666f, edge.getPercentagesOfTerrTrav()[4], delta);
		assert(Colors.terrainTypeToRGB(edge.getTypesOfTerrTrav()[0]) == Colors.getMountainColor().getRGB());
		assert(Colors.terrainTypeToRGB(edge.getTypesOfTerrTrav()[1]) == Colors.getWaterColor().getRGB());
		assert(Colors.terrainTypeToRGB(edge.getTypesOfTerrTrav()[2]) == Colors.getSteppeColor().getRGB());
		assert(Colors.terrainTypeToRGB(edge.getTypesOfTerrTrav()[3]) == Colors.getWaterColor().getRGB());
		assert(Colors.terrainTypeToRGB(edge.getTypesOfTerrTrav()[4]) == Colors.getSteppeColor().getRGB());
	}
		
}
