package unit_testing.mapcom.worldstructure;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Iterator;

import mapcom.worldconstruction.EdgesStructureHelper;
import mapcom.worldconstruction.EdgesStructureLoader;
import mapcom.worldconstruction.EdgesStructureSaver;
import mapcom.worldconstruction.EdgesStructureStatistician;
import mapcom.worldstructure.Edge;
import mapcom.worldstructure.EdgesStructure;
import mapcom.worldstructure.Point;

import org.junit.Before;
import org.junit.Test;

import unit_testing.mapcom.testing_utils.EdgeForTestFactory;
import unit_testing.mapcom.testing_utils.PointForTestFactory;
import unit_testing.mapcom.testing_utils.TestHelper;

public abstract class EdgesStructureTest
{
	EdgesStructure es, es1, es2;
	Point tempPoint;
	Edge tempEdge;
	Iterator<Edge> innerIter;
	Iterator<Iterator<Edge>> outerIter;
	EdgesStructureSaver saver;
	EdgesStructureLoader loader;
	EdgesStructureHelper helper;
	
	boolean draw = true;
	int pointThickness = 2;
	int lineThickness = 0;
	String testFilesDir = "src/unit_testing/mapcom/worldstructure/EdgesStructureTest_Files/";
	double delta = 0.00000000001;  // 10 trylionowych, o tyle conajwyzej moze byc blad w double'ach
	
	public abstract EdgesStructure createInstance();
	
	@Before
	public void initialize()
	{
		tempPoint = null;
		tempEdge = null;
		innerIter = null;
		outerIter = null;
		es = createInstance();
		es1 = createInstance();
		es2 = createInstance();
		saver = new EdgesStructureSaver();
		loader = new EdgesStructureLoader();
		helper = new EdgesStructureHelper();
	}	
		private Edge createEdge(Point start, Point end)
		{
			return EdgeForTestFactory.createEdge(start, end);
		}
	
	
	/******************************************************************************************************************
	 *  basic operations on edgesStructure
	 *****************************************************************************************************************/

	static Point createPoint(int id)
	{
		return PointForTestFactory.createPoint(id);
	}
	@Test // 0010
	public void addAndThenGetEdgeTest()
	{	
		es.addEdgeAndReversedEdge(createEdge(createPoint(1), createPoint(0)));
		
		tempEdge = es.getEdge(1, 0);
		tempPoint = tempEdge.getStartPoint();
		assertEquals(1, tempPoint.getId());
		
		tempEdge = es.getEdge(0, 1);
		tempPoint = tempEdge.getStartPoint();
		assertEquals(0, tempPoint.getId());
	}
	@Test // 0020
	public void addEdgeAndCheckNumberOfEdgesTest()
	{	
		es.addEdgeAndReversedEdge(createEdge(createPoint(1), createPoint(0)));		
		assertEquals(2, EdgesStructureStatistician.numberOfAllEdges(es));
	}
	@Test // 0030
	public void addEdgeAndRemoveEdgeTest()
	{	
		es.addEdgeAndReversedEdge(createEdge(createPoint(1), createPoint(0)));
		es.removeEdgeAndReversedEdge(1, 0);
		assertNull(es.getEdge(0, 1));
		assertNull(es.getEdge(1, 0));
		assertEquals(0, EdgesStructureStatistician.numberOfAllEdges(es));
	}
	@Test // 0035
	public void removeEdgeFromEmptyEdgeStructure()
	{			
		es.removeEdgeAndReversedEdge(1, 0);
		assertEquals(0, EdgesStructureStatistician.numberOfAllEdges(es));
	}	
	@Test // 0040
	public void addEdgeAndRemoveReversedEdgeTest()
	{	
		es.addEdgeAndReversedEdge(createEdge(createPoint(1), createPoint(0)));
		es.removeEdgeAndReversedEdge(0, 1);
		assertNull(es.getEdge(0, 1));
		assertNull(es.getEdge(1, 0));
		assertEquals(0, EdgesStructureStatistician.numberOfAllEdges(es));
	}
	@Test // 0044
	public void addEdgesStartingInTheSamePointTest()
	{
		es.addEdgeAndReversedEdge(createEdge(createPoint(1), createPoint(0)));
		es.addEdgeAndReversedEdge(createEdge(createPoint(1), createPoint(2)));
		
		assertEquals(4, EdgesStructureStatistician.numberOfAllEdges(es));
		
		assertEquals(2, EdgesStructureStatistician.numberOfEdgesStartingInPoint(es, 1));
		assertEquals(1, EdgesStructureStatistician.numberOfEdgesStartingInPoint(es, 0));
		assertEquals(1, EdgesStructureStatistician.numberOfEdgesStartingInPoint(es, 2));		
	}
	@Test // 0046
	public void addEdgesStartingInTheSamePointAndRemoveOneOfThemTest()
	{
		es.addEdgeAndReversedEdge(createEdge(createPoint(1), createPoint(0)));
		es.addEdgeAndReversedEdge(createEdge(createPoint(1), createPoint(2)));
		es.removeEdgeAndReversedEdge(1, 2);
		
		assertEquals(2, EdgesStructureStatistician.numberOfAllEdges(es));
		
		assertEquals(1, EdgesStructureStatistician.numberOfEdgesStartingInPoint(es, 1));
		assertEquals(1, EdgesStructureStatistician.numberOfEdgesStartingInPoint(es, 0));
		assertEquals(0, EdgesStructureStatistician.numberOfEdgesStartingInPoint(es, 2));
	}	
	@Test // 0050
	public void edgeExistInEmptyEdgesStructureTest()
	{
		assertFalse(es.exists(0, 1));
	}
	@Test // 0055
	public void edgeExistTest()
	{
		es.addEdgeAndReversedEdge(createEdge(createPoint(1), createPoint(0)));
		es.addEdgeAndReversedEdge(createEdge(createPoint(2), createPoint(0)));
		assertTrue(es.exists(0, 1));
		assertTrue(es.exists(1, 0));
		assertTrue(es.exists(2, 0));
		assertTrue(es.exists(0, 2));
		assertFalse(es.exists(0, 4));		
	}
	@Test // 0060
	public void innerIteratorOnEmptyStructureTest()
	{
		innerIter = es.innerIterator(0);
		assertEquals(0, TestHelper.countTraversedByIterator(innerIter));
	}
	@Test // 0070
	public void outerIteratorOnEmptyStructureTest()
	{
		outerIter = es.outerIterator();
		assertEquals(0, TestHelper.countTraversedByIterator(outerIter));
	}
	@Test // 0080
	public void innerIteratorOnStructureWithTwoEdgesTest()
	{		
		es.addEdgeAndReversedEdge(createEdge(createPoint(7), createPoint(11)));
		
		innerIter = es.innerIterator(7);
		assertEquals(1, TestHelper.countTraversedByIterator(innerIter));		
		innerIter = es.innerIterator(11);
		assertEquals(1, TestHelper.countTraversedByIterator(innerIter));
	}	
	@Test // 0090
	public void iterateStructureWithTwoEdgesTest()
	{
		es.addEdgeAndReversedEdge(createEdge(createPoint(7), createPoint(11)));
		
		Iterator<Iterator<Edge>> outerIter = es.outerIterator();		
		assertEquals(2, TestHelper.countTraversedByIterator(outerIter));
	}
	@Test // 0095
	public void iterateStructureWithFourEdgesTest()
	{
		es.addEdgeAndReversedEdge(createEdge(createPoint(7), createPoint(11)));
		es.addEdgeAndReversedEdge(createEdge(createPoint(7), createPoint(13)));
		
		Iterator<Iterator<Edge>> outerIter = es.outerIterator();		
		assertEquals(3, TestHelper.countTraversedByIterator(outerIter));
	}
	@Test // 0100
	public void iterateThroughPointWithoutNeighboursOnNonEmptyEdgeStructure()
	{
		es.addEdgeAndReversedEdge(createEdge(createPoint(7), createPoint(11)));
		
		innerIter = es.innerIterator(13);
		assertEquals(0, TestHelper.countTraversedByIterator(innerIter));
	}
	
	

	/******************************************************************************************************************
	 *  Loading/Saving Contract edgesStructure	  
	 *****************************************************************************************************************/	
	
	@Test
	public void saveLoadContractVerificationOnSimpleEdgesStructureTest() throws IOException{
		es1.addEdgeAndReversedEdge(EdgeForTestFactory.createEdge(1, 2));
		es1.addEdgeAndReversedEdge(EdgeForTestFactory.createEdge(1, 3));
		
		saver.saveEdges(es1, testFilesDir + "EdgesStructure.ser");
		loader.loadEdges(es2, testFilesDir + "EdgesStructure.ser");		
		
		assertTrue( helper.containTheSameEdges(es1, es2) );
	}	
	@Test
	public void saveLoadContractVerificationOnMediumEdgesStructureTestTest() throws IOException{
		es1.addEdgeAndReversedEdge(EdgeForTestFactory.createEdge(1, 2));
		es1.addEdgeAndReversedEdge(EdgeForTestFactory.createEdge(1, 3));
		
		es1.addEdgeAndReversedEdge(EdgeForTestFactory.createEdge(2, 4));
		es1.addEdgeAndReversedEdge(EdgeForTestFactory.createEdge(2, 5));
		
		saver.saveEdges(es1, testFilesDir + "EdgesStructure.ser");
		loader.loadEdges(es2, testFilesDir + "EdgesStructure.ser");		
		
		assertTrue( helper.containTheSameEdges(es1, es2) );
	}
	@Test
	public void saveLoadContractVerificationOnBigEdgesStructureTestTest() throws IOException{
		es1.addEdgeAndReversedEdge(EdgeForTestFactory.createEdge(1, 2));
		es1.addEdgeAndReversedEdge(EdgeForTestFactory.createEdge(1, 3));
		
		es1.addEdgeAndReversedEdge(EdgeForTestFactory.createEdge(2, 4));
		es1.addEdgeAndReversedEdge(EdgeForTestFactory.createEdge(2, 5));
		
		es1.addEdgeAndReversedEdge(EdgeForTestFactory.createEdge(3, 4));
		es1.addEdgeAndReversedEdge(EdgeForTestFactory.createEdge(3, 5));
		
		saver.saveEdges(es1, testFilesDir + "EdgesStructure.ser");
		loader.loadEdges(es2, testFilesDir + "EdgesStructure.ser");		
		
		assertTrue( helper.containTheSameEdges(es1, es2) );
	}
	
}















