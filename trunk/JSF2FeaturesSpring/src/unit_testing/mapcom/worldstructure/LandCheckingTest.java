package unit_testing.mapcom.worldstructure;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import mapcom.imgproc.ImgProc;
import mapcom.imgproc.PointDrawing;
import mapcom.mapprojections.Equirectangular;
import mapcom.mapprojections.MapProjection;
import mapcom.worldconstruction.LandChecking;
import mapcom.worldstructure.Capital;
import mapcom.worldstructure.Colony;
import mapcom.worldstructure.Point;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import unit_testing.mapcom.testing_utils.PointForTestFactory;
import unit_testing.mapcom.testing_utils.TestHelper;

public class LandCheckingTest {
	
	private static String testsDirectory = TestHelper.testsDirectory + "worldstructure/LandCheckingTest_Files/";
	private static String  testsResultsDirectory = testsDirectory + "Results/";	
	
	private static LandChecking checking;
	private Point interPointOnWater, itnerPointOnLand;
	private Colony colonyOnWater;
	private Capital capitalOnWater;	
	private double longitude, latitude;
	private static MapProjection proj;
	
	@BeforeClass
	public static void setUp(){
		checking = new LandChecking();
		initializeMapProjection();
		TestHelper.deleteContentOfDirectoryExceptGitIgnoreFile(testsResultsDirectory);
	}
	
	@Before
	public void initialize(){
		initializeMapProjection();
		longitude = -179;
		latitude = 0;
		interPointOnWater = PointForTestFactory.createWaterPoint(longitude, latitude);
		itnerPointOnLand = PointForTestFactory.createLandPoint();
		colonyOnWater = PointForTestFactory.createWaterColony();
		capitalOnWater = PointForTestFactory.createWaterCapital();
	}	
	
	private static void initializeMapProjection(){
		proj = new Equirectangular(TestHelper.tinyMapPath);
	}
	
	@Test
	public void pointOnWaterIsNotOk() {				
		drawResults("0010", interPointOnWater);
		initializeMapProjection();
		
		assertFalse( checking.checkSurfaceOnSpot(proj, interPointOnWater) );
	}
	@Test
	public void pointOnLandIsOk() {		
		Point spotOfWarsaw = PointForTestFactory.createLandPoint(TestHelper.warsawLongitude, TestHelper.warsawLatitude);			
		drawResults("0020", spotOfWarsaw);
		initializeMapProjection();
		
		assertTrue( checking.checkSurfaceOnSpot(proj, spotOfWarsaw) );
	}		
		private void drawResults(String testNumber, Point toDraw){
			PointDrawing.drawPoint(toDraw, proj);
			ImgProc.saveImage(proj.getImg(), testsResultsDirectory + testNumber+ ".jpg" );
		}
		
	@Test
	public void waterInterPointCannotBelongToLandEdge(){
		assertFalse( checking.mightBelongToEdge(interPointOnWater) );		
	}
	@Test
	public void waterCapitalMightBelongToLandEdge(){
		assertTrue( checking.mightBelongToEdge(capitalOnWater) );		
	}
	@Test
	public void waterColonyMightBelongToLandEdge(){
		assertTrue( checking.mightBelongToEdge(colonyOnWater) );		
	}
	@Test
	public void landInterPointMightBelongToLandEdge(){
		assertTrue( checking.mightBelongToEdge(itnerPointOnLand) );		
	}

}
