package unit_testing.mapcom.worldconstruction;

import static org.junit.Assert.*;

import mapcom.imgproc.ImgProc;
import mapcom.imgproc.PointDrawing;
import mapcom.mapprojections.Equirectangular;
import mapcom.mapprojections.MapProjection;
import mapcom.worldconstruction.PointsVerifier;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.PointsHashMap;
import mapcom.worldstructure.PointsStructure;

import org.junit.Test;

public class PointsVerifierTest
{
	String pathToTestFiles = "src/unit_testing/mapcom/worldconstruction/PointsVerifier_Test_Files/";
	
	

	
	/******************************************************************************************************************
	 *  switchSurfaceTypeOfIncorrectPoints Method TEST 
	 *****************************************************************************************************************/

	@Test
	public void switchSurfaceTypeOfIncorrectPoints_Test_0010()
	{
		MapProjection proj = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");		
		Point tempPoint;
		PointsStructure points = new PointsHashMap();
		tempPoint = new Point(0, 0, 0, false, true);
		points.addPointWithId(tempPoint, tempPoint.getId());
		tempPoint = new Point(1, 1, 1, true, true);
		points.addPointWithId(tempPoint, tempPoint.getId());
		tempPoint = new Point(90, -50, 2, true, true);
		points.addPointWithId(tempPoint, tempPoint.getId());
		tempPoint = new Point(89, -49, 3, false, true);
		points.addPointWithId(tempPoint, tempPoint.getId());
		
		// potrzebujemy oddzielnej mapy do rysowania
		MapProjection proj1 = new Equirectangular(pathToTestFiles + "Maps/terrains_1350x675.png");
		drawPoints(points, proj1, pathToTestFiles + "0010a.png");
		
		PointsVerifier verifier = new PointsVerifier();
		verifier.switchSurfaceTypeOfIncorrectPoints(points, proj);
		
		assertTrue(verifier.isSurfaceOfPointCorrect(points.getPointWithId(0), proj));
		assertTrue(verifier.isSurfaceOfPointCorrect(points.getPointWithId(1), proj));
		assertTrue(verifier.isSurfaceOfPointCorrect(points.getPointWithId(2), proj));
		assertTrue(verifier.isSurfaceOfPointCorrect(points.getPointWithId(3), proj));
		assertTrue( points.getPointWithId(0).isWater() );
		assertTrue( points.getPointWithId(1).isWater() );
		assertTrue( !points.getPointWithId(2).isWater() );
		assertTrue( !points.getPointWithId(3).isWater() );
		
		drawPoints(points, proj1, pathToTestFiles + "0010b.png");
	}
	
	private void drawPoints(PointsStructure points, MapProjection proj, String outputFileName)
	{
		PointDrawing.drawPoints(points, proj);
		ImgProc.saveImage(proj.getImg(), outputFileName);
	}

	
	
	
	
}
