package unit_testing.mapcom.worldconstruction;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

import java.awt.image.BufferedImage;

import mapcom.imgproc.Colors;
import mapcom.mapprojections.Equirectangular;
import mapcom.worldconstruction.EdgeBuilder;
import mapcom.worldconstruction.LandChecking;
import mapcom.worldconstruction.WaterChecking;
import mapcom.worldstructure.Point;

import org.junit.Before;
import org.junit.Test;

import unit_testing.mapcom.testing_utils.PointForTestFactory;
import unit_testing.mapcom.testing_utils.TestHelper;

public class EdgeBuilderTest
{
	private String testDirectory = TestHelper.testsDirectory + "worldconstruction/EdgeBuilderTest_Files/";
	private String testsResultsDirectory = testDirectory + "Results/";
	private Equirectangular map = new Equirectangular(TestHelper.tinyMapPath);
	private WaterChecking waterChecking = new WaterChecking();
	private LandChecking landChecking = new LandChecking();
	private EdgeBuilder builder = new EdgeBuilder();
	private BufferedImage img;
	private MapProjection proj;
	Edge e;
	private Point point1, point2;
	
	
	/******************************************************************************************************************
	 *  IfEdgeOnTheColor Method TEST 
	 *****************************************************************************************************************/

	
	@Before
	public void initialize(){
		TestHelper.deleteContentOfDirectoryExceptGitIgnoreFile(testsResultsDirectory);
	}
	
	@Test
	public void testCheckIfEdgeIsOfSurfaceType0010()	{
		// comment
		point1 = PointForTestFactory.createPoint(-180.0, -45.0);
		point2 = PointForTestFactory.createPoint(-180.0, 0.0);		
		
		assertTrue( builder.checkIfEdgeIsOfSurfaceType(map, point1, point2, waterChecking) );
	}	
	@Test
	public void testCheckIfEdgeIsOfSurfaceType0011()	{
		point1 = PointForTestFactory.createPoint(179.0, -45.0);
		point2 = PointForTestFactory.createPoint(180.0, 0.0);
		assertTrue( builder.checkIfEdgeIsOfSurfaceType(map, point1, point2, waterChecking) );
	}	
	@Test
	public void testCheckIfEdgeIsOfSurfaceType0020(){
		point1 = PointForTestFactory.createPoint(180.0, -45.0);
		point2 = PointForTestFactory.createPoint(180.0, 0.0);
		
		assertTrue( builder.checkIfEdgeIsOfSurfaceType(map, point1, point2, waterChecking) );
	}
	@Test
	public void testCheckIfEdgeIsOfSurfaceType0030(){
		point1 = PointForTestFactory.createPoint(-180.0, 0.0);
		point2 = PointForTestFactory.createPoint(179.0, 0.0);
		
		assertTrue( builder.checkIfEdgeIsOfSurfaceType(map, point1, point2, waterChecking) );
	}
	
	@Test
	public void testCheckIfEdgeIsOfSurfaceType0040(){
		point1 = PointForTestFactory.createPoint(-180.0, -45.0);
		point2 = PointForTestFactory.createPoint(-180.0, 0.0);
				
		assertTrue( !builder.checkIfEdgeIsOfSurfaceType(map, point1, point2, landChecking) );
	}	
	@Test
	public void testCheckIfEdgeIsOfSurfaceType0050(){
		point1 = PointForTestFactory.createPoint(179.0, -45.0);
		point2 = PointForTestFactory.createPoint(180.0, 0.0);
		assertTrue( !builder.checkIfEdgeIsOfSurfaceType(map, point1, point2, landChecking) );
	}	
	@Test
	public void testCheckIfEdgeIsOfSurfaceType0060(){
		point1 = PointForTestFactory.createPoint(180.0, -45.0);
		point2 = PointForTestFactory.createPoint(180.0, 0.0);
		
		assertTrue( !builder.checkIfEdgeIsOfSurfaceType(map, point1, point2, landChecking) );
	}
	@Test
	public void testCheckIfEdgeIsOfSurfaceType0070(){
		point1 = PointForTestFactory.createPoint(-180.0, 0.0);
		point2 = PointForTestFactory.createPoint(179.0, 0.0);
		
		assertTrue( !builder.checkIfEdgeIsOfSurfaceType(map, point1, point2, landChecking) );
	}


	private int plain = Colors.getPlainColor();
	private int jungle = Colors.getJungleColor();
	private int desert = Colors.getDesertColor();
	private int mountain = Colors.getMountainColor();
	
	@Test
	public void verifyThatEdgeHaveCorrectTerrainsAndPercentages(){
		img = mock(BufferedImage.class);
		
		//  The mocked BufferedImage. Pixel have colors as follows:
		// -180             -90                  0                   90				 180
		//  plain plain plain plain desert desert Jungle Jungle plain plain plain plain        
		//  plain plain plain plain desert desert Jungle Jungle plain plain plain plain		
		mockImg(new int[][]{ {plain, plain, plain , plain, desert, jungle, jungle, desert, plain, plain, plain , plain},
							 {plain, plain, plain , plain, desert, jungle, jungle, desert, plain, plain, plain , plain} },
						    img);		
	  	proj = mock(Equirectangular.class);		
	  	when(proj.apprStep(point1, point2)).thenReturn(0.0001);
	  	when(proj.getImg()).thenReturn(img);
		point1 = PointForTestFactory.createPoint(-89, 0);
		point2 = PointForTestFactory.createPoint(89, 0);		
		e = landChecking.createEdge(point1, point2, proj);
		
		assertEquals(TerrainType.Plain, e.getTerrainType(0));
		assertEquals(TerrainType.Desert, e.getTerrainType(1));
		assertEquals(TerrainType.Jungle, e.getTerrainType(2));
		assertEquals(TerrainType.Plain, e.getTerrainType(3));
		
		
	}
	
	
	
	
	
}
