package unit_testing.mapcom.worldconstruction;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import mapcom.worldconstruction.EdgesStructureHelper;
import mapcom.worldstructure.EdgesStructure;
import mapcom.worldstructure.EdgesStructureOnHashMaps;
import mapcom.worldstructure.Point;

import org.junit.Before;
import org.junit.Test;

import unit_testing.mapcom.testing_utils.EdgeForTestFactory;

public class EdgesStructureHelperTest {

	EdgesStructureHelper helper;
	EdgesStructure es1;
	EdgesStructure es2;
	Point point1, point2, point3, point4, point5;
	
	@Before
	public void initialize(){
		es1 = new EdgesStructureOnHashMaps();
		es2 = new EdgesStructureOnHashMaps();
		helper = new EdgesStructureHelper();
	}
	
	@Test
	public void emptyEdgesStructureContainTheSameEdges() {		
		assertTrue(helper.containTheSameEdges(es1, es2));
	}

	@Test
	public void oneOfStructureEmptyAndSecondContainAnEdge() {
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(1, 0) );
		assertFalse(helper.containTheSameEdges(es1, es2));
	}

	@Test
	public void bothEdgesStructuresContainOneEdgeButSlightlyDifferentEdges() {		
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 1) );
		es2.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 2) );
		assertFalse(helper.containTheSameEdges(es1, es2));
	}
	
	@Test
	public void bothEdgesStructuresContainOneEdgeAndTheseAreTheSameEdges() {		
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 1) );
		es2.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 1) );		
		assertTrue(helper.containTheSameEdges(es1, es2));
	}
	
	@Test
	public void bothEdgesStructuresContainTheSameEdges() {		
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 1) );
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 2) );
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(3, 4) );
		
		es2.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 1) );
		es2.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 2) );
		es2.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(3, 4) );
		
		assertTrue(helper.containTheSameEdges(es1, es2));
	}
	
	@Test
	public void bothEdgesStructuresAreSlighltyDifferent() {		
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 1) );
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 2) );
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(3, 4) );
		
		es2.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 1) );
		es2.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 3) );
		es2.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(3, 4) );
		
		assertFalse(helper.containTheSameEdges(es1, es2));
	}
	
	
	
	@Test
	public void copyingEmptyStructureContent(){		
		helper.copyContent(es1, es2);
		assertTrue(helper.containTheSameEdges(es1, es2));
	}
	@Test
	public void copyingEdgesStructureWithOneEdge(){
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 1) );
		helper.copyContent(es1, es2);
		assertTrue(helper.containTheSameEdges(es1, es2));
	}
	@Test
	public void copyingEdgesStructureEdgeInStarShape(){
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 1) );
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 2) );
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 3) );
		helper.copyContent(es1, es2);
		assertTrue(helper.containTheSameEdges(es1, es2));
	}
	@Test
	public void copyingEdgesStructureEdgeWithTwoStarsShapes(){
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 1) );
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 2) );
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(0, 3) );
		
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(4, 5) );
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(4, 6) );
		es1.addEdgeAndReversedEdge( EdgeForTestFactory.createEdge(4, 7) );
		
		helper.copyContent(es1, es2);
		assertTrue(helper.containTheSameEdges(es1, es2));
	}
	
}


















