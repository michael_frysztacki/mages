package unit_testing.mapcom.worldconstruction;

import static org.junit.Assert.*;


import mapcom.worldconstruction.EdgesPerformer;
import mapcom.worldstructure.Edge;
import mapcom.worldstructure.EdgesHashMapOfHashMaps;
import mapcom.worldstructure.EdgesStructure;
import mapcom.worldstructure.TerrainType;

import org.junit.Test;

public class EdgesStructurePerformerTest
{

	/******************************************************************************************************************
	 *  countEdges Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void countEdges_Test_0009()
	{
		EdgesStructure edges = new EdgesHashMapOfHashMaps();
		
		EdgesPerformer performer = new EdgesPerformer();
		assertEquals(0, performer.countEdges(edges), 0);
	}
	
	@Test
	public void countEdges_Test_0010()
	{
		EdgesStructure edges = new EdgesHashMapOfHashMaps();
		edges.add(createEdge(0, 1));
		edges.add(createEdge(1, 0));
		edges.add(createEdge(2, 2));
		edges.add(createEdge(2, 3));
		
		EdgesPerformer performer = new EdgesPerformer();
		assertEquals(4, performer.countEdges(edges), 0);
	}
	
	@Test
	public void countEdges_Test_0020()
	{
		EdgesStructure edges = new EdgesHashMapOfHashMaps();
		edges.add(createEdge(0, 1));
		edges.add(createEdge(1, 0));
		edges.add(createEdge(2, 2));
		edges.add(createEdge(2, 3));
		
		edges.add(createEdge(0, 1));
		edges.add(createEdge(1, 0));
		edges.add(createEdge(2, 2));
		edges.add(createEdge(2, 3));
		edges.remove(2, 3);
		
		EdgesPerformer performer = new EdgesPerformer();
		assertEquals(3, performer.countEdges(edges), 0);
	}
		private Edge createEdge(int id1, int id2)
		{
			return new Edge(id1, id2, new TerrainType[]{TerrainType.Jungle}, new float[]{1.0f} );
		}	

}
