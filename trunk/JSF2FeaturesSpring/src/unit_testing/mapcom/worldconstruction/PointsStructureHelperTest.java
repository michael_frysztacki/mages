package unit_testing.mapcom.worldconstruction;

import static org.junit.Assert.*;
import mapcom.worldconstruction.PointsStructureHelper;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.PointsHashMap;
import mapcom.worldstructure.PointsStructure;

import org.junit.Before;
import org.junit.Test;

import unit_testing.mapcom.testing_utils.PointForTestFactory;

public class PointsStructureHelperTest {

	
	
	PointsStructure ps1, ps2;
	PointsStructureHelper helper;
	
	@Before
	public void initialize(){
		ps1 = new PointsHashMap();
		ps2 = new PointsHashMap();
		helper = new PointsStructureHelper();
	}
	
	@Test
	public void emptyPointsStructuresAreEqual(){
		assertTrue( helper.containTheSamePoints(ps1, ps2) );
	}

	@Test
	public void firstStructureIsEmptyAndTheSecondNotEmpty(){
		ps2.addPoint( PointForTestFactory.createPoint(0) );
		assertFalse( helper.containTheSamePoints(ps1, ps2) );
	}
	
	@Test
	public void secondStructureIsEmptyAndTheFirstNotEmpty(){
		ps1.addPoint( PointForTestFactory.createPoint(0) );
		assertFalse( helper.containTheSamePoints(ps1, ps2) );
	}
	
	@Test
	public void structuresContainOnePointButThesePointsAreNotEqual(){
		ps1.addPoint( PointForTestFactory.createPoint(0) );
		ps2.addPoint( PointForTestFactory.createPoint(1) );
		assertFalse( helper.containTheSamePoints(ps1, ps2) );
	}
	
	@Test
	public void StructureAreSlighlyDifferent(){
		Point temp = PointForTestFactory.createPoint(1);
		ps1.addPoint( temp );
		ps2.addPoint( temp );
		assertTrue( helper.containTheSamePoints(ps1, ps2) );
	}
	
}













