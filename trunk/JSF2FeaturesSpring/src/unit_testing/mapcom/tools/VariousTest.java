package unit_testing.mapcom.tools;

import static org.junit.Assert.*;
import mapcom.tools.Various;
import mapcom.worldstructure.Colony;
import mapcom.worldstructure.Point;

import org.junit.Test;



public class VariousTest
{

	/******************************************************************************************************************
	 *  getInversedFloatArray Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void getInversedFloatArray_Test_0010()
	{
		float[] array = new float[]{1.0f, 2.0f, 3.0f};
		float[] inversedArray = Various.getInversedFloatArray(array);
		assertEquals(1.0, inversedArray[0], 3.0f);
		assertEquals(1.0, inversedArray[1], 2.0f);
		assertEquals(1.0, inversedArray[2], 1.0f);		
	}
	@Test
	public void getInversedFloatArray_Test_0020()
	{
		float[] array = new float[]{1.0f, 2.0f, 3.0f, 4.0f};
		float[] inversedArray = Various.getInversedFloatArray(array);
		assertEquals(1.0, inversedArray[0], 4.0f);
		assertEquals(1.0, inversedArray[1], 3.0f);
		assertEquals(1.0, inversedArray[2], 2.0f);
		assertEquals(1.0, inversedArray[3], 1.0f);		
	}
	
	
	/******************************************************************************************************************
	 *  getInversedFloatArray Method TEST 
	 *****************************************************************************************************************/
	
	
	@Test
	public void createTableOfNumbersRepresentedInBinarySystem_Test_0030()
	{
		boolean[][] array = Various.createTableOfNumbersRepresentedInBinarySystem(1);
		assertEquals(2.0, array.length, 0.0);
		assertEquals(1.0, array[0].length, 0.0);
		assertTrue( !array[0][0] );
		assertTrue( array[1][0] );
		
	}
	@Test
	public void createTableOfNumbersRepresentedInBinarySystem_Test_0040()
	{
		boolean[][] array = Various.createTableOfNumbersRepresentedInBinarySystem(2);
		assertEquals( 4.0, array.length, 0.0);
		assertEquals( 2.0, array[0].length, 0.0 );
		
		// pierwszy kolumna
		assertTrue( !array[0][0] );
		assertTrue( array[1][0] );
		assertTrue( !array[2][0] );
		assertTrue( array[3][0] );

		// drugi kolumna
		assertTrue( !array[0][1] );
		assertTrue( !array[1][1] );
		assertTrue( array[2][1] );
		assertTrue( array[3][1] );
	}
	
	@Test
	public void createTableOfNumbersRepresentedInBinarySystem_Test_0050()
	{
		boolean[][] array = Various.createTableOfNumbersRepresentedInBinarySystem(4);
		assertEquals( 16.0, array.length, 0.0);
		assertEquals( 4.0, array[0].length, 0.0 );
		
		// pierwszy wiersz
		assertTrue( !array[0][0] );
		assertTrue( !array[0][1] );
		assertTrue( !array[0][2] );
		assertTrue( !array[0][3] );
		
		
		// szosty wiersz
		assertTrue( !array[6][0] );
		assertTrue( array[6][1] );
		assertTrue( array[6][2] );
		assertTrue( !array[6][3] );
		

		// ostatni (16-ty) wiersz
		assertTrue( array[15][0] );
		assertTrue( array[15][1] );
		assertTrue( array[15][2] );
		assertTrue( array[15][3] );		
	}
	
	@Test
	public void ifArraysHaveAtLeastOneTrueOnSameIndex_Test0060()
	{
		boolean[] array1 = new boolean[]{true, true, false, true, false};
		boolean[] array2 = new boolean[]{false, false, false, true, true};
		assertTrue( Various.ifArraysHaveAtLeastOneTrueOnSameIndex(array1, array2) );
	}
	
	@Test
	public void ifArraysHaveAtLeastOneTrueOnSameIndex_Test0070()
	{
		boolean[] array1 = new boolean[]{true, true, false, true, false};
		boolean[] array2 = new boolean[]{false, false, false, false, true};
		assertTrue( !Various.ifArraysHaveAtLeastOneTrueOnSameIndex(array1, array2) );
	}
	
	

	/******************************************************************************************************************
	 *  areOfTheSameClas Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void colonyAndPointAreNotOfTheSameClass()
	{
		Colony colony = new Colony(0, 0, 0, true, true);
		Point point = new Point(0, 0, 1, true, true);
		assertFalse(Various.areOfTheSameClass(colony, point));
	}
	@Test
	public void twoColoniesAreOfTheSameClass()
	{
		Colony colony1 = new Colony(0, 0, 0, true, true);
		Colony colony2 = new Colony(0, 0, 1, true, true);
		assertTrue(Various.areOfTheSameClass(colony1, colony2));
	}
	
	
}
