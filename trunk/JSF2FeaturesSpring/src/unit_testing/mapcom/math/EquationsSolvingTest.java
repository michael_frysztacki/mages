package unit_testing.mapcom.math;

import static org.junit.Assert.*;

import mapcom.math.EquationSolutions;
import mapcom.math.EquationsSolving;

import org.junit.Test;

public class EquationsSolvingTest
{
	double delta = 0.00000000001;  // 10 trylionowych, o tyle conajwyzej moze byc blad w double'ach
	
	
	/******************************************************************************************************************
	 *  solveQuadraticEquation Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void testSolveQuadraticEquation0010()
	{
		// x^2 + 2x + 1 = 0
		EquationSolutions solutions = EquationsSolving.solveQuadraticEquation(1, 2, 1);
		assertEquals(-1.0, solutions.getFirstSolution(), delta);
		assertTrue( solutions.ifHasOneSolution() );
		assertTrue( !solutions.ifHasTwoSolutions() );
	}
	@Test
	public void testSolveQuadraticEquation0020()
	{
		// -1*x^2 -2*x + 1 = 0
		EquationSolutions solutions = EquationsSolving.solveQuadraticEquation(1, -2.0, 1);
		assertEquals(1.0, solutions.getFirstSolution(), delta);
		assertTrue( solutions.ifHasOneSolution() );
		assertTrue( !solutions.ifHasTwoSolutions() );
	}
	@Test
	public void testSolveQuadraticEquation0030()
	{
		// 2*x^2 - 2x + 1 = 0
		EquationSolutions solutions = EquationsSolving.solveQuadraticEquation(2, -2.0, 1);
		assertTrue( solutions.ifHasNoSolutions() );
		assertTrue( !solutions.ifHasOneSolution() );
		assertTrue( !solutions.ifHasTwoSolutions() );
	}
	@Test
	public void testSolveQuadraticEquation0040()
	{
		// x^2 + x - 1 = 0
		EquationSolutions solutions = EquationsSolving.solveQuadraticEquation(1.0, 1.0, -1.0);
		
		assertEquals((-1.0 - Math.sqrt(5))/2.0, solutions.getFirstSolution(), delta);
		assertEquals((-1.0 + Math.sqrt(5))/2.0, solutions.getSecondSolution(), delta);
		
		assertTrue( !solutions.ifHasNoSolutions() );
		assertTrue( !solutions.ifHasOneSolution() );
		assertTrue( solutions.ifHasTwoSolutions() );
	}
	
	@Test
	public void testSolveQuadraticEquation0050()
	{
		// x^2 + 7x + 12 = 0
		EquationSolutions solutions = EquationsSolving.solveQuadraticEquation(1.0, 7.0, 12.0);
		
		assertEquals(-4.0, solutions.getFirstSolution(), delta);
		assertEquals(-3.0, solutions.getSecondSolution(), delta);
		
		assertTrue( !solutions.ifHasNoSolutions() );
		assertTrue( !solutions.ifHasOneSolution() );
		assertTrue( solutions.ifHasTwoSolutions() );
	}
	
	
	
	
	/******************************************************************************************************************
	 *  solveLinEquations2x2 Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void testSolveLinEquations2x20060()
	{
		//	2x + 3y = 6 
		//	4x + 9y = 15
		double[][] A = new double[][]{ {2, 3}, {4, 9} };
		double[] b = new double[]{6, 15};
		
		EquationSolutions solution = EquationsSolving.solveLinEquations2x2(A, b);
		assertTrue(solution.ifHasTwoSolutions());
		assertEquals(3.0/2.0, solution.getFirstSolution(), delta);
		assertEquals(1.0, solution.getSecondSolution(), delta);
	}
	@Test
	public void testSolveLinEquations2x20070()
	{
		//	4x + 9y = 15 
		//	2x + 3y = 6
		double[][] A = new double[][]{ {4, 9}, {2, 3} };
		double[] b = new double[]{15, 6};
		
		EquationSolutions solution = EquationsSolving.solveLinEquations2x2(A, b);
		assertTrue(solution.ifHasTwoSolutions());
		assertEquals(3.0/2.0, solution.getFirstSolution(), delta);
		assertEquals(1.0, solution.getSecondSolution(), delta);
	}	
	@Test
	public void testSolveLinEquations2x20080()
	{
		// 8x + 7y = 38
		// 3x - 5y = -1
		double[][] A = new double[][]{ {8, 7}, {3, -5} };
		double[] b = new double[]{38, -1};
		
		EquationSolutions solution = EquationsSolving.solveLinEquations2x2(A, b);
		assertTrue(solution.ifHasTwoSolutions());
		assertEquals(3.0, solution.getFirstSolution(), delta);
		assertEquals(2.0, solution.getSecondSolution(), delta);
	}			
	@Test
	public void testSolveLinEquations2x20090()
	{
		// x + y = 2
		// x - y = 0
		double[][] A = new double[][]{ {1, 1}, {1, -1} };
		double[] b = new double[]{2, 0};
		
		EquationSolutions solution = EquationsSolving.solveLinEquations2x2(A, b);
		assertTrue(solution.ifHasTwoSolutions());
		assertEquals(1.0, solution.getFirstSolution(), delta);
		assertEquals(1.0, solution.getSecondSolution(), delta);
	}
	@Test
	public void testSolveLinEquations2x20100()
	{
		// x + y = 2
		// 0.0000000001*x - 0.0000000001*y = 0 
		double[][] A = new double[][]{ {1, 1}, {0.0000000001, -0.0000000001} };
		double[] b = new double[]{2, 0};
		
		EquationSolutions solution = EquationsSolving.solveLinEquations2x2(A, b);
		assertTrue(solution.ifHasTwoSolutions());
		assertEquals(1.0, solution.getFirstSolution(), delta);
		assertEquals(1.0, solution.getSecondSolution(), delta);
	}	
	@Test
	public void testSolveLinEquations2x20110()
	{
		// 0.0000000001*x - 0.0000000001*y = 0
		// x + y = 2 
		double[][] A = new double[][]{ {0.0000000001, -0.0000000001}, {1, 1}};
		double[] b = new double[]{0, 2};
		
		EquationSolutions solution = EquationsSolving.solveLinEquations2x2(A, b);
		assertTrue(solution.ifHasTwoSolutions());
		assertEquals(1.0, solution.getFirstSolution(), delta);
		assertEquals(1.0, solution.getSecondSolution(), delta);
	}
	@Test
	public void testSolveLinEquations2x20120()
	{
		// 0.0000000001*x + 0.0000000001*y = 0.0000000002
		// 1000*x - 1000*y = 0 
		double[][] A = new double[][]{ {0.0000000001, 0.0000000001}, {1000, -1000} };
		double[] b = new double[]{0.0000000002, 0};
		
		EquationSolutions solution = EquationsSolving.solveLinEquations2x2(A, b);
		assertTrue(solution.ifHasTwoSolutions());
		assertEquals(1.0, solution.getFirstSolution(), delta);
		assertEquals(1.0, solution.getSecondSolution(), delta);
	}	
	@Test
	public void testSolveLinEquations2x20130()
	{
		// 1000*x - 1000*y = 0 
		// 0.0000000001*x + 0.0000000001*y = 0.0000000002
		
		double[][] A = new double[][]{ {1000, -1000}, {0.0000000001, 0.0000000001} };
		double[] b = new double[]{0, 0.0000000002};
		
		EquationSolutions solution = EquationsSolving.solveLinEquations2x2(A, b);
		assertTrue(solution.ifHasTwoSolutions());
		assertEquals(1.0, solution.getFirstSolution(), delta);
		assertEquals(1.0, solution.getSecondSolution(), delta);
	}	
}
