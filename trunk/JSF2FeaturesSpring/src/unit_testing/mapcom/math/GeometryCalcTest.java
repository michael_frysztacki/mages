package unit_testing.mapcom.math;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertTrue;
import mapcom.imgproc.Colors;
import mapcom.imgproc.ImgProc;
import mapcom.imgproc.LineDrawing;
import mapcom.imgproc.PointDrawing;
import mapcom.mapprojections.Equirectangular;
import mapcom.mapprojections.MapProjection;
import mapcom.math.GeometryCalc;
import mapcom.math.PointOnWorldSphere;
import mapcom.math.Transl;
import mapcom.math.WorldConstants;

import org.junit.BeforeClass;
import org.junit.Test;

import unit_testing.mapcom.testing_utils.TestHelper;

public class GeometryCalcTest
{
	double delta = 0.00000000001;  // 10 trylionowych, o tyle conajwyzej moze byc blad w double'ach
	boolean draw = true;
	double PI = Math.PI;
	
		
	private static String testsDirectory = TestHelper.testsDirectory + "math/GeometryCalcTest_Files/";
	private static String  testsResultsDirectory = testsDirectory + "Results/";
	
	PointOnWorldSphere A, B, C;
	
	/******************************************************************************************************************
	 *  rotateVectorAroundZAxis Method TEST 
	 *****************************************************************************************************************/	
	
	@BeforeClass
	public static void setUp(){
		TestHelper.deleteContentOfDirectoryExceptGitIgnoreFile(testsResultsDirectory);
	}
	
	@Test		// Obrot wektora o 45 stopni
	public void RotateVectorAroundZAxis_Test_0010()
	{		
		double[] vector; 
				
		vector = new double[]{1,1,3};
		vector = GeometryCalc.rotateVectorAroundZAxis(vector, PI/4);
		assertEquals(0.0, vector[0], delta);
		assertEquals(Math.sqrt(2.0), vector[1], delta);
		assertEquals(3.0, vector[2], delta);		
	}	
	@Test		// Obrot wektora o -60 stopni
	public void RotateVectorAroundZAxis_Test_0020()
	{
		double[] vector; 
	
		vector = new double[]{Math.sqrt(3), 1.0, 3};
		vector = GeometryCalc.rotateVectorAroundZAxis(vector, -PI/3);
		assertEquals(Math.sqrt(3), vector[0], delta);
		assertEquals(-1.0, vector[1], delta);
		assertEquals(3.0, vector[2], delta);		
	}
	@Test 		// Obrot wektora o 90 stopni
	public void RotateVectorAroundZAxis_Test_0030()
	{
		double[] vector;
		
		vector = new double[]{Math.sqrt(3), 1.0, 3};
		vector = GeometryCalc.rotateVectorAroundZAxis(vector, PI/2);
		assertEquals(-1.0, vector[0], delta);
		assertEquals(Math.sqrt(3), vector[1], delta);
		assertEquals(3.0, vector[2], delta);		
	}
	
	@Test 		// Obrot wektora o 450 stopni  
	public void RotateVectorAroundZAxis_Test_0040()
	{
		double[] vector;		
	
		vector = new double[]{Math.sqrt(3), 1.0, 3};
		vector = GeometryCalc.rotateVectorAroundZAxis(vector, PI/2 + 2*PI);
		assertEquals(-1.0, vector[0], delta);
		assertEquals(Math.sqrt(3), vector[1], delta);
		assertEquals(3.0, vector[2], delta);		
	}
	@Test 		// Obrot wektora o 90 stopni   
	public void RotateVectorAroundZAxis_Test_0050()
	{
		double[] vector;		
	
		vector = new double[]{0, 1, 0};
		vector = GeometryCalc.rotateVectorAroundZAxis(vector, PI/2);
		assertEquals(-1, vector[0], delta);
		assertEquals(0.0, vector[1], delta);
		assertEquals(0.0, vector[2], delta);		
	}
	@Test 		// Obrot wektora o 180 stopni   
	public void RotateVectorAroundZAxis_Test_0060()
	{
		double[] vector;		
	
		vector = new double[]{0, 1, 0};
		vector = GeometryCalc.rotateVectorAroundZAxis(vector, PI);
		assertEquals(0.0, vector[0], delta);
		assertEquals(-1.0, vector[1], delta);
		assertEquals(0.0, vector[2], delta);		
	}
	
	
	/******************************************************************************************************************
	 *  rotateVectorAroundXAxis Method TEST 
	 *****************************************************************************************************************/
	
	@Test		// Obrot wektora o 45 stopni
	public void RotateVectorAroundXAxis_Test_0070()
	{
		double[] vector; 
				
		vector = new double[]{0, 1, 0};
		vector = GeometryCalc.rotateVectorAroundXAxis(vector, PI/4);
		assertEquals(0.0, vector[0], delta);
		assertEquals(Math.sqrt(2)/2.0, vector[1], delta);
		assertEquals(Math.sqrt(2.0)/2.0, vector[2], delta);		
	}
	
	@Test		// Obrot wektora o 90 stopni
	public void RotateVectorAroundXAxis_Test_0080()
	{
		double[] vector; 
				
		vector = new double[]{0, 1, 0};
		vector = GeometryCalc.rotateVectorAroundXAxis(vector, PI/2);
		assertEquals(0.0, vector[0], delta);
		assertEquals(0.0, vector[1], delta);
		assertEquals(1.0, vector[2], delta);		
	}	
	@Test		// Obrot wektora o 180 stopni
	public void RotateVectorAroundXAxis_Test_0090()
	{
		double[] vector; 
				
		vector = new double[]{0, 1, 0};
		vector = GeometryCalc.rotateVectorAroundXAxis(vector, PI);
		assertEquals(0.0, vector[0], delta);
		assertEquals(-1.0, vector[1], delta);
		assertEquals(0.0, vector[2], delta);		
	}	
	
	/******************************************************************************************************************
	 *  PlaneSlopeOXAxis Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void PlaneSlopeOXAxis_Test_0100() 
	{
		// wektor ma tylko dodatnia skladowa Z, czyli plaszczyzna nie jest obrocona wokol x		
		assertEquals(0.0, GeometryCalc.planeSlopeOXAxis(new double[]{0.0, 0.0, 1.0}), delta);
	}
	@Test
	public void PlaneSlopeOXAxis_Test_0110() 
	{
		// wektor ma tylko ujemna skladowa Z, czyli plaszczyzna nie jest obrocona wokol x 
		assertEquals(0.0, GeometryCalc.planeSlopeOXAxis(new double[]{0.0, 0.0, -5.0}), delta);
	}
	@Test
	public void PlaneSlopeOXAxis_Test_0120() 
	{
		// wektor ma tylko dodatnia skladowa Y, czyli plaszczyzna jest obrocona wokol x o 90 stopni
		assertEquals(PI/2, GeometryCalc.planeSlopeOXAxis(new double[]{0.0, 5.0, 0.0}), delta);	
	}
	@Test
	public void PlaneSlopeOXAxis_Test_0140() 
	{
		// wektor ma rowne, dodatnie skladowe Y i Z, oraz skladowa X jest rowna zero, czyli plaszczyzna  
		// jest obrocona wokol OX o -45 stopni
		assertEquals(-PI/4, GeometryCalc.planeSlopeOXAxis(new double[]{0.0, 1.0, 1.0}), delta);
	}
	@Test
	public void PlaneSlopeOXAxis_Test_0150() 
	{
		// Wektor ma rowne, ujemne skladowe Y i Z, oraz skladowa X jest rowna zero;
		// wynik powinien byc taki sam jak powyzszego wywolania metody asserEquals.
		// Czyli plaszczyzna jest obrocona wokol OX o -45 stopni		
		assertEquals(-PI/4, GeometryCalc.planeSlopeOXAxis(new double[]{0.0, -1.0, -1.0}), delta);
	}
	@Test
	public void PlaneSlopeOXAxis_Test_0160() 
	{
		// wektor ma rowne, ale rozne w znaku skladowe Y i Z, oraz skladowa X rowna zero.  
		// Czyli plaszczyzna jest obrocona wokol OX o 45 stopni		
		assertEquals(PI/4, GeometryCalc.planeSlopeOXAxis(new double[]{0.0, -1.0, 1.0}), delta);
	}
	@Test
	public void PlaneSlopeOXAxis_Test_0170() 
	{
		// Plaszczyzna obrocona o 30 stopni
		assertEquals(-PI/6, GeometryCalc.planeSlopeOXAxis(new double[]{0, 1, Math.sqrt(3)}), delta);
	}
	@Test
	public void PlaneSlopeOXAxis_Test_0180() 
	{
		// Plaszczyzna obrocona o 0 stopni
		assertEquals(0.0, GeometryCalc.planeSlopeOXAxis(new double[]{1, 0, Math.sqrt(3)}), delta);
	}
	@Test
	public void PlaneSlopeOXAxis_Test_0190() 
	{
		// Plaszczyzna obrocona o 60 stopni
		assertEquals(-PI/3, GeometryCalc.planeSlopeOXAxis(new double[]{0, Math.sqrt(3), 1}), delta);
	}
	@Test
	public void PlaneSlopeOXAxis_Test_0200() 
	{
		// Plaszczyzna obrocona o 0 stopni
		assertEquals(0.0, GeometryCalc.planeSlopeOXAxis(new double[]{Math.sqrt(3), 0, 1}), delta);
	}
	
	/******************************************************************************************************************
	 *  PlaneSlopeOZAxis Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void PlaneSlopeOZAxis_Test_0210() 
	{
		// obrot o -45 stopni
		assertEquals(-PI/4.0, GeometryCalc.planeSlopeOZAxis(new double[]{-5.0, -5.0, 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0220() 
	{		
		// obrot o -45 stopni		
		assertEquals(-PI/4.0, GeometryCalc.planeSlopeOZAxis(new double[]{5.0, 5.0, 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0230() 
	{
		// obrot o 45 stopni
		assertEquals(PI/4.0, GeometryCalc.planeSlopeOZAxis(new double[]{5.0, -5.0, 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0240() 
	{
		// obrot o 45 stopni
		assertEquals(PI/4.0, GeometryCalc.planeSlopeOZAxis(new double[]{5.0, -5.0, 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0250() 
	{
		// obrot o -60 stopni 
		assertEquals(-PI/3.0, GeometryCalc.planeSlopeOZAxis(new double[]{Math.sqrt(3.0), 1.0, 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0260() 
	{
		// obrot o 60 stopni		
		assertEquals(PI/3.0, GeometryCalc.planeSlopeOZAxis(new double[]{-Math.sqrt(3.0), 1.0, 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0270() 
	{		
		// obrot o -60 stopni
		assertEquals(-PI/3.0, GeometryCalc.planeSlopeOZAxis(new double[]{-Math.sqrt(3.0), -1.0, 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0280() 
	{
		// obrot o 60 stopni
		assertEquals(PI/3.0, GeometryCalc.planeSlopeOZAxis(new double[]{Math.sqrt(3.0), -1.0, 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0290() 
	{
		// obrot o -30 stopni
		assertEquals(-PI/6.0, GeometryCalc.planeSlopeOZAxis(new double[]{1.0, Math.sqrt(3.0), 4.0}), delta);		
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0300() 
	{
		// obrot o 30 stopni
		assertEquals(PI/6.0, GeometryCalc.planeSlopeOZAxis(new double[]{1.0, -Math.sqrt(3.0), 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0310() 
	{
		// obrot o -30 stopni
		assertEquals(-PI/6.0, GeometryCalc.planeSlopeOZAxis(new double[]{-1.0, -Math.sqrt(3.0), 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0320() 
	{
		// obrot o 30 stopni
		assertEquals(PI/6.0, GeometryCalc.planeSlopeOZAxis(new double[]{-1.0, Math.sqrt(3.0), 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0330() 
	{
		// obrot o 30 stopni
		assertEquals(PI/6.0, GeometryCalc.planeSlopeOZAxis(new double[]{-1.0, Math.sqrt(3.0), 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0340() 
	{
		// Brak skladowej X, wiec brak obrotu wokol osi OZ
		assertEquals(0.0, GeometryCalc.planeSlopeOZAxis(new double[]{0.0, Math.sqrt(3.0), 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0350() 
	{
		// Brak skladowej X, wiec brak obrotu wokol osi OZ
		assertEquals(0.0, GeometryCalc.planeSlopeOZAxis(new double[]{0.0, -Math.sqrt(3.0), -4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0360() 
	{
		// Brak skladowej X, wiec brak obrotu wokol osi OZ
		assertEquals(0.0, GeometryCalc.planeSlopeOZAxis(new double[]{0.0, -Math.sqrt(3.0), 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0370() 
	{
		// Brak skladowej X, wiec brak obrotu wokol osi OZ
		assertEquals(0.0, GeometryCalc.planeSlopeOZAxis(new double[]{0.0, Math.sqrt(3.0), -4.0}), delta);				
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0380() 
	{
		// Y=0 => obrot o 90 stopni
		assertEquals(PI/2.0, GeometryCalc.planeSlopeOZAxis(new double[]{0.5, 0.0, 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0390() 
	{
		// Y=0, X=0;
		assertEquals(0.0, GeometryCalc.planeSlopeOZAxis(new double[]{0.0, 0.0, 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0400() 
	{
		// X=0  ==> obrot o 0 stopni
		assertEquals(0.0, GeometryCalc.planeSlopeOZAxis(new double[]{0, 1.0, 4.0}), delta);
	}
	@Test
	public void PlaneSlopeOzAxis_Test_0410() 
	{
		// X=0  ==> obrot o 0 stopni
		assertEquals(0.0, GeometryCalc.planeSlopeOZAxis(new double[]{0, 1.0, 0.0}), delta);
	}		
	

	/******************************************************************************************************************
	 *  getPlaneSlopes Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void GetPlaneSlopes_Test_0420()
	{
		double[] vector = new double[]{1, 1, Math.sqrt(2)};
		assertEquals( -PI/4, GeometryCalc.getPlaneSlopes(vector)[0], delta);
		assertEquals( -PI/4, GeometryCalc.getPlaneSlopes(vector)[1], delta);
	}	
	@Test
	public void GetPlaneSlopes_Test_0430()
	{
		double[] vector = new double[]{0,0,2};
		assertEquals(0.0, GeometryCalc.getPlaneSlopes(vector)[0], delta);
		assertEquals(0.0, GeometryCalc.getPlaneSlopes(vector)[1], delta);
	}
	@Test
	public void GetPlaneSlopes_Test_0440()
	{
		double[] vector = new double[]{0,0,-2};
		assertEquals(0.0, GeometryCalc.getPlaneSlopes(vector)[0], delta);
		assertEquals(0.0, GeometryCalc.getPlaneSlopes(vector)[1], delta);
	}	
	@Test
	public void GetPlaneSlopes_Test_0450()
	{
		double[] vector = new double[]{0.5, Math.sqrt(3)/2.0, Math.sqrt(3) };
		assertEquals( -PI/6, GeometryCalc.getPlaneSlopes(vector)[0], delta);
		assertEquals( -PI/6, GeometryCalc.getPlaneSlopes(vector)[1], delta);
	}
	@Test
	public void GetPlaneSlopes_Test_0460()
	{
		// tylko skladowa X jest niezerowa
		double[] vector = new double[]{-5, 0, 0};
		assertEquals( PI/2, GeometryCalc.getPlaneSlopes(vector)[0], delta);
		assertEquals( PI/2, GeometryCalc.getPlaneSlopes(vector)[1], delta);
	}
	@Test
	public void GetPlaneSlopes_Test_0470()
	{
		// tylko skladowa X jest niezerowa
		double[] vector = new double[]{5, 0, 0};
		assertEquals( PI/2, GeometryCalc.getPlaneSlopes(vector)[0], delta);
		assertEquals( PI/2, GeometryCalc.getPlaneSlopes(vector)[1], delta);
	}
	@Test
	public void GetPlaneSlopes_Test_0480()
	{
		// tylko skladowa Y jest niezerowa
		double[] vector = new double[]{0, -5, 0};
		assertEquals( PI/2, GeometryCalc.getPlaneSlopes(vector)[0], delta);
		assertEquals( 0.0, GeometryCalc.getPlaneSlopes(vector)[1], delta);
	}
	@Test
	public void GetPlaneSlopes_Test_0490()
	{
		// tylko skladowa Y jest niezerowa
		double[] vector = new double[]{0, 5, 0};
		assertEquals( PI/2, GeometryCalc.getPlaneSlopes(vector)[0], delta);
		assertEquals( 0.0, GeometryCalc.getPlaneSlopes(vector)[1], delta);
	}
	
	
	// WERSJA DLA METODY, KTORA PRZYJMUJE NA WEJSCIU PUNTKY
	
	@Test
	public void GetPlaneSlopes_Test_0491()
	{
		A = new PointOnWorldSphere(150.0, 0);
		B = new PointOnWorldSphere(150, 10);
		
		assertEquals( PI/2, Math.abs( GeometryCalc.getPlaneSlopes(A, B)[0] ), delta );
		assertEquals( PI/3.0, GeometryCalc.getPlaneSlopes(A, B)[1], delta );
	}
	@Test
	public void GetPlaneSlopes_Test_0492()
	{
		A = new PointOnWorldSphere(-45.0, -10);
		B = new PointOnWorldSphere(45, -10);
		
		assertTrue( GeometryCalc.getPlaneSlopes(A, B)[0] < 0);
		assertEquals( 0.0, GeometryCalc.getPlaneSlopes(A, B)[1], delta );
	}
	@Test
	public void GetPlaneSlopes_Test_0493()
	{
		A = new PointOnWorldSphere(-45.0, 10);
		B = new PointOnWorldSphere(45, 10);
		
		assertTrue( GeometryCalc.getPlaneSlopes(A, B)[0] > 0);
		assertEquals( 0.0, GeometryCalc.getPlaneSlopes(A, B)[1], delta );
	}
	/******************************************************************************************************************
	 *  getDistance Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void GetDistance_Test_0500()   
	{
		// sprawdzenie odleglosci dla dwoch pkt polozonych na rowniku oddalonych o 45 stopni
		assertEquals(WorldConstants.EARTH_RADIUS_LENGTH*PI/4.0, GeometryCalc.sphericalDistance(0, 0, 45, 0 ), delta);
	}	
	@Test
	public void GetDistance_Test_0510() 
	{
		// sprawdzenie odleglosci dla dwoch pkt polozonych na rowniku oddalonych o 170 stopni
		assertEquals(WorldConstants.EARTH_RADIUS_LENGTH*PI*17.0/18.0, GeometryCalc.sphericalDistance(0, 0, 170, 0), delta);
	}
	@Test
	public void GetDistance_Test_0520() 
	{
		// sprawdzenie odleglosci dla dwoch pkt polozonych na rowniku oddalonych o 180 stopni
		assertEquals(WorldConstants.EARTH_RADIUS_LENGTH*PI, GeometryCalc.sphericalDistance(0, 0, 180, 0), delta);
	}
	@Test
	public void GetDistance_Test_0530() 
	{
		// sprawdzenie odleglosci dla dwoch pkt polozonych na rowniku oddalonych o 180 stopni
		assertEquals(WorldConstants.EARTH_RADIUS_LENGTH*PI, GeometryCalc.sphericalDistance(-90, 0, 90, 0), delta);
	}
	@Test
	public void GetDistance_Test_0540() 
	{
		// Jeden z pkt jest na rowniku, a drugi na lat 45N. Obydwa na tej samej dlugosci geograficznej.
		assertEquals(WorldConstants.EARTH_RADIUS_LENGTH*PI/4.0, GeometryCalc.sphericalDistance(20, -45, 20 , 0 ), delta);
	}
	@Test
	public void GetDistance_Test_0550() 
	{
		// sprawdzenie odleglosci dla dwoch pkt polozonych na poludniku zero. Jeden z pkt jest na rowniku, a drugi na biegunie N
		assertEquals(WorldConstants.EARTH_RADIUS_LENGTH*PI/2.0, GeometryCalc.sphericalDistance(0, -90, 0 , 0 ), delta);
	}
	@Test
	public void GetDistance_Test_0560() 
	{
		// sprawdzenie odleglosci dla dwoch pkt polozonych na poludniku zero. Jeden z pkt jest na rowniku, a drugi na biegunie S
		assertEquals(WorldConstants.EARTH_RADIUS_LENGTH*PI/2.0, GeometryCalc.sphericalDistance(0, 90, 0 , 0 ), delta);
	}
	@Test
	public void GetDistance_Test_0570() 
	{
		// sprawdzenie odleglosci dla dwoch pkt polozonych na poludniku 0. Pkty polozone na biegunach S i N
		assertEquals(WorldConstants.EARTH_RADIUS_LENGTH*PI, GeometryCalc.sphericalDistance(0, -90, 0, 90 ), delta);
	}
	@Test
	public void GetDistance_Test_0580() 
	{
		// sprawdzenie odleglosci dla dwoch pkt polozonych na poludniku 90. Pkty polozone na biegunach S i N
		assertEquals(WorldConstants.EARTH_RADIUS_LENGTH*PI, GeometryCalc.sphericalDistance(90, -90, 90, 90 ), delta);
	}
	@Test
	public void GetDistance_Test_0590() 
	{
		// odleg�o�� dla antypodalnych punkt�w
		assertEquals(WorldConstants.EARTH_RADIUS_LENGTH*PI, GeometryCalc.sphericalDistance(77, -90, 77, 90 ), delta);
	}	
	
	
	/******************************************************************************************************************
	 *  getLatitudeDistance Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void GetLatitudeDistance_Test_0600()
	{
		A = new PointOnWorldSphere(0, -90);
		B = new PointOnWorldSphere(5, 90);
		assertEquals(PI, GeometryCalc.latitudeDistanceInRad(A, B), delta);
	}
	@Test
	public void GetLatitudeDistance_Test_0610()
	{
		A = new PointOnWorldSphere(0, -30);
		B = new PointOnWorldSphere(5, 60);
		assertEquals(PI/2.0, GeometryCalc.latitudeDistanceInRad(A, B), delta);
	}
	@Test
	public void GetLatitudeDistance_Test_0620()
	{
		A = new PointOnWorldSphere(120, -90);
		B = new PointOnWorldSphere(5, 90);
		assertEquals(PI, GeometryCalc.latitudeDistanceInRad(A, B), delta);
	}
	@Test
	public void GetLatitudeDistance_Test_0630()
	{
		A = new PointOnWorldSphere(0, 45);
		B = new PointOnWorldSphere(180, 45);
		assertEquals(0, GeometryCalc.latitudeDistanceInRad(A, B), delta);
	}
	@Test
	public void GetLatitudeDistance_Test_0640()
	{
		A = new PointOnWorldSphere(-45, 45);
		B = new PointOnWorldSphere(180, -45);
		assertEquals(PI/2, GeometryCalc.latitudeDistanceInRad(A, B), delta);
	}
	
	
	/******************************************************************************************************************
	 *  getLatitudeDistance Method TEST 
	 *****************************************************************************************************************/

	@Test
	public void GetLongitudeDistance_Test_0650()
	{
		A = new PointOnWorldSphere(50, 45);
		B = new PointOnWorldSphere(180, -45);
		assertEquals(PI*13.0/18.0, GeometryCalc.longitudeDistanceInRad(A, B), delta);
	}
	@Test
	public void GetLongitudeDistance_Test_0660()
	{
		A = new PointOnWorldSphere(-50, 45);
		B = new PointOnWorldSphere(180, -45);
		assertEquals(PI*13.0/18.0, GeometryCalc.longitudeDistanceInRad(A, B), delta);
	}
	@Test
	public void GetLongitudeDistance_Test_0670()
	{
		A = new PointOnWorldSphere(0, 45);
		B = new PointOnWorldSphere(0, -45);
		assertEquals(0, GeometryCalc.longitudeDistanceInRad(A, B), delta);
	}
	
	/******************************************************************************************************************
	 *  getAngleDistance Method TEST 
	 *****************************************************************************************************************/

	@Test
	public void angleDistanceInRad_Test_0680()
	{
		PointOnWorldSphere start = new PointOnWorldSphere(0.0, 60.0);
		PointOnWorldSphere end = new PointOnWorldSphere(0.0, 60.0);
		assertEquals(0, GeometryCalc.angleDistanceInRad(start, end), delta);
	}
	@Test
	public void GetAngleDistance_Test_0690()
	{
		// jeden wektor rownolegly do OX, drugi do OY
		PointOnWorldSphere start = new PointOnWorldSphere(0.0, 90.0);
		PointOnWorldSphere end = new PointOnWorldSphere(0.0, 0.0);
		assertEquals(PI/2.0, GeometryCalc.angleDistanceInRad(start, end), delta);
	}
	@Test
	public void GetAngleDistance_Test_0700()
	{
		// Dwa wektory Oddalone o 90 stopni
		PointOnWorldSphere start = new PointOnWorldSphere(30.0, 0.0);
		PointOnWorldSphere end = new PointOnWorldSphere(120.0, 0.0);
		assertEquals(PI/2.0, GeometryCalc.angleDistanceInRad(start, end), delta);
	}
	@Test
	public void GetAngleDistance_Test_0710()
	{
		// Dwa wektory Oddalone o 135 stopni
		PointOnWorldSphere start = new PointOnWorldSphere(30.0, 90.0);
		PointOnWorldSphere end = new PointOnWorldSphere(30.0, -45.0);
		assertEquals(PI*3.0/4.0, GeometryCalc.angleDistanceInRad(start, end), delta);
	}
	@Test
	public void GetAngleDistance_Test_0720()
	{
		// Dwa wektory Oddalone o 180 stopni
		PointOnWorldSphere start = new PointOnWorldSphere(180.0, 0.0);
		PointOnWorldSphere end = new PointOnWorldSphere(0.0, 0.0);
		assertEquals(PI, GeometryCalc.angleDistanceInRad(start, end), delta);
	}
	@Test
	public void GetAngleDistance_Test_0730()
	{
		// Dwa wektory Oddalone o 180 stopni
		PointOnWorldSphere start = new PointOnWorldSphere(0.0, 90.0);
		PointOnWorldSphere end = new PointOnWorldSphere(0.0, -90.0);
		assertEquals(PI, GeometryCalc.angleDistanceInRad(start, end), delta);
	}
	@Test
	public void GetAngleDistance_Test_0731()
	{
		// Dwa wektory Oddalone o 180 stopni
		PointOnWorldSphere start = new PointOnWorldSphere(90.0, 0.0);
		PointOnWorldSphere end = new PointOnWorldSphere(0.0, 90.0);
		assertEquals(PI/2, GeometryCalc.angleDistanceInRad(start, end), delta);
	}
	
	
	/******************************************************************************************************************
	 *  getPointBetween Method TEST 
	 *****************************************************************************************************************/

	@Test
	public void GetPointBetween_Test_0740()
	{		
		// brak przesuniecia z pkt poczatkowego
		A = new PointOnWorldSphere(50.0, 30.0);
		B = new PointOnWorldSphere(100.0, 60.0);
		C = GeometryCalc.getPointBetween(A, B, 0);
		assertEquals(50.0, C.getLongit(), delta );
		assertEquals(30.0, C.getLat(), delta );
	}
	@Test
	public void GetPointBetween_Test_0750()
	{
		// przesuniecie na o 20 stopni po rowniku
		A = new PointOnWorldSphere(45.0, 0.0);
		B = new PointOnWorldSphere(90.0, 0.0);
		C = GeometryCalc.getPointBetween(A, B, Math.toRadians(20) );
		assertEquals(65.0, C.getLongit(), delta );
		assertEquals(0.0, C.getLat(), delta );
	}
	@Test
	public void GetPointBetween_Test_0760()
	{
		A = new PointOnWorldSphere(45.0, 0.0);
		B = new PointOnWorldSphere(90.0, 0.0);
		C = GeometryCalc.getPointBetween(A, B, Math.toRadians(45));
		assertEquals(90.0, C.getLongit(), delta );
		assertEquals(0.0, C.getLat(), delta );
	}
	@Test
	public void GetPointBetween_Test_0770()
	{
		// przesuniecie o 178 stopni po rowniku
		A = new PointOnWorldSphere(1.0, 0.0);
		B = new PointOnWorldSphere(180.0, 0.0);
		C = GeometryCalc.getPointBetween(A, B, Math.toRadians(178));
		assertEquals(179.0, C.getLongit(), delta );
		assertEquals(0.0, C.getLat(), delta );
	}
	@Test
	public void GetPointBetween_Test_0780()
	{
		// przesuniecie o 178 stopni po rownolezniku 0
		A = new PointOnWorldSphere(0.0, -89.0);
		B = new PointOnWorldSphere(0.0, 90.0);
		C = GeometryCalc.getPointBetween(A, B, Math.toRadians(178));
		assertEquals(0.0, C.getLongit(), delta );
		assertEquals(89.0, C.getLat(), delta );
	}
	@Test
	public void GetPointBetween_Test_0790()
	{
		// przesuniecie o 178 stopni po rownolezniku 90
		A = new PointOnWorldSphere(90.0, -89.0);
		B = new PointOnWorldSphere(90.0, 90.0);
		C = GeometryCalc.getPointBetween(A, B, Math.toRadians(178));
		assertEquals(90.0, C.getLongit(), delta );
		assertEquals(89.0, C.getLat(), delta );
	}
	@Test
	public void GetPointBetween_Test_0800()
	{
		// przesuniecie o 45 stopni po rownolezniku 180
		A = new PointOnWorldSphere(-180.0, -45.0);
		B = new PointOnWorldSphere(-180.0, 0.0);
		C = GeometryCalc.getPointBetween(A, B, Math.toRadians(25));
		assertEquals(-180, C.getLongit(), delta );
		assertEquals(-20, C.getLat(), delta );
	}
	@Test
	public void GetPointBetween_Test_0810()
	{
		// przesuniecie o -45 stopni po rownolezniku -180
		A = new PointOnWorldSphere(-180.0, 0.0);
		B = new PointOnWorldSphere(-180.0, -45.0);		
		C = GeometryCalc.getPointBetween(A, B, Math.toRadians(25));
		assertEquals(-180, C.getLongit(), delta );
		assertEquals(-25, C.getLat(), delta );
	}	
	@Test
	public void GetPointBetween_Test_0811()
	{
		// przejscie przez biegun polnocny
		A = new PointOnWorldSphere(-90.0, 1.0);
		B = new PointOnWorldSphere( 90.0, 1.0);		
		C = GeometryCalc.getPointBetween(A, B, Math.toRadians(88));
		assertEquals( -90, C.getLongit(), delta*100 );
		assertEquals( 89, C.getLat(), delta*100 );
	}
	
	
	
	/******************************************************************************************************************
	 *  getInscribedAngle Method TEST 
	 *****************************************************************************************************************/
	
	// Wszystkie ponizsze testy sprawdzaja czy kat oparty na cieciwe jest dwa razy wiekszy jesli jest zaczepiony na srodku
	// kola niz w przypadku gdy jest zaczepiony na okregu
	
	// ponizsze 6 testow dla pkt lezacych na rowniku
	@Test
	public void GetInscribedAngle_Test_0820()
	{
		A = new PointOnWorldSphere(0.0, 0.0);
		B = new PointOnWorldSphere(90.0, 0.0);		
		C = new PointOnWorldSphere(-180, 0.0);
		assertEquals(PI/4, GeometryCalc.getInscribedAngle(A, B, C), delta);
	}
	@Test
	public void GetInscribedAngle_Test_0830()
	{
		A = new PointOnWorldSphere(0.0, 0.0);
		B = new PointOnWorldSphere(90.0, 0.0);		
		C = new PointOnWorldSphere(180, 0.0);
		assertEquals(PI/4, GeometryCalc.getInscribedAngle(A, B, C), delta);
	}	
	@Test
	public void GetInscribedAngle_Test_0840()
	{
		A = new PointOnWorldSphere(0.0, 0.0);
		B = new PointOnWorldSphere(180.0, 0.0);		
		C = new PointOnWorldSphere(-90, 0.0);
		assertEquals(PI/2, GeometryCalc.getInscribedAngle(A, B, C), delta);
	}
	@Test
	public void GetInscribedAngle_Test_0850()
	{
		A = new PointOnWorldSphere(0.0, 0.0);
		B = new PointOnWorldSphere(180.0, 0.0);		
		C = new PointOnWorldSphere(-90.0, 0.0);
		assertEquals(PI/2, GeometryCalc.getInscribedAngle(A, B, C), delta);
	}
	@Test
	public void GetInscribedAngle_Test_0860()
	{
		A = new PointOnWorldSphere(0.0, 0.0);
		B = new PointOnWorldSphere(-180.0, 0.0);		
		C = new PointOnWorldSphere(-90.0, 0.0);
		assertEquals(PI/2, GeometryCalc.getInscribedAngle(A, B, C), delta);
	}
	@Test
	public void GetInscribedAngle_Test_0861()
	{
		A = new PointOnWorldSphere(0.0, 0.0);
		B = new PointOnWorldSphere(60.0, 0.0);		
		C = new PointOnWorldSphere(217.0, 0.0);
		assertEquals(PI/6, GeometryCalc.getInscribedAngle(A, B, C), delta);
	}
	
	// ponizsze testy dla pkt lezacych na poludniku 0	
	@Test
	public void GetInscribedAngle_Test_0870()
	{
		A = new PointOnWorldSphere(0.0, -90.0);
		B = new PointOnWorldSphere(0.0, 90.0);		
		C = new PointOnWorldSphere(180.0, .0);
		assertEquals(PI/2, GeometryCalc.getInscribedAngle(A, B, C), delta);
	}
	@Test
	public void GetInscribedAngle_Test_0880()
	{
		A = new PointOnWorldSphere(0.0, -45.0);
		B = new PointOnWorldSphere(0.0, 45.0);		
		C = new PointOnWorldSphere(180.0, .0);
		assertEquals(PI/4, GeometryCalc.getInscribedAngle(A, B, C), delta);
	}
	
	/******************************************************************************************************************
	 *  getPointClosestToAPole Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void GetPointClosestToAPole_Test_0890() 
	{
		// pkt B blizszy biegunowi polnocnemu
		A = new PointOnWorldSphere (50.0, -50.0);
		B = new PointOnWorldSphere (50.0, -49.0);
		C = GeometryCalc.getPointClosestToAPole(A, B); 
		assertEquals(-50.0, C.getLat(), delta );
	}
	@Test
	public void GetPointClosestToAPole_Test_0900() 
	{
		// pkt B blizszy biegunowi polnocnemu
		A = new PointOnWorldSphere (50.0, 15.0);
		B = new PointOnWorldSphere (50.0, 89.0);
		C = GeometryCalc.getPointClosestToAPole(A, B); 
		assertEquals(89.0, C.getLat(), delta );
	}
	@Test
	public void GetPointClosestToAPole_Test_0910() 
	{
		// pkt A blizszy biegunowi polnocnemu
		A = new PointOnWorldSphere (50.0, -90.0);
		B = new PointOnWorldSphere (50.0, 89.0);
		C = GeometryCalc.getPointClosestToAPole(A, B); 
		assertEquals(-90.0, C.getLat(), delta );
	}
	@Test
	public void GetPointClosestToAPole_Test_0920() 
	{
		// pkt B blizszy biegunowi poludniowemu
		A = new PointOnWorldSphere (50.0, -45.0);
		B = new PointOnWorldSphere (50.0, 89.0);
		C = GeometryCalc.getPointClosestToAPole(A, B); 
		assertEquals(89.0, C.getLat(), delta );
	}
	@Test
	public void GetPointClosestToAPole_Test_0930() 
	{
		// pkt B blizszy biegunowi poludniowemu
		A = new PointOnWorldSphere (50.0, -45.0);
		B = new PointOnWorldSphere (50.0, 89.0);
		C = GeometryCalc.getPointClosestToAPole(A, B); 
		assertEquals(89.0, C.getLat(), delta );
	}
	@Test
	public void GetPointClosestToAPole_Test_0940() 
	{
		// pkt B blizszy biegunowi poludniowemu
		A = new PointOnWorldSphere (50.0, 5.0);
		B = new PointOnWorldSphere (50.0, 89.0);
		C = GeometryCalc.getPointClosestToAPole(A, B); 
		assertEquals(89.0, C.getLat(), delta );
	}
	

	
	/******************************************************************************************************************
	 *  getGreaterAngleInscribedOnLineSegment Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void getGreaterAngleInscribedOnLineSegment_Test_0950()
	{
		// kat zaczepiony w srodku kola na luku 60 stopni, kat oparty na tym luku ale zaczepiony na okregu ma 30 stopni, 
		// a kat przeciwny musi miec 150 stopni
		A = new PointOnWorldSphere (0.0, 0.0);
		B = new PointOnWorldSphere (60.0, 0.0);
		assertEquals(PI/6.0*5.0, GeometryCalc.getGreaterAngleInscribedOnLineSegment(A, B), delta );		
	}
	@Test
	public void GetAngleInscribedOnLineSegment_Test_0960()
	{
		// kat zaczepiony w srodku kola na luku 120 stopni, kat oparty na tym luku ale zaczepiony na okregu ma 60 stopni, 
		// a kat przeciwny musi miec 120 stopni
		A = new PointOnWorldSphere (0.0, 0.0);
		B = new PointOnWorldSphere (120.0, 0.0);
		assertEquals(PI/3.0*2.0, GeometryCalc.getGreaterAngleInscribedOnLineSegment(A, B), delta );		
	}
	@Test
	public void GetAngleInscribedOnLineSegment_Test_0970()
	{
		// kat zaczepiony w srodku kola na luku 90 stopni, kat oparty na tym luku ale zaczepiony na okregu ma 45 stopni, 
		// a kat przeciwny musi miec 135 stopni
		A = new PointOnWorldSphere (0.0, 0.0);
		B = new PointOnWorldSphere (90.0, 0.0);
		assertEquals(PI*3.0/4.0, GeometryCalc.getGreaterAngleInscribedOnLineSegment(A, B), delta );		
	}
	@Test
	public void GetAngleInscribedOnLineSegment_Test_0980()
	{
		// kat zaczepiony w srodku kola na luku 180 stopni, kat oparty na tym luku ale zaczepiony na okregu ma 90 stopni, 
		// a kat przeciwny musi miec 90 stopni
		A = new PointOnWorldSphere (0.0, 0.0);
		B = new PointOnWorldSphere (180.0, 0.0);
		assertEquals(PI/2.0, GeometryCalc.getGreaterAngleInscribedOnLineSegment(A, B), delta );		
	}
	
	
	
	/******************************************************************************************************************
	 *  isPointBetweenPointsOnEdge Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void isPointIsBetweenPointsOnEdge_Test_0990()
	{		
		A = new PointOnWorldSphere(45, 45.0);
		B = new PointOnWorldSphere(-45, -45.0);
		double beta = GeometryCalc.angleDistanceInRad(A, B);
		PointOnWorldSphere start = GeometryCalc.getPointBetween(A, B, beta/10.0);
		PointOnWorldSphere end = GeometryCalc.getPointBetween(A, B, 9*beta/10.0);
		PointOnWorldSphere between = GeometryCalc.getPointBetween(A, B, 5*beta/10.0);
		if(draw)
		{			
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawPointBetween(start, end, between, proj, testsResultsDirectory + "0990.png");
		}
		assertTrue( GeometryCalc.isPointBetweenPointsOnEdge(start, end, between));
	}
	@Test
	public void isPointIsBetweenPointsOnEdge_Test_1000()
	{
		PointOnWorldSphere start = new PointOnWorldSphere(45, 0.0);
		PointOnWorldSphere end = new PointOnWorldSphere(-45, -45.0);
		double beta = GeometryCalc.angleDistanceInRad(start, end);
		PointOnWorldSphere between = GeometryCalc.getPointBetween(start, end, beta*0.9999);
		if(draw)
		{
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawPointBetween(start, end, between, proj, testsResultsDirectory + "1000.png");
		}
		assertTrue( GeometryCalc.isPointBetweenPointsOnEdge(start, end, between) );
	}
	@Test
	public void isPointIsBetweenPointsOnEdge_Test_1010()
	{		
		PointOnWorldSphere start = new PointOnWorldSphere(45, 0.0);
		PointOnWorldSphere end = new PointOnWorldSphere(-45, -45.0);
		double beta = GeometryCalc.angleDistanceInRad(start, end);
		PointOnWorldSphere between = GeometryCalc.getPointBetween(start, end, beta*1.001);
		if(draw)
		{
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawPointBetween(start, end, between, proj, testsResultsDirectory + "1010.png");
		}
		assertTrue( !GeometryCalc.isPointBetweenPointsOnEdge(start, end, between) );
	}
	@Test
	public void isPointIsBetweenPointsOnEdge_Test_1020()
	{
		PointOnWorldSphere start = new PointOnWorldSphere(0.0, 89.0);
		PointOnWorldSphere end = new PointOnWorldSphere(90.0, 0.0);
		double beta = GeometryCalc.angleDistanceInRad(start, end);
		PointOnWorldSphere between = GeometryCalc.getPointBetween(start, end, beta*0.999);
		if(draw)
		{
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawPointBetween(start, end, between, proj, testsResultsDirectory + "1020.png");
		}
		assertTrue( GeometryCalc.isPointBetweenPointsOnEdge(start, end, between) );
	}
	@Test
	public void isPointIsBetweenPointsOnEdge_Test_1030()
	{
		PointOnWorldSphere start = new PointOnWorldSphere(0.0, 89.0);
		PointOnWorldSphere end = new PointOnWorldSphere(90.0, 0.0);
		double beta = GeometryCalc.angleDistanceInRad(start, end);
		PointOnWorldSphere between = GeometryCalc.getPointBetween(start, end, beta*1.001);
		if(draw)
		{
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawPointBetween(start, end, between, proj, testsResultsDirectory + "1030.png");
		}
		assertTrue( !GeometryCalc.isPointBetweenPointsOnEdge(start, end, between) );
	}
	@Test
	public void isPointIsBetweenPointsOnEdge_Test_1040()
	{
		PointOnWorldSphere start = new PointOnWorldSphere(0.0, 0.0);
		PointOnWorldSphere end = new PointOnWorldSphere(179.0, 1.0);
		double beta = GeometryCalc.angleDistanceInRad(start, end);
		PointOnWorldSphere between = GeometryCalc.getPointBetween(start, end, beta*0.5);
		if(draw)
		{
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawPointBetween(start, end, between, proj, testsResultsDirectory + "1040.png");
		}
		assertTrue( GeometryCalc.isPointBetweenPointsOnEdge(start, end, between) );		
	}
	@Test
	public void isPointIsBetweenPointsOnEdge_Test_1045()
	{
		PointOnWorldSphere start = new PointOnWorldSphere(0.0, 0.0);
		PointOnWorldSphere end = new PointOnWorldSphere(179.0, 1.0);
		double beta = GeometryCalc.angleDistanceInRad(start, end);
		PointOnWorldSphere between = GeometryCalc.getPointBetween(start, end, beta*0.1);
		if(draw)
		{
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawPointBetween(start, end, between, proj, testsResultsDirectory + "1045.png");
		}
		assertTrue( GeometryCalc.isPointBetweenPointsOnEdge(start, end, between) );
	}
	

	private void drawPointBetween(PointOnWorldSphere start, PointOnWorldSphere end, PointOnWorldSphere extremePoint, MapProjection proj, String fileName)
	{
		LineDrawing.drawGeodesic(proj, start, end, 0, Colors.violet);
		PointDrawing.markWithCrossOfColor(start, 5, proj, Colors.darkblue);
		PointDrawing.markWithCrossOfColor(end, 5, proj, Colors.darkblue);
		PointDrawing.markWithXOfColor(extremePoint, 9, proj, Colors.violet);	
		ImgProc.saveImage(proj.getImg(), fileName);
	}
	
	
	/******************************************************************************************************************
	 *  getTheHighestOrLowestPointOnGreatCircle Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void getTheHighestOrLowestPointOnGreatCircle_Test_1049()
	{
		A = new PointOnWorldSphere(0.0, 45.0);
		B = new PointOnWorldSphere (90, 0.0);
		PointOnWorldSphere extremePoint = GeometryCalc.getTheHighestOrLowestPointOnGreatCircle(A, B);
		if(draw)
		{
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawGreatCircleAndTheHighestOrLowestPoint(A, B, extremePoint, proj, testsResultsDirectory + "1049.png");
		}
		assertEquals(45.0, Math.abs(extremePoint.getLat()), delta);
	}
	@Test
	public void getTheHighestOrLowestPointOnGreatCircle_Test_1050()
	{		
		A = new PointOnWorldSphere(0.0, 0.0);
		B = new PointOnWorldSphere (179, -1.0);
		PointOnWorldSphere extremePoint = GeometryCalc.getTheHighestOrLowestPointOnGreatCircle(A, B);
		if(draw)
		{
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawGreatCircleAndTheHighestOrLowestPoint(A, B, extremePoint, proj, testsResultsDirectory + "1050.png");
		}
		assertEquals( 45.004363544655284, extremePoint.getLat(), delta);	
	}
		private void drawGreatCircleAndTheHighestOrLowestPoint(PointOnWorldSphere A, PointOnWorldSphere B, PointOnWorldSphere extremePoint, MapProjection proj, String fileName)
		{
			LineDrawing.drawGreatCircle(proj, A, B, 0, Colors.violet);
			PointDrawing.markWithCrossOfColor(A, 5, proj, Colors.darkblue);
			PointDrawing.markWithCrossOfColor(B, 5, proj, Colors.darkblue);
			PointDrawing.markPointWithSquareOfColor(extremePoint, 9, proj, Colors.violet);	
			ImgProc.saveImage(proj.getImg(), fileName);
		}
	
	/******************************************************************************************************************
	 *  getTheHighestOrLowestPointOnEdge Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void getTheHighestOrLowestPointOnEdge_Test_1090()
	{		
		PointOnWorldSphere start = new PointOnWorldSphere (0.0, 0.0);
		PointOnWorldSphere end = new PointOnWorldSphere (0.0, 45.0);
		PointOnWorldSphere extremePoint = GeometryCalc.getTheHighestOrLowestPointOnEdge(start, end);
		if(draw)
		{
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawEdgeAndTheHighestOrLowestPoint(start, end, extremePoint, proj, testsResultsDirectory + "1090.png");
		}
		assertEquals(45.0, extremePoint.getLat(), delta);
	}
	@Test
	public void getTheHighestOrLowestPointOnEdge_Test_1100()
	{	
		PointOnWorldSphere start = new PointOnWorldSphere (0.0, 0.0);
		PointOnWorldSphere end = new PointOnWorldSphere (-80, 0.0);
		PointOnWorldSphere extremePoint = GeometryCalc.getTheHighestOrLowestPointOnEdge(start, end);
		if(draw)
		{
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawEdgeAndTheHighestOrLowestPoint(start, end, extremePoint, proj,testsResultsDirectory + "1100.png");
		}
		assertEquals(0.0, extremePoint.getLat(), delta);
	}
	@Test
	public void getTheHighestOrLowestPointOnEdge_Test_1110()
	{
		PointOnWorldSphere start = new PointOnWorldSphere (0.0, 45.0);
		PointOnWorldSphere end = new PointOnWorldSphere (90, 0.0);
		PointOnWorldSphere extremePoint = GeometryCalc.getTheHighestOrLowestPointOnEdge(start, end);
		if(draw)
		{
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawEdgeAndTheHighestOrLowestPoint(start, end, extremePoint, proj, testsResultsDirectory + "1110.png");
		}
		assertEquals(45.0, extremePoint.getLat(), delta);
	}
	@Test
	public void getTheHighestOrLowestPointOnEdge_Test_1120()
	{		
		PointOnWorldSphere start = new PointOnWorldSphere (0.0, 0.0);
		PointOnWorldSphere end = new PointOnWorldSphere (179, -1.0);
		PointOnWorldSphere extremePoint = GeometryCalc.getTheHighestOrLowestPointOnEdge(start, end);
		if(draw)
		{
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawEdgeAndTheHighestOrLowestPoint(start, end, extremePoint, proj, testsResultsDirectory + "1120.png");
		}		
		assertEquals(-45.004363544655284, extremePoint.getLat(), delta);
	}
	@Test
	public void getTheHighestOrLowestPointOnEdge_Test_1121()
	{
		
		PointOnWorldSphere start = new PointOnWorldSphere (0.0, 0.0);
		PointOnWorldSphere end = new PointOnWorldSphere (180, 60.0);
		PointOnWorldSphere extremePoint = GeometryCalc.getTheHighestOrLowestPointOnEdge(start, end);
		if(draw)
		{
			MapProjection proj = new Equirectangular(TestHelper.tinyMapPath);
			drawEdgeAndTheHighestOrLowestPoint(start, end, extremePoint, proj, testsResultsDirectory + "1121.png");
		}		
		assertEquals(90.0, extremePoint.getLat(), delta);
	}
	
		private void drawEdgeAndTheHighestOrLowestPoint(PointOnWorldSphere start, PointOnWorldSphere end, PointOnWorldSphere extremePoint, MapProjection proj, String fileName)
		{
			LineDrawing.drawGeodesic(proj, start, end, 0, Colors.violet);
			PointDrawing.markWithCrossOfColor(start, 5, proj, Colors.darkblue);
			PointDrawing.markWithCrossOfColor(end, 5, proj, Colors.darkblue);
			PointDrawing.markPointWithSquareOfColor(extremePoint, 9, proj, Colors.violet);	
			ImgProc.saveImage(proj.getImg(), fileName);
		}
		
	
	/******************************************************************************************************************
	 *  getHalfOfTheDistanceOfTheIntersectionsOfNaviCircle  Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void getHalfOfTheDistanceOfTheIntersectionsOfNaviCircle_Test_1130()
	{
		A = new PointOnWorldSphere(0.0, 0.0);
		B = new PointOnWorldSphere(40.0, 0.0);
		GeometryCalc.getHalfOfTheDistanceOfTheIntersectionsOfNaviCircles(A, B, 4000.0);
		
	}
	
	
	
	/******************************************************************************************************************
	 *  isPointInNavRange  Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void isPointInNavRange_Test_1140()
	{
		A = new PointOnWorldSphere(0.0, 0.0);
		B = new PointOnWorldSphere(40.0, 0.0);		
		assertTrue( GeometryCalc.isPointInNavRange(A, B, Transl.degreesToKmOnEquator(40.05)));
	}
	
	@Test
	public void isPointInNavRange_Test_1150()
	{
		A = new PointOnWorldSphere(0.0, 0.0);
		B = new PointOnWorldSphere(40.0, 0.0);
		assertTrue( !GeometryCalc.isPointInNavRange(A, B, Transl.degreesToKmOnEquator(39.995)));
	}
	
	
	
	
	/******************************************************************************************************************
	 *  findAnyPointOfIntersactionOfTwoPlanes   Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void findAnyPointOfIntersactionOfTwoPlanes_Test_1155()	
	{		
		PointOnWorldSphere pointA = new PointOnWorldSphere(0.0, 45.0); 	
		PointOnWorldSphere pointB = new PointOnWorldSphere(-60.0, 45.0);  
		PointOnWorldSphere pointC = new PointOnWorldSphere(-30.0, 45.0);
		double xc = Transl.coordToCartX(pointC);
		double yc = Transl.coordToCartY(pointC);
		double zc = Transl.coordToCartZ(pointC);
		double[] normVector = GeometryCalc.crossProduct(pointA, pointB);
		
	    // jedna z plaszczyzn jest definiowana za pomoca krawedzi rozpinanej przez punkty pointA i pointB
		// druga plaszczyzna jest prostopadla do wektora zaczeopionego w punktach pointC oraz (0, 0 ,0)
		double A1 = normVector[0];
	    double B1 = normVector[1];
	    double C1 = normVector[2];
	    double D1 = 0;
	    double A2 = xc;
	    double B2 = yc;
	    double C2 = zc;
	    double D2 = WorldConstants.EARTH_RADIUS_LENGTH;

		double[] P = GeometryCalc.findAnyPointOfIntersactionOfTwoPlanes(A1, B1, C1, D1, A2, B2, C2, D2);		
		assertEquals(0, GeometryCalc.distanceOfPointToPlane(P[0], P[1], P[2], A1, B1, C1, D1), delta);
		assertEquals(0, GeometryCalc.distanceOfPointToPlane(P[0], P[1], P[2], A2, B2, C2, D2), delta);
	}
	
	@Test
	public void findAnyPointOfIntersactionOfTwoPlanes_Test_1156()	
	{
		// Dwie trywialne plaszczyzny jedna to plaszczyzna z = 0, druga to x = 0;
		double A1 = 0;
	    double B1 = 0;
	    double C1 = 1;
	    double D1 = 0;
	    double A2 = 1;
	    double B2 = 0;
	    double C2 = 0;
	    double D2 = 0;
		double[] P =GeometryCalc.findAnyPointOfIntersactionOfTwoPlanes(A1, B1, C1, D1, A2, B2, C2, D2);				
		assertEquals(0, GeometryCalc.distanceOfPointToPlane(P[0], P[1], P[2], A1, B1, C1, D1), delta);
		assertEquals(0, GeometryCalc.distanceOfPointToPlane(P[0], P[1], P[2], A2, B2, C2, D2), delta);
	}
	
	@Test
	public void findAnyPointOfIntersactionOfTwoPlanes_Test_1157()	
	{		
		// Dwie trywialne plaszczyzny jedna to plaszczyzna z = 0, druga to y = 0;
		double A1 = 0;
	    double B1 = 0;
	    double C1 = 1;
	    double D1 = 0;
	    double A2 = 0;
	    double B2 = 1;
	    double C2 = 0;
	    double D2 = 0;
		double[] P =GeometryCalc.findAnyPointOfIntersactionOfTwoPlanes(A1, B1, C1, D1, A2, B2, C2, D2);				
		assertEquals(0, GeometryCalc.distanceOfPointToPlane(P[0], P[1], P[2], A1, B1, C1, D1), delta);
		assertEquals(0, GeometryCalc.distanceOfPointToPlane(P[0], P[1], P[2], A2, B2, C2, D2), delta);
	}
	
	@Test
	public void findAnyPointOfIntersactionOfTwoPlanes_Test_1158()	
	{		

		// Dwie trywialne plaszczyzny jedna to plaszczyzna x = 0, druga to y = 0;
		double A1 = 1;
	    double B1 = 0;
	    double C1 = 0;
	    double D1 = 0;
	    double A2 = 0;
	    double B2 = 1;
	    double C2 = 0;
	    double D2 = 0;
		double[] P =GeometryCalc.findAnyPointOfIntersactionOfTwoPlanes(A1, B1, C1, D1, A2, B2, C2, D2);				
		assertEquals(0, GeometryCalc.distanceOfPointToPlane(P[0], P[1], P[2], A1, B1, C1, D1), delta);
		assertEquals(0, GeometryCalc.distanceOfPointToPlane(P[0], P[1], P[2], A2, B2, C2, D2), delta);
	}
	
	@Test
	public void findAnyPointOfIntersactionOfTwoPlanes_Test_1159()	
	{		

		// pierwsza plaszczyzna X + Z = 0 ; druga plaszczyzna x = 1
		double A1 = -1;
	    double B1 = 0;
	    double C1 = 1;
	    double D1 = 0;
	    double A2 = 1;
	    double B2 = 0;
	    double C2 = 0;
	    double D2 = 1;
		double[] P = GeometryCalc.findAnyPointOfIntersactionOfTwoPlanes(A1, B1, C1, D1, A2, B2, C2, D2);			
		assertEquals(0, GeometryCalc.distanceOfPointToPlane(P[0], P[1], P[2], A1, B1, C1, D1), delta);
		assertEquals(0, GeometryCalc.distanceOfPointToPlane(P[0], P[1], P[2], A2, B2, C2, D2), delta);
	}
	
	/******************************************************************************************************************
	 *  getIntersOfEdgeAndNaviCircle   Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void getIntersOfEdgeAndNaviCircle_Test_1160()
	{
		A = new PointOnWorldSphere(0.0, 45.0);
		B = new PointOnWorldSphere(-90.0, 0.0);
		C = new PointOnWorldSphere(0.0, 90.0);		
		double naviRadiusKm = Transl.degreesToKmOnEquator(44.0);  // promien nawigacji to prawie 1/8 obwodu ziemi.		
		
		PointOnWorldSphere[] intersections = GeometryCalc.findIntersOfEdgeAndNaviCircle(A, B, C, naviRadiusKm);
		assertEquals(0, intersections.length, delta);		
		
		if( draw )
			drawIntersectionsAndEdge(TestHelper.tinyMapPath,
					testsResultsDirectory + "1160.png", A, B, C, naviRadiusKm, intersections);
	}
	@Test
	public void getIntersOfEdgeAndNaviCircle_Test_1170()
	{
		A = new PointOnWorldSphere(0.0, 45.0);
		B = new PointOnWorldSphere(-90.0, 0.0);
		C = new PointOnWorldSphere(0.0, 90.0);		
		double naviRadiusKm = Transl.degreesToKmOnEquator(45.5);  // promien nawigacji to troche wiecej niz 1/8 obwodu ziemi. 				
		
		PointOnWorldSphere[] intersections = GeometryCalc.findIntersOfEdgeAndNaviCircle(A, B, C, naviRadiusKm);
		assertEquals(1, intersections.length, delta);
		
		if( draw )
			drawIntersectionsAndEdge(TestHelper.tinyMapPath, testsResultsDirectory + "1170.png", A, B, C, naviRadiusKm, intersections);		

		assertEquals(-10.673902586596853, intersections[0].getLongit(), delta);
		assertEquals(44.499999999999986, intersections[0].getLat(), delta);		 
		
		MapProjection proj = new Equirectangular(testsResultsDirectory + "1170.png");
		assertEquals(1, intersections.length, delta);
		assertTrue(Colors.darkblue == proj.getRGB(634, 503));
	}
	@Test
	public void getIntersOfEdgeAndNaviCircle_Test_1180()
	{
		// Ponizsze dwa punkty okreslaja krawedz przechodzaca w najwyzszym punkcie przez rownoleznik 45
		A = new PointOnWorldSphere(0.0, 45.0);
		B = new PointOnWorldSphere(-90.0, 0.0);
		
		// chcemy aby krawedz znalezione ponizej dwa punkty przeciec nalezaly do krawedzi
		PointOnWorldSphere Bprim = GeometryCalc.getPointBetween(A, new PointOnWorldSphere(90.0, 0.0), PI/4);
		
		C = new PointOnWorldSphere(0.0, 90.0);		
		double naviRadiusKm = Transl.degreesToKmOnEquator(44.5);  // promien nawigacji to troche mniej niz 1/8 obwodu ziemi. 				
		
		PointOnWorldSphere[] intersections = GeometryCalc.findIntersOfEdgeAndNaviCircle(A, B, C, naviRadiusKm);
		assertEquals(0, intersections.length, delta);
		
		if( draw )
			drawIntersectionsAndEdge(TestHelper.tinyMapPath,
					testsResultsDirectory + "1180.png", A, Bprim, C, naviRadiusKm, intersections);
	}
	@Test
	public void getIntersOfEdgeAndNaviCircle_Test_1190()
	{

		A = new PointOnWorldSphere(-5.0, 0.0);  // poczatek i....	
		B = new PointOnWorldSphere(5.0, 0.0);   // ....koniec krawedzi leza na rowniku 
		C = new PointOnWorldSphere(0.0, 0.0);   		// centrum nawigacji lezy na rowniku		
		double naviRadiusKm = Transl.degreesToKmOnEquator(2);   				
		
		PointOnWorldSphere[] intersections = GeometryCalc.findIntersOfEdgeAndNaviCircle(A, B, C, naviRadiusKm);
		assertEquals(2, intersections.length, delta);	
		
		if( draw )
			drawIntersectionsAndEdge(TestHelper.tinyMapPath,
					testsResultsDirectory + "1190.png", A, B, C, naviRadiusKm, intersections);

		assertEquals(-1.9999999999996163, intersections[0].getLongit(), delta);
		assertEquals(0, intersections[0].getLat(), delta);
		assertEquals(1.9999999999996163, intersections[1].getLongit(), delta);
		assertEquals(0.0, intersections[1].getLat(), delta);		
		
		MapProjection proj = new Equirectangular(testsResultsDirectory + "1190.png");
		assertEquals(2, intersections.length, delta);
		assertTrue(Colors.darkblue == proj.getRGB(666, 336));
		assertTrue(Colors.darkblue == proj.getRGB(681, 336));
	}
	@Test
	public void getIntersOfEdgeAndNaviCircle_Test_1200()
	{
		A = new PointOnWorldSphere(3.0, 45.0); 	
		B = new PointOnWorldSphere(-3.0, 45.0);  
		C = new PointOnWorldSphere(0.0, 45.0);		
		double naviRadiusKm = Transl.degreesToKmOnEquator(2);   				
		
		PointOnWorldSphere[] intersections = GeometryCalc.findIntersOfEdgeAndNaviCircle(A, B, C, naviRadiusKm);
		assertEquals(2, intersections.length, delta);
		
		if( draw )
			drawIntersectionsAndEdge(TestHelper.tinyMapPath,
					testsResultsDirectory + "1200.png", A, B, C, naviRadiusKm, intersections);		

		assertEquals(2.8286717131761208, intersections[0].getLongit(), delta);
		assertEquals(45.0043610506616, intersections[0].getLat(), delta);
		assertEquals(-2.8286717131761208, intersections[1].getLongit(), delta);
		assertEquals(45.0043610506616, intersections[1].getLat(), delta);
		
		MapProjection proj = new Equirectangular(testsResultsDirectory + "1200.png");
		assertEquals(2, intersections.length, delta);
		assertTrue(Colors.darkblue == proj.getRGB(663, 505));
		assertTrue(Colors.darkblue == proj.getRGB(684, 505));
	}
	@Test
	public void getIntersOfEdgeAndNaviCircle_Test_1210()
	{

		A = new PointOnWorldSphere(0.0, 45.0); 	
		B = new PointOnWorldSphere(-60.0, 45.0);  
		C = new PointOnWorldSphere(-30.0, 45.0);		
		double naviRadiusKm = Transl.degreesToKmOnEquator(10);   				
		
		PointOnWorldSphere[] intersections = GeometryCalc.findIntersOfEdgeAndNaviCircle(A, B, C, naviRadiusKm);
		assertEquals(2, intersections.length, delta);
		
		if( draw )
			drawIntersectionsAndEdge(TestHelper.tinyMapPath,
					testsResultsDirectory + "1210.png", A, B, C, naviRadiusKm, intersections);
		
		assertEquals(-16.213532830178053, intersections[0].getLongit(), delta);
		assertEquals(48.27612720275657, intersections[0].getLat(), delta);
		assertEquals(-43.78646716982194, intersections[1].getLongit(), delta);
		assertEquals(48.27612720275657, intersections[1].getLat(), delta);

		MapProjection proj = new Equirectangular(testsResultsDirectory + "1210.png");
		assertEquals(2, intersections.length, delta);
		assertTrue(Colors.darkblue == proj.getRGB(509, 517));
		assertTrue(Colors.darkblue == proj.getRGB(613, 517));
	}
	@Test
	public void getIntersOfEdgeAndNaviCircle_Test_1220()
	{

		A = new PointOnWorldSphere(30.0, 49.0); 	
		B = new PointOnWorldSphere(-30.0, 45.0);  
		C = new PointOnWorldSphere(0.0, 48.0);		
		double naviRadiusKm = Transl.degreesToKmOnEquator(5);   				
		
		PointOnWorldSphere[] intersections = GeometryCalc.findIntersOfEdgeAndNaviCircle(A, B, C, naviRadiusKm);
		assertEquals(2, intersections.length, delta);
		
		if( draw )
			drawIntersectionsAndEdge(TestHelper.tinyMapPath,
					testsResultsDirectory + "1220.png", A, B, C, naviRadiusKm, intersections);
		
		assertEquals(5.74467169310698, intersections[0].getLongit(), delta);
		assertEquals(51.347611217319404, intersections[0].getLat(), delta);
		assertEquals(-6.580329659308532, intersections[1].getLongit(), delta);
		assertEquals(50.56963304305087, intersections[1].getLat(), delta);

		MapProjection proj = new Equirectangular(testsResultsDirectory + "1220.png");
		assertEquals(2, intersections.length, delta);
		assertTrue(Colors.darkblue == proj.getRGB(649, 525));
		assertTrue(Colors.darkblue == proj.getRGB(695, 528));
	}
	
	@Test
	public void getIntersOfEdgeAndNaviCircle_Test_1221()
	{

		A = new PointOnWorldSphere(30.0, 0.0); 	
		B = new PointOnWorldSphere(-30.0, 0.0);  
		C = new PointOnWorldSphere(0.0, 0.0);		
		double naviRadiusKm = Transl.degreesToKmOnEquator(15);   				
		
		PointOnWorldSphere[] intersections = GeometryCalc.findIntersOfEdgeAndNaviCircle(A, B, C, naviRadiusKm);
		assertEquals(2, intersections.length, delta);
		
		if( draw )
			drawIntersectionsAndEdge(TestHelper.tinyMapPath,
					testsResultsDirectory + "1221.png", A, B, C, naviRadiusKm, intersections);
		
		assertEquals(15.000000000000043, intersections[0].getLongit(), delta);
		assertEquals(0, intersections[0].getLat(), delta);
		assertEquals(-15.000000000000043, intersections[1].getLongit(), delta);
		assertEquals(0, intersections[1].getLat(), delta);
		
		MapProjection proj = new Equirectangular(testsResultsDirectory + "1221.png");
		assertEquals(2, intersections.length, delta);
		assertTrue(Colors.darkblue == proj.getRGB(617, 336));
		assertTrue(Colors.darkblue == proj.getRGB(730, 336));
	}	
	
	@Test
	public void getIntersOfEdgeAndNaviCircle_Test_1222()
	{

		A = new PointOnWorldSphere(30.0, -10.0); 	
		B = new PointOnWorldSphere(-45.0, 10.0);  
		C = new PointOnWorldSphere(0.0, 0.0);		
		double naviRadiusKm = Transl.degreesToKmOnEquator(15);   				
		
		PointOnWorldSphere[] intersections = GeometryCalc.findIntersOfEdgeAndNaviCircle(A, B, C, naviRadiusKm);
		assertEquals(2, intersections.length, delta);
		
		if( draw )
			drawIntersectionsAndEdge(TestHelper.tinyMapPath,
					testsResultsDirectory + "1222.png", A, B, C, naviRadiusKm, intersections);

		assertEquals(13.773387321927203, intersections[0].getLongit(), delta);
		assertEquals(-5.999187850685792, intersections[0].getLat(), delta);
		assertEquals(-14.852442986241002, intersections[1].getLongit(), delta);
		assertEquals(2.122813979831918, intersections[1].getLat(), delta);
		
		MapProjection proj = new Equirectangular(testsResultsDirectory + "1222.png");
		assertEquals(2, intersections.length, delta);
		assertTrue(Colors.darkblue == proj.getRGB(618, 344));
		assertTrue(Colors.darkblue == proj.getRGB(725, 314));
	}	
	
	@Test
	public void getIntersOfEdgeAndNaviCircle_Test_1223()
	{

		A = new PointOnWorldSphere(-179.0, 89.0); 	
		B = new PointOnWorldSphere(-170.0, 70.0);  
		C = new PointOnWorldSphere(-174, 80.0);		
		double naviRadiusKm = Transl.degreesToKmOnEquator(4);   				
		
		PointOnWorldSphere[] intersections = GeometryCalc.findIntersOfEdgeAndNaviCircle(A, B, C, naviRadiusKm);
		
		
		if( draw ){
			drawIntersectionsAndEdge(TestHelper.tinyMapPath,
					testsResultsDirectory + "1223.png", A, B, C, naviRadiusKm, intersections);
		}
				
		assertEquals(-171.10647635983076, intersections[0].getLongit(), delta);
		assertEquals(83.98088638593002, intersections[0].getLat(), delta);
		assertEquals(-170.21120379427788, intersections[1].getLongit(), delta);
		assertEquals(76.07572997381313, intersections[1].getLat(), delta);
		
		MapProjection proj = new Equirectangular(testsResultsDirectory + "1223.png");
		assertEquals(2, intersections.length, delta);
		assertTrue(Colors.darkblue == proj.getRGB(36, 621));
		assertTrue(Colors.darkblue == proj.getRGB(32, 650));						
	}		
	
		private void drawIntersectionsAndEdge(String inputFileName, String outputFileName, PointOnWorldSphere A, PointOnWorldSphere B, PointOnWorldSphere naviCenter, 
				double naviRadiusKm, PointOnWorldSphere[] intersections)
		{
			MapProjection proj = new Equirectangular(inputFileName);
			
			LineDrawing.drawNavCircle(proj, naviCenter, naviRadiusKm, 0, Colors.violet);
			LineDrawing.drawGeodesic(proj, A, B, 0, Colors.orange);
			
			for(int i = 0; i < intersections.length; i++)		
				PointDrawing.markWithXOfColor(intersections[i], 4, proj, Colors.darkblue);			
			
						
			ImgProc.saveImage(proj.getImg(), outputFileName);
		}
	
	
	
	/******************************************************************************************************************
	 *  distanceOfNaviCircleToSphereCenter   Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void distanceOfNaviCircleToSphereCenter_Test_1300()
	{
		double navRadius = WorldConstants.EARTH_RADIUS_LENGTH * PI/3;
		assertEquals(WorldConstants.EARTH_RADIUS_LENGTH/2.0,
				GeometryCalc.distanceOfNaviCircleToSphereCenter(navRadius), delta);
	}
	
	@Test	
	public void distanceOfNaviCircleToSphereCenter_Test_1310()
	{
		double navRadius = WorldConstants.EARTH_RADIUS_LENGTH * PI/4;
		assertEquals(WorldConstants.EARTH_RADIUS_LENGTH/Math.sqrt(2.0),
				GeometryCalc.distanceOfNaviCircleToSphereCenter(navRadius), delta);
	}
	
	@Test	
	public void distanceOfNaviCircleToSphereCenter_Test_1320()
	{
		double navRadius = WorldConstants.EARTH_RADIUS_LENGTH * PI/2;
		assertEquals(0,
				GeometryCalc.distanceOfNaviCircleToSphereCenter(navRadius), delta);
	}
	
	
	/******************************************************************************************************************
	 *  determinant2x2   Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void determinant2x2_Test_1330()
	{
		// 0 0
		// 0 0
		assertEquals(0.0, GeometryCalc.determinant2x2(0, 0, 0, 0), delta);
	}
	@Test
	public void determinant2x2_Test_1340()
	{
		// 1 0
		// 0 3
		assertEquals(3.0, GeometryCalc.determinant2x2(1, 0, 0, 3), delta);
	}
	@Test
	public void determinant2x2_Test_1350()
	{
		//  0 1
		// -1 3
		assertEquals(3.0, GeometryCalc.determinant2x2(0, 1, -3, 3), delta);
	}
	
	/******************************************************************************************************************
	 *  crossProduct   Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void crossProduct_Test_1360()
	{													//xa ya za xb yb zb
		double[] crossProduct = GeometryCalc.crossProduct(1, 0, 0, 0, 1, 0);
		assertEquals(crossProduct[0], 0, delta);
		assertEquals(crossProduct[1], 0, delta);
		assertEquals(crossProduct[2], 1, delta);
	}
	@Test
	public void crossProduct_Test_1370()
	{													//xa ya za xb yb zb
		double[] crossProduct = GeometryCalc.crossProduct(1, 1, 0, 1, 1, 0);
		assertEquals(crossProduct[0], 0, delta);
		assertEquals(crossProduct[1], 0, delta);
		assertEquals(crossProduct[2], 0, delta);
	}
	@Test
	public void crossProduct_Test_1380()
	{													//xa ya za xb yb zb
		double[] crossProduct = GeometryCalc.crossProduct(0, 1, 0, 1, 0, 1);
		assertEquals(crossProduct[0], 1, delta);
		assertEquals(crossProduct[1], 0, delta);
		assertEquals(crossProduct[2], -1, delta);
	}
	
	/******************************************************************************************************************
	 *  dotProduct   Method TEST 
	 *****************************************************************************************************************/
	
	// 3D VECTOR variant
	@Test
	public void dotProduct_Test_1390()
	{										      //xa ya za xb yb zb
		double dotProduct = GeometryCalc.dotProduct(0, 0, 1, 1, 0, 1);
		assertEquals(dotProduct, 1, delta);
	}
	@Test
	public void dotProduct_Test_1400()
	{											   //xa ya za xb yb zb
		double dotProduct = GeometryCalc.dotProduct(-1, 0, 1, 1, 0, 1);
		assertEquals(dotProduct, 0, delta);
	}
	@Test
	public void dotProduct_Test_1410()
	{											   //xa ya za xb yb zb
		double dotProduct = GeometryCalc.dotProduct(-1, 0, 1, 1, 0, 1);
		assertEquals(dotProduct, 0.0, delta);
	}
	@Test
	public void dotProduct_Test_1420()
	{                    						   //xa ya za xb yb zb
		double dotProduct = GeometryCalc.dotProduct(-1, 4, 1, 6, 0, 1);
		assertEquals(dotProduct, -5.0, delta);
	}
	@Test
	public void dotProduct_Test_1430()
	{                    						   //xa ya za xb yb zb
		double dotProduct = GeometryCalc.dotProduct(-1, 0, 1, -5, 10, 1);
		assertEquals(dotProduct, 6.0, delta);
	}
	
	
	// 2D VECTOR variant
	@Test
	public void dotProduct_Test_1440()
	{										      //xa ya xb yb
		double dotProduct = GeometryCalc.dotProduct(0, 0, 1, 0);
		assertEquals(dotProduct, 0, delta);
	}
	@Test
	public void dotProduct_Test_1450()
	{											   //xa ya xb yb
		double dotProduct = GeometryCalc.dotProduct(-1, 0, 0, 1);
		assertEquals(dotProduct, 0, delta);
	}
	@Test
	public void dotProduct_Test_1460()
	{											   //xa ya xb yb
		double dotProduct = GeometryCalc.dotProduct(-1, 0, 1, 0);
		assertEquals(dotProduct, -1.0, delta);
	}
	@Test
	public void dotProduct_Test_1470()
	{                    						   //xa ya xb yb
		double dotProduct = GeometryCalc.dotProduct(-1, 4, 6, 0);
		assertEquals(dotProduct, -6.0, delta);
	}
	@Test
	public void dotProduct_Test_1480()
	{                    						   //xa ya xb yb
		double dotProduct = GeometryCalc.dotProduct(-1, 0, -5, 10);
		assertEquals(dotProduct, 5.0, delta);
	}
	
	
	/******************************************************************************************************************
	 * angleBetweenVectorsInRad    Method TEST 
	 ******************************************************************************************************************/
	
	@Test
	public void angleBetweenVectorsInRad_Test_1490()
	{
		double angle = GeometryCalc.angleBetweenVectorsInRad(1, -1, 1, 1);
		assertEquals(PI/2, angle, delta);
	}
	@Test
	public void angleBetweenVectorsInRad_Test_1500()
	{
		double angle = GeometryCalc.angleBetweenVectorsInRad(1, 0, -1, 0);
		assertEquals(PI, angle, delta);
	}
	@Test
	public void angleBetweenVectorsInRad_Test_1510()
	{
		double angle = GeometryCalc.angleBetweenVectorsInRad(1, 1, 1, 1);
		assertEquals(0, angle, delta*10000);
	}
	@Test
	public void angleBetweenVectorsInRad_Test_1511()
	{
		double angle = GeometryCalc.angleBetweenVectorsInRad(1, 2, 2.5, 5);
		assertEquals(0, angle, delta*10000);
	}	
	@Test
	public void angleBetweenVectorsInRad_Test_1520()
	{
		double angle = GeometryCalc.angleBetweenVectorsInRad(Math.sqrt(3)/2.0, 1/2.0, 1/2.0, Math.sqrt(3)/2.0);
		assertEquals(PI/6, angle, delta);
	}
	
	/******************************************************************************************************************
	 *  distanceOfPointToPlane   Method TEST 
	 *****************************************************************************************************************/
	
	@Test
	public void distanceOfPointToPlane_Test_1530()
	{
		double Px = 0;
		double Py = 0;
		double Pz = 0;
		double A = 0;
		double B = 0;
		double C = 5;
		double D = 0;
		assertEquals(0, GeometryCalc.distanceOfPointToPlane(Px, Py, Pz, A, B, C, D), delta);
	}
	@Test
	public void distanceOfPointToPlane_Test_1540()
	{
		double Px = 0;
		double Py = 5;
		double Pz = 1;
		double A = 1;
		double B = 0;
		double C = 1;
		double D = 0;
		assertEquals(Math.sqrt(2)/2, GeometryCalc.distanceOfPointToPlane(Px, Py, Pz, A, B, C, D), delta);
	}	
	@Test
	public void distanceOfPointToPlane_Test_1550()
	{
		double Px = 0;
		double Py = 0;
		double Pz = 1;
		double A = 0;
		double B = 0;
		double C = 1;
		double D = 0;
		assertEquals(1, GeometryCalc.distanceOfPointToPlane(Px, Py, Pz, A, B, C, D), delta);
	}
	@Test
	public void distanceOfPointToPlane_Test_1560()
	{
		double Px = 0;
		double Py = 0;
		double Pz = 1;
		double A = 0;
		double B = 0;
		double C = 1;
		double D = 0;
		assertEquals(1, GeometryCalc.distanceOfPointToPlane(Px, Py, Pz, A, B, C, D), delta);
	}
	@Test
	public void distanceOfPointToPlane_Test_1570()
	{
		double Px = 1;
		double Py = 0;
		double Pz = 1;
		double A = 1;
		double B = 0;
		double C = 0;
		double D = 1;
		assertEquals(0, GeometryCalc.distanceOfPointToPlane(Px, Py, Pz, A, B, C, D), delta);
	}
	
	/******************************************************************************************************************
	 *  edgeInNavigationCircles   Method TEST 
	 *****************************************************************************************************************/
	
	public void edgeInNavigationCircles_Test_1579()
	{
		A = new PointOnWorldSphere(10, 50);
		B = new PointOnWorldSphere(60, 30);		
		PointOnWorldSphere temp1 = new PointOnWorldSphere(30, 20);
		PointOnWorldSphere[] centers = new PointOnWorldSphere[]{temp1};
				
		double naviRadiusKm = Transl.degreesToKmOnEquator(50);
		double[] naviRadiusesKm = new double[]{naviRadiusKm};
		assertTrue( GeometryCalc.edgeInNavigationCircles(A, B, centers, naviRadiusesKm) );
		
		drawCirclesAndEdge(TestHelper.tinyMapPath, 
				testsResultsDirectory + "1579.png", A, B, centers, naviRadiusesKm);
	}
	@Test
	public void edgeInNavigationCircles_Test_1580()
	{
		A = new PointOnWorldSphere(-50, 0);
		B = new PointOnWorldSphere(50, 0);
		PointOnWorldSphere temp1 = new PointOnWorldSphere(-30, 0);
		PointOnWorldSphere temp2 = new PointOnWorldSphere(-0, 0);
		PointOnWorldSphere temp3 = new PointOnWorldSphere(30, 0);
		PointOnWorldSphere[] centers = new PointOnWorldSphere[]{temp1, temp2, temp3};
		
		double naviRadiusKm = Transl.degreesToKmOnEquator(25);
		double[] naviRadiusesKm = new double[]{naviRadiusKm, naviRadiusKm, naviRadiusKm};
		assertTrue( GeometryCalc.edgeInNavigationCircles(A, B, centers, naviRadiusesKm) );
		
		drawCirclesAndEdge(TestHelper.tinyMapPath, 
				testsResultsDirectory + "1580.png", A, B, centers, naviRadiusesKm);
	}
	@Test
	public void edgeInNavigationCircles_Test_1590()
	{
		A = new PointOnWorldSphere(-50, 0);
		B = new PointOnWorldSphere(50, 50);
		PointOnWorldSphere temp1 = new PointOnWorldSphere(-30, 0);
		PointOnWorldSphere temp2 = new PointOnWorldSphere(-0, 0);
		PointOnWorldSphere temp3 = new PointOnWorldSphere(50, 40);
		PointOnWorldSphere[] centers = new PointOnWorldSphere[]{temp1, temp2, temp3};
		
		double naviRadiusKm = Transl.degreesToKmOnEquator(25);
		double[] naviRadiusesKm = new double[]{naviRadiusKm, naviRadiusKm, naviRadiusKm*0.5};
				
		drawCirclesAndEdge(TestHelper.tinyMapPath, 
				testsResultsDirectory + "1590.png", A, B, centers, naviRadiusesKm);
		assertTrue( !GeometryCalc.edgeInNavigationCircles(A, B, centers, naviRadiusesKm) );
	}
	@Test
	public void edgeInNavigationCircles_Test_1600()
	{
		A = new PointOnWorldSphere(-45, 45);
		B = new PointOnWorldSphere(45, 45);

		PointOnWorldSphere temp1 = new PointOnWorldSphere(-30, 40);
		PointOnWorldSphere temp2 = new PointOnWorldSphere(30, 40);
		PointOnWorldSphere[] centers = new PointOnWorldSphere[]{temp1, temp2};
		
		double naviRadiusKm = Transl.degreesToKmOnEquator(25);
		double[] naviRadiusesKm = new double[]{naviRadiusKm, naviRadiusKm*0.5};
				
		drawCirclesAndEdge(TestHelper.tinyMapPath, 
				testsResultsDirectory + "1600.png", A, B, centers, naviRadiusesKm);
		assertTrue( !GeometryCalc.edgeInNavigationCircles(A, B, centers, naviRadiusesKm) );
	}
	
	@Test
	public void edgeInNavigationCircles_Test_1610()
	{
		A = new PointOnWorldSphere(-141, 5);
		B = new PointOnWorldSphere(121, -5);

		PointOnWorldSphere temp1 = new PointOnWorldSphere(-160, 5);
		PointOnWorldSphere temp2 = new PointOnWorldSphere(170, 1);
		PointOnWorldSphere temp3 = new PointOnWorldSphere(140, -5);
		PointOnWorldSphere[] centers = new PointOnWorldSphere[]{temp1, temp2, temp3};
			
		double naviRadiusKm = Transl.degreesToKmOnEquator(20);
		double[] naviRadiusesKm = new double[]{naviRadiusKm, naviRadiusKm*0.8, naviRadiusKm};
				
		drawCirclesAndEdge(TestHelper.tinyMapPath, 
				testsResultsDirectory + "1610.png", A, B, centers, naviRadiusesKm);
		assertTrue( GeometryCalc.edgeInNavigationCircles(A, B, centers, naviRadiusesKm) );
	}
	@Test
	public void edgeInNavigationCircles_Test_1620()
	{
		A = new PointOnWorldSphere(-50, 45);
		B = new PointOnWorldSphere(50, 45);

		PointOnWorldSphere temp1 = new PointOnWorldSphere(-50, 25);
		PointOnWorldSphere temp2 = new PointOnWorldSphere(0, 25);
		PointOnWorldSphere temp3 = new PointOnWorldSphere(50, 25);
		PointOnWorldSphere[] centers = new PointOnWorldSphere[]{temp1, temp2, temp3};
			
		double naviRadiusKm = Transl.degreesToKmOnEquator(30);
		double[] naviRadiusesKm = new double[]{naviRadiusKm, naviRadiusKm, naviRadiusKm};
		
		drawCirclesAndEdge(TestHelper.tinyMapPath, 
				testsResultsDirectory + "1620.png", A, B, centers, naviRadiusesKm);
		assertTrue( !GeometryCalc.edgeInNavigationCircles(A, B, centers, naviRadiusesKm) );
	}
		private void drawCirclesAndEdge(String inputFileName, String outputFileName, PointOnWorldSphere A, PointOnWorldSphere B, 
				PointOnWorldSphere[] centers, double[] naviRadiusesKm)
		{
			MapProjection proj = new Equirectangular(inputFileName);
			for(int i = 0; i<centers.length; i++)
			{				
				LineDrawing.drawNavCircle(proj, centers[i], naviRadiusesKm[i], 0, Colors.violet);
				PointDrawing.markWithXOfColor(centers[i], 2, proj, Colors.violet);
			}			
			LineDrawing.drawGeodesic(proj, A, B, 0, Colors.darkblue);					
			ImgProc.saveImage(proj.getImg(), outputFileName);
		}
		
		
		
		
	/******************************************************************************************************************
	 *  haveTheSameCoordinates Method TEST 
	 *****************************************************************************************************************/

	@Test
	public void pointsHaveDifferentLongitudes_Test_1630()
	{
		A = new PointOnWorldSphere(10, 20);
		B = new PointOnWorldSphere(10.000001, 20);
		assertTrue( !GeometryCalc.haveTheSameCoordinates(A, B));
	}	
	@Test
	public void pointsHaveDifferentLatitudes_Test_1640()
	{
		A = new PointOnWorldSphere(10, 20);
		B = new PointOnWorldSphere(10, 20.0000001);
		assertTrue( !GeometryCalc.haveTheSameCoordinates(A, B));
	}
	@Test
	public void pointsHaveSameLatitudes_Test_1640()
	{
		A = new PointOnWorldSphere(10, 20);
		B = new PointOnWorldSphere(10, 20);
		assertTrue( GeometryCalc.haveTheSameCoordinates(A, B));
	}
		
}
	









