package unit_testing.mapcom.math;

import static org.junit.Assert.*;

import mapcom.math.Auxiliary;

import org.junit.Test;

public class AuxiliaryTest
{
	double delta = 0.00000000001;  // 10 trylionowych, o tyle conajwyzej moze byc blad w double'ach
	
	/******************************************************************************************************************
	 * numberToIntegerPower   Method TEST 
	 ******************************************************************************************************************/
	
	@Test
	public void numberToIntegerPower_test_0009()
	{
		assertEquals(1.0, Auxiliary.numberToIntegerPower(2, 0), delta );
	}
	@Test
	public void numberToIntegerPower_test_0010()
	{
		assertEquals(2.0, Auxiliary.numberToIntegerPower(2, 1), delta );
	}
	@Test
	public void numberToIntegerPower_test_0020()
	{
		assertEquals(4.0, Auxiliary.numberToIntegerPower(2, 2), delta );
	}
	@Test
	public void numberToIntegerPower_test_0030()
	{
		assertEquals(8.0, Auxiliary.numberToIntegerPower(2, 3), delta );
	}
	@Test
	public void numberToIntegerPower_test_0040()
	{
		assertEquals(16.0, Auxiliary.numberToIntegerPower(2, 4), delta );
	}
	@Test
	public void numberToIntegerPower_test_0050()
	{
		assertEquals(.5, Auxiliary.numberToIntegerPower(2, -1), delta );
	}
	@Test
	public void numberToIntegerPower_test_0060()
	{
		assertEquals(.25, Auxiliary.numberToIntegerPower(2, -2), delta );
	}
	@Test
	public void numberToIntegerPower_test_0070()
	{
		assertEquals(.125, Auxiliary.numberToIntegerPower(2, -3), delta );
	}

	
	/******************************************************************************************************************
	 * theNearestTo90Degrees   Method TEST 
	 ******************************************************************************************************************/
	@Test
	public void theNearestTo90Degrees_test_0080()
	{
		assertEquals(2, Auxiliary.theNearestTo90Degrees(Math.PI, Math.PI-0.001, 0.002), delta );
	}
	@Test
	public void theNearestTo90Degrees_test_0090()
	{
		assertEquals(0, Auxiliary.theNearestTo90Degrees(Math.PI/2.0, Math.PI-0.001, 0.002), delta );
	}
	@Test
	public void theNearestTo90Degrees_test_0100()
	{
		assertEquals(0, Auxiliary.theNearestTo90Degrees(Math.PI-0.003, Math.PI-0.001, 0.002), delta );
	}
	@Test
	public void theNearestTo90Degrees_test_0110()
	{
		assertEquals(1, Auxiliary.theNearestTo90Degrees(Math.PI, 0.0021, 0.002), delta );
	}
}
