package unit_testing.world_definition_for_civilization_class_testing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import entities.Civil;
import entities.UserConfig;
import game_model_impl.BoosterPrototype;
import game_model_impl.Building;
import game_model_impl.ConvertBuilding;
import game_model_impl.ExtractBuilding;
import game_model_impl.GameModel;
import game_model_impl.Improvement;
import game_model_impl.LandUnit;
import game_model_impl.PriceStockSetImpl;
import game_model_impl.Requirement;
import game_model_impl.Research;
import game_model_impl.StockType;
import game_model_impl.Unit;
import game_model_impl.LandUnit.TerainSpeedPair;
import game_model_impl.Text.LanguagePair;
import game_model_impl.Text.Text;
import game_model_impl.Text.Text.Language;
import game_model_impl.boosters.ArmorBooster;
import game_model_impl.boosters.ArmorTypeBooster;
import game_model_impl.boosters.AttackBooster;
import game_model_impl.boosters.AttackTypeBooster;
import game_model_impl.boosters.BuildTimeBooster;
import game_model_impl.boosters.FortyficationHPBooster;
import game_model_impl.boosters.FortyficationRepairBooster;
import game_model_impl.boosters.HPBooster;
import game_model_impl.boosters.ProductionBooster;
import game_model_impl.boosters.RadiusBooster;
import game_model_impl.boosters.SpeedBooster;
import game_model_impl.boosters.convert_building.ConvertBuildingInputBooster;
import game_model_impl.boosters.convert_building.ConvertBuildingSubstractor;
import game_model_impl.city_model.CityModelBooster;
import game_model_impl.city_model.SatisfactionBooster;
import game_model_interfaces.PriceStock;
import game_model_interfaces.IUnitModel.ArmorType;
import game_model_interfaces.IUnitModel.AttackType;
import game_model_interfaces.IUnitModel.TerainType;
import game_model_interfaces.civilization_defining_interfaces.ICivilizationConfigProvider;


public class Config_WestEurope implements ICivilizationConfigProvider{

	@Override
	public Integer getSpongingByPattern(int productionOfOutcomeResource, int patternId)
	{
		switch(patternId)
		{
			case 1: // pobieram dwa razy mniej inputu niz produkuje
				return productionOfOutcomeResource/2;
		}
		return null;
	}
	
	@Override
	public Integer getImprovedInputByPattern(int currentInput,
			int improvementLevel, int patternId) {
		switch(patternId)
		{
			case 0:
				return (int) (currentInput - 0.2d*improvementLevel*currentInput ); // ka�dy level zmniejsza pobeiranie o 20%
			default:
				return null;
		}
	}

	/*
	 * UWAGA. nie przeci��a� case 0, bo inne testy sie wysypi�.
	 */
	@Override
	public PriceStockSetImpl getCostByPattern(PriceStockSetImpl baseCost,
			int patternId, int lvl, UserConfig uc) {
		
		switch(patternId)
		{
			case 1:
				return new PriceStockSetImpl(baseCost, (lvl +1)*2);
			default:
				return null;
		}
	}

	@Override
	public Integer getProductionByPattern(int peopleCount, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getExtractionByPattern(Integer peopleCount,
			Integer lodeFactor, int patternId) {
		switch(patternId)
		{
			case 1:
				return lodeFactor * 2 * peopleCount;
			default:
				return null;
		}
	}

	@Override
	public Integer getMaxPeopleByPattern(int baseAmount, int buildingLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getResearchBuildTimeByPattern(Long timeFactor, int rlvl,
			int blvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getUnitBuildTimeByPattern(Long baseBuildTime, int blevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getBuildingBuildTimeByPattern(int patternId, int currLvl,
			Long baseBuildTime) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMaxLvlByPattern(Integer myLevel, int patternId) {
		switch(patternId)
		{
			case 1000:
				return 4;
			default:
				return null;
		}
		
	}

	@Override
	public Integer maxPeopleBoostPattern(int currentMaxPeople,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PriceStockSetImpl PriceBoostPattern(PriceStockSetImpl current,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer ArmorBoostPattern(int current, int improvementLevel,
			int patternId) {
		switch(patternId)
		{
			case 0:
				return (int) (current * (1 + (double)improvementLevel *0.2d));
			default:
				return null;
		}
		
	}

	@Override
	public Integer getImprovedProductionByPattern(int currentProduction,
			int improvementLevel, int patternId) {
		switch(patternId)
		{
			case 0:
				return (int) (currentProduction + 0.2d*improvementLevel*currentProduction ); // ka�dy level zwi�ksza produkcje o 20%

			default:
				return null;
		}
	}

	@Override
	public Boolean boostAntarcticaSettlemantAbility(int reserachLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostSatisfaction(int currentSatisf, int objectLvl,
			int patternId) {
		switch(patternId){
		case 0:
			return currentSatisf + 4*objectLvl;
		default:
			return null;
	}
	}

	@Override
	public Integer boostCityRadius(int currentRadius, int objectLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostMoralFactor(int currentMoral, int objectLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostFortificationRepairPerHour(int currentRepairPerHour,
			int objectLvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostHP(int currentHP, int objectLvl, int patternId) {
		
		switch(patternId)
		{
			case 0:
				return (int) (currentHP + (currentHP*0.5d * (double)objectLvl) ) ;
			case 1:
				return (int) (currentHP + (currentHP*0.2d * (double)objectLvl) ) ;
			case 2:
				return currentHP + 10*objectLvl;
			default:
				return null;
		}
	}

	@Override
	public Integer boostDirectionsAmount(int objectLvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getPercentRobberySteal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Civil getCivilization() {
		return Civil.WestEurope;
	}

	@Override
	public Civil extendsCivilization() {
		return Civil.common;
	}

	@Override
	public StockType getMainResource() {
		return StockType.gold;
	}

	@Override
	public Double getPeopleFallRate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean canSettleAntarctica() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer mapDirectionsAmount() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GameModel[] gameModels() {
		return new GameModel[]{
				
			new Improvement(300,1,0,new PriceStockSetImpl(),10,0,false),
			new ExtractBuilding(100,100,0,0,0,0,new PriceStockSetImpl( new PriceStock(StockType.wood, 100)),10,StockType.stone),
			new ExtractBuilding(101,100,0,0,1,0,new PriceStockSetImpl(),10,StockType.iron),
			new ExtractBuilding(2001,100,0,0,0,0,new PriceStockSetImpl(),10,StockType.wheat),
			new ConvertBuilding(2000,100,0,0,0,new PriceStockSetImpl( new PriceStock(StockType.iron, 200), new PriceStock(StockType.wood, 300) ),0,10,StockType.brewery),
			new Improvement(2002, 2, 0, new PriceStockSetImpl(), 10, 0, true),
			new Improvement(2003,2,0,new PriceStockSetImpl(),10,0,true),
			new Improvement(2004,2,0,new PriceStockSetImpl(),10,0,false),
			
			new Improvement(2005,1,0,new PriceStockSetImpl(),10,0,false),
			new LandUnit(2500,100,100, AttackType.normal,10,ArmorType.heavy, null, 10, 0, 0, new PriceStockSetImpl(),10,
					new TerainSpeedPair(TerainType.plain, 100)
					),
			new LandUnit(2600,100,100, AttackType.normal,10,ArmorType.heavy, null, 10, 0, 0, new PriceStockSetImpl( new PriceStock(StockType.peopleCount,1)),10,
					new TerainSpeedPair(TerainType.plain, 100),
					new TerainSpeedPair(TerainType.desert, 200)
					),
			new Improvement(2501,-1, 0, new PriceStockSetImpl(), 10, 0, false),
			new Improvement(2502,1, 0, new PriceStockSetImpl(), 0,0,false),
			new Improvement(2503,1,0, new PriceStockSetImpl(), 0,0, false),
			new Improvement(2504,1,0, new PriceStockSetImpl(), 0,0, false),
			new Improvement(2505,1,0, new PriceStockSetImpl(), 0,0, false),
			new Improvement(2506,1,0, new PriceStockSetImpl(), 0,0, false),
			new Improvement(2507, -1, 0, null, 10, 0, false),
			new Improvement(2508, -1, 0, null, 10, 0, false),
			new Improvement(2509, -1, 0, null, 10, 0, false),
			new Improvement(2510, -1, 0, null, 10, 0, false),
			new Improvement(2511, -1, 0, null, 10, 0, false),
			new Improvement(2512, -1, 0, null, 10, 0, false),
			
			new Building(2513,0,0, new PriceStockSetImpl(),10),
			
			
		};
	}
	
	@Override
	public Text[] getNames() {
		return new Text[]
		{
			new Text(300, new LanguagePair(Language.PL, "napraw kanaly")),
			new Text(3,300,1,new LanguagePair(Language.PL, "naprawione kanaly")),
			new Text(0,new LanguagePair(Language.PL, "krolestwo europejskie")),
			new Text(6, new LanguagePair(Language.PL, "europe market place")),
			new Text(2000, new LanguagePair(Language.PL, "Browarnia")),
			new Text(2001, new LanguagePair(Language.PL, "Farma pszenicy")),
			new Text(2002,new LanguagePair(Language.PL, "ka�dy poziom zwi�ksza produkcje browaru o 20% warto�ci pocz�tkowej")),
			new Text(2003,new LanguagePair(Language.PL, "ka�dy poziom zmniejsza pobieranie wody przez browar o 20 % warto�ci pocz�tkowej ")),
			new Text(2004,new LanguagePair(Language.PL, "Druk. Ka�dy level skraca czas wszystkich bada� o po�ow�.")),
			new Text(2500, new LanguagePair(Language.PL, "Piechota ze strzelb�.")),
			new Text(2600, new LanguagePair(Language.PL, "kusznik.")),
			new Text(2501, new LanguagePair(Language.PL, "Ka�dy level Zwi�ksza armor Piechoty ze strzelb� o 20% warto�ci pocz�tkowej.")),
			new Text(2502, new LanguagePair(Language.PL, "Zmiena armor Piechoty ze strzelb� na lekki.")),
			new Text(2503, new LanguagePair(Language.PL, "Ulepszenie zmienia atak piechoty na type range")),
			new Text(2504, new LanguagePair(Language.PL, "Zwi�ksza atak piechoty ze strzelb� o 20% warto�c pocz�tkowej")),
			new Text(2505, new LanguagePair(Language.PL, "Zwi�ksza pr�dko�� piechoty ze strzelb� o 20% po jungli")),
			new Text(2506, new LanguagePair(Language.PL, "Zwi�ksza pr�dko�� kusznika po wszystkich rodzajach pod�o�a.")),
			new Text(2507, new LanguagePair(Language.PL, "Ulepszenie zwi�ksza hp kusznika o 50%")),
			new Text(2508, new LanguagePair(Language.PL, "Ulepszenie zwi�ksza hp wszytkich jednostek o 20%")),
			new Text(2509, new LanguagePair(Language.PL, "Ulepszenie zwi�ksza hp wszytkich jednostek o +10")),
			new Text(2510, new LanguagePair(Language.PL, "Ulepszenie zwi�ksza atak kusznika o 20%")),
			new Text(2511, new LanguagePair(Language.PL, "Ulepszenie zwi�ksza atak kusznika o +20")),
			new Text(2512, new LanguagePair(Language.PL, "Ulepszenie zwi�ksza promie� kartografii o +10"))
		};
	}
	
	@Override
	public Requirement[] getRequirements() {
		return new Requirement[]
		{
			new Requirement(2001, 0, false, 0),
			new Requirement(2500,1,false,1001), // unit "Piechota ze strzelb�" wymaga badania proch strzelniczy z cywilizacji korea�skiej.
			new Requirement(2000, 1, false, 0),
			
			new Requirement(2600, 1, true, 6), // unit potrzebuje market place
			new Requirement(300,1,true,3), // badanie "napraw kana�y" wymaga budynku dostawca wody.
			new Requirement(300,1000,false,0), // badanie "napraw kana�y" wymaga 4 lvl tiera.
			new Requirement(2002, 1, true, 2000), // badanie "zwi�ksz produkcje browaru" wymaga budynku browarnia.
			new Requirement(2003,1,true,2000), // badanie "zmniejsz pobieranie wody w borwarni" wymaga budynku browarnia.
			new Requirement(2004,1,true,500), // badanie druk wymaga uniwersytetu
		};
	}

	@Override
	public ConvertBuildingSubstractor[] convertBuildingSubstractors() {
		
		return new ConvertBuildingSubstractor[]
		{
			new ConvertBuildingSubstractor(2000, 1, StockType.wheat),
			new ConvertBuildingSubstractor(2000, 0, StockType.water)
		};
	}

	@Override
	public BoosterPrototype[] boosters() {
		return new BoosterPrototype[]{
			new BoosterPrototype(new ProductionBooster(2002, 0), 2000),
			new BoosterPrototype(new ConvertBuildingInputBooster(2003, 0, StockType.water ), 2000),
			new BoosterPrototype(new BuildTimeBooster(2004, 0), Research.class,Improvement.class),
			new BoosterPrototype(new ProductionBooster(300,0) , 3),
			new BoosterPrototype(new ArmorBooster(2501, 0), 2500),
			new BoosterPrototype(new ArmorTypeBooster(2502, 0), 2500),
			new BoosterPrototype(new AttackTypeBooster(2503, 0), 2500),
			new BoosterPrototype(new AttackBooster(2504, 0), 2500),
			new BoosterPrototype(new SpeedBooster( 2505, 0, TerainType.jungle ), 2500 ),
			new BoosterPrototype(new SpeedBooster( 2506, 0),  2600 ),
			new BoosterPrototype(new HPBooster( 2509, 2),  ( Unit.class ) ),
			new BoosterPrototype(new HPBooster( 2507, 0),  ( 2600 ) ),
			new BoosterPrototype(new HPBooster( 2508, 1),  ( Unit.class ) ),
			new BoosterPrototype(new AttackBooster( 2510, 0),  ( 2600 ) ),
			new BoosterPrototype(new AttackBooster( 2511, 1),  ( 2600 ) ),
			new BoosterPrototype(new FortyficationHPBooster(100, 0),  (510)), // level tartaku wp�ywa na hp fortyfikacji.
			new BoosterPrototype(new FortyficationHPBooster(101, 1),  (510)), // level kopalni �elaza wp�ywa na hp fortyfikacji.
			new BoosterPrototype(new FortyficationRepairBooster(100, 0),  (510)),
			new BoosterPrototype(new RadiusBooster(2512, 0),  (511))
			
		};
	}

	@Override
	public Text[] getDescriptions() {
		// TODO Auto-generated method stub
		return null;
	}

	

	@Override
	public Integer boostBuildTimePattern(int currentTime, int brLevel,
			int patternId) {
		switch(patternId)
		{
			case 0: // Ka�dy level skraca czas pocz�tkowy o po�ow�.
				int divide = (int) Math.pow(2, brLevel); 
				return currentTime/divide;
			default:
				return null;
		}
	}

	@Override
	public Integer getPeopleLimitInHouses(int housesLevel, int basePeopleLimit,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArmorType boostArmorType(int patternId, int rLevel, ArmorType current)
	{	
		switch( patternId)
		{
			case 0:
				return rLevel > 0 ? ArmorType.light : current;
			default:
				return null;
		}
	}

	@Override
	public AttackType boostAttackType(int patternId, int rLevel,
			AttackType current) {
		return null;
	}

	@Override
	public Integer boostAttack(int patternId, int rLevel, int current) {
		// TODO Auto-generated method stub
		switch( patternId)
		{
			case 0:
				return (int) (current * (1.0d + 0.2d * (double)rLevel));
			case 1:
				return current + 10 * rLevel;
			default:
				return null;
		}
	}

	@Override
	public Integer boostCapacity(int current, int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostSpeed(int current, int improvementLevel, int patternId) {
		return null;
	}

	@Override
	public StockType getMajorMeat() {
		return StockType.poultry;
	}

	@Override
	public StockType getMajorCarbo() {
		return StockType.wheat;
	}

	@Override
	public StockType getMajorAddition() {
		return StockType.brewery;
	}

	@Override
	public Integer getFortyfiactionHP(int baseHP, Integer fortyficationLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getFortyficationRepairRate(int baseRepairRate,
			Integer fortyficationLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostFortyficationHP(int current, int improvementLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostFortyficationRepairRate(int current,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getRadius(int baseRadius, Integer radiusResearchLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostRadius(int current, int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CityModelBooster[] cityModelBoosters() {
		return new CityModelBooster[]{
				new SatisfactionBooster(100, 0) // tartak zwi�ksza satysfakcj� o +4
			};
	}

	@Override
	public Integer getSpongingPatternById(String convBuildId_stockType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getCostPatternById(int gameModelId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getExtractionPatternById(int extractBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getProductionPatternById(int convertBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMaxPeoplePatternById(int extractBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getResearchBuildTimePatternById(int reseachId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getUnitBuildTimePatternById(int unitId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getBuildingBuildTimePatternById(int buildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getRadiusPatternById(int radiusReseach) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationProtectionLevel(AttackType aType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationProtectionLevel(int unitId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationArmorBoost(int fortyficationLevel,
			int currentArmor) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationAttackBoost(int fortyficationLevel,
			int currentAttack) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Integer> excludedObjects() {
		return new ArrayList<Integer>(Arrays.asList(512,517));
	}

	


}
