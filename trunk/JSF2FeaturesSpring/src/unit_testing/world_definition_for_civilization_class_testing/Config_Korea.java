package unit_testing.world_definition_for_civilization_class_testing;

import java.util.List;


import entities.Civil;
import entities.UserConfig;
import game_model_impl.BoosterPrototype;
import game_model_impl.Building;
import game_model_impl.ConvertBuilding;
import game_model_impl.ExtractBuilding;
import game_model_impl.GameModel;
import game_model_impl.Improvement;
import game_model_impl.LandUnit;
import game_model_impl.PriceStockSetImpl;
import game_model_impl.Requirement;
import game_model_impl.StockType;
import game_model_impl.Unit;
import game_model_impl.LandUnit.TerainSpeedPair;
import game_model_impl.Text.LanguagePair;
import game_model_impl.Text.Text;
import game_model_impl.Text.Text.Language;
import game_model_impl.boosters.convert_building.ConvertBuildingSubstractor;
import game_model_impl.city_model.CityModelBooster;
import game_model_interfaces.PriceStock;
import game_model_interfaces.IUnitModel.ArmorType;
import game_model_interfaces.IUnitModel.AttackType;
import game_model_interfaces.IUnitModel.TerainType;
import game_model_interfaces.civilization_defining_interfaces.ICivilizationConfigProvider;



public class Config_Korea implements ICivilizationConfigProvider{

	@Override
	public Civil getCivilization() {
		return Civil.korea;
	}
	@Override
	public Civil extendsCivilization() {
		return Civil.common;
	}

	@Override
	public StockType getMainResource() {
		return StockType.silver;
	}

	@Override
	public GameModel[] gameModels() {
		return koreaModel;
	}
	@Override
	public Text[] getNames() {
		return names;
	}

	@Override
	public Text[] getDescriptions() {
		return descriptions;
	}
	
	private final Text[] names =
	{
		new Text(1, new LanguagePair( Language.PL, "Jakis improvement"),
								new LanguagePair( Language.ENG, "Some improvement")),
		new Text(2,2, 0, new LanguagePair(Language.PL, "mur")),
		new Text(2,2, 7, new LanguagePair(Language.PL, "wielki mur chinski")),
		
		new Text(3,new LanguagePair(Language.PL, "Kanaly wodne"),
		  				new LanguagePair(Language.ENG, "water cannals")),
		
		new Text(4, new LanguagePair(Language.PL, "Koreanski wlocznik")),
		  				
		new Text(4, 5, 1, new LanguagePair( Language.PL , "Koreanski pikinier")),
		
		new Text(6, new LanguagePair(Language.PL, "korea market place")),
		
		new Text(10, new LanguagePair(Language.PL, "jakis improvement w sul house")),
		
		new Text(1001, new LanguagePair(Language.PL, "Proch strzelniczy."))
	};
	private final Text[] descriptions =
	{
		new Text(1, new LanguagePair(Language.PL, "Jakis improvement description"),
						 new LanguagePair(Language.ENG, "Some improvement description"))
	};

	private final GameModel[] koreaModel =
		{
			new Improvement(1,-1,0, new PriceStockSetImpl(),1,0,false),
			new Building(2,0,0,new PriceStockSetImpl(new PriceStock(StockType.stone,1000)),10),
			new LandUnit(4,100,5, AttackType.normal,5,ArmorType.heavy,null,10,0,0,new PriceStockSetImpl(),10,
					new TerainSpeedPair(TerainType.plain, 14) ),
					
			new LandUnit(15,100,5, AttackType.normal,5,ArmorType.heavy,null,100,0,0,new PriceStockSetImpl(),10,
					new TerainSpeedPair(TerainType.plain,14)
					),
			new Improvement(5, 4, 0, new PriceStockSetImpl(), 10, 0, false),
			
			new ConvertBuilding(8,100,0,0,0,new PriceStockSetImpl(),0,10,StockType.sul),
			new Improvement(10,4,0,new PriceStockSetImpl(),10,0,true),
			
			new ExtractBuilding(1000, 100, 0, 0, 0, 0, new PriceStockSetImpl(), 10, StockType.rice),
			
			new Improvement(1001,1,0,new PriceStockSetImpl(),10,0,false)
			
		};
	@Override
	public Requirement[] getRequirements() {
		return new Requirement[]
		{
				new Requirement(1,0,false,0),
				new Requirement(2,0,false,0),
				new Requirement(4,0,false,0),
				new Requirement(5,0,false,0),
				new Requirement(10,1,true,8) // "jakis improvement w sul house" wymaga budynku sul house
		};
	}

	
	
//	new Improvement(0,-1,"ulepszone budynki","ka�dy level zwi�ksza hp o 10% i armor o 15%",0, new PriceStockSet(),1,0,false),  //0
//	/*1*/	new Improvement(1,3,"ulepszeni w��cznicy","ka�dy kolejny level zwi�ksza hp o +100", 0, new PriceStockSet(),5000,0,false,
//				new Requirement(4,true,5)
//				), //1
//	/*2*/	new Improvement(2,-1,"ulepszone kilofy","ka�dy kolejny level zwi�ksza wydobycie o 10% w kopalniach kruszc�w i kamienia", 0,
//					new PriceStockSet(new PriceStock(StockType.gold,10000)),10000,0,true,
//					new Requirement(3,true, 201)), // ulepszone kilofy wymaga kopalni z�ota lub kamienia), //2
//			new Improvement(44,-1,"wydobycie z�ota", "zwi�ksza wydobycie o 5%",0,new PriceStockSet(),1,0,true,
//					new Requirement(4,true, 200)),
//			new Improvement(24,-1,"kalendarz","zwi�ksza plony farm",0,new PriceStockSet(),1,0,false),
//			new Improvement(26,1,"�elazne opancerzenie statk�w","pozwala na kolonizowanie teren�w na antarktydzie",0,new PriceStockSet(),1,0,false),
//			new Improvement(27,3,"Zr�nicowania pasza","zwi�ksza szybko�� produkcji koni,mi�sa wieprzowego i wo�owego o 10% warto�ci podstawowej",0,new PriceStockSet(),1,0,true),
//			new Improvement(34,1,"szkolenie elitarnego lekkiego je�dzca","zwi�ksza hp o 80 i armor o 80",0,new PriceStockSet(),1,1,false),
//			new Improvement(35,1,"szkolenie elitarnego ci�kiego je�dzca","zwi�ksza hp o 100 i armor o 100",0,new PriceStockSet(),1,1,false),
//			new Improvement(40,1,"szkolenie �ucznika","zwi�ksza hp o 100 i armor o 100",0,new PriceStockSet(),1,1,false),
//			new Improvement(41,1,"szkolenie kusznika","zwi�ksza hp o 100 i armor o 100",0,new PriceStockSet(),1,1,false),
//			new Improvement(42,1,"szkolenie konny �ucznik","zwi�ksza hp o 100 i armor o 100",0,new PriceStockSet(),1,1,false),
//			new Improvement(45,-1,"ulepszony sul house","zmniejsza ilosc ryzu i wodu potrzbna do produkcji sul",0,new PriceStockSet(),1000,0,true,
//					new Requirement(4,true,205)),
//			// budynki
//			/*
//			 * id 6 jest wolne
//			 */
//			new ConvertBuilding(205,0,100,"sul house","alkohol korea�czyk�w", 0,0,0,0,new PriceStockSet(new PriceStock(StockType.gold,300), new PriceStock(StockType.wood,200), new PriceStock(StockType.iron,200)),0,5000,StockType.sul,
//					new Requirement(4,false,203), // wymaganie do suls house
//					new Requirement(4,false,204) // drugie wymaganie do suls house),
//					),
//					
//	/*0*/	new Building(5,"koszary","asd",0,0, new PriceStockSet(
//				new PriceStock(StockType.gold,1000),
//				new PriceStock(StockType.wood,300))
//			,5000),
//	/*1*/	//new MilitaryBuilding(6,1,100,8,"archers house","asd",0,0,0, new NotFoodSet(),5000),
//			new Building(21,"Craftsmen house","naprawa budynk�w",0,0,new PriceStockSet(),5000),
//			
//			new Building(29,"Buddist Temple","�wi�tynia zwi�ksza morale",0,0,new PriceStockSet(),5000),
//			new Building(30,"Stajnia","produkcja jednostek konnych",0,0,new PriceStockSet(),5000,
//					new Requirement(4, false, 209) //stajnia -> stadnina koni
//					),
//			new Building(36,"Archer's house","jednostki strzelaj�ce",0,0,new PriceStockSet(),5000),
//			new Building(43,"Spy facility","produkcja szpieg�w i zwiadowc�w",0,0,new PriceStockSet(),5000),
//			
//			// jednostki
////public Unit(int id,int hp, int attack ,int armor,ArmorType armorType ,Double eatPerHour ,
//			//Integer capacity ,String name, String description,int costPattern,int buildTimePattern,PriceStockSet cost,long baseBuildTime, float speed, Requirement... reqs){
//	/*0*/	new Unit(10, 100,5,5,ArmorType.heavy, null,10,"w��cznik", "w��cznicy to wojownicy z w��czniami",0,0,
//				new PriceStockSet(
//						new PriceStock(StockType.peopleCount,1),
//						new PriceStock(StockType.gold,100))
//				,5000,50,
//					new Requirement(4, true,5)),
//	/*1*/	new RegularUnit(11,30,5, null, 10,"strzelec", "w��cznicy to wojownicy z w��czniami",0,0, new PriceStockSet(),5000,50), //1
//	/*2*/	new RegularUnit(12,40,6,null, 10,"�o�nierz", "w��cznicy to wojownicy z w��czniami",0,0, new PriceStockSet(),5000,50), //2
//			new HorseUnit(103,50,8,null, 10,"w�z handlowy","handel,kolonizowanie",0,0,new PriceStockSet(),5000,50),
//			new HorseUnit(31,100,8,null, 10,"Lekki je�dziec","lekka jednostka konna",0,0,new PriceStockSet(),5000,50),
//			new HorseUnit(32,100,8,null, 10,"Cie�ki je�dziec","ci�ka jednostka konna",0,0,new PriceStockSet(),5000,50),
//			new HorseUnit(33,100,8,null, 10,"strzelec konny","jednostka konna strzelaj�ca",0,0,new PriceStockSet(),5000,50),
//			new RegularUnit(37,100,8,null, 10,"�ucznik","�ucznik",0,0,
//					new PriceStockSet(new PriceStock(StockType.peopleCount,1)),
//					5000,50,
//					new Requirement(4, true,36) // �uczik -> archers house
//					),
//			new RegularUnit(38,100,8,null, 10,"kusznik","kusznik",0,0,
//					new PriceStockSet(new PriceStock(StockType.peopleCount,1)),
//					5000,50,
//					new Requirement(4, true,36) // kusznik -> archers house
//					),
//			new RegularUnit(39,100,8,null, 10,"konny �ucznik","konny �ucznik",0,0,
//					new PriceStockSet(new PriceStock(StockType.peopleCount,1)),
//					5000,50,
//					new Requirement(4, true,36) // konny lucznik -> archers house
//					),
//			new RegularUnit(104,100,8,null, 10,"szpieg","szpieg",0,0,
//					new PriceStockSet(new PriceStock(StockType.peopleCount,1)),
//					5000,50),
//			new RegularUnit(105,100,8,null, 10,"zwiadowca","zwiadowca",0,0,
//					new PriceStockSet(new PriceStock(StockType.peopleCount,1)),
//					5000,50)
	@Override
	public ConvertBuildingSubstractor[] convertBuildingSubstractors() {
		
		return new ConvertBuildingSubstractor[]{
				new ConvertBuildingSubstractor(8, 0, StockType.rice),
				new ConvertBuildingSubstractor(8, 0, StockType.water)
			};
	}

	@Override
	public BoosterPrototype[] boosters() {
		return boosters;
	}
	
	private final BoosterPrototype[] boosters =
		{
//			new BoosterPrototype(new HPBooster(0,0),new DecoratorPattern(IBuildingModel.class)),
//			new BoosterPrototype(new ArmorBooster(0,1),new DecoratorPattern(IBuildingModel.class)),
//			new BoosterPrototype(new HPBooster(1,2),new DecoratorPattern(10)),
//			new BoosterPrototype(new ExtractionBooster(2,3),new DecoratorPattern(200,201)),
//			new BoosterPrototype(new ExtractionBooster(24,6),new DecoratorPattern(204,212)),
//			new BoosterPrototype(new ExtractionBooster(27,0),new DecoratorPattern(209,207)),
//			new BoosterPrototype(new HPBooster(34,10),new DecoratorPattern(31)),
//			new BoosterPrototype(new ArmorBooster(34,10),new DecoratorPattern(31)),
//			new BoosterPrototype(new HPBooster(35,11),new DecoratorPattern(32)),
//			new BoosterPrototype(new HPBooster(40,11),new DecoratorPattern(37)),
//			new BoosterPrototype(new HPBooster(41,11),new DecoratorPattern(38)),
//			new BoosterPrototype(new HPBooster(42,11),new DecoratorPattern(39)),
//			new BoosterPrototype(new ExtractionBooster(44,12),new DecoratorPattern(200)),
			//new BoosterPrototype(new ExtractionBooster(45, 14,StockType.water),  new DecoratorPattern(205)),
			//new BoosterPrototype(new ExtractionBooster(45, 14,StockType.rice),  new DecoratorPattern(205))
		};

	@Override
	public Integer getResearchBuildTimeByPattern(Long timeFactor, int rlvl,
			int blvl, int patternId) {
		return null;
	}

	@Override
	public Integer getBuildingBuildTimeByPattern(int patternId, int currLvl,
			Long baseBuildTime) {
		return null;
	}

	@Override
	public Integer getSpongingByPattern(int productionOfOutcomeResource,
			int patternId) {
		return null;
	}

	@Override
	public PriceStockSetImpl getCostByPattern(PriceStockSetImpl baseCost,
			int patternId, int lvl, UserConfig uc) {
		return null;
	}

	@Override
	public Integer getExtractionByPattern(Integer peopleCount,
			Integer lodeFactor, int patternId) {
		return null;
	}

	@Override
	public Integer getMaxPeopleByPattern(int baseAmount, int buildingLvl,
			int patternId) {
		return null;
	}

	@Override
	public Integer maxPeopleBoostPattern(int currentMaxPeople,
			int improvementLevel, int patternId) {
		return null;
	}

	@Override
	public PriceStockSetImpl PriceBoostPattern(PriceStockSetImpl current,
			int improvementLevel, int patternId) {
		return null;
	}
	
	@Override
	public Integer getUnitBuildTimeByPattern(Long baseBuildTime, int blevel, int patternId)
	{
		Integer result = null;
		switch(patternId)
		{
			case 0: 
				result = (int) (baseBuildTime / blevel);
			break;
		}
		return result;
	}
	
	@Override
	public Integer getProductionByPattern(int peopleCount, int patternId)
	{
		Integer result = null;
		switch(patternId)
		{
		case 0:
			result = peopleCount;
			break;
		case 1:
			result = peopleCount * 2;
		case 99: // zarezerwowane dla craftsmen house, peopleCount oznacza w tym przypadku level budynku craftsmenhouse
			result = peopleCount;
			break;
		}
		return result;
	}
	
	@Override
	public Integer getMaxLvlByPattern(Integer myLevel, int patternId)
	{
		Integer reqParentsLevel = null;
		switch(patternId)
		{
			case 1: // zale�no�� - maksymalny level obiektu musi by� dwukrotnie mniejszy od obiektu parenta 
				reqParentsLevel = myLevel*2;
				break;
			case 2: //kartografia - kartografie mo�emy podnosi� od poziom�w 2 i 5 nawigacji, parentem jest tutaj nawigacja
				if(myLevel == 2)reqParentsLevel = 5;
				else if(myLevel == 1)reqParentsLevel = 2;
				break;
			case 3: // wystraczy miec 2 lvl obiektu parenta
				reqParentsLevel = 2;
				break;
		}
		return reqParentsLevel;
	}
	
	@Override
	public Integer ArmorBoostPattern(int current, int improvementLevel,
			int patternId) {
		return null;
	}

	@Override
	public Integer getImprovedProductionByPattern(int currentProduction,
			int improvementLevel, int patternId) {
		return null;
	}

	@Override
	public Double getPercentRobberySteal() {
		return null;
	}

	@Override
	public Double getPeopleFallRate() {
		return null;
	}
	
	@Override
	public Integer boostSatisfaction(int currentSatisf, int objectLvl, int patternId) {
		return null;
	}

	@Override
	public Integer boostCityRadius(int currentRadius, int objectLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostMoralFactor(int currentMoral, int objectLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostFortificationRepairPerHour(int currentRepairPerHour,
			int objectLvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostHP(int currentHP, int objectLvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean boostAntarcticaSettlemantAbility(int reserachLvl,
			int patternId) {
		return null;
	}

	@Override
	public Boolean canSettleAntarctica() {
		return null;
	}

	@Override
	public Integer mapDirectionsAmount() {
		return null;
	}

	@Override
	public Integer boostDirectionsAmount(int objectLvl, int patternId) {
		return null;
	}
	
	@Override
	public Integer getImprovedInputByPattern(int currentInput,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer boostBuildTimePattern(int currentTime, int brLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getPeopleLimitInHouses(int housesLevel, int basePeopleLimit,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ArmorType boostArmorType(int patternId, int rLevel, ArmorType current) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public AttackType boostAttackType(int patternId, int rLevel,
			AttackType current) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer boostAttack(int patternId, int rLevel, int current) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer boostCapacity(int current, int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Integer boostSpeed(int current, int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public StockType getMajorMeat() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public StockType getMajorCarbo() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public StockType getMajorAddition() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getFortyfiactionHP(int baseHP, Integer fortyficationLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getFortyficationRepairRate(int baseRepairRate,
			Integer fortyficationLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer boostFortyficationHP(int current, int improvementLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer boostFortyficationRepairRate(int current,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getRadius(int baseRadius, Integer radiusResearchLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer boostRadius(int current, int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public CityModelBooster[] cityModelBoosters() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getSpongingPatternById(String convBuildId_stockType) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getCostPatternById(int gameModelId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getExtractionPatternById(int extractBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getProductionPatternById(int convertBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getMaxPeoplePatternById(int extractBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getResearchBuildTimePatternById(int reseachId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getUnitBuildTimePatternById(int unitId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getBuildingBuildTimePatternById(int buildingId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer getRadiusPatternById(int radiusReseach) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer fortyficationProtectionLevel(AttackType aType) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer fortyficationProtectionLevel(int unitId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer fortyficationArmorBoost(int fortyficationLevel,
			int currentArmor) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Integer fortyficationAttackBoost(int fortyficationLevel,
			int currentAttack) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<Integer> excludedObjects() {
		// TODO Auto-generated method stub
		return null;
	}





}
