package unit_testing.world_definition_for_civilization_class_testing;

import java.util.List;


import entities.Civil;
import entities.UserConfig;
import game_model_impl.BoosterPrototype;
import game_model_impl.GameModel;
import game_model_impl.PriceStockSetImpl;
import game_model_impl.Requirement;
import game_model_impl.StockType;
import game_model_impl.Text.LanguagePair;
import game_model_impl.Text.Text;
import game_model_impl.Text.Text.Language;
import game_model_impl.boosters.convert_building.ConvertBuildingSubstractor;
import game_model_impl.city_model.CityModelBooster;
import game_model_interfaces.IUnitModel.ArmorType;
import game_model_interfaces.IUnitModel.AttackType;
import game_model_interfaces.civilization_defining_interfaces.ICivilizationConfigProvider;


public class Config_Korea_City implements ICivilizationConfigProvider{

	@Override
	public Integer getSpongingByPattern(int productionOfOutcomeResource,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PriceStockSetImpl getCostByPattern(PriceStockSetImpl baseCost,
			int patternId, int lvl, UserConfig uc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getProductionByPattern(int peopleCount, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getExtractionByPattern(Integer peopleCount,
			Integer lodeFactor, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMaxPeopleByPattern(int baseAmount, int buildingLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getResearchBuildTimeByPattern(Long timeFactor, int rlvl,
			int blvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getUnitBuildTimeByPattern(Long baseBuildTime, int blevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getBuildingBuildTimeByPattern(int patternId, int currLvl,
			Long baseBuildTime) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMaxLvlByPattern(Integer myLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer maxPeopleBoostPattern(int currentMaxPeople,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PriceStockSetImpl PriceBoostPattern(PriceStockSetImpl current,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer ArmorBoostPattern(int current, int improvementLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getImprovedProductionByPattern(int currentProduction,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean boostAntarcticaSettlemantAbility(int reserachLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostSatisfaction(int currentSatisf, int objectLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostCityRadius(int currentRadius, int objectLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostMoralFactor(int currentMoral, int objectLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostFortificationRepairPerHour(int currentRepairPerHour,
			int objectLvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostHP(int currentHP, int objectLvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostDirectionsAmount(int objectLvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getPercentRobberySteal() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Civil getCivilization() {
		return Civil.city_in_korea;
	}

	@Override
	public Civil extendsCivilization() {
		// TODO Auto-generated method stub
		return Civil.korea;
	}

	@Override
	public StockType getMainResource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getPeopleFallRate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean canSettleAntarctica() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer mapDirectionsAmount() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GameModel[] gameModels() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConvertBuildingSubstractor[] convertBuildingSubstractors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BoosterPrototype[] boosters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Text[] getNames() {
		
		return new Text[]{
				new Text(0,0,1,new LanguagePair(Language.PL, "Wieki ciemne w miescie koreanskim"),
						 new LanguagePair(Language.ENG, "Dark Ages in korea city")),

				new Text(0,0,2,new LanguagePair(Language.PL,"Wieki zamk�w w miescie koreanskim"),
						 new LanguagePair(Language.ENG, "Castle Ages in korea city")),	
						 
				new Text(6, new LanguagePair(Language.PL, "city korea market place"))
		};
	}

	@Override
	public Text[] getDescriptions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Requirement[] getRequirements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getImprovedInputByPattern(int currentInput,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostBuildTimePattern(int currentTime, int brLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getPeopleLimitInHouses(int housesLevel, int basePeopleLimit,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArmorType boostArmorType(int patternId, int rLevel, ArmorType current) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AttackType boostAttackType(int patternId, int rLevel,
			AttackType current) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostAttack(int patternId, int rLevel, int current) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostCapacity(int current, int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Integer boostSpeed(int current, int improvementLevel, int patternId) {
		
		return null;
	}

	@Override
	public StockType getMajorMeat() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StockType getMajorCarbo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StockType getMajorAddition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getFortyfiactionHP(int baseHP, Integer fortyficationLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getFortyficationRepairRate(int baseRepairRate,
			Integer fortyficationLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostFortyficationHP(int current, int improvementLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostFortyficationRepairRate(int current,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getRadius(int baseRadius, Integer radiusResearchLevel,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostRadius(int current, int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CityModelBooster[] cityModelBoosters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getSpongingPatternById(String convBuildId_stockType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getCostPatternById(int gameModelId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getExtractionPatternById(int extractBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getProductionPatternById(int convertBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMaxPeoplePatternById(int extractBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getResearchBuildTimePatternById(int reseachId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getUnitBuildTimePatternById(int unitId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getBuildingBuildTimePatternById(int buildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getRadiusPatternById(int radiusReseach) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationProtectionLevel(AttackType aType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationProtectionLevel(int unitId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationArmorBoost(int fortyficationLevel,
			int currentArmor) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationAttackBoost(int fortyficationLevel,
			int currentAttack) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Integer> excludedObjects() {
		// TODO Auto-generated method stub
		return null;
	}



}
