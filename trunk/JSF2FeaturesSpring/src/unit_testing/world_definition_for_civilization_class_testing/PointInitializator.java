package unit_testing.world_definition_for_civilization_class_testing;

import entities.CityLode;
import entities.Civil;
import entities.Point;
import game_model_impl.StockType;
import game_model_impl.StockType.StockFamily;
import game_model_interfaces.initializations.IPointInitializator;

public class PointInitializator implements IPointInitializator {

	@Override
	public void initPoint(Point point, Civil civil) {
		for(StockType rt : StockType.values())
		{
			if(rt.getStockFamily() == StockFamily.food || rt.getStockFamily() == StockFamily.resource)
			{
				point.getCityLodes().add(new CityLode(rt,5));
			}
		}
	}



	
}
