package unit_testing.world_definition_for_civilization_class_testing;

import java.util.List;


import entities.Civil;
import entities.UserConfig;
import game_model_impl.BoosterPrototype;
import game_model_impl.Building;
import game_model_impl.ExtractBuilding;
import game_model_impl.Fortyfication;
import game_model_impl.GameModel;
import game_model_impl.Houses;
import game_model_impl.Improvement;
import game_model_impl.LandUnit;
import game_model_impl.PriceStockSetImpl;
import game_model_impl.RadiusReseach;
import game_model_impl.Requirement;
import game_model_impl.Research;
import game_model_impl.StockType;
import game_model_impl.Unit;
import game_model_impl.LandUnit.TerainSpeedPair;
import game_model_impl.Text.LanguagePair;
import game_model_impl.Text.Text;
import game_model_impl.Text.Text.Language;
import game_model_impl.boosters.ArmorBooster;
import game_model_impl.boosters.BuildTimeBooster;
import game_model_impl.boosters.HPBooster;
import game_model_impl.boosters.PeopleBooster;
import game_model_impl.boosters.convert_building.ConvertBuildingSubstractor;
import game_model_impl.city_model.CityModelBooster;
import game_model_impl.city_model.SatisfactionBooster;
import game_model_interfaces.PriceStock;
import game_model_interfaces.TaxPriceStock;
import game_model_interfaces.IUnitModel.ArmorType;
import game_model_interfaces.IUnitModel.AttackType;
import game_model_interfaces.IUnitModel.TerainType;
import game_model_interfaces.civilization_defining_interfaces.ICivilizationConfigProvider;


public class Config_Common implements ICivilizationConfigProvider{

	@Override
	public Integer getSpongingByPattern(int productionOfOutcomeResource,
			int patternId) {
		switch(patternId)
		{
			case 0: // pobieram tyle inputu ile produkuje
				return productionOfOutcomeResource;
		}
		return null;
	}

	@Override
	public final PriceStockSetImpl getCostByPattern(final PriceStockSetImpl baseCost,int patternId, int lvl, UserConfig uc)
	{
		switch(patternId)
		{
			case 0:
				return new PriceStockSetImpl(baseCost, lvl + 1);
			default:
				return null;
		}
		
	}
	
	@Override
	public Integer getMaxPeopleByPattern(int baseAmount,int buildingLvl,int patternId)
	{
		switch(patternId)
		{
			case 0:
				return baseAmount + (buildingLvl-1) * baseAmount;
			default:
				return null;
		}
	}
	
	@Override
	public Integer getBuildingBuildTimeByPattern(int patternId, int currLvl, Long baseBuildTime)
	{
		Integer result = 0;
		switch(patternId)
		{
			case 1:
				result = 5000;
				break;
			case 0:
				result = (int) (baseBuildTime * (currLvl + 1));
				break;
		}
		return result;
	}
	
	@Override
	public Integer getUnitBuildTimeByPattern(Long baseBuildTime, int blevel, int patternId)
	{
		Integer result = 0;
		switch(patternId)
		{
			case 0:
				result = (int) (baseBuildTime / blevel);
			break;
		}
		return result;
	}
	/*
	 * blvl - level budynku
	 * rlvl - level badania
	 * timeFactor - wsp�czynnik czasowy
	 */
	public Integer getResearchBuildTimeByPattern(Long timeFactor, int rlvl, int blvl, int patternId)
	{
		Integer result = 0;
		switch(patternId)
		{
			case 0: 
				result = (int) (timeFactor*(1+rlvl) / blvl);
			break;
		}
		return result;
	}
	
	@Override
	public Integer getProductionByPattern(int peopleCount, int patternId) {
		
		switch(patternId)
		{
			case 0:
				return peopleCount;
		}
		return null;
	}

	@Override
	public Integer getExtractionByPattern(Integer peopleCount,
			Integer lodeFactor, int patternId) {
		int result = 0;
		switch(patternId)
		{
		case 0:
			result = peopleCount * lodeFactor;
			break;
		}
		return result;
		
	}

	@Override
	public Integer getMaxLvlByPattern(Integer myLevel, int patternId) {
		switch(patternId)
		{
			case 0: //przypadek domy�lny 0: wystarczy mie� zerowy level obiektu wymaganego. To si� przydaje przy wymaganiu tiera. Na pocz�tku tier mamy na zerowym levelu.
				return 0;
			case 1: // wystarcz mie� pierwszy level parenta.
				return 1;
			case 2: // wystarcz mie� drugi level parenta.
				return 2;
			case 3: // wystarcz mie� trzeci level parenta.
				return 3;
		}
		return null;
	}

	@Override
	public Integer maxPeopleBoostPattern(int currentMaxPeople,
			int improvementLevel, int patternId) {
		switch(patternId)
		{
			case 0:
				return currentMaxPeople + 10*improvementLevel;
			case 1:
				return (int) (currentMaxPeople*(1.0d + 0.2d*(double)improvementLevel));
			case 2:
				return (int) (currentMaxPeople*(1.0d + 0.3d*(double)improvementLevel));
			default:
				return null;
		}
	}

	@Override
	public PriceStockSetImpl PriceBoostPattern(PriceStockSetImpl current,
			int improvementLevel, int patternId) {
		return null;
	}

	@Override
	public Integer ArmorBoostPattern(int current, int improvementLevel,
			int patternId) {
		switch(patternId)
		{
			case 1:
				return (int) (current*(1.0d + 0.2d*(double)improvementLevel));
			case 2:
				return (int) (current*(1.0d + 0.3d*(double)improvementLevel));
			default:
				return null;	
		}
	}

	@Override
	public Integer getImprovedProductionByPattern(int currentProduction,
			int improvementLevel, int patternId) {
		return null;
	}

	@Override
	public Double getPercentRobberySteal() {
		return 0.3d;
	}

	@Override
	public Civil getCivilization() {
		return Civil.common;
	}

	@Override
	public StockType getMainResource() {
		return null;
	}

	@Override
	public Double getPeopleFallRate() {
		return null;
	}

	@Override
	public Text[] getNames() {
		return names;
	}

	@Override
	public Text[] getDescriptions() {
		return descriptions;
	}
	
	private final Text[] names =
	{
		new Text(0,0,0,new LanguagePair(Language.PL, "Wieki Ciemne"),
				       new LanguagePair(Language.ENG, "Dark Age")),
				 
		new Text(0,0,1,new LanguagePair(Language.PL, "Epoka Feudalna"),
					   new LanguagePair(Language.ENG, "Feudal Age")),
		
		new Text(0,0,2,new LanguagePair(Language.PL,"Epoka Zamk�w"),
					   new LanguagePair(Language.ENG, "Castle Age")),
							
		new Text(0,0,3,new LanguagePair(Language.PL,"Epoka Imperialna"),
				  	   new LanguagePair(Language.ENG, "Imperial Age")),
		
		new Text(200, new LanguagePair(Language.PL, "Kopalnia z�ota"),
					  new LanguagePair(Language.ENG, "Gold mine")),
		
		new Text(3, new LanguagePair(Language.PL, "Dostawca wody"),
				    new LanguagePair(Language.ENG, "Water provider")),
		
		new Text(6, new LanguagePair(Language.PL, "market place")),
		
		new Text(7, 7,0, new LanguagePair(Language.PL, "koszary")),
		
		new Text(7, 7,4, new LanguagePair(Language.PL, "wielkie koszary")),
		
		new Text(500, new LanguagePair(Language.PL, "Uniwersytet")),
		
		new Text(502, new LanguagePair(Language.PL, "Ulepszenie zwi�kszaj�ce wszystkim jednostkom hp o 50%")),
		
		new Text(505, new LanguagePair(Language.PL, "Zwi�ksza maksymaln� ilo�� ludzi u dostawcy wody o 20%")),
		
		new Text(506, new LanguagePair(Language.PL, "Zwi�ksza maksymaln� ilo�� ludzi u dostawcy wody o 30%")),
		new Text(509, new LanguagePair(Language.PL, "Skraca czas budowy domk�w dla ludzi o po�ow�.")),
		
		new Text(511, new LanguagePair(Language.PL, "Badanie kartografia."))
	};
	private final Text[] descriptions =
	{
		new Text(200,new LanguagePair(Language.PL, "Kopalnia z�ota wydobywa z�oto"),
						  new LanguagePair(Language.ENG,"Gold Mine extract gold")) 
	};
	
	

	/*
	 * public ExtractBuilding(int id, int maxPeople,
			int timePattern,int costPattern, int ExtractionPattern,
			int maxPeoplePattern, PriceStockSet cost, long baseBuildTime,StockType typ, Requirement... reqs) {
	 */
	
	@Override
	public GameModel[] gameModels() {
		return new GameModel[]{
				new Research(0, 4, 0, new PriceStockSetImpl(
									  		new TaxPriceStock(1200)), 1, 0),
				
				new ExtractBuilding(200,100,0,0,0,0,
						new PriceStockSetImpl(
							new PriceStock(StockType.gold,300),
							new PriceStock(StockType.wood,200),
							new PriceStock(StockType.iron,200)
										 ),
						4000l,StockType.gold),//2
				
				new ExtractBuilding(3,100,0,0,0,0, new PriceStockSetImpl(),10,StockType.water),
				
				
						
				new Building(6, 0,0,new PriceStockSetImpl(),10),
				
				new Building(7,0,0,new PriceStockSetImpl(),0),
				
				new Building(500,0,0, new PriceStockSetImpl(),10),
				
				new Houses(501, 0, 0, new PriceStockSetImpl(), 100, 0, 100),
				
				new Improvement(502, -1, 0, null, 0,0,false),
				
				new LandUnit(503,100,100,AttackType.normal,10, ArmorType.light, null,10,0,0,null,10, new TerainSpeedPair(TerainType.plain , 50)),
				
				new Improvement(504,4,0,null,0,0,true),
				
				new Improvement(505,4,0,null,0,0,true),
				new Improvement(506,4,0,null,0,0,true),
				
				new Improvement(507,4,0,null,0,0,true),
				new Improvement(508,4,0,null,0,0,true),
				new Improvement(509,4,0,null,0,0,true),
				new Fortyfication(510, 0, 0, null, 10, 100, 0, 10, 0),
				new RadiusReseach(511, -1, 0, null, 10, 0, 100, 0),
				new LandUnit(512,100,100,AttackType.normal,10, ArmorType.light, null,10,0,0,null,10, new TerainSpeedPair(TerainType.plain , 50)),
				
				/*
				 * poni�sze budynki tworz� bardziej skomplikowane drzewo wymaga� kt�re tworzy diament.
				 * Na samej g�rze jest 517. 
				 * 517 ma children: 518 i 519.
				 * 518 ma childre: 520 i 521.
				 * 519 ma children 520 i 522
				 * 521 ma children 520.
				 */
				new Improvement(518,4,0,null,0,0,true),
				new Improvement(519,4,0,null,0,0,true),
				new Improvement(520,4,0,null,0,0,true),
				
				new Building(517,0,0, new PriceStockSetImpl(),10),
				new Building(521,0,0, new PriceStockSetImpl(),10),
				new LandUnit(522,100,100,AttackType.normal,10, ArmorType.light, null,10,0,0,null,10, new TerainSpeedPair(TerainType.plain , 50)),
				
			};
	
	}
	@Override
	public BoosterPrototype[] boosters() {
		return new BoosterPrototype[]{
			new BoosterPrototype(new HPBooster(502, 1), Unit.class),
			new BoosterPrototype(new PeopleBooster(504, 0),  3),
			new BoosterPrototype(new PeopleBooster(505, 1),  3),
			new BoosterPrototype(new PeopleBooster(506, 2),  3),
			new BoosterPrototype(new ArmorBooster(507, 1),  503),
			new BoosterPrototype(new ArmorBooster(508, 2),  503),
			new BoosterPrototype(new BuildTimeBooster(509, 0),  501),
		};
	}
	@Override
	public Requirement[] getRequirements() {
		return new Requirement[]
		{
			new Requirement(200,0, false, 0),
			new Requirement(3,0, false, 0),
			new Requirement(6,0, false, 0),
			new Requirement(500, 2, false, 0),
			
			/*skompikowany siament*/
			new Requirement(518,0, true, 517),
			new Requirement(519,0, true, 517),
			new Requirement(521, 0, false, 518),
			new Requirement(520, 0, false, 518),
			new Requirement(520, 0, false, 519),
			new Requirement(520, 0, true, 521),
			new Requirement(522, 0, false, 519),
			new Requirement(522, 0, true, 517),
			/*koniec skomplikowany diament*/
			
		};
	}

	@Override
	public ConvertBuildingSubstractor[] convertBuildingSubstractors() {
		return null;
		
	}

	@Override
	public Integer boostSatisfaction(int currentSatisf, int objectLvl, int patternId) {
		switch(patternId){
			case 0:
				return currentSatisf + 3*objectLvl;
			default:
				return null;
		}
	}

	@Override
	public Integer boostCityRadius(int currentRadius, int objectLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostMoralFactor(int currentMoral, int objectLvl,
			int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostFortificationRepairPerHour(int currentRepairPerHour,
			int objectLvl, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostHP(int currentHP, int objectLvl, int patternId) {
		switch(patternId)
		{
			case 1:
				return (int) (currentHP + (currentHP*0.5d * (double)objectLvl) ) ;
			default:
				return null;
		}
	}

	@Override
	public Boolean boostAntarcticaSettlemantAbility(int reserachLvl, int patternId) {
		switch(patternId)
		{
		/*
		 * przypadek defaultowy jest przypisany dla pewnego badania typu "�elaze opancerzenie", wystarczy mie� pierwszy level tego badania.
		 */
			case 0:
				if(reserachLvl>0)return true;
				else return false;
		}
		return null;
	}

	@Override
	public Boolean canSettleAntarctica() {
		return false;
	}

	@Override
	public Integer mapDirectionsAmount() {
		return 0;
	}

	@Override
	public Integer boostDirectionsAmount(int objectLvl, int patternId) {
		switch(patternId)
		{
			case 0:
				// TODO ilos� kierunkow w zale�no�ci od jakiego� badania/budynku
				return 0;
		}
		return null;
	}

	@Override
	public Civil extendsCivilization() {
		return null;
	}

	

	@Override
	public Integer getImprovedInputByPattern(int currentInput,
			int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostBuildTimePattern(int currentTime, int brLevel,
			int patternId) {
		switch(patternId)
		{
			case 0:
				return (int) (currentTime * Math.pow(0.5d, brLevel));
			default:
				return null;
		}
	}

	@Override
	public Integer getPeopleLimitInHouses(int housesLevel, int basePeopleLimit,
			int patternId) {
		
		switch(patternId)
		{
			case 0:
				return housesLevel *basePeopleLimit;
			default:
				return null;
		}
	}

	@Override
	public ArmorType boostArmorType(int patternId, int rLevel, ArmorType current) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AttackType boostAttackType(int patternId, int rLevel,
			AttackType current) {
		switch( patternId )
		{
			case 0:
				return rLevel > 0 ? AttackType.range : current;
			default:
				return null;
		}
	}

	@Override
	public Integer boostAttack(int patternId, int rLevel, int current) {
		switch( patternId)
		{
			case 0:
				return (int) (current * (1 + (double)rLevel*0.2d));
			default:
				return null;
		}
	}

	@Override
	public Integer boostCapacity(int current, int improvementLevel, int patternId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer boostSpeed(int current, int improvementLevel, int patternId) {
		
		switch ( patternId)
		{
			case 0:
				return (int) (current * ( 1 + (double)improvementLevel * 0.2d));
			default:
				return null;
		}
	}

	@Override
	public StockType getMajorMeat() {
		return null;
	}

	@Override
	public StockType getMajorCarbo() {
		return null;
	}

	@Override
	public StockType getMajorAddition() {
		return null;
	}

	@Override
	public Integer getFortyfiactionHP(int baseHP, Integer fortyficationLevel,
			int patternId) {
		switch(patternId){
			case 0:
				return baseHP * fortyficationLevel;
			default:
				return null;
		}
	}

	@Override
	public Integer getFortyficationRepairRate(int baseRepairRate,
			Integer fortyficationLevel, int patternId) {
		switch(patternId){
		case 0:
			return (baseRepairRate * fortyficationLevel);
		default:
			return null;
	}
	}

	@Override
	public Integer boostFortyficationHP(int current, int improvementLevel, int patternId){
		switch(patternId){
			case 0:
				return (int) (current * (1 + 0.2d * (double)improvementLevel));
			case 1:
				return current + improvementLevel * 10;
			default:
				return null;
			
		}
	}

	@Override
	public Integer boostFortyficationRepairRate(int current, int improvementLevel, int patternId){
		switch( patternId ){
			case 0:
				return (int) (current * (1 + 0.2d * (double)improvementLevel));
			case 1:
				return current + improvementLevel * 10;
			default:
				return null;
		}
	}

	@Override
	public Integer getRadius(int baseRadius, Integer radiusResearchLevel, int patternId) {
		switch( patternId){
			case 0:
				return baseRadius * radiusResearchLevel;
			default:
				return null;
		}
	}

	@Override
	public Integer boostRadius(int current, int improvementLevel, int patternId) {
		switch( patternId ){
			case 0:
				return current + improvementLevel *10;
			default:
				return null;
		}
	}

	@Override
	public CityModelBooster[] cityModelBoosters() {
		
		return new CityModelBooster[]{
			new SatisfactionBooster(500, 0)
		};
	}

	@Override
	public Integer getSpongingPatternById(String convBuildId_stockType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getCostPatternById(int gameModelId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getExtractionPatternById(int extractBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getProductionPatternById(int convertBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getMaxPeoplePatternById(int extractBuildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getResearchBuildTimePatternById(int reseachId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getUnitBuildTimePatternById(int unitId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getBuildingBuildTimePatternById(int buildingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getRadiusPatternById(int radiusReseach) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationProtectionLevel(AttackType aType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationProtectionLevel(int unitId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationArmorBoost(int fortyficationLevel,
			int currentArmor) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer fortyficationAttackBoost(int fortyficationLevel,
			int currentAttack) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Integer> excludedObjects() {
		// TODO Auto-generated method stub
		return null;
	}


}
