package unit_testing.game_model_impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import entities.Civil;
import entities.Colony;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UserResearch;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.FreeHand;
import entities.stocks.StockChanger;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.Civilization;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IHousesModel;

import org.junit.Before;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_Korea_City;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;


public class HousesModelTests {

	WorldDefinition ch;
	UsersCityConfig ucc;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	
	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException
	{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea(), new Config_Korea_City(), new Config_WestEurope_City());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
	}
	
	/*
	 * Budynek domki. Level domk�w domy�lnie ustawiony jest na 1 level i mie�ci 100 ludzi. na drugim levelu mie�ci 200 ludzi.
	 */
	@Test
	public void test_houses_people_limit()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		
		IHousesModel houses = westEurope.getById(501, IHousesModel.class);
		
		assertTrue( ucc.getLevel(501) == 1 );
		
		assertTrue(houses.getPeopleLimit(ucc) == 100);
		
		fh.setLevel(ucc.getBuilding(CityBuilding.class, 501), 2);
		
		assertTrue(houses.getPeopleLimit(ucc) == 200);
		
	}
	
	/*
	 * Ulepszenie kt�re skraca czas budowy domku o po�ow�.
	 * ulepszenie id : 509
	 * domki id : 501
	 */
	@Test
	public void test_houses_build_time_booster()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		
		IHousesModel houses = westEurope.getById(501, IHousesModel.class);
		UserResearch ur = uc.getResearch(509);
		
		assertTrue( ucc.getLevel(501) == 1 );
		
		assertEquals(new Integer(200),houses.getBuildTime(ucc));
		
		fhr.setResearchLvl(ur, 1);
		
		assertEquals(new Integer(100),houses.getBuildTime(ucc));
		
	}
	
}
