package unit_testing.game_model_impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import org.junit.Before;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Civil;
import entities.Colony;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;
import entities.buildings.FreeHand;
import entities.stocks.StockChanger;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.PriceStockSetImpl;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IGameModel;

public class GameModelTest {

	WorldDefinition ch;
	UsersCityConfig ucc;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	
	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException
	{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
	}
	
	@Test
	public void test_Price_pattern_from_parent_civilization()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		IGameModel model = uc.getMyCivilization().getById(100, IGameModel.class);
		PriceStockSetImpl pss = model.getPrice(ucc);
		assertEquals(1, pss.getStocks( uc.getMyCivilization() ).size());
		assertEquals(new Integer(100), pss.getStockByType(StockType.wood, uc.getMyCivilization()).getVisibleAmount() );
		
		ExtractCityBuilding eb = ucc.getBuilding(ExtractCityBuilding.class, 100);
		fh.setLevel(eb, 1);
		
		pss = model.getPrice(ucc);
		assertTrue(pss.getStocks( uc.getMyCivilization() ).size() == 1);
		assertTrue(pss.getStockByType(StockType.wood, uc.getMyCivilization()).getVisibleAmount() == 200);
	}
	
	@Test
	public void test_Price_pattern_from_current_civil()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		IGameModel model = uc.getMyCivilization().getById(2000, IGameModel.class);
		PriceStockSetImpl pss = model.getPrice(ucc);
		assertTrue(pss.getStocks( uc.getMyCivilization() ).size() == 2);
		assertTrue(pss.getStockByType(StockType.iron, uc.getMyCivilization()).getVisibleAmount() == 200);
		assertTrue(pss.getStockByType(StockType.wood, uc.getMyCivilization()).getVisibleAmount() == 300);
		
		ExtractCityBuilding eb = ucc.getBuilding(ExtractCityBuilding.class, 2000);
		fh.setLevel(eb, 1);
		
		pss = model.getPrice(ucc);
		assertTrue(pss.getStocks( uc.getMyCivilization() ).size() == 2);
		assertTrue(pss.getStockByType(StockType.iron, uc.getMyCivilization()).getVisibleAmount() == 400);
		assertTrue(pss.getStockByType(StockType.wood, uc.getMyCivilization()).getVisibleAmount() == 600);
	}
	
	@Test
	public void testss(){
		assertEquals(543, solution(345));
		assertEquals(554, solution(545));
		assertEquals(2111, solution(1112));
		assertEquals(1, solution(1));
		assertEquals(0, solution(0));
		assertEquals(4332211, solution(1234321));
		
		int[][] A = {{5, }, 
					 {5, }, 
					 {5, }, 
					 {5, },
					 {4, },
					 {4, },
					 {5, }, 
					 {5, }, 
					 {5, }};
		
		assertEquals(3, solution2(A));
		
		int[][] B = {{1,0},{0,1}};
		assertEquals(4, solution2(B));
		
		int[][] C = {{1,0},{1,0}};
		assertEquals(2, solution2(C));
		
		int[][] D = {{1,0,1},{1,0,1}};
		assertEquals(3, solution2(D));
		
		int[][] K = {{1}};
		assertEquals(1, solution2(K));
		
		int[][] X = {{1,1,1},{1,1,1},{1,1,1}};
		assertEquals(1, solution2(X));
		
		int[][] T = {{},{}};
		assertEquals(0, solution2(T));
		
		assertEquals(true, isArithmetic(new int[]{1,3,5}, 0,2) );
		assertEquals(true, isArithmetic(new int[]{-1,3,7},0,2) );
		assertEquals(false, isArithmetic(new int[]{-1,3,8},0,2) );
		
		assertEquals(true, isArithmetic(new int[]{1,2,3} ,0,2) );
		assertEquals(true, isArithmetic(new int[]{3, -1, -5, -9},0,3 ) );
		assertEquals(true, isArithmetic(new int[]{ 7,  7,  7,  7},0,3 ) );
		assertEquals(true, isArithmetic(new int[]{ 1,  3,  5,  7, 9} ,0,4) );
		assertEquals(false, isArithmetic(new int[]{ 1, 1, 2, 5, 7} ,0,4) );
		
		assertEquals( 5 , solution3(new int[]{-1,1,3,3,3,2,1,0} ));
		
		assertEquals( 1 , solution3(new int[]{-1,1,3} ));
		
		assertEquals( 0 , solution3(new int[]{-1,1} ));
		
		assertEquals( 0 , solution3(new int[]{-1} ));
		assertEquals( 0 , solution3(new int[]{} ));
		
		
	}
	
	public int solution3(int[] A) {
		int left = 0;
		int count = 0;
		for( int i= 0 ; i < A.length ; i++){
			for ( int j = left+2 ; j < A.length ; j++){
				
				if ( isArithmetic(A, left, j))count++;
				
			}
			left++;
		}
		if(count > 1000000000)return -1;
		return count;
    }
	
	public boolean isArithmetic(int[] slice , int low, int up){
		
		BigInteger diff = new BigInteger( Integer.toString(slice[low])).subtract( new BigInteger( Integer.toString(slice[low+1]))) ; 
		for(int i=low;i < up ; i++){
			if(! new BigInteger( Integer.toString(slice[i])).subtract( new BigInteger( Integer.toString(slice[i+1]))).equals(diff)){
				return false;
			}
		}
		return true;
	};
	
	public int solution2(int[][] A) {
        boolean[][] visited = new boolean[A.length][A[0].length];
        for(int i=0 ; i < visited.length ;i++){
        	for(int j=0 ; j < visited[0].length ;j++){
        		visited[i][j] = false;
        	}
        }
        int ret=0;
        for(int i=0 ; i < visited.length ;i++){
        	for(int j=0 ; j < visited[0].length ;j++){
        		if(visited[i][j]==false){
        			ret++;
        			visit(A,i,j,visited);
        		}
        	}
        }
        return ret;
    }
	public void visit(int[][] A , int x,int y, boolean[][] visited){
		if(visited[x][y])return;
		visited[x][y] = true;
		if( y != A[0].length-1 && A[x][y+1] == A[x][y]){
			visit(A,x,y+1,visited);
		}
		if( y != 0 && A[x][y-1] == A[x][y]){
			visit(A,x,y-1,visited);	
		}
		if(x != A.length-1 && A[x+1][y] == A[x][y] ){
			visit(A,x+1,y,visited);
		}
		if( x != 0 && A[x-1][y] == A[x][y]){
			visit(A,x-1,y,visited);
		}
	}
	
	int solution(int N) {
	    // write your code here...
	    String str = Integer.toString(N);
	    List<Integer> list = new ArrayList<Integer>();
	    for(char ch : str.toCharArray()){
	      list.add( Character.getNumericValue(ch) ) ;
	    }
	    Collections.sort(list, Collections.reverseOrder());
	    String ret = "";
	    for(Integer i : list){
	    	ret += Integer.toString(i);
	    }
	    int res = Integer.parseInt(ret);
	    if (res > 100000000)return -1;
	    return res;
	    
	}
	
	
}
