package unit_testing.game_model_impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_Korea_City;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Civil;
import entities.Colony;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UserResearch;
import entities.buildings.ExtractCityBuilding;
import entities.buildings.FreeHand;
import entities.stocks.StockChanger;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IConvertBuildingModel;
import game_model_interfaces.IExtractBuildingModel;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext-test.xml")
public class ConvertBuildingTests {

	WorldDefinition wd;
	Colony ucc;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();

	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException
	{
		System.out.print("before build");
		System.out.print("after build");
		su = mock(SpringJSFUtil.class);
		wd = new WorldDefinition(0,new Config_Common() ,new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea(), new Config_Korea_City() );
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
	}
	
	/*
	 * Zdefiniowano w cywilizacji kora�skiej sul house.
	 * Produkuje tyle, ile jest w nim ludzi.
	 * pobiera wody i ry�u tyle ile produkuje sul.
	 * przyk�ad: pobiera 100 wody i 100 ry�u na /h , produkuje 100 sul.
	 * 
	 * W mie�cie nie ma ani ry�u ani wody (brak produkcji i w spichlerzu), produkcja sul powinna wynosi� 0.
	 *
	 */
	@Test
	public void test_convert_building_if_lack_of_input()
	{
		UserConfig uc = new UserConfig("friko", "pass", wd  , Civil.city_in_korea, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc,  new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		StockChanger sch = new StockChanger();
		sch.freeHand(ucc.getCityStocks().getPeopleStock() , 100.0d );
		ExtractCityBuilding ccb = ucc.getBuilding(ExtractCityBuilding.class, 8);
		fh.setLevel(ccb, 1);
		
		// upewniamy sie ze w miescie nie ma wody i ry�u
		assertFalse(ucc.getCityStocks().getStockByType(StockType.water).isAvailable() );
		assertFalse(ucc.getCityStocks().getStockByType(StockType.rice).isAvailable() );
		
		assertTrue( uc.getMyCivilization().getById(3, IExtractBuildingModel.class).getOutputPerHour(ucc) == 0.0d );
		assertTrue( uc.getMyCivilization().getById(1000, IExtractBuildingModel.class).getOutputPerHour(ucc) == 0.0d );
		
		
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(ccb.setPeople(100));
		IConvertBuildingModel model = uc.getMyCivilization().getById(8, IConvertBuildingModel.class);
		assertTrue(model.getOutputPerHour(ucc)==0);
		assertTrue(model.getMaxPeople(ucc)==100);
		
	}
	
	/*
	 * Zdefiniowano w cywilizacji korea sul house.
	 * Produkuje tyle, ile jest w nim ludzi.
	 * pobiera wody i ry�u tyle ile produkuje sul.
	 * przyk�ad: pobiera 100 wody i 100 ry�u na /h , produkuje 100 sul.
	 * 
	 * W spichlerzu nie ma wody ani ry�u,
	 * natomiast fabryka wody i ry�u produkuj� po 50/h, tzn dostarczaj� po�ow� zapotrzebowania dla sulhouse.
	 * produkcja sul house powinna wiec by� 50.
	 *
	 */
	@Test
	public void test_convert_building_if_input_is_half_provided()
	{
		UserConfig uc = new UserConfig("friko", "pass", wd  , Civil.city_in_korea, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc,  new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		StockChanger sch = new StockChanger();
		sch.freeHand(ucc.getCityStocks().getPeopleStock() , 120.0d );
		ExtractCityBuilding ccb = ucc.getBuilding(ExtractCityBuilding.class, 8);
		fh.setLevel(ccb, 1);
		
		ExtractCityBuilding waterProvider = ucc.getBuilding(ExtractCityBuilding.class, 3);
		fh.setLevel(waterProvider, 1);
		assertTrue(waterProvider.setPeople(10));
		ExtractCityBuilding riceFarm = ucc.getBuilding(ExtractCityBuilding.class, 1000);
		fh.setLevel(riceFarm, 1);
		assertTrue(riceFarm.setPeople(10));
		
		// upewniamy sie ze w miescie nie ma wody i ry�u
		assertFalse(ucc.getCityStocks().getStockByType(StockType.water).isAvailable() );
		assertFalse(ucc.getCityStocks().getStockByType(StockType.rice).isAvailable() );
		
		assertTrue( uc.getMyCivilization().getById(3, IExtractBuildingModel.class).getOutputPerHour(ucc) == 50.0d );
		assertTrue( uc.getMyCivilization().getById(1000, IExtractBuildingModel.class).getOutputPerHour(ucc) == 50.0d );
		
		
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(ccb.setPeople(100));
		IConvertBuildingModel model = uc.getMyCivilization().getById(8, IConvertBuildingModel.class);
		assertTrue(model.getOutputPerHour(ucc)== 50.0d);
		assertTrue(model.getProductionRate(ucc) == 0.5d);
		assertTrue(model.getMaxPeople(ucc)==100);
		
	}
	
	/*
	 * Zdefiniowano w cywilizacji korea sul house.
	 * Produkuje tyle, ile jest w nim ludzi.
	 * pobiera wody i ry�u tyle ile produkuje sul.
	 * przyk�ad: pobiera 100 wody i 100 ry�u na /h , produkuje 100 sul.
	 * 
	 * W spichlerzu jest woda i ry�, tak wi�c wydajno�� sull house powinna by� 100%.
	 * fabryka ry�u i wody nie produkuj� nic.
	 *
	 */
	@Test
	public void test_convert_building_if_all_inputs_are_provided()
	{
		UserConfig uc = new UserConfig("friko", "pass", wd  , Civil.city_in_korea, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc,  new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		StockChanger sch = new StockChanger();
		sch.freeHand(ucc.getCityStocks().getPeopleStock() , 120.0d );
		ExtractCityBuilding ccb = ucc.getBuilding(ExtractCityBuilding.class, 8);
		fh.setLevel(ccb, 1);
		
		ExtractCityBuilding waterProvider = ucc.getBuilding(ExtractCityBuilding.class, 3);
		fh.setLevel(waterProvider, 1);
		ExtractCityBuilding riceFarm = ucc.getBuilding(ExtractCityBuilding.class, 1000);
		fh.setLevel(riceFarm, 1);
		
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.water), 10.0d);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.rice), 0.5d);
		
		
		// upewniamy sie ze w mie�cie jest ry� i woda ( w spichlerzu )
		assertTrue(ucc.getCityStocks().getStockByType(StockType.water).isAvailable() );
		assertTrue(ucc.getCityStocks().getStockByType(StockType.rice).isAvailable() );
		
		assertTrue( uc.getMyCivilization().getById(3, IExtractBuildingModel.class).getOutputPerHour(ucc) == 0.0d );
		assertTrue( uc.getMyCivilization().getById(1000, IExtractBuildingModel.class).getOutputPerHour(ucc) == 0.0d );
		
		
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(ccb.setPeople(100));
		IConvertBuildingModel model = uc.getMyCivilization().getById(8, IConvertBuildingModel.class);
		assertTrue(model.getOutputPerHour(ucc)== 100.0d);
		assertTrue(model.getProductionRate(ucc) == 1.0d);
		assertTrue(model.getMaxPeople(ucc)==100);
		
	}
	
	/*
	 * Zdefiniowano w cywilizacji korea sul house.
	 * Produkuje tyle, ile jest w nim ludzi.
	 * pobiera wody i ry�u tyle ile produkuje sul.
	 * przyk�ad: pobiera 100 wody i 100 ry�u na /h , produkuje 100 sul.
	 * 
	 * W spichlerzu jest woda ale nie ma ry�u.
	 * Produkcja ry�u wynosi 40/h, tak wi�c produkcja sul housa powinna by� dostosowana do produkcji ry�u.
	 *
	 */
	@Test
	public void test_convert_building_if_one_input_is_provided_production_on()
	{
		UserConfig uc = new UserConfig("friko", "pass", wd  , Civil.city_in_korea, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc,  new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		StockChanger sch = new StockChanger();
		sch.freeHand(ucc.getCityStocks().getPeopleStock() , 120.0d );
		ExtractCityBuilding ccb = ucc.getBuilding(ExtractCityBuilding.class, 8);
		fh.setLevel(ccb, 1);
		
		ExtractCityBuilding waterProvider = ucc.getBuilding(ExtractCityBuilding.class, 3);
		fh.setLevel(waterProvider, 1);
		
		ExtractCityBuilding riceFarm = ucc.getBuilding(ExtractCityBuilding.class, 1000);
		fh.setLevel(riceFarm, 1);
		assertTrue(riceFarm.setPeople(8));
		
		
		
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.water), 10.0d);
		
		
		// upewniamy sie ze w mie�cie jest woda, ale nie ma ry�u.
		assertTrue(ucc.getCityStocks().getStockByType(StockType.water).isAvailable() );
		assertFalse(ucc.getCityStocks().getStockByType(StockType.rice).isAvailable() );
		
		assertTrue( uc.getMyCivilization().getById(3, IExtractBuildingModel.class).getOutputPerHour(ucc) == 0.0d );
		assertTrue( uc.getMyCivilization().getById(1000, IExtractBuildingModel.class).getOutputPerHour(ucc) == 40.0d );
		
		
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(ccb.setPeople(100));
		IConvertBuildingModel model = uc.getMyCivilization().getById(8, IConvertBuildingModel.class);
		assertTrue(model.getOutputPerHour(ucc)== 40.0d);
		assertTrue(model.getProductionRate(ucc) == 0.4d);
		assertTrue(model.getMaxPeople(ucc)==100);
		
	}
	
	/*
	 * Zdefiniowano w cywilizacji korea sul house.
	 * Produkuje tyle, ile jest w nim ludzi.
	 * pobiera wody i ry�u tyle ile produkuje sul.
	 * przyk�ad: pobiera 100 wody i 100 ry�u na /h , produkuje 100 sul.
	 * 
	 * W spichlerzu jest ry� ale nie ma wody.
	 * Produkcja wody wynosi 40/h, tak wi�c produkcja sul housa powinna by� dostosowana do produkcji ry�u.
	 *
	 */
	@Test
	public void test_convert_building_if_one_input_is_provided_production_on_2()
	{
		UserConfig uc = new UserConfig("friko", "pass", wd  , Civil.city_in_korea, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc,  new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		StockChanger sch = new StockChanger();
		sch.freeHand(ucc.getCityStocks().getPeopleStock() , 120.0d );
		ExtractCityBuilding ccb = ucc.getBuilding(ExtractCityBuilding.class, 8);
		fh.setLevel(ccb, 1);
		
		ExtractCityBuilding waterProvider = ucc.getBuilding(ExtractCityBuilding.class, 3);
		fh.setLevel(waterProvider, 1);
		assertTrue(waterProvider.setPeople(8));
		
		ExtractCityBuilding riceFarm = ucc.getBuilding(ExtractCityBuilding.class, 1000);
		fh.setLevel(riceFarm, 1);
		
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.rice), 10.0d);
		
		// upewniamy sie ze w mie�cie jest woda, ale nie ma ry�u.
		assertFalse(ucc.getCityStocks().getStockByType(StockType.water).isAvailable() );
		assertTrue(ucc.getCityStocks().getStockByType(StockType.rice).isAvailable() );
		
		assertTrue( uc.getMyCivilization().getById(3, IExtractBuildingModel.class).getOutputPerHour(ucc) == 40.0d );
		assertTrue( uc.getMyCivilization().getById(1000, IExtractBuildingModel.class).getOutputPerHour(ucc) == 0.0d );
		
		
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(ccb.setPeople(100));
		IConvertBuildingModel model = uc.getMyCivilization().getById(8, IConvertBuildingModel.class);
		assertTrue(model.getOutputPerHour(ucc)== 40.0d);
		assertTrue(model.getProductionRate(ucc) == 0.4d);
		assertTrue(model.getMaxPeople(ucc)==100);
		
	}
	
	/*
	 * Zdefiniowano w cywilizacji korea sul house.
	 * Produkuje tyle, ile jest w nim ludzi.
	 * pobiera wody i ry�u tyle ile produkuje sul.
	 * przyk�ad: pobiera 100 wody i 100 ry�u na /h , produkuje 100 sul.
	 * 
	 * W spichlerzu jest woda ale nie ma ry�u.
	 * Produkcja ry�u i wody wynosi 0.
	 * Sul house nie powinien wi�c w og�le produkowa� sul, bo brakuje ry�u.
	 *
	 */
	@Test
	public void test_convert_building_if_one_input_is_provided_production_off()
	{
		UserConfig uc = new UserConfig("friko", "pass", wd  , Civil.city_in_korea, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc,  new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		StockChanger sch = new StockChanger();
		sch.freeHand(ucc.getCityStocks().getPeopleStock() , 120.0d );
		ExtractCityBuilding ccb = ucc.getBuilding(ExtractCityBuilding.class, 8);
		fh.setLevel(ccb, 1);
		
		ExtractCityBuilding waterProvider = ucc.getBuilding(ExtractCityBuilding.class, 3);
		fh.setLevel(waterProvider, 1);
		
		ExtractCityBuilding riceFarm = ucc.getBuilding(ExtractCityBuilding.class, 1000);
		fh.setLevel(riceFarm, 1);
		
		
		
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.water), 10.0d);
		
		// upewniamy sie ze w mie�cie jest woda, ale nie ma ry�u.
		assertTrue(ucc.getCityStocks().getStockByType(StockType.water).isAvailable() );
		assertFalse(ucc.getCityStocks().getStockByType(StockType.rice).isAvailable() );
		
		assertTrue( uc.getMyCivilization().getById(3, IExtractBuildingModel.class).getOutputPerHour(ucc) == 0.0d );
		assertTrue( uc.getMyCivilization().getById(1000, IExtractBuildingModel.class).getOutputPerHour(ucc) == 0.0d );
		
		
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(ccb.setPeople(100));
		IConvertBuildingModel model = uc.getMyCivilization().getById(8, IConvertBuildingModel.class);
		assertTrue(model.getOutputPerHour(ucc)== 0.0d);
		assertTrue(model.getProductionRate(ucc) == 0.0d);
		assertTrue(model.getMaxPeople(ucc)==100);
		
	}
	
	/*
	 * Zdefiniowano w cywilizacji korea sul house.
	 * Produkuje tyle, ile jest w nim ludzi.
	 * pobiera wody i ry�u tyle ile produkuje sul.
	 * przyk�ad: pobiera 100 wody i 100 ry�u na /h , produkuje 100 sul.
	 * 
	 * W spichlerzu nie ma wody ani ry�u.
	 * Produkcja ry�u wynosi 80 a wody wynosi 60.
	 * Sul house nie powinien produkowa� sul w ilosci 60/h.
	 *
	 */
	@Test
	public void test_convert_building_if_producent_are_different_no_stacks_in_city()
	{
		UserConfig uc = new UserConfig("friko", "pass", wd  , Civil.city_in_korea, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc,  new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		StockChanger sch = new StockChanger();
		sch.freeHand(ucc.getCityStocks().getPeopleStock() , 150.0d );
		ExtractCityBuilding ccb = ucc.getBuilding(ExtractCityBuilding.class, 8);
		fh.setLevel(ccb, 1);
		
		ExtractCityBuilding waterProvider = ucc.getBuilding(ExtractCityBuilding.class, 3);
		fh.setLevel(waterProvider, 1);
		assertTrue(waterProvider.setPeople(12));
		
		
		ExtractCityBuilding riceFarm = ucc.getBuilding(ExtractCityBuilding.class, 1000);
		fh.setLevel(riceFarm, 1);
		assertTrue(riceFarm.setPeople(16));
		
		
		// upewniamy sie ze w mie�cie nie ma wody i ry�u.
		assertFalse(ucc.getCityStocks().getStockByType(StockType.water).isAvailable() );
		assertFalse(ucc.getCityStocks().getStockByType(StockType.rice).isAvailable() );
		
		assertTrue( uc.getMyCivilization().getById(3, IExtractBuildingModel.class).getOutputPerHour(ucc) == 60.0d );
		assertTrue( uc.getMyCivilization().getById(1000, IExtractBuildingModel.class).getOutputPerHour(ucc) == 80.0d );
		
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(ccb.setPeople(100));
		IConvertBuildingModel model = uc.getMyCivilization().getById(8, IConvertBuildingModel.class);
		assertTrue(model.getOutputPerHour(ucc)== 60.0d);
		assertTrue(model.getProductionRate(ucc) == 0.6d);
		assertTrue(model.getMaxPeople(ucc)==100);
		
	}
	
	/*
	 * Zdefiniowano w cywilizacji korea sul house.
	 * Produkuje tyle, ile jest w nim ludzi.
	 * pobiera wody i ry�u tyle ile produkuje sul.
	 * przyk�ad: pobiera 100 wody i 100 ry�u na /h , produkuje 100 sul.
	 * 
	 * W spichlerzu nie ma wody ani ry�u.
	 * Produkcja ry�u wynosi 200 a wody wynosi 180.
	 * Sul house powinien produkowa� sul w ilosci 100/h.
	 *
	 */
	@Test
	public void test_convert_building_if_producent_are_different_no_stacks_in_city_overloaded_producers()
	{
		UserConfig uc = new UserConfig("friko", "pass", wd  , Civil.city_in_korea, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc,  new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		StockChanger sch = new StockChanger();
		sch.freeHand(ucc.getCityStocks().getPeopleStock() , 200.0d );
		ExtractCityBuilding ccb = ucc.getBuilding(ExtractCityBuilding.class, 8);
		fh.setLevel(ccb, 1);
		
		ExtractCityBuilding waterProvider = ucc.getBuilding(ExtractCityBuilding.class, 3);
		fh.setLevel(waterProvider, 1);
		assertTrue(waterProvider.setPeople(36));
		
		
		ExtractCityBuilding riceFarm = ucc.getBuilding(ExtractCityBuilding.class, 1000);
		fh.setLevel(riceFarm, 1);
		assertTrue(riceFarm.setPeople(40));
		
		
		// upewniamy sie ze w mie�cie nie ma wody i ry�u.
		assertFalse(ucc.getCityStocks().getStockByType(StockType.water).isAvailable() );
		assertFalse(ucc.getCityStocks().getStockByType(StockType.rice).isAvailable() );
		
		assertTrue( uc.getMyCivilization().getById(3, IExtractBuildingModel.class).getOutputPerHour(ucc) == 180.0d );
		assertTrue( uc.getMyCivilization().getById(1000, IExtractBuildingModel.class).getOutputPerHour(ucc) == 200.0d );
		
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(ccb.setPeople(100));
		IConvertBuildingModel model = uc.getMyCivilization().getById(8, IConvertBuildingModel.class);
		assertTrue(model.getOutputPerHour(ucc)== 100.0d);
		assertTrue(model.getProductionRate(ucc) == 1.0d);
		assertTrue(model.getMaxPeople(ucc)==100);
		
	}
	
	/*
	 * Zdefiniowano budynek "browar" w cywilizacji europejskiej
	 * kt�ry przyjmuje na wej�cie 100/h wody oraz 50/h wheat, by produkowa� 100/h browaru.
	 * W spichlerzu nie ma wody ani wheat.
	 * Produkcja wody wynosi 100 i wheat 50, tak wi�c wydajno�� browaru powinna by� 100%.
	 */
	@Test
	public void test_convertBuilding_sponging_inputs_by_different_patterns()
	{
		UserConfig uc = new UserConfig("friko", "pass", wd, Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc,  new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		StockChanger sch = new StockChanger();
		sch.freeHand(ucc.getCityStocks().getPeopleStock() , 200.0d );
		ExtractCityBuilding ccb = ucc.getBuilding(ExtractCityBuilding.class, 2000);
		fh.setLevel(ccb, 1);
		
		ExtractCityBuilding waterProvider = ucc.getBuilding(ExtractCityBuilding.class, 3);
		fh.setLevel(waterProvider, 1);
		assertTrue(waterProvider.setPeople(20));
		
		
		ExtractCityBuilding wheatFarm = ucc.getBuilding(ExtractCityBuilding.class, 2001);
		fh.setLevel(wheatFarm, 1);
		assertTrue(wheatFarm.setPeople(10));
		
		
		// upewniamy sie ze w mie�cie nie ma wody i ry�u.
		assertFalse(ucc.getCityStocks().getStockByType(StockType.water).isAvailable() );
		assertFalse(ucc.getCityStocks().getStockByType(StockType.wheat).isAvailable() );
		
		assertTrue( uc.getMyCivilization().getById(3, IExtractBuildingModel.class).getOutputPerHour(ucc) == 100.0d );
		assertTrue( uc.getMyCivilization().getById(2001, IExtractBuildingModel.class).getOutputPerHour(ucc) == 50.0d );
		
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(ccb.setPeople(100));
		IConvertBuildingModel model = uc.getMyCivilization().getById(2000, IConvertBuildingModel.class);
		assertTrue(model.getOutputPerHour(ucc)== 100.0d);
		assertTrue(model.getProductionRate(ucc) == 1.0d);
		assertTrue(model.getMaxPeople(ucc)==100);
		
	}
	
	/*
	 * Zdefiniowano budynek "browar" w cywilizacji europejskiej
	 * kt�ry przyjmuje na wej�cie 100/h wody oraz 50/h wheat, by produkowa� 100/h browaru.
	 * W spichlerzu nie ma wody ani wheat.
	 * Produkcja wody wynosi 80 i wheat 50, tak wi�c wydajno�� browaru powinna by� 80% (woda nie wyrabia).
	 */
	@Test
	public void test_convertBuilding_sponging_inputs_by_different_patterns_insufficient_water()
	{
		UserConfig uc = new UserConfig("friko", "pass", wd, Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc,  new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		StockChanger sch = new StockChanger();
		sch.freeHand(ucc.getCityStocks().getPeopleStock() , 200.0d );
		ExtractCityBuilding ccb = ucc.getBuilding(ExtractCityBuilding.class, 2000);
		fh.setLevel(ccb, 1);
		
		ExtractCityBuilding waterProvider = ucc.getBuilding(ExtractCityBuilding.class, 3);
		fh.setLevel(waterProvider, 1);
		assertTrue(waterProvider.setPeople(16));
		
		
		ExtractCityBuilding wheatFarm = ucc.getBuilding(ExtractCityBuilding.class, 2001);
		fh.setLevel(wheatFarm, 1);
		assertTrue(wheatFarm.setPeople(10));
		
		
		// upewniamy sie ze w mie�cie nie ma wody i ry�u.
		assertFalse(ucc.getCityStocks().getStockByType(StockType.water).isAvailable() );
		assertFalse(ucc.getCityStocks().getStockByType(StockType.wheat).isAvailable() );
		
		assertTrue( uc.getMyCivilization().getById(3, IExtractBuildingModel.class).getOutputPerHour(ucc) == 80.0d );
		assertTrue( uc.getMyCivilization().getById(2001, IExtractBuildingModel.class).getOutputPerHour(ucc) == 50.0d );
		
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(ccb.setPeople(100));
		IConvertBuildingModel model = uc.getMyCivilization().getById(2000, IConvertBuildingModel.class);
		assertTrue(model.getOutputPerHour(ucc)== 80.0d);
		assertTrue(model.getProductionRate(ucc) == 0.8d);
		assertTrue(model.getMaxPeople(ucc)==100);
		
	}
	
	/*
	 * Zdefiniowano budynek "browar" w cywilizacji europejskiej, oraz ulepszenie, kt�re zwi�ksza produkcje piwa, bez zwi�kszania pobierania 
	 * input�w, czyli wody i wheat. 
	 * Browar przyjmuje na wej�cie 100/h wody oraz 50/h wheat, by produkowa� standardowo 100/h browaru.
	 * ulepszenie zwi�ksza produkcj� piwa o 20%, tak wi�c browar powinien produkowa�:
	 * 120/h piwa, oraz pobiera� 100/h wody i 50/h wheat.
	 * W spichlerzu nie ma wody ani wheat.
	 * Produkcja wody wynosi 100/h i wheat 50/h, tak wi�c wydajno�� browaru powinna by� 100%.
	 */
	@Test
	public void test_convertBuilding_improvement_for_brewery_production()
	{
		UserConfig uc = new UserConfig("friko", "pass", wd, Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc,  new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		StockChanger sch = new StockChanger();
		sch.freeHand(ucc.getCityStocks().getPeopleStock() , 200.0d );
		ExtractCityBuilding ccb = ucc.getBuilding(ExtractCityBuilding.class, 2000);
		fh.setLevel(ccb, 1);
		
		ExtractCityBuilding waterProvider = ucc.getBuilding(ExtractCityBuilding.class, 3);
		fh.setLevel(waterProvider, 1);
		assertTrue(waterProvider.setPeople(20));
		
		
		ExtractCityBuilding wheatFarm = ucc.getBuilding(ExtractCityBuilding.class, 2001);
		fh.setLevel(wheatFarm, 1);
		assertTrue(wheatFarm.setPeople(10));
		
		// upewniamy sie ze w mie�cie nie ma wody i ry�u.
		assertFalse(ucc.getCityStocks().getStockByType(StockType.water).isAvailable() );
		assertFalse(ucc.getCityStocks().getStockByType(StockType.wheat).isAvailable() );
		
		assertTrue( uc.getMyCivilization().getById(3, IExtractBuildingModel.class).getOutputPerHour(ucc) == 100.0d );
		assertTrue( uc.getMyCivilization().getById(2001, IExtractBuildingModel.class).getOutputPerHour(ucc) == 50.0d );
				
		UserResearch breweryImprovement = uc.getResearch(2002);
		assertTrue(ccb.setPeople(100));
		IConvertBuildingModel model = uc.getMyCivilization().getById(2000, IConvertBuildingModel.class);
		
		assertTrue(model.getOutputPerHour(ucc)== 100.0d);
		assertTrue(model.getInputPerHour(ucc, StockType.water) == 100);
		assertTrue(model.getInputPerHour(ucc, StockType.wheat) == 50);
		assertTrue(model.getProductionRate(ucc) == 1.0d);
		assertTrue(model.getMaxPeople(ucc)==100);
		
		fhr.setResearchLvl(breweryImprovement, 1);
		
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		
		assertTrue(model.getOutputPerHour(ucc)== 120.0d);
		assertTrue(model.getInputPerHour(ucc, StockType.water) == 100);
		assertTrue(model.getInputPerHour(ucc, StockType.wheat) == 50);
		assertTrue(model.getProductionRate(ucc) == 1.0d);
		assertTrue(model.getMaxPeople(ucc)==100);
		
	}
	
	/*
	 * Zdefiniowano budynek "browar" w cywilizacji europejskiej, oraz ulepszenie, kt�re zmiejsza ilo�� pobieranej wody. 
	 * Browar przyjmuje bez ulepszenia na wej�cie 100/h wody oraz 50/h wheat, by produkowa� standardowo 100/h browaru.
	 * ulepszenie zmniejsza ilo�� pobieranej wody o 20%. Tak wi�c parametry browary powinny by� nast�puj�ce:
	 * output: 100/h piwa
	 * input: 80/h wody
	 * input: 50/h wheat.
	 * W spichlerzu nie ma wody ani wheat.
	 * Produkcja wody wynosi 100/h i wheat 50/h, tak wi�c wydajno�� browaru powinna by� 100%.
	 */
	@Test
	public void test_convertBuilding_improvement_for_brewery_input()
	{
		UserConfig uc = new UserConfig("friko", "pass", wd, Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		StockChanger sch = new StockChanger();
		sch.freeHand(ucc.getCityStocks().getPeopleStock() , 200.0d );
		ExtractCityBuilding ccb = ucc.getBuilding(ExtractCityBuilding.class, 2000);
		fh.setLevel(ccb, 1);
		
		ExtractCityBuilding waterProvider = ucc.getBuilding(ExtractCityBuilding.class, 3);
		fh.setLevel(waterProvider, 1);
		assertTrue(waterProvider.setPeople(20));
		
		ExtractCityBuilding wheatFarm = ucc.getBuilding(ExtractCityBuilding.class, 2001);
		fh.setLevel(wheatFarm, 1);
		assertTrue(wheatFarm.setPeople(10));
		
		// upewniamy sie ze w mie�cie nie ma wody i ry�u.
		assertFalse(ucc.getCityStocks().getStockByType(StockType.water).isAvailable() );
		assertFalse(ucc.getCityStocks().getStockByType(StockType.wheat).isAvailable() );
		
		assertTrue( uc.getMyCivilization().getById(3, IExtractBuildingModel.class).getOutputPerHour(ucc) == 100.0d );
		assertTrue( uc.getMyCivilization().getById(2001, IExtractBuildingModel.class).getOutputPerHour(ucc) == 50.0d );
				
		UserResearch breweryImprovement = uc.getResearch(2003);
		IConvertBuildingModel model = uc.getMyCivilization().getById(2000, IConvertBuildingModel.class);
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(ccb.setPeople(100));
		
		assertEquals(100.0d, model.getOutputPerHour(ucc) , 0.00000001d);
		assertTrue(model.getInputPerHour(ucc, StockType.water) == 100);
		assertTrue(model.getInputPerHour(ucc, StockType.wheat) == 50);
		assertTrue(model.getProductionRate(ucc) == 1.0d);
		assertTrue(model.getMaxPeople(ucc)==100);
		
		fhr.setResearchLvl(breweryImprovement, 1);
		
		assertTrue(model.getOutputPerHour(ucc)== 100.0d);
		assertTrue(model.getInputPerHour(ucc, StockType.water) == 80);
		assertTrue(model.getInputPerHour(ucc, StockType.wheat) == 50);
		assertTrue(model.getProductionRate(ucc) == 1.0d);
		assertTrue(model.getMaxPeople(ucc)==100);
		
	}
	
}
