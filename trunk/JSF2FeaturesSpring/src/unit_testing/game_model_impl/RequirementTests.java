package unit_testing.game_model_impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import entities.Capital;
import entities.Civil;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.FreeHand;
import entities.stocks.StockChanger;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.Civilization;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IConvertBuildingModel;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IGameModel;
import game_model_interfaces.IImprovementModel;
import game_model_interfaces.IUnitModel;

import java.util.List;


import org.junit.Before;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext-test.xml")
public class RequirementTests {

	WorldDefinition ch;
	UsersCityConfig ucc;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	
	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException
	{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_WestEurope_City(), new Config_Korea());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
	}
	
	/*
	 *W tym te�cie sprawdzamy, czy RequirementNode zawieraj� prawid�ow� referencj� do swojego IGameModel
	 */
	@Test
	public void test_requrement_node_IGameModel()
	{
		for(ICivilization civil : ch.getAllCivilizations())
		{
			List<IGameModel> list = civil.getGameModelList(false);
			for(IGameModel gm : list)
			{
				assertTrue(gm.printDecoratorChain().equals(gm.getRequirementNode().getMyGameModel().printDecoratorChain()));
			}
		}
	}
	
	/*
	 * browarnia (id: 2000) ma dwa badania: id-2003 i id-2002.
	 * ma r�wnie� wymaganie tier lvl 1
	 */
	@Test
	public void test_brewery_childs_and_parents()
	{
		ICivilization europe = ch.getCivilization(Civil.WestEurope);
		IExtractBuildingModel browar = europe.getById( 2000 , IExtractBuildingModel.class);
		
		assertTrue(browar.getRequirementNode().getChildren().size() == 2);
		
		assertTrue(browar.getRequirementNode().getChildById(2003) != null);
		assertTrue(browar.getRequirementNode().getChildById(2002) != null);
		
		assertTrue(browar.getRequirementNode().getParentById(0) != null);
		assertTrue(browar.getRequirementNode().getParents().size() == 1);
		
	}
	
	/*
	 * Browar do budowy potrzebuje pierwszy level tier1. sprawdzamy mo�liwo�� zbudowania browaru, gdy mam level tiera 0 i 1. 
	 */

	@Test
	public void can_upgrade_brewery()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Capital capital = new Capital(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		IConvertBuildingModel browar =  uc.getMyCivilization().getById(2000,IConvertBuildingModel.class);
		
		assertFalse(browar.getRequirementNode().canBuild(capital));
		
		fhr.setResearchLvl(uc.getResearch(0), 1);
		
		assertTrue(browar.getRequirementNode().canBuild(capital));
		
	}
	
	/*
	 * 300 - badanie napraw kana�y
	 * 3 - dostawca wody
	 * 0 - badanie tier
	 * 
	 * Do upgrade "napraw kanaly" potrzeba 4 level tiera i 1 level dostawcy wody.
	 */
	@Test
	public void can_upgrade_two_requirements()
	{
		
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Capital capital = new Capital(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		IImprovementModel naprawKanaly =  uc.getMyCivilization().getById(300,IImprovementModel.class);
		
		assertFalse(naprawKanaly.getRequirementNode().canBuild(capital));
		
		fh.setLevel(capital.getBuilding(CityBuilding.class, 3), 1);
		
		assertFalse(naprawKanaly.getRequirementNode().canBuild(capital));
		
		fhr.setResearchLvl(uc.getResearch(0), 4);
		
		assertTrue(naprawKanaly.getRequirementNode().canBuild(capital));
		
		fh.setLevel(capital.getBuilding(CityBuilding.class, 3), 0);
		
		assertFalse(naprawKanaly.getRequirementNode().canBuild(capital));
		
	}
	
	/*
	 * Zdefiniowane jest wymaganie pomi�dzy "piechurem ze strzelb�" a "proch strzelniczy".
	 * Sprawdzenie czy obydwa modele maj� requirementNody, oraz czy requirementNody posiadaj� swoje modele 
	 */
	@Test
	public void requirement_have_node()
	{
		ICivilization civil = ch.getCivilization(Civil.korea);
		IImprovementModel proch = civil.getById(1001, IImprovementModel.class);
		assertTrue(proch != null);
		
		assertTrue(proch.getRequirementNode() != null);
		
		assertTrue(proch.getRequirementNode().getMyGameModel().equals(proch));
		
		ICivilization europe = ch.getCivilization(Civil.WestEurope);
		IUnitModel piechurStrzelba = europe.getById(2500, IUnitModel.class);
		
		assertTrue(piechurStrzelba.getRequirementNode() != null);
		
		assertTrue(piechurStrzelba.getRequirementNode().getMyGameModel().equals(piechurStrzelba));
		
	}
	
	/*
	 * W cywilizacji europejskiej zdefiniowano jednostke "piechur ze strzelb�", kt�ra wymaga badania "proch strzelniczy"
	 * z cywilizacji korea�skiej. Sprawdzamy czy dzia�aj� wymagania z obcych cywilziacji
	 * proch strzelniczy - id 1001
	 * piechur ze strzelb� - id 2500
	 */
	@Test
	public void requirement_from_loan_civilization()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Capital capital = new Capital(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		IUnitModel piechurStrzelba = uc.getMyCivilization().getById(2500, IUnitModel.class);
		
		assertFalse(piechurStrzelba.getRequirementNode().canBuild(capital));
		
		fhr.setResearchLvl(uc.getResearch(1001), 1);
		
		assertTrue(piechurStrzelba.getRequirementNode().canBuild(capital));
	}
	/*
	 * 516, , 513, 514, 515
	 * Testujemy drzewo wymaga�, jesli utworzy si� diament tzn. 516 jest na samej gorze, 512 i 513 wymagaj� 516, 514 wymaga 512 i 513.
	 */
	@Test
	public void diamond_requirements()
	{	
		IBuildingModel topBuilding = ch.getCivilization(Civil.common).getById(516, IBuildingModel.class);
		assertEquals(2, topBuilding.getRequirementNode().getChildren(IImprovementModel.class).size());
		assertEquals(3, topBuilding.getRequirementNode().getChildrenRecursively(IGameModel.class).size());
		assertEquals(3, topBuilding.getRequirementNode().getChildrenRecursively(IImprovementModel.class).size());
		assertEquals(0, topBuilding.getRequirementNode().getChildrenRecursively(IBuildingModel.class).size());
		
	}
	
	/*
	 * 
	 * Skomplikowane drzewo wymaga�.
	 */
	@Test
	public void diamond_requirements_spaghetti_requirements()
	{	
		IBuildingModel topBuilding = ch.getCivilization(Civil.common).getById(517, IBuildingModel.class);
		IBuildingModel bottomBuilding = ch.getCivilization(Civil.common).getById(521, IBuildingModel.class);
		IImprovementModel improvement1 = ch.getCivilization(Civil.common).getById(518, IImprovementModel.class);
		IImprovementModel improvement2 = ch.getCivilization(Civil.common).getById(519, IImprovementModel.class);
		IImprovementModel improvement3 = ch.getCivilization(Civil.common).getById(520, IImprovementModel.class);
		IUnitModel unit = ch.getCivilization(Civil.common).getById(522, IUnitModel.class);
		
		assertEquals(0, topBuilding.getRequirementNode().getParents().size());
		assertEquals(3, topBuilding.getRequirementNode().getChildren().size());
		assertEquals(2, improvement1.getRequirementNode().getChildren().size());
		assertEquals(1, improvement1.getRequirementNode().getParents().size());
		assertEquals(2, improvement2.getRequirementNode().getChildren().size());
		assertEquals(1, improvement2.getRequirementNode().getParents().size());
		assertEquals(0, improvement3.getRequirementNode().getChildren().size());
		assertEquals(3, improvement3.getRequirementNode().getParents().size());
		assertEquals(1, bottomBuilding.getRequirementNode().getParents().size());
		assertEquals(1, bottomBuilding.getRequirementNode().getChildren().size());
		assertEquals(0, unit.getRequirementNode().getChildren().size());
		assertEquals(2, unit.getRequirementNode().getParents().size());
		
		assertEquals(5, topBuilding.getRequirementNode().getChildrenRecursively(IGameModel.class).size());
		assertEquals(1, topBuilding.getRequirementNode().getChildrenRecursively(IUnitModel.class).size());
		assertEquals(3, topBuilding.getRequirementNode().getChildrenRecursively(IImprovementModel.class).size());
		assertEquals(1, topBuilding.getRequirementNode().getChildrenRecursively(IBuildingModel.class).size());
		
	}
	
}
