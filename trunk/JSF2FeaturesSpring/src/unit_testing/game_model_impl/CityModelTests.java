package unit_testing.game_model_impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_Korea_City;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Civil;
import entities.Colony;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.FreeHand;
import entities.stocks.CityFood;
import entities.stocks.StockChanger;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.ICityModel;
import game_model_interfaces.ICivilization;

/*
 * klasa testuje model miasta: birthRate , satysfakcje i wszystkie parametry zwi�zane z miastem.
 */
public class CityModelTests {

	WorldDefinition ch;
	UsersCityConfig ucc;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	
	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException
	{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea(), new Config_Korea_City(), new Config_WestEurope_City());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
	}
	
	/*
	 * private final int waterSatisf = 2;
	private final int meatSatisf = 2;
	private final int carboSatisf = 2;
	private final int addMeatSatisf = 1;
	private final int addCarboSatisf = 1;
	private final int additionSatisf = 1;
	private final double peopleFallRate = -3600.0d;
	private final long satisfCooldownMs = 5000l;
	private final int firstCitizens = 10;
	private final int maxFightRounds = 6;
	private final int birthMultiplier = 1;
	private final int eatPerHour = 1;
	 */
	@Test
	public void test_satisfaction_no_food()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		
		// upewniamy si� �e w mie�cie nie mamy zadnego jedzenia
		for(CityFood cf : ucc.getCityStocks().getFood())
		{
			assertFalse(cf.isAvailable());
		}
		
		ICityModel city = westEurope.getCityModel();
		assertEquals(0, city.getSatisfactionFromObjects(ucc));
		
	}
	
	@Test
	public void test_satisfaction_have_only_major_meat()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		// upewniamy si� �e w mie�cie nie mamy zadnego jedzenia
		for(CityFood cf : ucc.getCityStocks().getFood())
		{
			assertFalse(cf.isAvailable());
		}
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.poultry), 10.0d);
		
		ICityModel city = westEurope.getCityModel();
		assertEquals(2, city.getSatisfactionFromObjects(ucc));
		
	}
	
	@Test
	public void test_satisfaction_have_only_major_wheat()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		// upewniamy si� �e w mie�cie nie mamy zadnego jedzenia
		for(CityFood cf : ucc.getCityStocks().getFood())
		{
			assertFalse(cf.isAvailable());
		}
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.wheat), 10.0d);
		
		ICityModel city = westEurope.getCityModel();
		assertEquals(2, city.getSatisfactionFromObjects(ucc));
		
	}
	
	@Test
	public void test_satisfaction_have_only_major_addition()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		// upewniamy si� �e w mie�cie nie mamy zadnego jedzenia
		for(CityFood cf : ucc.getCityStocks().getFood())
		{
			assertFalse(cf.isAvailable());
		}
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.brewery), 10.0d);
		
		ICityModel city = westEurope.getCityModel();
		assertEquals(2, city.getSatisfactionFromObjects(ucc));
		
	}
	
	@Test
	public void test_satisfaction_have_two_major_foods_addition_and_carbo()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		// upewniamy si� �e w mie�cie nie mamy zadnego jedzenia
		for(CityFood cf : ucc.getCityStocks().getFood())
		{
			assertFalse(cf.isAvailable());
		}
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.brewery), 10.0d);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.wheat), 10.0d);
		
		ICityModel city = westEurope.getCityModel();
		assertEquals(4, city.getSatisfactionFromObjects(ucc));
		
	}
	
	@Test
	public void test_satisfaction_have_two_major_foods_addition_and_meat()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		// upewniamy si� �e w mie�cie nie mamy zadnego jedzenia
		for(CityFood cf : ucc.getCityStocks().getFood())
		{
			assertFalse(cf.isAvailable());
		}
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.brewery), 10.0d);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.poultry), 10.0d);
		
		ICityModel city = westEurope.getCityModel();
		assertEquals(4, city.getSatisfactionFromObjects(ucc));
		
	}
	
	@Test
	public void test_satisfaction_have_all_major_foods()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		// upewniamy si� �e w mie�cie nie mamy zadnego jedzenia
		for(CityFood cf : ucc.getCityStocks().getFood())
		{
			assertFalse(cf.isAvailable());
		}
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.brewery), 10.0d);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.poultry), 10.0d);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.wheat), 10.0d);
		
		ICityModel city = westEurope.getCityModel();
		assertEquals(6, city.getSatisfactionFromObjects(ucc));
		
	}
	@Test
	public void test_satisfaction_have_two_major_and_one_additional_meat()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		// upewniamy si� �e w mie�cie nie mamy zadnego jedzenia
		for(CityFood cf : ucc.getCityStocks().getFood())
		{
			assertFalse(cf.isAvailable());
		}
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.poultry), 10.0d);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.wheat), 10.0d);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.pork), 10.0d);
		
		ICityModel city = westEurope.getCityModel();
		assertEquals(5, city.getSatisfactionFromObjects(ucc));
		
	}
	@Test
	public void test_satisfaction_have_only_additional_meat()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		// upewniamy si� �e w mie�cie nie mamy zadnego jedzenia
		for(CityFood cf : ucc.getCityStocks().getFood())
		{
			assertFalse(cf.isAvailable());
		}
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.pork), 10.0d);
		
		ICityModel city = westEurope.getCityModel();
		assertEquals(1, city.getSatisfactionFromObjects(ucc));
		
	}
	
	@Test
	public void test_satisfaction_have_only_additional_foods()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		// upewniamy si� �e w mie�cie nie mamy zadnego jedzenia
		for(CityFood cf : ucc.getCityStocks().getFood())
		{
			assertFalse(cf.isAvailable());
		}
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.pork), 10.0d);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.fish), 10.0d);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.rice), 10.0d);
		
		ICityModel city = westEurope.getCityModel();
		assertEquals(3, city.getSatisfactionFromObjects(ucc));
		
	}
	
	/*
	 * Test sprawdzaj�cy satysfakcj� , kiedy w mie�cie mamy jedno jedzenie, kt�re daje dodatkow� satysfakcj�,
	 * oraz drewno. Satysfakcja powinna wynosi� 1.
	 */
	@Test
	public void test_satisfaction_have_wood_and_fish()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		// upewniamy si� �e w mie�cie nie mamy zadnego jedzenia
		for(CityFood cf : ucc.getCityStocks().getFood())
		{
			assertFalse(cf.isAvailable());
		}
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.fish), 10.0d);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.wood), 10.0d);
		
		ICityModel city = westEurope.getCityModel();
		assertEquals(1, city.getSatisfactionFromObjects(ucc));
		
	}
	
	/*
	 * Test sprawdzaj�cy satysfakcj� , kiedy w mie�cie mamy du�o rodzaj�w po�ywienia, nasze g�wne jedzenia oraz obce
	 */
	@Test
	public void test_satisfaction_have_plethora_major_and_minor_foods()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		// upewniamy si� �e w mie�cie nie mamy zadnego jedzenia
		for(CityFood cf : ucc.getCityStocks().getFood())
		{
			assertFalse(cf.isAvailable());
		}
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.fish), 10.0d); //+1
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.poultry), 10.0d); // +2
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.gold), 10.0d); // +0
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.rice), 10.0d); // +1
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.sul), 10.0d); //+1
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.brewery), 10.0d); //+2
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.beef), 10.0d); // +1
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.wheat), 10.0d); // +2
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.soy), 10.0d); //+1
		
		
		ICityModel city = westEurope.getCityModel();
		assertEquals(11, city.getSatisfactionFromObjects(ucc));
		
	}
	
	@Test
	public void test_satisfaction_boosted_by_europe_lumber_jack()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		
		CityBuilding tartak = ucc.getBuilding(CityBuilding.class, 100);
		fh.setLevel(tartak, 1);
		
		assertEquals(4,  westEurope.getCityModel().getSatisfactionFromObjects(ucc));
		
		
	}
	
	@Test
	public void test_satisfaction_boosted_by_common_building()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		
		CityBuilding uniwersytet = ucc.getBuilding(CityBuilding.class, 500);
		fh.setLevel(uniwersytet, 1);
		assertEquals(3,  westEurope.getCityModel().getSatisfactionFromObjects(ucc));
		
	}
	
	@Test
	public void test_satisfaction_boosted_by_common_building_and_lumber_jack()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ICivilization westEurope = ch.getCivilization(Civil.WestEurope);
		
		CityBuilding uniwersytet = ucc.getBuilding(CityBuilding.class, 500);
		fh.setLevel(uniwersytet, 1);
		
		CityBuilding tartak = ucc.getBuilding(CityBuilding.class, 100);
		fh.setLevel(tartak, 1);
		
		assertEquals(7,  westEurope.getCityModel().getSatisfactionFromObjects(ucc));
		
	}
	
	
}
