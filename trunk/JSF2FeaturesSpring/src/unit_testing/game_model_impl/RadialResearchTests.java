package unit_testing.game_model_impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_Korea_City;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Civil;
import entities.Colony;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UserResearch;
import entities.UsersCityConfig;
import entities.buildings.FreeHand;
import entities.stocks.StockChanger;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.Civilization;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IRadialResearchModel;


public class RadialResearchTests {

	WorldDefinition ch;
	UsersCityConfig ucc;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	
	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException
	{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea(), new Config_Korea_City(), new Config_WestEurope_City());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
	}
	
	/*
	 * Test kartografi. sprawdzamy promien na 1 levelu
	 * Id kartografii : 511
	 */
	@Test
	public void getRadius_pattern_in_common_civil()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		
		IRadialResearchModel kartografiaModel = westEurope.getById(511, IRadialResearchModel.class);
		
		assertTrue( uc.getLevel(511) == 0 );
		UserResearch kartografia = uc.getResearch(511);
		fhr.setResearchLvl(kartografia, 1);
		assertEquals(100 , kartografiaModel.getRadius(uc) );
		fhr.setResearchLvl( kartografia, 2);
		assertEquals(200 , kartografiaModel.getRadius(uc));
		
	}
	
	/*
	 * Test kartografi. sprawdzamy ulepsze zwiększające promień kartografii
	 * Id kartografii : 511
	 * id ulepszenia: 2512
	 */
	@Test
	public void getRadius_boost_radius()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		
		IRadialResearchModel kartografiaModel = westEurope.getById(511, IRadialResearchModel.class);
		UserResearch ur = uc.getResearch(2512);
		
		assertTrue( uc.getLevel(511) == 0 );
		UserResearch kartografia = uc.getResearch(511);
		fhr.setResearchLvl(kartografia, 1);
		assertEquals(100 , kartografiaModel.getRadius(uc) );
		fhr.setResearchLvl( ur, 1);
		assertEquals(110 , kartografiaModel.getRadius(uc));
		
	}
	
	
}
