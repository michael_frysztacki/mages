package unit_testing.game_model_impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import entities.Capital;
import entities.Civil;
import entities.Colony;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UserResearch;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.FreeHand;
import entities.stocks.StockChanger;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.ILandUnitModel;
import game_model_interfaces.IUnitModel;
import game_model_interfaces.IUnitModel.ArmorType;
import game_model_interfaces.IUnitModel.AttackType;
import game_model_interfaces.IUnitModel.TerainType;

import org.junit.Before;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_Korea_City;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;

/*
 * klasa testuje ulepszenia dla budynk�w i bada�. Ulepszenia mog� by� r�norakie np.
 * ulepszenia dla budynk�w wydbywczych,
 * ulepszenia dla wszystkich bada� po wzgl�dem czasu wykonania(np. technologia "druk" u europejczyk�w.
 * Testuje si� r�wnie�, czy po przej�ciu budynk�w obcej cywilizacji, nasze ulepszenia r�wnie� b�d� tam dzia�a�.
 * Np. czy badanie druk u europejczyk�w b�dzie mia�o wp�yw na czas badania w obcym budynku.
 */
public class UnitModelTests {

	WorldDefinition ch;
	UsersCityConfig ucc;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	
	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException
	{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea(), new Config_Korea_City(), new Config_WestEurope_City());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
	}
	
	@Test
	public void checkIfDefaultEatPerHourIsSet()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Capital ucc = new Capital(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		ILandUnitModel piechota = uc.getMyCivilization().getById(2500, ILandUnitModel.class);
		assertEquals( 1, piechota.getEatPerHour(uc), 0.00001d );
	}
	/*
	 * Sprawdzamy wp�yw badania ulepszony armor na piechot� ze strzelb�.
	 * Ka�dy level zwi�ksza armor o 20% warto�ci pocz�tkowej
	 */
	@Test
	public void test_armor_booster()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Capital ucc = new Capital(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserResearch ur = uc.getResearch(2501);
		IUnitModel piechota = uc.getMyCivilization().getById(2500, IUnitModel.class);
		
		assertEquals(10, piechota.getArmorOutsideCity(uc) );
		
		fhr.setResearchLvl(ur, 1);
		
		assertTrue( piechota.getArmorOutsideCity(uc) == 12);
		
		fhr.setResearchLvl(ur, 2);
		
		assertTrue( piechota.getArmorOutsideCity(uc) == 14);
		
	}
	
	
	/*
	 * Sprawdzamy ulepszenie kt�re zmienia armor na lekki od pierwszego levelu.
	 */
	@Test
	public void test_armor_type_booster()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Capital ucc = new Capital(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserResearch ur = uc.getResearch(2502);
		IUnitModel piechota = uc.getMyCivilization().getById(2500, IUnitModel.class);
		
		assertTrue( piechota.getArmorType(uc) == ArmorType.heavy);
		
		fhr.setResearchLvl(ur, 1);
		
		assertTrue( piechota.getArmorType(uc) == ArmorType.light );
		
		
	}
	
	/*
	 * Sprawdzamy ulepszenie kt�re zmienia armor na lekki od pierwszego levelu.
	 */
	@Test
	public void test_attack_type_booster()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Capital ucc = new Capital(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserResearch ur = uc.getResearch(2503);
		IUnitModel piechota = uc.getMyCivilization().getById(2500, IUnitModel.class);
		
		assertTrue( piechota.getAttackType(uc) == AttackType.normal);
		
		fhr.setResearchLvl(ur, 1);
		
		assertTrue( piechota.getAttackType(uc) == AttackType.range );
		
	}
	
	/*
	 * Sprawdzamy ulepszenie kt�re zmienia armor na lekki od pierwszego levelu.
	 */
	@Test
	public void test_attack_booster()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Capital ucc = new Capital(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserResearch ur = uc.getResearch(2504);
		IUnitModel piechota = uc.getMyCivilization().getById(2500, IUnitModel.class);
		
		assertTrue( piechota.getAttackOutsideCity(uc) == 100);
		
		fhr.setResearchLvl(ur, 1);
		
		assertTrue( piechota.getAttackOutsideCity(uc) == 120 );
		
	}
	
	/*
	 * Sprawdzamy ulepszenie kt�re zwi�ksza pr�dko�� piechoty o 20% ale tylko po d�ungli
	 */
	@Test
	public void test_jungle_speed_booster()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Capital ucc = new Capital(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserResearch ur = uc.getResearch(2505);
		ILandUnitModel piechota = uc.getMyCivilization().getById(2500, ILandUnitModel.class);
		
		assertTrue( piechota.getSpeed(uc, TerainType.jungle) == 100);
		assertTrue( piechota.getSpeed(uc, TerainType.desert) == 100);
		assertTrue( piechota.getSpeed(uc, TerainType.steps) == 100);
		assertTrue( piechota.getSpeed(uc, TerainType.plain) == 100);
		assertTrue( piechota.getSpeed(uc, TerainType.mountains) == 100);
		
		fhr.setResearchLvl(ur, 1);
		
		assertTrue( piechota.getSpeed(uc, TerainType.jungle) == 120);
		assertTrue( piechota.getSpeed(uc, TerainType.desert) == 100);
		assertTrue( piechota.getSpeed(uc, TerainType.steps) == 100);
		assertTrue( piechota.getSpeed(uc, TerainType.plain) == 100);
		assertTrue( piechota.getSpeed(uc, TerainType.mountains) == 100);
		
	}
	
	/*
	 * Sprawdzamy ulepszenie kt�re zwi�ksza pr�dko�� kusznika o 20% na wszytkich terenach
	 */
	@Test
	public void test_speed_booster()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Capital ucc = new Capital(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserResearch ur = uc.getResearch(2506);
		ILandUnitModel kusznik = uc.getMyCivilization().getById(2600, ILandUnitModel.class);
		
		assertTrue( kusznik.getSpeed(uc, TerainType.jungle) == 100);
		assertEquals(200, kusznik.getSpeed(uc, TerainType.desert));
		assertTrue( kusznik.getSpeed(uc, TerainType.steps) == 100);
		assertTrue( kusznik.getSpeed(uc, TerainType.plain) == 100);
		assertTrue( kusznik.getSpeed(uc, TerainType.mountains) == 100);
		
		fhr.setResearchLvl(ur, 1);
		
		assertTrue( kusznik.getSpeed(uc, TerainType.jungle) == 120);
		assertTrue( kusznik.getSpeed(uc, TerainType.desert) == 240);
		assertTrue( kusznik.getSpeed(uc, TerainType.steps) == 120);
		assertTrue( kusznik.getSpeed(uc, TerainType.plain) == 120);
		assertTrue( kusznik.getSpeed(uc, TerainType.mountains) == 120);
		
	}
	
	/*
	 * Zwyk�y test hp jednostki, bez �adnych ulepsze�
	 */
	@Test
	public void test_unit_hp()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		
		IUnitModel kusznik = uc.getMyCivilization().getById(2600, IUnitModel.class);
		
		assertTrue( kusznik.getHP(uc) == 100);
		
	}
	
	/*
	 * ulepszenie w cywilizacji europejskiej, kt�re zwi�ksza hp o 50%.
	 */
	@Test
	public void test_unit_hp_increased_by_50_percent()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		
		IUnitModel kusznik = uc.getMyCivilization().getById(2600, IUnitModel.class);
		
		UserResearch ur = uc.getResearch(2507);
		
		assertTrue( kusznik.getHP(uc) == 100);
		
		fhr.setResearchLvl(ur, 1);
		
		assertTrue( kusznik.getHP(uc) == 150);
		
	}
	
	/*
	 * ulepszenie w cywilizacji og�lnej, kt�re zwi�ksza hp wszystkich jednostek o 50% na ka�dym levelu.
	 * Sprawdzamy czy kusznik kt�ry znajduj� sie w cywilizacji rozszerzaj�cej cywilizacj� common, b�dzie mia� ulepszone hp.
	 */
	@Test
	public void test_increase_hp_of_all_units_extended_civil()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		
		IUnitModel kusznik = uc.getMyCivilization().getById(2600, IUnitModel.class);
		
		UserResearch ur = uc.getResearch(502);
		
		assertTrue( kusznik.getHP(uc) == 100);
		
		fhr.setResearchLvl(ur, 1);
		
		assertEquals( 150 , kusznik.getHP(uc) );
		
	}
	
	/*
	 * ulepszenie w cywilizacji og�lnej, kt�re zwi�ksza hp wszystkich jednostek o 50% na ka�dym levelu.
	 * Sprawdzamy czy jednostka znajduj� sie w tej samej cywilziacji (common) te� b�dzie mia�a ulepszenie.
	 */
	@Test
	public void test_increase_hp_of_all_units_same_civil()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		
		IUnitModel someUnit = uc.getMyCivilization().getById(503, IUnitModel.class);
		
		UserResearch ur = uc.getResearch(502);
		
		assertTrue( someUnit.getHP(uc) == 100);
		
		fhr.setResearchLvl(ur, 1);
		
		assertTrue( someUnit.getHP(uc) == 150);
		
	}
	
	/*
	 * ulepszenie w cywilizacji WestEurope, kt�re zwi�ksza hp wszystkich jednostek o 20% na ka�dym levelu.
	 * Sprawdzamy czy jednostka znajduj�ca si� w cywilizacji common te� b�dzie mia�a ulepszenie.
	 */
	@Test
	public void test_increase_hp_of_all_units_same_civil_improvement_in_extending_civil()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		
		IUnitModel someUnit = uc.getMyCivilization().getById(503, IUnitModel.class);
		IUnitModel kusznik = uc.getMyCivilization().getById(2600, IUnitModel.class);
		
		UserResearch ur = uc.getResearch(2508);
		
		assertTrue( someUnit.getHP(uc) == 100);
		assertTrue( kusznik.getHP(uc) == 100);
		
		fhr.setResearchLvl(ur, 1);
		assertEquals(120, someUnit.getHP(uc));
		assertEquals(120, kusznik.getHP(uc));
		
	}
	/*
	 * ulepszenie w cywilizacji WestEurope, kt�re zwi�ksza hp wszystkich jednostek o 20% na ka�dym levelu.
	 * Sprawdzamy czy jednostka znajduj�ca si� w cywilizacji ni�ej, b�dzie mia�a wp�yw z tego badania na jednostk�
	 */
	@Test
	public void test_increase_hp_of_all_units_same_civil_improvement_in_child_civil()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch , Civil.Malbork_WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		
		IUnitModel someUnit = uc.getMyCivilization().getById(5000, IUnitModel.class);
		
		assertTrue(someUnit != null);
		
		UserResearch ur = uc.getResearch(2508);
		
		assertTrue(ur != null);
		
		assertEquals(100, someUnit.getHP(uc));
		
		fhr.setResearchLvl(ur, 1);
		assertEquals(120, someUnit.getHP(uc));
		
	}
	
	/*
	 * Sprawdzamy, czy badanie w cywilizacji west europe zwi�kszaj�ce hp o 20%, ma wp�yw na inne cywilizacje,
	 * nie mog�ce w og�le robi� tego badania.
	 */
	@Test
	public void test_increase_hp_of_all_units_pararell_civilizations()
	{
		long start = System.currentTimeMillis();
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.korea , true, null, Language.PL);
		long stop = System.currentTimeMillis();
		
		System.out.print("czas: " + (stop - start) + "\n");
		uc.setSpringJSFUtil(su);
		
		
		IUnitModel unitFromKorea = ch.getCivilization(Civil.korea).getById(15, IUnitModel.class);
		
		
		assertEquals(100,  unitFromKorea.getHP(uc));
		
	}
	
	/*
	 * Mamy dwa badania zwi�kszj�ce hp jedno o 20%, drugie o 50%.
	 * Oba badania powinny zwi�kszy� warto�� bazow� i nie nak�ada� si� na siebie.
	 * 2508 , 502
	 */
	@Test
	public void test_overlaped_hp_improvement()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		
		IUnitModel unitFromKorea = ch.getCivilization(Civil.WestEurope).getById(2600, IUnitModel.class);
		
		assertEquals(100, unitFromKorea.getHP(uc));
		
		UserResearch ur1 = uc.getResearch(502);
		assertTrue(ur1 != null);
		
		UserResearch ur2 = uc.getResearch(2508);
		assertTrue(ur2 != null);
		
		assertEquals(100, unitFromKorea.getHP(uc));
		
		fhr.setResearchLvl(ur1, 1);
		fhr.setResearchLvl(ur2, 1);
		
		assertEquals(170, unitFromKorea.getHP(uc));
		
	}
	
	/*
	 * Mamy dwa badania zwi�kszj�ce hp jedno o 20%, drugie o +10.
	 * Najpierw powinno zadzia�a� badanie +20% a potem dopiero +10.
	 * W definicji cywilizacji najpierw zdefiionwano hpBooster kt�ry robi hp+10, a potem dopiero hpbooster kt�ry mno�y hp*1.2,
	 * po to aby wymusi� niepoprawn� kolejno�� pod��czania booster�w.
	 * 2508 , 2509 to id bada�
	 */
	@Test
	public void test_overlaped_hp_improvement_multiply_and_addition()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		
		IUnitModel unitFromKorea = ch.getCivilization(Civil.WestEurope).getById(2600, IUnitModel.class);
		
		assertEquals(100, unitFromKorea.getHP(uc));
		
		UserResearch ur1 = uc.getResearch(2509);
		assertTrue(ur1 != null);
		
		UserResearch ur2 = uc.getResearch(2508);
		assertTrue(ur2 != null);
		
		fhr.setResearchLvl(ur1, 1);
		fhr.setResearchLvl(ur2, 1);
		
		assertEquals(130, unitFromKorea.getHP(uc));
		
	}
	
	/*
	 * Mamy dwa badania zwi�kszj�ce armor jedno o 20%, drugie o +10.
	 * Najpierw powinno zadzia�a� badanie +20% a potem dopiero +10.
	 * 507,508 to id bada�
	 */
	@Test
	public void test_overlaped_armor_improvement_double_multiply()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		
		IUnitModel unitFromCommon = ch.getCivilization(Civil.WestEurope).getById(503, IUnitModel.class);
		
		assertEquals(10, unitFromCommon.getArmorOutsideCity(uc) );
		
		UserResearch ur1 = uc.getResearch(507);
		assertTrue(ur1 != null);
		
		UserResearch ur2 = uc.getResearch(508);
		assertTrue(ur2 != null);
		
		fhr.setResearchLvl(ur1, 1);
		fhr.setResearchLvl(ur2, 1);
		
		assertEquals(15, unitFromCommon.getArmorOutsideCity(uc) );
		
	}
	
	/*
	 * Oba badania s� zdefiniowane w cywilziacji West_Europe
	 * Mamy dwa badania zwi�kszj�ce atack jedno o 20%, drugie o +10.
	 * Najpierw powinno zadzia�a� badanie +20% a potem dopiero +10.
	 * Zwi�kszaj� atak kusznika.
	 * 2510, 2511 to id bada�
	 */
	@Test
	public void test_overlaped_armor_improvement_double_attack_booster()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.Malbork_WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		
		IUnitModel unitFromEurope = ch.getCivilization(Civil.WestEurope).getById(2600, IUnitModel.class);
		
		assertEquals(100, unitFromEurope.getAttackOutsideCity(uc));
		
		UserResearch ur1 = uc.getResearch(2510);
		assertTrue(ur1 != null);
		
		UserResearch ur2 = uc.getResearch(2511);
		assertTrue(ur2 != null);
		
		fhr.setResearchLvl(ur1, 1);
		
		assertEquals(120, unitFromEurope.getAttackOutsideCity(uc));
		
		fhr.setResearchLvl(ur2, 1);
		
		assertEquals(130, unitFromEurope.getAttackOutsideCity(uc));
		
	}
	
	/*
	 * Oba badania s� zdefiniowane w cywilziacji West_Europe_city
	 * Mamy dwa badania zwi�kszj�ce capacity wszystkich jednostek , jedno o 20%, drugie o +30%.
	 * Badania nie powinny si� na�o�y� na siebie.
	 * Zwi�kszaj� atak kusznika.
	 * 3001, 3002 , id bada�
	 */
	@Test
	public void test_overlaped_capacity_improvement_double_booster()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.Malbork_WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		
		IUnitModel unitFromMalbork = ch.getCivilization(Civil.Malbork_WestEurope).getById(5000, IUnitModel.class);
		IUnitModel unitFromEurope = ch.getCivilization(Civil.WestEurope).getById(2600, IUnitModel.class);
		IUnitModel unitFromCommon = ch.getCivilization(Civil.common).getById(503, IUnitModel.class);
		
		assertEquals(10, unitFromMalbork.getCapacity(uc));
		assertEquals(10, unitFromEurope.getCapacity(uc));
		assertEquals(10, unitFromCommon.getCapacity(uc));
		
		UserResearch ur1 = uc.getResearch(3001);
		assertTrue(ur1 != null);
		
		UserResearch ur2 = uc.getResearch(3002);
		assertTrue(ur2 != null);
		
		fhr.setResearchLvl(ur1, 1);
		
		assertEquals(12, unitFromMalbork.getCapacity(uc));
		assertEquals(12, unitFromEurope.getCapacity(uc));
		assertEquals(12, unitFromCommon.getCapacity(uc));
		
		fhr.setResearchLvl(ur2, 1);
		
		assertEquals(15, unitFromMalbork.getCapacity(uc));
		assertEquals(15, unitFromEurope.getCapacity(uc));
		assertEquals(15, unitFromCommon.getCapacity(uc));
		
	}
	
	/*
	 * Testujemy fortyfikacj�.
	 * Fortyfikacja w europie nie zwi�ksza ataku jednostko z normalnym atakiem (id 2600)
	 * Level fortyfikacji nie powinien zwi�ksza� atakku jednostec 2600
	 */
	@Test
	public void test_fortyfication_no_improved_attack()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig frikoCity = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityBuilding fortyfication = frikoCity.getBuilding(CityBuilding.class, 510);
		
		IUnitModel unitFromEurope = ch.getCivilization(Civil.WestEurope).getById(2600, IUnitModel.class);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 4);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 5);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 100);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
	}
	
	/*
	 * Testujemy fortyfikacj�.
	 * Fortyfikacja w europie od 5 levelu zwi�ksza atak wszystkim jednostkom strzelaj�cym.
	 * Ka�dy level fortyfikacji zwi�ksza o +100 obra�enia.
	 */
	@Test
	public void test_fortyfication_improved_attack()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig frikoCity = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityBuilding fortyfication = frikoCity.getBuilding(CityBuilding.class, 510);
		
		IUnitModel unitFromEurope = ch.getCivilization(Civil.WestEurope).getById(2601, IUnitModel.class);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		
		fh.setLevel(fortyfication, 4);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 5);
		assertEquals(600, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 6);
		assertEquals(700, unitFromEurope.getAttackInsideCity(frikoCity));
	}
	
	/*
	 * Testujemy fortyfikacj�.
	 * Fortyfikacja w europie od 5 levelu zwi�ksza atak wszystkim jednostkom strzelaj�cym.
	 * Ka�dy level fortyfikacji zwi�ksza o +100 obra�enia.
	 * Jednostka w europie id 2602 jest chroniona przez fortyfikacj� szybciej, bo on 4 levelu fortyfikacji
	 */
	@Test
	public void test_fortyfication_override_protection_for_ranged()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig frikoCity = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityBuilding fortyfication = frikoCity.getBuilding(CityBuilding.class, 510);
		
		IUnitModel unitFromEurope = ch.getCivilization(Civil.WestEurope).getById(2602, IUnitModel.class);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		
		fh.setLevel(fortyfication, 3);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 4);
		assertEquals(500, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 5);
		assertEquals(600, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 6);
		assertEquals(700, unitFromEurope.getAttackInsideCity(frikoCity));
	}
	
	/*
	 * Testujemy fortyfikacj�.
	 * Fortyfikacja w europie od 5 levelu zwi�ksza atak wszystkim jednostkom strzelaj�cym.
	 * Ka�dy level fortyfikacji zwi�ksza o +100 obra�enia.
	 * Jednostka w europie id 2603 jest chroniona przez fortyfikacj� p�niej, bo od 10 levelu fortyfikacji
	 */
	@Test
	public void test_fortyfication_override_protection_for_ranged_second_test()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig frikoCity = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityBuilding fortyfication = frikoCity.getBuilding(CityBuilding.class, 510);
		
		IUnitModel unitFromEurope = ch.getCivilization(Civil.WestEurope).getById(2603, IUnitModel.class);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		
		fh.setLevel(fortyfication, 3);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 4);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 5);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 9);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 10);
		assertEquals(1100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 11);
		assertEquals(1200, unitFromEurope.getAttackInsideCity(frikoCity));
	}
	
	/*
	 * Testujemy fortyfikacj�.
	 * Fortyfikacja w europie od 5 levelu zwi�ksza atak wszystkim jednostkom strzelaj�cym.
	 * Ka�dy level fortyfikacji zwi�ksza o +100 obra�enia.
	 * Sprawdzamy czy gracz graj�cy malborkiem, kt�ry rozszerza europe, b�dzie mia� r�wnie� zapewnion� ochron�
	 */
	@Test
	public void test_fortyfication_protection_for_player_with_extending_civil()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.Malbork_WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig frikoCity = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityBuilding fortyfication = frikoCity.getBuilding(CityBuilding.class, 510);
		
		IUnitModel unitFromEurope = ch.getCivilization(Civil.WestEurope).getById(2603, IUnitModel.class);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		
		fh.setLevel(fortyfication, 3);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 4);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 5);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 9);
		assertEquals(100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 10);
		assertEquals(1100, unitFromEurope.getAttackInsideCity(frikoCity));
		fh.setLevel(fortyfication, 11);
		assertEquals(1200, unitFromEurope.getAttackInsideCity(frikoCity));
	}
	
}
