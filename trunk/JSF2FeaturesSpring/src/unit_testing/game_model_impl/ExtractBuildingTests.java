package unit_testing.game_model_impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_NorthEurope;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;
import unit_testing.game_model_impl.BuildingTests.TestRandomizator;

import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Civil;
import entities.Colony;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UserResearch;
import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;
import entities.buildings.FreeHand;
import entities.stocks.StockChanger;
import entities.units.TripArmy;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IExtractBuildingModel;



//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext-test.xml")
public class ExtractBuildingTests {

	WorldDefinition ch;
	UsersCityConfig ucc;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	
	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException
	{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea(), new Config_NorthEurope(), new Config_WestEurope_City());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
	}
	
	/*
	 * w tym te�cie zdefiniujemy sobie wzorek na wydobywanie w cywilizacji og�lnej, a kopalnie w cywilizacji europejskiej.
	 * Wzorek na maksymaln� ilo�� ludzi definiujemy w cywilizacji europejskiej jako baseAmount*lvl. Level budynku ustawiamy na 1,
	 * bazowa ilo�� ludzi ustawiona jest na 100, wi�c maks ludzi ile mo�emy umie�ci� w tym budynku to 100.
	 * sprawdzimy czy wzorek b�dzie dzia�a�.
	 *
	 */
	@Test
	public void test_pattern_from_parent_civilization()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ExtractCityBuilding eb = ucc.getBuilding(ExtractCityBuilding.class, 100);
		fh.setLevel(eb, 1);
		
		stockCh.freeHand(ucc.getCityStocks().getPeopleStock(), 100.0d);
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(eb.setPeople(100));
		IExtractBuildingModel model = uc.getMyCivilization().getById(100, IExtractBuildingModel.class);
		assertEquals(500, model.getOutputPerHour(ucc));
		assertEquals(100, model.getMaxPeople(ucc));
		
		
	}
	/*
	 * w tym te�cie zdefiniujemy sobie wzorek na wydobywanie w cywilizacji og�lnej, a kopalnie w cywilizacji europejskiej.
	 * Wzorek na maksymaln� ilo�� ludzi definiujemy w cywilizacji europejskiej jako baseAmount*lvl. Level budynku ustawiamy na 1,
	 * bazowa ilo�� ludzi ustawiona jest na 100, ale nie wsadzamy do budynku �adnych ludzi.
	 * sprawdzimy czy wzorek b�dzie dzia�a�. Wydobycie powinno wynosi� 0.
	 *
	 */
	@Test
	public void test_pattern_from_parent_civilization_2()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		ExtractCityBuilding eb = ucc.getBuilding(ExtractCityBuilding.class, 100);
		fh.setLevel(eb, 1);
		
		IExtractBuildingModel model = uc.getMyCivilization().getById(100, IExtractBuildingModel.class);
		assertTrue(model.getOutputPerHour(ucc)==0);
		assertTrue(model.getMaxPeople(ucc)==100);
	}
	/*
	 * W tym te�cie zdefiniujemy sobie wzorek na wydobywanie w tej samej cywilizacji co zdefiniowany jest budynek.
	 * wzorek na wydobycie to lodeFactor*2*peopleCount. lodeFactor = 5, peopleCount= 100.  
	 * Wzorek na maksymaln� ilo�� ludzi definiujemy w cywilizacji europejskiej jako baseAmount*lvl. Level budynku ustawiamy na 1,
	 * bazowa ilo�� ludzi ustawiona jest na 100, wi�c maks ludzi ile mo�emy umie�ci� w tym budynku to 100.
	 * sprawdzimy czy wzorek b�dzie dzia�a�.
	 *
	 */
	@Test
	public void test_pattern_from_own_civilization()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		ExtractCityBuilding eb = ucc.getBuilding(ExtractCityBuilding.class, 101);
		fh.setLevel(eb, 1);
		
		stockCh.freeHand(ucc.getCityStocks().getPeopleStock(), 100.0d);
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(eb.setPeople(100));
		
		IExtractBuildingModel model = uc.getMyCivilization().getById(101, IExtractBuildingModel.class);
		assertEquals(1000, model.getOutputPerHour(ucc));
		assertEquals(100, model.getMaxPeople(ucc));
	}
	
	/*
	 *   
	 * Wzorek na maksymaln� ilo�� ludzi definiujemy w cywilizacji europejskiej jako baseAmount*lvl. Level budynku ustawiamy na 1,
	 * W cywilizacji og�lnej zdefiniowano badanie, kt�re zwi�ksza maksymaln� ilo�� ludzi w budynku o id = 3
	 *
	 */
	@Test
	public void test_max_People_boost()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		ExtractCityBuilding eb = ucc.getBuilding(ExtractCityBuilding.class, 3);
		assertTrue(eb != null);
		fh.setLevel(eb, 1);
		
		UserResearch imp = uc.getResearch(504);
		assertTrue(imp != null);
		
		stockCh.freeHand(ucc.getCityStocks().getPeopleStock(), 100.0d);
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(eb.setPeople(100));
		
		IExtractBuildingModel model = uc.getMyCivilization().getById(3, IExtractBuildingModel.class);
		assertEquals(100,model.getMaxPeople(ucc));
		fhr.setResearchLvl(imp, 1);
		assertEquals(110,model.getMaxPeople(ucc));
	}
	
	/*
	 *   
	 * Wzorek na maksymaln� ilo�� ludzi definiujemy w cywilizacji europejskiej jako baseAmount*lvl. Level budynku ustawiamy na 1,
	 * W cywilizacji og�lnej zdefiniowano dwa badania, jedno zwi�ksza o 20% , drugie o 30% maksymaln� ilo��i ludzi w budynku.
	 * Sprawdzmy czy badania si� na siebie na nak�adaj�.
	 */
	@Test
	public void test_max_People_double_boost()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		ExtractCityBuilding eb = ucc.getBuilding(ExtractCityBuilding.class, 3);
		assertTrue(eb != null);
		fh.setLevel(eb, 1);
		
		UserResearch imp = uc.getResearch(505);
		UserResearch imp2 = uc.getResearch(506);
		assertTrue(imp != null);
		assertTrue(imp2 != null);
		
		stockCh.freeHand(ucc.getCityStocks().getPeopleStock(), 100.0d);
		/*
		 * w mie�cie mam 1000 wolnych ludzi, wie� ustawienie 100 ludzi w budynku musi si� powie��
		 */
		assertTrue(eb.setPeople(100));
		
		IExtractBuildingModel model = uc.getMyCivilization().getById(3, IExtractBuildingModel.class);
		assertEquals(100,model.getMaxPeople(ucc));
		fhr.setResearchLvl(imp, 1);
		fhr.setResearchLvl(imp2, 1);
		assertEquals(150,model.getMaxPeople(ucc));
	}
	
	/*
	 * Przejmujemy budynek cywilziacji korea�skiej.
	 * Sprawdzamy czy dzia�a tam wzorek dla cywilizacji korea�skiej na czas budowy budynku.
	 */
	@Test
	public void getBuildTime_takingOverExtractBuildings(){
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.NorthEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		UsersCityConfig frikoCity = new Colony(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserConfig galan = new UserConfig("galan", "pass", ch  , Civil.korea , true, null, Language.PL);
		galan.setSpringJSFUtil(su);
		Colony galanCity = new Colony(galan, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		TripArmy winner = mock(TripArmy.class);
		when(winner.getOwner()).thenReturn(friko);
		
		galanCity.takeOverColony(winner, new TestRandomizator());
		
		ExtractCityBuilding ecb = galanCity.getBuilding(ExtractCityBuilding.class, 1000);
		assertTrue(ecb.getUcc().getUserConfig() == friko);
		assertTrue(ecb.getUcc() == galanCity);
		
		IExtractBuildingModel riceFarm = galan.getMyCivilization().getById(1000, IExtractBuildingModel.class);
		assertEquals( new Integer(10) , riceFarm.getBuildTime(galanCity));
		
	}
	
	/*
	 * W cywilizacji og�lnej zdefiniowany jest budynek produkuj�cy ryby (id = 600) , kt�ry ma okre�lony wz�r na ilo�� ludzi w budynku.
	 * W cywilizacji europejskie zredefiniujemy sobie sam wzorek na ilo�� ludzi tak, �eby mie�ci�a ona o 20% wi�cej ludzi.
	 */
	@Test
	public void test_changed_pattern()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		UsersCityConfig frikoCity = new Colony(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserConfig galan = new UserConfig("galan", "pass", ch  , Civil.korea , true, null, Language.PL);
		galan.setSpringJSFUtil(su);
		UsersCityConfig galanCity = new Colony(galan, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		IExtractBuildingModel fishProd = ch.getCivilization(Civil.common).getById(600, IExtractBuildingModel.class);
		
		ExtractCityBuilding europeFish = frikoCity.getBuilding(ExtractCityBuilding.class, 600);
		fh.setLevel(europeFish, 1);
		assertEquals(120 , fishProd.getMaxPeople(frikoCity)  );
		
		ExtractCityBuilding koreanFish = galanCity.getBuilding(ExtractCityBuilding.class, 600);
		fh.setLevel(koreanFish, 1);
		assertEquals( 100 , fishProd.getMaxPeople(galanCity));
		
	}
	
	/*
	 * W cywilizacji og�lnej zdefiniowany jest budynek produkuj�cy ryby (id = 600) , kt�ry ma okre�lony wz�r na ilo�� ludzi w budynku.
	 * Ten sam wz�r na ludzi wykorzystuje r�wnie� chodowla kurczak�w, kt�ra te� zdefiniowana jest w cywilizacji wsp�lnej.
	 * W cywilizacji europejskiej chcieliby�my zmieni� sam wz�r na maksymaln� ilo�� ludzi tylko na chodowl� ryb - zwi�kszaj�c o 20%.
	 * Wz�r na maksymaln� ilo�� ludzi w chodowli kurczak�w powinien zosta� niezmieniony dla europejczyka. 
	 */
	@Test
	public void test_changed_pattern_only_for_fish_house()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		UsersCityConfig frikoCity = new Colony(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserConfig galan = new UserConfig("galan", "pass", ch  , Civil.korea , true, null, Language.PL);
		galan.setSpringJSFUtil(su);
		UsersCityConfig galanCity = new Colony(galan, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		IExtractBuildingModel fishProd = ch.getCivilization(Civil.common).getById(600, IExtractBuildingModel.class);
		IExtractBuildingModel poultryProd = ch.getCivilization(Civil.common).getById(601, IExtractBuildingModel.class);
		
		ExtractCityBuilding koreanFish = galanCity.getBuilding(ExtractCityBuilding.class, 600);
		fh.setLevel(koreanFish, 1);
		ExtractCityBuilding koreanPoultry = galanCity.getBuilding(ExtractCityBuilding.class, 601);
		fh.setLevel(koreanPoultry, 1);
		
		assertEquals( 100 , fishProd.getMaxPeople(galanCity));
		assertEquals( 100 , poultryProd.getMaxPeople(galanCity));
		
		ExtractCityBuilding europeFish = frikoCity.getBuilding(ExtractCityBuilding.class, 600);
		fh.setLevel(europeFish, 1);
		ExtractCityBuilding europePoultry = frikoCity.getBuilding(ExtractCityBuilding.class, 601);
		fh.setLevel(europePoultry, 1);
		
		assertEquals(120 , fishProd.getMaxPeople(frikoCity) );
		assertEquals( 100 , poultryProd.getMaxPeople(frikoCity) );
		
	}
	
	/*
	 * W cywilizacji og�lnej zdefiniowany jest budynek produkuj�cy ryby (id = 600) , kt�ry ma okre�lony wz�r na ilo�� ludzi w budynku.
	 * Ten sam wz�r na ludzi wykorzystuje r�wnie� chodowla kurczak�w, kt�ra te� zdefiniowana jest w cywilizacji wsp�lnej.
	 * W cywilizacji europejskiej chcieliby�my zmieni� sam wz�r na maksymaln� ilo�� ludzi tylko na chodowl� ryb - zwi�kszaj�c o 20%.
	 * Wz�r na maksymaln� ilo�� ludzi w chodowli kurczak�w powinien zosta� niezmieniony dla europejczyka.
	 * Wzory cywilizacji rozszerzaj�cych cywilziacj� europejsk�, r�wnie� powinny si� zachowywa� jak wzory europejskie 
	 */
	@Test
	public void test_changed_pattern_only_for_fish_house_civil_extending_europe(){
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.Malbork_WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		UsersCityConfig frikoCity = new Colony(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		UserConfig galan = new UserConfig("galan", "pass", ch  , Civil.korea , true, null, Language.PL);
		galan.setSpringJSFUtil(su);
		UsersCityConfig galanCity = new Colony(galan, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		IExtractBuildingModel fishProd = ch.getCivilization(Civil.common).getById(600, IExtractBuildingModel.class);
		IExtractBuildingModel poultryProd = ch.getCivilization(Civil.common).getById(601, IExtractBuildingModel.class);
		
		ExtractCityBuilding koreanFish = galanCity.getBuilding(ExtractCityBuilding.class, 600);
		fh.setLevel(koreanFish, 1);
		ExtractCityBuilding koreanPoultry = galanCity.getBuilding(ExtractCityBuilding.class, 601);
		fh.setLevel(koreanPoultry, 1);
		
		assertEquals( 100 , fishProd.getMaxPeople(galanCity));
		assertEquals( 100 , poultryProd.getMaxPeople(galanCity));
		
		ExtractCityBuilding europeFish = frikoCity.getBuilding(ExtractCityBuilding.class, 600);
		fh.setLevel(europeFish, 1);
		ExtractCityBuilding europePoultry = frikoCity.getBuilding(ExtractCityBuilding.class, 601);
		fh.setLevel(europePoultry, 1);
		
		assertEquals(120 , fishProd.getMaxPeople(frikoCity) );
		assertEquals( 100 , poultryProd.getMaxPeople(frikoCity) );
		
	}
	/*
	 * W cywilizacji og�lnej zdefiniowany jest budynek produj�cy wod� (id = e) , kt�ry ma okre�lony wz�r na czas budowy.
	 * W cywilizacji europejskiej chcieliby�my zredefiniowa� wz�r na czas budowy, z tym �e na wz�r, kt�ry znajduje sie 
	 * cywilizacji wsp�lnej ( tzn ze nowy wz�r wcale nie jest w cywilizacji w kt�rej zrobili�my przekierowanie ). 
	 */
	@Test
	public void test_changed_pattern_for_water_provider_changed_pattern_from_same_civilization_as_water_provider()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		UsersCityConfig frikoCity = new Colony(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		UserConfig galan = new UserConfig("galan", "pass", ch  , Civil.korea , true, null, Language.PL);
		galan.setSpringJSFUtil(su);
		UsersCityConfig galanCity = new Colony(galan, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		IExtractBuildingModel waterProd = ch.getCivilization(Civil.common).getById(3, IExtractBuildingModel.class);
		assertEquals( new Integer(10) , waterProd.getBuildTime(galanCity));
		assertEquals( new Integer(3000) , waterProd.getBuildTime(frikoCity));
		
	}
	/*
	 * W cywilizacji og�lnej zdefiniowany jest budynek produj�cy wod� (id = e) , kt�ry ma okre�lony wz�r na czas budowy.
	 * W cywilizacji europejskiej chcieliby�my zredefiniowa� wz�r na czas budowy, z tym �e na wz�r, kt�ry znajduje sie 
	 * cywilizacji wsp�lnej ( tzn ze nowy wz�r wcale nie jest w cywilizacji w kt�rej zrobili�my przekierowanie ).
	 * W cywilziacjach rozszerzaj�cych cywilizacj� europejsk�, r�wnie� powinne zachodzi� powy�sze rzeczy. 
	 */
	@Test
	public void test_changed_pattern_for_water_provider_city_civil()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.Malbork_WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		UsersCityConfig frikoCity = new Colony(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserConfig galan = new UserConfig("galan", "pass", ch  , Civil.korea , true, null, Language.PL);
		galan.setSpringJSFUtil(su);
		UsersCityConfig galanCity = new Colony(galan, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		IExtractBuildingModel waterProd = ch.getCivilization(Civil.common).getById(3, IExtractBuildingModel.class);
		
		assertEquals( new Integer(10) , waterProd.getBuildTime(galanCity));
		
		assertEquals( new Integer(3000) , waterProd.getBuildTime(frikoCity));
		
	}
	/*
	 * W cywilizacji og�lnej zdefiniowany jest budynek produkuj�cy ryby (id = 600) , kt�ry ma okre�lony wz�r na ilo�� ludzi w budynku.
	 * Ten sam wz�r na ludzi wykorzystuje r�wnie� chodowla kurczak�w, kt�ra te� zdefiniowana jest w cywilizacji wsp�lnej.
	 * W cywilizacji nordyckiej chcieliby�my zmieni� sam wz�r na maksymaln� ilo�� ludzi w chodowli ryb i kurczak�w.
	 * maxPeoplePatternId dla produkcji kurczka i ryb pozostaje nie zmieniony, podmieniamy tylko sam wzorek na taki,
	 * kt�ry powoduje o 20% wi�kszy przyrost ludzi w tych budynkach dla cywilizacji nordyckiej
	 */
	@Test
	public void test_changed_pattern_for_fish_and_poultry()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.NorthEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		UsersCityConfig frikoCity = new Colony(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserConfig galan = new UserConfig("galan", "pass", ch  , Civil.korea , true, null, Language.PL);
		galan.setSpringJSFUtil(su);
		UsersCityConfig galanCity = new Colony(galan, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		IExtractBuildingModel fishProd = ch.getCivilization(Civil.common).getById(600, IExtractBuildingModel.class);
		IExtractBuildingModel poultryProd = ch.getCivilization(Civil.common).getById(601, IExtractBuildingModel.class);
		
		ExtractCityBuilding koreanFish = galanCity.getBuilding(ExtractCityBuilding.class, 600);
		fh.setLevel(koreanFish, 1);
		ExtractCityBuilding koreanPoultry = galanCity.getBuilding(ExtractCityBuilding.class, 601);
		fh.setLevel(koreanPoultry, 1);
		
		assertEquals( 100 , fishProd.getMaxPeople(galanCity));
		assertEquals( 100 , poultryProd.getMaxPeople(galanCity));
		
		ExtractCityBuilding europeFish = frikoCity.getBuilding(ExtractCityBuilding.class, 600);
		fh.setLevel(europeFish, 1);
		ExtractCityBuilding europePoultry = frikoCity.getBuilding(ExtractCityBuilding.class, 601);
		fh.setLevel(europePoultry, 1);
		
		assertEquals(120 , fishProd.getMaxPeople(frikoCity) );
		assertEquals( 120 , poultryProd.getMaxPeople(frikoCity) );
	}
	
	
}
