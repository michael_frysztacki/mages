package unit_testing.game_model_impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import entities.Civil;
import entities.Colony;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.FreeHand;
import entities.stocks.StockChanger;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.Civilization;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IFortyficationModel;

import org.junit.Before;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_Korea_City;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;


public class FortyficationTests {

	WorldDefinition ch;
	UsersCityConfig ucc;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	
	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException
	{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea(), new Config_Korea_City(), new Config_WestEurope_City());
		ch = wd;
		
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
	}
	
	/*
	 * Test hp fortyfikacji. sprawdzy jego ilo�� na 1 levelu.
	 * Id fortyfikacji : 510
	 */
	@Test
	public void getFortyficationHP_pattern_in_common_civil()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		
		IFortyficationModel fortyfikacjaModel = westEurope.getById(510, IFortyficationModel.class);
		
		assertTrue( ucc.getLevel(510) == 0 );
		CityBuilding fortyfikacja = ucc.getBuilding(CityBuilding.class, 510);
		fh.setLevel(fortyfikacja, 1);
		assertEquals(100 , fortyfikacjaModel.getFortyficationHP(ucc));
		fh.setLevel(fortyfikacja, 2);
		assertEquals(200 , fortyfikacjaModel.getFortyficationHP(ucc));
		
	}
	
	/*
	 * Test ulepszenia hp fortyfikacji. Ka�dy level tartaku zwi�ksza hp o 20%.
	 * Id fortyfikacji : 510
	 * id tartaku: 100
	 */
	@Test
	public void getFortyficationHP_hp_boosted_by_lumber_mill()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		IFortyficationModel fortyfikacjaModel = westEurope.getById(510, IFortyficationModel.class);
		assertTrue( ucc.getLevel(510) == 0 );
		CityBuilding fortyfikacja = ucc.getBuilding(CityBuilding.class, 510);
		fh.setLevel(fortyfikacja, 1);
		assertTrue( ucc.getLevel(510) == 1 );
		CityBuilding tartak = ucc.getBuilding(CityBuilding.class, 100);
		fh.setLevel(tartak, 1);
		assertEquals(120 , fortyfikacjaModel.getFortyficationHP(ucc));
		fh.setLevel(tartak, 2);
		assertEquals(140 , fortyfikacjaModel.getFortyficationHP(ucc));
		
	}
	
	/*
	 * Test ulepszenia hp fortyfikacji. Tartak zwi�ksza hp o 20% , kopalnia �elaza zwi�ksza o +10.
	 * Id fortyfikacji : 510
	 * id kopalni �elaza : 101
	 * id tartaku: 100
	 */
	@Test
	public void getRepairRate_double_boosted_repair_rate_by_lumber_jack_and_iron_factory()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		
		IFortyficationModel fortyfikacjaModel = westEurope.getById(510, IFortyficationModel.class);
		
		assertTrue( ucc.getLevel(510) == 0 );
		CityBuilding fortyfikacja = ucc.getBuilding(CityBuilding.class, 510);
		fh.setLevel(fortyfikacja, 1);
		
		assertTrue( ucc.getLevel(510) == 1 );
		CityBuilding tartak = ucc.getBuilding(CityBuilding.class, 100);
		
		fh.setLevel(tartak, 1);
		assertEquals(120 , fortyfikacjaModel.getFortyficationHP(ucc));
		fh.setLevel(tartak, 2);
		assertEquals(140 , fortyfikacjaModel.getFortyficationHP(ucc));
		
		CityBuilding kopalnia�elaza = ucc.getBuilding(CityBuilding.class, 101);
		fh.setLevel(kopalnia�elaza, 1);
		
		assertEquals(150, fortyfikacjaModel.getFortyficationHP(ucc));
		
	}
	
	/*
	 * Test repair rate fortyfikacji.
	 * Id fortyfikacji : 510
	 */
	@Test
	public void getRepairRate_simple_example()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		
		IFortyficationModel fortyfikacjaModel = westEurope.getById(510, IFortyficationModel.class);
		
		assertTrue( ucc.getLevel(510) == 0 );
		CityBuilding fortyfikacja = ucc.getBuilding(CityBuilding.class, 510);
		fh.setLevel(fortyfikacja, 1);
	
		assertEquals(10 , fortyfikacjaModel.getRepairRate(ucc) );
		
	}
	
	/*
	 * Ulepszenie repair rate. Ka�dy level tartaku zwi�ksz repair rate o 20%
	 * Id fortyfikacji : 510
	 */
	@Test
	public void getRepairRate_boosted_repair_rate()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		
		IFortyficationModel fortyfikacjaModel = westEurope.getById(510, IFortyficationModel.class);
		
		assertTrue( ucc.getLevel(510) == 0 );
		CityBuilding fortyfikacja = ucc.getBuilding(CityBuilding.class, 510);
		fh.setLevel(fortyfikacja, 1);
	
		assertEquals(10 , fortyfikacjaModel.getRepairRate(ucc) );
		
		CityBuilding tartak = ucc.getBuilding(CityBuilding.class, 100);
		fh.setLevel(tartak, 1);
		assertEquals(12 , fortyfikacjaModel.getRepairRate(ucc) );
		
		
	}
	
	
}
