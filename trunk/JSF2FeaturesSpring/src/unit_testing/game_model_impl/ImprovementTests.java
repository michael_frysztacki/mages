package unit_testing.game_model_impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import entities.Capital;
import entities.Civil;
import entities.Colony;
import entities.Colony.TakeOverExtractBuildingRandomizator;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UserResearch;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.buildings.FreeHand;
import entities.stocks.StockChanger;
import entities.units.TripArmy;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.Civilization;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IImprovementModel;

import java.util.List;


import org.junit.Before;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_Korea_City;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;

/*
 * klasa testuje ulepszenia dla budynk�w i bada�. Ulepszenia mog� by� r�norakie np.
 * ulepszenia dla budynk�w wydbywczych,
 * ulepszenia dla wszystkich bada� po wzgl�dem czasu wykonania(np. technologia "druk" u europejczyk�w.
 * Testuje si� r�wnie�, czy po przej�ciu budynk�w obcej cywilizacji, nasze ulepszenia r�wnie� b�d� tam dzia�a�.
 * Np. czy badanie druk u europejczyk�w b�dzie mia�o wp�yw na czas badania w obcym budynku.
 */
public class ImprovementTests {

	WorldDefinition ch;
	UsersCityConfig ucc;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	public static class TestRandomizator extends TakeOverExtractBuildingRandomizator{
		@Override
		public List<ExtractCityBuilding> choseExtractBuildingsToTakeOver(List<ExtractCityBuilding> allBuildings, WorldDefinition wd){
			return allBuildings;
		}
	}
	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException
	{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea(), new Config_Korea_City(), new Config_WestEurope_City());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
	}
	
	/*
	 * Zdefioniowano badanie druk, kt�rego kazdy level skraca czas wszystkich bada� o po�ow�.
	 * badanie druk - id 2004,
	 * badanie napraw kana�y - id 300
	 * badanie zwi�ksza produkcji browara - id 2002.
	 * 
	 * uniwersytet - id 500
	 * dostawca wody - id 3
	 * budynek browar - id 2000
	 * 
	 * Bo ustawieniu badania druk na 1 lvl, wszystkie badania, razem z drukiem, powinny mie� skr�cony czas budowania.
	 * UWAGA. Czas ulepszenia badania "druk" b�dzie na 0 i 1 level taki sam.
	 * Normalnie czas powinien by� 10, 20, 30 ,40 dla kolejnych level badania druk.
	 * Poniewa� druk dzia�a r�wnie� sam na siebie, czasy b�d� nast�puj�ce :
	 * 10, 10, 15, 20 - na ka�dym etapie czas zosta� skr�cony o po�ow�, opr�cz pierwszego etapu, bo badanie druk jest na 0 poziomie.
	 *
	 */
	@Test
	public void test_badanie_druk()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		IImprovementModel drukModel = westEurope.getById(2004, IImprovementModel.class);
		IImprovementModel naprawKanalyModel = westEurope.getById(300, IImprovementModel.class);
		IImprovementModel prodBrowaraModel = westEurope.getById(2002, IImprovementModel.class);
		
		CityBuilding uniwersytet = ucc.getBuilding(CityBuilding.class, 500);
		CityBuilding browar = ucc.getBuilding(CityBuilding.class, 2000);
		CityBuilding dostawcaWody = ucc.getBuilding(CityBuilding.class, 3);
		
		fh.setLevel(uniwersytet, 1);
		fh.setLevel(browar, 1);
		fh.setLevel(dostawcaWody, 1);
		
		assertTrue(drukModel.getBuildTime(ucc) == 10);
		assertTrue(naprawKanalyModel.getBuildTime(ucc) == 10);
		assertTrue(prodBrowaraModel.getBuildTime(ucc) == 10);
		
		UserResearch druk = uc.getResearch(2004);
		fhr.setResearchLvl(druk, 1);
		
		assertTrue(drukModel.getBuildTime(ucc) == 10); 
		assertTrue(naprawKanalyModel.getBuildTime(ucc) == 5);
		assertTrue(prodBrowaraModel.getBuildTime(ucc) == 5);
		
		
	}
	
	/*
	 * Zdefioniowano badanie druk, kt�rego kazdy level skraca czas wszystkich bada� o po�ow�.
	 * badanie druk - id 2004,
	 * badanie napraw kana�y - id 300
	 * badanie zwi�ksza produkcji browara - id 2002.
	 * 
	 * uniwersytet - id 500
	 * dostawca wody - id 3
	 * budynek browar - id 2000
	 * 
	 * Test sprawdza, czy badanie druk b�dzie dzia�a�o w obcych badaniach po przej�ciu obcego budynku (nie z naszej cywilziacji )
	 * Badanie druk r�wnie� powinno tam dzia�a�. U�ytkownik ma ustawion� cywilziacj� Europejsk� i zak�adamy, �e przej�� miasto korea�skie.
	 * Sprawdzamy czy badanie z sul house b�dzie mia�o skr�cony czas.
	 */
	@Test
	public void test_badanie_druk_w_obcej_cywilizacji()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UserConfig uc_korea = new UserConfig("galan", "pass", ch  , Civil.city_in_korea, true, null, Language.PL);
		uc_korea.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc_korea, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		TripArmy winner = mock(TripArmy.class);
		when(winner.getOwner()).thenReturn(uc);
		ucc.takeOverColony(winner, new TestRandomizator() );

		fh.setLevel(ucc.getBuilding(CityBuilding.class, 8), 1);
		
		Civilization koreaCity = ch.getCivilization(Civil.city_in_korea);
		IImprovementModel ulepszenieWSulHouse = koreaCity.getById(10, IImprovementModel.class);
		
		assertTrue(ulepszenieWSulHouse.getBuildTime(ucc) == 10); 
		
		UserResearch druk = uc.getResearch(2004);
		fhr.setResearchLvl(druk, 1);
		
		assertTrue(ulepszenieWSulHouse.getBuildTime(ucc) == 5); // normalnie powinno by� 10. 
		
		
	}
	
	/*
	 * Sprawdzenie, czy badanie druk dzia�a w cywilizacjach rozszerzaj�cych cywilizacj� europejsk�.
	 * badanie druk - id 2004,
	 * badanie napraw kana�y - id 300
	 * badanie zwi�ksza produkcji browara - id 2002.
	 *
	 */
	@Test
	public void test_badanie_druk_in_extending_civil()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.Malbork_WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Capital ucc = new Capital(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		Civilization malbork = ch.getCivilization(Civil.Malbork_WestEurope);
		IImprovementModel kilofy = malbork.getById(3000, IImprovementModel.class);
		
		CityBuilding uniwersytet = ucc.getBuilding(CityBuilding.class, 500);
		fh.setLevel(uniwersytet, 1);
		
		assertTrue(kilofy.getBuildTime(ucc) == 10);
		
		UserResearch druk = uc.getResearch(2004);
		fhr.setResearchLvl(druk, 1);
		
		assertTrue(kilofy.getBuildTime(ucc) == 5); 
		
	}
	/*
	 * W tym te�cie sprawdzamy ulepszanie jednego obiektu ( new DecoratorPattern(id) )
	 * w cywilizacji europejskie zdefiniowano badanie napraw kanaly, kt�re zwi�ksza produkcje u dostawcy wody o 20%.
	 * Budynek dostawca wody zdefiniowany jest w cywilizacji og�lnej.
	 */
	@Test
	public void test_improved_object_in_extended_civil()
	{
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope , true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Capital ucc = new Capital(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		stockCh.freeHand(ucc.getCityStocks().getPeopleStock(), 100.0d);
		
		Civilization malbork = ch.getCivilization(Civil.WestEurope);
		IExtractBuildingModel m_waterProvider = malbork.getById(3, IExtractBuildingModel.class);
		
		assertTrue(m_waterProvider.getMaxPeople(ucc) == 0); // mamy 0 level dostawcy wody, wiec wydobycie musi byc 0
		assertTrue(m_waterProvider.getOutputPerHour(ucc) == 0); // to samo z wydobyciem
		
		ExtractCityBuilding waterProvider = ucc.getBuilding(ExtractCityBuilding.class, 3);
		fh.setLevel(waterProvider, 1);
		assertTrue(waterProvider.setPeople(100));
		assertTrue(m_waterProvider.getOutputPerHour(ucc) == 500);
		
		UserResearch naprawKanaly = uc.getResearch(300);
		fhr.setResearchLvl(naprawKanaly, 1);
		assertTrue(m_waterProvider.getOutputPerHour(ucc) == 600);
		
	}
	
}
