package unit_testing.game_model_impl;


import game_model_impl.BuildModelException;
import game_model_impl.WorldDefinition;

import org.junit.Before;
import org.junit.Test;

import unit_testing.world_definition_build_model_test.CommonCivilRepeatedId;
import unit_testing.world_definition_build_model_test.ExtendedCivilRepeatedIds;
import unit_testing.world_definition_build_model_test.HardCodedCommonParameters;

import com.jsfsample.managedbeans.SpringJSFUtil;

/*
 * klasa testuje, czy wyst�puj� b��dy przy budowaniu cywilizacji niepoprawnie zdefiniowanymi ICivilizationConfigProvider
 * TODO -kiedy� trzeba to zrobi�. Validacja poprawno�ci , patrz klasa WorldDefinition.CivilizationConfigValidator
 */  
 
public class BuildModelExceptionTest {

	WorldDefinition ch;
	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException
	{
		
	}
	
	
	@Test(expected=BuildModelException.class)
	public void are_ids_repeated() throws BuildModelException
	{
		new WorldDefinition(0,new CommonCivilRepeatedId(),new HardCodedCommonParameters(), new ExtendedCivilRepeatedIds());
		
	}

	
}
