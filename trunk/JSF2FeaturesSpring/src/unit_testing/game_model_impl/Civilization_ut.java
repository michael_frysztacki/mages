package unit_testing.game_model_impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;


import org.junit.Before;
import org.junit.Test;

import unit_testing.world_definition_for_civilization_class_testing.Config_Common;
import unit_testing.world_definition_for_civilization_class_testing.Config_Korea;
import unit_testing.world_definition_for_civilization_class_testing.Config_Korea_City;
import unit_testing.world_definition_for_civilization_class_testing.Config_NorthEurope;
import unit_testing.world_definition_for_civilization_class_testing.Config_WestEurope;
import unit_testing.world_definition_for_civilization_class_testing.Config_WestEurope_City;
import unit_testing.world_definition_for_civilization_class_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Civil;
import entities.FreeHandForReasearch;
import entities.UsersCityConfig;
import entities.buildings.FreeHand;
import entities.stocks.StockChanger;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.Civilization;
import game_model_impl.WorldDefinition;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IGameModel;
import game_model_interfaces.ILandUnitModel;
import game_model_interfaces.IResearchModel;
import game_model_interfaces.IUnitModel;



/*
 * klasa testuje CommonCivilization i ConcreteCivilization
 */
public class Civilization_ut {

	WorldDefinition ch;
	UsersCityConfig ucc;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	
	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException
	{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea(), new Config_Korea_City(), new Config_WestEurope_City(), new Config_NorthEurope());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
	}
	
	@Test
	public void getById_object_from_common_civilization()
	{
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		IBuildingModel uniwersytet = westEurope.getById(500, IBuildingModel.class);
		assertTrue(uniwersytet != null);	
	}
	
	@Test
	public void getById_object_from_westeurope_civilization()
	{
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		IGameModel impr = westEurope.getById(2002, IGameModel.class);
		assertTrue(impr != null);	
	}
	
	@Test
	public void getById_object_from_child_civilization()
	{
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		IGameModel unit = westEurope.getById(5000, IGameModel.class);
		assertTrue(unit == null);	
	}
	
	@Test
	public void getById_non_existing_object()
	{
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		IBuildingModel uniwersytet = westEurope.getById(10101010, IBuildingModel.class);
		assertTrue(uniwersytet == null);
	}
	
	@Test
	public void getChildCivilizations_west_europe()
	{
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		List<ICivilization> children = westEurope.getChildCivilizations();
		assertEquals(1, children.size() );
		assertEquals(Civil.Malbork_WestEurope, children.get(0).getCivilization() );
	}
	
	@Test
	public void getChildCivilizations_common()
	{
		Civilization common = ch.getCivilization(Civil.common);
		List<ICivilization> children = common.getChildCivilizations();
		assertEquals(3, children.size() );
		assertTrue( children.contains(ch.getCivilization(Civil.korea)) );
		assertTrue( children.contains(ch.getCivilization(Civil.NorthEurope)) );
		assertTrue( children.contains(ch.getCivilization(Civil.WestEurope)) );
	}
	
	@Test
	public void getChildCivilizations_korea_city()
	{
		Civilization koreaCity = ch.getCivilization(Civil.city_in_korea);
		List<ICivilization> children = koreaCity.getChildCivilizations();
		assertEquals(0, children.size() );
	}
	
	@Test
	public void getParentCivilizations_korea_city()
	{
		Civilization koreaCity = ch.getCivilization(Civil.city_in_korea);
		Civilization parent = koreaCity.getParentCivilization();
		assertEquals(Civil.korea, parent.getCivilization() );
	}
	
	@Test
	public void get_full_game_model_list()
	{
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		List<IGameModel> fullList = westEurope.getGameModelList(false);
		assertEquals( 24 , fullList.size() );
	}
	
	@Test
	public void get_civilization_game_model_list()
	{
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		List<IGameModel> civilList = westEurope.getGameModelList(true);
		assertEquals( 41 , civilList.size() );
	}
	
	@Test
	public void getGameModelList_west_europe_buildings_inherit_true()
	{
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		List<IBuildingModel> buildingList = westEurope.getGameModelList(IBuildingModel.class, true, false) ;
		assertEquals( 5 , buildingList.size() );
	}
	
	@Test
	public void getGameModelList_west_europe_buildings_inherit_false()
	{
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		List<IBuildingModel> buildingList = westEurope.getGameModelList(IBuildingModel.class, false, false) ;
		assertEquals( 1 , buildingList.size() );
	}
	
	/*
	 * jednostka o id 512 jest wy��czona z cywilizacji europejskiej.
	 */
	@Test
	public void getByid_excluded_unit()
	{
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		Civilization common = ch.getCivilization(Civil.common);
		assertTrue( common.getById(512, IGameModel.class) != null);
		assertTrue( westEurope.getById(512, IGameModel.class) == null);
	}
	
	/*
	 * jednostka o id 512 jest wy��czona z cywilizacji europejskiej.
	 * Sprawdzamy czy zmiania b�dzie r�wnie� widoczna w malborku.
	 */
	@Test
	public void getByid_excluded_unit_malbork()
	{
		Civilization westEurope = ch.getCivilization(Civil.Malbork_WestEurope);
		Civilization common = ch.getCivilization(Civil.common);
		assertTrue( common.getById(512, IGameModel.class) != null);
		assertTrue( westEurope.getById(512, IGameModel.class) == null);
	}
	
	/*
	 * jednostka o id 512 jest wy��czona z cywilizacji europejskiej.
	 * Jednostka powinna by� widoczna w cywilizacji korea�skiej.
	 */
	@Test
	public void getByid_excluded_unit_visible_in_korea()
	{
		Civilization westEurope = ch.getCivilization(Civil.korea);
		Civilization common = ch.getCivilization(Civil.common);
		assertTrue( common.getById(512, IGameModel.class) != null);
		assertTrue( westEurope.getById(512, IGameModel.class) != null);
	}
	
	/*
	 * 
	 * Testowanie metody getGameModelList , sprawdzamy jak zachowuje sie unit 512, kt�ry jest wy��czony w cywilziacji europejskiej
	 */
	@Test
	public void getGameModelList_full_list_excluded_unit()
	{
		Civilization malbork = ch.getCivilization(Civil.Malbork_WestEurope);
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		Civilization common = ch.getCivilization(Civil.common);
		Civilization korea = ch.getCivilization(Civil.korea);
		IUnitModel excludedUnit = common.getById(512, IUnitModel.class);
		assertTrue(common.getGameModelList(true).contains(excludedUnit) );
		assertTrue(korea.getGameModelList(true).contains(excludedUnit) );
		assertFalse(malbork.getGameModelList(true).contains(excludedUnit) );
		assertFalse( westEurope.getGameModelList(true).contains(excludedUnit) );
		
	}
	/*
	 * 
	 * Testowanie metody getGameModelList , sprawdzamy jak zachowuje sie unit 512, kt�ry jest wy��czony w cywilziacji europejskiej.
	 * U�ywamy metody GameModelList z argumentami ustalaj�cymi typ obiekt�w.
	 */
	@Test
	public void getGameModelList_specified_object_type()
	{
		Civilization malbork = ch.getCivilization(Civil.Malbork_WestEurope);
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		Civilization common = ch.getCivilization(Civil.common);
		Civilization korea = ch.getCivilization(Civil.korea);
		ILandUnitModel excludedUnit = common.getById(512, ILandUnitModel.class);
		assertTrue(common.getGameModelList(ILandUnitModel.class, false, true).contains(excludedUnit) );
		assertTrue(korea.getGameModelList(ILandUnitModel.class, false, true).contains(excludedUnit) );
		assertFalse(westEurope.getGameModelList(ILandUnitModel.class, false, true).contains(excludedUnit) );
		assertFalse(malbork.getGameModelList(ILandUnitModel.class, false, true).contains(excludedUnit) );
	}
	
	/*
	 * Testowanie metody getGameModelList , sprawdzamy jak zachowuje sie unit 512, kt�ry jest wy��czony w cywilziacji europejskiej.
	 * U�ywamy metody GameModelList pobieraj�c tylko budynki. Unit nie powinien sie znajdowa� w �adnej li�cie.
	 * Budynek 6 znajduje sie w cywilziacji common, budynek 2 w cywilizacji koreanskiej.
	 */
	@Test
	public void getGameModelList_getOnlybuildings()
	{
		Civilization malbork = ch.getCivilization(Civil.Malbork_WestEurope);
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		Civilization common = ch.getCivilization(Civil.common);
		Civilization korea = ch.getCivilization(Civil.korea);
		IUnitModel excludedUnit = common.getById(512, IUnitModel.class);
		IBuildingModel commonBuilding = common.getById(6, IBuildingModel.class);
		IBuildingModel koreaBuilding = korea.getById(2, IBuildingModel.class);
		assertTrue(commonBuilding != null);
		assertTrue(koreaBuilding != null);
		assertTrue(common.getGameModelList(IBuildingModel.class, false, true).contains(commonBuilding));
		assertTrue(common.getGameModelList(IBuildingModel.class, true, false).contains(commonBuilding) );
		assertFalse(common.getGameModelList(IUnitModel.class, true, false).contains(commonBuilding) );
		assertFalse(common.getGameModelList(IResearchModel.class, true, false).contains(commonBuilding) );
		assertFalse(common.getGameModelList(IExtractBuildingModel.class, true, false).contains(commonBuilding) );
		assertTrue(korea.getGameModelList(IBuildingModel.class, false, true).contains(commonBuilding) );
		assertFalse(common.getGameModelList(IBuildingModel.class, false, true).contains(excludedUnit) );
		assertFalse(common.getGameModelList(IBuildingModel.class, false, true).contains(excludedUnit) );
		assertFalse(korea.getGameModelList(IBuildingModel.class, false, true).contains(excludedUnit) );
		assertFalse(westEurope.getGameModelList(IBuildingModel.class, false, true).contains(excludedUnit) );
		assertFalse(malbork.getGameModelList(IBuildingModel.class, false, true).contains(excludedUnit) );
	}
	
	/*
	 * W cywilizacji common znajduje sie skomplikwoane drzewo wymaga� ( na samej gorze jest budynek id 517 ).
	 * Sprawdzamy , czy je�li w cywilizacji europejskiej wy�aczymy budynek 517, to czy wymagania-dzieci (budynki, unity, badania)
	 * r�wnie� zostan� usuni�te z cywilizacji
	 */
	@Test
	public void getById_excluded_building_with_children()
	{
		Civilization malbork = ch.getCivilization(Civil.Malbork_WestEurope);
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		Civilization common = ch.getCivilization(Civil.common);
		Civilization korea = ch.getCivilization(Civil.korea);
		
		assertTrue( common.getById(517, IGameModel.class) != null );
		assertTrue( common.getById(518, IGameModel.class) != null );
		assertTrue( common.getById(519, IGameModel.class) != null );
		assertTrue( common.getById(521, IGameModel.class) != null );
		assertTrue( common.getById(520, IGameModel.class) != null );
		assertTrue( common.getById(522, IGameModel.class) != null );
		
		assertTrue( korea.getById(517, IGameModel.class) != null );
		assertTrue( korea.getById(518, IGameModel.class) != null );
		assertTrue( korea.getById(519, IGameModel.class) != null );
		assertTrue( korea.getById(521, IGameModel.class) != null );
		assertTrue( korea.getById(520, IGameModel.class) != null );
		assertTrue( korea.getById(522, IGameModel.class) != null );
		
		assertTrue( westEurope.getById(517, IGameModel.class) == null );
		assertTrue( westEurope.getById(518, IGameModel.class) == null );
		assertTrue( westEurope.getById(519, IGameModel.class) == null );
		assertTrue( westEurope.getById(521, IGameModel.class) == null );
		assertTrue( westEurope.getById(520, IGameModel.class) == null );
		assertTrue( westEurope.getById(522, IGameModel.class) == null );
		
		assertTrue( malbork.getById(517, IGameModel.class) == null );
		assertTrue( malbork.getById(518, IGameModel.class) == null );
		assertTrue( malbork.getById(519, IGameModel.class) == null );
		assertTrue( malbork.getById(521, IGameModel.class) == null );
		assertTrue( malbork.getById(520, IGameModel.class) == null );
		assertTrue( malbork.getById(522, IGameModel.class) == null );
		
	}
	
	/*
	 * W cywilizacji common znajduje sie skomplikwoane drzewo wymaga� ( na samej gorze jest budynek id 517 ).
	 * Sprawdzamy , czy je�li w cywilizacji europejskiej wy�aczymy budynek 517, to czy wymagania-dzieci (budynki, unity, badania)
	 * r�wnie� zostan� usuni�te z cywilizacji
	 */
	@Test
	public void getGameModelList_excluded_building_with_children()
	{
		Civilization malbork = ch.getCivilization(Civil.Malbork_WestEurope);
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		Civilization common = ch.getCivilization(Civil.common);
		Civilization korea = ch.getCivilization(Civil.korea);
		
		IGameModel europeModel = westEurope.getById(300, IGameModel.class);
		IGameModel europeModel2 = westEurope.getById(2500, IGameModel.class);
		
		IGameModel topBuilding = common.getById(517, IGameModel.class);
		IGameModel improvement1 = common.getById(518, IGameModel.class);
		IGameModel improvement2 = common.getById(519, IGameModel.class);
		IGameModel improvement3 = common.getById(519, IGameModel.class);
		IGameModel bottomBuilding = common.getById(521, IGameModel.class);
		IGameModel bottomUnit = common.getById(522, IGameModel.class);
		
		assertTrue(common.getGameModelList(IGameModel.class, true, true).contains(topBuilding));
		assertTrue(common.getGameModelList(IGameModel.class, true, true).contains(improvement1));
		assertTrue(common.getGameModelList(IGameModel.class, true, true).contains(improvement2));
		assertTrue(common.getGameModelList(IGameModel.class, true, true).contains(improvement3));
		assertTrue(common.getGameModelList(IGameModel.class, true, true).contains(bottomBuilding));
		assertTrue(common.getGameModelList(IGameModel.class, true, true).contains(bottomUnit));
		
		assertTrue(common.getGameModelList(IGameModel.class, true, false).contains(topBuilding));
		assertTrue(common.getGameModelList(IGameModel.class, true, false).contains(improvement1));
		assertTrue(common.getGameModelList(IGameModel.class, true, false).contains(improvement2));
		assertTrue(common.getGameModelList(IGameModel.class, true, false).contains(improvement3));
		assertTrue(common.getGameModelList(IGameModel.class, true, false).contains(bottomBuilding));
		assertTrue(common.getGameModelList(IGameModel.class, true, false).contains(bottomUnit));
		
		assertFalse(westEurope.getGameModelList(IGameModel.class, true, true).contains(topBuilding));
		assertFalse(westEurope.getGameModelList(IGameModel.class, true, true).contains(improvement1));
		assertFalse(westEurope.getGameModelList(IGameModel.class, true, true).contains(improvement2));
		assertFalse(westEurope.getGameModelList(IGameModel.class, true, true).contains(improvement3));
		assertFalse(westEurope.getGameModelList(IGameModel.class, true, true).contains(bottomBuilding));
		assertFalse(westEurope.getGameModelList(IGameModel.class, true, true).contains(bottomUnit));
		
		assertTrue(westEurope.getGameModelList(IGameModel.class, true, true).contains(europeModel));
		assertTrue(westEurope.getGameModelList(IGameModel.class, true, true).contains(europeModel2));
		
		assertFalse(westEurope.getGameModelList(IGameModel.class, true, false).contains(topBuilding));
		assertFalse(westEurope.getGameModelList(IGameModel.class, true, false).contains(improvement1));
		assertFalse(westEurope.getGameModelList(IGameModel.class, true, false).contains(improvement2));
		assertFalse(westEurope.getGameModelList(IGameModel.class, true, false).contains(improvement3));
		assertFalse(westEurope.getGameModelList(IGameModel.class, true, false).contains(bottomBuilding));
		assertFalse(westEurope.getGameModelList(IGameModel.class, true, false).contains(bottomUnit));
		
		assertFalse(malbork.getGameModelList(IGameModel.class, true, true).contains(topBuilding));
		assertFalse(malbork.getGameModelList(IGameModel.class, true, true).contains(improvement1));
		assertFalse(malbork.getGameModelList(IGameModel.class, true, true).contains(improvement2));
		assertFalse(malbork.getGameModelList(IGameModel.class, true, true).contains(improvement3));
		assertFalse(malbork.getGameModelList(IGameModel.class, true, true).contains(bottomBuilding));
		assertFalse(malbork.getGameModelList(IGameModel.class, true, true).contains(bottomUnit));
		
		assertFalse(malbork.getGameModelList(IGameModel.class, true, false).contains(topBuilding));
		assertFalse(malbork.getGameModelList(IGameModel.class, true, false).contains(improvement1));
		assertFalse(malbork.getGameModelList(IGameModel.class, true, false).contains(improvement2));
		assertFalse(malbork.getGameModelList(IGameModel.class, true, false).contains(improvement3));
		assertFalse(malbork.getGameModelList(IGameModel.class, true, false).contains(bottomBuilding));
		assertFalse(malbork.getGameModelList(IGameModel.class, true, false).contains(bottomUnit));
		
		assertTrue(korea.getGameModelList(IGameModel.class, true, true).contains(topBuilding));
		assertTrue(korea.getGameModelList(IGameModel.class, true, true).contains(improvement1));
		assertTrue(korea.getGameModelList(IGameModel.class, true, true).contains(improvement2));
		assertTrue(korea.getGameModelList(IGameModel.class, true, true).contains(improvement3));
		assertTrue(korea.getGameModelList(IGameModel.class, true, true).contains(bottomBuilding));
		assertTrue(korea.getGameModelList(IGameModel.class, true, true).contains(bottomUnit));
		
		assertFalse(korea.getGameModelList(IGameModel.class, true, false).contains(topBuilding));
		assertFalse(korea.getGameModelList(IGameModel.class, true, false).contains(improvement1));
		assertFalse(korea.getGameModelList(IGameModel.class, true, false).contains(improvement2));
		assertFalse(korea.getGameModelList(IGameModel.class, true, false).contains(improvement3));
		assertFalse(korea.getGameModelList(IGameModel.class, true, false).contains(bottomBuilding));
		assertFalse(korea.getGameModelList(IGameModel.class, true, false).contains(bottomUnit));
		
		assertTrue(korea.getGameModelList(IGameModel.class, true, true).contains(topBuilding));
		assertTrue(korea.getGameModelList(IGameModel.class, true, true).contains(improvement1));
		assertTrue(korea.getGameModelList(IGameModel.class, true, true).contains(improvement2));
		assertTrue(korea.getGameModelList(IGameModel.class, true, true).contains(improvement3));
		assertTrue(korea.getGameModelList(IGameModel.class, true, true).contains(bottomBuilding));
		assertTrue(korea.getGameModelList(IGameModel.class, true, true).contains(bottomUnit));
		
	}
	
	/*
	 * W cywilizacji malbork wy��czyli�my dodatkowo budynek id 2513, kt�ry znajduje sie w europie.
	 * Nie powinnismy miec tego budynku, ani obiekt�w wy��czonych w europe.
	 */
	@Test
	public void getGameModelList_excluded_building_in_malbork()
	{
		Civilization malbork = ch.getCivilization(Civil.Malbork_WestEurope);
		Civilization westEurope = ch.getCivilization(Civil.WestEurope);
		Civilization common = ch.getCivilization(Civil.common);
		Civilization korea = ch.getCivilization(Civil.korea);
		
		IGameModel excludedInMalbork = westEurope.getById(2513, IGameModel.class);
		IGameModel excludedInEurope = common.getById(517, IGameModel.class);
		IGameModel excludedInheritlyInEurope2 = common.getById(512, IGameModel.class);
		IGameModel excludedInheritlyInEurope3 = common.getById(520, IGameModel.class);
		
		assertTrue(excludedInMalbork != null);
		assertTrue(excludedInEurope != null);
		assertFalse(malbork.getGameModelList(IGameModel.class, true, true).contains(excludedInMalbork));
		assertFalse(malbork.getGameModelList(IGameModel.class, true, true).contains(excludedInEurope));
		assertFalse(malbork.getGameModelList(IGameModel.class, true, true).contains(excludedInheritlyInEurope2));
		assertFalse(malbork.getGameModelList(IGameModel.class, true, true).contains(excludedInheritlyInEurope3));
		assertTrue(westEurope.getGameModelList(IGameModel.class, true, true).contains(excludedInMalbork));
		
	}
	
}
