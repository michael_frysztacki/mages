package unit_testing.game_model_impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_NorthEurope;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Colony.TakeOverExtractBuildingRandomizator;
import entities.FreeHandForReasearch;
import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;
import entities.buildings.FreeHand;
import entities.stocks.StockChanger;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.WorldDefinition;

public class BuildingTests {

	public static class TestRandomizator extends TakeOverExtractBuildingRandomizator{
		@Override
		public List<ExtractCityBuilding> choseExtractBuildingsToTakeOver(List<ExtractCityBuilding> allBuildings, WorldDefinition wd){
			return allBuildings;
		}
	}
	WorldDefinition ch;
	UsersCityConfig ucc;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	
	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea(), new Config_NorthEurope(), new Config_WestEurope_City());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
	}
	
}
