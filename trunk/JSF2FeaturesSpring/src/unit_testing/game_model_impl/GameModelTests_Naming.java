package unit_testing.game_model_impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.Civilization;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IResearchModel;
import game_model_interfaces.IUnitModel;
import game_model_interfaces.civilization_defining_interfaces.ICivilizationConfigProvider;
import game_model_interfaces.initializations.ICityInitializator;

import org.junit.Before;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_Korea_City;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.PlayerInitializatorForTestCastleAgesName;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.TestCityInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Civil;
import entities.Colony;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UserResearch;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.FreeHand;


/*
 * Testy testuj� defioniowanie nazwenictwa dla r�nych obiekt�w w r�nych cywilizacjawd.
 * Testowane s� g�ownie NameBoostery, ich kolejno�� do��czania, oraz czy kolejno�� do��czania NameBooster�w wp�ywa
 * na wynik IGameModel.getName(). R�na kolejno�� budowania cywilziacji nie powinna mie� wp�ywu na metod� getName w IGameModel.
 * Kolejno�� do��czania NameBooster�w zale�y od kolejno�ci budowania cywilizacji, dlatego w testach b�dziemy do��czali je w r�nych kolejno�ciach.
 * 
 * Ka�dy test jest wykonywany w p�tli, dla r�nych kolejno�ci budowania cywilizacji.
 * Na IGameModelu z pomoc� metody IGameModel.printDecoratorChain(), mo�na obserwowa� zmian� kolejno�ci booster�w.
 * 
 * 1 kolejno��:
 * 
 * new HardCodedKoreaTestConfig(),
   new CityInKoreaConfig(),
   new EuropaZachodniaConfig()
   
   2 kolejno��:
   
   new HardCodedKoreaTestConfig(),
   new EuropaZachodniaConfig(),
   new CityInKoreaConfig()
  
   3 kolejno��:
   
   new CityInKoreaConfig(),
   new HardCodedKoreaTestConfig(),
   new EuropaZachodniaConfig()
   
   4 kolejno��:
   
   new CityInKoreaConfig(),
   new EuropaZachodniaConfig(),
   new HardCodedKoreaTestConfig()
   
   
 * 
 * 
 * Testy nie testuj� poprawno�ci definiowania nazewnictwa, tzn. je�li zdefiniujemy w spos�b nieprawid�owy nazewnictwo w pliku
 * konfiguracyjnym (ICivilConfigProvider), wynik b�dzie niezdefiniowany.
 * Regu�y tworzenia nazwenictwa s� opisane w komentarzu w pliku
 * Text[] ICivilizationConfigProvider.getNames().
 */

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext-test.xml")
public class GameModelTests_Naming {

	UsersCityConfig ucc;
	ICivilizationConfigProvider[][] extendedCivils;
	private FreeHand fh = new FreeHand();
	private FreeHandForReasearch fhr = new FreeHandForReasearch();
	TestCityInitializator initializator = new TestCityInitializator();
	@Before
	public void setUp() throws BuildModelException
	{
		System.out.print("before build");
		
		extendedCivils = new ICivilizationConfigProvider[4][];
		
		extendedCivils[0] = new ICivilizationConfigProvider[]{
				new Config_Korea(),
				new Config_Korea_City(),
				new Config_WestEurope(),
				new Config_WestEurope_City()
		};
		
		extendedCivils[1] = new ICivilizationConfigProvider[]{
				new Config_Korea(),
				   new Config_WestEurope(),
				   new Config_WestEurope_City(),
				   new Config_Korea_City()
		};
		
		extendedCivils[2] = new ICivilizationConfigProvider[]{
				 new Config_Korea_City(),
				   new Config_Korea(),
				   new Config_WestEurope_City(),
				   new Config_WestEurope()
		};
		
		extendedCivils[3] = new ICivilizationConfigProvider[]{
				new Config_Korea_City(),
				new Config_WestEurope_City(),
				   new Config_WestEurope(),
				   new Config_Korea()
		};
		
		
		
		
		
		
	}
	/* 
	 * testowanie j�zyk�w. dla polskiego powinno zwr�ci� Wieki ciemne.
	 * 
	 * INFO IMPLEMENTACYJNE:
	 * W GameModelu ustawiony jest ImprovemtnTextBunwd.
	 * GameModel(improvement text bunch).
	 */
	@Test
	public void test3_language_GR1() throws BuildModelException
	{
		for(int i=0; i<extendedCivils.length ; i++)
		{
			SpringJSFUtil su = mock(SpringJSFUtil.class);
			WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
					extendedCivils[i]);
			AllWorlds aw = new AllWorlds(wd);
			when(su.getAllWorlds()).thenReturn(aw);
			
			wd = su.getAllWorlds().getWorldModel(0);
			
			UserConfig uc = new UserConfig("friko", "pass", wd , Civil.korea, true, null, Language.PL);
			uc.setSpringJSFUtil(su);
			
			UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
			IResearchModel b = wd.getCivilization(Civil.common).getById(0, IResearchModel.class);
			assertEquals("Wieki Ciemne", b.getName(ucc) );
		}
		
	}
	
	/*
	 * W tym te�cie testowany jest tylko zmieniony j�zyk(jest taki sam jak wy�ej), wi�c nie ma sensu tworzy� na nowo wszystkich kolejno�ci.
	 * testowanie j�zyk�w. dla angielskiego powinno zwr�ci� DarkAges.
	 * 
	 * INFO IMPLEMENTACYJNE:
	 * W GameModelu ustawiony jest ImprovemtnTextBunwd.
	 * GameModel(improvement text bunch).
	 */
	@Test
	public void test_language_ENG() throws BuildModelException
	{
		for(int i=0; i<extendedCivils.length ; i++)
		{
			SpringJSFUtil su = mock(SpringJSFUtil.class);
			WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
					extendedCivils[i]);
			AllWorlds aw = new AllWorlds(wd);
			when(su.getAllWorlds()).thenReturn(aw);
			
			UserConfig uc = new UserConfig("friko", "pass", wd ,Civil.korea, true, null, Language.ENG);
			uc.setSpringJSFUtil(su);
			
			this.fhr.setResearchLvl(uc.getResearch(0), 1);
			
			TestCityInitializator initializator = new TestCityInitializator();
			UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
			IExtractBuildingModel b = wd.getCivilization(Civil.common).getById(200, IExtractBuildingModel.class);
			System.out.print("\n" + b.getRequirementNode().getParents().get(0).getParentNode().getMyGameModel().getName(ucc)+ "\n");
			assertTrue("Feudal Age" == b.getRequirementNode().getParents().get(0).getParentNode().getMyGameModel().getName(ucc) );
		}
	}
	/*
	 * GRUPA TEST�W: 2
	 * 
	 * Mamy drugi poziom tiera czyli Castle Ages. Nasze badanie ktore m�wi nam o poziomie naszego tiera powinno zwr�ci� "Castle Ages".
	 * Testowanie nazwenictwa gdy zmienia si� poziom tego samego obiektu.
	 * 
	 * INFO IMPLEMENTACYJNE:
	 * W obiekcie GameModel ustawiony jest ImprovementTextBunch, kt�ry sprawdza level tego samego obiektu.
	 */
	@Test
	public void test_CastleAgesName_GR2() throws BuildModelException
	{
		
		for(int i=0; i<extendedCivils.length ; i++)
		{
			SpringJSFUtil su = mock(SpringJSFUtil.class);
			WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
					extendedCivils[i]);
			AllWorlds aw = new AllWorlds(wd);
			when(su.getAllWorlds()).thenReturn(aw);
			
			UserConfig uc = new UserConfig("friko", "pass", wd ,Civil.korea, true, null, Language.ENG);
			uc.setSpringJSFUtil(su);
			
			this.fhr.setResearchLvl(uc.getResearch(0), 2);
			
			ICityInitializator initializator = new PlayerInitializatorForTestCastleAgesName();
			UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
			Civilization civil = wd.getCivilization(uc.getCivil());
			IResearchModel model = civil.getById(0, IResearchModel.class);
			assertTrue("Castle Age" == model.getName(ucc) );
		}
	}
	/*
	 * Testujemy budynek, kt�ry ma zmieni� swoj� nazw� po osi�gni�ci pewnego levelu.
	 * We�my dla przyk�adu budynek nazwany standardowo "mur". Po rozwini�ciu go na level "7", powinien zmieni� nazw� na "wielki mur chinski
	 * mur jest budynkiem korei, a nie wsp�lnym.
	 * 
	 * INFO IMPLEMENTACYJNE:
	 * W GameModelu ustawiony jest ImprovementTextBunwd.
	 */
	@Test
	public void test_greatChinaWallName() throws BuildModelException
	{
		for(int i=0; i<extendedCivils.length ; i++)
		{
			SpringJSFUtil su = mock(SpringJSFUtil.class);
			WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
					extendedCivils[i]);
			AllWorlds aw = new AllWorlds(wd);
			when(su.getAllWorlds()).thenReturn(aw);
			
			UserConfig uc = new UserConfig("friko", "pass", wd ,Civil.korea, true, null, Language.PL);
			uc.setSpringJSFUtil(su);
			
			Civilization civil = wd.getCivilization(uc.getCivil());
			//IInitializator init = new PlayerInitializatorForTestCastleAgesName();
			UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
			FreeHand fh = new FreeHand();
			CityBuilding wallB = ucc.getBuilding(CityBuilding.class, 2);
			fh.setLevel(wallB, 6);
			IBuildingModel wall = civil.getById(2, IBuildingModel.class);
			assertTrue(wall.getName(ucc) == "mur");
			fh.setLevel(wallB,7);
			assertTrue(wall.getName(ucc) == "wielki mur chinski");
			fh.setLevel(wallB,8);
			assertTrue(wall.getName(ucc) == "wielki mur chinski");
		}
		
	}
	/*
	 * Teraz zdefiniujemy sobie jeden budynek wsp�lny np. "dostawca wody" w cywilizacji common, a w korei
	 * b�dzie si� zawsze nazywa� "Kanaly wodne". Nazwa kana�y wodne w cywilizacji korea�skiej, b�dzie zawsze widnia�a
	 * jako "Kana�y wodne", niezale�nie od levelu budynku. Jest to nazwa zmieniona na sta�e specjalnie dla tej cywilizacji.
	 * 
	 * INFO IMPLEMENTACYJNE:
	 * Do GameModelu do��czany jest NameBooster, kt�ry ma w sobie RegularModelTextBunwd.
	 */
	@Test
	public void test_civilizationOverridesANameInCommonCivilization() throws BuildModelException
	{
		for(int i=0; i<extendedCivils.length ; i++)
		{
			SpringJSFUtil su = mock(SpringJSFUtil.class);
			WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
					extendedCivils[i]);
			AllWorlds aw = new AllWorlds(wd);
			when(su.getAllWorlds()).thenReturn(aw);
			
			UserConfig uc = new UserConfig("friko", "pass", wd ,Civil.korea, true, null, Language.PL);
			uc.setSpringJSFUtil(su);
			
			UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
			Civilization civil = wd.getCivilization(uc.getCivil());
			IExtractBuildingModel waterCannals = civil.getById(3, IExtractBuildingModel.class);
			assertTrue(waterCannals.getName(ucc) == "Kanaly wodne");
		}
		
	}
	/*
	 * zdefiniowane jest wsp�lne badanie "wieki ciemne". rozszerzanie cywilizacji : common <- korea <- city_in_korea
	 * w cywilizacji city_in korea redefiniujemy nazewnictwo dla tier�w:
	 * tier 1: "Wieki ciemne w miescie koreanskim" od 1 lvl
	 * tier 2: "Wieki zamk�w w miescie koreanskim" od 2 lvl
	 * 
	 * INFO IMPLEMENTACYJNE:
	 * GameModel(ImprovementTextBunch) <- NameBooster(city_in_korea, improvementTextBunch)
	 */
	@Test
	public void test_civilizationOverridesANameInCommonCivilizationByLevel() throws BuildModelException
	{
		for(int i=0; i<extendedCivils.length ; i++)
		{
			SpringJSFUtil su = mock(SpringJSFUtil.class);
			WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
					extendedCivils[i]);
			AllWorlds aw = new AllWorlds(wd);
			when(su.getAllWorlds()).thenReturn(aw);
			
			UserConfig uc = new UserConfig("friko", "pass", wd,  Civil.city_in_korea, true, null, Language.PL);
			uc.setSpringJSFUtil(su);
			
			UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
			Civilization civil = wd.getCivilization(uc.getCivil());
			FreeHandForReasearch fhr = new FreeHandForReasearch();
			IResearchModel model = civil.getById(0, IResearchModel.class);
			UserResearch ur = uc.getResearch(0);
			fhr.setResearchLvl(ur, 1);
			assertTrue(model.getName(ucc) == "Wieki ciemne w miescie koreanskim");
		}
		
	}
	
	/*
	 * Standardowo mamy zdefioniowana jednostk�(id 4) "Koreanski wlocznik" w cywilizacji korea.
	 * Mamy te� badanie(id 5) "ulepszony koreanski wlocznik", kt�re zmieni nazw� w��cznika na
	 * "Koreanski pikinier"
	 * 
	 * INFO IMPLEMENTACYJNE:
	 * GameModel(RegularModelTextBunch) <- NameBooster(ImprovementModelTextBunch)
	 */
	@Test
	public void test_koreanskiPikinier() throws BuildModelException
	{
		for(int i=0; i<extendedCivils.length ; i++)
		{
			SpringJSFUtil su = mock(SpringJSFUtil.class);
			WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
					extendedCivils[i]);
			AllWorlds aw = new AllWorlds(wd);
			when(su.getAllWorlds()).thenReturn(aw);
			
			UserConfig uc = new UserConfig("friko", "pass", wd ,Civil.korea, true, null, Language.PL);
			uc.setSpringJSFUtil(su);
			
			UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
			Civilization civil = wd.getCivilization(uc.getCivil());
			FreeHandForReasearch fhr = new FreeHandForReasearch();
			IUnitModel wlocznik = civil.getById(4, IUnitModel.class);
			assertTrue(wlocznik.getName(ucc) == "Koreanski wlocznik");
			UserResearch ur = uc.getResearch(5);
			fhr.setResearchLvl(ur, 1);
			assertTrue(wlocznik.getName(ucc) == "Koreanski pikinier");
		}
	}
	/*
	 * Mamy �a�cuch trzech cywilizacji common <- korea <- city_in_korea.
	 * redefiniujemy nazw� budynku market place na ka�dej cywilizacji:
	 * Market place <- korea market place <- city korea market place.
	 * Gracz jako cywilizacj� ma ustawion� korea, czyli �rodkow� w powy�szym �a�cuchu.
	 * nazwa market place powinna wyswietlic sie jako "korea market place"
	 * 
	 * INFO IMPLEMENTACYJNE:
	 * test ten ma sprawdzi�, jak zachowuj� sie po��czone ze sob� NameBoostery.
	 * najbardziej zwen�trzny name booster("city korea market place") powinien zosta� pomini�ty.
	 * Nazwa powinna zosta� wy�uskana z boostera dla "korea"
	 * GameMode(RegularTextBunch) <- NameBooster(korea,RegularTextBunch) <- NameBooster(city_in_korea,ImprovementTextBunch) 
	 */
	@Test
	public void test_nameBoosterChainLeaveOut() throws BuildModelException
	{
		for(int i=0; i<extendedCivils.length ; i++)
		{
			SpringJSFUtil su = mock(SpringJSFUtil.class);
			WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
					extendedCivils[i]);
			AllWorlds aw = new AllWorlds(wd);
			when(su.getAllWorlds()).thenReturn(aw);
			
			UserConfig uc = new UserConfig("friko", "pass", wd ,Civil.korea, true, null, Language.PL);
			uc.setSpringJSFUtil(su);
			
			UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
			Civilization civil = wd.getCivilization(uc.getCivil());
			//FreeHandForReasearch fhr = new FreeHandForReasearch();
			IBuildingModel marketPlace = civil.getById(6, IBuildingModel.class);
			assertEquals(marketPlace.getName(ucc) , "korea market place");
		}
	}
	/*
	 * W tym te�cie stworzyli�my poboczny �a�cuch cywilizacyjny - Europa Zachodnia
	 * sprawdzamy jako zachowa si� �a�cuch name booster�w korea�skich z europejskim ( wszystkie name boostery pod��czone s�
	 * do tego samego GameModelu). Zdefiniowano now� nazw� "naprawione kanaly" dla budynku og�lnego "dostawca wody".
	 * nazwa naprawione kana�y b�dzie dost�pna dla gracza graj�cego europ� zachodni� dopiero wtedy gdy wykona badanie(id 300) "napraw kana�y"
	 * 
	 * INFO IMPLEMENTACYJNE:
	 * GameModel(RegularTextBunch) <- NameBooster(WestEurope, ImprovementTextBunch)
	 */
	@Test
	public void test_repairedCanals() throws BuildModelException
	{
		for(int i=0; i<extendedCivils.length ; i++)
		{
			SpringJSFUtil su = mock(SpringJSFUtil.class);
			WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
					extendedCivils[i]);
			AllWorlds aw = new AllWorlds(wd);
			when(su.getAllWorlds()).thenReturn(aw);
			
			UserConfig uc = new UserConfig("friko", "pass", wd, Civil.WestEurope, true, null, Language.PL);
			uc.setSpringJSFUtil(su);
			
			UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
			Civilization civil = wd.getCivilization(uc.getCivil());
			FreeHandForReasearch fhr = new FreeHandForReasearch();
			IBuildingModel dostawcaWody = civil.getById(3, IBuildingModel.class);
			assertTrue(dostawcaWody.getName(ucc) == "Dostawca wody");
			fhr.setResearchLvl(uc.getResearch(300), 1);
			assertTrue(dostawcaWody.getName(ucc) == "naprawione kanaly");
		}
	}
	/*
	 * ulepszenie "tier" (wieki ciemne, castle ages itd),zmienia swoj� nazw� w zale�no�ci od levelu tego badania.
	 * Badanie i nazewnictwo jest zdefiniowane w cywilizacji common. 
	 * W europie zdefiniujemy nazw� tego badania tak, aby zawsze nazywa�o sie tak samo.
	 * 
	 * INFO IMPLEMENTACYJNE:
	 * Przes�oniona jest zale�no�� levelowa(kt�ra znajduje sie w GameModel), zale�no�cia sta�� ( kt�ra b�dzie w name boosterze ).
	 * poziom tiera ustawiamy na 2, potem na 3 na nazwa tiera powinna zosta� niezmieniona w cywilizacji europa zachodnia.
	 * GameModel(ImprovementTextBunch) <- NameBooster(Europa, RegularTextBunch).
	 */
	@Test
	public void test_levelDependenceNameOverhiddenByConstantName() throws BuildModelException
	{
		for(int i=0; i<extendedCivils.length ; i++)
		{
			SpringJSFUtil su = mock(SpringJSFUtil.class);
			WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
					extendedCivils[i]);
			AllWorlds aw = new AllWorlds(wd);
			when(su.getAllWorlds()).thenReturn(aw);
			
			UserConfig uc = new UserConfig("friko", "pass", wd, Civil.WestEurope, true, null, Language.PL);
			uc.setSpringJSFUtil(su);
			
			UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
			Civilization civil = wd.getCivilization(uc.getCivil());
			FreeHandForReasearch fhr = new FreeHandForReasearch();
			IResearchModel tier = civil.getById(0, IResearchModel.class);
			assertTrue(tier.getName(ucc) == "krolestwo europejskie");
			fhr.setResearchLvl(uc.getResearch(0), 2);
			assertTrue(tier.getName(ucc) == "krolestwo europejskie");
			fhr.setResearchLvl(uc.getResearch(0), 3);
			assertTrue(tier.getName(ucc) == "krolestwo europejskie");
		}
	}
	
	/*
	 * Mamy �a�cuch trzech cywilizacji common <- korea <- city_in_korea.
	 * redefiniujemy nazw� budynku market place na ka�dej cywilizacji:
	 * Market place <- korea market place <- city korea market place.
	 * Dodatkowo redefiniujemy nazw� w cywilziacji europejskiej : "europe market place"
	 * Gracz jako cywilizacj� ma ustawion� europe.
	 * nazwa market place powinna wyswietlic sie jako "europe market place"
	 * 
	 * INFO IMPLEMENTACYJNE:
	 * test ten ma sprawdzi�, jak zachowuj� sie po��czone ze sob� NameBoostery.
	 * najbardziej zwen�trzny name booster("city korea market place") powinien zosta� pomini�ty.
	 * Nazwa powinna zosta� wy�uskana z boostera dla "korea"
	 * GameMode(RegularTextBunch), NameBooster(korea,RegularTextBunch), NameBooster(city_in_korea, RegularTextBunch),
	 * NameBooster(Europa, RegularTextBunch).
	 * Nie wiadomo w jakies kolejno�ci po��czone s� name boostery(to zale�y od kolejno�ci budowania).
	 */
	@Test
	public void test_nameBoosterChainLeaveOut_Europe() throws BuildModelException
	{
		for(int i=0; i<extendedCivils.length ; i++)
		{
			SpringJSFUtil su = mock(SpringJSFUtil.class);
			WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
					extendedCivils[i]);
			AllWorlds aw = new AllWorlds(wd);
			when(su.getAllWorlds()).thenReturn(aw);
			
			UserConfig uc = new UserConfig("friko", "pass", wd ,Civil.WestEurope, true, null, Language.PL);
			uc.setSpringJSFUtil(su);
			UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
			Civilization civil = wd.getCivilization(uc.getCivil());
			//FreeHandForReasearch fhr = new FreeHandForReasearch();
			IBuildingModel marketPlace = civil.getById(6, IBuildingModel.class);
			assertTrue(marketPlace.getName(ucc) == "europe market place");
			
		}
	}
	
	/*
	 * W cywilziacji og�lnej mamy zdefionowany budynek koszary.
	 * gdy rozbudujemy koszary na 4 level, zmienia on swoj� nazw� na "wielke koszary".
	 * 
	 * INFO IMPLEMENTACYJNE:
	 * ustawiono ImprovementTextBunch w GameModel.
	 */
	@Test
	public void test_common_civil_improvementTextBunch() throws BuildModelException
	{
		for(int i=0; i<extendedCivils.length ; i++){
			SpringJSFUtil su = mock(SpringJSFUtil.class);
			WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
					extendedCivils[i]);
			AllWorlds aw = new AllWorlds(wd);
			when(su.getAllWorlds()).thenReturn(aw);
			
			UserConfig uc = new UserConfig("friko", "pass", wd ,Civil.WestEurope, true, null, Language.PL);
			uc.setSpringJSFUtil(su);
			UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
			Civilization civil = wd.getCivilization(uc.getCivil());
			IBuildingModel koszary = civil.getById(7, IBuildingModel.class);
			assertTrue(koszary.getName(ucc) == "koszary");
			fh.setLevel(ucc.getBuilding(CityBuilding.class, 7), 4);
			assertTrue(koszary.getName(ucc) == "wielkie koszary");
			//System.out.print("\n" + koszary.printDecoratorChain() + "\n");
		}
	}
	
	@Test
	public void chainOfThreeCivilizations_eachWithDifferentNaming() throws BuildModelException{
		for(int i=0; i<extendedCivils.length ; i++){
			SpringJSFUtil su = mock(SpringJSFUtil.class);
			WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
					extendedCivils[i]);
			AllWorlds aw = new AllWorlds(wd);
			when(su.getAllWorlds()).thenReturn(aw);
			
			UserConfig uc = new UserConfig("friko", "pass", wd ,Civil.Malbork_WestEurope, true, null, Language.PL);
			uc.setSpringJSFUtil(su);
			UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
			IBuildingModel building = uc.getMyCivilization().getById(2700, IBuildingModel.class);
			assertEquals("building west europe city", building.getName(ucc));
			
		}
	}
	
	
	
}
