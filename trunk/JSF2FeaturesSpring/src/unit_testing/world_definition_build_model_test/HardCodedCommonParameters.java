package unit_testing.world_definition_build_model_test;

import game_model_interfaces.civilization_defining_interfaces.ICommonParameters;

public class HardCodedCommonParameters implements ICommonParameters{

	private final int waterSatisf = 2;
	private final int meatSatisf = 2;
	private final int carboSatisf = 2;
	private final int addMeatSatisf = 1;
	private final int addCarboSatisf = 1;
	private final int additionSatisf = 1;
	private final double peopleFallRate = -3600.0d;
	private final long satisfCooldownMs = 5000l;
	private final int firstCitizens = 10;
	private final int maxFightRounds = 6;
	private final int birthMultiplier = 1;
	private final int eatPerHour = 1;
	@Override
	public int getFurtherAdditionSatisfaction() {
		return additionSatisf;
	}

	@Override
	public int getFurtherCarboSatisfaction() {
		return addCarboSatisf;
	}

	@Override
	public int getFurtherMeatSatisfaction() {
		return addMeatSatisf;
	}

	@Override
	public int getMajorCarboSatisfaction() {
		return carboSatisf;
	}

	@Override
	public int getMajorMeatSatisfaction() {
		return meatSatisf;
	}

	@Override
	public int getWaterSatisfaction() {
		return waterSatisf;
	}

	@Override
	public int satisfactionCooldownSec() {
		return this.satisfCooldownMs;
	}

	@Override
	public int getFirstCitizensAmount() {
		return firstCitizens;
	}

	@Override
	public int getMaxFightRounds() {
		return this.maxFightRounds;
	}

	@Override
	public int getBirthMultiplier() {
		return birthMultiplier;
	}

	@Override
	public int getEatPerHour() {
		return eatPerHour;
	}

	@Override
	public int getMajorAdditionSatisfaction() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double chanceForTakingOverExtractBuilding() {
		// TODO Auto-generated method stub
		return 0;
	}

}
