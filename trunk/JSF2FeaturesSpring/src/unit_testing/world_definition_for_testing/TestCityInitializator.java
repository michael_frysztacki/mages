package unit_testing.world_definition_for_testing;


import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.buildings.FreeHand;
import entities.stocks.CityStockSet;
import entities.stocks.StockChanger;
import entities.units.UnitGroupChanger;
import game_model_impl.StockType;
import game_model_interfaces.initializations.ICityInitializator;


public class TestCityInitializator extends StockChanger implements ICityInitializator{

	@Override
	public UsersCityConfig initCity(UsersCityConfig ucc)
	{
		// TODO Auto-generated method stub
		//UsersCityConfig ucc = new UsersCityConfig(owner, isCapital, point, nsCityTime, this.ch);
		CityStockSet css = ucc.getCityStocks();
		
		this.freeHand(css.getPeopleStock(), 1000.0d);
		FreeHand fh = new FreeHand();
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(CityBuilding.class,203);//water
		fh.setLevel(ecb, 3);
		ecb.setPeople(200);
		ecb = ucc.getBuilding(ExtractCityBuilding.class,204);//rice
		fh.setLevel(ecb, 3);
		ecb.setPeople(200);
		
		CityBuilding wall = ucc.getBuilding(CityBuilding.class, 2);
		fh.setLevel(wall, 6);
		
		this.freeHand(css.getStockByType(StockType.water), 10.0d);
		this.freeHand(css.getStockByType(StockType.beef), 11.0d);
		this.freeHand(css.getStockByType(StockType.rice), 12.0d);
		
		this.freeHand(css.getStockByType(StockType.peopleCount), 1000.0d);
		this.freeHand(css.getStockByType(StockType.gold), 10000.0d);
		this.freeHand(css.getStockByType(StockType.iron), 10000.0d);
		this.freeHand(css.getStockByType(StockType.wood), 10000.0d);
		
		UnitGroupChanger changer = new UnitGroupChanger();
		
		changer.freeHand(ucc.getMyArmy().getUnitGroupById(10), 100.0d);
		fh.setLevel(ucc.getBuilding(CityBuilding.class,5),1);
		return ucc;
	}
	
}
