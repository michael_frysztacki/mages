package unit_testing.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;



import org.junit.Before;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;


import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Civil;
import entities.Colony;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.buildings.FreeHand;
import entities.buildings.BuildingBuild.BuildingCurrentlyUpgradingException;
import entities.stocks.StockChanger;
import entities.units.UnitGroupChanger;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import game_model_impl.PriceStockSetImpl.PayException;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IRequirementNode;
import game_model_interfaces.IRequirementNode.RequirementNotFullfilledException;
import game_model_interfaces.IUnitModel;

public class ut_CityBuilding{
	WorldDefinition ch;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	UnitGroupChanger unitChanger = new UnitGroupChanger();
	
	SpringJSFUtil su;
	UserConfig friko;
	UsersCityConfig ucc;
	public final static double delta = 0.00005d;
	@Before
	public void setUp() throws BuildModelException{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea() , new Config_WestEurope_City());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		ucc = new Colony(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
	}
	/*
	 * upgradujemy budynek.
	 * Mamy wystarcz�c� ilo�� surowc�w, spe�nione wymagania.
	 * potrzebne jest 200 drewna na upgrade z 1 na 2 level.
	 * W czasie upgrade mo�na wyci�ga�, wsadza� ludzi do budynku.
	 */
	@Test
	public void upgrade_success(){
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.wood),200.0d);
		stockCh.freeHand(ucc.getCityStocks().getPeopleStock(), 200.0d);
		ExtractCityBuilding stonePit = ucc.getBuilding(ExtractCityBuilding.class, 100);
		fh.setLevel(stonePit, 1);
		assertTrue(stonePit.setPeople(100));
		fh.setLevel(stonePit, 1);
		try {
			stonePit.upgrade();
			assertTrue(stonePit.setPeople(90));
			assertFalse(stonePit.setPeople(110));

		} catch (PayException e) {
			fail();
		} catch (BuildingCurrentlyUpgradingException e) {
			fail();
		} catch (IRequirementNode.RequirementNotFullfilledException e) {
			fail();
		}
		
		
	}
	/*
	 * potrzeba 100 drewna
	 */
	@Test
	public void upgrade_fail_not_enough_resources(){ 
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.wood),90.0d);
		ExtractCityBuilding stonePit = ucc.getBuilding(ExtractCityBuilding.class, 100);
		try {
			stonePit.upgrade();
			fail();
		} catch (PayException e) {
		} catch (BuildingCurrentlyUpgradingException e) {
			fail();
		} catch (IRequirementNode.RequirementNotFullfilledException e) {
			fail();
		}
		
	}
	
	/*
	 * budynek o id: 2514(west europe) potrzebuje tiera na level 1.
	 * koszt budynku :
	 * 	wood:100
	 */
	@Test
	public void upgrade_fail_requirements_not_fullfilled(){ 
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.wood),100.0d); // spe�niamy wymagania kosztowe
		assertEquals(0, uc.getResearch(0).getLevel());
		CityBuilding ironPit = ucc.getBuilding(CityBuilding.class, 2514);
		try {
			ironPit.upgrade();
			fail();
		} catch (PayException e) {
			fail();
		} catch (BuildingCurrentlyUpgradingException e) {
			fail();
		} catch (IRequirementNode.RequirementNotFullfilledException e) {
			
		}
		
	}
	
	/*
	 * Budynek si� ju� upgraduje, nie mo�emy na nim wykonwa� drugi raz operacji upgrade.
	 */
	@Test
	public void upgrade_fail_building_already_upgrading(){ 
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.wood),100.0d); // spe�niamy wymagania kosztowe
		ExtractCityBuilding stonePit = ucc.getBuilding(ExtractCityBuilding.class, 100);
		try {
			stonePit.upgrade();
			stonePit.upgrade();
			fail();
		} catch (PayException e) {
			fail();
		} catch (BuildingCurrentlyUpgradingException e) {
			
		} catch (IRequirementNode.RequirementNotFullfilledException e) {
			fail();
		}
		
	}
	
	/*
	 * Cena kusznika:
	 * wood: 50,
	 * free people: 1.
	 */
	@Test
	public void build_one_unit_in_building_if_all_is_ok(){ 
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		Colony ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		stockCh.freeHand(ucc.getCityStocks().getPeopleStock() , 1 );
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.wood),50.0d);
		CityBuilding marketPlace = ucc.getBuilding(ExtractCityBuilding.class, 6);
		fh.setLevel(marketPlace, 1);
		IUnitModel kusznik = uc.getMyCivilization().getById(2600, IUnitModel.class);
		assertEquals(1.0d, ucc.getCityStocks().getPeopleStock().available(), delta);
		assertEquals(50.0d, ucc.getCityStocks().getStockByType(StockType.wood).getVisibleAmount(), delta);
		assertEquals(0 , marketPlace.getObjectBuildQueue().getBuildsAmount());
		try {
			marketPlace.buildUnit(kusznik, 1);
		} catch (PayException e) {
			fail();
		}
		assertEquals(1, marketPlace.getObjectBuildQueue().getBuildsAmount());
		assertEquals(0, ucc.getCityStocks().getPeopleStock().getVisibleAmount(), delta);
		assertEquals(0, ucc.getCityStocks().getStockByType(StockType.wood).getVisibleAmount(), delta);
		
	}
	
	/*
	 * Cena kusznika:
	 * wood: 50,
	 * free people: 1.
	 */
	@Test
	public void buildThreeUnits_butHaveResourcesOnlyForOneUnit_onlyOneUnitShouldBeAddedToQueue(){ 
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		long currentTimeNs = System.currentTimeMillis()*1000*1000;
		Colony ucc = new Colony(uc, new Point(new PointInitializator()), currentTimeNs);
		stockCh.freeHand(ucc.getCityStocks().getPeopleStock() , 1 );
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.wood),50.0d);
		CityBuilding marketPlace = ucc.getBuilding(ExtractCityBuilding.class, 6);
		fh.setLevel(marketPlace, 1);
		IUnitModel kusznik = uc.getMyCivilization().getById(2600, IUnitModel.class);
		assertEquals(1.0d, ucc.getCityStocks().getPeopleStock().available(), delta);
		assertEquals(50.0d, ucc.getCityStocks().getStockByType(StockType.wood).getVisibleAmount(), delta);
		assertEquals(0 , marketPlace.getObjectBuildQueue().getBuildsAmount());
		try {
			marketPlace.buildUnit(kusznik, 3);
		} catch (PayException e) {
			fail();
		}
		assertEquals(1, marketPlace.getObjectBuildQueue().getBuildsAmount());
		//marketPlace.getObjectBuildQueue().getNs
		//assertEquals( marketPlace.getObjectBuildQueue().g
		assertEquals(0.0d, ucc.getCityStocks().getPeopleStock().getVisibleAmount(), delta );
		assertEquals(0.0d, ucc.getCityStocks().getStockByType(StockType.wood).getVisibleAmount(), delta);
		
	}
	
	
}

