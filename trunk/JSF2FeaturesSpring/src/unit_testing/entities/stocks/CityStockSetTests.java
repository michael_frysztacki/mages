package unit_testing.entities.stocks;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import database.entities.DataBaseEntityFactory;
import economycomputation.recursion_events.RecursionEvent;
import entities.EntityFactory;
import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;
import entities.stocks.CityStock;
import entities.stocks.CityStockSet;
import framework.IStock;
import framework.IntegerStock;
import game_model_interfaces.ICityModel;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IGameParameters;
import game_model_interfaces.IWorldDefinition;
import game_model_interfaces.PriceStock;
import game_model_interfaces.PriceStockSet;
import game_model_interfaces.initializations.ICityInitializator;

@RunWith(Enclosed.class)
public class CityStockSetTests {

	CityStockSet cityStocks;
	EntityFactory factory = new DataBaseEntityFactory();
	@Mock
	UsersCityConfig city;
	
	@Mock
	ExtractCityBuilding porkFactory;
	
	@Mock
	ExtractCityBuilding riceFactory;
	
	@Mock
	ExtractCityBuilding waterFactory;
	
	@Mock
	ICityModel cityModel;
	@Mock
	IWorldDefinition wd;
	@Mock
	ICivilization europe;
	final String europeId = "europeId";
	@Mock
	ICivilization korea;
	final String koreaId = "koreaId";
	@Mock
	ICityInitializator cityInit;
	@Mock
	IGameParameters gameParams;
	
	@Mock
	IExtractBuildingModel porkFactoryModel;
	final String porkId = "porkId";
	
	@Mock
	IExtractBuildingModel riceFactoryModel;
	final String riceId = "riceId";
	
	@Mock
	IExtractBuildingModel waterFactoryModel;
	final String waterId = "waterId";
	final int waterSatisfaction = 3;
	
	@Mock
	IExtractBuildingModel ironFactoryModel;
	final String ironId = "ironId";
	
	@Mock
	IExtractBuildingModel stoneFactoryModel;
	final String stoneId = "stoneId";
	
	final String goldId = "goldId";
	final String silverId = "silverId";
	final String peopleId = "peopleId";
	final int peopleEatPerHour = 1;
	final int birthRateMultiplier = 1;
	
	final long oneHourNs = 60l*60l*1000l*1000l*1000l;
	final long halfHourNs = oneHourNs/2;
	final long oneAndHalfHourNs = oneHourNs + oneHourNs/2;
	final long twoHoursNs = oneHourNs*2;
	final long threeHoursNs = oneHourNs*3;
	@Before
	public void setup() {
		
		mock_foodIds();
		mock_resourceIds();
		mock_city();
		when(cityModel.getSatisfactionFromObjects(city)).thenReturn(0);
		when(europe.getCityModel()).thenReturn(cityModel);
		when(europe.getMainResource()).thenReturn(goldId);
		when(europe.getCivilization()).thenReturn(europeId);
		when(korea.getMainResource()).thenReturn(silverId);
		when(korea.getCivilization()).thenReturn(koreaId);
		when(korea.getCityInitializator()).thenReturn(cityInit);
		when(gameParams.getPeopleStockId()).thenReturn(peopleId);
		when(wd.getEatPerHour()).thenReturn(peopleEatPerHour);
		when(wd.getBirthMultiplier()).thenReturn(birthRateMultiplier);
		when(wd.getSatisfaction(waterId, europeId)).thenReturn(waterSatisfaction);
		
		mock_worldDefinition();	
		mockFoodProducers();
		mockResourceProducers();
	}
		private void mock_city() {
			when(city.getWorldDefinition()).thenReturn(wd);
			when(city.getMyCivilization()).thenReturn(europe);
			when(city.getGameParams()).thenReturn(gameParams);
			when(city.getCityInitializator()).thenReturn(cityInit);
			when(city.getCivilId()).thenReturn(europeId);
			when(porkFactory.isFull()).thenReturn(true);
			when(riceFactory.isFull()).thenReturn(true);
			when(waterFactory.isFull()).thenReturn(true);
		}
		private void mockFoodProducers() {
			when(wd.getProducer(riceId)).thenReturn(riceFactoryModel);
			when(wd.getProducer(waterId)).thenReturn(waterFactoryModel);
			when(wd.getProducer(porkId)).thenReturn(porkFactoryModel);
		}
		private void mockResourceProducers() {
			when(wd.getProducer(ironId)).thenReturn(ironFactoryModel);
			when(wd.getProducer(stoneId)).thenReturn(stoneFactoryModel);
		}
		private void mock_worldDefinition() {
			List<ICivilization> allCivils = new ArrayList<ICivilization>();
			allCivils.add(europe);
			allCivils.add(korea);
			when(wd.getAllCivilizations()).thenReturn(allCivils);
		}
		private void mock_resourceIds() {
			List<String> resourceIds = new ArrayList<String>();
			resourceIds.add(ironId);
			resourceIds.add(stoneId);
			when(wd.getAllResourceIds()).thenReturn(resourceIds);
		}
		private void mock_foodIds() {
			List<String> foodIds = new ArrayList<String>();
			foodIds.add(porkId);
			foodIds.add(riceId);
			foodIds.add(waterId);
			when(wd.getAllFoodIds()).thenReturn(foodIds);
		}
		
	@RunWith(MockitoJUnitRunner.class)
	public static class startUpTests extends CityStockSetTests {
		
		@Test
		public void startUp() {
			
			cityStocks = factory.createCityStocks(city);
			assertEquals(goldId, cityStocks.getTaxStock().getStockId());
			assertEquals(peopleId, cityStocks.getPeopleStock().getStockId());
			assertEquals(3, cityStocks.getFood().size());
			assertTrue(contains(cityStocks.getFood(),"waterId"));
			assertTrue(contains(cityStocks.getFood(),"riceId"));
			assertTrue(contains(cityStocks.getFood(),"porkId"));
			assertEquals(3, cityStocks.getResources().size());
			assertTrue(contains(cityStocks.getResources(),"ironId"));
			assertTrue(contains(cityStocks.getResources(),"stoneId"));
			assertTrue(contains(cityStocks.getResources(),"silverId"));
		}
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class PayTest extends CityStockSetTests {
		
		@Test
		public void canPay_sufficientResources() {
			PriceStockSet priceSet = createPrice(
				new IntegerStock(stoneId, 20),
				new IntegerStock(ironId,  30));
			initialCityStocks(
				new IntegerStock(stoneId, 30),
				new IntegerStock(ironId,  40));
			cityStocks = factory.createCityStocks(city);
			assertTrue(cityStocks.canPay(priceSet));
		}
		
		@Test
		public void canPay_equalResurces() {
			PriceStockSet priceSet = createPrice(
				new IntegerStock(stoneId, 20),
				new IntegerStock(ironId,  30));
			initialCityStocks(
				new IntegerStock(stoneId, 20),
				new IntegerStock(ironId,  30));
			cityStocks = factory.createCityStocks(city);
			assertTrue(cityStocks.canPay(priceSet));
		}
		
		@Test
		public void canPay_allResourcesInsufficient() {
			PriceStockSet priceSet = createPrice(
				new IntegerStock(stoneId, 20),
				new IntegerStock(ironId,  30));
			initialCityStocks(
				new IntegerStock(stoneId, 10),
				new IntegerStock(ironId,  10));
			cityStocks = factory.createCityStocks(city);
			assertFalse(cityStocks.canPay(priceSet));
		}
		
		@Test
		public void canPay_oneResourceInsufficient() {
			PriceStockSet priceSet = createPrice(
				new IntegerStock(stoneId, 20),
				new IntegerStock(ironId,  30));
			initialCityStocks(
				new IntegerStock(stoneId, 10),
				new IntegerStock(ironId,  100));
			cityStocks = factory.createCityStocks(city);
			assertFalse(cityStocks.canPay(priceSet));
		}
		
		@Test
		public void canPay_priceContainsThreeResources() {
			PriceStockSet priceSet = createPrice(
				new IntegerStock(stoneId, 20),
				new IntegerStock(ironId,  30),
				new IntegerStock(goldId,  30));
			initialCityStocks(
				new IntegerStock(stoneId, 100),
				new IntegerStock(ironId,  100));
			cityStocks = factory.createCityStocks(city);
			assertFalse(cityStocks.canPay(priceSet));
		}
		
		@Test
		public void doPay_sufficientResources() {
			PriceStockSet priceSet = createPrice(
				new IntegerStock(stoneId, 20),
				new IntegerStock(ironId,  30));
			initialCityStocks(
				new IntegerStock(stoneId, 30),
				new IntegerStock(ironId,  100));
			cityStocks = factory.createCityStocks(city);
			cityStocks.doPay(priceSet);
			assertEquals(10, cityStocks.getStockByType(stoneId).getVisibleAmount());
			assertEquals(70, cityStocks.getStockByType(ironId).getVisibleAmount());
		}
		
		@Test
		public void doPay_equalResources() {
			PriceStockSet priceSet = createPrice(
				new IntegerStock(stoneId, 20),
				new IntegerStock(ironId,  30));
			initialCityStocks(
				new IntegerStock(stoneId, 20),
				new IntegerStock(ironId,  30));
			cityStocks = factory.createCityStocks(city);
			cityStocks.doPay(priceSet);
			assertEquals(0, cityStocks.getStockByType(stoneId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(ironId).getVisibleAmount());
		}
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class ProcessingResources extends CityStockSetTests {
		
		@Test
		public void simpleTest_ironFactory() {
			
			final int startIronAmount = 0;
			final int ironProductionPerHour = 10;
			when(ironFactoryModel.getOutputPerHour(city)).thenReturn(ironProductionPerHour);
			initialCityStocks(
					new IntegerStock(peopleId, 0),
					new IntegerStock(ironId,  startIronAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(oneHourNs);
			
			assertEquals(startIronAmount + ironProductionPerHour, cityStocks.getStockByType(ironId).getVisibleAmount());
		}
		
		@Test
		public void ironInCity() {
			
			final int startIronAmount = 20;
			final int ironProductionPerHour = 10;
			when(ironFactoryModel.getOutputPerHour(city)).thenReturn(ironProductionPerHour);
			initialCityStocks(
					new IntegerStock(peopleId, 0),
					new IntegerStock(ironId,  startIronAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(twoHoursNs);
			
			assertEquals(startIronAmount + ironProductionPerHour*2, cityStocks.getStockByType(ironId).getVisibleAmount());
		}
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class FoodRecursion extends CityStockSetTests {
		
		@Test
		public void zeroPeople_birthRateZero_NoRecursionEvent() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction * -1); // for setting general satisfaction to zero
			final int startWaterAmount = 30;
			final int startPeopleAmount = 0;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			assertEquals(0, cityStocks.getShortestRecursionEvents().size());
		}
		
		@Test
		public void tenPeople_birthRateZero_expectFoodFall() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction * -1); // for setting general satisfaction to zero
			final int startWaterAmount = 30;
			final int startPeopleAmount = 10;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			assertEquals(1, cityStocks.getShortestRecursionEvents().size());
			assertEquals(threeHoursNs, cityStocks.getShortestRecursionEvents().get(0).recursionTimeNs().longValue());
		}
		
		@Test
		public void tenPeople_birthRateZero_twoFoodTypes_expectFoodFall() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction * -1); // for setting general satisfaction to zero
			final int startWaterAmount = 30;
			final int startRiceAmount = 30;
			final int startPeopleAmount = 10;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(riceId, startRiceAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			assertEquals(2, cityStocks.getShortestRecursionEvents().size());
			assertEquals(2 * threeHoursNs, cityStocks.getShortestRecursionEvents().get(0).recursionTimeNs().longValue());
		}
		
		@Test
		public void tenPeople_birthRateZero_threeFoodTypes_expectFoodFall() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction * -1); // for setting general satisfaction to zero
			final int startWaterAmount = 30;
			final int startRiceAmount = 30;
			final int startPorkAmount = 30;
			final int startPeopleAmount = 10;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(riceId, startRiceAmount),
				new IntegerStock(porkId, startPorkAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			assertEquals(3, cityStocks.getShortestRecursionEvents().size());
			assertEquals(3 * threeHoursNs, cityStocks.getShortestRecursionEvents().get(0).recursionTimeNs().longValue());
		}
		
		@Test
		public void tenPeople_birthRateZero_twoFoodTypes_oneInsufficientSupportingPorkFactroy_expectFoodFall() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction * -1); // for setting general satisfaction to zero
			when(porkFactoryModel.getOutputPerHour(city)).thenReturn(2); //insuficient factory
			final int startWaterAmount = 20;
			final int startRiceAmount = 20;
			final int startPeopleAmount = 10;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(riceId, startRiceAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			assertEquals(2, cityStocks.getShortestRecursionEvents().size());
			assertEquals(5 * oneHourNs, cityStocks.getShortestRecursionEvents().get(0).recursionTimeNs().longValue());
		}
		
		@Test
		public void processRE_tenPeople_birthRateZero_twoFoodTypes_oneInsufficientSupportingPorkFactroy_expectFoodFall() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction * -1); // for setting general satisfaction to zero
			when(porkFactoryModel.getOutputPerHour(city)).thenReturn(2); //insuficient factory
			final int startWaterAmount = 20;
			final int startRiceAmount = 20;
			final int startPeopleAmount = 10;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(riceId, startRiceAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			processRecursionEvents(cityStocks.getShortestRecursionEvents());
			
			assertEquals(0, cityStocks.getShortestRecursionEvents().size());
			assertEquals(10, cityStocks.getPeopleStock().getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		@Test
		public void startWithFloatPeople_birthRateNegative_onlyWater_FoodFallBeforeFirstPersonLeave() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction *-(1) -1); // for setting general satisfaction to minus one
			final int startWaterAmount = 10;
			final int startPeopleAmount = 11;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(halfHourNs);
			
			assertEquals(10, cityStocks.getPeopleStock().getVisibleAmount());
			assertEquals(5, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(1, cityStocks.getShortestRecursionEvents().size());
			assertEquals(halfHourNs, cityStocks.getShortestRecursionEvents().get(0).recursionTimeNs().longValue());
		}
		
		//@Ignore
		@Test
		public void startWithFloatPeople_birthRateNegative_onlyWater_FoodFallAfterPersonLeave() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction *(-1) -1); // for setting general satisfaction to minus one
			final int startWaterAmount = 9;
			final int startPeopleAmount = 10;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			assertNull(cityStocks.getCityFood(waterId).getCooldownRemainingTime());
			assertEquals(1, cityStocks.getShortestRecursionEvents().size());
			assertEquals(oneHourNs, cityStocks.getShortestRecursionEvents().get(0).recursionTimeNs().longValue());
		}
		
		private void processRecursionEvents(List<RecursionEvent> list) {
			for(RecursionEvent re: list)
				re.process();
		}
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class ProcessingFood extends CityStockSetTests {
		
		@Before
		public void ProcessingFoodSetup() {
			
		}
		
		/*
		 * water in city: 30
		 * people in city: 0
		 * water factory: 0/h
		 * birthRate: 0/h;
		 * 
		 * after processing water should be in same amount.
		 */
		@Test
		public void zeroBirthRate_factoriesAreOff() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction * -1); // for setting general satisfaction to zero
			final int startWaterAmount = 30;
			initialCityStocks(
				new IntegerStock(peopleId, 0),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(oneHourNs);
			
			assertEquals(startWaterAmount, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(peopleId).getVisibleAmount());
		}
		
		/*
		 * 
		 * water in city: 100
		 * people in city: 10
		 * water factory: 0/h
		 * birthRate: 0/h;
		 */
		@Test
		public void zeroBirthRate_factoriesOff_peopleAndWaterInCity() {
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction*(-1)); // for setting general satisfaction to minus one
			initialCityStocks(
				new IntegerStock(waterId, 100),
				new IntegerStock(peopleId,10));
			cityStocks = factory.createCityStocks(city);
			assertEquals(0, cityStocks.getPeopleStock().getVisibleBirthRate());
			
			cityStocks.process(oneHourNs);
			
			assertEquals(90, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
			assertEquals(10, cityStocks.getPeopleStock().getVisibleAmount());
		}
		
		/*
		 * water in city: 30
		 * people in city: 20
		 * water factory: 0/h
		 * birthRate: -1/h;
		 * 
		 * after processing people should leave the city.
		 */
		@Test
		public void negativeBirthRate_factoriesAreOff_processHalfHour() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction*(-1) - 1); // for setting general satisfaction to minus one
			final int startPeopleAmount = 20;
			final int startWaterAmount = 30;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(oneHourNs/2);
			
			assertEquals(startPeopleAmount-1, cityStocks.getStockByType(peopleId).getVisibleAmount());                                                                                                                  
			assertEquals(20, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * water in city: 30
		 * people in city: 20
		 * water factory: 0/h
		 * birthRate: -1/h;
		 * 
		 * after processing people should leave the city.
		 */
		@Test
		public void negativeBirthRate_factoriesAreOff_processOneHour() {
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction*(-1) - 1); // for seting general satisfaction to minus one
			final int startWaterAmount = 30;
			final int startPeopleAmount = 20;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(oneHourNs);
			
			assertEquals(startPeopleAmount-1, cityStocks.getStockByType(peopleId).getVisibleAmount());
			assertEquals(11, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * water in city: 30
		 * people in city: 20
		 * water factory: 0/h
		 * birthRate: -1/h;
		 * 
		 * after processing people should leave the city.
		 */
		@Test
		public void negativeBirthRate_factoriesAreOff_processOneAndHalfHour() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction*(-1) - 1); // For setting general satisfaction to minus one.
			final int startWaterAmount = 30;
			final int startPeopleAmount = 20;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(oneAndHalfHourNs);
			
			assertEquals(startPeopleAmount-2, cityStocks.getStockByType(peopleId).getVisibleAmount());
			assertEquals(2, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * water in city: 30
		 * rice in city: 20
		 * people in city: 20
		 * water factory: 0/h
		 * birthRate: -1/h;
		 * 
		 * after processing people should leave the city.
		 */
		@Test
		public void negativeBirthRate_factoriesAreOff_twoFoodTypes_processOneHour() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction*(-1) - 1); // For setting general satisfaction to minus one.
			final int startWaterAmount = 30;
			final int startRiceAmount = 20;
			final int startPeopleAmount = 20;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(riceId, startRiceAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(oneHourNs);
			
			assertEquals(startPeopleAmount-1, cityStocks.getStockByType(peopleId).getVisibleAmount());
			assertEquals(20, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(10, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * water in city: 30
		 * rice in city: 20
		 * people in city: 20
		 * water factory: 0/h
		 * birthRate: 1/h;
		 * 
		 * after processing people should leave the city.
		 */
		@Test
		public void positiveBirthRate_factoriesAreOff_twoFoodTypes_processOneHour() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction*(-1) + 1); // For setting general satisfaction to plus one.
			final int startWaterAmount = 30;
			final int startRiceAmount = 20;
			final int startPeopleAmount = 20;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(riceId, startRiceAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(oneHourNs);
			
			assertEquals(startPeopleAmount+1, cityStocks.getStockByType(peopleId).getVisibleAmount());
			assertEquals(20, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(10, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * water in city: 30
		 * rice in city: 20
		 * pork in city: 50
		 * people in city: 20
		 * water factory: 0/h
		 * birthRate: 1/h;
		 *
		 */
		@Test
		public void positiveBirthRate_factoriesAreOff_threeFoodTypes_processTwoHours() {
			
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction*(-1) + 1); // For setting general satisfaction to plus one.
			final int startWaterAmount = 30;
			final int startRiceAmount = 20;
			final int startPorkAmount = 50;
			final int startPeopleAmount = 20;
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(riceId, startRiceAmount),
				new IntegerStock(porkId, startPorkAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(twoHoursNs);
			
			assertEquals(startPeopleAmount+2, cityStocks.getStockByType(peopleId).getVisibleAmount());
			assertEquals(16, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(6, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(36, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * water in city: 30
		 * water factory: 20/h
		 * people in city: 20
		 * birthRate: 1/h;
		 *
		 */
		@Test
		public void positiveBirthRate_waterFactoryWorking_waterInCity() {
			
			final int startWaterAmount = 30;
			final int startPeopleAmount = 20;
			final int waterProductionPerHour = 20;
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction*(-1) + 1); // For setting general satisfaction to plus one.
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProductionPerHour);
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(twoHoursNs);
			
			int waterEatenAfterTwoHours = 41;
			assertEquals(startPeopleAmount+2, cityStocks.getStockByType(peopleId).getVisibleAmount());
			assertEquals(startWaterAmount - waterEatenAfterTwoHours + waterProductionPerHour*2, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		/*
		 * water in city: 30
		 * rice in city: 20
		 * people in city: 20
		 * water factory: 20/h
		 * water factory: 30/h
		 * birthRate: 1/h;
		 *
		 */
		@Test
		public void positiveBirthRate_waterAndRiceFactoryWorking_waterAndRiceInCity() {
			
			final int startWaterAmount = 30;
			final int startPeopleAmount = 20;
			final int startRiceAmount = 40;
			final int waterProductionPerHour = 20;
			final int riceProductionPerHour = 30;
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction*(-1) + 1); // For setting general satisfaction to plus one.
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProductionPerHour);
			when(riceFactoryModel.getOutputPerHour(city)).thenReturn(riceProductionPerHour);
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(riceId, startRiceAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(twoHoursNs);
			
			assertEquals(startPeopleAmount+2, cityStocks.getStockByType(peopleId).getVisibleAmount());
			assertEquals(49, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(79, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * water in city: 30
		 * rice in city: 20
		 * pork in city: 0
		 * people in city: 20
		 * water factory: 20/h
		 * rice factory: 30/h
		 * pork factory: 5/h - insufficient
		 * birthRate: 1/h;
		 *
		 */
		@Test
		public void positiveBirthRate_waterAndRiceFactoryWorking_porkFactoryInsufficient_waterAndRiceInCity() {
			
			final int startWaterAmount = 30;
			final int startPeopleAmount = 20;
			final int startRiceAmount = 40;
			final int waterProductionPerHour = 20;
			final int riceProductionPerHour = 30;
			final int porkProductionPerHour = 5;
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction*(-1) + 1); // For setting general satisfaction to plus one.
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProductionPerHour);
			when(riceFactoryModel.getOutputPerHour(city)).thenReturn(riceProductionPerHour);
			when(porkFactoryModel.getOutputPerHour(city)).thenReturn(porkProductionPerHour);
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(riceId, startRiceAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(twoHoursNs);
			
			assertEquals(startPeopleAmount+2, cityStocks.getStockByType(peopleId).getVisibleAmount());
			assertEquals(54, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(84, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * water in city: 30
		 * rice in city: 0
		 * pork in city: 0
		 * people in city: 20
		 * water factory: 20/h
		 * rice factory: 7/h - insufficient
		 * pork factory: 5/h - insufficient
		 * birthRate: 1/h;
		 */
		@Test
		public void positiveBirthRate_waterFactoryWorking_porkAndRiceFactoriesAreInsufficient_waterInCity() {
			
			final int startWaterAmount = 30;
			final int startPeopleAmount = 20;
			final int waterProductionPerHour = 20;
			final int riceProductionPerHour = 7;
			final int porkProductionPerHour = 5;
			when(city.getTaxSatisfaction()).thenReturn(waterSatisfaction*(-1) + 1); // For setting general satisfaction to plus one.
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProductionPerHour);
			when(riceFactoryModel.getOutputPerHour(city)).thenReturn(riceProductionPerHour);
			when(porkFactoryModel.getOutputPerHour(city)).thenReturn(porkProductionPerHour);
			initialCityStocks(
				new IntegerStock(peopleId, startPeopleAmount),
				new IntegerStock(waterId,  startWaterAmount));
			cityStocks = factory.createCityStocks(city);
			
			cityStocks.process(twoHoursNs);
			
			assertEquals(startPeopleAmount+2, cityStocks.getStockByType(peopleId).getVisibleAmount());
			assertEquals(53, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class InitializingStateOfFood extends CityStockSetTests {
		
		@Before
		public void InitializingStateOfFoodSetup() {
			when(wd.getEatPerHour()).thenReturn(1);
		}
		
		@Test
		public void doNotChangeStateOfNonZeroFood() {
			initialCityStocks(
				new IntegerStock(porkId, 100),
				new IntegerStock(waterId, 100),
				new IntegerStock(riceId, 100));
			cityStocks = factory.createCityStocks(city);
			assertEquals(100, cityStocks.getStockByType(porkId).getVisibleAmount());
			assertEquals(100, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(100, cityStocks.getStockByType(riceId).getVisibleAmount());
		}
		
		@Test
		public void zeroAllFood_zeroPeople_riceFactoryExtracting_riceShouldBeInCity() {
			final int riceProduction = 50;
			when(riceFactoryModel.getOutputPerHour(city)).thenReturn(riceProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(1, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		@Test
		public void zeroAllFood_zeroPeople_riceAndWaterFactoryExtracting_riceAndWaterShouldBeInCity() {
			final int riceProduction = 50;
			when(riceFactoryModel.getOutputPerHour(city)).thenReturn(riceProduction);
			final int waterProduction = 50;
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(1, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(1, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * Eating per hour in city: 10/h
		 * water in city: 0.
		 * rice in city: 0.
		 * water factory: 8/h.
		 * rice factory: 0/h.
		 * 
		 * People want to eat 10 water/h, thus water shouldn't be in city.
		 */
		@Test
		public void zeroAllFood_10People_riceAndWaterFactoryExtracting_riceAndWaterShouldBeInCity() {
			initialCityStocks(new IntegerStock(peopleId, 10));
			final int riceProduction = 50;
			when(riceFactoryModel.getOutputPerHour(city)).thenReturn(riceProduction);
			final int waterProduction = 50;
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(1, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(1, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * Zjadanie w mie�cie: 10/h
		 * stan wody w mie�cie: 0.
		 * stan ry�u w mie�cie: 0.
		 * fabryka wody produkuje 8/h.
		 * fabryka ry�u produkuje 0/h.
		 * 
		 * Ludzie chc� je�� 10 wody/h, tak wiec wody nie powinno byc w miescie.
		 */
		@Test
		public void zeroAllFood_10People_waterFactoryExtractingInsufficient_waterShouldNotBeInCity() {
			initialCityStocks(new IntegerStock(peopleId, 10));
			final int waterProduction = 8;
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(0, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * Zjadanie w mie�cie: 10/h
		 * stan wody w mie�cie: 0.
		 * stan ry�u w mie�cie: 0.
		 * fabryka wody produkuje 8/h.
		 * fabryka ry�u produkuje 8/h.
		 * 
		 * Ludzie chc� je�� po 5/h ry�u i wody, tak wi�c do miasta powinny nap�ywa� ry� i woda (+3/h)
		 */
		@Test
		public void zeroAllFood_10People_waterAndRiceExtractingSufficient() {
			initialCityStocks(new IntegerStock(peopleId, 10));
			final int waterProduction = 8;
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProduction);
			final int riceProduction = 8;
			when(riceFactoryModel.getOutputPerHour(city)).thenReturn(riceProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(1, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(1, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * Zjadanie w mie�cie: 10/h
		 * stan wody w mie�cie: 0.
		 * stan ry�u w mie�cie: 0.
		 * fabryka wody produkuje 4/h.
		 * fabryka ry�u produkuje 8/h.
		 * 
		 * Ludzie chc� je�� po 5/h ry�u i wody, tak wi�c do miasta powinna nap�ywa� ry�,
		 * ale nie powinna nap�ywa� woda, gdy� jest ca�kowicie zjadana.
		 */
		@Test
		public void zeroAllFood_10People_waterSufficient_riceInsufficient() {
			initialCityStocks(new IntegerStock(peopleId, 10));
			final int waterProduction = 4;
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProduction);
			final int riceProduction = 8;
			when(riceFactoryModel.getOutputPerHour(city)).thenReturn(riceProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(0, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(1, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * 
		 */
		@Test
		public void amountOfPorkThatAlreadyWasInCity_ShouldStaySameAmount() {
			initialCityStocks(new IntegerStock(peopleId, 10),
							  new IntegerStock(porkId, 10));
			final int porkProduction = 5;
			when(porkFactoryModel.getOutputPerHour(city)).thenReturn(porkProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(0, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(10, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * Zjadanie w mie�cie: 10/h
		 * stan wody w mie�cie: 0.
		 * stan ry�u w mie�cie: 0.
		 * fabryka wody produkuje 3/h.
		 * fabryka ry�u produkuje 6/h.
		 * 
		 * Ludzie chc� je�� po 5/h ry�u i wody. Nie mog� je�� po 5 ry�u i wody,
		 * bo jest niewystarczaj�co wody. Tak wi�c wod� zjadaj� ca�� i chc� jeszcze
		 * 7 jedzenia. Ry� jest produkowany w ilo�ci 6/h, tak wi�c i on zostanie ca�y zjedzony.
		 * Do miasta nie nap�ywa ani woda ani ry�.
		 */
		@Test
		public void zeroAllFood_10People_waterInsufficient_riceInsufficient_complexCase() {
			
			initialCityStocks(new IntegerStock(peopleId, 10));
			final int waterProduction = 3;
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProduction);
			final int riceProduction = 6;
			when(riceFactoryModel.getOutputPerHour(city)).thenReturn(riceProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(0, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * Zjadanie w mie�cie: 10/h
		 * stan wody w mie�cie: 0.
		 * stan ry�u w mie�cie: 0.
		 * fabryka wody produkuje 3/h.
		 * fabryka ry�u produkuje 7/h.
		 * 
		 * Ludzie chc� je�� po 5/h ry�u i wody. Nie mog� je�� po 5 ry�u i wody,
		 * bo jest niewystarczaj�co wody. Tak wi�c wod� zjadaj� ca�� i chc� jeszcze
		 * 7 jedzenia. Ry� jest produkowany w ilo�ci 7/h, tak wi�c zostaje on zjedzony w ca�o�ci (wystarcza go na styk).
		 * Do miasta nie nap�ywa ani woda ani ry�.
		 */
		@Test
		public void zeroAllFood_10People_waterInsufficient_riceEqual_complexCase() {
			
			initialCityStocks(new IntegerStock(peopleId, 10));
			final int waterProduction = 3;
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProduction);
			final int riceProduction = 7;
			when(riceFactoryModel.getOutputPerHour(city)).thenReturn(riceProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(0, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * Eating per hour in city: 10/h
		 * water in city: 10.
		 * rice in city: 10.
		 * pork in city: 0.
		 * water factory: 0/h.
		 * rice factory: 0/h.
		 * pork factory: 4/h.
		 * 
		 * People want to eat 3.(3)/h each of food. Pork factory is sufficient,
		 * thus pork should be in city.
		 */
		@Test
		public void existingFoodSupportingFoodFactory() {
			initialCityStocks(new IntegerStock(peopleId, 10),
							  new IntegerStock(waterId, 10),
							  new IntegerStock(riceId, 10));
			final int porkProduction = 4;
			when(porkFactoryModel.getOutputPerHour(city)).thenReturn(porkProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(10, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(10, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(1, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * Eating per hour in city: 10/h
		 * water in city: 10.
		 * rice in city: 10.
		 * pork in city: 0.
		 * water factory: 0/h.
		 * rice factory: 0/h.
		 * pork factory: 3/h.
		 * 
		 * People want to eat 3.(3)/h each of food. Pork factory is insufficient,
		 * thus pork shouldn't be in city.
		 */
		@Test
		public void existingFoodSupportingFoodFactoryButNotEnough() {
			initialCityStocks(new IntegerStock(peopleId, 10),
							  new IntegerStock(waterId, 10),
							  new IntegerStock(riceId, 10));
			final int porkProduction = 3;
			when(porkFactoryModel.getOutputPerHour(city)).thenReturn(porkProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(10, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(10, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * Eating per hour in city: 21/h
		 * water in city: 0.
		 * rice in city: 0.
		 * pork in city: 0.
		 * water factory: 7/h.
		 * rice factory: 7/h.
		 * pork factory: 7/h.
		 * 
		 * People want to eat 7/h each of food. All factories are producing food
		 * enough for people, but not enough for storing in city
		 */
		@Test
		public void zeroFood_AllFactoriesAreExtractingEqualyForPeople() {
			initialCityStocks(new IntegerStock(peopleId, 21));
			final int waterProduction = 7;
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProduction);
			final int riceProduction = 7;
			when(riceFactoryModel.getOutputPerHour(city)).thenReturn(riceProduction);
			final int porkProduction = 7;
			when(porkFactoryModel.getOutputPerHour(city)).thenReturn(porkProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(0, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * Eating per hour in city: 21/h
		 * water in city: 0.
		 * rice in city: 0.
		 * pork in city: 0.
		 * water factory: 7/h.
		 * rice factory: 7/h.
		 * pork factory: 8/h.
		 * 
		 * People want to eat 7/h each of food. All factories are producing food
		 * enough for people, but not enough for storing in city, excpet pork.
		 */
		@Test
		public void zeroFood_AllFactoriesAreExtractingEqualyForPeopleExceptPorkWhichIsExtractingMore() {
			initialCityStocks(new IntegerStock(peopleId, 21));
			final int waterProduction = 7;
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProduction);
			final int riceProduction = 7;
			when(riceFactoryModel.getOutputPerHour(city)).thenReturn(riceProduction);
			final int porkProduction = 8;
			when(porkFactoryModel.getOutputPerHour(city)).thenReturn(porkProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(0, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(0, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(1, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
		/*
		 * water in city: 30
		 * rice in city: 20
		 * pork in city: 0
		 * people in city: 20
		 * water factory: 20/h
		 * rice factory: 30/h
		 * pork factory: 7/h - insufficient
		 * */
		@Test
		public void complexTest01() {
			initialCityStocks(new IntegerStock(peopleId, 20),
							  new IntegerStock(riceId, 20),
							  new IntegerStock(waterId, 30));
			final int waterProduction = 20;
			when(waterFactoryModel.getOutputPerHour(city)).thenReturn(waterProduction);
			final int riceProduction = 30;
			when(riceFactoryModel.getOutputPerHour(city)).thenReturn(riceProduction);
			final int porkProduction = 7;
			when(porkFactoryModel.getOutputPerHour(city)).thenReturn(porkProduction);
			cityStocks = factory.createCityStocks(city);
			assertEquals(30, cityStocks.getStockByType(waterId).getVisibleAmount());
			assertEquals(20, cityStocks.getStockByType(riceId).getVisibleAmount());
			assertEquals(1, cityStocks.getStockByType(porkId).getVisibleAmount());
		}
		
	}
	
	
		PriceStockSet createPrice(IntegerStock... prices) {
			PriceStockSet priceSet = mock(PriceStockSet.class);
			List<PriceStock> priceStocks = new ArrayList<PriceStock>();
			for(IntegerStock p: prices) {
				PriceStock price = mock(PriceStock.class);
				when(price.getStockId()).thenReturn(p.getStockId());
				when(price.getVisibleAmount()).thenReturn(p.getVisibleAmount());
				priceStocks.add(price);
			}
			when(priceSet.getPriceStocks(europe)).thenReturn(priceStocks);
			return priceSet;
		}
		void initialCityStocks(IntegerStock... stocks) {
			List<IStock> initialStocksInCity = new ArrayList<IStock>();
			initialStocksInCity.addAll(Arrays.asList(stocks));
			when(cityInit.initialCityStocks()).thenReturn(initialStocksInCity);
		}
		boolean contains(List<? extends CityStock> stocks, String stockId) {
			for(CityStock s: stocks) {
				if(s.getStockId().equals(stockId))
					return true;
			}
			return false;
		}
}

