package unit_testing.entities;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import database.entities.DataBaseEntityFactory;

import entities.EntityFactory;
import entities.UserConfig;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IResearchModel;
import game_model_interfaces.IWorldDefinition;

@RunWith(MockitoJUnitRunner.class)
public class PlayerTests {

	UserConfig player;
	final String nick = "friko";
	final String pass = "pass";
	final String europeId = "europeId";
	EntityFactory factory = new DataBaseEntityFactory();
	
	@Mock
	IWorldDefinition wd;
	
	@Mock
	ICivilization myCivil;
	
	@Mock
	IResearchModel improvedGoldMine;
	
	@Mock
	IResearchModel improvedGoldMine;
	
	@Before
	public void setup() {
		
		when(wd.getCivilization(europeId)).thenReturn(myCivil);
		player = factory.createUserConfig(wd,nick,pass);
	}
	
	@Test
	public void startup() throws Exception {
		
	}
}
