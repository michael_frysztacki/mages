package unit_testing.entities.CityTests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import framework.IntegerStock;

@RunWith(MockitoJUnitRunner.class)
public class OneFoodFallingTests extends SimpleGameModelTemplate{

	@Before
	public void setUpOneFoodFallingTests() {
		
		mock_initialCityStocks(
				new IntegerStock(peopleId, 10),
				new IntegerStock(waterId, 10));
		mockObjectsSatisfaction(0);
		mockFoodSatisfaction(waterId, 1);
		mockInitialTaxSatisfaction(-1);
		mockEatPerHour(1);
		mock_birthRateMultiplier();
		ucc = factory.createCity(friko, cityPoint, nsCityStartTime);
		
	}
	
	@Test
	public void simpleCaseFood() {
		
		long start = System.nanoTime();
		ucc.process(oneHourNs/2);
		long stop = System.nanoTime();
		System.out.print((stop-start));
		
		assertEquals(0, ucc.getCityStocks().getPeopleStock().getSatisfaction());
		assertEquals(1, ucc.getCityStocks().getCityFood(waterId).getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getPeopleStock().getVisibleBirthRate());
		assertEquals(5, ucc.getCityStocks().getCityFood(waterId).getVisibleAmount());
		assertEquals(10, ucc.getCityStocks().getPeopleStock().getVisibleAmount());
	}
	
	@Test
	public void foodFallRE_process() {
	
		ucc.process(oneHourNs + halfHourNs);
		
		assertEquals(-1, ucc.getCityStocks().getPeopleStock().getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getCityFood(waterId).getSatisfaction());
		assertEquals(-1, ucc.getCityStocks().getPeopleStock().getVisibleBirthRate());
		assertEquals(0, ucc.getCityStocks().getCityFood(waterId).getVisibleAmount());
		assertEquals(9, ucc.getCityStocks().getPeopleStock().getVisibleAmount());
	}
	@Test
	public void foodFallRE() {
		
		// process after one hour
		ucc.process(oneHourNs);
		
		// check city state after one hour
		assertEquals(-1, ucc.getCityStocks().getPeopleStock().getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getCityFood(waterId).getSatisfaction());
		assertEquals(-1, ucc.getCityStocks().getPeopleStock().getVisibleBirthRate());
		assertEquals(0, ucc.getCityStocks().getCityFood(waterId).getVisibleAmount());
		assertEquals(10, ucc.getCityStocks().getPeopleStock().getVisibleAmount());
	}
	
}
