package unit_testing.entities.CityTests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import framework.IntegerStock;

@RunWith(MockitoJUnitRunner.class)
public class TwoFoodsWithSameAmountFallingTests extends SimpleGameModelTemplate{

	@Before
	public void setUpTwoFoodsWithSameAmountFallingTests() {
		
		//Mock game model
		mockObjectsSatisfaction(0);
		mockFoodSatisfaction(waterId, 1);
		mockFoodSatisfaction(riceId, 1);
		mockInitialTaxSatisfaction(-2);
		mockEatPerHour(1);
		mock_birthRateMultiplier();
		// End mocking game model
		
		// set initial city state
		mock_initialCityStocks(
			new IntegerStock(peopleId, 10),
			new IntegerStock(waterId, 10),
			new IntegerStock(riceId, 10));

		ucc = factory.createCity(friko, cityPoint, nsCityStartTime);
		
		assertEquals(0, ucc.getCityStocks().getPeopleStock().getVisibleBirthRate());
	}
	
	@Test
	public void process() {
		
		ucc.process(oneHourNs);
		
		// check city state after processing
		assertEquals(0, ucc.getCityStocks().getPeopleStock().getSatisfaction());
		assertEquals(1, ucc.getCityStocks().getCityFood(waterId).getSatisfaction());
		assertEquals(1, ucc.getCityStocks().getCityFood(riceId).getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getPeopleStock().getVisibleBirthRate());
		assertEquals(5, ucc.getCityStocks().getCityFood(waterId).getVisibleAmount());
		assertEquals(5, ucc.getCityStocks().getCityFood(riceId).getVisibleAmount());
		assertEquals(10, ucc.getCityStocks().getPeopleStock().getVisibleAmount());
	}
	
	@Test
	public void foodFallRE() {
		
		ucc.process(oneHourNs*2);
		
		// check city state after processing
		assertEquals(-2, ucc.getCityStocks().getPeopleStock().getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getCityFood(waterId).getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getSatisfaction());
		assertEquals(-2, ucc.getCityStocks().getPeopleStock().getVisibleBirthRate());
		assertEquals(0, ucc.getCityStocks().getCityFood(waterId).getVisibleAmount());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getVisibleAmount());
		assertEquals(10, ucc.getCityStocks().getPeopleStock().getVisibleAmount());
	}
	
	@Test
	public void foodFallRE_process() {
		
		ucc.process(oneHourNs*2 + oneHourNs);
		
		// check city state after processing
		assertEquals(-2, ucc.getCityStocks().getPeopleStock().getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getCityFood(waterId).getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getSatisfaction());
		assertEquals(-2, ucc.getCityStocks().getPeopleStock().getVisibleBirthRate());
		assertEquals(0, ucc.getCityStocks().getCityFood(waterId).getVisibleAmount());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getVisibleAmount());
		assertEquals(8, ucc.getCityStocks().getPeopleStock().getVisibleAmount());
	}
}
