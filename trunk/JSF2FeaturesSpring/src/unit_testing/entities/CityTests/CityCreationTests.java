package unit_testing.entities.CityTests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;
import framework.IntegerStock;

@RunWith(MockitoJUnitRunner.class)
public class CityCreationTests extends SimpleGameModelTemplate{

	@Test
	public void startup() {
		mock_initialCityStocks(new IntegerStock(peopleId,10), 
							   new IntegerStock(waterId, 100),
							   new IntegerStock(porkId, 100),
							   new IntegerStock(riceId,120),
							   new IntegerStock(ironId,100),
							   new IntegerStock(woodId,80));
		ucc = factory.createCity(friko, cityPoint, nsCityStartTime);
		
		// assert Buildings
		assertEquals(3, ucc.getBuildings().size());
		for(CityBuilding cb : ucc.getBuildings())
			assertFalse(cb.isCurrentlyUpgrading());
		assertEquals(6, ucc.getExtractBuildings().size());
		for(ExtractCityBuilding ecb : ucc.getExtractBuildings())
			assertFalse(ecb.isCurrentlyUpgrading());
		// assert city stocks
		assertEquals(4, ucc.getCityStocks().getFood().size());
		assertEquals(10, ucc.getCityStocks().getPeopleStock().getVisibleAmount());
		assertEquals(100, ucc.getCityStocks().getCityFood(waterId).getVisibleAmount());
		assertEquals(100, ucc.getCityStocks().getCityFood(porkId).getVisibleAmount());
		assertEquals(120, ucc.getCityStocks().getCityFood(riceId).getVisibleAmount());
		
		assertEquals(2, ucc.getCityStocks().getResources().size());
		assertEquals(100, ucc.getCityStocks().getCityResource(ironId).getVisibleAmount());
		assertEquals(80, ucc.getCityStocks().getCityResource(woodId).getVisibleAmount());
		
		assertEquals(peopleId, ucc.getCityStocks().getPeopleStock().getStockId());
		assertEquals(0, ucc.getCityStocks().getTaxStock().getVisibleAmount());
		assertEquals(nsCityStartTime, ucc.getCityTimeNs_());
	}
	
}
