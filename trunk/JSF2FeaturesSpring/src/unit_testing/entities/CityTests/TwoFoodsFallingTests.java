package unit_testing.entities.CityTests;

import static org.junit.Assert.assertEquals;

import org.jscience.mathematics.number.Rational;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import framework.IntegerStock;

@RunWith(MockitoJUnitRunner.class)
public class TwoFoodsFallingTests extends SimpleGameModelTemplate{

	@Before
	public void setUpTwoFoodsFallingTests() {
		
		//Mock game model
		mockObjectsSatisfaction(0);
		mockFoodSatisfaction(waterId, 1);
		mockFoodSatisfaction(riceId, 1);
		mockInitialTaxSatisfaction(-2);
		mockEatPerHour(1);
		mock_birthRateMultiplier();
		// End mocking game model
		
		// set initial city state
		mock_initialCityStocks(
			new IntegerStock(peopleId, 10),
			new IntegerStock(waterId, 10),
			new IntegerStock(riceId, 5));

		ucc = factory.createCity(friko, cityPoint, nsCityStartTime);
	}
	@Test
	public void twoFoods_foodFallRE() {
		
		// process exceeding one hour
		ucc.process(oneHourNs);
		
		// check city state after processing
		assertEquals(-1, ucc.getCityStocks().getPeopleStock().getSatisfaction());
		assertEquals(1, ucc.getCityStocks().getCityFood(waterId).getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getSatisfaction());
		assertEquals(-1, ucc.getCityStocks().getPeopleStock().getVisibleBirthRate());
		assertEquals(5, ucc.getCityStocks().getCityFood(waterId).getVisibleAmount());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getVisibleAmount());
		assertEquals(10, ucc.getCityStocks().getPeopleStock().getVisibleAmount());
	}
	
	@Test
	public void twoFoods_foodFallRE_process_short() {
		
		// process exceeding one hour
		ucc.process(oneHourNs*10/9);
		
		// check city state after processing
		assertEquals(-1, ucc.getCityStocks().getPeopleStock().getSatisfaction());
		assertEquals(1, ucc.getCityStocks().getCityFood(waterId).getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getSatisfaction());
		assertEquals(-1, ucc.getCityStocks().getPeopleStock().getVisibleBirthRate());
		assertEquals(4, ucc.getCityStocks().getCityFood(waterId).getVisibleAmount());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getVisibleAmount());
		assertEquals(9, ucc.getCityStocks().getPeopleStock().getVisibleAmount());
	}
	
	@Test
	public void twoFoods_foodFallRE_process_almostNextRE() {
		
		// process exceeding one hour
		ucc.process(oneHourNs*13/9);
		
		// check city state after processing
		assertEquals(-1, ucc.getCityStocks().getPeopleStock().getSatisfaction());
		assertEquals(1, ucc.getCityStocks().getCityFood(waterId).getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getSatisfaction());
		assertEquals(-1, ucc.getCityStocks().getPeopleStock().getVisibleBirthRate());
		assertEquals(1, ucc.getCityStocks().getCityFood(waterId).getVisibleAmount());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getVisibleAmount());
		assertEquals(9, ucc.getCityStocks().getPeopleStock().getVisibleAmount());
	}
	
	@Test
	public void twoFoods_foodFallRE_foodFallRE() {
		
		// process exceeding one hour
		ucc.process(oneHourNs*14/9);
		
		// check city state after processing
		assertEquals(-2, ucc.getCityStocks().getPeopleStock().getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getCityFood(waterId).getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getSatisfaction());
		assertEquals(-2, ucc.getCityStocks().getPeopleStock().getVisibleBirthRate());
		assertEquals(0, ucc.getCityStocks().getCityFood(waterId).getVisibleAmount());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getVisibleAmount());
		assertEquals(9, ucc.getCityStocks().getPeopleStock().getVisibleAmount());
	}
	
	// TODO second foodFallRE returns 1999999999 nanosecond, dont know if I can leave it so...
	@Test
	public void twoFoods_foodFallRE_foodFallRE_process() {
		
		// process exceeding one hour
		ucc.process(oneHourNs*2);
		
		// check city state after processing
		assertEquals(-2, ucc.getCityStocks().getPeopleStock().getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getCityFood(waterId).getSatisfaction());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getSatisfaction());
		assertEquals(-2, ucc.getCityStocks().getPeopleStock().getVisibleBirthRate());
		assertEquals(0, ucc.getCityStocks().getCityFood(waterId).getVisibleAmount());
		assertEquals(0, ucc.getCityStocks().getCityFood(riceId).getVisibleAmount());
		assertEquals(8, ucc.getCityStocks().getPeopleStock().getVisibleAmount());
	}
	
	@Test
	public void fractionVsDouble() throws Exception {
		
		double doubleA = 4/5.0d;
		double doubleB = 40/6.0d;
		Rational rA = Rational.valueOf(4, 5);
		Rational rB = Rational.valueOf(40, 6);
		long s1 = System.nanoTime();
		double res = doubleA * doubleB;
		long s2 = System.nanoTime();
		Rational rRes = rA.times(rB);
		long s3 = System.nanoTime();
		double res2 = times(doubleA, doubleB);
		long s4 = System.nanoTime();
		System.out.print("TIME DOUBLE: " + (s2 -s1) + "\n");
		System.out.print("TIME DOUBLE METHOD: " + (s4 -s3) + "\n");
		System.out.print("TIME RATIONAL: " + (s3 -s2) + "\n");
	}
	
	 double times(double a, double b) {
		return a*b;
	}
	
}
