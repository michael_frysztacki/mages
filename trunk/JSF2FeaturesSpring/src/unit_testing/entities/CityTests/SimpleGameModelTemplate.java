package unit_testing.entities.CityTests;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import database.entities.DataBaseEntityFactory;
import entities.EntityFactory;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.stocks.CityFood;
import framework.IStock;
import framework.IntegerStock;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.ICityModel;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IConvertBuildingModel;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IGameParameters;
import game_model_interfaces.IWorldDefinition;
import game_model_interfaces.initializations.ICityInitializator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import map.CityPoint;

import org.jscience.mathematics.number.Rational;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

import org.junit.experimental.runners.Enclosed;

public class SimpleGameModelTemplate {
	
	@Mock
	IWorldDefinition wd;
	
	@Mock
	ICivilization myCivil;
	@Mock
	ICityInitializator cityInit;
	@Mock
	ICityModel cityModel;
	@Mock
	IBuildingModel university;
	@Mock
	IBuildingModel spyFacility;
	@Mock
	IBuildingModel church;
	@Mock
	IExtractBuildingModel waterFactory;
	final String waterId = "waterId";
	@Mock
	IExtractBuildingModel riceFactory;
	final String riceId = "riceId";
	@Mock
	IExtractBuildingModel porkFactory;
	final String porkId = "porkId";
	@Mock
	IExtractBuildingModel ironFactory;
	final String ironId = "ironId";
	@Mock
	IExtractBuildingModel woodFactory;
	final String woodId = "woodId";
	@Mock
	IConvertBuildingModel sulFactory;
	final String sulId = "sulId";
	@Mock
	IGameParameters gameParameters;
	final String peopleId = "peopleId";
	
	@Mock
	UserConfig friko;
	final String civilId = "civilId"; 
	
	@Mock
	CityPoint cityPoint;
	
	UsersCityConfig ucc;
	long nsCityStartTime = 1000l;
	EntityFactory factory = new DataBaseEntityFactory();

	final long oneHourNs = 60l*60l*1000l*1000l*1000l;
	final long halfHourNs = oneHourNs/2;
	 
	@Before
	public void setUpTest() {
		
		mock_civil_buildings();
		mock_worldDefinition();
		when(friko.getGameParameters()).thenReturn(gameParameters);
		when(friko.getMyCivilization()).thenReturn(myCivil);
		when(friko.getMyWorldDefinition()).thenReturn(wd);
		when(friko.getCivilId()).thenReturn(civilId);
		when(gameParameters.getPeopleStockId()).thenReturn(peopleId);
		when(myCivil.getCityInitializator()).thenReturn(cityInit);
		when(myCivil.getCityModel()).thenReturn(cityModel);
	}
		private void mock_worldDefinition() {
			
			mock_FoodIds();
			mock_resourceIds();
			mock_producers();
		}
			private void mock_producers() {
				when(wd.getProducer(waterId)).thenReturn(waterFactory);
				when(wd.getProducer(riceId)).thenReturn(riceFactory);
				when(wd.getProducer(porkId)).thenReturn(porkFactory);
				when(wd.getProducer(sulId)).thenReturn(sulFactory);
				when(wd.getProducer(ironId)).thenReturn(ironFactory);
				when(wd.getProducer(woodId)).thenReturn(woodFactory);
			}
			private void mock_FoodIds() {
				List<String> allFoodIds = new ArrayList<String>();
				allFoodIds.add(riceId);
				allFoodIds.add(waterId);
				allFoodIds.add(porkId);
				allFoodIds.add(sulId);
				when(wd.getAllFoodIds()).thenReturn(allFoodIds);
			}
			private void mock_resourceIds() {
				List<String> allResourceIds = new ArrayList<String>();
				allResourceIds.add(ironId);
				allResourceIds.add(woodId);
				when(wd.getAllResourceIds()).thenReturn(allResourceIds);
			}

		protected void mock_initialCityStocks(IntegerStock... stocks) {
			List<IStock> initialStocksInCity = new ArrayList<IStock>();
			initialStocksInCity.addAll(Arrays.asList(stocks));
			when(cityInit.initialCityStocks()).thenReturn(initialStocksInCity);
		}

		private void mock_civil_buildings() {
			when(myCivil.getGameModelList(IBuildingModel.class, false, true)).thenReturn(
					Arrays.asList(
						church,
						spyFacility,
						university));
			when(myCivil.getGameModelList(IExtractBuildingModel.class, true, true)).thenReturn(
					Arrays.asList(
						riceFactory, 
						waterFactory, 
						porkFactory, 
						ironFactory, 
						sulFactory, 
						woodFactory));
		}
	
	 void mock_birthRateMultiplier() {
			when(wd.getBirthMultiplier()).thenReturn(1);
		}

		 void mockEatPerHour(int eph) {
			when(wd.getEatPerHour()).thenReturn(eph);
		}

		 void mockInitialTaxSatisfaction(int taxSatisfaction) {
			when(cityInit.initialTaxSatisfaction()).thenReturn(taxSatisfaction);
		}

		 void mockObjectsSatisfaction(int objSatisfaction) {
			when(cityModel.getSatisfactionFromObjects(ucc)).thenReturn(objSatisfaction);
		}

		 void mockFoodSatisfaction(String foodId, int satisfaction) {
			when(wd.getSatisfaction(foodId, civilId)).thenReturn(satisfaction);
		}

	private void printCity() {
		
		for(CityFood cf : ucc.getCityStocks().getFood()) {
			//System.out.print(b);
		}
	}
	
		private void assertBuildings() {
			assertEquals(3, ucc.getBuildings().size());
			for(CityBuilding cb : ucc.getBuildings())
				assertFalse(cb.isCurrentlyUpgrading());
			assertEquals(6, ucc.getExtractBuildings().size());
			for(ExtractCityBuilding ecb : ucc.getExtractBuildings())
				assertFalse(ecb.isCurrentlyUpgrading());
		}
			
		private void assertCityStocks() {
			assertEquals(4, ucc.getCityStocks().getFood().size());
			assertEquals(2, ucc.getCityStocks().getResources().size());
			assertEquals(peopleId, ucc.getCityStocks().getPeopleStock().getStockId());
			assertNotNull(ucc.getCityStocks().getTaxStock());
		}
	/*
	@Test
	public void getPeopleInCity_simple_example_free_people_in_city(){
		
		stockCh.freeHand(ucc.getCityStocks().getPeopleStock(), 100.0d); 
		assertTrue(ucc.getTrainedPeople()== 0.0d ); // upewniamy si� �e w mie�cie nie ma ludzi szkolonych
		for(ForeignArmy fa : ucc.getStationedArmies()){// upewniamy sie, �e w mie�cie nie ma armi sojuszniczych
			for (IntegerUnitGroup iug : fa.getUnits()){
				if(iug.getCount() != 0)
					fail("w mie�cie znajduj� si� armie sojusznicze");
			}
		}
		for(CityUnitGroup cug : ucc.getMyArmy().getUnits()){
			if(cug.getCount() != 0.0d)
				fail("w mie�cie znajduje si� nasza armia");
		}
		for(ExtractCityBuilding ecb : ucc.getCityBuildingsByClass(ExtractCityBuilding.class, true)){
			if(ecb.getPopulation() != 0)
				fail("w budynkach wydobywczych znajduj� si� ludzie");
		}
		assertEquals( 100.0d , ucc.getPeopleInCity() , 0.000000001d);
		
	}
	
	@Test
	public void getPeopleInCity_simple_example_working_people(){
		
		stockCh.freeHand(ucc.getCityStocks().getPeopleStock(), 100.0d);
		// 100, 2000 ,2001
		fh.setLevel(ucc.getBuilding(ExtractCityBuilding.class, 100), 1);
		fh.setLevel(ucc.getBuilding(ExtractCityBuilding.class, 2000), 1);
		fh.setLevel(ucc.getBuilding(ExtractCityBuilding.class, 2001), 1);
		
		assertTrue(ucc.getBuilding(ExtractCityBuilding.class, 100).setPeople(30));
		assertTrue(ucc.getBuilding(ExtractCityBuilding.class, 2000).setPeople(30));
		assertTrue(ucc.getBuilding(ExtractCityBuilding.class, 2001).setPeople(30));
		
		assertTrue(ucc.getTrainedPeople()== 0.0d ); // upewniamy si� �e w mie�cie nie ma ludzi szkolonych
		for(ForeignArmy fa : ucc.getStationedArmies()){// upewniamy sie, �e w mie�cie nie ma armi sojuszniczych
			for (IntegerUnitGroup iug : fa.getUnits()){
				if(iug.getCount() != 0)
					fail("w mie�cie znajduj� si� armie sojusznicze");
			}
		}
		for(CityUnitGroup cug : ucc.getMyArmy().getUnits()){
			if(cug.getCount() != 0.0d)
				fail("w mie�cie znajduje si� nasza armia");
		}
		
		assertEquals( 100.0d , ucc.getPeopleInCity() , 0.000000001d);
		assertEquals( 10.0d , ucc.getCityStocks().getPeopleStock().available() , 0.000000001d);
		assertEquals(30, ucc.getBuilding(ExtractCityBuilding.class, 100).getPopulation());
		assertEquals(30, ucc.getBuilding(ExtractCityBuilding.class, 2000).getPopulation());
		assertEquals(30, ucc.getBuilding(ExtractCityBuilding.class, 2001).getPopulation());
		
	}
	
	@Test
	public void getPeopleInCity_simple_example_trained_People() throws PayException{
		
		stockCh.freeHand(ucc.getCityStocks().getPeopleStock(), 100.0d);
		stockCh.freeHand(ucc.getCityStocks().getStockByType(StockType.wood), 2500.0d);
		// 100, 2000 ,2001
		fh.setLevel(ucc.getBuilding(CityBuilding.class, 6), 1); // w tym budynku budujemy unita.
		
		IUnitModel unit = this.ucc.getUserConfig().getMyCivilization().getById(2600, IUnitModel.class);
		assertEquals(50,  ucc.getBuilding(CityBuilding.class, 6).buildUnit(unit, 50));
		//this.unitChanger.freeHand(ucc.getMyArmy().getUnitGroupById(2500), 50);
		
		//ucc.getBuilding(CityBuilding.class, 6).getObjectBuildQueue()
		CityBuilding cb = ucc.getBuilding(CityBuilding.class, 6);
		assertEquals(1, cb.getObjectBuildQueue().getBuildsAmount());
		assertTrue( cb.getObjectBuildQueue().get(0).getClass() == UnitBuild.class);
		assertEquals(50.0d, ucc.getTrainedPeople(), 0.0001d); // upewniamy si� �e w mie�cie s� ludzie szkoleni
		for(ForeignArmy fa : ucc.getStationedArmies()){// upewniamy sie, �e w mie�cie nie ma armi sojuszniczych
			for (IntegerUnitGroup iug : fa.getUnits()){
				if(iug.getCount() != 0)
					fail("w mie�cie znajduj� si� armie sojusznicze");
			}
		}
		for(CityUnitGroup cug : ucc.getMyArmy().getUnits()){
			if(cug.getCount() != 0.0d)
				fail("w mie�cie znajduje si� nasza armia");
		}
		for(ExtractCityBuilding ecb : ucc.getCityBuildingsByClass(ExtractCityBuilding.class, true)){
			if(ecb.getPopulation() != 0)
				fail("w budynkach wydobywczych znajduj� si� ludzie");
		}
		assertEquals( 50.0d , ucc.getCityStocks().getPeopleStock().available() , 0.000000001d);
		assertEquals( 100.0d , ucc.getPeopleInCity() , 0.000000001d);
		
		assertEquals( 50.0d , ucc.getTrainedPeople() , 0.000000001d);
		
	}
	
	@Test
	public void getPeopleInCity_simple_example_army_in_city(){
		
		stockCh.freeHand(ucc.getCityStocks().getPeopleStock(), 100.0d);
		
		unitChanger.freeHand(ucc.getMyArmy().getUnitGroupById(2500), 100.0d);
		
		unitChanger.freeHand(ucc.getMyArmy().getUnitGroupById(503), 100.0d);
		
		assertTrue(ucc.getTrainedPeople()== 0.0d ); // upewniamy si� �e w mie�cie nie ma ludzi szkolonych
		for(ForeignArmy fa : ucc.getStationedArmies()){// upewniamy sie, �e w mie�cie nie ma armi sojuszniczych
			for (IntegerUnitGroup iug : fa.getUnits()){
				if(iug.getCount() != 0)
					fail("w mie�cie znajduj� si� armie sojusznicze");
			}
		}
		
		for(ExtractCityBuilding ecb : ucc.getCityBuildingsByClass(ExtractCityBuilding.class, true)){
			if(ecb.getPopulation() != 0)
				fail("w budynkach wydobywczych znajduj� si� ludzie");
		}
		assertEquals( 300.0d , ucc.getPeopleInCity() , 0.000000001d);
		
	}
	*/

}