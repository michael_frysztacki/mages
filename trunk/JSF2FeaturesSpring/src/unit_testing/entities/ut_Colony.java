package unit_testing.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;




import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_NorthEurope;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;
import unit_testing.game_model_impl.BuildingTests.TestRandomizator;


import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Civil;
import entities.Colony;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.Colony.TakeOverExtractBuildingRandomizator;
import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.buildings.FreeHand;
import entities.buildings.ExtractCityBuilding.CivilChanger;
import entities.stocks.StockChanger;
import entities.units.TripArmy;
import entities.units.UnitGroupChanger;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IConvertBuildingModel;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IResearchModel;


public class ut_Colony {

	private class BuildingCivilChanger implements CivilChanger{

		private Civil civil;
		public BuildingCivilChanger(Civil civil){
			this.civil = civil;
		}
		@Override
		public Civil changeToCivilization() {
			return civil;
		}
		
	}
	WorldDefinition ch;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	UnitGroupChanger unitChanger = new UnitGroupChanger();
	
	SpringJSFUtil su;
	UserConfig friko;
	UsersCityConfig ucc;
	@Before
	public void setUp() throws BuildModelException
	{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea() , new Config_WestEurope_City(), new Config_NorthEurope());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
		
		
	}
	
	public static class RandomizatorMock1 extends TakeOverExtractBuildingRandomizator{
		
		@Override
		public List<ExtractCityBuilding> choseExtractBuildingsToTakeOver(List<ExtractCityBuilding> allBuildings, WorldDefinition wd){
			List<ExtractCityBuilding> ret = new ArrayList<ExtractCityBuilding>();
			for(ExtractCityBuilding b : allBuildings){
				int id = b.getBuildingId();
				if(id == 8 || id==600 || id==601 ){
					ret.add(b);
				}
			}
			return ret;
		}
	}
	
	public static class RandomizatorMock2 extends TakeOverExtractBuildingRandomizator{
		
		@Override
		public List<ExtractCityBuilding> choseExtractBuildingsToTakeOver(List<ExtractCityBuilding> allBuildings, WorldDefinition wd){
			List<ExtractCityBuilding> ret = new ArrayList<ExtractCityBuilding>();
			return ret;
		}
	}
	
	public static class RandomizatorMock3 extends TakeOverExtractBuildingRandomizator{
		
		@Override
		public List<ExtractCityBuilding> choseExtractBuildingsToTakeOver(List<ExtractCityBuilding> allBuildings, WorldDefinition wd){
			List<ExtractCityBuilding> ret = new ArrayList<ExtractCityBuilding>();
			for(ExtractCityBuilding b : allBuildings){
				int id = b.getBuildingId();
				if(id == 1000 ){
					ret.add(b);
				}
			}
			return ret;
		}
	}
	
	/*
	 * Europejczyk przejmuje kolonie korea�sk�. Przej�te zostan�: 
	 * sul : id 8 (korea)
	 * 	sul improvement(mozemy robi�) : id 10
	 * 	sul improvement(nie mozemy robi�) : id 11
	 * 	sul unit(nie mo�emy robi�) : id 12
	 * fish : id 600 (common)
	 * poultry : id 601 (common)
	 * Poultry by�o nale�a�o wcze�niej do Malborku, wiec po przej�ciu powinno mie� t� cywilziacj�
	 * Sprawdzamy czy po przej�ciu tych budynk�w ustawione s� na nich odpowiednie cywilizacje.
	 * Sprawdzamy tez, czy ilosc budynk�w w mie�cie si� zgadza.
	 * Sprawdzamy czy badanie z sul house b�dzie w sul, kt�rego nie b�dziemy mogli robi� bo wymaga budynku mur, 
	 * kt�ry jest zdefiniowany w korei, a koreanskiego muru nigdy nie b�dziemy mieli.
	 */
	@Test
	public void takeOverColony_taken_over_extract_buildings(){
		
		friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		ucc = new Colony(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserConfig galan = new UserConfig("galan", "pass", ch  , Civil.korea , true, null, Language.PL);
		galan.setSpringJSFUtil(su);
		Colony galanCity = new Colony(galan, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		galanCity.getBuilding(ExtractCityBuilding.class, 601).setBuildingsCivil(new BuildingCivilChanger(Civil.Malbork_WestEurope));
		TripArmy winner = mock(TripArmy.class);
		when(winner.getOwner()).thenReturn(friko);
		IConvertBuildingModel sulHouse = ch.getCivilization(Civil.korea).getById(8, IConvertBuildingModel.class);
		assertEquals(1 , sulHouse.getUnits(galan).size());
		assertEquals(12 , sulHouse.getUnits(galan).get(0).getId() );
		assertEquals(2 , sulHouse.getResearches(galanCity).size());
		galanCity.takeOverColony(winner, new RandomizatorMock1());
		
		assertTrue(galanCity.getUserConfig()==friko);
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 8) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 1000) == null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 200) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 3) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 600) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 601) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 6) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 7) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 500) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 501) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 510) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 100) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 101) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 2000) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 2001) != null );
		
		assertEquals(Civil.korea , galanCity.getBuilding(ExtractCityBuilding.class, 8).getBuildingsCivil() );
		assertEquals(Civil.korea , galanCity.getBuilding(ExtractCityBuilding.class, 600).getBuildingsCivil() );
		assertEquals(Civil.Malbork_WestEurope , galanCity.getBuilding(ExtractCityBuilding.class, 601).getBuildingsCivil() );
		assertEquals(Civil.WestEurope , galanCity.getBuilding(ExtractCityBuilding.class, 3).getBuildingsCivil() );
		assertEquals(Civil.WestEurope , galanCity.getBuilding(ExtractCityBuilding.class, 200).getBuildingsCivil() );
		
		assertEquals(0 , sulHouse.getUnits(friko).size());
		assertEquals(1 , sulHouse.getResearches(galanCity).size());
		assertEquals(10 , sulHouse.getResearches(galanCity).get(0).getId());
		assertTrue( galanCity.getUserConfig().getMyCivilization().getById(10, IResearchModel.class) == null);
		assertTrue( galanCity.getUserConfig().getMyCivilization().getById(11, IResearchModel.class) == null);
		assertTrue( galanCity.getUserConfig().getMyCivilization().getById(12, IResearchModel.class) == null);
		
	}
	
	/*
	 * Europejczyk przejmuje kolonie korea�sk�. Nie udaje si� przej�� �adnego budynku.
	 * Sprawdzamy czy przej�te budynki maj� ustawion� cywilizacj� gracza kt�ry atakowa�.
	 * Sprawdzamy tez, czy ilosc budynk�w w mie�cie si� zgadza.
	 */
	@Test
	public void takeOverColony_taken_over_extract_buildings_no_buildings_took_over(){
		
		friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		ucc = new Colony(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserConfig galan = new UserConfig("galan", "pass", ch  , Civil.korea , true, null, Language.PL);
		galan.setSpringJSFUtil(su);
		Colony galanCity = new Colony(galan, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		galanCity.getBuilding(ExtractCityBuilding.class, 601).setBuildingsCivil(new BuildingCivilChanger(Civil.Malbork_WestEurope));
		TripArmy winner = mock(TripArmy.class);
		when(winner.getOwner()).thenReturn(friko);
		IConvertBuildingModel sulHouse = ch.getCivilization(Civil.korea).getById(8, IConvertBuildingModel.class);
		assertEquals(1 , sulHouse.getUnits(galan).size());
		assertEquals(1 , sulHouse.getUnits(galan).size());
		assertEquals(2 , sulHouse.getResearches(galanCity).size());
		galanCity.takeOverColony(winner, new RandomizatorMock2());
		
		assertTrue(galanCity.getUserConfig()==friko);
		List<CityBuilding> list = galanCity.getCityBuildingsByClass(CityBuilding.class, true);
		list.clear();
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 8) == null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 1000) == null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 200) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 3) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 600) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 601) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 6) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 7) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 500) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 501) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 510) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 100) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 101) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 2000) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 2001) != null );
		
		assertEquals(Civil.WestEurope , galanCity.getBuilding(ExtractCityBuilding.class, 3).getBuildingsCivil() );
		assertEquals(Civil.WestEurope , galanCity.getBuilding(ExtractCityBuilding.class, 200).getBuildingsCivil() );
		assertEquals(Civil.WestEurope, galanCity.getBuilding(ExtractCityBuilding.class, 100).getBuildingsCivil() );
		assertEquals(Civil.WestEurope, galanCity.getBuilding(ExtractCityBuilding.class, 101).getBuildingsCivil() );
		assertEquals(Civil.WestEurope, galanCity.getBuilding(ExtractCityBuilding.class, 2000).getBuildingsCivil() );
		
		assertEquals(0, galanCity.getBuilding(ExtractCityBuilding.class, 100).getPopulation() );
		assertEquals(0, galanCity.getBuilding(ExtractCityBuilding.class, 101).getPopulation() );
		assertEquals(0, galanCity.getBuilding(ExtractCityBuilding.class, 2000).getPopulation() );
		
		assertTrue( galanCity.getUserConfig().getMyCivilization().getById(10, IResearchModel.class) == null);
		assertTrue( galanCity.getUserConfig().getMyCivilization().getById(11, IResearchModel.class) == null);
		assertTrue( galanCity.getUserConfig().getMyCivilization().getById(12, IResearchModel.class) == null);
		
	}
	
	/*
	 * Przejmujemy budynek cywilziacji korea�skiej.
	 * Cywilziacja p�lnocno europejska i korea�ska maj� wsp�lny budynek dostawca wody, jednak w cywilziacji korea�skiej
	 * ilosc ludzi jest bLevel*people*1.5. Sprawdzamy czy po przejeciu takiego koreanskiego budynku,
	 * b�dziemy mieli wzorek na wydobycie korea�czyk�w.
	 */
	@Test
	public void takingOverColony_changed_pattern(){
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.NorthEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		UsersCityConfig frikoCity = new Colony(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserConfig galan = new UserConfig("galan", "pass", ch  , Civil.korea , true, null, Language.PL);
		galan.setSpringJSFUtil(su);
		Colony galanCity = new Colony(galan, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		TripArmy winner = mock(TripArmy.class);
		when(winner.getOwner()).thenReturn(friko);
		
		
		galanCity.takeOverColony(winner, new TestRandomizator());
		
		stockCh.freeHand(frikoCity.getCityStocks().getPeopleStock(), 100.0d);
		stockCh.freeHand(galanCity.getCityStocks().getPeopleStock(), 100.0d);
		
		fh.setLevel(frikoCity.getBuilding(ExtractCityBuilding.class, 3), 1);
		assertTrue(frikoCity.getBuilding(ExtractCityBuilding.class, 3).setPeople(100));
		
		IExtractBuildingModel dostawcaWody = friko.getMyCivilization().getById(3, IExtractBuildingModel.class);
		
		assertEquals(120, dostawcaWody.getMaxPeople(frikoCity));
		
		ExtractCityBuilding ecb = galanCity.getBuilding(ExtractCityBuilding.class, 1000);
		assertTrue(ecb.getUcc().getUserConfig() == friko);
		assertTrue(ecb.getUcc() == galanCity);
		
		fh.setLevel(galanCity.getBuilding(ExtractCityBuilding.class, 3), 1);
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 3).setPeople(100));
		
		assertEquals( 150 , dostawcaWody.getMaxPeople(galanCity) );
		
	}
	
	/*
	 * Europejczyk przejmuje kolonie korea�sk�. Udaje si� przej�� tylko farm� ry�u.
	 * Sprawdzamy czy przej�te budynki maj� ustawion� cywilizacj� gracza kt�ry atakowa�.
	 * Sprawdzamy tez, czy ilosc budynk�w w mie�cie si� zgadza.
	 */
	@Test
	public void takeOverColony_taken_over_extract_buildings_rice_farm_took_over(){
		
		friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		ucc = new Colony(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserConfig galan = new UserConfig("galan", "pass", ch  , Civil.korea , true, null, Language.PL);
		galan.setSpringJSFUtil(su);
		Colony galanCity = new Colony(galan, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		galanCity.getBuilding(ExtractCityBuilding.class, 601).setBuildingsCivil(new BuildingCivilChanger(Civil.Malbork_WestEurope));
		TripArmy winner = mock(TripArmy.class);
		when(winner.getOwner()).thenReturn(friko);
		IConvertBuildingModel sulHouse = ch.getCivilization(Civil.korea).getById(8, IConvertBuildingModel.class);
		assertEquals(1 , sulHouse.getUnits(galan).size());
		assertEquals(1 , sulHouse.getUnits(galan).size());
		assertEquals(2 , sulHouse.getResearches(galanCity).size());
		galanCity.takeOverColony(winner, new RandomizatorMock3());
		
		assertTrue(galanCity.getUserConfig()==friko);
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 8) == null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 1000) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 200) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 3) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 600) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 601) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 6) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 7) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 500) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 501) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 510) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 100) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 101) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 2000) != null );
		assertTrue(galanCity.getBuilding(ExtractCityBuilding.class, 2001) != null );
		
		assertEquals(Civil.WestEurope , galanCity.getBuilding(ExtractCityBuilding.class, 3).getBuildingsCivil() );
		assertEquals(Civil.WestEurope , galanCity.getBuilding(ExtractCityBuilding.class, 200).getBuildingsCivil() );
		assertEquals(Civil.WestEurope, galanCity.getBuilding(ExtractCityBuilding.class, 100).getBuildingsCivil() );
		assertEquals(Civil.WestEurope, galanCity.getBuilding(ExtractCityBuilding.class, 101).getBuildingsCivil() );
		assertEquals(Civil.WestEurope, galanCity.getBuilding(ExtractCityBuilding.class, 2000).getBuildingsCivil() );
		assertEquals(Civil.korea, galanCity.getBuilding(ExtractCityBuilding.class, 1000).getBuildingsCivil() );
		
		assertTrue( galanCity.getUserConfig().getMyCivilization().getById(10, IResearchModel.class) == null);
		assertTrue( galanCity.getUserConfig().getMyCivilization().getById(11, IResearchModel.class) == null);
		assertTrue( galanCity.getUserConfig().getMyCivilization().getById(12, IResearchModel.class) == null);
		
	}
	
	@Ignore
	@Test
	public void test(){
		fail("przejmowanie koloni nie jest do konca przetestowane...");
	}
	
}

