package unit_testing.entities;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;




import org.junit.Before;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Capital;
import entities.Civil;
import entities.Colony;
import entities.FreeHandForReasearch;
import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.FreeHand;
import entities.stocks.CityStockSet;
import entities.stocks.StockChanger;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.PriceStockSetImpl;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import game_model_impl.PriceStockSetImpl.PayException;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IResearchModel;
import game_model_interfaces.PriceStock;
import game_model_interfaces.TaxPriceStock;



public class ut_PriceStockSet {

	WorldDefinition ch;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	
	SpringJSFUtil su;
	UserConfig uc;
	UsersCityConfig ucc;
	@Before
	public void setUp() throws BuildModelException
	{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea() , new Config_WestEurope_City());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
		uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
	}
	
	@Test
	public void test_PriceStock_copy_constructor()
	{
		PriceStock ps = new PriceStock(StockType.water, 2000);
		PriceStock copy = ps.clone();
		assertTrue(copy.getVisibleAmount() == 2000);
		assertTrue(copy.getStockType() == StockType.water);

	}
	@Test
	public void test_PriceStockSet_copy_constructor()
	{
		PriceStockSetImpl pss = new PriceStockSetImpl( new PriceStock(StockType.wood, 200), new PriceStock(StockType.stone, 500));
		PriceStockSetImpl copy = new PriceStockSetImpl(pss, 2.0d);
		PriceStockSetImpl nextCopy = new PriceStockSetImpl(copy, 2.0d);
		assertTrue(pss.getStockByType(StockType.wood, null).getVisibleAmount() == 200);
		assertTrue(pss.getStockByType(StockType.wood, null).getStockType() == StockType.wood);
		
		assertTrue(pss.getStockByType(StockType.stone, null).getVisibleAmount() == 500);
		assertTrue(pss.getStockByType(StockType.stone, null).getStockType() == StockType.stone);
		
		assertTrue(copy.getStockByType(StockType.wood, null).getVisibleAmount() == 400);
		assertTrue(copy.getStockByType(StockType.wood, null).getStockType() == StockType.wood);
		
		assertTrue(copy.getStockByType(StockType.stone, null).getVisibleAmount() == 1000);
		assertTrue(copy.getStockByType(StockType.stone, null).getStockType() == StockType.stone);
		
		assertTrue(nextCopy.getStockByType(StockType.wood, null).getVisibleAmount() == 800);
		assertTrue(nextCopy.getStockByType(StockType.wood, null).getStockType() == StockType.wood);
		
		assertTrue(nextCopy.getStockByType(StockType.stone, null).getVisibleAmount() == 2000);
		assertTrue(nextCopy.getStockByType(StockType.stone, null).getStockType() == StockType.stone);
		
		assertTrue(pss.getStockByType(StockType.wood, null).getVisibleAmount() == 200);
		assertTrue(pss.getStockByType(StockType.wood, null).getStockType() == StockType.wood);
		
		assertTrue(pss.getStockByType(StockType.stone, null).getVisibleAmount() == 500);
		assertTrue(pss.getStockByType(StockType.stone, null).getStockType() == StockType.stone);
		

	}
	
	/*
	 * Mamy zdefiniowane badanie tier w cywilizacji og�lnej, za kt�re trzeba zap�aci� podatek.
	 * Ka�da cywilizacja ma inny surowiec podatkowy, wiec w ka�dej cywilizacji cena tiera b�dzie si� r�ni� rodzajem
	 * p�aconego surowca. Test sprawdza, czy w cywilizacji koreanskiej tier kosztuje srebro, a w cywilizacji europejskiej - z�oto.
	 */
	@Test
	public void test_PriceStockSet_with_Tax()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserConfig galan = new UserConfig("galan", "pass", ch  , Civil.korea, true, null, Language.PL);
		galan.setSpringJSFUtil(su);
		Capital galanCapital = new Capital(galan, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);

		UsersCityConfig westEurope = ch.getCivilization(Civil.WestEurope);
		UsersCityConfig korea = ch.getCivilization(Civil.korea);
		
		IResearchModel tier = westEurope.getById(0, IResearchModel.class);
		PriceStockSetImpl tierPrice = tier.getPrice(frikoCapital);
		
		assertTrue(tierPrice.getStockByType(StockType.gold,westEurope).getVisibleAmount() == 1200);
		
		tierPrice = tier.getPrice(galanCapital);
		
		assertTrue(tierPrice.getStockByType(StockType.silver,korea).getVisibleAmount() == 1200);
		
		
	}
	
	/*
	 *W tym te�cie PriceStockSet zawiera cene typu tax, sprawdzimy czy dla gracza Europ� Zachodni�
	 */
	@Test
	public void test_PriceStockdd_copy_constructor()
	{
		
	}
	
	@Test
	public void test_PriceStock_doPay_unsuficcient_stocks() throws PayException
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityStockSet cityStocks = frikoCapital.getCityStocks();
		
		stockCh.freeHand(cityStocks.getStockByType(StockType.gold), 100);
		stockCh.freeHand(cityStocks.getStockByType(StockType.wood), 100);
		stockCh.freeHand(cityStocks.getStockByType(StockType.iron), 100);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new PriceStock(StockType.gold,300),
				new PriceStock(StockType.wood,200),
				new PriceStock(StockType.iron,500)
							 );
		try{
			price.doPay(cityStocks);
			fail("doPay powinien zwr�ci� wyj�tek");
		}
		catch(PayException e){};
		assertEquals(100.0d , cityStocks.getStockByType(StockType.gold).getVisibleAmount() , 0.00000001);
		assertEquals(100.0d , cityStocks.getStockByType(StockType.wood).getVisibleAmount()  ,0.00000001);
		assertEquals(100.0d , cityStocks.getStockByType(StockType.iron).getVisibleAmount() , 0.00000001);
		
	}
	
	@Test
	public void test_PriceStock_doPay_one_unsufficient_stock() throws PayException
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityStockSet cityStocks = frikoCapital.getCityStocks();
		
		stockCh.freeHand(cityStocks.getStockByType(StockType.gold), 400);
		stockCh.freeHand(cityStocks.getStockByType(StockType.wood), 300);
		stockCh.freeHand(cityStocks.getStockByType(StockType.iron), 100);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new PriceStock(StockType.gold,300),
				new PriceStock(StockType.wood,200),
				new PriceStock(StockType.iron,500)
							 );
		try{
			price.doPay(cityStocks);
			fail("doPay powinien zwr�ci� wyj�tek");
		}
		catch(PayException e){};
		
		assertEquals(400.0d , cityStocks.getStockByType(StockType.gold).getVisibleAmount(),0.000001d);
		assertEquals(300.0d , cityStocks.getStockByType(StockType.wood).getVisibleAmount() ,0.000001d);
		assertEquals(100.0d , cityStocks.getStockByType(StockType.iron).getVisibleAmount(),0.000001d);
		
	}
	
	@Test
	public void test_PriceStock_doPay_wood_unsufficient_stock() throws PayException
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityStockSet cityStocks = frikoCapital.getCityStocks();
		
		stockCh.freeHand(cityStocks.getStockByType(StockType.gold), 400);
		stockCh.freeHand(cityStocks.getStockByType(StockType.wood), 100);
		stockCh.freeHand(cityStocks.getStockByType(StockType.iron), 600);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new PriceStock(StockType.gold,300),
				new PriceStock(StockType.wood,200),
				new PriceStock(StockType.iron,500)
							 );
		try{
			price.doPay(cityStocks);
			fail("doPay powinien zwr�ci� wyj�tek");
		}
		catch(PayException e){};
		
		assertEquals(400.0d , cityStocks.getStockByType(StockType.gold).getVisibleAmount() , 0.000001d);
		assertEquals(100.0d , cityStocks.getStockByType(StockType.wood).getVisibleAmount() , 0.000001d);
		assertEquals(600.0d , cityStocks.getStockByType(StockType.iron).getVisibleAmount() ,  0.000001d);
		
	}
	
	@Test
	public void test_PriceStock_doPay_no_stocks()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityStockSet cityStocks = frikoCapital.getCityStocks();
		
		stockCh.freeHand(cityStocks.getStockByType(StockType.gold), 0);
		stockCh.freeHand(cityStocks.getStockByType(StockType.wood), 0);
		stockCh.freeHand(cityStocks.getStockByType(StockType.iron), 0);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new PriceStock(StockType.gold,300),
				new PriceStock(StockType.wood,200),
				new PriceStock(StockType.iron,500)
							 );

		try{
			price.doPay(cityStocks);
			fail("doPay powinien zwr�ci� wyj�tek");
		}
		catch(PayException e){};
		

		assertEquals(0.0d , cityStocks.getStockByType(StockType.gold).getVisibleAmount() , 0.00000001d);
		assertEquals(0.0d , cityStocks.getStockByType(StockType.wood).getVisibleAmount(), 0.00000001d );
		assertEquals(0.0d , cityStocks.getStockByType(StockType.iron).getVisibleAmount(), 0.00000001d );
		
	}
	
	@Test
	public void test_PriceStock_doPay_exact_suffiecient_stocks()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityStockSet cityStocks = frikoCapital.getCityStocks();
		
		stockCh.freeHand(cityStocks.getStockByType(StockType.gold), 300);
		stockCh.freeHand(cityStocks.getStockByType(StockType.wood), 200);
		stockCh.freeHand(cityStocks.getStockByType(StockType.iron), 500);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new PriceStock(StockType.gold,300),
				new PriceStock(StockType.wood,200),
				new PriceStock(StockType.iron,500)
							 );
		try{
			price.doPay(cityStocks);
		}
		catch(PayException e){
			fail("doPay nie powinien zwr�ci� wyj�tku");
		};
		
		assertEquals(0.0d , cityStocks.getStockByType(StockType.gold).getVisibleAmount() , 0.00000001 );
		assertEquals(0.0d , cityStocks.getStockByType(StockType.wood).getVisibleAmount() , 0.00000001);
		assertEquals(0.0d , cityStocks.getStockByType(StockType.iron).getVisibleAmount(), 0.00000001);
		
	}
	
	@Test
	public void test_PriceStock_doPay_suffiecient_stocks()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityStockSet cityStocks = frikoCapital.getCityStocks();
		
		stockCh.freeHand(cityStocks.getStockByType(StockType.gold), 1000);
		stockCh.freeHand(cityStocks.getStockByType(StockType.wood), 1000);
		stockCh.freeHand(cityStocks.getStockByType(StockType.iron), 1000);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new PriceStock(StockType.gold,300),
				new PriceStock(StockType.wood,200),
				new PriceStock(StockType.iron,500)
							 );
		try{
			price.doPay(cityStocks);
		}
		catch(PayException e){
			fail("doPay nie powinien zwr�ci� wyj�tku");
		};
		
		assertEquals(700.0d , cityStocks.getStockByType(StockType.gold).getVisibleAmount() , 0.00000001 );
		assertEquals(800.0d , cityStocks.getStockByType(StockType.wood).getVisibleAmount() , 0.00000001);
		assertEquals(500.0d , cityStocks.getStockByType(StockType.iron).getVisibleAmount(), 0.00000001);
		
	}
	
	@Test
	public void test_PriceStock_doPay_unsuffiecient_taxstock()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityStockSet cityStocks = frikoCapital.getCityStocks();
		
		stockCh.freeHand(cityStocks.getStockByType(StockType.gold), 200);
		stockCh.freeHand(cityStocks.getStockByType(StockType.wood), 1000);
		stockCh.freeHand(cityStocks.getStockByType(StockType.iron), 1000);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new TaxPriceStock(300),
				new PriceStock(StockType.wood,200),
				new PriceStock(StockType.iron,500)
							 );
		try{
			price.doPay(cityStocks);
			fail("doPay powinien zwr�ci� wyj�tek");
		}
		catch(PayException e){
			
		};
		
		assertEquals(200.0d , cityStocks.getStockByType(StockType.gold).getVisibleAmount() , 0.00000001 );
		assertEquals(1000.0d , cityStocks.getStockByType(StockType.wood).getVisibleAmount() , 0.00000001);
		assertEquals(1000.0d , cityStocks.getStockByType(StockType.iron).getVisibleAmount(), 0.00000001);
		
	}
	
	@Test
	public void test_PriceStock_doPay_suffiecient_taxstock()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityStockSet cityStocks = frikoCapital.getCityStocks();
		
		stockCh.freeHand(cityStocks.getStockByType(StockType.gold), 400);
		stockCh.freeHand(cityStocks.getStockByType(StockType.wood), 1000);
		stockCh.freeHand(cityStocks.getStockByType(StockType.iron), 1000);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new TaxPriceStock(300),
				new PriceStock(StockType.wood,200),
				new PriceStock(StockType.iron,500)
							 );
		try{
			price.doPay(cityStocks);
		}
		catch(PayException e){
			fail("doPay nie powinien zwr�ci� wyj�tek");
		};
		
		assertEquals(100.0d , cityStocks.getStockByType(StockType.gold).getVisibleAmount() , 0.00000001 );
		assertEquals(800.0d , cityStocks.getStockByType(StockType.wood).getVisibleAmount() , 0.00000001);
		assertEquals(500.0d , cityStocks.getStockByType(StockType.iron).getVisibleAmount(), 0.00000001);
		
	}
	
	@Test
	public void test_PriceStock_doPay_exact_suffiecient_taxstock()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityStockSet cityStocks = frikoCapital.getCityStocks();
		
		stockCh.freeHand(cityStocks.getStockByType(StockType.gold), 300);
		stockCh.freeHand(cityStocks.getStockByType(StockType.wood), 200);
		stockCh.freeHand(cityStocks.getStockByType(StockType.iron), 500);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new TaxPriceStock(300),
				new PriceStock(StockType.wood,200),
				new PriceStock(StockType.iron,500)
							 );
		try{
			price.doPay(cityStocks);
			
		}
		catch(PayException e){
			fail("doPay nie powinien zwr�ci� wyj�tek");
		};
		
		assertEquals(0.0d , cityStocks.getStockByType(StockType.gold).getVisibleAmount() , 0.00000001 );
		assertEquals(0.0d , cityStocks.getStockByType(StockType.wood).getVisibleAmount() , 0.00000001);
		assertEquals(0.0d , cityStocks.getStockByType(StockType.iron).getVisibleAmount(), 0.00000001);
		
	}
	
	@Test
	public void test_PriceStock_doPay_unsuffiecient_silvertaxstock()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.korea, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityStockSet cityStocks = frikoCapital.getCityStocks();
		
		stockCh.freeHand(cityStocks.getStockByType(StockType.gold), 300);
		stockCh.freeHand(cityStocks.getStockByType(StockType.wood), 200);
		stockCh.freeHand(cityStocks.getStockByType(StockType.iron), 500);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new TaxPriceStock(300),
				new PriceStock(StockType.wood,200),
				new PriceStock(StockType.iron,500)
							 );
		try{
			price.doPay(cityStocks);
			fail("doPay powinien zwr�ci� wyj�tek");
		}
		catch(PayException e){
			
		};
		
		assertEquals(300.0d , cityStocks.getStockByType(StockType.gold).getVisibleAmount() , 0.00000001 );
		assertEquals(200.0d , cityStocks.getStockByType(StockType.wood).getVisibleAmount() , 0.00000001);
		assertEquals(500.0d , cityStocks.getStockByType(StockType.iron).getVisibleAmount(), 0.00000001);
		
	}
	
	@Test
	public void test_PriceStock_doPay_suffiecient_silvertaxstock()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.korea, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityStockSet cityStocks = frikoCapital.getCityStocks();
		
		stockCh.freeHand(cityStocks.getStockByType(StockType.silver), 300);
		stockCh.freeHand(cityStocks.getStockByType(StockType.gold), 200);
		stockCh.freeHand(cityStocks.getStockByType(StockType.wood), 200);
		stockCh.freeHand(cityStocks.getStockByType(StockType.iron), 500);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new TaxPriceStock(300),
				new PriceStock(StockType.wood,200),
				new PriceStock(StockType.iron,500)
							 );
		try{
			price.doPay(cityStocks);
			
		}
		catch(PayException e){
			fail("doPay nie powinien zwr�ci� wyj�tek");
		};
		
		assertEquals(0.0d , cityStocks.getStockByType(StockType.silver).getVisibleAmount() , 0.00000001 );
		assertEquals(200.0d , cityStocks.getStockByType(StockType.gold).getVisibleAmount() , 0.00000001 );
		assertEquals(0.0d , cityStocks.getStockByType(StockType.wood).getVisibleAmount() , 0.00000001);
		assertEquals(0.0d , cityStocks.getStockByType(StockType.iron).getVisibleAmount(), 0.00000001);
		
	}
	
	@Test
	public void test_PriceStock_doPay_suffiecient_only_one_goldtaxstock()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		CityStockSet cityStocks = frikoCapital.getCityStocks();
		
		stockCh.freeHand(cityStocks.getStockByType(StockType.gold), 200);

		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new TaxPriceStock(200)
							 );
		try{
			price.doPay(cityStocks);
			
		}
		catch(PayException e){
			fail("doPay nie powinien zwr�ci� wyj�tek");
		};
		
		assertEquals(0.0d , cityStocks.getStockByType(StockType.gold).getVisibleAmount() , 0.00000001 );
		
	}
	
	@Test
	public void test_PriceStock_getTaxStock_for_Europe()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new TaxPriceStock(200)
							 );
		PriceStock ps = price.getStockByType(StockType.gold, friko.getMyCivilization());
		assertEquals(StockType.gold, ps.getStockType());
		assertEquals(new Integer(200), ps.getVisibleAmount());
		
	}
	
	@Test
	public void test_PriceStock_getTaxStock_for_korea()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.korea, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new TaxPriceStock(200)
							 );
		PriceStock ps = price.getStockByType(StockType.silver, friko.getMyCivilization());
		assertTrue(ps != null);
		assertEquals(StockType.silver, ps.getStockType());
		assertEquals(new Integer(200), ps.getVisibleAmount());
		
	}
	
	@Test
	public void test_PriceStock_getTaxStock_for_Malbork()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.Malbork_WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new TaxPriceStock(200)
							 );
		PriceStock ps = price.getStockByType(StockType.gold, friko.getMyCivilization());
		assertTrue(ps != null);
		assertEquals(StockType.gold, ps.getStockType());
		assertEquals(new Integer(200), ps.getVisibleAmount());
		
	}
	
	@Test
	public void test_PriceStock_slaves_as_price()
	{
		UserConfig friko = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		friko.setSpringJSFUtil(su);
		Capital frikoCapital = new Capital(friko, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		PriceStockSetImpl price = new PriceStockSetImpl(
				new TaxPriceStock(200),
				new PriceStock(StockType.slaves, 100)
							 );
		
		CityStockSet cityStocks = frikoCapital.getCityStocks();
		stockCh.freeHand(cityStocks.getStockByType(StockType.gold), 200);
		stockCh.freeHand(cityStocks.getStockByType(StockType.slaves), 100);
		try{
			price.doPay(cityStocks);
		}
		catch(PayException e){
			fail("doPay nie powinien zwr�ci� wyj�tek");
		};
		
		assertEquals(0.0d , cityStocks.getStockByType(StockType.gold).getVisibleAmount() , 0.00000001);
		assertEquals(0.0d , cityStocks.getStockByType(StockType.slaves).getVisibleAmount(), 0.00000001);
		
	}
	
}

