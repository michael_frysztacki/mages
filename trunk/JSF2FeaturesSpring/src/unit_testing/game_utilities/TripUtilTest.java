package unit_testing.game_utilities;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import entities.Civil;
import entities.Colony;
import entities.FreeHandForReasearch;
import entities.Mission;
import entities.Point;
import entities.Trip;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.FreeHand;
import entities.stocks.StockChanger;
import entities.units.IUnitGroup;
import entities.units.IntegerUnitGroup;
import framework.IntegerStock;
import game_model_impl.AllWorlds;
import game_model_impl.BuildModelException;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IHousesModel;
import game_utilities.TripUtil;
import game_utilities.TripUtil.NotEnoughCapacityForFood;




import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import testing_component.world_definition_for_testing.Config_Common;
import testing_component.world_definition_for_testing.Config_Korea;
import testing_component.world_definition_for_testing.Config_Korea_City;
import testing_component.world_definition_for_testing.Config_WestEurope;
import testing_component.world_definition_for_testing.Config_WestEurope_City;
import testing_component.world_definition_for_testing.PointInitializator;
import testing_component.world_definition_for_testing.WorldParameters_Config;

import com.jsfsample.managedbeans.SpringJSFUtil;


/*
 * 
 */
public class TripUtilTest {

	WorldDefinition ch;
	UsersCityConfig ucc;
	FreeHand fh = new FreeHand();
	FreeHandForReasearch fhr = new FreeHandForReasearch();
	StockChanger stockCh = new StockChanger();
	
	SpringJSFUtil su;
	@Before
	public void setUp() throws BuildModelException
	{
		su = mock(SpringJSFUtil.class);
		WorldDefinition wd = new WorldDefinition(0,new Config_Common(),new WorldParameters_Config(),
				new Config_WestEurope(), new Config_Korea(), new Config_Korea_City(), new Config_WestEurope_City());
		ch = wd;
		AllWorlds aw = new AllWorlds(wd);
		when(su.getAllWorlds()).thenReturn(aw);
		
	}
	
	public class TripUnitGroupForTesting implements IUnitGroup<Integer>
	{
		int unitId, amount;
		public TripUnitGroupForTesting(int unitId, int amount)
		{
			this.amount = amount;
			this.unitId = unitId;
		}
		@Override
		public int getUnitId() {
			
			return unitId;
		}

		@Override
		public Integer getCount() {
			
			return amount;
		}
	}
	/*
	 * test sprawdzaj�cy klase TripUtil. Je�li jakim� unitom brakuje miejsca na jedzenie, bo podr�z jest zbyt d�uga,
	 * inne unity posiadaj�ce wi�ksz� pojemno�� mog� pom�c mniejszej jednostke w niesieniu jedzenia. Innymi s�owy,
	 * miejsce na jedzenie podr�uj�cej armi jest sumowane przez wszystkie jednostki. Je�li po bitwie wi�ksze jendostki gin�,
	 * trac�cyc tym samym zapasy po�ywienia, ca�a armia mo�e normalnie powr�ci� do domu. 
	 * 
	 * unit id 4 zjada 1/h , capacity: 10
	 * 
	 * wysy�amy 10 unit�w na wyprawe kt�ra liczy 11 godzin. armia ma za ma�o pojemno�ci, �eby zabra� ze sob� wystarczaj�c� ilo�� jedzenia.
	 * 
	 */
	@Ignore
	@Test( expected = NotEnoughCapacityForFood.class )
	public void test_TripUtil_sending_army_with_lacking_space_for_food() throws NotEnoughCapacityForFood
	{
		TripUtil tu = new TripUtil();
		
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserConfig uc2 = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc2.setSpringJSFUtil(su);
		UsersCityConfig ucc2 = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		Set<IntegerStock> stocksForTrip = new HashSet<IntegerStock>();
		
		Set<TripUnitGroupForTesting> unitsForTrip = new HashSet<TripUnitGroupForTesting>();
		TripUnitGroupForTesting ug = new TripUnitGroupForTesting(4, 10);
		unitsForTrip.add(ug);
		
		tu.sendTrip(stocksForTrip, unitsForTrip, ucc , ucc2, Mission.robbery, 11*60*60*1000 );

	}
	
	/* 
	 * test sprawdzaj�cy klase TripUtil. Je�li jakim� unitom brakuje miejsca na jedzenie, bo podr�z jest zbyt d�uga,
	 * inne unity posiadaj�ce wi�ksz� pojemno�� mog� pom�c mniejszej jednostke w niesieniu jedzenia. Innymi s�owy,
	 * miejsce na jedzenie podr�uj�cej armi jest sumowane przez wszystkie jednostki. Je�li po bitwie wi�ksze jendostki gin�,
	 * trac�cyc tym samym zapasy po�ywienia, ca�a armia mo�e normalnie powr�ci� do domu. 
	 * 
	 * unit id 4 zjada 1/h , capacity: 10
	 * unit id 15 zjada 1/h , capacity: 100
	 * wysy�amy 10 unit�w o (id 4) na wyprawe kt�ra liczy 11 godzin. 
	 * Do tego wysy�amy jednego unita, kt�ry ma pojemno�� 100 i zrekompensuje armii ca�kowit� pojemno�� na jedzenie.
	 * 
	 * 
	 */
	@Ignore
	@Test
	public void test_TripUtil_sending_army_with_support_for_bearing_food() throws NotEnoughCapacityForFood
	{
		TripUtil tu = new TripUtil();
		
		UserConfig uc = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc.setSpringJSFUtil(su);
		UsersCityConfig ucc = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		UserConfig uc2 = new UserConfig("friko", "pass", ch  , Civil.WestEurope, true, null, Language.PL);
		uc2.setSpringJSFUtil(su);
		UsersCityConfig ucc2 = new Colony(uc, new Point(new PointInitializator()), System.currentTimeMillis()*1000*1000);
		
		Set<IntegerStock> stocksForTrip = new HashSet<IntegerStock>();
		
		Set<TripUnitGroupForTesting> unitsForTrip = new HashSet<TripUnitGroupForTesting>();
		TripUnitGroupForTesting ug = new TripUnitGroupForTesting(4, 10);
		TripUnitGroupForTesting ug2 = new TripUnitGroupForTesting(15, 1);
		unitsForTrip.add(ug);
		unitsForTrip.add(ug2);
		Trip trip = tu.sendTrip(stocksForTrip, unitsForTrip, ucc , ucc2, Mission.robbery, 11*60*60*1000 );
		assertTrue(trip != null);
	}
	
}
