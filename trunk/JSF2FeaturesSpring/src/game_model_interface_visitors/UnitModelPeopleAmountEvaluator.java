package game_model_interface_visitors;

import entities.UsersCityConfig;
import game_model_impl.PriceStockSetImpl;
import game_model_impl.StockType;
import game_model_interfaces.IUnitModel;
import game_model_interfaces.PriceStock;

public class UnitModelPeopleAmountEvaluator {

	private IUnitModel unitModel;
	private UsersCityConfig myCity;
	private int peopleInUnit;
	public UnitModelPeopleAmountEvaluator(IUnitModel unitModel, UsersCityConfig myCity){
		this.unitModel = unitModel;
		this.myCity = myCity;
		PriceStockSetImpl price = getPriceOfUnitModel();
		PriceStock people = price.getStockByType(StockType.peopleCount, myCity);
		peopleInUnit = people == null ? 0 : people.getVisibleAmount();
		
	}
		private PriceStockSetImpl getPriceOfUnitModel() {
			return unitModel.getPrice( myCity );
		}
	
	public double getPeopleAmountInUnit() {
		
	}
		
	
	public boolean unitModelContainsPeople() {
		
	}
	
	
	
	

}
