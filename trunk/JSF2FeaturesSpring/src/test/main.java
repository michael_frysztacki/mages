package test;

import initializations.DefaultCityInitializator;

import java.lang.reflect.InvocationTargetException;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import junittest.modeltest.TestCityInitializator;
import junittest.old_tests.Testing;

import model.Building;
import model.CivilizationHolder;
import model.ConvertBuilding;
import model.ExtractBuilding;
import model.Research;
import model.Unit;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;


import economycomputation.CityCompUtil;

import pojo.Capital;
import pojo.Civil;
import pojo.Point;
import pojo.Role;
import pojo.UserConfig;
import pojo.UserResearch;
import pojo.UsersCityConfig;
import pojo.buildings.CityBuilding;
import pojo.buildings.ConvertCityBuilding;
import pojo.buildings.ExtractCityBuilding;
import pojo.stocks.CityStockSet;
import pojo.stocks.CityResource;
import pojo.stocks.StockType;
import pojo.units.CityArmy;
import pojo.units.CityUnitGroup;
import services.CityService;
import services.LoginService;

@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext.xml")
@Component
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class main{

	/**
	 * @param args
	 * @throws NoSuchFieldException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws NoSuchMethodException 
	 * @throws IllegalArgumentException 
	 * @throws SecurityException 
	 */
	@Autowired CityService cityService;
	@Autowired CivilizationHolder ch;
	@Autowired CityCompUtil ccu;
	@Autowired LoginService loginService;
	public static void main(String[] args) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("file:WebContent/WEB-INF/applicationContext-test.xml"); //get Spring context
		 
		main m = ctx.getBean(main.class); 
		m.register();
	}
	
	public void register() throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	{
		System.out.print("rejestracja");
		long msNow = System.currentTimeMillis();
		UserConfig friko = loginService.register("friko", "friko", Civil.korea, msNow*1000l*1000l, new DefaultCityInitializator(), new  );
	}

}
