package junittest_spring_hibernate;

import java.lang.reflect.InvocationTargetException;

import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;
import game_model_impl.StockType;
import game_model_interfaces.initializations.ICityInitializator;


public class TestVictimCityInitializer implements ICityInitializator{

	@Override
	public UsersCityConfig initCity(UsersCityConfig ucc)
			throws SecurityException, IllegalArgumentException,
			NoSuchMethodException, IllegalAccessException,
			InvocationTargetException, NoSuchFieldException {
		// TODO Auto-generated method stub
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(1000.0d);
		ucc.setMaxLudzi(1000);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//water
		ecb.setLevel(5);
		ecb.setPeople(500);
		
		//ucc.getArmy().getUnitGroupById(10).setCount(100.0d);
		
		return ucc;
	}

}
