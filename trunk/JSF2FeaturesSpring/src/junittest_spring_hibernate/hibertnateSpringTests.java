package junittest_spring_hibernate;

import static org.junit.Assert.*;

import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import game_model_interfaces.initializations.ICityInitializator;
import game_model_interfaces.initializations.TestPointInitializator;
import game_utilities.TripUtil;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;



import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.transaction.AfterTransaction;



import dao.UserConfigHomeImpl;
import dao.UsersCityConfigHome;
import dto.ConcreteFoodDTO;
import dto.ConcreteResourceDTO;
import dto.DetailedUnitDTO;
import dto.UnitDTO;
import entities.Civil;
import entities.Mission;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;


import services.BuildingService;
import services.CityService;
import services.EconomyService;
import services.LoginService;
import services.ResearchService;
import services.TripService;
import services.UnitService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext-test.xml")
//@TransactionConfiguration(transactionManager="transactionManager", defaultRollback=false)
//@Transactional
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class hibertnateSpringTests implements ICityInitializator{

	
	public static int uccId;
	public static int targetId;
	public static UserConfig friko;
	public static UserConfig galan;
	@Autowired private UsersCityConfigHome cityDao;
	@Autowired private CityService cityService;
	@Autowired private UserConfigHomeImpl userDao;
	@Autowired private EconomyService es;
	@Autowired private TripService tripService;
	@Autowired private TripUtil tripUtil;
	@Autowired private LoginService ls;
	@Autowired private WorldDefinition ch;
	@Autowired private LoginService loginService;
	@Autowired private BuildingService buildingService;
	@Autowired private UnitService unitService;
	@Autowired private ResearchService researchService;
	private long nsNow;
	private final long msTravelTime = 60*1000;// 1 min


	@Override
	public UsersCityConfig initCity(UsersCityConfig ucc)
			throws SecurityException, IllegalArgumentException,
			NoSuchMethodException, IllegalAccessException,
			InvocationTargetException, NoSuchFieldException {
		// TODO Auto-generated method stub
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(1000.0d);
		ucc.setMaxLudzi(1000);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//water
		ecb.setLevel(3);
		ecb.setPeople(200);
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(3);
		ecb.setPeople(200);
		
		ucc.getMyResources().getResource(StockType.gold).setCount(1200.0d);
		ucc.getMyResources().getResource(StockType.wood).setCount(300.0d);
		
		ucc.getArmy().getUnitGroupById(10).setCount(10.0d);
		
		return ucc;
	}
	
    @Before
    public void setUpTestDataWithinTransaction() throws Exception{
        // set up test data within the transaction
    	
    	long msNow = System.currentTimeMillis();
		nsNow = msNow*1000l*1000l;
		
		UserConfig friko = loginService.register("friko", "friko", Civil.korea, nsNow, this, new TestPointInitializator());
		if(friko == null)friko = loginService.findByName("friko");
		Set<UsersCityConfig> frikoCities =  cityService.getCities(friko);
		uccId = frikoCities.iterator().next().getId();
		UserConfig galan = loginService.register("galan", "galan", Civil.korea, nsNow, new TestVictimCityInitializer(), new TestPointInitializator());
		if(galan == null)galan = loginService.findByName("galan");
		Set<UsersCityConfig> galanCities =  cityService.getCities(galan);
		targetId = galanCities.iterator().next().getId();
		
		//UsersCityConfig ucc = es.getEconomy(uccId, msNow);
		
    }
    
    @Test
    // testowanie budowania bada� i jednostek
    @Rollback(true)
    
    public void researchUpgradeTest2() throws Exception
    {
    	long msNow = nsNow/1000l/1000l + 1000l*60l*60l*24l; // po 24 h
        UsersCityConfig ucc = es.getEconomy(uccId, msNow);
        
        assertTrue(buildingService.upgradeBuilding(5, ucc.getId(), msNow));
        
        ucc = es.getEconomy(uccId, msNow);
        
        assertTrue(ucc.getMyResources().getResource(StockType.gold).getVisibleAmount() == 200.0d);
        assertTrue(ucc.getMyResources().getResource(StockType.wood).getVisibleAmount() == 0.0d);
        
        ucc = es.getEconomy(uccId, msNow + 4000);
        assertTrue(ucc.getBuilding(5).getLevel() == 0);
        ucc = es.getEconomy(uccId, msNow + 6000);
        assertTrue(ucc.getBuilding(5).getLevel() == 1);
        
        msNow += 6000;
        DetailedUnitDTO unitDto = new DetailedUnitDTO(null, null, 0, 10); // (id = 10);
        unitDto.setBuildCount(1); //budujemy dw�ch w�ocznik�w 
        assertTrue(unitService.buildArmy(unitDto, uccId, msNow) == 1);
        
        ucc = es.getEconomy(uccId, msNow + 4000);
        
        assertTrue(ucc.getMyResources().getResource(StockType.gold).getVisibleAmount() == 100.0d);
        assertTrue(ucc.getMyResources().getResource(StockType.wood).getVisibleAmount() == 0.0d);
        assertTrue(ucc.getUserConfig().getResearch(1).getLevel() == 0 );
        
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() > 10.0d && ucc.getArmy().getUnitGroupById(10).getVisibleAmount() < 11.0d);
        
        assertTrue(researchService.upgradeResearch(1, uccId, msNow + 4000));
        assertTrue(unitService.buildArmy(unitDto, uccId, msNow + 4000) == 1);
        msNow += 4000;
        
        ucc = es.getEconomy(uccId, msNow + 800);
        
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() > 10.0d && ucc.getArmy().getUnitGroupById(10).getVisibleAmount() < 11.0d);
        assertTrue(ucc.getUserConfig().getResearch(1).getLevel() == 0 );
        
        ucc = es.getEconomy(uccId, msNow + 1000);
        
        assertTrue(ucc.getMyResources().getResource(StockType.gold).getVisibleAmount() == 0.0d);
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 11.0d);
        assertTrue(ucc.getUserConfig().getResearch(1).getLevel() == 0 );
        
        ucc = es.getEconomy(uccId, msNow + 5500);
        
        assertTrue(ucc.getUserConfig().getResearch(1).getLevel() == 0 );
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 11.0d);
        
        ucc = es.getEconomy(uccId, msNow + 6000);
        
        assertTrue(ucc.getUserConfig().getResearch(1).getLevel() == 1 );
        
        ucc = es.getEconomy(uccId, msNow + 11000);
        
        assertTrue(ucc.getUserConfig().getResearch(1).getLevel() == 1 );
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 12.0d);
        
        ucc = es.getEconomy(uccId, msNow + 12000);
        
        assertTrue(ucc.getUserConfig().getResearch(1).getLevel() == 1 );
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 12.0d);
        
    }
    @Test
    // testowanie budowania bada� i jednostke
    @Rollback(true)
    public void researchUpgradeTest() throws Exception
    {
    	long msNow = nsNow/1000l/1000l + 1000l*60l*60l*24l; // po 24 h
        UsersCityConfig ucc = es.getEconomy(uccId, msNow);
        
        assertTrue(buildingService.upgradeBuilding(5, ucc.getId(), msNow));
        
        ucc = es.getEconomy(uccId, msNow);
        
        assertTrue(ucc.getMyResources().getResource(StockType.gold).getVisibleAmount() == 200.0d);
        assertTrue(ucc.getMyResources().getResource(StockType.wood).getVisibleAmount() == 0.0d);
        
        ucc = es.getEconomy(uccId, msNow + 4000);
        assertTrue(ucc.getBuilding(5).getLevel() == 0);
        ucc = es.getEconomy(uccId, msNow + 6000);
        assertTrue(ucc.getBuilding(5).getLevel() == 1);
        
        assertFalse(researchService.upgradeResearch(1, uccId, msNow + 4000));
        assertTrue(researchService.upgradeResearch(1, uccId, msNow + 6000));
        
        msNow += 6000;
        
        DetailedUnitDTO unitDto = new DetailedUnitDTO(null, null, 0, 10); // (id = 10);
        unitDto.setBuildCount(2); //budujemy dw�ch w�ocznik�w 
        assertTrue(unitService.buildArmy(unitDto, uccId, msNow) == 2);
        
        ucc = es.getEconomy(uccId, msNow + 4000);
        
        assertTrue(ucc.getMyResources().getResource(StockType.gold).getVisibleAmount() == 0.0d);
        assertTrue(ucc.getMyResources().getResource(StockType.wood).getVisibleAmount() == 0.0d);
        assertTrue(ucc.getUserConfig().getResearch(1).getLevel() == 0 );
        
        ucc = es.getEconomy(uccId, msNow + 5000);
        
        assertTrue(ucc.getUserConfig().getResearch(1).getLevel() == 1 );
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 10.0d);
        
        ucc = es.getEconomy(uccId, msNow + 5200);
        
        assertTrue(ucc.getUserConfig().getResearch(1).getLevel() == 1 );
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() > 10.0d);
        
        ucc = es.getEconomy(uccId, msNow + 9999);
        
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() < 11.0d);
        
        ucc = es.getEconomy(uccId, msNow + 10000);
        
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 11.0d);
        
        ucc = es.getEconomy(uccId, msNow + 15000);
        
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 12.0d);
        
    }
    @Test
    // testowanie wyprawy, dwa miasta , robbery atack.
    @Rollback(true)
    public void robberyTripTest() throws Exception {
    	
    	long msNow = nsNow/1000l/1000l + 1000l*60l*60l*24l; // po 24 h
        UsersCityConfig ucc = es.getEconomy(uccId, msNow + this.msTravelTime);
        /*
         * tyle jedzenie ma miasta, gdy nie wysy�a armi po rabunek, po czasie msNow + czas podr�zy w dwie strony
         */
        double waterNoTrip = ucc.getMyFood().getFood(StockType.water).getVisibleAmount();
        UsersCityConfig target = es.getEconomy(targetId, msNow);
        
        Set<UnitDTO> units = new HashSet<UnitDTO>();
        UnitDTO unitdto = new UnitDTO(null,null,10,10);
        units.add(unitdto);
        Set<ConcreteResourceDTO> res = new HashSet<ConcreteResourceDTO>();
        Set<ConcreteFoodDTO> food = new HashSet<ConcreteFoodDTO>();
        
        /*
         * poni�ej jest merge obydw�ch miast do bazy danych
         */
        tripService.sendTrip(units, food, res, ucc.getId(), target.getId(), msNow, Mission.robbery, this.msTravelTime/2);
        
        /*
         * miasto broni�ce 10 ms przed atakiem
         */
        target = es.getEconomy(targetId,msNow + msTravelTime/2 -10);
        assertTrue(target.getMyFood().getFood(StockType.rice).getVisibleAmount() == 0.0d );
        assertTrue(target.getIncomeTrips().size() == 1 );
        
        /*
         * miasto broni�ce 1 ms po ataku
         */
        target = es.getEconomy(targetId,msNow + msTravelTime/2 +1);
        assertTrue(target.getIncomeTrips().size() == 0 );
        assertTrue(target.getUserConfig().getMessages().size() == 1);
      //  assertTrue(target.getUserConfig().getMessages().size() == 1  );
        double waterAfterAttack = target.getMyFood().getFood(StockType.water).getVisibleAmount();
        
        
        /*
         * miasto broni�ce w czasie ataku
         */
        target = es.getEconomy(targetId,msNow + msTravelTime/2);
        double waterAtAttackTime = target.getMyFood().getFood(StockType.water).getVisibleAmount();
        assertTrue(target.getIncomeTrips().size() == 1 );
        
        // roznica przed i po ataku wody w miescie broni�cym to 50 +- 1.0 (produkcja i jedzenie armi zabrane na wyprawe)
        assertTrue(waterAtAttackTime - waterAfterAttack < 51.0d && waterAtAttackTime - waterAfterAttack > 49.0d);
        
        
        ucc = es.getEconomy(uccId,msNow + msTravelTime);
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 10.0d);
        double waterAfterArmyReturn = ucc.getMyFood().getFood(StockType.water).getVisibleAmount();
        assertTrue( ucc.getOutcomeTrips().size() == 0);
        assertTrue( ucc.getUserConfig().getMessages().size() == 1);
     
        /*
         * tyle wody ukradnie wojsko, 100(ilosc miejsca ) - 1/12(w czasie bitwy beda mieli tyle jedzenia na podroz powrotna)  / 2(kradn� wode i wolnych ludzi)
         */
        
        double waterStolen = ((100.0d - 1.0d/12.0d)/2.0d );
        double t1 = waterNoTrip + waterStolen;
        assertTrue(waterAfterArmyReturn == t1 );
        
        ucc = es.getEconomy(uccId,msNow + msTravelTime - 1);
        double waterBeforeArmyReturn = ucc.getMyFood().getFood(StockType.water).getVisibleAmount();
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 0.0d);
        
        ucc = es.getEconomy(uccId,msNow + msTravelTime + 10);
        assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 10.0d);
        
        System.out.print("\nucc water:" + ucc.getMyFood().getFood(StockType.water).getVisibleAmount() + "\n" );
		
    }

    @After
    public void tearDownWithinTransaction() {
        // execute "tear down" logic within the transaction
    	
    }


	
}
