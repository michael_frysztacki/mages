package for_galan;

import java.lang.reflect.InvocationTargetException;

import org.hibernate.Session;
import org.hibernate.Transaction;

import entities.Point;

import test.HibernateUtil;

public class ForGalan {

	public static void main(String[] args) throws Exception{
		
		
		/*
		 * Przyk�ad zapisania 100000 punkt�w
		 */
		for(int i =0; i < 10 ; i++)
		{
			Point p = new Point();
			p.setId(i);
			p.setX(10.0d);
			p.setY(11.0d);
			SavePoint.savePoint(p);
		}
		SavePoint.commit();
		System.out.print("DONE");
		
		/*
		 * Przyk�ad zapisanai drogi
		 */
		Point start = new Point();
		start.setId(1);
		start.setX(1.0d);
		start.setY(2.0d);
		
		Point end = new Point();
		end.setId(2);
		end.setX(1.0d);
		end.setY(2.0d);
		
		Point p1 = new Point();
		p1.setId(3);
		p1.setX(1.0d);
		p1.setY(2.0d);
		
		Point p2 = new Point();
		p2.setId(4);
		p2.setX(1.0d);
		p2.setY(2.0d);
		
		long startT = System.currentTimeMillis();
		for(int i = 0 ; i < 25000; i++)
		{
			Track track = new Track(start,p1,p2,end);
			SaveTrack.saveTrack(track,true);
		}
		SaveTrack.commit();
		long stopT = System.currentTimeMillis();
		
		System.out.print("DONE2: " + (double)((double)stopT - (double)startT)/1000.0d);
		
		
	}
}
