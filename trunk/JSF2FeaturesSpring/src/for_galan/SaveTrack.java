package for_galan;

import org.hibernate.Session;
import org.hibernate.Transaction;

import entities.Point;

import test.HibernateUtil;

public class SaveTrack {

	private static int counter = 0;
	private static int track_counter = 0;
	private static Transaction tx;
	private static Session session = null;
	private static String sql_query = new String();
	
	public static void saveTrack(Track p,boolean isLandTrack)
	{
		if(session == null)
		{
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
		}
		/*
		 * track_id
		 * order_
		 * point_id
		 */
		if(counter == 0 ) 
		{
			String table = isLandTrack ? "land_track" : "water_track";
			sql_query = "insert into "+ table +"(track_id,order_,point_id) values";
		}
		sql_query += ",("+ track_counter+",0,"+p.getStartPoint().getId()+")";
		sql_query += ",("+ track_counter+",-1,"+p.getEndPoint().getId()+")";
		counter += 2;
		int i=1;
		for(Point pp : p.getPunkty_posrednie())
		{
			sql_query += ",("+ track_counter +","+i+","+ pp.getId() + ")";
			i++;
			counter++;
		}
		if ( counter > 800  ) {
	        //flush a batch of inserts and release memory:
			sql_query = sql_query.replace("values,", "values");
			//System.out.print(sql_query + "\n");
	        session.createSQLQuery(sql_query).executeUpdate();
	        counter = 0;
	    }
		track_counter++;
	}
	public static void commit()
	{
		sql_query = sql_query.replace("values,","values");
		//System.out.print(sql_query + "\n");
		session.createSQLQuery(sql_query).executeUpdate();
		tx.commit();
		session = null;
	}
}
