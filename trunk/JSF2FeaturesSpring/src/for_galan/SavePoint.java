package for_galan;

import org.hibernate.Session;
import org.hibernate.Transaction;

import entities.Point;


import test.HibernateUtil;

public class SavePoint {

	private static int counter = 0;
	private static Transaction tx;
	private static Session session = null;
	
	public static void savePoint(Point p)
	{
		if(session == null)
		{
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			tx = session.beginTransaction();
		}
		session.save(p);
		counter++;
		if ( counter % 30 == 0 ) { //30, same as the JDBC batch size
	        //flush a batch of inserts and release memory:
	        session.flush();
	        session.clear();
	        counter = 0;
	    }
	}
	public static void commit()
	{
		tx.commit();
		session = null;
	}
}
