package for_galan;

import java.util.ArrayList;
import java.util.List;

import entities.Point;


/*
 * Droge
 */
public class Track {

	private Track(){};
	private List<Point> punkty_posrednie=null;
	public List<Point> getPunkty_posrednie() {
		return punkty_posrednie;
	}
	public Point getStartPoint() {
		return startPoint;
	}
	public Point getEndPoint() {
		return endPoint;
	}
	private Point startPoint = null;
	private Point endPoint = null;
	public Track (Point... points) throws Exception
	{
		if(points.length < 2) throw new Exception("Trasa musi si� sk�ada� z przynamniej dw�ch punkt�w");
		punkty_posrednie= new ArrayList<Point>();
		startPoint = points[0];
		for(int i = 1 ; i < points.length - 1; i++)
		{
			punkty_posrednie.add(points[i]);
		}
		endPoint = points[points.length - 1];
		
	}
}
