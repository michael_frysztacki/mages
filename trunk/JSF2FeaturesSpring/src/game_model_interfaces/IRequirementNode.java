package game_model_interfaces;

import java.util.List;

import entities.UsersCityConfig;

public interface IRequirementNode {

	public static class RequirementNotFullfilledException extends Exception {	
		private static final long serialVersionUID = 1L;
	}
	public List<IRequirementLink> getParents();
	public abstract boolean canBuild(UsersCityConfig where);
	public abstract <T extends IGameModel> List<T> getChildrenRecursively(Class<T> clazz);
}