package game_model_interfaces;

import game_model_interfaces.civilization_defining_interfaces.ICivilizationParameters;
import game_model_interfaces.civilization_defining_interfaces.Patterns;
import game_model_interfaces.initializations.ICityInitializator;

import java.util.List;

public interface ICivilization extends Patterns, ICivilizationParameters {

	public <T extends IGameModel> T getById(int id, Class<T> clazz);
	public ICityModel getCityModel();
	public ICivilization getParentCivilization();
	public List<ICivilization> getChildCivilizations();
	public List<IGameModel> getGameModelList(boolean fullList);
	public <T extends IGameModel> List<T> getGameModelList(Class<T> objectType, boolean inherit, boolean fullList);
	public IHousesModel getHousesModel();
	public Class<?> getCivilClass();
	public ICityInitializator getCityInitializator();
	public IRadialResearchModel getLandRadialResearch();
}