package game_model_interfaces;


public interface IImprovementModel extends IResearchModel{
	
	public Boolean isExtractionImprovement();

}
