package game_model_interfaces;

import entities.UserConfig;

public interface ILandUnitModel extends IUnitModel {
	public double getEatPerHour(UserConfig uc);
	public int getSpeed(UserConfig uc, TerainType type);
}
