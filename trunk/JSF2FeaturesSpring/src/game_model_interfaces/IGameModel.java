package game_model_interfaces;


import java.util.List;

import entities.Civil;
import entities.UsersCityConfig;
import game_model_impl.GameModel;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import helpers.Three;


public interface IGameModel {

	public int getId();
	public String getName(UsersCityConfig uc);
	public String getDescription(UsersCityConfig uc);
	public Integer getBuildTime(UsersCityConfig ucc);
	public PriceStockSet getPrice(UsersCityConfig ucc);
	public IRequirementNode getRequirementNode();
	public ICivilization getMyCivilization();
	
	public Class<? extends IGameModel> getObjectsClass();
	public GameModel getInternalGameModel();
	public WorldDefinition getCivilizationHolder();
	public <T extends IGameModel> T cast(Class<T> clazz);
	public <T extends IGameModel> boolean canCast(Class<T> clazz);
	/*
	 *  metoda printDecoratorChain() jest u�ywana tylko w celach testowych.
	 */
	public String printDecoratorChain();
	public String dump(Language lang, Civil civil);
	public List<Three<Integer, String, String>> dumpHeader(Language lang, Civil civil);
	public String dumpBody(Language lang, Civil civil);
	public String getSimpleName(Language lang);
}
