package game_model_interfaces.used;

import game_model_interfaces.ICivilization;
import game_model_interfaces.IWorldDefinition;

public interface UserConfig {

	public ICivilization getMyCivilization();
	public IWorldDefinition getWorldDefinition();
}
