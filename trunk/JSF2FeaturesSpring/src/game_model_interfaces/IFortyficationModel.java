package game_model_interfaces;

import entities.UsersCityConfig;

public interface IFortyficationModel extends IBuildingModel{

	public int getFortyficationHP(UsersCityConfig ucc);
	public int getRepairRate(UsersCityConfig ucc); // wsp�czynnik naprawiana w hp na godzin�
}
