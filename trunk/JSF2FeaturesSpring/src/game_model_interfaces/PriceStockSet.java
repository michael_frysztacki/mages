package game_model_interfaces;

import java.util.List;

import framework.StockSet;

public abstract class PriceStockSet extends StockSet<PriceStock>{

	/*
	 * regular stock mean's that this stock can not be a tax stock
	 */
	public PriceStock getStockById(String stockId) {
		return null;
	}
	
	public PriceStock getTaxPriceStock(ICivilization civil) {
		return null;
	}
	
	public PriceStockSet cloneAsMultiplied(int multiply) {
		return null;
	}

	public List<PriceStock> getPriceStocks(ICivilization civil) {
		return null;
	}
	/*
	 * poniewa� PriceStockSet mo�e zawiera� cen� typu podatek, a podatek ka�dej cywilizacji mo�e by� r�ny,
	 * trzeba najpierw ustawi� odpowiednio PriceStockSet w zale�no�ci od gracza, np. korea�czyk mo�e mie� w cenie do zap�aty
	 * srebro, a europejczyk z�oto. Je�li PriceStockSet nie zawiera ceny-podatku, metoda zwr�ci normaln� cene.
	 */
	private PriceStockSet adjustPriceStockSetForPlayer(PriceStockSet basePrice) {
		PriceStockSet local = basePrice.cloneAsMultiplied(1);
		TaxPriceStock taxPrice = local.getTaxPriceStock();
		if(taxPrice!=null)
			taxPrice.setTaxId(getMyCivil().getMainResource());
		return local;
	}

}
