package game_model_interfaces;

import entities.UserConfig;
import entities.UsersCityConfig;

public interface IUnitModel extends IGameModel{

	public enum ArmorType
	{
		light,
		heavy,
		animals,
		siege
	}
	public enum AttackType
	{
		siege,
		normal,
		pierce,
		range
	}
	public enum TerainType{
		plain,
		steps,
		jungle,
		desert,
		mountains
	}
	//public int getSpeed(UserConfig uc, TerainType type);
	public int getCapacity(UserConfig uc);
	public int getHP(UserConfig uc);
	public AttackType getAttackType(UserConfig uc);
	public int getAttackOutsideCity(UserConfig uc); // atak dla jednostki poza miastem
	public int getAttackInsideCity(UsersCityConfig uc); // atak dla jednsotki w mie�cie (fortyfikacja )
	public ArmorType getArmorType(UserConfig uc);
	public int getArmorOutsideCity(UserConfig uc); // armor dla jednostki poza miastem
	public int getArmorInsideCity(UsersCityConfig uc); // armor dla jednostki w miescie (fortyfikacja )

	
	
}
