package game_model_interfaces;

import entities.UsersCityConfig;

public interface IHousesModel extends IBuildingModel{

	public int getPeopleLimit(UsersCityConfig ucc);
	
}
