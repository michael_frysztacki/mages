package game_model_interfaces;

import framework.IntegerStock;

public abstract class PriceStock extends IntegerStock {

	public PriceStock(String stockId, int amount) {
		super(stockId, amount);
	}

	void multiply(double multiplier) {
		amount = (int) (multiplier * (double)amount);
	}

}
