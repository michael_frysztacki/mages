package game_model_interfaces.civilization_defining_interfaces;

import game_model_impl.BoosterPrototype;
import game_model_impl.GameModel;
import game_model_impl.Requirement;
import game_model_impl.Text.Text;
import game_model_impl.boosters.convert_building.ConvertBuildingSubstractor;
import game_model_impl.city_model.CityModelBooster;

public interface ICivilizationConfigProvider extends Patterns, ICivilizationParameters{

	/*
	 * Civilization buildings, units and researches
	 */
	public GameModel[] gameModels();
	/*
	 * boosters for convert buildings
	 */
	public ConvertBuildingSubstractor[] convertBuildingSubstractors();
	/*
	 * boosters for improvements
	 */
	public BoosterPrototype[] boosters();
	public CityModelBooster[] cityModelBoosters();
	
	public Requirement[] getRequirements();
	
	/*
	 * W metodzie getNames() oraz getDescriptions() definiujemy nazewnictwo dla obiekt�w naszej gry.
	 * Klasa Text definiuje dwa konstruktory:
	 * 
	 * public Text(int objId, LanguagePair... pairs)
	 * oraz
	 * public Text(int objId, int changerId, int reqLvl, LanguagePair... pairs)
	 * 
	 * Z pomoc� obu konstruktor�w, mo�emy tworzy� nazewnictwo dla obiekt�w znajduj�cych si� w tej samej klasie defininuj�cej cywilizacj� oraz 
	 * cywilizacjach rozszerzaj�cych.
	 * ( kt�re rozszerza interfejs ICivilizationConfigProvider ).
	 * 
	 * Text(int objId, LanguagePair... pairs)
	 * 
	 * Tego konstruktora u�ywamy, gdy nazwa nie zale�y od poziomiu w�asnego lub obcego(np. badania) obiektu.
	 * Dla tego konstruktora w jednej klasie definiuj�cej cywilizacj�, mo�emy u�y� tylko raz tego konstruktora dla jednego obiektu.
	 * Tzn. nie mo�emy zrobi� czego� takiego:
	 * 
	 * new Text(12, LanguagePair[] ),
	 * new Text(12, LanguagePair[] )
	 * 
	 * Zastosowanie takiego czego� nie mia�obyby sensu, dlatego �e obiekt mia�by dwuznaczn� nazw�.
	 * Mo�emy natomiast ponownie zdefiniowa� ten sam obiekt, ale w klasie cywilizacyjnej, kt�ra rozszerza cywilizacj�.
	 * Np. w cywilizacji common definiujemy: new Text(5, LanguagePair[]), a w cywilizacji rozszerzaj�cej cywilizacj� common,
	 * mo�emy ponownie zdefiniowa� new Text(5, LanguagePair[] ). Taki zabieg spowoduje przeci��enie nazwy dla tej cywilizacji danego obiektu.
	 * W �a�cuchu cywilizacyjnym mo�emy redefiniowa� nazw� dla obiektu dla ka�dej cywilizacji.
	 * 
	 * Drugiego konstruktora : Text(int objId, int changerId, int reqLvl, LanguagePair... pairs)
	 * 
	 * public Text(int objId, int changerId, int reqLvl, LanguagePair... pairs),
	 * u�ywamy gdy chcemy aby nazwa obiektu zale�a�a od poziomu w�asnego lub obiektu zewn�trznego.
	 * 
	 * je�li chcemy aby nazwa zale�a�a od poziomu w�asnego u�ywamy:
	 * public Text(0, 0, 3, LanguagePair[]),
	 * gdzie obiektem jest obiekt o id=0.
	 * 
	 * je�li chcemy aby nazwa zale�a�a od poziomu obiektu zewn�trznego:
	 * public Text(0, 13, 3, LanguagePair[]),
	 * gdzie 0 to id obiektu w kt�rym zmieniamy nazw�, a 13 to id obiektu, od kt�rego zale�y nazwa zale�y od level.
	 * 
	 * Trzeci argument to poziom obiektu(zewn�trznego lub ten sam), od kt�rego nazwa ma si� zamieni� na nazw� podan� w czwartym argumencie.
	 * 
	 * Przyk�ad definicji w jednej klasie konfiguracyjnej (czwarty argument jest u�ywany jako pseudokod, dla uproszczenia, poprawne u�ycie zobacz w przyk�adach):
	 * 
	 * new Text(0, 0, 0, "fortyfikacja"),
	 * new Text(0, 0, 7, "wielki mur chinski")
	 * 
	 * trzeci argument okre�la level, od kt�rego ma dzia�a� nowa nazwa. W powy�szym przyk�adzie, obiekt b�dzie si� nazywa� fortyfikacja
	 * od levelu 0-6, a od 7 level b�dzie si� nazywa� wielki mur chi�ski.
	 * 
	 * Je�li w jakie� cywilizacji rozszerzaj�cej przeci��ymy nazw� obiektu znajduj�cego si� w cywilizacji wy�ej w spos�b:
	 * 
	 * new Text(0, 0, 7, "koreanski market palce")
	 * 
	 *  a aktualnie nasz market place zdefiniowany w cywilizacji wy�ej jest ni�szy od 7 levelu, odpowiednia nazwa b�dzie szukana dalej
	 *  w cywilizacjach wy�ej( w cywilizacji Common powinno by� zdefiniowane np. new Text(0, "market place").
	 * 
	 * NIE MO�NA:
	 * - w jednym pliku konfiguracyjnym cywilizacji u�y� obu rodzaj�w konstruktor�w dla jednego obiektu( nazwenictwo b�dzie niejednoznaczne )
	 * - w jednym pliku konfiguracyjnym u�y� konstruktora Text(12, LanguagePair[] ) wi�cej ni� jeden raz dla jednego obiektu.
	 * - w jednym pliku konfiguracyjnym u�yc konstruktora Text(0, 0, 3, LanguagePair[]) dla tego samego obiektu, gdy obiektem zmieniaj�cym
	 * s� r�ne obiektu np.
	 * 
	 * przyk�ad:
	 * 
	 * Text(0, 0, 3, LanguagePair[])
	 * Text(0, 0, 5, LanguagePair[])
	 * Text(0, 1, 7, LanguagePair[]) - tego juz nie mozemy uzyc po uzyciu dwoch pierwszych konstruktor�w.
	 * 
	 * Przyk�ady u�y� znajduj� sie w unit testach: 
	 * 
	 * junittest -> model_test -> text
	 * 
	 */
	public Text[] getNames();
	public Text[] getDescriptions();
	
}
