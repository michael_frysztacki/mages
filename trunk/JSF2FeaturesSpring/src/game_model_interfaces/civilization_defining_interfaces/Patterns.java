package game_model_interfaces.civilization_defining_interfaces;

import entities.UserConfig;
import game_model_impl.PriceStockSetImpl;
import game_model_interfaces.IUnitModel.ArmorType;
import game_model_interfaces.IUnitModel.AttackType;

public interface Patterns {

	public Integer fortyficationProtectionLevel(AttackType aType);
	public Integer fortyficationProtectionLevel(int unitId);
	
	public Integer fortyficationArmorBoost(int fortyficationLevel, int currentArmor);
	public Integer fortyficationAttackBoost(int fortyficationLevel, int currentAttack);
	/*
	 * wzory na id pattern�w - jako argument podajemy id obiektu a zwraca nam kt�ry wzorek ma ten id budynku.
	 * wzory budynk�w, kt�re wyst�puj� pojedy�czo ( fortyfikacja -getHP, domki - getPeopleLimitInHouses ), nie maj� tutaj pobierania wzorku.
	 * Tym budynkom pattern przypisuje si� bezpo�rednio do GameModelu.
	 * Tymi metodami przypisujemy id wzork�w (nie wzorki ! - id wzork�w z kolei dopiero pokazuj� na wzorki) do GameModeli.
	 */
	public Integer getSpongingPatternById( String convBuildId_stockType );
	public Integer getCostPatternById( int gameModelId );
	public Integer getExtractionPatternById (int extractBuildingId);
	public Integer getProductionPatternById ( int convertBuildingId);
	public Integer getMaxPeoplePatternById( int extractBuildingId);
	public Integer getResearchBuildTimePatternById ( int reseachId );
	public Integer getUnitBuildTimePatternById ( int unitId);
	public Integer getBuildingBuildTimePatternById ( int buildingId);
	public Integer getRadiusPatternById ( int radiusReseach);
	
	/*podstawowe wzory dla budynk�w, bada� i jednostek*/
	
	public Integer getPeopleLimitInHouses(int housesLevel, int basePeopleLimit,int patternId);
	/*
	 * productionOfOutComeResource - produkcja surowca wychodz�cego z convert building
	 * zwraca ilosc zuzytego surowca na produkcje productionOfOutcomeResource
	 * przyk�ad sulhouse -> sul.
	 */
	public Integer getSpongingByPattern( int productionOfOutcomeResource,int patternId);
	
	public PriceStockSetImpl getCostByPattern(PriceStockSetImpl baseCost,int patternId, int lvl, UserConfig uc);
	
	/*
	 * dla budynk�w wydobywczych(pr�cz konwertuj�cych).
	 */
	public Integer getExtractionByPattern(Integer peopleCount,Integer lodeFactor, int patternId);
	
	
	/*
	 * dla budynk�w konwertuj�cych
	 */
	public Integer getProductionByPattern(int peopleCount, int patternId);

	
	public Integer getMaxPeopleByPattern(int baseAmount, int buildingLvl, int patternId);
	/*
	 * blvl - level budynku
	 * rlvl - level badania
	 * timeFactor - wsp�czynnik czasowy
	 */
	public Integer getResearchBuildTimeByPattern(Long timeFactor, int rlvl, int blvl, int patternId);
	
	public Integer getUnitBuildTimeByPattern(Long baseBuildTime, int blevel, int patternId);
	
	public Integer getBuildingBuildTimeByPattern(int patternId, int currLvl, Long baseBuildTime);
	
	/*
	 * metoda dla wymaga�, okresla max levele.
	 * jako argument podajemy level, jaki chcemy mie� i wzorzec(patternId).
	 * W return otrzymujemy jaki level powinien mie� parent, je�li chcemy miec level obiektu r�wny 'myLevel'
	 * Tzn je�li aktualnie mamy level budynku 2, chcemy zupgradowac na poziom 3, to musimy sprawdzic
	 * jaki jest wymagany level parenta dla przypadku kiedy my mamy level 3, wiec powinnismy podac w argumencie myLevel =3.
	 * Je�eli jest to jednostka, myLevel jest null.
	 */
	public Integer getMaxLvlByPattern(Integer myLevel, int patternId);
	
	/*
	 * wzory dla fortyfikacji
	 */
	
	public Integer getFortyfiactionHP(int baseHP, Integer fortyficationLevel, int patternId);
	public Integer getFortyficationRepairRate(int baseRepairRate, Integer fortyficationLevel, int patternId);
	
	/*
	 * wz�r dla ulepszen radialnych ( kartografia , nawigacja )
	 */
	public Integer getRadius(int baseRadius, Integer radiusResearchLevel, int patternId);
	
	/*
	 * wzory dla ulepsze�(booster�w)
	 */
	public Integer boostRadius(int current, int improvementLevel, int patternId);
	
	public Integer boostFortyficationHP(int current, int improvementLevel, int patternId);
	public Integer boostFortyficationRepairRate(int current, int improvementLevel, int patternId);
	
	public Integer ArmorBoostPattern(int current, int improvementLevel, int patternId);
	public ArmorType boostArmorType(int patternId, int rLevel, ArmorType current);
	public AttackType boostAttackType(int patternId, int rLevel, AttackType current);
	public Integer boostAttack(int patternId, int rLevel, int current);
	
	public Integer boostCapacity(int current, int improvementLevel, int patternId);
	public Integer boostSpeed(int current, int improvementLevel, int patternId);
	/*
	 * metoda zwi�kszaj�ca hp u obiektu takiego jak np. unit czy fortyfikacja.
	 */
	public Integer boostHP(int currentHP, int objectLvl, int patternId);
	
	/*
	 * brLevel - level budynku lub badania kt�re wp�ywa na czas.
	 */
	public Integer boostBuildTimePattern(int currentTime, int brLevel, int patternId);
	
	/*
	 * dla budynk�w konwertuj�cych i zwyk�ych wydobywczych. Wzorek stosowany przy ulepszaniu produkcji w budynkach
	 * konwertuj�cych i zwyk�ych wydobywczych.
	 */
	public Integer getImprovedProductionByPattern(int currentProduction, int improvementLevel, int patternId);
	/*
	 * dla budynk�w konwertuj�cych.
	 */
	public Integer getImprovedInputByPattern(int currentInput, int improvementLevel, int patternId);
	
	public Integer maxPeopleBoostPattern(int currentMaxPeople, int improvementLevel,int patternId);
	public PriceStockSetImpl PriceBoostPattern(PriceStockSetImpl current, int improvementLevel,int patternId);
	
	
	/*
	 * metoda dla wymaga�, okresla max levele.
	 * jako argument podajemy level, jaki chcemy mie� i wzorzec(patternId).
	 * W return otrzymujemy jaki level powinien mie� parent, je�li chcemy miec level obiektu r�wny 'myLevel'
	 * Tzn je�li aktualnie mamy level budynku 2, chcemy zupgradowac na poziom 3, to musimy sprawdzic
	 * jaki jest wymagany level parenta dla przypadku kiedy my mamy level 3, wiec powinnismy podac w argumencie myLevel =3.
	 */	

	/*
	 * civil config boosters
	 */
	public Boolean boostAntarcticaSettlemantAbility(int reserachLvl, int patternId);
	/*
	 * wzorzec obliczaj�cy satysfakcj� z posiadania budynku ew. badania.
	 */
	public Integer boostSatisfaction(int currentSatisf, int objectLvl, int patternId);
	/*
	 * object level - najcze�ciej powinien to by� level kartografii, ale zostawiam mo�liwo�� �e co� innego(budynek lub badanie) te� mo�e zwi�kszy� promie� nawigacyjny.
	 */
	public Integer boostCityRadius(int currentRadius,int objectLvl, int patternId);
	public Integer boostMoralFactor(int currentMoral, int objectLvl, int patternId);
	/*
	 *u aztek�w budynkiem kt�ry b�dzie zwi�ksza� naprawe fortyfikacji to b�dzie kopalnia kamienia,
	 *wi�c tak b�dzie dla przyk�adu mo�na wykorzysta� poni�sz� metode
	 */
	public Integer boostFortificationRepairPerHour(int currentRepairPerHour, int objectLvl, int patternId);
	
	public Integer boostDirectionsAmount(int objectLvl, int patternId);
	
}
