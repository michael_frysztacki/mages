package game_model_interfaces.civilization_defining_interfaces;

import java.util.List;

public interface ICivilizationParameters {
	/*
	 * kt�re obiekty chcemy wyrzucic z cywilizacji nadrz�dnych
	 */
	public List<Integer> excludedObjects();
	/*
	 * return null for default 
	 */
	public Double getPercentRobberySteal();
	/*
	 * must be not-null value
	 */
	public String getCivilization();
	/*
	 * kt�r� cywilizacj� rozszarza 
	 */
	public String extendsCivilization();
	/*
	 * Resource determining the resource from taxes.
	 * By implementing a new concrete civilization(not common civilization), can not be null.
	 */
	public String getMainResource();
	public String getMajorMeat();
	public String getMajorCarbo();
	public String getMajorAddition();
	/*
	 * return null for default
	 */
	
	public Boolean canSettleAntarctica();
	public Integer mapDirectionsAmount();
}
