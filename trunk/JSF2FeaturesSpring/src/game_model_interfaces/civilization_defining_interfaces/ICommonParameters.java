package game_model_interfaces.civilization_defining_interfaces;

import java.util.List;

public interface ICommonParameters {
	
	public int getFurtherAdditionSatisfaction();
	public int getFurtherCarboSatisfaction();
	public int getFurtherMeatSatisfaction();
	public int getMajorAdditionSatisfaction();
	public int getMajorCarboSatisfaction();
	public int getMajorMeatSatisfaction();
	public int getWaterSatisfaction();
	public int satisfactionCooldownSec();
	public int getMaxFightRounds();
	public int getBirthMultiplier();
	public int getEatPerHour();
	public double chanceForTakingOverExtractBuilding();
	public Double getPeopleFallRate();
	public int getSatisfaction(String stockId, String playersCivilId);
	public List<String> getAllResourceIds();
	public List<String> getAllFoodIds();
}
