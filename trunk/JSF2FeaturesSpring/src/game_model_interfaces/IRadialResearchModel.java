package game_model_interfaces;

import entities.UserConfig;

public interface IRadialResearchModel extends IResearchModel{

	public int getRadius(UserConfig uc);
}
