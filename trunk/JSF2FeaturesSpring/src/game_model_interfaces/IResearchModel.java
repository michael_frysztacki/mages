package game_model_interfaces;

public interface IResearchModel extends IGameModel {
	
	public int getMaxLevel();
}
