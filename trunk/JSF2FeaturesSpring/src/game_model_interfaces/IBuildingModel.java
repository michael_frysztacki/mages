package game_model_interfaces;

import java.util.List;

import entities.UserConfig;
import entities.UsersCityConfig;


public interface IBuildingModel extends IGameModel {

	public List<IUnitModel> getUnits(UserConfig city);
	/*
	 * getResearches jest zale�ne nie tylko od usera ale tak�e od miasta.
	 * W przypadku kiedy przej�li�my obce miasto, nie wszystkie badania mog� by� widoczne.
	 * Niewidoczne b�d� te, kt�re wymagajaj� budynku, kt�rego my akurat w tym mie�cie nie mamy.
	 */
	public List<IResearchModel> getResearches(UsersCityConfig city);
}
