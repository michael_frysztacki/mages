package game_model_interfaces;

import entities.UsersCityConfig;

public interface ICityModel {

	public int getSatisfactionFromObjects(UsersCityConfig ucc);
	
}
