package game_model_interfaces;


public class TaxPriceStock extends PriceStock {

	public TaxPriceStock(int amount)
	{
		super(null,amount);
	}
	
	public void setTaxId(String taxId) {
		this.stockId = taxId;
	}
	@Override
	public PriceStock clone()
	{
		return new TaxPriceStock(this);
	}
	
	private TaxPriceStock(TaxPriceStock tpp)
	{
		this(tpp.getVisibleAmount());
	}
	
}
