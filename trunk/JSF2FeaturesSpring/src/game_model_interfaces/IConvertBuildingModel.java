package game_model_interfaces;



import java.util.List;

import entities.UsersCityConfig;
import game_model_impl.StockType;


public interface IConvertBuildingModel extends IExtractBuildingModel{

	public int getInputPerHour(UsersCityConfig ucc, StockType resource);
	public Double getProductionRate(UsersCityConfig ucc);
	public int getPureOutputPerHour(UsersCityConfig ucc);
	public List<StockType> getInputTypes();
}
