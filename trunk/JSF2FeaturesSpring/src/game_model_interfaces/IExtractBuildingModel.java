package game_model_interfaces;

import entities.UsersCityConfig;
import game_model_impl.StockType;

public interface IExtractBuildingModel extends IBuildingModel{

	/*
	 * If providing city, in which the extract building does not exist, should return 0.
	 */
	public int getOutputPerHour(UsersCityConfig ucc);
	public StockType getResourceType();
	public int getMaxPeople(UsersCityConfig ucc);
	
}
