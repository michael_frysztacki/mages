package game_model_interfaces;

import java.util.List;

import game_model_interfaces.civilization_defining_interfaces.ICommonParameters;

public interface IWorldDefinition extends ICommonParameters {

	ICivilization getCivilization(String civilId);
	List<ICivilization> getAllCivilizations();
	IExtractBuildingModel getProducer(String stockId);
}
