package game_model_interfaces;

public interface IAllWorlds {
	
	public IWorldDefinition getWorldModel(int id);
}
