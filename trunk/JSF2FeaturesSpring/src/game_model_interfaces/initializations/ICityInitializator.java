package game_model_interfaces.initializations;

import java.util.List;
import framework.IStock;

public interface ICityInitializator {
	
	public List<IStock> initialCityStocks();
	public int initialTaxSatisfaction();
}
