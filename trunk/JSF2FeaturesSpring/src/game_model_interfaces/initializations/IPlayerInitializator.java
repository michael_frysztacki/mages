package game_model_interfaces.initializations;

import entities.UserConfig;

public interface IPlayerInitializator {

	public UserConfig initPlayer(UserConfig uc);
}
