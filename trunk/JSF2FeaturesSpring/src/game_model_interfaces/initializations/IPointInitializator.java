package game_model_interfaces.initializations;

import entities.Civil;
import entities.Point;

public interface IPointInitializator {

	public void initPoint(Point p, Civil civil);
	
}
