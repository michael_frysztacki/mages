package game_model_interfaces;

public interface IRequirementLink {

	public IRequirementNode getParentNode();
}
