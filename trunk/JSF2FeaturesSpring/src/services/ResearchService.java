package services;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import dto.BuildingDTO;
import dto.DetailedExtractBuildingDTO;
import dto.DetailedBuildingDTO;
import dto.ExtractBuildingDTO;
import entities.UsersCityConfig;

public interface ResearchService {

	public boolean upgradeResearch(int researchId, int cityId, long msTime) throws Exception;
	
}
