package services;

import entities.Civil;
import entities.UserConfig;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.initializations.ICityInitializator;
import game_model_interfaces.initializations.IPointInitializator;

public interface LoginService {

	public UserConfig findById(int id);
	public UserConfig findByName(String nick);
	public UserConfig register(String nick, String pass,Civil civil, Language lang, long nsAccountStartTime, IPointInitializator pInit,ICityInitializator initializator); 
	
}
