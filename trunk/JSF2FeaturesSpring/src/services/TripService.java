package services;

import java.util.Set;

import dto.ConcreteFoodDTO;
import dto.ConcreteResourceDTO;
import dto.UnitDTO;
import entities.Mission;
import entities.UsersCityConfig;
import entities.units.IUnitGroup;
import framework.IStock;

public interface TripService {

	public boolean sendTrip(Set<? extends IUnitGroup<Integer>> units,Set<? extends IStock<Integer>> stocks, int startCityId, int targetCityId, long msNow ,Mission mission,long msOneWayTravelTime);
}
