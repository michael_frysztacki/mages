package services.impl;

import java.util.Set;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


import dao.UsersCityConfigHome;
import economycomputation.CityCompUtil;
import entities.UserConfig;
import entities.UsersCityConfig;
import game_model_impl.WorldDefinition;

import services.CityService;

@Transactional("transactionManager")
public class CityServiceImpl implements CityService{

	private UsersCityConfigHome cityDao;
	public UsersCityConfigHome getCityDao()
	{
		return cityDao;
	}
	public void setCityDao(UsersCityConfigHome cityDao)
	{
		this.cityDao = cityDao;
	}
	public UsersCityConfig getCity(int id)
	{
		return cityDao.findById(id);
	}
	@Override
	@Transactional(readOnly=true)
	public Set<UsersCityConfig> getCities(UserConfig userConfig)
	{
		return cityDao.getUserCities(userConfig);
	}
	@Override
	public void mergeCity(UsersCityConfig uccAfter)
	{
		cityDao.merge(uccAfter);
	}
	@Autowired WorldDefinition ch;
	@Autowired CityCompUtil ccu;
	

}
