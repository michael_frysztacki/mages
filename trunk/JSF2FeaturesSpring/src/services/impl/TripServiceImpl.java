package services.impl;

import entities.Mission;
import entities.RobberyTrip;
import entities.Trip;
import entities.UsersCityConfig;
import entities.stocks.CityStockSet;
import entities.stocks.TripStockSet;
import entities.units.IUnitGroup;
import framework.IStock;
import game_utilities.TripUtil;
import helpers.TimeTranslation;

import java.util.GregorianCalendar;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import services.EconomyService;
import services.TripService;

import dao.TripHome;
import dao.UsersCityConfigHome;
import dto.ConcreteFoodDTO;
import dto.ConcreteResourceDTO;
import dto.UnitDTO;

@Service("tripService")
@Transactional(readOnly = true)
public class TripServiceImpl implements TripService{

	@Autowired private UsersCityConfigHome cityDao;
	@Autowired private TripHome tripDao;
	@Autowired private TripUtil tripUtil;
	@Autowired private EconomyService es;

//	public void sendTripDTO(Set<UnitDTO> units,Set<ConcreteFoodDTO> food, Set<ConcreteResourceDTO> resources ,UsersCityConfig startAfter, UsersCityConfig targetAfter, long nsNow_ ,Mission mission,long msOneWayTravelTime) throws SecurityException, IllegalArgumentException, IllegalAccessException
//	{
//		Army army = new Army()
//	}
	@Transactional(readOnly = false)
	public boolean sendTrip(Set<? extends IUnitGroup<Integer>> units,Set<? extends IStock<Integer>> stocks, int startCityId, int targetCityId, long msNow ,Mission mission,long msOneWayTravelTime)
	{
		UsersCityConfig startCity = es.getEconomy(startCityId, msNow);
		UsersCityConfig endCity = es.getEconomy(targetCityId, msNow);
		
		Trip trip = tripUtil.sendTrip(stocks,units,startCity,endCity,TimeTranslation.msToNs(msNow),mission,msOneWayTravelTime);
		if(trip != null)
		{
			tripDao.persist(trip);
			cityDao.merge(startCity);
			cityDao.merge(endCity);
			return true;
		}
		else return false;
		
	}
}
