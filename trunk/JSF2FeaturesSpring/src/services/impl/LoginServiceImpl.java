package services.impl;


import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;



import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.authentication.UserServiceBeanDefinitionParser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.initializations.DefaultCityInitializator;
import game_model_interfaces.initializations.ICityInitializator;
import game_model_interfaces.initializations.IPointInitializator;

import services.CityService;
import services.LoginService;

import dao.IUserConfigDao;
import dao.SpringSessionFactory;
import dao.UserConfigHomeImpl;
import dao.UsersCityConfigHome;
import entities.Civil;
import entities.Point;
import entities.Role;
import entities.UserConfig;
import entities.UsersCityConfig;

@Service("loginService")
@Transactional("transactionManager")
public class LoginServiceImpl implements LoginService{

	@Autowired UserConfigHomeImpl userDao;
	@Autowired CityService cityService;
	@Autowired UsersCityConfigHome cityDao;
	@Autowired WorldDefinition ch;
	
	@Override
	@Transactional(readOnly=true)
	public UserConfig findById(int id)
	{
		UserConfig uc = userDao.findById(id);
		return uc;
	}
	@Override
	@Transactional(readOnly=true)
	public UserConfig findByName(String name)
	{
		return userDao.findByName(name);
	}
	@Override
	@Transactional(readOnly=false)
	public UserConfig register(String nick, String pass,Civil civil, Language lang,long nsAccountStartTime, IPointInitializator pInit ,ICityInitializator initializator)
	{
		// TODO Auto-generated method stub
		UserConfig registred = userDao.findByName(nick);
		if(registred != null)return null;
		Set roles = new HashSet();
		Role role = new Role();
		role.setAuthority("ROLE_ADMIN");
		roles.add(role);
		UserConfig uc = new UserConfig(nick,pass,civil,true,roles, lang);
		userDao.persist(uc);
		Point p = new Point();
		pInit.initPoint(p,uc.getCivil());
		UsersCityConfig ucc = new UsersCityConfig(uc, true, p , nsAccountStartTime, ch);
		ucc = initializator.initCity(ucc);
		cityDao.save(ucc);
		return uc;
	}
	
	
}
