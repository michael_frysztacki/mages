package services.impl;

import game_model_impl.Building;
import game_model_impl.WorldDefinition;
import interfaces.IExtractBuilding;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.jsfsample.managedbeans.SpringJSFUtil;



import dao.UsersCityConfigHome;
import dto.BuildingDTO;

import dto.DetailedExtractBuildingDTO;
import dto.DetailedBuildingDTO;

import economycomputation.CityCompUtil;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import services.BuildingService;
import services.EconomyService;

public class BuildingServiceImpl implements BuildingService,Serializable{
	
	@Autowired private CityCompUtil foodComp;
	@Autowired private WorldDefinition ch;
	@Autowired private UsersCityConfigHome cityDao;
	@Autowired private EconomyService economyService;
	
	public UsersCityConfigHome getCityDao() {
		return cityDao;
	}
	public void setCityDao(UsersCityConfigHome cityDao) {
		this.cityDao = cityDao;
	}
	@Override
	@Transactional(readOnly = false)
	public boolean upgradeBuilding(int buildingId, int cityId, long msNow) throws Exception
	{
		UsersCityConfig uccAfter = economyService.getEconomy(cityId, msNow);
		CityBuilding cb = uccAfter.getBuilding(CityBuilding.class,buildingId);
		if(!cb.upgrade(null))return false;
		cityDao.merge(uccAfter);
		return true;
	}
	@Override
	@Transactional(readOnly = false)
	public boolean setPeopleInBuilding(UsersCityConfig uccAfter,int buildingId, int count) throws Exception
	{
		CityBuilding building = uccAfter.getBuilding(CityBuilding.class,buildingId);
		if(building instanceof IExtractBuilding)
		{
			IExtractBuilding pop = (IExtractBuilding)building;
			if(pop.setPeople(count))
			{
				cityDao.merge(uccAfter);
				return true;
			}
			else return false;
		}
		return false;
	}
	@Override
	@Transactional(readOnly = true)
	public ArrayList<DetailedExtractBuildingDTO> getExtractAndConvertBuildings(UsersCityConfig ucc) throws Exception
	{
		ArrayList<DetailedExtractBuildingDTO> ret = new ArrayList<DetailedExtractBuildingDTO>();
		
		Iterator i = ucc.getBuildings().iterator();
		while(i.hasNext())
		{
			CityBuilding cb = (CityBuilding)i.next();
			if(cb instanceof IExtractBuilding)
			{
				IExtractBuilding eb = (IExtractBuilding)cb;
				DetailedExtractBuildingDTO dto = (DetailedExtractBuildingDTO)eb.createDetailDTO(ucc);
				ret.add(dto);
			}
		}
		return ret;
	}
	@Override
	@Transactional(readOnly = true)
	public ArrayList<BuildingDTO> getCityBuildings(UsersCityConfig ucc) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	{
		ArrayList<BuildingDTO> ret = new ArrayList<BuildingDTO>();
		UsersCityConfig myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		List<Boostable> source = myCivil.getBoostableList(Building.class, false);
		for(Boostable b : source)
		{
			CityBuilding cb = ucc.getBuilding(b.getId());
			ret.add((BuildingDTO) cb.createCommonDTO());
		}
		return ret;
	}
	@Override
	@Transactional(readOnly = true)
	public ArrayList<BuildingDTO> getMilitaryBuildings(UsersCityConfig ucc) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	{
		ArrayList<BuildingDTO> ret = new ArrayList<BuildingDTO>();
		UsersCityConfig myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		List<Boostable> source = myCivil.getBoostableList(MilitaryBuilding.class, false);
		for(Boostable b : source)
		{
			CityBuilding cb = ucc.getBuilding(b.getId());
			ret.add((BuildingDTO) cb.createCommonDTO());
		}
		return ret;
	}
	@Override
	@Transactional(readOnly = true)
	public DetailedBuildingDTO getCityBuilding(UsersCityConfig ucc, int buildingId) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	{
		//Civilization myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		//Boostable building = myCivil.getById(buildingId);
		CityBuilding cb = ucc.getBuilding(buildingId);
		return (DetailedBuildingDTO) cb.createDetailDTO(null);
	}
	@Override
	@Transactional(readOnly = true)
	public DetailedExtractBuildingDTO getExtractBuilding(UsersCityConfig ucc,int buildingId) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	{
		UsersCityConfig myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		CityBuilding ecb = ucc.getBuilding(buildingId);
		return (DetailedExtractBuildingDTO) ecb.createDetailDTO(null);
	}
	
//	public MilitaryBuildingDTO createViewMilitaryBuilding(Boostable b, UsersCityConfig ucc) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
//	{
//		
//	}
	
	public WorldDefinition getCh() {
		return ch;
	}
	public void setCh(WorldDefinition ch) {
		this.ch = ch;
	}
}
