package services.impl;

import helpers.TimeTranslation;

import java.lang.reflect.InvocationTargetException;
import java.util.GregorianCalendar;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import dao.UsersCityConfigHome;


import services.EconomyService;

import economycomputation.CityCompUtil;
import entities.UsersCityConfig;

//@Service("economyService")
@Transactional("transactionManager")
public class EconomyServiceImpl implements EconomyService{

	@Autowired private CityCompUtil foodComp;
	private UsersCityConfigHome cityDao;
	public UsersCityConfigHome getCityDao() {
		return cityDao;
	}
	public void setCityDao(UsersCityConfigHome cityDao) {
		this.cityDao = cityDao;
	}
	
	@Transactional(readOnly=true, propagation = Propagation.REQUIRED)
	public UsersCityConfig getEconomy(int cityId, long msNow, Map<String,Object>... ecoVariables)
	{
		Map<String,Object> ecoVar = null;
		if(ecoVariables.length == 1)ecoVar = ecoVariables[0];
		UsersCityConfig ucc = cityDao.findById(cityId);
		long nsCityTime = ucc.getNsTime_();
		GregorianCalendar cityTime = new GregorianCalendar();
		cityTime.setTimeInMillis(nsCityTime/1000l/1000l);
		GregorianCalendar now = new GregorianCalendar();
		now.setTimeInMillis(msNow);
		long diffNs = TimeTranslation.msToNs(msNow) - nsCityTime;
		Integer i = new Integer(0);
		Map<String,Object> ecoV = foodComp.getFoodAT(ucc, diffNs,i);
		if(ecoVar != null)ecoVar.putAll(ecoV);
		System.out.print("iteracje:" + i + "\n");
		return ucc;
	}

	@Override
	public int getSatisfaction(UsersCityConfig ucc) throws SecurityException,
			IllegalArgumentException, NoSuchMethodException,
			IllegalAccessException, InvocationTargetException,
			NoSuchFieldException {
		// TODO Auto-generated method stub
		return 0;
	}
	@Transactional(readOnly=false, propagation = Propagation.REQUIRES_NEW)
	public void setNewPeopleCount(UsersCityConfig uccAfter,int newPeopleCount) throws Exception
	{
		uccAfter.setMaxLudzi(newPeopleCount);
		cityDao.merge(uccAfter);
	}

	public CityCompUtil getFoodComp() {
		return foodComp;
	}

	public void setFoodComp(CityCompUtil foodComp) {
		this.foodComp = foodComp;
	}
	
}
