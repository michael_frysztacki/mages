package services.impl;

import helpers.TimeTranslation;

import java.lang.reflect.InvocationTargetException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.jsfsample.managedbeans.SpringJSFUtil;

import dao.UnitBuildHome;
import dao.UnitGroupHome;
import dao.UsersCityConfigHome;
import dto.DetailedUnitDTO;
import dto.UnitDTO;
import economycomputation.CityCompUtil;
import entities.Storage;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.ObjectBuildQueue;
import entities.buildings.UnitBuild;
import entities.units.CityUnitGroup;
import services.EconomyService;
import services.UnitService;

@Service("unitService")
@Transactional(readOnly = true)
public class UnitServiceImpl implements UnitService{

	@Autowired private UsersCityConfigHome cityDao;
	@Autowired private UnitBuildHome unitBuildDao;
	@Autowired private UnitGroupHome ugDao;
	@Autowired private EconomyService economyService;
	
	@Override
	//@Transactional(readOnly = true)
	public DetailedUnitDTO getDetailedUnit(UsersCityConfig ucc, int unitId)
	{
		CityUnitGroup ug = ucc.getArmy().getUnitGroupById(unitId);
		//ug  = ugDao.findById(ug.getId());
		return (DetailedUnitDTO)ug.createDetailDTO(ucc);
	}
	@Override
	@Transactional(readOnly = false)
	public int buildArmy(DetailedUnitDTO unit, int cityId, long msNow) throws Exception {
		if(unit.getBuildCount() <= 0)return 0;
		UsersCityConfig uccAfter = economyService.getEconomy(cityId, msNow);
		UsersCityConfig myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(uccAfter.getUserConfig().getCivil());
		Boostable bunit = myCivil.getById(unit.getId());
		int buildingId = bunit.getMotherBuilding();
		CityBuilding cb = uccAfter.getBuilding(buildingId);
		int buildC = cb.buildUnit(unit.getId(), unit.getBuildCount());
		cityDao.merge(uccAfter);
		return buildC;
	}

	public UnitBuildHome getUnitBuildDao() {
		return unitBuildDao;
	}

	public void setUnitBuildDao(UnitBuildHome unitBuildDao) {
		this.unitBuildDao = unitBuildDao;
	}

}
