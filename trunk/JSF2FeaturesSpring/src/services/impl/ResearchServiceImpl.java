package services.impl;

import java.io.Serializable;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import dao.UsersCityConfigHome;
import economycomputation.CityCompUtil;
import entities.UserResearch;
import entities.UsersCityConfig;
import game_model_impl.WorldDefinition;
import services.EconomyService;
import services.ResearchService;

public class ResearchServiceImpl implements ResearchService,Serializable{
	
	@Autowired private CityCompUtil foodComp;
	@Autowired private WorldDefinition ch;
	@Autowired private UsersCityConfigHome cityDao;
	@Autowired private EconomyService economyService;
	
	public UsersCityConfigHome getCityDao() {
		return cityDao;
	}
	public void setCityDao(UsersCityConfigHome cityDao) {
		this.cityDao = cityDao;
	}
	@Override
	@Transactional(readOnly = false,propagation = Propagation.REQUIRED)
	public boolean upgradeResearch(int researchId, int cityId, long msTime) throws Exception
	{
		UsersCityConfig ucc = economyService.getEconomy(cityId, msTime);
		UserResearch ur = ucc.getUserConfig().getResearch(researchId);
		if(!ur.upgrade(ucc))return false;
		cityDao.merge(ucc);
		return true;
	}
	
	public WorldDefinition getCh() {
		return ch;
	}
	public void setCh(WorldDefinition ch) {
		this.ch = ch;
	}
}
