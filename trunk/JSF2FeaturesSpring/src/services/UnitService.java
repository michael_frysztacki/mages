package services;

import java.lang.reflect.InvocationTargetException;
import java.util.GregorianCalendar;

import dto.DetailedUnitDTO;
import dto.UnitDTO;
import entities.UsersCityConfig;

public interface UnitService {

	public int buildArmy(DetailedUnitDTO unit, int cityId, long msNow) throws Exception;
	public DetailedUnitDTO getDetailedUnit(UsersCityConfig ucc, int unitId) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;
}
