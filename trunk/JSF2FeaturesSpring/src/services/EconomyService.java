package services;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entities.UsersCityConfig;
import game_model_impl.StockType;




public interface EconomyService {

	//public double getBirthRate(UsersCityConfig ucc) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;
	public int getSatisfaction(UsersCityConfig ucc) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;
	public UsersCityConfig getEconomy(int cityId, long msNow, Map<String,Object>... ecoVariables);
	public void setNewPeopleCount(UsersCityConfig ucc,int newPeopleCount) throws Exception;
	//public double getHiredPeople(UsersCityConfig ucc);
//	public NotFoodSet getResources(UsersCityConfig ucc) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;
}
