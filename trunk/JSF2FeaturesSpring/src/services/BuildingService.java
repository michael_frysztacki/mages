package services;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import dto.BuildingDTO;
import dto.DetailedExtractBuildingDTO;
import dto.DetailedBuildingDTO;
import dto.ExtractBuildingDTO;
import entities.UsersCityConfig;

public interface BuildingService {

	ArrayList<DetailedExtractBuildingDTO> getExtractAndConvertBuildings(UsersCityConfig ucc)throws Exception;
	public boolean upgradeBuilding(int buildingId, int cityId, long msNow) throws Exception;
	public boolean setPeopleInBuilding(UsersCityConfig ucc,int buildingId, int count) throws Exception;
	public DetailedExtractBuildingDTO getExtractBuilding(UsersCityConfig ucc,int buildingId) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;
	public ArrayList<BuildingDTO> getMilitaryBuildings(UsersCityConfig ucc) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;
	public DetailedBuildingDTO getCityBuilding(UsersCityConfig ucc, int buildingId) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;
	public ArrayList<BuildingDTO> getCityBuildings(UsersCityConfig ucc) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;
}
