package services;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Set;

import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;


public interface CityService {

	public Set getCities(UserConfig userConfig);
	public void mergeCity(UsersCityConfig uccAfter);
	public UsersCityConfig getCity(int id);
}
