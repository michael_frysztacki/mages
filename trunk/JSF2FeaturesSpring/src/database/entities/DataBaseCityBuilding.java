package database.entities;

import entities.UsersCityConfig;
import entities.buildings.BuildingBuild;
import entities.buildings.CityBuilding;
import entities.buildings.ObjectBuildQueue;

public class DataBaseCityBuilding extends CityBuilding{

	private int level;
	private int buildingId;
	private UsersCityConfig myCity;
	private ObjectBuildQueue obq;
	private BuildingBuild bb;
	
	@Override
	public int getLevel() {
		return level;
	}

	@Override
	public int getBuildingId() {
		return buildingId;
	}

	@Override
	protected UsersCityConfig getCity() {
		return myCity;
	}

	@Override
	protected ObjectBuildQueue getObjectBuildQueue() {
		return obq;
	}

	@Override
	protected BuildingBuild getBuildingBuild() {
		return bb;
	}

	@Override
	protected void setLevel(int level) {
		this.level = level;
	}

	@Override
	protected void setMyCity(UsersCityConfig city) {
		this.myCity = city;
	}

	@Override
	protected void setObjectBuildQueue(ObjectBuildQueue obq) {
		this.obq = obq;
	}

	@Override
	protected void setBuildingBuild(BuildingBuild bb) {
		this.bb = bb;
	}

	@Override
	protected void setBuildingId(int buildingId) {
		this.buildingId = buildingId;
		
	}
	
}