package database.entities;

import entities.stocks.CityStockSet;
import entities.stocks.TaxStock;

public class DataBaseTaxStock extends TaxStock{

	private double amount;
	private CityStockSet cityStocks;
	@Override
	protected double getAmount() {
		return amount;
	}

	@Override
	protected void setAmount(double amount) {
		this.amount=amount;
	}

	@Override
	protected CityStockSet getCityStocks() {
		return cityStocks;
	}

	@Override
	protected void setCityStocks(CityStockSet cityStocks) {
		this.cityStocks=cityStocks;
	}

}
