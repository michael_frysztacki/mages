package database.entities;

import entities.stocks.CityPeople;
import entities.stocks.CityStockSet;

public class DataBaseCityPeople extends CityPeople {

	private double amount;
	private CityStockSet cityStocks;
	private boolean isRaising;
	@Override
	protected double getAmount() {
		return amount;
	}

	@Override
	protected void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	protected CityStockSet getCityStocks() {
		return cityStocks;
	}

	@Override
	protected void setCityStocks(CityStockSet cityStocks) {
		this.cityStocks=cityStocks;
	}

	@Override
	protected boolean isRaising() {
		return isRaising;
	}

	@Override
	protected void setIsRaising(boolean isRaising) {
		this.isRaising = isRaising;
	}

}
