package database.entities;

import entities.stocks.CityResource;
import entities.stocks.CityStockSet;

public class DataBaseCityResource extends CityResource{

	private String resourceId;
	private double amount;
	private CityStockSet cityStocks;
	@Override
	public String getStockId() {
		return resourceId;
	}

	@Override
	protected void setStockId(String stockId) {
		this.resourceId=stockId;
	}

	@Override
	protected double getAmount() {
		return amount;
	}

	@Override
	protected void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	protected CityStockSet getCityStocks() {
		return cityStocks;
	}

	@Override
	protected void setCityStocks(CityStockSet cityStocks) {
		this.cityStocks=cityStocks;
	}

}
