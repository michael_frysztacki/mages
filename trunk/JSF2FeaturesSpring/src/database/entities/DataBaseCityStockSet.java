package database.entities;

import java.util.List;

import entities.UsersCityConfig;
import entities.stocks.CityStock;
import entities.stocks.CityStockSet;

public class DataBaseCityStockSet extends CityStockSet{

	private List<CityStock> cityStocks;
	private UsersCityConfig city;
	@Override
	protected void setStocks(List<CityStock> cityStocks) {
		this.cityStocks=cityStocks;
	}

	@Override
	protected UsersCityConfig getCity() {
		return city;
	}

	@Override
	protected void setCity(UsersCityConfig city) {
		this.city=city;
	}

	@Override
	protected List<CityStock> getStocks() {
		return cityStocks;
	}

}
