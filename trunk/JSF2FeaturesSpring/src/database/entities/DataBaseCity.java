package database.entities;

import java.util.List;

import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.stocks.CityStockSet;
import entities.units.CityArmy;

public class DataBaseCity extends UsersCityConfig {

	private UserConfig uc;
	private List<CityBuilding> cityBuildings;
	private List<ExtractCityBuilding> extractCityBuildings;
	private CityStockSet cityStocks;
	private int taxSatisfaction;
	private long cityTimeNs;
	private int pointId;
	@Override
	public UserConfig getUserConfig() {
		return uc;
	}

	@Override
	public List<CityBuilding> getBuildings() {
		return cityBuildings;
	}

	@Override
	public List<ExtractCityBuilding> getExtractBuildings() {
		return extractCityBuildings;
	}

	@Override
	public int getTaxSatisfaction() {
		return taxSatisfaction;
	}

	@Override
	public CityStockSet getCityStocks() {
		return cityStocks;
	}

	@Override
	public long getCityTimeNs_() {
		return cityTimeNs;
	}

	@Override
	protected void setBuildings(List<CityBuilding> buildings) {
		this.cityBuildings = buildings;
	}

	@Override
	protected void setExtractBuildings(List<ExtractCityBuilding> buildings) {
		this.extractCityBuildings = buildings;
	}

	@Override
	protected void setUserConfig(UserConfig player) {
		this.uc = player;
	}

	@Override
	protected void setCityArmy(CityArmy cityArmy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setCityTimeNs_(long cityTimeNs_) {
		this.cityTimeNs = cityTimeNs_;
		
	}

	@Override
	protected void setPointId(int pointId) {
		this.pointId = pointId;
	}

	@Override
	protected void setCityStocks(CityStockSet cityStocks) {
		this.cityStocks = cityStocks;
	}

	@Override
	protected void setTaxSatisfaction(int taxSatisfaction) {
		this.taxSatisfaction = taxSatisfaction;
	}

	@Override
	protected int getPointId() {
		return pointId;
	}

}
