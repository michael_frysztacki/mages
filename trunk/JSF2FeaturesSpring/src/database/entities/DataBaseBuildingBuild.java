package database.entities;

import entities.buildings.BuildingBuild;
import entities.buildings.CityBuilding;


public class DataBaseBuildingBuild extends BuildingBuild {

	private CityBuilding cb;
	private long doneTimeNs;

	@Override
	protected CityBuilding getCityBuilding() {
		return cb;
	}

	@Override
	protected long getDoneTimeNs() {
		
		return doneTimeNs;
	}

	@Override
	protected void setDoneTimeNs(long doneTime) {
		
		this.doneTimeNs = doneTime;
	}

	@Override
	protected void setCityBuilding(CityBuilding myBuilding) {
		this.cb = myBuilding;
		
	}
	
}
