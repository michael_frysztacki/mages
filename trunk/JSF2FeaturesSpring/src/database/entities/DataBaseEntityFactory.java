package database.entities;

import entities.EntityFactory;
import entities.UsersCityConfig;
import entities.buildings.BuildingBuild;
import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.buildings.ObjectBuildQueue;
import entities.stocks.CityFood;
import entities.stocks.CityPeople;
import entities.stocks.CityResource;
import entities.stocks.CityStockSet;
import entities.stocks.TaxStock;
import entities.units.CityArmy;

public class DataBaseEntityFactory extends EntityFactory{

	@Override
	protected CityStockSet instantiateCityStockSet() {
		return new DataBaseCityStockSet();
	}

	@Override
	protected CityResource instantiateCityResource() {
		return new DataBaseCityResource();
	}

	@Override
	protected TaxStock instantiateTaxStock() {
		return new DataBaseTaxStock();
	}

	@Override
	protected BuildingBuild instantiateBuildBuild() {
		return new DataBaseBuildingBuild();
	}

	@Override
	protected ExtractCityBuilding instantiateExtractCityBuilding() {
		return new DataBaseExtractCityBuilding();
	}

	@Override
	protected CityBuilding instantiateCityBuilding() {
		return new DataBaseCityBuilding();
	}

	@Override
	protected CityArmy instantiateCityArmy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ObjectBuildQueue instantiateObjectBuildQueue() {
		return new DataBaseObjectBuildQueue();
	}

	@Override
	protected CityFood instantiateCityFood() {
		return new DataBaseCityFood();
	}

	@Override
	protected CityPeople instantiateCityPeople() {
		return new DataBaseCityPeople();
	}

	@Override
	protected UsersCityConfig instantiateCity() {
		return new DataBaseCity();
	}


}
