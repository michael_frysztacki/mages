package database.entities;

import entities.UsersCityConfig;
import entities.buildings.BuildingBuild;
import entities.buildings.ExtractCityBuilding;
import entities.buildings.ObjectBuildQueue;

public class DataBaseExtractCityBuilding extends ExtractCityBuilding{

	private int buildingId;
	private int population;
	private int level;
	private UsersCityConfig city;
	private ObjectBuildQueue obq;
	private BuildingBuild bb;
	@Override
	public int getPopulation() {
		return population;
	}

	@Override
	protected void setPopulation(int population) {
		this.population = population;
	}

	@Override
	protected void setBuildingId(int buildingId) {
		this.buildingId = buildingId;
	}

	@Override
	public int getLevel() {
		return level;
	}

	@Override
	public int getBuildingId() {
		return buildingId;
	}

	@Override
	protected void setMyCity(UsersCityConfig city) {
		this.city = city;
	}

	@Override
	protected void setObjectBuildQueue(ObjectBuildQueue obq) {
		this.obq = obq;
	}

	@Override
	protected void setBuildingBuild(BuildingBuild bb) {
		this.bb = bb;
	}

	@Override
	protected UsersCityConfig getCity() {
		return city;
	}

	@Override
	protected ObjectBuildQueue getObjectBuildQueue() {
		return obq;
	}

	@Override
	protected BuildingBuild getBuildingBuild() {
		return bb;
	}

	@Override
	protected void setLevel(int level) {
		this.level = level;
	}

}
