package database.entities;

import entities.stocks.CityFood;
import entities.stocks.CityStockSet;

public class DataBaseCityFood extends CityFood{

	private String foodId;
	private double amount;
	private long raiseTimeNs;
	private CityStockSet cityStocks;
	@Override
	public String getStockId() {
		return foodId;
	}

	@Override
	protected long getFoodRaiseTimeNs_() {
		return raiseTimeNs;
	}

	@Override
	protected void setFoodRaiseTimeNs_(long raiseTimeNs) {
		this.raiseTimeNs=raiseTimeNs;
	}

	@Override
	protected void setStockId(String stockId) {
		this.foodId=stockId;
	}

	@Override
	protected double getAmount() {
		return amount;
	}

	@Override
	protected void setAmount(double amount) {
		this.amount=amount;
	}

	@Override
	protected CityStockSet getCityStocks() {
		return cityStocks;
	}

	@Override
	protected void setCityStocks(CityStockSet cityStocks) {
		this.cityStocks=cityStocks;
	}

}
