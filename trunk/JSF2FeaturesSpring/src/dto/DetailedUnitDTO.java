package dto;

import java.util.ArrayList;
import java.util.List;


import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.UserConfig;
import entities.UsersCityConfig;
import entities.units.CityUnitGroup;
import game_model_impl.PriceStockSetImpl;
import game_model_impl.Requirement;
import game_model_interfaces.PriceStock;

public class DetailedUnitDTO extends UnitDTO{

	public DetailedUnitDTO(String name, String description, int count, int id) {
		super(name, description, count, id);
		// TODO Auto-generated constructor stub
	}
	public DetailedUnitDTO(CityUnitGroup cug)
	{
		UserConfig uc = cug.getMyArmy().getMyCity().getUserConfig();
		UsersCityConfig myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization( cug.getMyArmy().getMyCity().getUserConfig().getCivil());
		Boostable unit = myCivil.getById( cug.getUnitId() );
		this.setId(cug.getUnitId());
		this.setName(unit.getName(uc));
		this.setDescription(unit.getDescription(uc));
		this.setTime(new TimeDTO(unit.getBuildTime(cug.getMyArmy().getMyCity())));
		this.setCount( (int)Math.floor(cug.getCount()) );
		PriceStockSetImpl price = unit.getPrice(null);
		List<Requirement> reqs = unit.getRequirementNode();
		List<PriceDTO> pricel = new ArrayList<PriceDTO>();
		for(PriceStock ps : price.getStocks() )
		{
			if(ps.getVisibleAmount() != 0)
			{
				pricel.add( new PriceDTO(ps) );
			}
		}
		this.setPrice(pricel);
		List<RequirementDTO> reql = new ArrayList<RequirementDTO>();
		Map<Integer,Integer> reqMap = new HashMap<Integer,Integer>();
		this.canBuild_Requirements(where, reqMap);
		for(Requirement r : reqs)
		{
			for(int l_parent : r.parents)
			{
				RequirementDTO rdto = new RequirementDTO();
				Boostable parent =  myCivil.getById(l_parent);
				rdto.setParentId(l_parent);
				rdto.setParentName(parent.getName(getUc()));
				rdto.setReqLvl(reqMap.get(l_parent));
				reql.add(rdto);
			}
		}
		this.setReqs(reql);
	}
	
	private int buildCount;
	public List<RequirementDTO> getReqs() {
		return reqs;
	}
	public void setReqs(List<RequirementDTO> reqs) {
		this.reqs = reqs;
	}
	public List<PriceDTO> getPrice() {
		return price;
	}
	public void setPrice(List<PriceDTO> price) {
		this.price = price;
	}
	public TimeDTO getTime() {
		return time;
	}
	public void setTime(TimeDTO time) {
		this.time = time;
	}
	public int getBuildCount() {
		return buildCount;
	}
	public void setBuildCount(int buildCount) {
		this.buildCount = buildCount;
	}
	private TimeDTO time;
	private List<RequirementDTO> reqs;
	private List<PriceDTO> price;
	
}
