package dto;

import java.io.Serializable;

import entities.stocks.CityResource;
import entities.stocks.CityStock;
import framework.IStock;
import game_model_impl.StockType;


public class ConcreteResourceDTO implements IStock<Integer>,Serializable{

	private int count;
	private String type;
	private StockType stockType;
	public ConcreteResourceDTO(int count, String rt)
	{
		this.count = count;
		this.type = rt;
	}
	public ConcreteResourceDTO(CityStock cr)
	{
		this.count = (int)Math.floor(cr.getVisibleAmount());
		this.type = cr.getStockType().toString();
		this.stockType = cr.getStockType();
	}
	public String getType() {
		return type;
	}
	public void setType(String resourceType) {
		this.type = resourceType;
	}
	@Override
	public int getVisibleAmount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	@Override
	public StockType getStockType() {
		// TODO Auto-generated method stub
		return stockType;
	}
}
