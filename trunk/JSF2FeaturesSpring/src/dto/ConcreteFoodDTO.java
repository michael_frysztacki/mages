package dto;

import java.io.Serializable;




import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Storage;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;
import entities.stocks.CityFood;
import game_model_impl.StockType;


public class ConcreteFoodDTO implements Serializable{

	private int buildingId;
	private int maxBuildingPeople;
	private int currentPeople;
	private Number count;
	private Double extractionPerHour;
	private Storage storage;
	private StockType type;
	private int lode;
	
	public ConcreteFoodDTO(Number number, Storage storage, StockType type)
	{
		if(storage == Storage.nie || storage == Storage.nastyk)this.count = 0.0d;
		else this.count= number;
		this.storage = storage;
		this.type = type;
	}
	
	public ConcreteFoodDTO(CityFood cf,UsersCityConfig city)
	{
		this.type = cf.getStockType();
		this.count = (int)Math.floor(cf.getVisibleAmount());
		this.storage = cf.getStorage();
		UsersCityConfig civil = SpringJSFUtil.getCivilizationHolder().getCivilization(city.getUserConfig().getCivil());
		Boostable producer = civil.getProducer(city.getUserConfig(), cf.getStockType());
		
		this.extractionPerHour = (double)producer.getOutputPerHour(city, cf.getStockType());
		this.buildingId = producer.getId();
		this.maxBuildingPeople = producer.getMaxPeople(city);
		this.currentPeople = city.getBuilding(ExtractCityBuilding.class,producer.getId()).getPopulation();
		this.lode = city.getPoint().getCityLode(cf.getStockType()).getValue();
	}
	public Number getCount() {
		return count;
	}
	public void setCount(Number count) {
		this.count = count;
	}
	public Storage getStorage() {
		return storage;
	}
	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	public StockType getType() {
		return type;
	}
	public void setType(StockType type) {
		this.type = type;
	}

	public Double getExtractionPerHour() {
		return extractionPerHour;
	}

	public void setExtractionPerHour(Double extractionPerHour) {
		this.extractionPerHour = extractionPerHour;
	}

	public int getMaxBuildingPeople() {
		return maxBuildingPeople;
	}

	public void setMaxBuildingPeople(int maxBuldingPeople) {
		this.maxBuildingPeople = maxBuldingPeople;
	}

	public int getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(int buildingId) {
		this.buildingId = buildingId;
	}

	public int getCurrentPeople() {
		return currentPeople;
	}

	public void setCurrentPeople(int currentPeople) {
		this.currentPeople = currentPeople;
	}

	public int getLode() {
		return lode;
	}

	public void setLode(int lode) {
		this.lode = lode;
	}
}
