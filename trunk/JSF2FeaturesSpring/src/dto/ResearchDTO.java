package dto;


import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.UserConfig;
import entities.UsersCityConfig;

public class ResearchDTO{

	public ResearchDTO(int researchId, UserConfig owner)
	{
		UsersCityConfig myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(owner.getCivil());
		this.id = researchId;
		this.level = owner.getResearch(researchId).getLevel();
		Boostable thisR = myCivil.getById(researchId);
		this.setName(thisR.getName(owner));
	}
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private int level,id;
	
}
