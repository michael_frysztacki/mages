package dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;




import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.UserResearch;
import entities.UsersCityConfig;
import entities.units.CityUnitGroup;
import game_model_impl.PriceStockSetImpl;
import game_model_interfaces.PriceStock;

public class DetailedBuildingDTO implements Serializable{

	public DetailedBuildingDTO(){};
	public DetailedBuildingDTO(UsersCityConfig city,int buildingId)
	{
		UsersCityConfig myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(city.getUserConfig().getCivil());
		Boostable cityBuilding = myCivil.getById(buildingId);
		List<Integer> units = cityBuilding.getUnits(null);
		for(Integer unitId : units)
		{
			CityUnitGroup unit = city.getMyArmy().getUnitGroupById(unitId);
			UnitDTO udto =  new UnitDTO(city.getUserConfig(), unit);
			this.getUnits().add(udto);
		}
		List<Integer> researches = cityBuilding.getResearches(null);
		for(Integer rId : researches)
		{
			UserResearch re = city.getUserConfig().getResearch(rId);
			ResearchDTO rdto =  new ResearchDTO(re.getObjectId(), city.getUserConfig());
			this.getResearches().add(rdto);
		}
		PriceStockSetImpl cost = cityBuilding.getPrice(city);
		for(PriceStock ps : cost.getStocks() )
		{
			this.getCost().add(new PriceDTO(ps));
		}
		this.setTime(new TimeDTO(cityBuilding.getBuildTime(city)));
		this.setName(cityBuilding.getName(city.getUserConfig()));
		this.setDescription(cityBuilding.getDescription(city.getUserConfig()));
		this.setId(cityBuilding.getId());
		this.setLevel(city.getLevel(cityBuilding.getId()));
		
	}
	
	private String name,description;
	private int id, level;
	private List<PriceDTO> cost = new ArrayList<PriceDTO>();
	private List<UnitDTO> units = new ArrayList<UnitDTO>();
	private List<ResearchDTO> researches = new ArrayList<ResearchDTO>();
	private TimeDTO time;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	
	public List<UnitDTO> getUnits() {
		return units;
	}
	public void setUnits(List<UnitDTO> units) {
		this.units = units;
	}
	public List<ResearchDTO> getResearches() {
		return researches;
	}
	public void setResearches(List<ResearchDTO> researches) {
		this.researches = researches;
	}
	public TimeDTO getTime() {
		return time;
	}
	public void setTime(TimeDTO time) {
		this.time = time;
	}
	public List<PriceDTO> getCost() {
		return cost;
	}
	public void setCost(List<PriceDTO> cost) {
		this.cost = cost;
	}
	
}
