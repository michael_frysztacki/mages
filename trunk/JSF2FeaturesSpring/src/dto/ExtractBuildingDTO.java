package dto;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;



import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;
import game_model_impl.Building;
import game_model_impl.GameModel;
import game_model_impl.Requirement;
import game_model_impl.StockType;





public class ExtractBuildingDTO implements Serializable{
	
	//private Building[] motherBuildings;
	private int maxPeople,minPeople,actualPeople,level;
	private int production;
	private String name;
	public String getName() {
		return name;
	}

	public ExtractBuildingDTO(ExtractCityBuilding ecb)
	{
		UsersCityConfig civil = SpringJSFUtil.getCivilizationHolder().getCivilization(ecb.getUcc().getUserConfig().getCivil());
		Boostable thisB = civil.getById(ecb.getObjectId());
		this.setActualPeople( ecb.getPopulation() );
		this.setId( ecb.getObjectId() );
		this.setLevel(this.getLevel());
		this.setMaxPeople(thisB.getMaxPeople(ecb.getUcc()));
		this.setMinPeople(thisB.getMinPeople(ecb.getUcc()));
		this.setName(thisB.getName(ecb.getUcc().getUserConfig()));
		this.setProduction(thisB.getOutputPerHour(ecb.getUcc(), thisB.getResourceType()));
		this.setType(thisB.getResourceType());
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	private int id;
	private StockType type;

	public void setMaxPeople(int maxPeople) {
		this.maxPeople = maxPeople;
	}

	public void setMinPeople(int minPeople) {
		this.minPeople = minPeople;
	}

	public int getMaxPeople()
	{
		// TODO Auto-generated method stub
		return maxPeople;
	}

	public int getMinPeople()
	{
		// TODO Auto-generated method stub
		return minPeople;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getActualPeople() {
		return actualPeople;
	}

	public void setActualPeople(int actualPeople) {
		this.actualPeople = actualPeople;
	}


	public int getProduction() {
		return production;
	}

	public void setProduction(int production) {
		this.production = production;
	}

	public StockType getType() {
		return type;
	}

	public void setType(StockType type) {
		this.type = type;
	}
			

}
