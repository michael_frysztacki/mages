package dto;

import java.io.Serializable;



import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.UserConfig;
import entities.UsersCityConfig;
import entities.units.IUnitGroup;


public class UnitDTO implements Serializable{

	private String name, description;
	private int count,id;
	public UnitDTO(UserConfig owner, IUnitGroup<Double> ug)
	{
		UsersCityConfig myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(owner.getCivil());
		String name = myCivil.getById( ug.getUnitId() ).getName(owner);
		String description = myCivil.getById(ug.getUnitId()).getDescription(owner);
		this.name = name;
		this.description = description;
		this.count = (int)Math.floor(ug.getCount());
		this.id = ug.getUnitId();
	}
	UnitDTO(){};
	UnitDTO(String name, String description, int count, int id) {
		
		this.name = name;
		this.description = description;
		this.count = count;
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
