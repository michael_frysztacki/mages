package dto;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;



import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.UserConfig;
import entities.UsersCityConfig;
import game_model_impl.Building;
import game_model_impl.GameModel;
import game_model_impl.Requirement;




public class BuildingDTO implements Serializable{
	
	
	public BuildingDTO(int buildingId, UsersCityConfig city)
	{
		UsersCityConfig myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(city.getUserConfig().getCivil());
		Boostable building = myCivil.getById(buildingId);
		this.setName(building.getName(city.getUserConfig()));
		this.setLevel(city.getLevel(buildingId));
		this.setId(buildingId);
	}
	private BuildingDTO(){};
	private List<Requirement> requirements;
	public List<Requirement> getRequirements() {
		return requirements;
	}
	public void setRequirements(List<Requirement> requirements) {
		this.requirements = requirements;
	}
	public List<UnitDTO> getUnits() {
		return units;
	}
	public void setUnits(List<UnitDTO> units) {
		this.units = units;
	}
	public List<ResearchDTO> getResearches() {
		return researches;
	}
	public void setResearches(List<ResearchDTO> researches) {
		this.researches = researches;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getBuildTime() {
		return buildTime;
	}
	public void setBuildTime(double buildTime) {
		this.buildTime = buildTime;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	private List<UnitDTO> units = new ArrayList<UnitDTO>();
	private List<ResearchDTO> researches = new ArrayList<ResearchDTO>();
	private int level,id;
	private double buildTime;
	private String name,description;
	
			

}
