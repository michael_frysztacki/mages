package dto;

public class FoodConsumptionDTO{

	private String consumer;
	private double count;
	private double eatPerHour;
	
	public FoodConsumptionDTO(String consumer, double count, double eph)
	{
		this.consumer = consumer;
		this.count = count;
		this.eatPerHour = eph;
	}
	
	public String getConsumer() {
		return consumer;
	}
	public void setConsumer(String consumer) {
		this.consumer = consumer;
	}
	public double getCount() {
		return count;
	}
	public void setCount(double count) {
		this.count = count;
	}
	public double getEatPerHour() {
		return eatPerHour;
	}
	public void setEatPerHour(double eatPerHour) {
		this.eatPerHour = eatPerHour;
	}
}
