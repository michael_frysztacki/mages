package dto;

import framework.IStock;
import game_model_impl.StockType;

public class PriceDTO implements IStock<Integer>{

	private int count;
	private StockType type;
	public PriceDTO(IStock<Integer> priceStock)
	{
		type = priceStock.getStockType();
		count = priceStock.getVisibleAmount();
	}
	@Override
	public int getVisibleAmount() {
		// TODO Auto-generated method stub
		return count;
	}

	@Override
	public StockType getStockType() {
		// TODO Auto-generated method stub
		return type;
	}

}
