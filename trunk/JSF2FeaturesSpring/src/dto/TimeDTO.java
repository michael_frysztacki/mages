package dto;

public class TimeDTO{

	private int hours,minutes,seconds;

	public TimeDTO(Long ms)
	{
		this.hours = (int) Math.floor((double)ms/3600000.0d);
		int rest = (int)(ms % (long)3600000);
		this.minutes = (int)Math.floor(rest / ((long)60*(long)1000));
		rest = rest % (60*1000);
		this.seconds = rest/1000;
	}
	public String toString()
	{
		return hours+"h "+minutes+"min "+seconds+"sec";
	}
	public int getSeconds() {
		return seconds;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}
	
}
