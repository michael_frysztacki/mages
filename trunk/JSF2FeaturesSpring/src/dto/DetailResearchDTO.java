package dto;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;




import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.UserResearch;
import entities.UsersCityConfig;
import game_model_impl.PriceStockSetImpl;
import game_model_interfaces.PriceStock;

public class DetailResearchDTO extends ResearchDTO{

	//name , level , id jest juz zaimplementowane w kalsie wyzej
	public DetailResearchDTO(UserResearch ur, UsersCityConfig where)
	{
		super(ur.getObjectId(), ur.getUserConfig());
		UsersCityConfig myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(ur.getUserConfig().getCivil());
		this.setId( ur.getObjectId() );
		this.setLevel(this.getLevel());
		Boostable thisR = myCivil.getById( ur.getObjectId() );
		this.setName(thisR.getName( ur.getUserConfig()));
		this.setDescription(thisR.getDescription( ur.getUserConfig()));
		GregorianCalendar bt = new GregorianCalendar();
		this.setTime(new TimeDTO(thisR.getBuildTime(where)));
		PriceStockSetImpl cost = thisR.getPrice(where);
		for(PriceStock cr : cost.getStocks() )
		{
			if(cr.getVisibleAmount()!= 0.0d)this.getCost().add(new PriceDTO(cr));
		}
	}
	private String description;
	private TimeDTO time;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<PriceDTO> getCost() {
		return cost;
	}
	public void setCost(List<PriceDTO> cost) {
		this.cost = cost;
	}
	public List<RequirementDTO> getReqs() {
		return reqs;
	}
	public void setReqs(List<RequirementDTO> reqs) {
		this.reqs = reqs;
	}
	
	public TimeDTO getTime() {
		return time;
	}
	public void setTime(TimeDTO time) {
		this.time = time;
	}

	private List<PriceDTO> cost  = new ArrayList<PriceDTO>();
	private List<RequirementDTO> reqs =  new ArrayList<RequirementDTO>();
}
