package dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.UserResearch;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.units.CityUnitGroup;
import game_model_impl.StockType;


public class DetailedExtractBuildingDTO extends DetailedBuildingDTO implements Serializable{

	private int maxPeople,minPeople,actualPeople;
	private int production;
	private StockType type;
	
	public DetailedExtractBuildingDTO(CityBuilding cb)
	{
		UsersCityConfig myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization( cb.getUcc().getUserConfig().getCivil());
		Boostable extractBuilding = myCivil.getById(cb.getId());
		this.setName(extractBuilding.getName(cb.getUcc().getUserConfig()));
		this.setDescription(extractBuilding.getDescription(cb.getUcc().getUserConfig()));
		this.setId(cb.getId());
		this.setLevel(cb.getUcc().getLevel(cb.getId()));
		List<Integer> units = extractBuilding.getUnits(null);
		//List<UnitDTO> unitsDto = new ArrayList<UnitDTO>();
		//Set unitsSet = ucc.getArmy().getUnits();
		for(Integer unitId : units)
		{
			CityUnitGroup unit = cb.getUcc().getMyArmy().getUnitGroupById(unitId);
			UnitDTO udto =  new UnitDTO(cb.getUc(), unit);
			this.getUnits().add(udto);
		}
		List<Integer> researches = extractBuilding.getResearches(null);
		for(Integer rId : researches)
		{
			UserResearch re = cb.getUcc().getUserConfig().getResearch(rId);
			ResearchDTO rdto =  new ResearchDTO(re.getObjectId(), cb.getUc());
			this.getResearches().add(rdto);
		}
//		NotFoodSet_dead cost = extractBuilding.getPrice(this.getUcc(), null);
//		for(CityResource cr : cost.getResources())
//		{
//			if(cr.getCount()!= 0.0d)this.getCost().add(new ConcthiseResourceDTO(cr));
//		}
		this.setTime(new TimeDTO(extractBuilding.getBuildTime(cb.getUcc())));
		this.setProduction(extractBuilding.getOutputPerHour(cb.getUcc(), extractBuilding.getResourceType()));
		this.setType(extractBuilding.getResourceType());
		this.setActualPeople(cb.getUcc().getPopulation(cb.getObjectId()));
		this.setMaxPeople(extractBuilding.getMaxPeople(cb.getUcc()));
		this.setMinPeople(extractBuilding.getMinPeople(cb.getUcc()));
	}
	public int getMaxPeople() {
		return maxPeople;
	}
	public void setMaxPeople(int maxPeople) {
		this.maxPeople = maxPeople;
	}
	public int getMinPeople() {
		return minPeople;
	}
	public void setMinPeople(int minPeople) {
		this.minPeople = minPeople;
	}
	public int getActualPeople() {
		return actualPeople;
	}
	public void setActualPeople(int actualPeople) {
		this.actualPeople = actualPeople;
	}
	public int getProduction() {
		return production;
	}
	public void setProduction(int production) {
		this.production = production;
	}

	public StockType getType() {
		return type;
	}
	public void setType(StockType type) {
		this.type = type;
	}

	
}
