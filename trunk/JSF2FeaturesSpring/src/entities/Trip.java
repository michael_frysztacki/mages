package entities;
// default package
// Generated 2012-09-13 22:47:10 by Hibernate Tools 3.4.0.CR1

import helpers.TimeTranslation;

import java.util.ArrayList;
import java.util.List;
import entities.stocks.TripStockSet;
import entities.units.CityArmy;
import entities.units.TripArmy;

public abstract class Trip implements java.io.Serializable{

	private int id;
	private UsersCityConfig startCity;
	private UsersCityConfig endCity;
	private long nsStartTime;
	public long getNsStartTime() {
		return nsStartTime;
	}

	public void setNsStartTime(long nsStartTime) {
		this.nsStartTime = nsStartTime;
	}

	private TripStockSet tripStocks;
	/*
	 * czas podr�y w jedna strone
	 */
	private long msTravelTime;
	/*
	 * punkty po�rednie podr�y.
	 * Wszystkie punkty to podr�y w odpowiednej kolejnosci to:
	 * startCity.point -> points -> endCity.point
	 */
	private List<Point> points = new ArrayList<Point>();
	//private Mission mission;

//	public Trip(Trip trip)
//	{
//		this.id = trip.id;
//		
//	}
	private TripArmy tripArmy;
	
//	@Override
//	public Object clone()
//	{
//		try {
//			Trip trip = (Trip)super.clone();
//			trip.nsStartTime = this.nsStartTime;
//			//trip.resources = new NotFoodSet_dead(this.getResources());
//			//trip.food = new CityStockSet(this.getFood());
//			trip.endCity = this.getEndCity();
//			trip.startCity = this.getStartCity();
//			trip.points = this.getPoints();
//			trip.tripArmy = new CityArmy(this.getTripArmy());
//			return trip;
//		} catch (CloneNotSupportedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
//	}
	
	@Override
	public boolean equals(Object o)
	{
		Trip trip = (Trip)o;
		if(trip.getId() == this.getId())return true;
		else return false;
	}
	@Override
	public int hashCode() {
		int ret = 37;
		ret *= this.id;
		return ret;
	}
	
	Trip() {
	}

	public Trip(UsersCityConfig startCity,
			UsersCityConfig endCity,
			long nsStartTime_,
			TripArmy army,
			TripStockSet tripStocks,
			long msTravelTime
			) {
		this.nsStartTime = nsStartTime_;
		this.startCity = startCity;
		this.endCity = endCity;
		this.tripArmy = army;
		this.setTripStocks(tripStocks);
		this.setMsTravelTime(msTravelTime);
	}
	
	public abstract void syncReturnedTripWithCity();
	public abstract List<Report> getTripReports();
	/*
	 * metoda zwraca czy trip musi byc skalkulowany na nowo
	 * Trip musi byc skalkulowany na nowo, kiedy odbyla sie jakas dodatkowa walka, jakies zdarzenie,
	 * ktorego ten trip jeszcze nie uwzglednia.
	 */
	protected abstract boolean isUpToDate(long nsNow_);
	/*
	 * metoda zwraca czas stanu armi zapisanego w tym tripie w ns.
	 * Przyk�ad:
	 * Po przeprowadzonym ataku stan armi zmienia sie, wiec getCurrentTime
	 * zwroci czas w ktorym odby�a sie walka tzn w ktorym stan tej armi sie zmieni�
	 */
	public abstract long getCurrentTime_();
	/*
	 * symuluje trip.
	 * Je�li w tripie odbylo sie jakies zdarzenie , wywolanie tej metody, spowoduje
	 * skalkulowanie spotkania ( moze to byc rabunek , scout, oblezenie ).
	 * Jesli jest to rabunek, przeprowadzi symulacje walki, odbierze surowce miastu broniacemu,
	 * jesli atakujacy wygra, generuje raporty tripu, generalnie uaktualnia ca�y trip.
	 * w parametrze forAggressor podajemy czy chcemy skalkulowac trip dla atakujacego czy dla broniacego
	 * Jesli dla atakujacego, createTripAfter nie bedzie wprowadzac zmiana w miescie broniacego,
	 * bo z perspektywy gracza, liczy sie dla mnie tylko stan mojej armi.
	 * Jesli miasto broniace chce zobaczyc co sie stalo po walce z jego miastem, musi uruchomic ta metoda
	 * z parametrem false, czyli dla broniacego.
	 */
	public abstract Trip createTripAfter(long nsNow_,boolean forAggresor);
	/*
	 * metoda zwraca czas w godzinach do nastepnej interakcji miedzy mastami.
	 * Jesli nie ma juz wiecej zadnej interakcji zwroc null.
	 * Je�li czas do nastepnej interakcji jest r�wny czasowi podanemu jako now, zwroc null,
	 * bo jak zwrocisz 0 to sie bedzie fukcjna rekurencyjna zapetla� ( ca�y czas b�dzie + 0 dodawa� do czasu miasta ).
	 * Rozumiane tutaj interakcje dotycza tylko interakcji w miescie broniacego, a to znaczy
	 * ze nie jest tutaj uwzgledniany czas powrotu , bo to juz dotyczy miasta z ktorego zostaly 
	 * wyslane wojska.
	 */
	public abstract Long timeLeftToNextInteraction(long nsTime_);
	/*
	 * metoda zwraca czy miasto moze juz usunac trip ze swojej listy.
	 * Miasto wysylajace armie moze usunac trip ze swojej listy po powrocie armi,
	 * a miasto broniace po ostatnim ataku.
	 */
	public abstract boolean canRemove(boolean isAggressor, long nsNow_);
	/*
	 * zwraca czas w ns dotarcie na miejsce
	 */
	public long getArriveTime_()
	{
		return TimeTranslation.msToNs(this.getMsTravelTime()) + this.getNsStartTime();
	}
	/*
	 * zwraca czas powrotu armi w ns od 1970
	 */
	public abstract Long getReturnTime_();
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public UsersCityConfig getStartCity() {
		return startCity;
	}

	public void setStartCity(UsersCityConfig startCity) {
		this.startCity = startCity;
	}

	public UsersCityConfig getEndCity() {
		return endCity;
	}

	public void setEndCity(UsersCityConfig endCity) {
		this.endCity = endCity;
	}

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}


	public TripArmy getTripArmy() {
		return tripArmy;
	}

	public void setTripArmy(TripArmy tripArmy) {
		this.tripArmy = tripArmy;
	}

	public long getMsTravelTime() {
		return msTravelTime;
	}

	public void setMsTravelTime(long msTravelTime) {
		this.msTravelTime = msTravelTime;
	}

	public TripStockSet getTripStocks() {
		return tripStocks;
	}

	public void setTripStocks(TripStockSet tripStocks) {
		this.tripStocks = tripStocks;
	}

}
