package entities.stocks;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import economycomputation.EcoCalculator;
import economycomputation.RecursionCalculator;
import game_model_interfaces.IWorldDefinition;
import helpers.TimeTranslation;

@RunWith(Enclosed.class)
public class CityFoodTests {

	final String porkId = "pork";
	final String playersCivilId = "Europe";
	final long baseCityTime = 200l;
	final double oneHour = 1.0d;
	final double oneMinute = 1.0d/60.0d;
	final double fiveMinutes = 5.0d/60.0d;
	
	final long twoMinutesNs = 2l*60l*1000l*1000l*1000l;
	final int fiveMinutesSec = 60*5;
	final long oneHourInNs = 60l*60l*1000l*1000l*1000l;
	final double delta = 0.00001d;
	final int porkSatisfaction = 2;
	
	CityFood cf;
	
	@Mock
	CityStockSet cityStocks;
	
	@Mock
	RecursionCalculator recCalc;
	
	@Mock
	EcoCalculator ecoCalc;
	
	@Mock
	IWorldDefinition wd;
	
	@Before
	public void setUp() {
		when(cityStocks.getWorldDefinition()).thenReturn(wd);
		when(wd.satisfactionCooldownSec()).thenReturn(fiveMinutesSec);
		when(cityStocks.getCityTimeNs_()).thenReturn(baseCityTime);
		when(cityStocks.getPlayersCivilId()).thenReturn(playersCivilId);
		when(wd.getSatisfaction(porkId, playersCivilId)).thenReturn(porkSatisfaction);
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class SomeFoodAtStart extends CityFoodTests{
		
		private final int initialFoodAmount = 20;
		
		@Before
		public void SomeFoodAtStartSetup() {
			cf = new CityFoodStub();
			cf.init(porkId, cityStocks, initialFoodAmount);
		}
		
		@Test
		public void startup() {
			when(recCalc.getFoodFallTime(cf)).thenReturn(null);
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(null);
			
			assertNull(cf.getCooldownRemainingTime());
			assertNull(cf.getNextRecursionEvent(recCalc, ecoCalc));
			assertEquals(20, cf.getVisibleAmount());
			assertEquals(porkSatisfaction, cf.getSatisfaction());
		}
		
		@Test
		public void addFood() {
			cf.addFood(3);
			assertEquals(porkSatisfaction, cf.getSatisfaction());
			assertEquals(initialFoodAmount + 3, cf.getVisibleAmount());
			assertNull(cf.getCooldownRemainingTime());
			
		}
		
		@Test
		public void foodIsRaising() throws Exception {
			when(ecoCalc.getFood(porkId, oneHourInNs)).thenReturn(22.0d);
			
			cf.process(ecoCalc, oneHourInNs);
			
			assertEquals(22 , cf.getVisibleAmount());
			
			assertNull(cf.getCooldownRemainingTime());
			assertEquals(porkSatisfaction, cf.getSatisfaction());
		}
		
		@Test
		public void RE_foodIsFalling() throws Exception {
			when(recCalc.getFoodFallTime(cf)).thenReturn(oneHour);
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(null);
			
			processRecursionEvent();
			verifyIfGetFoodWasNotInvoked();
			
			assertNull(cf.getCooldownRemainingTime());
			
			assertEquals(0,cf.getVisibleAmount());
			assertNull(cf.getNextRecursionEvent(recCalc, ecoCalc));
			assertEquals(0, cf.getSatisfaction());
		}
		
		@Test
		public void RE_foodIsFalling_RE_foodIsRaisning() throws Exception {
			
			when(recCalc.getFoodFallTime(cf)).thenReturn(oneHour);
			processRecursionEvent();
			
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(oneHour);
			processRecursionEvent();
			verifyIfGetFoodWasNotInvoked();
			
			
			assertEquals(1, cf.getVisibleAmount());
			assertNotNull(cf.getCooldownRemainingTime());
			assertEquals(satisfactionCooldownInNs(), cf.getCooldownRemainingTime().longValue());
			assertEquals(0, cf.getSatisfaction());
		}
		
		@Test
		public void RE_foodIsFalling_RE_foodIsRaisning_RE_cooldownStopped() throws Exception {
			
			when(recCalc.getFoodFallTime(cf)).thenReturn(oneHour);
			processRecursionEvent();
			
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(oneHour);
			processRecursionEvent();
			verifyIfGetFoodWasNotInvoked();
			
			when(ecoCalc.getFood(porkId, satisfactionCooldownInNs())).thenReturn(13.0d);
			processRecursionEvent();
			
			assertNull(cf.getCooldownRemainingTime());
			assertEquals(13, cf.getVisibleAmount());
			assertEquals(porkSatisfaction, cf.getSatisfaction());
		}

		private void verifyIfGetFoodWasNotInvoked() {
			verify(ecoCalc, times(0)).getFood(anyString(), anyLong());
		}
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class ZeroFoodAtStartAndDoesNotRaiseAtStartup extends CityFoodTests {
		
		@Before
		public void ZeroFoodAtStartAndStayZeroSetup() {
			cf = new CityFoodStub();
			cf.init(porkId, cityStocks, 0);
		}
		
		@Test
		public void onStartup() {
			
			assertEquals(0, cf.getVisibleAmount());
			assertNull(cf.getCooldownRemainingTime());
			assertEquals(0, cf.getSatisfaction());
		}
		
		@Test
		public void onStartup_noFoodRaiseRecursionEvent() {
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(null);
			when(recCalc.getFoodFallTime(cf)).thenReturn(null);
			
			assertNull(cf.getNextRecursionEvent(recCalc, ecoCalc));
			assertEquals(0, cf.getSatisfaction());
		}
		
		@Test
		public void processingFoodshouldNotChangeAnythingBeforeRecursionEvent() {
			cf.process(ecoCalc, 100l);
			
			
			assertEquals(0, cf.getVisibleAmount());
			assertNull(cf.getCooldownRemainingTime());
			assertEquals(0, cf.getSatisfaction());
		}
		
		@Test
		public void RE_processFoodRaiseAfterTimeRecursionEvent_cooldownTimerShouldStart() {
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(1.0d);
			when(recCalc.getFoodFallTime(cf)).thenReturn(null);
			
			processRecursionEvent();
	
			assertNotNull(cf.getCooldownRemainingTime());
			assertEquals(satisfactionCooldownInNs(), cf.getCooldownRemainingTime().longValue());
			assertNotNull(cf.getNextRecursionEvent(recCalc, ecoCalc));
			assertEquals(satisfactionCooldownInNs(), cf.getNextRecursionEvent(recCalc, ecoCalc).recursionTimeNs().longValue());
			
			assertEquals(1, cf.getVisibleAmount());
			assertEquals(0, cf.getSatisfaction());
		}
		
		@Test
		public void processFoodRaiseAfterTimeRecursionEvent_processCityFood_CooldownTimerShouldTick() {
			
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(1.0d);
			when(recCalc.getFoodFallTime(cf)).thenReturn(null);
			long processTimeNs = 300l;
			
			processRecursionEvent();
			processFood(processTimeNs, 20.0d);
	
			
			assertNotNull(cf.getCooldownRemainingTime());
			assertEquals(satisfactionCooldownInNs()-processTimeNs, cf.getCooldownRemainingTime().longValue());
			assertNotNull(cf.getNextRecursionEvent(recCalc, ecoCalc));
			assertEquals(satisfactionCooldownInNs()-processTimeNs, cf.getNextRecursionEvent(recCalc, ecoCalc).recursionTimeNs().longValue());
			assertEquals(20, cf.getVisibleAmount());
			assertEquals(0, cf.getSatisfaction());
			
		}
		
		@Test
		public void processFoodRaiseAfterTimeRecursionEvent_processCooldownTimer_CooldownTimerShouldStop() {			
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(1.0d);
			when(recCalc.getFoodFallTime(cf)).thenReturn(null);
			when(ecoCalc.getFood(porkId, satisfactionCooldownInNs())).thenReturn(20.0d);
			
			processRecursionEvent();
			processRecursionEvent();
	
			assertNull(cf.getCooldownRemainingTime());
			assertNull(cf.getNextRecursionEvent(recCalc, ecoCalc));
			assertEquals(20, cf.getVisibleAmount());
			assertEquals(porkSatisfaction, cf.getSatisfaction());
			
			
		}
		
		@Test
		public void processFoodRaiseAfterTimeRecursionEvent_processCooldownTimer_processFood() {			
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(1.0d);
			when(recCalc.getFoodFallTime(cf)).thenReturn(null);
			when(ecoCalc.getFood(porkId, satisfactionCooldownInNs())).thenReturn(20.0d);
			
			processRecursionEvent();
			processRecursionEvent();
			processFood(1400l, 30.0d);
	
			assertNull(cf.getCooldownRemainingTime());
			assertNull(cf.getNextRecursionEvent(recCalc, ecoCalc));
			assertEquals(30, cf.getVisibleAmount());
			assertEquals(porkSatisfaction, cf.getSatisfaction());
		}

		@Test
		public void addFoodFromTrip() {
			when(recCalc.getFoodFallTime(cf)).thenReturn(oneMinute);
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(null);
			
			cf.addFood(10);
			
			assertEquals(0, cf.getSatisfaction());
			
			assertEquals(10, cf.getVisibleAmount());
			assertNotNull(cf.getCooldownRemainingTime());
			assertEquals(satisfactionCooldownInNs(), cf.getCooldownRemainingTime().longValue());
		}
		
		@Test
		public void addFoodFromTripAndProcess_coolDownTimerShouldTick() {
			when(recCalc.getFoodFallTime(cf)).thenReturn(oneMinute);
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(null);
			
			cf.addFood(10);
			final long processTimeNs = 2000l;
			processFood(processTimeNs, 9);
			
			assertEquals(0, cf.getSatisfaction());
			
			assertEquals(9, cf.getVisibleAmount());
			assertNotNull(cf.getCooldownRemainingTime());
			assertEquals(satisfactionCooldownInNs()-processTimeNs, cf.getCooldownRemainingTime().longValue());
		}
		
		@Test
		public void addFoodFromTrip_FoodWillFallBeforeCooldownEnds() {
			when(recCalc.getFoodFallTime(cf)).thenReturn(oneMinute);
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(null);
			
			cf.addFood(10);
			processRecursionEvent();
			
			assertEquals(0, cf.getSatisfaction());
			assertEquals(0, cf.getVisibleAmount());
			
			assertNull(cf.getCooldownRemainingTime());
			assertNull(cf.getNextRecursionEvent(recCalc, ecoCalc));
		}
		
		@Test
		public void addFoodFromTrip_FoodWillFallAfterCooldownEnds() {
			when(recCalc.getFoodFallTime(cf)).thenReturn(oneHour);
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(null);
			when(ecoCalc.getFood(porkId, satisfactionCooldownInNs())).thenReturn(8.0d);
			
			cf.addFood(10);
			processRecursionEvent();
			
			assertEquals(porkSatisfaction, cf.getSatisfaction());
			assertEquals(8, cf.getVisibleAmount());
			
			assertNull(cf.getCooldownRemainingTime());
			assertNotNull(cf.getNextRecursionEvent(recCalc, ecoCalc));
		}
		
		@Test
		public void addFoodFromTrip_RE_CoolDownEnds_RE_foodWillFall() {
			when(recCalc.getFoodFallTime(cf)).thenReturn(oneHour);
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(null);
			when(ecoCalc.getFood(porkId, satisfactionCooldownInNs())).thenReturn(8.0d);
			
			cf.addFood(10);
			processRecursionEvent();
			processRecursionEvent();
			
			assertEquals(0, cf.getSatisfaction());
			assertEquals(0, cf.getVisibleAmount());
			
			assertNull(cf.getCooldownRemainingTime());
			assertNull(cf.getNextRecursionEvent(recCalc, ecoCalc));
		}
		
		@Test
		public void addFoodFromTrip_FoodWillFallInSameTimeAsCooldownEnds() {
			when(recCalc.getFoodFallTime(cf)).thenReturn(fiveMinutes);
			when(recCalc.getFoodRaiseTime(cf)).thenReturn(null);
			when(ecoCalc.getFood(porkId, satisfactionCooldownInNs())).thenReturn(8.0d);
			
			// make sure if these two recursion event will have the same recursion time - checking possible rounding errors
			assertEquals(TimeTranslation.hoursToNs(fiveMinutes), satisfactionCooldownInNs());
			
			cf.addFood(10);
			processRecursionEvent();
			
			assertEquals(0, cf.getSatisfaction());
			assertEquals(0, cf.getVisibleAmount());
			
			assertNull(cf.getCooldownRemainingTime());
			assertNull(cf.getNextRecursionEvent(recCalc, ecoCalc));
		}
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class ZeroFoodAtStartAndRaiseAtStartup extends CityFoodTests {
		
		@Before
		public void ZeroFoodAtStartAndIncreasingSetUp() {
			
			cf = new CityFoodStub();
			cf.init(porkId, cityStocks, 0);
			cf.setToAvailableState();
		}
		
		@Test
		public void onStartUp() {
			
			assertEquals(1, cf.getVisibleAmount());
			assertNotNull(cf.getCooldownRemainingTime());
			assertEquals(0, cf.getSatisfaction());
		}
		
		@Test
		public void onStartup_cooldownTimerShouldTick_thenFoodWillNeverFallDown() {
			when(recCalc.getFoodFallTime(cf)).thenReturn(null);
			
			assertNotNull(cf.getNextRecursionEvent(recCalc, ecoCalc));
			assertEquals(satisfactionCooldownInNs(), cf.getNextRecursionEvent(recCalc, ecoCalc).recursionTimeNs().longValue());
			assertNotNull(cf.getCooldownRemainingTime());
			assertEquals(0, cf.getSatisfaction());
		}
		
		@Test
		public void onStartup_cooldownTimerShouldTick_thenFoodWillFallAfterOneHour() {
			when(recCalc.getFoodFallTime(cf)).thenReturn(1.0d);
			
			cf.getNextRecursionEvent(recCalc, ecoCalc).process();
			assertNotNull(cf.getNextRecursionEvent(recCalc, ecoCalc));
			assertEquals(oneHourInNs, cf.getNextRecursionEvent(recCalc, ecoCalc).recursionTimeNs().longValue());
			assertNull(cf.getCooldownRemainingTime());
			assertEquals(porkSatisfaction, cf.getSatisfaction());
		}

		@Test
		public void process_amountBeetwenZeroAndOne() throws Exception {
			
			final long processTimeNs = 100;
			processFood(processTimeNs, 0.6d);
			
			assertEquals(1, cf.getVisibleAmount());
			
			assertNotNull(cf.getCooldownRemainingTime());
			assertEquals(satisfactionCooldownInNs()-processTimeNs, cf.getCooldownRemainingTime().longValue());
			assertEquals(0, cf.getSatisfaction());
		}
		
		@Test
		public void process_amountExceedsOne() throws Exception {
			
			final long processTimeNs = 100;
			processFood(processTimeNs, 1.2d);
			
			assertEquals(1, cf.getVisibleAmount());
			
			assertNotNull(cf.getCooldownRemainingTime());
			assertEquals(satisfactionCooldownInNs()-processTimeNs, cf.getCooldownRemainingTime().longValue());
			assertEquals(0, cf.getSatisfaction());
		}
		
		@Test
		public void process_amountExceedsTwo() throws Exception {
			
			final long processTimeNs = 100;
			processFood(processTimeNs, 2.1d);
			
			assertEquals(2, cf.getVisibleAmount());
			
			assertNotNull(cf.getCooldownRemainingTime());
			assertEquals(satisfactionCooldownInNs()-processTimeNs, cf.getCooldownRemainingTime().longValue());
			assertEquals(0, cf.getSatisfaction());
		}
		
		@Test
		public void process_amountExceedsTwenty() throws Exception {
			
			final long processTimeNs = 100;
			processFood(processTimeNs, 20.0d);
			
			assertNotNull(cf.getCooldownRemainingTime());
			assertEquals(satisfactionCooldownInNs()-processTimeNs, cf.getCooldownRemainingTime().longValue());
			assertEquals(20, cf.getVisibleAmount());
			assertEquals(0, cf.getSatisfaction());
			
		}
		
		@Test
		public void process_AmountReachesTwenty_addFoodFromTrip() throws Exception {
			
			final long processTimeNs = 100;
			final int foodAfterProcess = 20;
			final int foodAddedFromTrip = 30;
			processFood(processTimeNs, foodAfterProcess);
			cf.addFood(foodAddedFromTrip);
			
			assertNotNull(cf.getCooldownRemainingTime());
			assertEquals(satisfactionCooldownInNs()-processTimeNs, cf.getCooldownRemainingTime().longValue());
			assertEquals( foodAfterProcess + foodAddedFromTrip, cf.getVisibleAmount());
			assertEquals(0, cf.getSatisfaction());
			
		}
		
		@Test
		public void RE_cooldownTimerShouldStop() {
			
			final double porkAmountAfterCd = 20;
			when(ecoCalc.getFood(porkId, satisfactionCooldownInNs())).thenReturn(porkAmountAfterCd);
			when(recCalc.getFoodFallTime(cf)).thenReturn(oneHour);
			processRecursionEvent();
			
			assertEquals((int)porkAmountAfterCd, cf.getVisibleAmount());
			assertEquals(porkSatisfaction, cf.getSatisfaction());
			
			assertNull(cf.getCooldownRemainingTime());
		}
		
		@Test
		public void RE_cooldownTimerShouldStop_RE_foodWillFallDown() {
			
			final double porkAmountAfterCd = 20;
			when(ecoCalc.getFood(porkId, satisfactionCooldownInNs())).thenReturn(porkAmountAfterCd);
			when(recCalc.getFoodFallTime(cf)).thenReturn(null);
			processRecursionEvent();
			when(recCalc.getFoodFallTime(cf)).thenReturn(oneHour);
			processRecursionEvent();
			
			assertEquals(0, cf.getVisibleAmount());
			assertEquals(0, cf.getSatisfaction());
			assertNull(cf.getCooldownRemainingTime());
		}
	}
	void processFood(long someTimeNs, double foodAmountAfterTime) {
		when(ecoCalc.getFood(porkId, someTimeNs)).thenReturn(foodAmountAfterTime);
		cf.process(ecoCalc, someTimeNs);
		synchronizeCityTime(someTimeNs);
	}
	
	void processRecursionEvent() {

		long recTimeNs = cf.getNextRecursionEvent(recCalc, ecoCalc).recursionTimeNs();
		cf.getNextRecursionEvent(recCalc, ecoCalc).process();
		synchronizeCityTime(recTimeNs);
	}
	
	long satisfactionCooldownInNs() {
		return TimeTranslation.secToNs(cf.getCityStocks().getWorldDefinition().satisfactionCooldownSec());
	}
	
	void synchronizeCityTime(long timeToAddNs) {
		long current = cityStocks.getCityTimeNs_();
		when(cityStocks.getCityTimeNs_()).thenReturn(current + timeToAddNs); //synchronize city time
	}
	
	@Ignore
	public static class CityFoodStub extends CityFood {

		private CityStockSet cityStocks;
		private double amount;
		private String stockId;
		private long foodRaiseTimeNs_;
		@Override
		public String getStockId() {
			return stockId;
		}

		@Override
		protected long getFoodRaiseTimeNs_() {
			return foodRaiseTimeNs_;
		}

		@Override
		protected void setFoodRaiseTimeNs_(long raiseTimeNs) {
			this.foodRaiseTimeNs_ = raiseTimeNs;
		}

		@Override
		protected void setStockId(String stockId) {
			this.stockId = stockId;
		}

		@Override
		protected double getAmount() {
			return amount;
		}

		@Override
		protected void setAmount(double amount) {
			this.amount = amount;
		}

		@Override
		protected CityStockSet getCityStocks() {
			return cityStocks;
		}

		@Override
		protected void setCityStocks(CityStockSet cityStocks) {
			this.cityStocks = cityStocks;
			
		}
		
	}
	
}
