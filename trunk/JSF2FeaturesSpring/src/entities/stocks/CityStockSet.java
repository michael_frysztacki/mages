package entities.stocks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.jscience.mathematics.number.Rational;

import comparators.RecursionEventComparator;
import economycomputation.EcoCalculator;
import economycomputation.RecursionCalculator;
import economycomputation.recursion_events.RecursionEvent;
import entities.EntityFactory;
import entities.UsersCityConfig;
import framework.StockSet;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IGameParameters;
import game_model_interfaces.IWorldDefinition;
import game_model_interfaces.PriceStock;
import game_model_interfaces.PriceStockSet;
import game_model_interfaces.initializations.ICityInitializator;
import helpers.CollectionUtil;
import helpers.DefaultStockBinder;
import helpers.Pair;

public abstract class CityStockSet extends StockSet<CityStock> {
	
	private FoodStateCalculator calc;
	protected abstract void setStocks(List<CityStock> cityStocks);
	protected abstract UsersCityConfig getCity();
	protected abstract void setCity(UsersCityConfig city);
	public void init(UsersCityConfig city, EntityFactory factory) {
		setCity(city);
		setStocks(initStocks(factory));
	}
		private List<CityStock> initStocks(EntityFactory factory) {
			List<CityStock> cityStocks = new ArrayList<CityStock>();
			addPeopleStock(factory, cityStocks);
			addFoodStocks(factory, cityStocks);
			addResourceStocks(factory, cityStocks);
			addTaxStock(factory, cityStocks);
			addTaxStocksAsResourcesFromOtherCivils(factory, cityStocks);
			return cityStocks;
		}
			private void addResourceStocks(EntityFactory factory, List<CityStock> cityStocks) {
				for(String resourceId : getCity().getWorldDefinition().getAllResourceIds())
					cityStocks.add(factory.createCityResource(this, resourceId));
			}
			private void addFoodStocks(EntityFactory factory, List<CityStock> cityStocks) {
				for(String foodId : getCity().getWorldDefinition().getAllFoodIds())
					cityStocks.add(factory.createCityFood(this, foodId));
			}
			private void addPeopleStock(EntityFactory factory, List<CityStock> cityStocks) {
				cityStocks.add(factory.createPeopleStock(this));
			}
			private void addTaxStock(EntityFactory factory, List<CityStock> cityStocks) {
				cityStocks.add(factory.createTaxStock(this));
			}
			private void addTaxStocksAsResourcesFromOtherCivils(EntityFactory factory, List<CityStock> cityStocks) {
				for(ICivilization civil : allCivilizations()) {
					if(isForeignCivilization(civil))
						cityStocks.add(factory.createCityResource(this, civil.getMainResource()));
				}
			}
				private List<ICivilization> allCivilizations() {
					return getCity().getWorldDefinition().getAllCivilizations();
				}
				private boolean isForeignCivilization(ICivilization civil) {
					return !civil.getCivilization().equals(getCity().getMyCivilization().getCivilization());
				}
	public <T extends CityStock> double getSum(Class<T> clazz) {
		double sum = 0.0d;
		List<T> stocks = super.getStocksByClass(clazz);
		for(T stock : stocks)
			sum += stock.getVisibleAmount();
		return sum;
	}
	
	public CityFood getCityFood(String foodId) {
		for(CityFood cf: getFood())
			if(cf.getStockId().equals(foodId))
				return cf;
		return null;
	}
	
	public CityResource getCityResource(String resourceId) {
		for(CityResource cr: getResources())
			if(cr.getStockId().equals(resourceId))
				return cr;
		return null;
	}
	
	public List<CityFood> getFood() {
		 return super.getStocksByClass(CityFood.class);
	}
	public List<CityResource> getResources() {
		 return super.getStocksByClass(CityResource.class);
	}
	public CityPeople getPeopleStock() {
		return getStocksByClass(CityPeople.class).iterator().next();
	}
	public TaxStock getTaxStock() {
		return getStocksByClass(TaxStock.class).iterator().next();
	}
	/*
	 * invoking doPay without checking if we have sufficient resource( canPay ) is undefined.
	 */
	public void doPay(PriceStockSet price) {
		Set<Pair<PriceStock,CityStock>> joined = 
				CollectionUtil.joinCollections(price.getPriceStocks(getMyCivil()), getStocks(), new DefaultStockBinder());
		for(Pair<PriceStock,CityStock> pair : joined)
			pair.second.pay(pair.first);
	}
	
	public boolean canPay(PriceStockSet price) {
		Set<Pair<PriceStock,CityStock>> joined = 
				CollectionUtil.joinCollections(price.getPriceStocks(getMyCivil()), getStocks(), new DefaultStockBinder());
		for(Pair<PriceStock,CityStock> pair : joined)
			if(!pair.second.canPay(pair.first))return false;
		return true;
	}
	
	@Override
	public String toString()
	{
		String ret = "";
		ret += "\n id:";
		for(CityFood cf : getFood()) {
			ret += cf.toString();
			ret += "\n";
		}
		ret += getPeopleStock().toString();
		return ret;
	}
	IWorldDefinition getWorldDefinition() {
		return getCity().getWorldDefinition();
	}
	ICivilization getMyCivil() {
		return getCity().getMyCivilization();
	}
	long getCityTimeNs_() {
		return getCity().getCityTimeNs_();
	}
	String getPlayersCivilId() {
		return getCity().getCivilId();
	}
	String getPeopleStockId() {
		return getCity().getGameParams().getPeopleStockId();
	}
	public ICityInitializator getCityInitializator() {
		return getCity().getCityInitializator();
	}
	public IGameParameters getGameParameters() {
		return getCity().getGameParams();
	}
	
	public void initStorage() {
		calc = new FoodStateCalculator();
		calc.calculate();
	}
	int getSOP() {
		return calc.getSOP();
	}
	int getSN() {
		return calc.getSN();
	}
	
	public void process(long nsTime) {
		
		EcoCalculator ecoCalc = new DefaultEcoCalculator(this);
		for(CityStock cs: getFood())
			cs.process(ecoCalc, nsTime);
		for(CityResource cr: getResources())
			cr.process(ecoCalc, nsTime);
		getPeopleStock().process(ecoCalc, nsTime);
	}
	
		private class FoodStateCalculator {
			
			private int SOP=0;
			private int SN=0;
			int rejectionBoundary=0;
			private List<CityFood> potentialPositiveFoods = new ArrayList<CityFood>();
			public int getSOP() {
				return SOP;
			}
			public int getSN() {
				return SN;
			}
			public FoodStateCalculator() {
				calculateSOP();
			}
				private void calculateSOP() {
					for(CityFood cf : getFood()) {
						if(productionOf(cf)>0 || cf.getVisibleAmount()>0)
							SOP++;
					}
				}
			public void calculate() {
				if(SOP>0) {
					initPotentialPositiveFoods();
					sortPotentialPositiveFoodsByFactoryProductions();
					findRejectedFactories();
					setToPositiveNotRejectedFood();
				}
			}
				private void initPotentialPositiveFoods() {
					for(CityFood cf : getFoodWithZeroAmount()) {
						if(productionOf(cf)>0)
							potentialPositiveFoods.add(cf);
					}
				}
				private void sortPotentialPositiveFoodsByFactoryProductions() {
					Collections.sort(potentialPositiveFoods, new ResourceProductionComparator());
				}
					private class ResourceProductionComparator implements Comparator<CityFood>{
						@Override
						public int compare(CityFood o1, CityFood o2) {
							int o1Prod = productionOf(o1);
							int o2Prod = productionOf(o2);
							return o1Prod-o2Prod;
						}
					}
				private void findRejectedFactories() {
					while(haveFactoriesForRejection()) {
						if(isGreaterOrEqual(ephOfOneFood(), nextFactoryProductionForRejection())) {
							SN += nextFactoryProductionForRejection();
							rejectNextFactory();
						}
						else break;
					}
				}
					private boolean haveFactoriesForRejection() {
						return rejectionBoundary < potentialPositiveFoods.size();
					}
					
					private boolean isGreaterOrEqual(Rational first, int second) {
						return first.isGreaterThan( Rational.valueOf(second,1)) || 
							   first.compareTo( Rational.valueOf(second,1))==0; 
					}
					private Rational ephOfOneFood() {
						return getGeneralEph().minus(Rational.valueOf(SN, 1)).divide(Rational.valueOf(SOP, 1));
					}
					private int nextFactoryProductionForRejection() {
						return productionOf(potentialPositiveFoods.get(rejectionBoundary));
					}
					private void rejectNextFactory() {
						rejectionBoundary++;
						SOP--;
					}
				private void setToPositiveNotRejectedFood() {
					while(haveFactoriesForRejection()) {
						potentialPositiveFoods.get(rejectionBoundary).setToAvailableState();
						rejectionBoundary++;
					}
				}
					private int productionOf(CityFood cf) {
						return getProducer(cf).getOutputPerHour(getCity());
					}
						private IExtractBuildingModel getProducer(CityFood cf) {
							return getCity().getWorldDefinition().getProducer(cf.getStockId());
						}	
				private List<CityFood> getFoodWithZeroAmount() {
					List<CityFood> emptyFood = new ArrayList<CityFood>();
					for(CityFood cf: getFood())
						if(cf.getVisibleAmount()==0)
							emptyFood.add(cf);
					return emptyFood;
				}
					private Rational getGeneralEph() {
						return Rational.valueOf(getCity().getWorldDefinition().getEatPerHour(),1).times(getPeopleAmount());
					}
						private int getPeopleAmount() {
							return getPeopleStock().getVisibleAmount();
						}
		}
	

	public List<RecursionEvent> getShortestRecursionEvents() {
		
		RecursionCalculator recCalc = new DefaultRecursionCalculator();
		EcoCalculator ecoCalc = new DefaultEcoCalculator(this);
		List<RecursionEvent> foodRecursionEvents = new ArrayList<RecursionEvent>();
		for(CityFood cf: getFood()) {
			
			foodRecursionEvents.add(cf.getNextRecursionEvent(recCalc, ecoCalc));
			
		}
		return CollectionUtil.findMaxMinObjects(foodRecursionEvents, new RecursionEventComparator(), false);
	}
		
}
