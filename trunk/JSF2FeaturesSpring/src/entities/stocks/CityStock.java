package entities.stocks;

import economycomputation.EcoCalculator;
import framework.IStock;
import game_model_interfaces.PriceStock;

public abstract class CityStock implements IStock {
	
	protected abstract void setStockId(String stockId);
	protected abstract double getAmount();
	protected abstract void setAmount(double amount);
	protected abstract CityStockSet getCityStocks();
	protected abstract void setCityStocks(CityStockSet cityStocks);
	protected void init(String stockId, CityStockSet cityStocks) {
		 setStockId(stockId);
		 setAmount(0.0d);
		 setCityStocks(cityStocks);
	}
	protected void init(String stockId, CityStockSet cityStocks, int amount) {
		 setStockId(stockId);
		 setAmount(amount);
		 setCityStocks(cityStocks);
	}
	@Override
	public String toString() {
		return getStockId() + ": " + getAmount();
	}
	public abstract void process(EcoCalculator ecoCalc, long nsTime);
	/*
	 * method determiantes if we have at least one unit of stock use for anything like transport, buying, sending army..etc.
	 * Anythink we can use this stock. If we have e.g. 10 people in city it doesn't mean that we have available this stock,
	 * because these people could be the first citizens, which we can't use. Another example is food.
	 * Even if to the player we show a amount of "1", the food state could be between 0 and 1, so in real we, doesn't have food for use.
	 */
	/*
	public boolean isAvailable() {
		return getAmount() != 0.0d;
	}
	*/
	/*
	 * Result is undefined if we have not enough city stocks
	 */
	public void pay(PriceStock cost) {
		setAmount(getAmount()-cost.getVisibleAmount());
	}
	
	/*
	 * Result is undefined if price stock id and this city stock id are different.
	 */
	public boolean canPay(PriceStock cost) {
		return getAmount() >= cost.getVisibleAmount();
	}
	
}
