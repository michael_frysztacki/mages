package entities.stocks;

import helpers.TimeTranslation;
import economycomputation.EcoCalculator;

public abstract class TaxStock extends CityStock {

	@Override
	public void init(String stockId, CityStockSet cityStocks, int amount) {
		 super.init(stockId, cityStocks, amount);
	}
	
	@Override
	public String getStockId() {
		return getCityStocks().getMyCivil().getMainResource();
	}
	@Override
	public int getVisibleAmount() {
		return (int) getAmount();
	}
	
	@Override
	protected void setStockId(String stockId) {
		return;
	}
	public void process(EcoCalculator ecoCalc, long nsTime) {
		double hTime = TimeTranslation.nsToHours(nsTime);
		setAmount(ecoCalc.getTaxCount(hTime, getStockId()));
	}

}
