package entities.stocks;

import framework.IStock;
import framework.FloatingPointStock;
import game_model_impl.StockType;

public class TripStock extends FloatingPointStock<Integer>
{
	@Override
	public boolean equals(Object o)
	{
		TripStock cf = (TripStock)o;
		if(cf.getStockType() == this.getStockType())
		{
			return true;
		}
		else return false;
	}
	TripStock(StockType st, Integer count)
	{
		super(st,count);
	}
	TripStock(IStock<Integer> stock)
	{
		super(stock);
	}

	@Override
	public String toString()
	{
		return "id:" + this.getId() +"typ:"+this.getStockType() + ": " + getVisibleAmount() + ", storage: ";
	}
	@SuppressWarnings("unused")
	private TripStock(){};
	
	public static TripStock createTripStock(CityStock source, IStock<Integer> stock)
	{
		if(stock.getStockType() != source.getStockType())return null;
		if(source.available() < stock.getVisibleAmount())return null;
		source.count -= stock.getVisibleAmount();
		StockType type = stock.getStockType();
		if(stock.getStockType() == StockType.peopleCount)type = StockType.slaves;
		return new TripStock(type,stock.getVisibleAmount());
	}
	boolean putTripStock(CityStock putHere)
	{
		if(putHere.stockType != this.stockType)return false;
		putHere.count += this.count;
		this.count = 0;
		return true;
	}

}