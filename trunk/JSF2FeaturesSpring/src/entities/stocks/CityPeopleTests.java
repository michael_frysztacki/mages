package entities.stocks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import economycomputation.EcoCalculator;
import economycomputation.RecursionCalculator;
import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;
import game_model_interfaces.ICityModel;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IHousesModel;
import game_model_interfaces.IWorldDefinition;
import helpers.TimeTranslation;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CityPeopleTests {

	@Mock
	ICivilization civil;
	
	@Mock
	IWorldDefinition wd;
	//public final int firstCitizensAmount = 10;
	public final int eatPerHour = 1;
	
	@Mock
	ExtractCityBuilding ecb1;
	
	@Mock
	ExtractCityBuilding ecb2;
	
	@Mock
	RecursionCalculator recCalc;
	
	@Mock
	ICityModel cityModel;
	
	@Mock
	EcoCalculator ecoCalc;
	
	@Mock
	IHousesModel houses;
	
	@Mock
	UsersCityConfig ucc;
	List<ExtractCityBuilding> cityBuildings = new ArrayList<ExtractCityBuilding>();
	
	@Mock
	CityStockSet cityStocks;
	List<CityFood> cityFood = new ArrayList<CityFood>();
	
	final int waterSatisfaction = 3;
	final int majorMeatSatisfaction = 2;
	final int majorCarboSatisfaction = 2;
	final int majorAdditionSatisfaction = 2;
	final int furtherMeatSatisfaction = 1;
	final int furtherCarboSatisfaction = 1;
	final int furtherAdditionSatisfaction = 1;
	@Mock
	CityFood pork;
	final String porkId = "pork";
	@Mock
	CityFood beef;
	final String beefId = "beef";
	@Mock
	CityFood water;
	final String waterId = "water";
	@Mock
	CityFood rice;
	final String riceId = "rice";
	@Mock
	CityFood sul;
	final String sulId = "sul";
	
	CityPeople cp;
	public final int startAmount = 12;
	public final int birthRateMultiplier = 2;
	public final String peopleStockId = "people";
	
	public final double delta = 0.00000005d;
	public final double oneHour = 1.0d;
	public final long oneHourNs = TimeTranslation.hoursToNs(oneHour);

	private final long processTimeNs = 100;
	
	@Before
	public void setup() {
		cp = new CityPeopleStub();
		cp.init(cityStocks, startAmount);
		initCityFood();
		when(wd.getEatPerHour()).thenReturn(eatPerHour);
		when(wd.getBirthMultiplier()).thenReturn(birthRateMultiplier);
		when(ucc.getCityBuildingsByClass(ExtractCityBuilding.class, true)).thenReturn(cityBuildings);
		when(ucc.getCityStocks()).thenReturn(cityStocks);
		when(cityStocks.getWorldDefinition()).thenReturn(wd);
		when(cityStocks.getCity()).thenReturn(ucc);
		when(cityStocks.getMyCivil()).thenReturn(civil);
		when(cityStocks.getFood()).thenReturn(cityFood);
		when(civil.getHousesModel()).thenReturn(houses);
		when(civil.getMajorMeat()).thenReturn(porkId);
		when(civil.getMajorCarbo()).thenReturn(riceId);
		when(civil.getMajorAddition()).thenReturn(sulId);
		when(civil.getCityModel()).thenReturn(cityModel);
		mock_extractCityBuildings();
		/*
		when(wd.getMajorMeatSatisfaction()).thenReturn(majorMeatSatisfaction);
		when(wd.getMajorCarboSatisfaction()).thenReturn(majorCarboSatisfaction);
		when(wd.getMajorAdditionSatisfaction()).thenReturn(majorAdditionSatisfaction);
		when(wd.getFurtherMeatSatisfaction()).thenReturn(furtherMeatSatisfaction);
		when(wd.getFurtherCarboSatisfaction()).thenReturn(furtherCarboSatisfaction);
		when(wd.getFurtherAdditionSatisfaction()).thenReturn(furtherAdditionSatisfaction);
		*/
	}

		private void initCityFood() {
			when(pork.getStockId()).thenReturn(porkId);
			cityFood.add(pork);
			when(beef.getStockId()).thenReturn(beefId);
			cityFood.add(beef);
			when(water.getStockId()).thenReturn(waterId);
			cityFood.add(water);
			when(rice.getStockId()).thenReturn(riceId);
			cityFood.add(rice);
			when(sul.getStockId()).thenReturn(sulId);
			cityFood.add(sul);
		}
		
		private void mock_extractCityBuildings() {
			cityBuildings.add(ecb1);
			cityBuildings.add(ecb2);
			when(ecb1.getPopulation()).thenReturn(0);
			when(ecb2.getPopulation()).thenReturn(0);
		}

	@Test
	public void startup()  {
		
		assertEquals(startAmount, cp.getVisibleAmount());
		assertEquals(0, cp.getHiredPeople());
		assertEquals(12.0d, cp.getPeopleEatPerHour(), delta);
	}
	
	@Test
	public void fallingPeople_visibleAmountShouldBeRoundedToFloorValue()  {
		
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(11.5d);
		cp.process(ecoCalc, processTimeNs);
		assertEquals(11, cp.getVisibleAmount());
	}
	
	@Test
	public void fallingPeopleAfterFewPeopleLeft_visibleAmountShouldBeRoundedToFloorValue()  {
		
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(9.2d);
		cp.process(ecoCalc, processTimeNs);
		assertEquals(9, cp.getVisibleAmount());
	}
	
	@Test
	public void fallingPeopleThenAgainIncreasing_visibleAmountShouldBeRoundedToFloorValue()  {
		
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(9.2d);
		cp.process(ecoCalc, processTimeNs);
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(10.5d);
		cp.process(ecoCalc, processTimeNs);
		assertEquals(10, cp.getVisibleAmount());
	}
	
	@Test
	public void fallingPeopleThenFallingAgain_visibleAmountShouldBeRoundedToFloorValue()  {
		
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(9.2d);
		cp.process(ecoCalc, processTimeNs);
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(9.1d);
		cp.process(ecoCalc, processTimeNs);
		assertEquals(9, cp.getVisibleAmount());
	}
	
	@Test
	public void increasingPeople_visibleAmountShouldBeRoundedToFloorValue()  {
		
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(12.5d);
		cp.process(ecoCalc, processTimeNs);
		assertEquals(12, cp.getVisibleAmount());
	}
	
	@Test
	public void increasingPeopleAfterSomePeopleCame_visibleAmountShouldBeRoundedToFloorValue()  {
		
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(14.5d);
		cp.process(ecoCalc, processTimeNs);
		assertEquals(14, cp.getVisibleAmount());
	}
	
	@Test
	public void increasingPeopleAndBounceOnceAcrossBoundary_visibleAmountShouldNotChange()  {
		
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(14.5d);
		cp.process(ecoCalc, processTimeNs);
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(13.5d);
		cp.process(ecoCalc, processTimeNs);
		assertEquals(13, cp.getVisibleAmount());
	}
	
	@Test
	public void increasingPeopleAndBoundcingTwiceAcrossBoundary_visibleAmountShouldBeRoundedToFloorValue()  {
		
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(14.5d);
		cp.process(ecoCalc, processTimeNs);
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(13.5d);
		cp.process(ecoCalc, processTimeNs);
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(14.5d);
		
		cp.process(ecoCalc, processTimeNs);
		
		assertEquals(14, cp.getVisibleAmount());
	}
	
	@Test
	public void peopleHiredInBuildings()  {
		when(ecb1.getPopulation()).thenReturn(3);
		when(ecb2.getPopulation()).thenReturn(5);
		
		assertEquals(startAmount-3-5, cp.getVisibleAmount());
		assertEquals(3+5, cp.getHiredPeople());
		assertEquals(12.0d, cp.getPeopleEatPerHour(), delta);
	}
	
	@Test
	public void houseLimitRecursionEvent()  {
		final int housesLimit = 20;
		when(houses.getPeopleLimit(ucc)).thenReturn(20);
		when(recCalc.timeWhenPeopleArriveLimit(ucc)).thenReturn(oneHour);
		
		cp.getNextRecursionEvent(recCalc,ucc).process();
		
		assertEquals(housesLimit, cp.getVisibleAmount()); 
	}
	
	@Test
	public void houseLimitRecursionEvent_wontReachBecausePeopleAreFalling()  {
		final int housesLimit = 20;
		when(houses.getPeopleLimit(ucc)).thenReturn(housesLimit);
		when(recCalc.timeWhenPeopleArriveLimit(ucc)).thenReturn(null);
		
		assertNull(cp.getNextRecursionEvent(recCalc, ucc)); 
	}
	
	@Test
	public void houseLimitRecursionEvent_processShouldStillBeHousesLimit()  {
		final int housesLimit = 20;
		when(houses.getPeopleLimit(ucc)).thenReturn(housesLimit);
		when(recCalc.timeWhenPeopleArriveLimit(ucc)).thenReturn(oneHour);
		when(ecoCalc.getPeopleCount(oneHourNs)).thenReturn((double) housesLimit);
		
		cp.getNextRecursionEvent(recCalc,ucc).process();
		cp.process(ecoCalc, oneHourNs);
		
		assertEquals(housesLimit, cp.getVisibleAmount());
	}
	
	@Test
	public void nothingAddsSatisfaction_birthRateShouldBeZero()  {
		when(cityModel.getSatisfactionFromObjects(ucc)).thenReturn(0);
		when(ucc.getTaxSatisfaction()).thenReturn(0);
		when(pork.getSatisfaction()).thenReturn(0);
		when(rice.getSatisfaction()).thenReturn(0);
		when(sul.getSatisfaction()).thenReturn(0);
		when(beef.getSatisfaction()).thenReturn(0);
		when(water.getSatisfaction()).thenReturn(0);
		
		assertEquals(0, cp.getBirthRatePerHour());
	}
	
	@Test
	public void taxesAddSatisfaction_birthRateShouldBePositive()  {
		final int taxSatisfaction = 2;
		when(cityModel.getSatisfactionFromObjects(ucc)).thenReturn(0);
		when(ucc.getTaxSatisfaction()).thenReturn(taxSatisfaction);
		when(pork.getSatisfaction()).thenReturn(0);
		when(rice.getSatisfaction()).thenReturn(0);
		when(sul.getSatisfaction()).thenReturn(0);
		when(beef.getSatisfaction()).thenReturn(0);
		when(water.getSatisfaction()).thenReturn(0);
		
		assertEquals(taxSatisfaction * wd.getBirthMultiplier(), cp.getBirthRatePerHour());
	}
	
	@Test
	public void taxesAndFoodAddsSatisfaction_birthRateShouldBePositive()  {
		final int taxSatisfaction = 2;
		when(cityModel.getSatisfactionFromObjects(ucc)).thenReturn(0);
		when(ucc.getTaxSatisfaction()).thenReturn(taxSatisfaction);
		when(pork.getSatisfaction()).thenReturn(majorMeatSatisfaction);
		when(rice.getSatisfaction()).thenReturn(majorCarboSatisfaction);
		when(sul.getSatisfaction()).thenReturn(0);
		when(beef.getSatisfaction()).thenReturn(0);
		when(water.getSatisfaction()).thenReturn(0);
		
		assertEquals((taxSatisfaction+majorMeatSatisfaction+majorCarboSatisfaction)*wd.getBirthMultiplier(), cp.getBirthRatePerHour());
	}
	
	@Test
	public void taxesAndFoodBalanceSatisfactionToZero_birthRateShouldBeZero()  {
		final int taxSatisfaction = -4;
		when(cityModel.getSatisfactionFromObjects(ucc)).thenReturn(0);
		when(ucc.getTaxSatisfaction()).thenReturn(taxSatisfaction);
		when(pork.getSatisfaction()).thenReturn(majorMeatSatisfaction);
		when(rice.getSatisfaction()).thenReturn(majorCarboSatisfaction);
		when(sul.getSatisfaction()).thenReturn(0);
		when(beef.getSatisfaction()).thenReturn(0);
		when(water.getSatisfaction()).thenReturn(0);
		
		assertEquals(0, cp.getBirthRatePerHour());
	}
	
	@Test
	public void objectsAddSatisfaction_birthRateShouldBePositive()  {
		final int taxSatisfaction = 0;
		final int objectsSatisfaction = 4;
		when(cityModel.getSatisfactionFromObjects(ucc)).thenReturn(objectsSatisfaction);
		when(ucc.getTaxSatisfaction()).thenReturn(taxSatisfaction);
		when(pork.getSatisfaction()).thenReturn(0);
		when(rice.getSatisfaction()).thenReturn(0);
		when(sul.getSatisfaction()).thenReturn(0);
		when(beef.getSatisfaction()).thenReturn(0);
		when(water.getSatisfaction()).thenReturn(0);
		
		assertEquals(objectsSatisfaction*wd.getBirthMultiplier(), cp.getBirthRatePerHour());
	}
	
	@Test
	public void onlyTaxSatisfaction()  {
		
		final int taxSatisfaction = 4;
		when(ucc.getTaxSatisfaction()).thenReturn(taxSatisfaction);
		
		assertEquals(taxSatisfaction * wd.getBirthMultiplier(), cp.getVisibleBirthRate());
	}
	
	@Test
	public void peopleAmountBetween0And1_negativeBirthRate()  {
		
		final int taxSatisfaction = -4;
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(0.5d);
		when(ucc.getTaxSatisfaction()).thenReturn(taxSatisfaction);
		
		cp.process(ecoCalc, processTimeNs);
		assertTrue("Actual value of birth Rate: " + cp.getBirthRatePerHour(), cp.getBirthRatePerHour() < 0);
		assertEquals(0, cp.getVisibleBirthRate());
	}
	
	@Test
	public void peopleAmountBetween0And1_positiveBirthRate()  {
		
		final int taxSatisfaction = 4;
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(0.5d);
		when(ucc.getTaxSatisfaction()).thenReturn(taxSatisfaction);
		
		cp.process(ecoCalc, processTimeNs);
		assertTrue ("Actual value of birth Rate: " + cp.getBirthRatePerHour(), cp.getBirthRatePerHour() > 0);
		assertEquals(cp.getBirthRatePerHour(), cp.getVisibleBirthRate());
	}
	
	@Test
	public void peopleAmountEqualsOne_negativeBirthRate()  {
		
		final int taxSatisfaction = -4;
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(1.0d);
		when(ucc.getTaxSatisfaction()).thenReturn(taxSatisfaction);
		
		cp.process(ecoCalc, processTimeNs);
		assertTrue ("Actual value of birth Rate: " + cp.getBirthRatePerHour(), cp.getBirthRatePerHour() < 0);
		assertEquals(cp.getBirthRatePerHour(), cp.getVisibleBirthRate());
	}
	
	@Test
	public void peopleAmountBetweenOneAndZero_zeroBirthRate()  {
		
		when(ecoCalc.getPeopleCount(processTimeNs)).thenReturn(0.5d);
		
		cp.process(ecoCalc, processTimeNs);
		assertEquals(0, cp.getBirthRatePerHour());
		assertEquals(0, cp.getVisibleBirthRate());
	}
	
	public class CityPeopleStub extends CityPeople {

		private double amount;
		private CityStockSet cityStocks;

		@Override
		protected double getAmount() {
			return amount;
		}

		@Override
		protected void setAmount(double amount) {
			this.amount = amount;
		}

		@Override
		protected CityStockSet getCityStocks() {
			return cityStocks;
		}

		@Override
		protected void setCityStocks(CityStockSet cityStocks) {
			this.cityStocks = cityStocks;
		}
		
	}
}
