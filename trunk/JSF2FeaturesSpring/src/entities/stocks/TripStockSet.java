package entities.stocks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import entities.UsersCityConfig;
import framework.IStock;
import framework.StockSet;
import game_model_impl.StockType;


public class TripStockSet extends StockSet<TripStock>{

	TripStockSet(){};
	@Override
	public List<TripStock> getStocks() {
		return transportStocks;
	}
	List<TripStock> transportStocks = new ArrayList<TripStock>();
	void setTransportStocks(List<TripStock> transportStocks)
	{
		this.transportStocks = transportStocks;
	}
	
	/*
	 * TODO - u�y� CollectionUtil.joinCollectionsAndPerformOpperation().
	 */
	public void putTransportStockSet(UsersCityConfig putHere)
	{
		Map<StockType,CityStock> helper = new HashMap<StockType,CityStock>();
		for(CityStock cs : putHere.getCityStocks().getStocks() )
		{
			helper.put(cs.getStockType(),cs);
		}
		for(TripStock ts : this.getStocks() )
		{
			ts.putTripStock( helper.get(ts.getStockType()));
		}
	}
	/*
	 * TODO - u�y� CollectionUtil.joinCollectionsAndPerformOpperation().
	 */
	public static TripStockSet createTripStockSet(CityStockSet source, Set<? extends IStock<Integer>> stocks)
	{
		TripStockSet ret = new TripStockSet();
		Map<StockType,CityStock> helper = new HashMap<StockType,CityStock>();
		for(CityStock stock : source.getStocks())
		{
			helper.put(stock.getStockType(), stock);
		}
		for(IStock<Integer> stock : stocks)
		{
			CityStock local = helper.get(stock.getStockType());
			if(local.available().intValue() < stock.getVisibleAmount())return null;
		}
		for(IStock<Integer> stock : stocks)
		{
			ret.getStocks().add( new TripStock(stock) );
		}
		return ret;
	}
}
					