package entities.stocks;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import entities.UsersCityConfig;
import entities.stocks.DefaultRecursionCalculator.FoodFallCalculator;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IWorldDefinition;

import org.jscience.JScience;
import org.jscience.mathematics.number.Rational;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DefaultRecursionCalculatorTests {

	private static final long oneHourInNs = 60l*60l*1000l*1000l*1000l;
	DefaultRecursionCalculator recCalc = new DefaultRecursionCalculator();
	public static double delta = 0.0d;
	
	@Mock
	CityFood water;
	final String waterId = "waterId";
	@Mock
	IExtractBuildingModel waterFactory;
	
	@Mock
	CityFood rice;
	final String riceId = "riceId";
	@Mock
	IExtractBuildingModel riceFactory;
	
	@Mock
	CityFood pork;
	final String porkId = "porkId";
	@Mock
	IExtractBuildingModel porkFactory;
	
	public final int noProduction = 0;
	
	@Mock
	CityPeople people;
	
	@Mock
	CityStockSet cityStocks;
	
	@Mock
	UsersCityConfig city;
	
	@Mock
	IWorldDefinition wd;
	public final int eatPerHour = 1;
	
	@Before
	public void setup() {
		
		mockCityFood(water, waterId);
		mockCityFood(rice, riceId);
		mockCityFood(pork, porkId);
		mockProducer(waterFactory, waterId);
		mockProducer(riceFactory, riceId);
		mockProducer(porkFactory, porkId);
		mockProduction(waterFactory, noProduction);
		mockProduction(riceFactory, noProduction);
		mockProduction(porkFactory, noProduction);
		when(wd.getEatPerHour()).thenReturn(eatPerHour);
		when(cityStocks.getPeopleStock()).thenReturn(people);
		List<CityFood> cityFood = new ArrayList<CityFood>();
		cityFood.add(rice);
		cityFood.add(water);
		cityFood.add(pork);
		when(cityStocks.getFood()).thenReturn(cityFood);
	}

		private void mockProduction(IExtractBuildingModel factory, int productionPerHOur) {
			when(factory.getOutputPerHour(city)).thenReturn(productionPerHOur);
		}

		private void mockProducer(IExtractBuildingModel factory, String foodId) {
			when(wd.getProducer(foodId)).thenReturn(factory);
		}

		private void mockCityFood(CityFood food, String foodId) {
			
			when(food.getCityStocks()).thenReturn(cityStocks);
			when(food.getCity()).thenReturn(city);
			when(food.getWorldDefinition()).thenReturn(wd);
			when(food.getStockId()).thenReturn(foodId);
		}
	
		/*
	@Test
	public void stepTimeCalculator_positiveBirthRate() {
		
		FoodFallCalculator foodFallCalc = new FoodFallCalculator(null);
		assert_equals(3, foodFallCalc.calculateLevelOfLastStep(2, 6, 1,1));
		assert_equals(3, foodFallCalc.calculateLevelOfLastStep(2, 5, 1,1));
		assert_equals(2, foodFallCalc.calculateLevelOfLastStep(2, 4, 1,1));
		assert_equals(2, foodFallCalc.calculateLevelOfLastStep(1, 3.0d, 1,1));
		assert_equals(1, foodFallCalc.calculateLevelOfLastStep(1, 2.9d, 1,1));
		assert_equals(1, foodFallCalc.calculateLevelOfLastStep(1, 1.1d, 1,1));
		assert_equals(1, foodFallCalc.calculateLevelOfLastStep(1, 1, 1,1));
		assert_equals(0, foodFallCalc.calculateLevelOfLastStep(1, 0.5d, 1,1));
		assert_equals(0, foodFallCalc.calculateLevelOfLastStep(1, 0.1d, 1,1));
		assert_equals(1, foodFallCalc.calculateLevelOfLastStep(0, 1, 1,1));
		assert_equals(0, foodFallCalc.calculateLevelOfLastStep(0, 0.5d, 1,1));
		assert_equals(1, foodFallCalc.calculateLevelOfLastStep(0, 0.5d, 2,1));
		assert_equals(1, foodFallCalc.calculateLevelOfLastStep(0, 1.0d, 2,1));
		assert_equals(2, foodFallCalc.calculateLevelOfLastStep(0, 2.0d, 2,1));
		assert_equals(2, foodFallCalc.calculateLevelOfLastStep(0, 2.9d, 2,1));
		assert_equals(3, foodFallCalc.calculateLevelOfLastStep(0, 3.0d, 2,1));
		assert_equals(3, foodFallCalc.calculateLevelOfLastStep(0, 3.1d, 2,1));
		assert_equals(3, foodFallCalc.calculateLevelOfLastStep(0, 3.4d, 2,1));
		assert_equals(3, foodFallCalc.calculateLevelOfLastStep(0, 3.5d, 2,1));
		assert_equals(3, foodFallCalc.calculateLevelOfLastStep(2, 10, 1, 2));
	}
	
	@Test
	public void stepTimeCalculator_negativeBirthRate() {

		FoodFallCalculator foodFallCalc = new FoodFallCalculator(null);
		assert_equals(5, foodFallCalc.calculateLevelOfLastStep(5, 7, -1, 1));
		assert_equals(10, foodFallCalc.calculateLevelOfLastStep(10, 10, -1, 1));
		assert_equals(2, foodFallCalc.calculateLevelOfLastStep(5, 14, -1, 1));
		assert_equals(2, foodFallCalc.calculateLevelOfLastStep(2, 2.5d, -1, 1));
		assert_equals(2, foodFallCalc.calculateLevelOfLastStep(2, 2.0d, -1, 1));
		assert_equals(4, foodFallCalc.calculateLevelOfLastStep(5, 10.5, -1, 1));
		assert_equals(4, foodFallCalc.calculateLevelOfLastStep(5, 9, -1, 1));
		assert_equals(5, foodFallCalc.calculateLevelOfLastStep(5, 8.9d, -1, 1));
		assert_equals(1, foodFallCalc.calculateLevelOfLastStep(1, 1, -1, 1));
		assert_equals(2, foodFallCalc.calculateLevelOfLastStep(3, 5.9d, -1,1));
		assert_equals(1, foodFallCalc.calculateLevelOfLastStep(3, 6, -1,1));
		assert_equals(4, foodFallCalc.calculateLevelOfLastStep(4, 2.0d, -2,1));
		assert_equals(1, foodFallCalc.calculateLevelOfLastStep(4, 5.0d, -2,1));
		assert_equals(1, foodFallCalc.calculateLevelOfLastStep(2, 6, -1, 2));
		assert_equals(2, foodFallCalc.calculateLevelOfLastStep(3, 11.9d, -1, 2));
		assert_equals(1, foodFallCalc.calculateLevelOfLastStep(3, 12.0d, -1, 2));
	}
	
	private void assert_equals(int expected, Integer actual) {
		assertEquals(new Integer(expected), actual);
	}
	
	@Test
	public void stepTimeCalculator_FoodDoesNotEndsInsideFirstStep() {
		
		FoodFallCalculator foodFallCalc = new FoodFallCalculator(null);
		assertNull(foodFallCalc.calculateLevelOfLastStep(5, 0.5d, -1,1));
		assertNull(foodFallCalc.calculateLevelOfLastStep(5, 1.0d, -1,1));
		assertNull(foodFallCalc.calculateLevelOfLastStep(1, 0.5d, -1,1));
		assertNull(foodFallCalc.calculateLevelOfLastStep(1, 0.9d, -1,1));
	}
	
	@Test
	public void stepTimeCalculator_FoodWontFall() {
		
		FoodFallCalculator foodFallCalc = new FoodFallCalculator(null);
		assert_equals(-1, foodFallCalc.calculateLevelOfLastStep(3, 6.000000000001d, -1, 1));
		assert_equals(-1, foodFallCalc.calculateLevelOfLastStep(3, 6.1d, -1, 1));
		assert_equals(-1, foodFallCalc.calculateLevelOfLastStep(3, 7, -1, 1));
		assert_equals(-1, foodFallCalc.calculateLevelOfLastStep(1, 2, -1, 1));
		assert_equals(-1, foodFallCalc.calculateLevelOfLastStep(1, 1.1d, -1, 1));
		assert_equals(-1, foodFallCalc.calculateLevelOfLastStep(0, 100, -1, 1));
		assert_equals(-1, foodFallCalc.calculateLevelOfLastStep(0, 1, -1, 1));
		assert_equals(-1, foodFallCalc.calculateLevelOfLastStep(0, 0.5d, -1, 1));
		assert_equals(-1, foodFallCalc.calculateLevelOfLastStep(0, 0.2d, -1, 1));
		assert_equals(-1, foodFallCalc.calculateLevelOfLastStep(0, 0.1d, -1, 1));
		assert_equals(-1, foodFallCalc.calculateLevelOfLastStep(4, 5.1d, -2, 1));
		assert_equals(-1, foodFallCalc.calculateLevelOfLastStep(3, 12.1d, -1, 2));
		assert_equals(-1, foodFallCalc.calculateLevelOfLastStep(3, 12.5d, -1, 2));
	}
*/
	@Test
	public void oneFoodAlreadyInCity_floatPeopleAmount_noFactory_birthRateZero() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(0);
		mockPeopleAmount(10.5d);
		testFoodFallTime(water, 5.0d, fraction(1, 2));
	}

	private Rational fraction(int nominator, int denominator) {
		return Rational.valueOf(nominator, denominator);
	}

	@Test
	public void floatFoodAlreadyInCity_noFactory_birthRateZero() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(0);
		mockPeopleAmount(11.0d);
		testFoodFallTime(water, 5.5d, fraction(1, 2));
	}
	
	@Test
	public void oneFoodAlreadyInCity_noFactory_birthRateZero() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(0);
		mockPeopleAmount(10.0d);
		testFoodFallTime(water, 30.0d, fraction(3, 1));
	}
	
	@Test
	public void oneFoodAlreadyInCity_factorySquaresEating_birthRateZero() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(0);
		mockPeopleAmount(10.0d);
		mockWaterProduction(10);
		testFoodFallTime(water, 30.0d, null);
	}
	
	@Test
	public void oneFoodAlreadyInCity_sufficientFactory_birthRateZero() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(0);
		mockPeopleAmount(10.0d);
		mockWaterProduction(11);
		testFoodFallTime(water, 30.0d, null);
	}
	
	@Test
	public void oneFoodInCity_insufficientFactory_birthRateZero() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(0);
		mockWaterProduction(5);
		mockPeopleAmount(10.0d);
		mockFood(water, 30.0d);
		testFoodFallTime(water, 30.0d, fraction(6, 1));
	}
	
	@Ignore // propably wrong test, food can not be 0.
	@Test
	public void oneFoodInCity_sufficientFactory_birthRateZero() {
		
		mockWaterProduction(15);
		mockSOP(1);
		mockSN(0);
		mockBirthRate(0);
		mockPeopleAmount(10.0d);
		testFoodFallTime(water, 0.0d, null); // <- here food is 0
	}
	
	@Test
	public void twoFoodsInCity_noFactory_birthRateZero() {
		
		mockSOP(2);
		mockSN(0);
		mockBirthRate(0);
		mockPeopleAmount(10.0d);
		testFoodFallTime(water, 10.0d, fraction(2, 1));
		testFoodFallTime(rice, 10.0d, fraction(2, 1));
	}
	
	@Test
	public void threeFoodInCity_noFactory_birthRateZero() {
		
		mockSOP(3);
		mockSN(0);
		mockBirthRate(0);
		mockPeopleAmount(10.0d);
		
		testFoodFallTime(water, 10, fraction(3, 1));
		testFoodFallTime(rice, 10, fraction(3, 1));
		testFoodFallTime(pork, 10, fraction(3, 1));
	}
	
	@Test
	public void threeFoodInCity_sufficientWaterFactory_birthRateZero() {
		
		mockSOP(3);
		mockSN(0);
		mockBirthRate(0);
		mockProduction(waterFactory, 10);
		mockPeopleAmount(10.0d);
		mockFood(water, 10.0d);
		mockFood(rice, 10.0d);
		mockFood(pork, 10.0d);
		
		testFoodFallTime(water, 10, null);
		testFoodFallTime(rice, 10, fraction(3, 1));
		testFoodFallTime(pork, 10, fraction(3, 1));
	}
	
	@Test
	public void twoFoodsInCity_insufficientPorkFactory_birthRateZero() {
		
		final int porkFactoryProduction = 2;
		mockProduction(porkFactory, porkFactoryProduction); // - insufficient
		mockSOP(2);
		mockSN(porkFactoryProduction);
		mockBirthRate(0);
		mockPeopleAmount(10.0d);
		
		testFoodFallTime(water, 8, fraction(2, 1));
		testFoodFallTime(rice, 8, fraction(2, 1));
	}
	
	@Test
	public void threeFoodsInCity_sufficientPorkFactory_birthRateZero() {
		
		final int porkFactoryProduction = 4;
		mockProduction(porkFactory, porkFactoryProduction); // - sufficient
		mockSOP(3);
		mockSN(0);
		mockBirthRate(0);
		mockPeopleAmount(10);
		testFoodFallTime(water, 10, fraction(3, 1));
		testFoodFallTime(rice, 10, fraction(3, 1));
		// testFoodFallTime(pork, 0, null); probably wrong unit test, food can not be 0 while testing when food falls.
	}
	
	@Test
	public void oneFoodInCity_noSupportingFactories_birthRateZero() {
		
		mockEatPerHour(2);
		mockSOP(1);
		mockSN(0);
		mockBirthRate(0);
		mockPeopleAmount(10);
		testFoodFallTime(water, 10, fraction(1, 2));
	}
	
	@Test
	public void oneFoodInCity_floatPeopleAmount_noSupportingFactories_birthRateNegative() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(-1);
		mockPeopleAmount(10.5d);
		testFoodFallTime(water, 2.5d, fraction(1, 4));
		testFoodFallTime(water, 5.0d, fraction(1, 2));
		testFoodFallTime(water, 9.5d, fraction(1, 1));
		testFoodFallTime(water, 14.0d, fraction(3, 2));
		testFoodFallTime(water, 18.0d, fraction(2, 1));
		testFoodFallTime(water, 27.25d, fraction(13, 4));
		testFoodFallTime(water, 35, fraction(9, 2));
		testFoodFallTime(water, 50.0d, fraction(19, 2));
		testFoodFallTime(water, 50.1d, null);
	}
	
	@Test
	public void oneFoodInCity_integerPeopleAmount_noSupportingFactories_birthRateNegative() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(-1);
		mockPeopleAmount(11.0d);
		testFoodFallTime(water, 5, fraction(1, 2));
		testFoodFallTime(water, 10, fraction(1, 1));
		testFoodFallTime(water, 14.5d, fraction(3, 2));
		testFoodFallTime(water, 19, fraction(2, 1));
		testFoodFallTime(water, 23, fraction(5, 2));
		testFoodFallTime(water, 27, fraction(3, 1));
		testFoodFallTime(water, 55, fraction(10, 1));
		testFoodFallTime(water, 55.1d, null);
	}
	
	@Test
	public void oneFoodInCity_integerPeopleAmount_noSupportingFactories_birthRateMinusTwo() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(-2);
		mockPeopleAmount(11.0d);
		testFoodFallTime(water, 9.5, fraction(1, 1));
		testFoodFallTime(water, 17, fraction(2, 1));
	}
	
	@Test
	public void oneFoodInCity_lowIntegerPeopleAmount_noSupportingFactories_birthRateNegative() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(-1);
		mockPeopleAmount(2.0d);
		testFoodFallTime(water, 0.5d, fraction(1, 2));
		testFoodFallTime(water, 1, fraction(1, 1));
		testFoodFallTime(water, 1.1d, null);
	}
	
	@Test
	public void oneFoodInCity_lowIntegerPeopleAmount_eatPerHourTwo_noSupportingFactories_birthRateNegative() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(-1);
		mockEatPerHour(2);
		mockPeopleAmount(2.0d);
		testFoodFallTime(water, 0.5d, fraction(1, 4));
		testFoodFallTime(water, 1, fraction(1, 2));
		testFoodFallTime(water, 2, fraction(1, 1));
		testFoodFallTime(water, 2.1d, null);
	}
	
	@Test
	public void Area100_CC_oneFood_SNoff_birthRateNegative() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(-1);
		mockPeopleAmount(1.5d);
		testFoodFallTime(water, 0.25, fraction(1, 4));
		testFoodFallTime(water, 0.5, fraction(1, 2));
		testFoodFallTime(water, 0.6, null);
		testFoodFallTime(water, 100, null);
	}
	
	@Test
	public void oneFoodInCity_cornerCasePeopleAmount_noSupportingFactories_birthRateNegative() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(-1);
		mockPeopleAmount(1.0d);
		testFoodFallTime(water, 0.0001, null);
		testFoodFallTime(water, 0.05, null);
		testFoodFallTime(water, 0.1, null);
		testFoodFallTime(water, 0.5, null);
	}
	
	@Test
	public void oneFoodInCity_cornerCaseFloatPeopleAmount_noSupportingFactories_birthRateNegative() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(-1);
		mockPeopleAmount(0.5d);
		testFoodFallTime(water, 0.001d, null);
		testFoodFallTime(water, 0.1d, null);
		testFoodFallTime(water, 0.5d, null);
		testFoodFallTime(water, 2.0d, null);
	}
	
	
	@Test
	public void oneFoodInCity_IntegerPeopleAmount_workingFactory_birthRateNegative() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(-1);
		mockPeopleAmount(6);
		mockProduction(waterFactory, 1);
		testFoodFallTime(water, 8, fraction(5, 2));
		testFoodFallTime(water, 9, fraction(3, 1));
		testFoodFallTime(water, 9.5, fraction(7, 2));
		testFoodFallTime(water, 10, fraction(4, 1));
		testFoodFallTime(water, 11, null);
	}
	
	@Test
	public void oneFoodInCity_FloatPeopleAmount_workingFactory_birthRateNegative() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(-1);
		mockPeopleAmount(5.5);
		mockProduction(waterFactory, 2);
		testFoodFallTime(water, 1, fraction(1, 3));
		testFoodFallTime(water, 2, fraction(3, 4));
		testFoodFallTime(water, 4, fraction(2, 1));
		testFoodFallTime(water, 5, null);
		testFoodFallTime(water, 8, null);
	}
	
	@Test
	public void twoFoodInCity_IntegerPeopleAmount_noFactory_birthRateNegative() {
		
		mockSOP(2);
		mockSN(0);
		mockBirthRate(-1);
		mockPeopleAmount(6);
		
		testFoodFallTime(water, 7.5d, fraction(5, 1));
	}
	
	@Test
	public void twoFoodInCity_FloatPeopleAmount_noFactory_birthRateNegative() {
		
		mockSOP(2);
		mockSN(0);
		mockBirthRate(-1);
		mockPeopleAmount(5.5d);
		testFoodFallTime(water, 6.0d, fraction(4, 1));
	}
	
	@Test
	public void twoFoodInCity_FloatPeopleAmount_workingFactory_foodShouldNotTerminate_birthRateNegative() {
		
		mockSOP(2);
		mockSN(0);
		mockProduction(waterFactory, 1);
		mockBirthRate(-1);
		mockPeopleAmount(5.5d);
		testFoodFallTime(water, 6.0d, null);
	}
	
	@Test
	public void twoFoodInCity_FloatPeopleAmount_workingFactory_foodShouldNotTerminate_anotherExample_birthRateNegative() {
		
		mockSOP(2);
		mockSN(0);
		mockProduction(waterFactory, 1);
		mockBirthRate(-1);
		mockPeopleAmount(5.5d);
		testFoodFallTime(water, 4.0d, null);
	}
	
	@Test
	public void twoFoodInCity_FloatPeopleAmount_workingFactory_foodShouldTerminate_birthRateNegative() {
		
		mockSOP(2);
		mockSN(0);
		mockProduction(waterFactory, 1);
		mockBirthRate(-1);
		mockPeopleAmount(5.5d);
		testFoodFallTime(water, 2.0d, fraction(2, 1));
	}
	
	@Test
	public void twoFoodInCity_lowIntegerPeopleAmount_workingFactory_foodShouldTerminate_birthRateNegative() {
		
		mockSOP(2);
		mockSN(0);
		mockProduction(waterFactory, 1);
		mockBirthRate(-1);
		mockPeopleAmount(3.0d);
		testFoodFallTime(water, 1.5d, null);
	}
	
	@Test
	public void Areas001_oneFood_noFactory_brPos() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(1);
		mockPeopleAmount(3.0d);
		testFoodFallTime(water, 1.5d, fraction(1, 2));
	}
	
	@Test
	public void Areas011_oneFood_noFactory_brPos_() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(1);
		mockPeopleAmount(3.0d);
		testFoodFallTime(water, 5.0d, fraction(3, 2));
	}
	
	@Test
	public void Areas100_oneFood_noFactory_brPos_() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(1);
		mockPeopleAmount(3.5d);
		testFoodFallTime(water, 0.75d, fraction(1, 4));
	}

	@Test
	public void Areas110_oneFood_noFactory_brPos_() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(1);
		mockPeopleAmount(3.5d);
		testFoodFallTime(water, 5.5d, fraction(3, 2));
	}
	
	@Test
	public void Areas111_oneFood_noFactory_brPos_() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(1);
		mockPeopleAmount(3.5d);
		testFoodFallTime(water, 6.75d, fraction(7, 4));
	}
	
	// CC - corner case
	@Test
	public void Areas010_CC_oneFood_noFactory_brPos_() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(1);
		mockPeopleAmount(0.0d);
		testFoodFallTime(water, 3.0d, fraction(3, 1));
	}
	
	@Test
	public void Areas100_threeFoods_workingFactory_brPos_() {
		
		mockSOP(3);
		mockSN(0);
		mockBirthRate(1);
		mockPeopleAmount(10.0d);
		mockProduction(waterFactory, 1);
		testFoodFallTime(water, 1.0d, fraction(3, 7));
	}
	
	@Test
	public void Areas010_oneFood_workingFactory_brPos_() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(1);
		mockPeopleAmount(5.0d);
		mockProduction(waterFactory, 5);
		testFoodFallTime(water, 6.0d, fraction(4, 1));
	}
	
	@Test
	public void Areas011_oneFood_workingFactory_brPos_() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(1);
		mockPeopleAmount(5.0d);
		mockProduction(waterFactory, 5);
		testFoodFallTime(water, 5.0d, fraction(11, 3));
	}
	
	@Test
	public void Areas111_oneFood_workingFactory_brPos_() {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(1);
		mockPeopleAmount(5.5d);
		mockProduction(waterFactory, 3);
		testFoodFallTime(water, 6.0d, fraction(2, 1));
	}
	
	@Test
	public void Areas111_threeFoods_workingFactory_brPos_() {
		
		mockSOP(3);
		mockSN(0);
		mockBirthRate(1);
		mockPeopleAmount(5.5d);
		mockProduction(waterFactory, 3);
		testFoodFallTime(water, 2.5d, fraction(115, 12));
	}
	
	@Test
	public void Areas011_twoFoods_factoryOn_brPos_sFactoriesOn() {
		
		mockSOP(2);
		mockSN(3);
		mockBirthRate(1);
		mockPeopleAmount(10.0d);
		mockProduction(waterFactory, 1);
		testFoodFallTime(water, 15.0d, fraction(40, 9));
	}
	
	@Test
	public void Areas111_twoFoods_factoryOff_brPos_sFactoriesOn() {
		
		mockSOP(2);
		mockSN(1);
		mockBirthRate(1);
		mockPeopleAmount(6.5d);
		mockProduction(waterFactory, 0);
		testFoodFallTime(water, 6.0d, fraction(2, 1));
	}
	
	/*
	 * For this special case look at method description: 
	 * DefaultRecursionCalculator.getFoodFallTime();
	 */
	@Test
	public void foodFallTimeReturnsZeroEvenThoughFoodIsPositive_BrO_CC() throws Exception {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(0);
		mockPeopleAmount(1.0d);
		mockEatPerHour(2);
		mockFood(water, min_double());
		assertEquals(0.0d, recCalc.getFoodFallTime(water), 0.0d);
	}
	
	/*
	 * For this special case look at method description: 
	 * DefaultRecursionCalculator.getFoodFallTime();
	 */
	@Test
	public void foodFallTimeReturnsZeroEvenThoughFoodIsPositive_negBr_CC() throws Exception {
		
		mockSOP(1);
		mockSN(0);
		mockBirthRate(-1);
		mockPeopleAmount(2.0d);
		mockEatPerHour(2);
		mockFood(water, min_double());
		assertEquals(0.0d, recCalc.getFoodFallTime(water), 0.0d);
	}
	
		private double min_double() {
		    double result = 1.0d;
		    while(true) {
		        if((result / 2) == 0.0) {
		            return result;
		        }
		        result = result / 2;
		    }
		}
		
		private void testFoodFallTime(CityFood food, double foodAmount, Rational expectedFallTimeInHours) {
			
			mockFood(food, foodAmount);
			if(expectedFallTimeInHours == null)
				assertNull(recCalc.getFoodFallTime(food));
			else
				assertEquals(expectedFallTimeInHours.doubleValue(), recCalc.getFoodFallTime(food), delta);
		}

		private void mockEatPerHour(int eph) {
			when(wd.getEatPerHour()).thenReturn(eph);
		}
		
		private void mockWaterProduction(int i) {
			when(waterFactory.getOutputPerHour(city)).thenReturn(i);
		}

		private void mockBirthRate(int br) {
			when(people.getBirthRatePerHour()).thenReturn(br);
		}
	
		private void mockFood(CityFood food, double amount) {
			when(food.getAmount()).thenReturn(amount);
		}
	
		private void mockPeopleAmount(double amount) {
			when(people.getAmount()).thenReturn(amount);
		}
	
		private void mockSN(int SN) {
			when(cityStocks.getSN()).thenReturn(SN);
		}
	
		private void mockSOP(int SOP) {
			when(cityStocks.getSOP()).thenReturn(SOP);
		}
}
