package entities.stocks;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IWorldDefinition;
import helpers.TimeTranslation;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/*
 * see images to this tests in src/entities/stocks/DefaultEcoCalculatorTests_img
 */
@RunWith(Enclosed.class)
public class DefaultEcoCalculatorTests {
	
	@Mock
	CityStockSet cityStocks;
	@Mock
	CityPeople cityPeople;
	@Mock
	IWorldDefinition wd;
	
	DefaultEcoCalculator ecoCalc;
	
	final double delta = 0.00000005d;
	final long oneHourNs = 60l*60l*1000l*1000l*1000l;
	final long halfHourNs = oneHourNs /2l;
	final long quaterHourNs = halfHourNs / 2l;
	final long twoHoursNs = 2l*oneHourNs;
	final long threeHoursNs = 3l*oneHourNs;
	final long fiveHoursNs = 5l*oneHourNs;
	
	@Before
	public void setup() {
		
		when(cityStocks.getPeopleStock()).thenReturn(cityPeople);
		when(cityStocks.getWorldDefinition()).thenReturn(wd);
		when(wd.getEatPerHour()).thenReturn(1);
		ecoCalc = new DefaultEcoCalculator(cityStocks);
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class GetFoodTests extends DefaultEcoCalculatorTests {
		
		@Mock
		CityFood water;
		final String waterId = "waterId";
	
		@Before
		public void getFoodTestsSetup() {
			
			when(water.getCityStocks()).thenReturn(cityStocks);
			when(cityStocks.getCityFood(waterId)).thenReturn(water);
		}
		
		@Test
		public void simpleCase() {
			
			mockSOP(1);
			mockSN(0);
			when(cityPeople.getAmount()).thenReturn(10.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
			when(water.getAmount()).thenReturn(14.0d);
			assertEquals(0.0d, ecoCalc.getFood(waterId, oneHourNs + halfHourNs), delta);
		}
		
		@Test
		public void simpleCase_birthRateZero() {
			
			mockSOP(1);
			mockSN(0);
			when(cityPeople.getAmount()).thenReturn(10.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(0);
			when(water.getAmount()).thenReturn(14.0d);
			assertEquals(4.0d, ecoCalc.getFood(waterId, oneHourNs), delta);
		}
		
		@Test
		public void simpleCase_birthRateZero_PeopleAlmostReachBorder() {
			
			mockSOP(1);
			mockSN(0);
			when(cityPeople.getAmount()).thenReturn(10.8d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(0);
			when(water.getAmount()).thenReturn(14.0d);
			assertEquals(4.0d, ecoCalc.getFood(waterId, oneHourNs), delta);
		}
		
		@Test
		public void simpleCase_birthRateZero_PeopleOnBorder() {
			
			mockSOP(1);
			mockSN(0);
			when(cityPeople.getAmount()).thenReturn(11.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(0);
			when(water.getAmount()).thenReturn(14.0d);
			assertEquals(3.0d, ecoCalc.getFood(waterId, oneHourNs), delta);
		}
		
		@Test
		public void simpleCase_birthRatePositive() {
			
			mockSOP(1);
			mockSN(0);
			when(cityPeople.getAmount()).thenReturn(11.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
			when(water.getAmount()).thenReturn(24.0d);
			assertEquals(1.0d, ecoCalc.getFood(waterId, twoHoursNs), delta);
		}
		
		@Test
		public void simpleCase_birthRatePositive_supportingFactory() {
			
			mockSOP(1);
			mockSN(4);
			when(cityPeople.getAmount()).thenReturn(11.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
			when(water.getAmount()).thenReturn(24.0d);
			assertEquals(9.0d, ecoCalc.getFood(waterId, twoHoursNs), delta);
		}
		
		@Test
		public void accuracyTest() {
			
			mockSOP(1);
			mockSN(0);
			when(cityPeople.getAmount()).thenReturn(10.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
			when(water.getAmount()).thenReturn(14.0d);
			assertNotEquals(ecoCalc.getFood(waterId, oneHourNs),
					ecoCalc.getFood(waterId, oneHourNs + 1), 0.0d);
		}
		
		@Test
		public void accuracyTestCase2() {
			
			mockSOP(1);
			mockSN(0);
			when(cityPeople.getAmount()).thenReturn(10.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
			when(water.getAmount()).thenReturn(14.0d);
			assertNotEquals(ecoCalc.getFood(waterId, oneHourNs + halfHourNs),
					ecoCalc.getFood(waterId, oneHourNs + halfHourNs - 1), 0.0d);
		}
		
		private void mockSN(int SN) {
			when(cityStocks.getSN()).thenReturn(SN);
		}
	
		private void mockSOP(int SOP) {
			when(cityStocks.getSOP()).thenReturn(SOP);
		}
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class FoodEatenByPeopleTests extends DefaultEcoCalculatorTests {
		
		@Test
		public void negativeBirthRate_case01() {
			
			when(cityPeople.getAmount()).thenReturn(19.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
			assertEquals(36, ecoCalc.foodEatenByPeople(twoHoursNs), delta);
		}
		
		@Test
		public void negativeBirthRate_case02() {
			
			when(cityPeople.getAmount()).thenReturn(19.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
			assertEquals(9, ecoCalc.foodEatenByPeople(halfHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_case03() {
			
			when(cityPeople.getAmount()).thenReturn(19.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
			assertEquals(9.5d, ecoCalc.foodEatenByPeople(halfHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_case04() {
			
			when(cityPeople.getAmount()).thenReturn(19.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
			assertEquals(27.5d, ecoCalc.foodEatenByPeople(oneHourNs + halfHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_case05() {
			
			when(cityPeople.getAmount()).thenReturn(19.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
			assertEquals(4.75d, ecoCalc.foodEatenByPeople(quaterHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_case06() {
			
			when(cityPeople.getAmount()).thenReturn(11.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-2);
			assertEquals(27.5d, ecoCalc.foodEatenByPeople(fiveHoursNs), delta);
		}
		
		@Test
		public void negativeBirthRate_case07() {
			
			when(cityPeople.getAmount()).thenReturn(11.3d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-2);
			assertEquals(27.85d, ecoCalc.foodEatenByPeople(twoHoursNs + twoHoursNs + quaterHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_case08() {
			
			when(cityPeople.getAmount()).thenReturn(11.3d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-2);
			
			assertEquals(27.5d + 1.65d, ecoCalc.foodEatenByPeople(fiveHoursNs + halfHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_case09() {
			
			when(cityPeople.getAmount()).thenReturn(11.3d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-2);
			
			assertEquals(27.5d + 1.65d, ecoCalc.foodEatenByPeople(fiveHoursNs + quaterHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_case10() {
			
			when(cityPeople.getAmount()).thenReturn(8.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
	
			assertEquals(7.0d, ecoCalc.foodEatenByPeople(oneHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_case11() {
			
			when(cityPeople.getAmount()).thenReturn(20.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
	
			assertEquals(19.0d, ecoCalc.foodEatenByPeople(oneHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_case12() {
			
			when(cityPeople.getAmount()).thenReturn(10.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
			
			assertEquals(14.0d, ecoCalc.foodEatenByPeople(oneHourNs + halfHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_accuracyTest() {
			
			when(cityPeople.getAmount()).thenReturn(10.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
			
			assertNotEquals(ecoCalc.foodEatenByPeople(oneHourNs + halfHourNs),
					ecoCalc.foodEatenByPeople(oneHourNs + halfHourNs - 1));
		}
		
		@Test
		public void negativeBirthRate_cornerCase01() {
			
			when(cityPeople.getAmount()).thenReturn(0.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
	
			assertEquals(0.0d, ecoCalc.foodEatenByPeople(halfHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_cornerCase02() {
			
			when(cityPeople.getAmount()).thenReturn(1.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
	
			assertEquals(0.0d, ecoCalc.foodEatenByPeople(oneHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_cornerCase03() {
			
			when(cityPeople.getAmount()).thenReturn(1.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
	
			assertEquals(0.5d, ecoCalc.foodEatenByPeople(halfHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_cornerCase04() {
			
			when(cityPeople.getAmount()).thenReturn(1.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
	
			assertEquals(0.5d, ecoCalc.foodEatenByPeople(oneHourNs + halfHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_cornerCase05() {
			
			when(cityPeople.getAmount()).thenReturn(2.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
	
			assertEquals(1.0d, ecoCalc.foodEatenByPeople(twoHoursNs), delta);
		}
		
		@Test
		public void negativeBirthRate_cornerCase06() {
			
			when(cityPeople.getAmount()).thenReturn(2.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
	
			assertEquals(0.5d, ecoCalc.foodEatenByPeople(halfHourNs), delta);
		}
		
		@Test
		public void negativeBirthRate_cornerCase07() {
			
			when(cityPeople.getAmount()).thenReturn(1.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(-1);
	
			assertEquals(0.0d, ecoCalc.foodEatenByPeople(halfHourNs), delta);
		}
		
		@Test
		public void birthRateZero_case01() {
			
			when(cityPeople.getAmount()).thenReturn(19.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(0);
	
			assertEquals(38, ecoCalc.foodEatenByPeople(twoHoursNs), delta);
		}
		
		@Test
		public void birthRateZero_case02() {
			
			when(cityPeople.getAmount()).thenReturn(19.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(0);
	
			assertEquals(38, ecoCalc.foodEatenByPeople(twoHoursNs), delta);
		}
		
		@Test
		public void birthRateZero_case03() {
			
			when(cityPeople.getAmount()).thenReturn(18.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(0);
	
			assertEquals(36, ecoCalc.foodEatenByPeople(twoHoursNs), delta);
		}
		
		@Test
		public void birthRateZero_case04() {
			
			when(cityPeople.getAmount()).thenReturn(18.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(0);
	
			assertEquals(45, ecoCalc.foodEatenByPeople(twoHoursNs + halfHourNs), delta);
		}
		
		@Test
		public void birthRateZero_case05() {
			
			when(cityPeople.getAmount()).thenReturn(18.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(0);
	
			assertEquals(4.5d, ecoCalc.foodEatenByPeople(quaterHourNs), delta);
		}
		
		@Test
		public void birthRateZero_case06() {
			
			when(cityPeople.getAmount()).thenReturn(18.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(0);
			when(wd.getEatPerHour()).thenReturn(2);
	
			assertEquals(36, ecoCalc.foodEatenByPeople(oneHourNs), delta);
		}
		
		@Test
		public void birthRateZero_case07() {
			
			when(cityPeople.getAmount()).thenReturn(17.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(0);
			when(wd.getEatPerHour()).thenReturn(1);
	
			assertEquals(17.0d/4.0d, ecoCalc.foodEatenByPeople(quaterHourNs), delta);
		}
		
		@Test
		public void birthRatePositive_case01() {
			
			when(cityPeople.getAmount()).thenReturn(0.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
	
			assertEquals(3.0d, ecoCalc.foodEatenByPeople(threeHoursNs), delta);
		}
		
		@Test
		public void birthRatePositive_case02() {
			
			when(cityPeople.getAmount()).thenReturn(0.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
	
			assertEquals(10.0d, ecoCalc.foodEatenByPeople(fiveHoursNs), delta);
		}
		
		@Test
		public void birthRatePositive_case03() {
			
			when(cityPeople.getAmount()).thenReturn(0.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
	
			assertEquals(0.0d, ecoCalc.foodEatenByPeople(halfHourNs), delta);
		}
		
		@Test
		public void birthRatePositive_case04() {
			
			when(cityPeople.getAmount()).thenReturn(0.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
	
			assertEquals(0.5d, ecoCalc.foodEatenByPeople(oneHourNs), delta);
		}
		
		@Test
		public void birthRatePositive_case05() {
			
			when(cityPeople.getAmount()).thenReturn(0.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
	
			assertEquals(4.5d, ecoCalc.foodEatenByPeople(threeHoursNs), delta);
		}
		
		@Test
		public void birthRatePositive_case06() {
			
			when(cityPeople.getAmount()).thenReturn(0.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
	
			assertEquals(1.0d, ecoCalc.foodEatenByPeople(oneHourNs + halfHourNs), delta);
		}
		
		@Test
		public void birthRatePositive_case07() {
			
			when(cityPeople.getAmount()).thenReturn(3.5d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
	
			assertEquals(8.0d, ecoCalc.foodEatenByPeople(twoHoursNs), delta);
		}
		
		@Test
		public void birthRatePositive_case08() {
			
			when(cityPeople.getAmount()).thenReturn(3.1d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
	
			assertEquals(2.7d + 4.0d + 3.0d, ecoCalc.foodEatenByPeople(twoHoursNs + halfHourNs), delta);
		}
		
		@Test
		public void birthRatePositive_cornerCase01() {
			
			when(cityPeople.getAmount()).thenReturn(0.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
	
			assertEquals(0.0d, ecoCalc.foodEatenByPeople(oneHourNs), delta);
		}
		
		@Test
		public void birthRatePositive_cornerCase02() {
			
			when(cityPeople.getAmount()).thenReturn(0.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
	
			assertEquals(0.5d, ecoCalc.foodEatenByPeople(oneHourNs + halfHourNs), delta);
		}
		
		@Test
		public void birthRatePositive_cornerCase03() {
			
			when(cityPeople.getAmount()).thenReturn(0.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
	
			assertEquals(1.0d, ecoCalc.foodEatenByPeople(twoHoursNs), delta);
		}
		
		@Test
		public void birthRatePositive_cornerCase04() {
			
			when(cityPeople.getAmount()).thenReturn(0.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
	
			assertEquals(1.0d, ecoCalc.foodEatenByPeople(twoHoursNs), delta);
		}
		
		@Test
		public void birthRatePositive_cornerCase05() {
			
			when(cityPeople.getAmount()).thenReturn(0.0d);
			when(cityPeople.getBirthRatePerHour()).thenReturn(1);
	
			assertEquals(0.0d, ecoCalc.foodEatenByPeople(halfHourNs), delta);
		}

	}
}
