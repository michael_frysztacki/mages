package entities.stocks;

import java.util.Comparator;

import framework.FloatingPointStock;

public class StockDoubleCountComparator implements Comparator<FloatingPointStock<Double>>{

	@Override
	public int compare(FloatingPointStock<Double> o1, FloatingPointStock<Double> o2) {
		// TODO Auto-generated method stub
		double result = o1.getVisibleAmount() - o2.getVisibleAmount();
		if(result > 0.0d)return 1;
		else if(result < 0.0d)return -1;
		else return 0;
	}

	

}
