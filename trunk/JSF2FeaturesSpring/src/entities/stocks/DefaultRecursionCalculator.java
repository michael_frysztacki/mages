package entities.stocks;

import org.jscience.mathematics.number.Rational;

import economycomputation.RecursionCalculator;
import entities.UsersCityConfig;
import game_model_impl.StockType;
import game_model_interfaces.IExtractBuildingModel;
import helpers.TimeTranslation;

public class DefaultRecursionCalculator implements RecursionCalculator{

	@Override
	public Double getFoodRaiseTime(CityFood cs) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * Do not provide already empty food - 0 amount. 
	 * If the real time when food terminates is less that minimum positive double value,
	 * the method returns 0.0d.
	 * 
	 */
	@Override
	public Double getFoodFallTime(CityFood cf) {
		
		return new FoodFallCalculator(cf).calulateTimeToEndOfFood();
		
	}
	
	static class FoodFallCalculator {
		
		private CityFood cf;
		private Integer levelOfLastIntegerStep = noIntegerStepsState;
		private double allFoodAlreadyEaten;
		//private double foodEatenDuringIntegerSteps;
		private final static Integer noIntegerStepsState = null;
		private final static Integer foodWillNeverEndState = -1;
		FoodFallCalculator(CityFood cf) {
			this.cf = cf;
		}
		
		Double calulateTimeToEndOfFood() {
			if(birthRatePerHour() != 0)
				return calculateForNonZeroBirstRate();
			return timeOfFoodEndAtBaseStep();
		}
	
			private Double calculateForNonZeroBirstRate() {
				if(foodAtBaseStepWillEnd() && timeOfFoodEndAtBaseStep() <= timeToEndOfFirstStep())
					return timeOfFoodEndAtBaseStep();
				else
					return calculateWithSteps();		
			}

				private boolean foodAtBaseStepWillEnd() {
					return timeOfFoodEndAtBaseStep() != null;
				}

			private Double calculateWithSteps() {
				calculateFoodEatenToEndOfFirstStep();
				calculateLevelOfLastStep();
				if(foodWillNeverEnd())
					return null;
				calculateFoodEatenDuringIntegerSteps();
				double a = timeToEndOfFirstStep();
				double b = timeDuringIntegerSteps();
				double c = timeOnLastStep();
				return timeToEndOfFirstStep() + timeDuringIntegerSteps() + timeOnLastStep();
			}

			private boolean foodWillNeverEnd() {
				return levelOfLastIntegerStep == foodWillNeverEndState;
			}
			private int birthRatePerHour() {
				return cf.getCityStocks().getPeopleStock().getBirthRatePerHour();
			}
			/*
			 * if food at base step never ends, returns null
			 */
			private Double timeOfFoodEndAtBaseStep() {
				return foodTerminationTimeWithBirthRateZero(cf.getAmount(), floorPeopleAmount());
			}
			private double timeToEndOfFirstStep() {
				return peopleEnding() * Math.abs(invBirthRate());
			}
			private void calculateFoodEatenToEndOfFirstStep() {
				allFoodAlreadyEaten = timeToEndOfFirstStep() * floorPeopleAmount();
			}
			private double timeDuringIntegerSteps() {
				if(levelOfLastIntegerStep == null)
					return 0;
				if(isAtLeastOneIntegerStep())
					return numberOfIntegerSteps() / (double)Math.abs(birthRatePerHour());
				else 
					return 0;
			}
			private double timeOnLastStep() {
				
				return foodTerminationTimeWithBirthRateZero(singleFoodLeft(), lastFragmentaryStep());
			}

				private int lastFragmentaryStep() {
					if(!isAtLeastOneIntegerStep())
						return birthRatePerHour() < 0 ? floorPeopleAmount()-1 : floorPeopleAmount();
					else
						return birthRatePerHour() < 0 ? levelOfLastIntegerStep-1 : levelOfLastIntegerStep+1;
				}

				private boolean isAtLeastOneIntegerStep() {
					if(levelOfLastIntegerStep == noIntegerStepsState)
						return false;
					return levelOfLastIntegerStep == foodWillNeverEndState || levelOfLastIntegerStep > 0; 
				}
				/*
				 * p*eph*t - SPIF*t
				 * ---------------- = dbv + f*t
				 * 		 SOP
				 * 
				 * p - people amount
				 * eph - eat per hour
				 * t - time ( unknown variable )
				 * SPIF - sum of productions of insufficient factories
				 * SOP - sum of food providers
				 * dbv - food amount of food in context
				 * f - factory production for food in context
				 * 
				 * above formula can be transformed to:
				 * 
				 * 		      SOP*dbv
				 * t = ----------------------
				 * 		p*eph - SPIF - SOP*f
				 * 			
				 */
				private Double foodTerminationTimeWithBirthRateZero(double singleFoodAmount, int peopleAmount) {
					if(singleFoodAmount==0)
						return 0.0d;
					
					//Rational debugTime = (rat.times(SOP())).divide(Rational.valueOf(peopleAmount*eph() - SPIF() - fProduction()*SOP(), 1));
					//Double time = debugTime.doubleValue();
					Double time = (singleFoodAmount * SOP())/(peopleAmount*eph() - SPIF() - fProduction()*SOP());
					if(time.isInfinite() || time<0)
						return null;
					return time;
					
					//return (singleFoodAmount * SOP())/(peopleAmount*eph() - SPIF() - fProduction()*SOP());
					//return foodAmount / (divide(eatPerHour() * peopleAmount - SPIF(), SOP()) - factoryProductionPerHour());
				}
				/*
				private Rational doubleToRational(double number) {
					//double number =  -0.15625;
					// Code below doesn't work for 0 and NaN - just check before

					long bits = Double.doubleToLongBits(number);

					long sign = bits >>> 63;
					long exponent = ((bits >>> 52) ^ (sign << 11)) - 1023;
					long fraction = bits << 12; // bits are "reversed" but that's not a problem

					long a = 1L;
					long b = 1L;

					for (int i = 63; i >= 12; i--) {
					    a = a * 2 + ((fraction >>> i) & 1);
					    b *= 2;
					}

					if (exponent > 0)
					    a *= 1 << exponent;
					else
					    b *= 1 << -exponent;

					if (sign == 1)
					    a *= -1;
					
					return Rational.valueOf(a, b);
				}
				*/
				private double singleFoodLeft() {
					double a = cf.getAmount();
					double b = (allFoodAlreadyEaten - SPIF()*(timeDuringIntegerSteps()+timeToEndOfFirstStep()))/SOP();
					double c = foodProductedTillLastIntegerStep();
					return cf.getAmount() - (allFoodAlreadyEaten - SPIF()*(timeDuringIntegerSteps()+timeToEndOfFirstStep()))/SOP() + foodProductedTillLastIntegerStep();
					//return (singleFoodLeftForIntegerSteps()*SOP() - allFoodAlreadyEaten + foodProductedDuringIntegerSteps())/SOP();
				}
				
					private double foodProductedTillLastIntegerStep() {
						return foodProductedDuringIntegerSteps() + timeToEndOfFirstStep()*fProduction();
					}
				
				private double foodProductedDuringIntegerSteps() {
					return fProduction() * timeDuringIntegerSteps();
				}
					private void calculateFoodEatenDuringIntegerSteps() {
						
						if(levelOfLastIntegerStep != noIntegerStepsState)
							allFoodAlreadyEaten += averageStepLevel() * timeDuringIntegerSteps() * eph();
					}
			
				private int numberOfIntegerSteps() {
					if(birthRatePerHour() < 0)
						return Math.abs(stepLevelAfterBaseStep() - levelOfLastIntegerStep) + 1;
					else
						return Math.abs(levelOfLastIntegerStep - stepLevelAfterBaseStep()) + 1;
				}
			private double averageStepLevel() {
				return Math.abs(stepLevelAfterBaseStep() + levelOfLastIntegerStep)/2.0d;
			}	
		private int stepLevelAfterBaseStep() {
			if(birthRatePerHour() < 0)
				return floorPeopleAmount()-1;
			else
				return floorPeopleAmount() + (firstAreaExist() ? 1 : 0);
		}

		private boolean firstAreaExist() {
			return peopleEnding()!=0.0d;
		}
		
		/*
		 * for birthRate == 0 the result is undefined.
		 */
		void calculateLevelOfLastStep() {
			
			int topStep = stepLevelAfterBaseStep();
			if(birthRatePerHour() > 0)
				calculateLevelOfLastStepForPosBirthRate(topStep);	
			else
				calculateLevelOfLastStepForNegBirthRate(topStep);
		}
			private void calculateLevelOfLastStepForPosBirthRate(int topStep) {
				double stepLevel = solveQuadEqForPosBirthRate(topStep);
				if(stepLevel < stepLevelAfterBaseStep())
					{levelOfLastIntegerStep = noIntegerStepsState;return;}
				levelOfLastIntegerStep = (int)stepLevel;return;
			}

			private void calculateLevelOfLastStepForNegBirthRate(int topStep) {
				int debug = stepLevelAfterBaseStep(); // TODO remove
				if(stepLevelAfterBaseStep() <= 0)
					{levelOfLastIntegerStep = foodWillNeverEndState;return;}
				Double stepLevel = solveQuadEqForNegBirthRate(topStep);
				if(stepLevel > stepLevelAfterBaseStep())
					{levelOfLastIntegerStep = noIntegerStepsState;return;}
				if(stepLevelIsLessThanOne(stepLevel))
					{levelOfLastIntegerStep =  foodWillNeverEndState;return;}
				int stepLevelRounded = (int)stepLevel.intValue();
				if(stepLevelRounded == stepLevel)
					stepLevelRounded--;
				levelOfLastIntegerStep =  stepLevelRounded + 1;
			}

			/*
			 * formula for finding the last integer step for positive birth rate for given food
			 * 
			 * a + b
			 * ----- * ( b - a + 1 ) * ibr * eph - SPIF * ( b - a + 1 )
			 *   2
			 * ------------------------------------------------------   = dbv + p * ( b - a + 1 ) * ibr
			 * 							SOP
			 * 
			 * 'a' - first step level
			 * 'b' - last integer step level ( this is the unknown variable )
			 * ibr - 1/abs(birth rate)
			 * eph - eat per hour for single person in city
			 * SPIF - sum of productions of insufficient factories
			 * SOP - sum of food providers
			 * dbv - food value at the begining
			 * p - factory production for food in context
			 * 
			 * above formula can be transformed to:
			 * 
			 * b^2 ( ibr * eph ) +  
			 * + b ( ibr*eph - 2*SPIF - 2*SOP*ibr*p ) - 
			 * - a^2*ibr*eph + ibr*eph*a + 2*a*SPIF - 2*SPIF - 2*SOP*DBV + 2*SOP*ibr*p*a - 2*SOP*ibr*p
			 */
			private double solveQuadEqForPosBirthRate(int topStep) {
				return solveQuadraticEquation(
						Math.abs(invBirthRate())*eph(),
						
						Math.abs(invBirthRate())*eph() - 2*SPIF() - 2*fProduction()*SOP()*Math.abs(invBirthRate()),
						
						topStep*Math.abs(invBirthRate())*eph() - //
						topStep*topStep*Math.abs(invBirthRate())*eph() - // 
						2*singleFoodLeft()*SOP() + //
						2*topStep*SPIF() - //
						2*SPIF() +//
						2*fProduction()*topStep*Math.abs(invBirthRate())*SOP() - 
						2*fProduction()*Math.abs(invBirthRate())*SOP())[1];
			}

			private double solveQuadEqForNegBirthRate(int topStep) {
				return solveQuadraticEquation(
						-Math.abs(invBirthRate())*eph(),	
						Math.abs(invBirthRate())*(eph()+2*fProduction()*SOP()),
						Math.abs(invBirthRate())*(topStep*eph()+topStep*topStep*eph()-
								2*fProduction()*topStep*SOP()-
								2*fProduction()*SOP())-
								2*singleFoodLeft()*SOP())[0];
			}

		private double singleFoodLeftForIntegerSteps() {
			double ret = cf.getAmount() - allFoodAlreadyEaten/SOP() + fProduction()*timeToEndOfFirstStep();
			//return (singleFoodAmount * SOP())/(peopleAmount*eatPerHour() - SPIF() - factoryProductionPerHour()*SOP());
			return ret;
		}

			private boolean stepLevelIsLessThanOne(Double stepLevel) {
				return stepLevel.isNaN() || stepLevel < 1;
			}
		
		private double invBirthRate() {
			return 1.0d / Math.abs(birthRatePerHour());
		}

		private double peopleEnding() {
			return cf.getCityStocks().getPeopleStock().getAmount() - Math.floor(cf.getCityStocks().getPeopleStock().getAmount()); 
		}

		private double[] solveQuadraticEquation(double a, double b, double c) {
			double delta = b*b - 4*a*c;
			double[] result = new double[2];
			result[0] = ((-b) - Math.sqrt(delta))/(2*a);
			result[1] = ((-b) + Math.sqrt(delta))/(2*a);
			return result;
		}
		
			private double divide(int nominator, int denominator) {
				return (double)nominator/(double)denominator;
			}

			private int SPIF() {
				int spif = cf.getCityStocks().getSN();
				return spif;
			}

			private int SOP() {
				return cf.getCityStocks().getSOP();
			}

			//factory production per hour
			private int fProduction() {
				IExtractBuildingModel foodFactory = cf.getWorldDefinition().getProducer(cf.getStockId());
				return foodFactory.getOutputPerHour(cf.getCity());
			}
		
			private int floorPeopleAmount() {
				return (int)cf.getCityStocks().getPeopleStock().getAmount();
			}
		
			private int eph() {
				return cf.getWorldDefinition().getEatPerHour();
			}
	}

	@Override
	public Double getFoodChangeTime(UsersCityConfig ucc, StockType rt,
			double A, double B, double C, boolean isFalling) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double freePeopleFall(UsersCityConfig ucc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double timeWhenPeopleArriveLimit(UsersCityConfig ucc) {
		// TODO Auto-generated method stub
		return null;
	}

}
