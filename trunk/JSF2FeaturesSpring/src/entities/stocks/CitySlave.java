package entities.stocks;

import economycomputation.EcoCalculator;

@Loot
@TransportStock
public abstract class CitySlave extends CityStock {

	public void init(CityStockSet cityStocks, int startAmount) {
		super.init("", cityStocks, startAmount);
	}

	public void process(EcoCalculator ecoCalc, long nsTime) {
		return;
	}

}
