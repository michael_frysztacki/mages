package entities.stocks;

import game_model_interfaces.IUnitModel;
import helpers.CollectionUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


import com.jsfsample.managedbeans.SpringJSFUtil;
import comparators.ResourceProductionComparator;
import economycomputation.ProductionUtil;
import entities.Storage;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.ObjectBuildQueue;
import entities.buildings.QueueBuild;
import entities.buildings.UnitBuild;
import entities.units.CityUnitGroup;


public class CityFoodInitializer {

	public void initStorage(CityStockSet cityStocks) {
		return;
	}
	/*
	 * b��d por�wnania double
	 * gdy jest przej�cie w rekurencji np. na stan 'na styk'.
	 * czas zmiany double(w godzinach) jest rzutowany na ns.
	 * Tracimy przy tym dok�adno��. Maksymalna dok�adno�� jaka mo�� by� otracona, to tyle, w ile mo�e przyj�� ludzi w w czasie 1 ns.
	 * Gdy birthRate wynosi 1kk( a takiego w naszej grze na pewno nigdy nie ustawimy) w ci�gu jednej ns mo�e przyjsc/odej�� 0.00000027 ludzi.
	 */
	public final static double eps = 0.00000027d;
	
	public Map<String,Number> getSN_SOP2(UsersCityConfig ucc)
	{
		Map<String,Number> ret = new HashMap<String, Number>();
		WorldDefinition ch = SpringJSFUtil.getCivilizationHolder();
		ProductionUtil pu = SpringJSFUtil.getProductionUtil();
		UsersCityConfig config = ch.getCivilization(ucc.getUserConfig().getCivil());
		CivilConfigBoostable ccb = config.getCivilConfig();
		Set<CityFood> ccfList =  ucc.getCityStocks().getFood() ;
		for(CityFood cf : ccfList)
		{
			if(cf.getVisibleAmount() > 0.0d) this.freeHand(cf, Storage.tak) ;
			else this.freeHand(cf, Storage.nie);
		}
		ArrayList<StockType> niedomiaroweFabryki = new ArrayList<StockType>();
		ArrayList<StockType> potNadmiarowcy =  ucc.getCityStocks().getNotStoredFood();
		ArrayList<StockType> delPotNadmiarowcy = new ArrayList<StockType>();
		for(StockType rt : potNadmiarowcy)
		{
			if(pu.getFoodProduction(rt,ucc) == 0)
			{
				delPotNadmiarowcy.addEdgeAndReversedEdge(rt);
			}
		}
		potNadmiarowcy.removeAll(delPotNadmiarowcy);
		/*
		 * 
		 * UWAGA !
		 * tutaj jeszcze pozniej trzeba b�dzie dorzuci� zjadanie przez niewolnik�w
		 */
		double eatPerHour = ucc.getPeopleEatPerHour() + ucc.getMyArmy().getEatPerHour() + ucc.getTrainedPeople()*config.getEatPerHour();
		
		
		/*
		 * jedzenie na styk.
		 * Jedzenie na styk ustawiamy wtedy, kiedy nie mamy �adnego pozywienia w kraju (warunek [1*]),
		 * tzn. wszystkiego mamy 0, ca�kowita produkcja jedzenia jest r�wna ilo�ci jedzenia zjadanego w kraju w 
		 * danym momencie (warunek [2*]), oraz pasek ludzi jest ustawion na wiecej lub r�wno aktualna ilosc ludzi (warunek [3*])(dlatego, �e jak ustawimy
		 * pasek ponizej, to jedzenie juz nie bedzie nastyk, tylko moze go przybywac).
		 */
		
		/*
		 * pierwszy warunek 1*
		 */
		if(ucc.getCityStocks().getNotStoredFood().size() == ucc.getCityStocks().getFood().size() )
		{
			double sumOfProductions = pu.sumOfFacotoryProductions(ucc.getCityStocks().getNotStoredFood(), ucc);
			/*
			 * drugi warunek 2*
			 */
			if(Math.abs(sumOfProductions - eatPerHour) < eps)
			{
				/*
				 * trzeci warunek 3*
				 * pasek wi�kszy lub r�wny
				 */
				
				if(ucc.getMaxLudzi() > ucc.getCityStocks().getPeopleStock().getVisibleAmount() || 
						Math.abs((double)ucc.getMaxLudzi() - ucc.getCityStocks().getPeopleStock().getVisibleAmount()) < eps)
				{
					double SN = 0;
					for(CityFood cf : ucc.getCityStocks().getFood() )
					{
						SN += pu.getFoodProduction(cf.getStockType(), ucc);
					}
					if(Math.abs( SN - eatPerHour ) < eps)
					{
						for(CityFood cf : ucc.getCityStocks().getFood() )
						{
							/*
							 * tutaj ustawiamy jedzenie kt�re w ogole jest produkowane na styk.
							 * Oznacza to, �e kazde jedzenie ktorego produkcja jest wi�ksza od 0
							 * bedzie mia�o storage 'nastyk'.
							 */
							if(pu.getFoodProduction(cf.getStockType(), ucc) > 0)
							{
								
								this.freeHand(cf, Storage.nastyk);;
							}
							else
							{
								this.freeHand(cf, Storage.nie);;
							}
						}
					}
				}
			}
		}
		
		/*
		 * je�li powy�sze ify wykaza�y �e jest jedzenie na styk,
		 * przejd� do poni�szego ifa, wylicz SN i SOP oraz wyjdz z tej metody.
		 */
		if(ucc.getCityStocks().getNastykStoredFood().size() > 0)
		{
			double foodProductions = 0;
			for(StockType rt : ucc.getCityStocks().getNastykStoredFood())
			{
				foodProductions += pu.getFoodProduction(rt, ucc);
			}
			ret.put("SOP", 0);
			ret.put("SN", foodProductions);
			return ret;
		}
		
		/*
		 * tutaj jest p�tla odrzucaj�ca po kolei surowce kt�re nie wyrabiaj�.
		 * sprawdzanie kt�re fabryki s� potencjalnymi nadmiarowcami a kt�re nie.
		 */
		while(true) {
			List<StockType> lp = CollectionUtil.findMaxMinObjects(potNadmiarowcy, new ResourceProductionComparator(ucc, pu), false);
			//ArrayList<ResourceType> lp = getLowestProductions(potNadmiarowcy, ucc);
			if(lp.size()==0)break;
			double SNproductions = 0;
			for(StockType rt : niedomiaroweFabryki)
			{
				SNproductions += pu.getFoodProduction(rt, ucc);
			}
			/*
			 * tutaj SNproductions ulega w ka�dej p�tli zwi�kszaniu wskutek odrzucania fabryk, kt�re nie wyrabiaj�
			 */
			double O = (eatPerHour - SNproductions) / (double)(ucc.getCityStocks().getStoredFood().size() + potNadmiarowcy.size());
			/*
			 * 
			 * powy�sze 'O' oznacza ile jest zjadane jednego jedzenia.
			 * poni�ej jest if, kt�ry sprawdza czy potencjalnie nadmiarowy surowiec, rzeczywi�cie jest nadmiarowy.
			 * je�li ten if przejdzie, to znaczy ze surowiec nie jest nadmiarowy i nale�y go usun�c z listy potNadmiarowcy i doda�
			 * do niedomiarowe fabryki.
			 * tutaj jest pewien myk na kt�ry trzeba uwa�a�:
			 * jest znak '<'
			 * Jesli jakies jedzenie produkuje dokladnie tyle ile jest potrzebne, czyli '0', to tymczasowo zostawiamy to jedzenie jako 
			 * potencjalny nadmiarowiec. To czy ko�cowo b�dzie nadmiarowcem, zale�y od tego czy ludzie od tego momentu zacz� przychodzi� do kraju
			 * czy odchodzi� (patrz [4*]). 
			 */
			if(pu.getFoodProduction(lp.get(0), ucc) < O && Math.abs(O - pu.getFoodProduction(lp.get(0), ucc)) > eps)
			{
				potNadmiarowcy.removeAll(lp);
				niedomiaroweFabryki.addAll(lp);
			}
			
			else break;
		}
		double SNproductions = 0;
		for(StockType rt : niedomiaroweFabryki)
		{
			SNproductions += pu.getFoodProduction(rt, ucc);
		}
		double finalO = (eatPerHour - SNproductions) / (double)(ucc.getCityStocks().getStoredFood().size() + potNadmiarowcy.size());
		
		for(StockType rt_ : potNadmiarowcy)
		{
			/*
			 * Je�li produkcja tego surowca jest na taka jak spozywane jedzenie jednego surowca, to trzeba sprawdzic, czy
			 * ludzie zaczn� przychodzi� czy odchodzi� z kraju. Je�li przychodzi�, to znaczy �e musimy ustawi� flage tego jedzenia na 'nie'
			 * w przeciwnym wypadku ustawiamy na 'tak'. W tym miejscu mo�na ju� u�ywa� getBirthRate, dlatego �e wst�pnie flagi jedze�
			 * s� poustawiane, �le mo�e by� ustawiona tylko ta flaga, kt�rej
			 * getFoodProduction(rt_, ucc) == finalO
			 * domy�lnie, flaga jedzenie jest ustawiona na nie. Musimy tutaj u�y� getBirthRate,
			 * z t� flag� 'nie', dlatego ze musimy sprawdzic, czy ludzie maj� zamiar odchodzi� czy przychodzi�.
			 * Je�li chc� odchodzi�, ustawiamy flag� na 'tak'.
			 * Je�li chc� przychodzi� lub nie zmienia� swojej ilo�� ustawiamy na 'nie' (czyli tak jak by�o).
			 * UWAGA. tutaj jest bardzo wa�ny punkt krytyczny.
			 * Mo�e sie zdarzy� tak, �e je�li BirthRate() < 0 i ustawimy t� flag� ma 'tak',
			 * birthRate mo�e przeskoczy� na +.Wtedy pojawia si� hu�tawka.
			 * Musimy j� zablokowa� stanem 'na styk', ale tylko dla tego jedzenia.
			 * Mo�e sie zdarzy� tak, �e to jedzenie ustawimy 'na styk', ale s� te� inne jedzenia kt�re maj� inne stany np. 'nie'(patrz test GET_SN_SOP - test68).
			 * Na przychodzenie/odchodzenie maj� wp�yw jeszcze inne czyniki, takie jak podatki, kt�re s� zawarte w getBirthRate().
			 * 
			 * [4*]
			 */
			if(Math.abs( pu.getFoodProduction(rt_, ucc) - finalO) < eps && ucc.getBirthRatePerHour() >= 0)
			{
				CityFood local = (CityFood) ucc.getCityStocks().getStockByType(rt_);
				this.freeHand(local, Storage.nie);
				//ucc.getMyFood().getFood(rt_) .setStorage(Storage.nie);
				//removePotNadm.add(rt_);
				niedomiaroweFabryki.addEdgeAndReversedEdge(rt_);
			}
			else
			{
				CityFood local = (CityFood) ucc.getCityStocks().getStockByType(rt_);
				this.freeHand(local, Storage.tak);
				/*
				 * sprawdzamy jeszcze raz czy zmiana powy�szej flagi nie spowodowa�a, zmiany getBirthRate() na znak przeciwny,
				 * je�li tak, ustawiamy 'na styk', bo bedzie hu�tawka.
				 */
				if(Math.abs( pu.getFoodProduction(rt_, ucc) - finalO) < eps && ucc.getBirthRatePerHour() >= 0)
				{
					local = (CityFood) ucc.getCityStocks().getStockByType(rt_);
					this.freeHand(local, Storage.nastyk);
					//removePotNadm.add(rt_);
					niedomiaroweFabryki.addEdgeAndReversedEdge(rt_);
				}
			}
		}
		SNproductions = 0.0d;
		for(StockType rt : niedomiaroweFabryki)
		{
			SNproductions += pu.getFoodProduction(rt, ucc);
			//ucc.getMyFood().getFood(rt).setStorage(Storage.nie);
		}
		//potNadmiarowcy.removeAll(removePotNadm);
		ret.put("SOP", ucc.getCityStocks().getStoredFood().size());
		ret.put("SN", SNproductions);
		return ret;
	}
	
	public Map<String,Double> getArmyEatFactors(UsersCityConfig ucc)
	{
		
		WorldDefinition ch = SpringJSFUtil.getCivilizationHolder();
		UsersCityConfig config = ch.getCivilization(ucc.getUserConfig().getCivil());
		double A = 0.0d;
		double B = 0.0d; 
		
		for(CityBuilding cb : ucc.getBuildings())
		{
			ObjectBuildQueue obq = cb.getObjectBuildQueue();
			if(obq.getBuildsAmount() > 0)
			{
				QueueBuild first = obq.get(0);
				if(first instanceof UnitBuild)
				{
					int unitId = first.getObjectId();
					IUnitModel unitInQueue = config.getByIdInInternalMap(unitId,IUnitModel.class);
					/*
					 * abr - Birth Rate(w godzinach) jednostki ktora sie aktualnie buduje w kolejce ubq
					 */
					double abr = 3600000.0d / (double)unitInQueue.getBuildTime(ucc);
					/*
					 * double il - ile ludzi zabiera jednsotka np balista moze skladac sie z trzech ludzi
					 * wtedy il = 3
					 */
					double il = unitInQueue.getPrice(ucc.getUserConfig()).getStockByType(StockType.peopleCount).getVisibleAmount();
					/*
					 * A - jest to birthRate ludzi odchodzacych do wojska.
					 * Je�li mamy przyk�adowo dwie kolejki budowy w dw�ch budynkach
					 * w jednej ludzie szkola sie z predkoscia 5 szkolonych /h,
					 * a w innych barakach znowu ludzie szkola sie z predkoscia 4/h, predkosc szkolenia
					 * sie wszystkich ludzi to 9/h. Jest to tak jakby predkosc z jaka ludzie odchodza i staja sie armia. 
					 */
					A += il * abr;
					
					/*
					 * ae - army eat per hour. predkosc z jaka je jednostka unitInQueue.
					 */
					double ae = unitInQueue.getEatPerHour(ucc.getUserConfig());
					/*
					 * B - je�li B pomno�ymy razy t*t*0.5 to bedzie to liczba przedstawia ilosc zjedzonego 
					 * jedzenia przez powiekszajaca sie 
					 * armie we wszsytkich kolejkach budowy.
					 */
					B += ae * abr;
					//double timeDiff = TimeTranslation.msToHours(ucc.getDate().getTimeInMillis() - ubq.getStartTime().getTimeInMillis());
					//Z += timeDiff*timeDiff/2.0d;
				}
			}
		}
		A *= -1.0d;
		/*
		 * C okresla nam jedzenie zjadane przez armai znajdujaca sie w miescie.
		 * Dotyczy to tylko armi ktora sie nie buduje  tylko po prostu wczesniej byla juz miescie.
		 * armia ktora sie buduje jest liczona wyzej we wspolczynniku B.
		 */
		double C = 0.0d;
		for(CityUnitGroup ug : ucc.getMyArmy().getUnits())
		{
			IUnitModel unitInCity = config.getByIdInInternalMap( ug.getUnitId() , IUnitModel.class);
			double ea = unitInCity.getEatPerHour(ucc.getUserConfig());
			C += ea * (double)ug.getCount();
		}
		
		Map<String,Double> ret = new HashMap<String,Double>();
		ret.put("A", A);
		ret.put("B", B);
		ret.put("C", C);
		return ret;
		
	}
	
}
