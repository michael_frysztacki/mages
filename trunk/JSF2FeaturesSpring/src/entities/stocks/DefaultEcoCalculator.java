package entities.stocks;

import static helpers.TimeTranslation.nsToHours;
import economycomputation.EcoCalculator;
import economycomputation.ProductionUtil;
import entities.Storage;
import entities.UsersCityConfig;
import game_model_interfaces.IExtractBuildingModel;
import helpers.TimeTranslation;

public class DefaultEcoCalculator extends EcoCalculator {

	private CityStockSet cityStocks;
	public DefaultEcoCalculator(CityStockSet cityStocks) {
		this.cityStocks = cityStocks;
	}
	@Override
	public double getPeopleCount(long timeNs)  {
		double current = peopleAmount();
		current += TimeTranslation.nsToHours(timeNs) * birthRate();
		return current;
	}

	@Override
	public double getResourceCount(double hTime ,StockType rt)
	{
		UsersCityConfig ucc = this.getUcc();
		WorldDefinition ch = SpringJSFUtil.getCivilizationHolder();
		UsersCityConfig civil = ch.getCivilization(ucc.getUserConfig().getCivil());
		double current = ucc.getCityStocks().getStockByType(rt).getVisibleAmount();
		IExtractBuildingModel b = civil.getProducer(ucc.getUserConfig(), rt);
		if(b != null)
		current += b.getOutputPerHour(ucc)*hTime;
		return current;
	}
	
	@Override
	public Double getFood(StockType rt, double hTime)
	{
		UsersCityConfig ucc = this.getUcc();
		Double SN = ucc.getSN();
		Integer SOP = ucc.getSOP();
		double A = ucc.getA();
		double B = ucc.getB();
		double C = ucc.getC();
		
		WorldDefinition ch = SpringJSFUtil.getCivilizationHolder();
		ProductionUtil pu = SpringJSFUtil.getProductionUtil();
		CityFood cf = (CityFood)ucc.getCityStocks().getStockByType(rt);
		if(cf.getStorage() == Storage.nie)return cf.getVisibleAmount();
		if(cf.getStorage() == Storage.nastyk)return 0.0d;
		double suma = 0;
		UsersCityConfig config = ch.getCivilization(ucc.getUserConfig().getCivil());
		//CivilConfigBoostable ccb = config.getCivilConfig();
		suma = shortcut(hTime,
				cf.getVisibleAmount(),
				pu.getFoodProduction(rt, ucc),
				config.getEatPerHour(),
				ucc.getCityStocks().getPeopleStock().getVisibleAmount(),
				ucc.getTrainedPeople(),
				ucc.getBirthRatePerHour(),
				SN,
				SOP,
				A,
				B,
				C,
				rt
				);
		/*
		 * UWAGA - ten komenatrz wraz z 2 linijkami to odnosnik do komentarza (1*)
		 * nie kasuj tego przypadkowo. awiera wyjasnienie o wykorzystaniu metody shortcut uzytej powyzej.
		suma = ucc.getMyFood().getFood(rt).getCount() + hTime * getFoodProduction(rt, ucc);
		suma -= getOneEatenFood(ucc, SN, SOP, hTime);
		*/
		return suma;
	}
	/*
	 * KOMENTARZ 1*
	 * zastosowano skrot do obliczenia zywnosci po czasie
	 * Obliczanie zywnosci mam tutaj na mysli, sprawdzenie ile jedzenia bede mial po hTime czasie.
	 * brane jest pod uwage aktualna ilosc jedzenia, wyprodukowane jedzenie po czasie hTime oraz
	 * zjedzone jedzenie przez ludzi po czasie hTime. Brane sa rowniez pod uwage wspolczynnki SOP i SN.
	 * wczesniej byly uzywana funkcja:
	 * private double getOneEatenFood(UsersCityConfig ucc, double SN, double SOP, double hTime) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	 * u�ycie tej funkcji jest zakomentowane w funkcji:
	 * Double getFood(ResourceType rt, UsersCityConfig ucc, double hTime,Double SN, Integer SOP)
	 * i zamiast tego jest uzyte wlasnie shortcut.
	 * shortcut zapobiega mnozeniu dwoch duzych hTime przez siebie.
	 * Normalnie we wzorze:
	 * 
	 * DBV + t * oph - e ( people * t + (br/2) * t^2) - SN * t) / SOP
	 * czasie jest w jednym miescu podnoszony do kwadratu.
	 * I tak to bylo liczone wczesniej.
	 * Shortcut wyciaga t przed nawias tym samym nie mno�y wielkich liczb przez siebie i upraszna kalkulacje,
	 * przez co nie pojawiaja sie b�edy zaokraglen.
	 * 
	 */
	private double shortcut(double hTime,double dbv, double oph, double eph,double p, double sz, double br, double SN, Integer SOP,double A, double B, double C, StockType rt)
	{
		//double old = dbv + hTime*(oph - ( ((hTime*0.5d)*(eph*(br+A)+B) + eph*(p+sz) + C - SN )/ SOP));
		
		double K = eph*p + 0.5d*hTime*br*eph+C+ 0.5d*hTime*A*eph + sz*eph + 0.5d*hTime*B - SN;
		double new_ = dbv + hTime*(oph - ( K/(double)SOP ));
		
		return new_;
		
		
	}
	@Override
	public double getTaxCount(double hTime, StockType rt) {
		// TODO Auto-generated method stub
		double br = this.getUcc().getBirthRatePerHour();
		double pc = this.getUcc().getCityStocks().getPeopleStock().getVisibleAmount();
		double count = this.getUcc().getCityStocks().getStockByType(rt).getVisibleAmount();
		count += this.getUcc().getTaxSatisfaction()*(br*hTime/2.0d + pc)*hTime;
		return count;
	}
	@Override
	public double getFood(String foodId, long nsTime) {
		double debug = (foodEatenByPeople(nsTime) - SPIF()*nsToHours(nsTime));
		return startAmount(foodId) + factoryProduction(nsTime, foodId) - (foodEatenByPeople(nsTime) - SPIF()*nsToHours(nsTime)) / sumOfProviders();
	}
		private double startAmount(String foodId) {
			return cityStocks.getCityFood(foodId).getAmount();
		}
		private double factoryProduction(long nsTime, String stockId) {
			
			if(factoryExistForGivenStock(stockId))
				return getFactory(stockId).getOutputPerHour(cityStocks.getCity())*nsToHours(nsTime);
			return 0;
			
		}
		private IExtractBuildingModel getFactory(String stockId) {
			return cityStocks.getWorldDefinition().getProducer(stockId);
		}
		private boolean factoryExistForGivenStock(String stockId) {
			return cityStocks.getWorldDefinition().getProducer(stockId) != null;
		}
		private int sumOfProviders() {
			return cityStocks.getSOP();
		}
		/*
		 * sum Of Productions Of Insufficient Factories
		 */
		private int SPIF() {
			return cityStocks.getSN();
		}
	@Override
	public double getResourceCount(long timeNs, String resourceId) {
		
		return cityStocks.getCityResource(resourceId).getAmount() + factoryProduction(timeNs, resourceId);
	}
	@Override
	public double getTaxCount(double hTime, String taxId) {
		// TODO Auto-generated method stub
		return 0;
	}
	/*
	 * in case when birth rate < 0, 'left' and 'right' are the non-integer areas inside the boundaries.
	 * in case when birth rate > 0, 'left' and 'right' are the non-integer areas outside the boundaries.
	 * boundary is the range between the start time and the end time of calculating food eaten by people.
	 * when birthrate < 0, then 'mid' is the integer middle area plus lacking steps at both sides.
	 * when birthrate > 0, then 'mid' is the integer middle area.   
	 */
	double foodEatenByPeople(long nsTime) {
		
		if(birthRate()==0) 
			return floor(peopleAmount()) * nsToHours(nsTime) * eatPerHour();
		double mid = meanValueOfSteps(nsTime) * numberOfSteps(nsTime) * invBirthRate();
		double left = (peopleAmount() - floor(peopleAmount())) * invBirthRate() * floor(peopleAmount());
		double right = (roof(peopleAmount(nsTime)) - peopleAmount(nsTime)) * invBirthRate() * floor(peopleAmount(nsTime));
		if(birthRate()<0)
			return eatPerHour()*(left + mid + right);
		else
			return eatPerHour()*(mid - right - left);
	}
		private int roof(double value) {
			return floor(value) + 1;
		}
		private int numberOfSteps(long nsTime) {
			return Math.abs(startFloor()-endFloor(nsTime)) + (birthRate()<0?-1:1);
		}
		
		private double meanValueOfSteps(long nsTime) {
			return (startFloor() + endFloor(nsTime))/2.0d;
		}
		
		private int startFloor() {
			return floor(peopleAmount());
		}
		
		private int endFloor(long nsTime) {
			return floor(peopleAmount(nsTime));
		}
		
		private int eatPerHour() {
			return cityStocks.getWorldDefinition().getEatPerHour();
		}
		private double invBirthRate() {
			return 1.0d/Math.abs(birthRate());
		}
		private int floor(double value) {
			return (int) Math.floor(value);
		}
		private double peopleAmount(long nsTime) {
			return peopleAmount() + nsToHours(nsTime)*birthRate();
		}
		private int birthRate() {
			return cityStocks.getPeopleStock().getBirthRatePerHour();
		}
		private double peopleAmount() {
			return cityStocks.getPeopleStock().getAmount();
		}

}
