package entities.stocks;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import economycomputation.EcoCalculator;
import economycomputation.RecursionCalculator;
import economycomputation.recursion_events.RecursionEvent;
import entities.UsersCityConfig;
import game_model_interfaces.IWorldDefinition;
import helpers.CollectionUtil;
import helpers.TimeTranslation;

@Loot
@TransportStock
public abstract class CityFood extends CityStock {
	
	private static final long counterInactiveState = -1; 
	private static final double lackOfFoodState = -1.0d;
	private static final double availabilityBoundary = 1.0d;
	private static final double startFoodAmount = 0.0d;
	protected abstract long getFoodRaiseTimeNs_();
	protected abstract void setFoodRaiseTimeNs_(long raiseTimeNs);
	public void init(String foodId, CityStockSet cityStocks, int startAmount) {
		super.init(foodId, cityStocks);
		setFoodRaiseTimeNs_(counterInactiveState);
		if(startAmount==0)
			setAmount(lackOfFoodState);
		else
			setAmount(startAmount);
		
	}
	public void init(String foodId, CityStockSet cityStocks) {
		super.init(foodId, cityStocks);
		setFoodRaiseTimeNs_(counterInactiveState);
		setAmount(lackOfFoodState);
	}
	
	void setToAvailableState() {
		setAmount(startFoodAmount);
		setFoodRaiseTimeNs_(getCityStocks().getCityTimeNs_());
	}
	void setToUnavailableState() {
		setAmount(lackOfFoodState);
	}
	UsersCityConfig getCity() {
		return getCityStocks().getCity();
	}
	/*
		private void determineFoodState(CityFoodInitializer init) {
			if(init.getStorage(this) == Storage.tak)
				setAmount(startFoodAmount);
			else
				setAmount(lackOfFoodState);
		}
		*/
	public void addFood(int amount) {
		if(isLackOfFoodState()){
			setFoodRaiseTimeNs_(CityFood.this.getCityStocks().getCityTimeNs_());
			setAmount(startFoodAmount);
		}
		setAmount(getAmount() + amount);
	}
	/*
	@Override
	public boolean isAvailable() {
		return getAmount() >= availabilityBoundary;
	}
	*/
	@Override
	public int getVisibleAmount() {
		if(isLackOfFoodState())
			return 0;
		if(getAmount() < availabilityBoundary)
			return 1;
		return (int) getAmount();
	}
		private boolean isLackOfFoodState(){
			return getAmount() == lackOfFoodState;
		}
	@Override
	public void process(EcoCalculator ecoCalc, long nsTime) {
		if(!isLackOfFoodState())
			setAmount(ecoCalc.getFood(getStockId(), nsTime));
	}

	public int getSatisfaction() {
		if(isLackOfFoodState() || isCooldownCounterActive())
			return 0;
		return getWorldDefinition().getSatisfaction(getStockId(), getPlayersCivilId());
	}
		private boolean isCooldownCounterActive() {
			return getFoodRaiseTimeNs_() != counterInactiveState;
		}
		IWorldDefinition getWorldDefinition() {
			return getCityStocks().getWorldDefinition();
		}
		private String getPlayersCivilId() {
			return getCityStocks().getPlayersCivilId();	
		}
	/*
	 * returns time in nano seconds.
	 */
	public Long getCooldownRemainingTime() {
		return new SatisfactionAfterCooldownRecursionEvent().recursionTimeNs();
	}

	public RecursionEvent getNextRecursionEvent(RecursionCalculator recCalc, EcoCalculator ecoCalc) {
		if(isLackOfFoodState())
			return getFoodRaiseTimeRecursionEvent(recCalc);
		else
			return new ShortestRecursionEventEvaluator(recCalc, ecoCalc).evaluate();
	}
		private class ShortestRecursionEventEvaluator {
			private List<RecursionEvent> list = new ArrayList<RecursionEvent>();
			private RecursionCalculator recCalc;
			private EcoCalculator ecoCalc;
			public ShortestRecursionEventEvaluator(RecursionCalculator recCalc, EcoCalculator ecoCalc) {
				this.recCalc = recCalc;
				this.ecoCalc = ecoCalc;
			}
			public RecursionEvent evaluate() {
				list.add(getFoodFallTimeRecursionEvent(recCalc));
				list.add(getSatisfactionRecursionEvent(ecoCalc));
				List<RecursionEvent> result = CollectionUtil.findMaxMinObjects(list, new ShortestREComparator(), false);
				if(result.size() != 0)
					return result.get(0);
				return null;
			}
			private class ShortestREComparator implements Comparator<RecursionEvent> 
			{
				@Override
				public int compare(RecursionEvent o1, RecursionEvent o2) {
					long result = o1.recursionTimeNs() - o2.recursionTimeNs();
					if(result == 0) return 0;
					return result < 0 ? -1 : 1;
				}
			}
		}
		private RecursionEvent getFoodRaiseTimeRecursionEvent(RecursionCalculator recCalc) {
			if(recCalc.getFoodRaiseTime(this) != null)
				return new FoodRaiseRecursionEvent(recCalc);
			return null;
		}
		private RecursionEvent getFoodFallTimeRecursionEvent(RecursionCalculator recCalc) {
			if(recCalc.getFoodFallTime(this) != null)
				return new FoodFallRecursionEvent(recCalc);
			return null;
		}
		private RecursionEvent getSatisfactionRecursionEvent(EcoCalculator ecoCalc) {
			if(isCooldownCounterActive())
				return new SatisfactionAfterCooldownRecursionEvent(ecoCalc);
			return null;
		}
	public class FoodFallRecursionEvent extends RecursionEvent {
		private RecursionCalculator recCalc;
		public FoodFallRecursionEvent(RecursionCalculator recCalc) {
			this.recCalc = recCalc;
		}
		
		@Override
		public Long recursionTimeNs() {
			return TimeTranslation.hoursToNs(recCalc.getFoodFallTime(CityFood.this));
		}

		/*
		 * method invocation requirement - this method should be invoked before city time is increased
		 */
		@Override
		public void process() {
			CityFood.this.setAmount(lackOfFoodState);
			setFoodRaiseTimeNs_(counterInactiveState);
		}
		
	}
	
	public class FoodRaiseRecursionEvent extends RecursionEvent {

		private RecursionCalculator recCalc;
		public FoodRaiseRecursionEvent(RecursionCalculator recCalc) {
			this.recCalc = recCalc;
		}
		
		@Override
		public Long recursionTimeNs() {
			return TimeTranslation.hoursToNs(recCalc.getFoodRaiseTime(CityFood.this));
		}

		@Override
		public void process() {	
			setAmount(startFoodAmount);
			setFoodRaiseTimeNs_(getCityStocks().getCityTimeNs_() + recursionTimeNs());
		}
		
	}
	
	public class SatisfactionAfterCooldownRecursionEvent extends RecursionEvent {

		private EcoCalculator ecoCalc;
		private SatisfactionAfterCooldownRecursionEvent(){

		}
		public SatisfactionAfterCooldownRecursionEvent(EcoCalculator ecoCalc) {
			this.ecoCalc = ecoCalc;
		}
		
		@Override
		public Long recursionTimeNs() {			
			if(isCooldownCounterActive())
				return TimeTranslation.secToNs(getCityStocks().getWorldDefinition().satisfactionCooldownSec()) - (getCityStocks().getCityTimeNs_() - getFoodRaiseTimeNs_());
			return null;
		}

		@Override
		public void process() {
			setAmount(ecoCalc.getFood(CityFood.this.getStockId(), recursionTimeNs()));
			setFoodRaiseTimeNs_(counterInactiveState);
		}
	}

}
