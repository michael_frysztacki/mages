package entities.stocks;

import game_model_interfaces.PriceStock;

public class ForeignTaxPriceStock extends PriceStock{

	public ForeignTaxPriceStock(int count) {
		super(null, count);
	}
	
	private ForeignTaxPriceStock(ForeignTaxPriceStock foreignTax)
	{
		this(foreignTax.getVisibleAmount());
	}
	@Override
	public PriceStock clone()
	{
		return new ForeignTaxPriceStock(this);
	}

}
