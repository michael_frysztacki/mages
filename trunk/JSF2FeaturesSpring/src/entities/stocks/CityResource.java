package entities.stocks;

import helpers.TimeTranslation;
import economycomputation.EcoCalculator;

@Loot
@TransportStock
public abstract class CityResource extends CityStock
{
	public void init(String stockId, CityStockSet cityStocks, int startAmount) {
		super.init(stockId, cityStocks, startAmount);
	}

	@Override
	public int getVisibleAmount() {
		return (int) getAmount();
	}
	/*
	@Override
	public int compare(CityResource arg0, CityResource arg1) {
	      return (arg0.getVisibleAmount()<arg1.getVisibleAmount() ? -1 : (arg0.getVisibleAmount()==arg1.getVisibleAmount() ? 0 : 1));
	}
	*/
	public void process(EcoCalculator ecoCalc, long nsTime) {
		setAmount(ecoCalc.getResourceCount(nsTime, getStockId()));		
	}
}
