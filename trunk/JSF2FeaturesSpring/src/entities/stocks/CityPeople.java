package entities.stocks;

import java.util.List;

import helpers.TimeTranslation;
import economycomputation.EcoCalculator;
import economycomputation.RecursionCalculator;
import economycomputation.recursion_events.RecursionEvent;
import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;

/*
 * metoda getCount na klasie CityPeople zwraca ilo�� wolnych ludzi + pracuj�cych w kopalniach.
 */
@Loot
public abstract class CityPeople extends CityStock {

	public void init(CityStockSet cityStocks, int startAmount) {
		super.init("", cityStocks, startAmount);
	}
	
	@Override
	public int getVisibleAmount() {
		 
		return (int)getAmount() - getHiredPeople();
	}
	
	@Override
	public String getStockId() {
		return getCityStocks().getPeopleStockId();
	}
	@Override
	protected void setStockId(String stockId) {
		return;
	}

	/*
	 * zwraca ile jedzenie spozywaja sami ludzie, bez armi, bez niewolnik�w
	 */
	public double getPeopleEatPerHour() {
		return getCityStocks().getWorldDefinition().getEatPerHour() * getAmount();
	}
	
	public int getHiredPeople() {
		int hired = 0;
		for(ExtractCityBuilding ecb : getExtractAndConvertBuildings())
			hired += ecb.getPopulation();
		return hired;
	}

		private List<ExtractCityBuilding> getExtractAndConvertBuildings() {
			return getCityStocks().getCity().getCityBuildingsByClass(ExtractCityBuilding.class,true);
		}
	
	@Override
	public void process(EcoCalculator ecoCalc, long nsTime) {
		
		setAmount(ecoCalc.getPeopleCount(nsTime));
	}

	public RecursionEvent getNextRecursionEvent(RecursionCalculator recCalc, UsersCityConfig ucc) {
		RecursionEvent re = new PeopleLimitRecursionEvent(recCalc, ucc);
		if(re.recursionTimeNs() == null)
			return null;
		return re;
	}
	
	public class PeopleLimitRecursionEvent extends RecursionEvent {

		private UsersCityConfig ucc;
		private Long recTime = null;
		public PeopleLimitRecursionEvent(RecursionCalculator recCalc, UsersCityConfig ucc) {
			this.ucc = ucc;
			if(recCalc.timeWhenPeopleArriveLimit(ucc) != null)
				this.recTime = TimeTranslation.hoursToNs(recCalc.timeWhenPeopleArriveLimit(ucc));
		}
		
		@Override
		public Long recursionTimeNs() {
			return this.recTime;
		}

		@Override
		public void process() {
			setAmount(getCityStocks().getMyCivil().getHousesModel().getPeopleLimit(ucc));
		}
	}
	
	public int getVisibleBirthRate() {
		
		int birthRate = getBirthRatePerHour();
		if(getAmount() < 1.0d && birthRate < 0)
			return 0;
		return birthRate;
	}

	int getBirthRatePerHour() {
		
		return getSatisfaction() * birthRateMultiplier();
	}
	
	public int getSatisfaction() {
			
			int satisfaction=0;
			satisfaction += satisfactionFromTaxes();
			satisfaction += satisfactionFromObjectsInCity();
			for(CityFood cf : getCityStocks().getFood())
				satisfaction += cf.getSatisfaction();
			return satisfaction;
		}
			private int satisfactionFromTaxes() {
				return getCityStocks().getCity().getTaxSatisfaction();
			}
			private int satisfactionFromObjectsInCity() {
				return getCityStocks().getMyCivil().getCityModel().getSatisfactionFromObjects(getCityStocks().getCity());
			}
		private int birthRateMultiplier() {
			return getCityStocks().getWorldDefinition().getBirthMultiplier();
		}
		
}
