package entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import entities.units.CityArmy;
import entities.units.ReportArmy;


public class FightRound {

	private int id;
	private FightReport fightReport;
	private ReportArmy aggressor;
	private ReportArmy deffender;
	
	private FightRound(){};
	public FightRound(FightRound fr)
	{
		this.id = fr.getId();
		this.aggressor = new ReportArmy(fr.getAggressor());
		this.deffender = new ReportArmy(fr.getDeffender());
	}
	public FightRound(ReportArmy aggressor,ReportArmy deffender)
	{
		this.aggressor = aggressor;
		this.deffender = deffender;
	}
	public FightReport getFightReport() {
		return fightReport;
	}
	public void setFightReport(FightReport fightReport) {
		this.fightReport = fightReport;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ReportArmy getAggressor() {
		return aggressor;
	}
	void setAggressor(ReportArmy aggressor) {
		this.aggressor = aggressor;
	}
	public ReportArmy getDeffender() {
		return deffender;
	}
	void setDeffender(ReportArmy deffender) {
		this.deffender = deffender;
	}
	
}
