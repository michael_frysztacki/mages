package entities;

import entities.stocks.CityStockSet;
import entities.stocks.TripStockSet;
import entities.units.CityArmy;

public class CityReport extends Report{

	private TripStockSet resources;
	private CityStockSet food;
	private CityArmy army;
	
	public CityStockSet getFood() {
		return food;
	}
	public void setFood(CityStockSet food) {
		this.food = food;
	}
	public CityArmy getArmy() {
		return army;
	}
	public void setArmy(CityArmy army) {
		this.army = army;
	}
	
	@Override
	public TripStockSet getTripStockSet() {
		// TODO Auto-generated method stub
		return resources;
	}
	
}
