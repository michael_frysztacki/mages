package entities;

import entities.buildings.CityBuilding;
import entities.buildings.ResearchBuild;
import game_model_impl.PriceStockSetImpl;
import game_model_interfaces.IResearchModel;

import java.util.Iterator;

public class UserResearch{

	private int id;
	private int level;
	private int researchId;
	private UserConfig userConfig;
	public int getLevel(){
		return level;
	}
	public void setLevel(int level){
		this.level = level;
	}
	public void incrementLevel(){
		level++;
	}
	public int getId(){
		return id;
	}
	public int getResearchId(){
		return this.researchId;
	}
	
	public UserConfig getUserConfig() {
		return userConfig;
	}
	void setUserConfig(UserConfig userConfig) {
		this.userConfig = userConfig;
	}
	public UserResearch(int researchId, int level, UserConfig uc)
	{
		this.setUserConfig(uc);
		this.level = level;
		this.researchId = researchId;
	}
	@Override
	public String toString()
	{
		return "id: " + Integer.toString(this.getResearchId());
	}
	/*
	 * je�li badanie jest dodane do kolejki w kt�rymkolwiek mie�cie, nie mozna badac badania.
	 */
	public boolean canUpgrade_Time(UsersCityConfig where) {
		// TODO Auto-generated method stub
		UsersCityConfig myCivil = this.getUserConfig().getMyCivilization();
		IResearchModel researchM = myCivil.getById(this.getId(), IResearchModel.class);
		ResearchBuild temp = new ResearchBuild();
		temp.setObjectId(this.getResearchId());
//		if(this.getUserConfig().getCapital().getBuilding(thisR.getMotherBuilding()).getObjectBuildQueue().contains(temp))
//		{
//			return false;
//		}
		
		/*
		 * sprawdzamy czy gdzies na koloni nie buduje sie juz to badanie.
		 */
		Iterator it = this.getUserConfig().getUsersCityConfigs().iterator();
		while(it.hasNext()){
			UsersCityConfig colony = (UsersCityConfig)it.next();
			if(colony.getBuilding(CityBuilding.class, researchM.getRequirementNode().getMotherBuilding().getId()).getObjectBuildQueue().contains(temp))
				return false;
		}
		return true;
	}

	public void upgrade(UsersCityConfig where) throws PayException{
			// TODO Auto-generated method stub
			//if(!canUpgrade_Time())return false;
			if(!this.canUpgrade_Time(where))return false;
			UsersCityConfig myCivil = this.getUserConfig().getMyCivilization();
			IResearchModel thisR = myCivil.getById(this.getResearchId(), IResearchModel.class);
			if( !UpgradeUtil.canUpgrade_Resources(where, thisR) )return false;
			if(! thisR.getRequirementNode().canBuild(where) )return false;
			PriceStockSetImpl price = thisR.getPrice(where);
			price.doPay(where.getCityStocks());
			//where.getMyResources().subNotFoodSet(price);
			//GregorianCalendar startTime = new GregorianCalendar();
			//startTime.setTimeInMillis(where.getDate().getTimeInMillis());
			//List buildingBuilds = ucc.getBuildingBuilds();
			ResearchBuild rb = new ResearchBuild(this.getResearchId() , this.getLevel()+1);
			//ResearchBuild upgrade = new ResearchBuild(this.getObjectId(),this.getLevel()+1,startTime, where.getUserConfig());
			rb.assignToProperQueue(where);
//			int motherBuildingId = thisR.getMotherBuilding();
//			where.getBuilding(motherBuildingId).getBuilds().add(upgrade);
			return true;
			//return this.addResearchBuild(upgrade);
	}
	
//	private boolean addResearchBuild(ResearchBuild rBuild)
//	{
//		List bbList = Arrays.asList(this.getUserConfig().getResearchBuilds().toArray());
//		int idx = bbList.indexOf(rBuild);
//		if(idx == -1)
//		{
//			this.getUserConfig().getResearchBuilds().add(rBuild);//  .getBuildingBuilds().add((BuildingBuild) buildingBuild);
//			rBuild.setUserConfig(this.getUserConfig());
//			//buildingBuild.setUcc(this.getUcc());
//			return true;
//		}
//		else return false;
//	}

	
}
