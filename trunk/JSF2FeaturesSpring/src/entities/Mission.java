package entities;

public enum Mission {

	robbery,
	siege,
	transport,
	station,
	scout,
	spy
}
