package entities;

import java.util.List;

import game_model_impl.WorldDefinition;
import game_model_interfaces.ICivilization;

import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.units.CityArmy;
import entities.units.TripArmy;
import game_model_interfaces.IBuildingModel;

public class Colony extends UsersCityConfig{

	public static class TakeOverExtractBuildingRandomizator{
		
		public List<ExtractCityBuilding> choseExtractBuildingsToTakeOver(List<ExtractCityBuilding> allBuildings, WorldDefinition wd){
			return null;
		}
	}
	public Colony(UserConfig owner, Point point, long nsCityTime)
	{
		super(owner, point, nsCityTime);
	}
	private Civil previousCivil=null;
	public void takeOverColony(TripArmy winner, TakeOverExtractBuildingRandomizator randomizator)
	{
		this.previousCivil = this.getUserConfig().getCivil();
	//	Civilization victimsCivil = this.getUserConfig().getMyCivilization();
		this.setUserConfig(winner.getOwner());
		List<ExtractCityBuilding> previousEb = this.getCityBuildingsByClass(ExtractCityBuilding.class, true);
		List<ExtractCityBuilding> takenBuildings = randomizator.choseExtractBuildingsToTakeOver(previousEb, winner.getOwner().getMyWorldDefinition());
		for(ExtractCityBuilding ecb : takenBuildings){
			ecb.setPeople(0);
			ecb.resetObjectBuildQueue();
		}
		this.getBuildings().clear();
		for(ExtractCityBuilding ecb : takenBuildings){
			this.getBuildings().add(ecb);
		}
		ICivilization winnersCivil = this.getUserConfig().getMyCivilization();
		for(IBuildingModel bm : winnersCivil.getGameModelList(IBuildingModel.class, true, true)){
			if(!this.getBuildings().contains(CityBuilding.CityBuildingFactory.createCityBuilding(bm, this))){
				this.getBuildings().addEdgeAndReversedEdge(CityBuilding.CityBuildingFactory.createCityBuilding(bm, this));
			}
		}
		
		/*
		
		Iterator<ExtractCityBuilding> it = previousEb.iterator();
		while(it.hasNext()){
			ExtractCityBuilding eb = it.next();
			if(!takenBuildings.contains(eb) && winnersCivil.getById(eb.getBuildingId(), IBuildingModel.class)==null ){
					it.remove();
			}
		}
		List<Integer> ids = new ArrayList<Integer>();
		for( CityBuilding eb : allPreviousB ){
			ids.add(eb.getBuildingId());
		}
		for(IBuildingModel bm : winnersCivil.getGameModelList(IBuildingModel.class, true, true)){
			if(! ids.contains(bm.getId() ) ){
				this.getBuildings().add(CityBuilding.CityBuildingFactory.createCityBuilding(bm, this));
			}
		}
		*/
		this.setMyArmy(new CityArmy(this));
		winner.putTripArmy(this);
	}
	public Civil getPreviousCivil()
	{
		return this.previousCivil;
	}

}
