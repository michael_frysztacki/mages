package entities.buildings;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import helpers.TimeTranslation;

import org.powermock.api.mockito.PowerMockito;


public class TestingUtils {

	
	public static void mock_ObjectBuildQueue_getIdx(ObjectBuildQueue obq, QueueBuild... consecutiveQueueBuilds){
		for( int idx=0 ; idx < consecutiveQueueBuilds.length ; idx++)
			when(obq.get(idx)).thenReturn( consecutiveQueueBuilds[idx] );	
	}
	public static long msToNs(int ms){
		return TimeTranslation.msToNs(ms);
	}
	public static void assertEqualTimes(int expectedMs, long expectedNs) {
		assertEquals( msToNs( expectedMs) , expectedNs);
	}
	
	public static void mock_ObjectBuildQueue_getCityTime(ObjectBuildQueue obq, int cityTimeMs) {
		when ( obq.getCityTime_()).thenReturn( msToNs(cityTimeMs) );
	}
	public static void mock_ObjectBuildQueue_getCityTime(ObjectBuildQueue obq, long cityTimeNs) {
		when ( obq.getCityTime_()).thenReturn( cityTimeNs );
	}

	public static void incrementCityTime(ObjectBuildQueue obq, int timeToAddMs) {
		mock_ObjectBuildQueue_getCityTime(obq, obq.getCityTime_() + msToNs( timeToAddMs) );
	}

	public static int fractionOfBuildTimeInMs(QueueBuild ub, int numerator, int denominator) throws Exception {
		int fraction = nsToMs(ub.getBuildTime())  * numerator / denominator;
		int fractionAddition = nsToMs(ub.getBuildTime()) * (denominator - numerator) / denominator;
		if(fraction + fractionAddition != nsToMs(ub.getBuildTime())){
			throw new Exception("QueueBuild can not be devided by fraction, lack of precision");
		}
		return (int) ((ub.getBuildTime()/1000000)  * numerator / denominator);
	}
	
	public static int nsToMs(long msTime) {
		return (int) (msTime / 1000000l);
	}
	
	public static void processQueueBuild(QueueBuild qb, int processTimeMs) {
		qb.processBuild( msToNs(processTimeMs) );
	}
	
	public static void mock_QueueBuild_getMyObjectsBuildTimeMs(QueueBuild ub, int buildTime) throws Exception {
		PowerMockito.doReturn(buildTime).when( ub , "getMyObjectsBuildTimeMs");
		//Mockito.when(ub.getMyObjectsBuildTimeMs()).thenReturn(buildTime);
		//Mockito.doReturn(buildTime).when(ub).getMyObjectsBuildTimeMs();
	}

}
