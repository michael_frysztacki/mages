package entities.buildings;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static entities.buildings.TestingUtils.assertEqualTimes;
import static entities.buildings.TestingUtils.msToNs;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import entities.EntityFactory;
import entities.PayException;
import entities.UsersCityConfig;
import entities.buildings.BuildingBuild.BuildingCurrentlyUpgradingException;
import entities.buildings.CityBuilding.QueueOfBuildIsActiveException;
import entities.stocks.CityFood;
import entities.stocks.CityPeople;
import entities.stocks.CityResource;
import entities.stocks.CityStockSet;
import entities.stocks.TaxStock;
import entities.units.CityArmy;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IRequirementNode;
import game_model_interfaces.IRequirementNode.RequirementNotFullfilledException;
import game_model_interfaces.IUnitModel;
import game_model_interfaces.PriceStockSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;
@RunWith(Enclosed.class)
public class CityBuildingTests{
	
	protected CityBuilding cb;
	protected final int buildingsId = 10;
	
	@Mock
	ICivilization civil;
	
	@Mock
	PriceStockSet buildingPrice;
	
	@Mock
	UsersCityConfig myCity;
	final int baseCityTimeMs = 10;
	
	@Mock
	CityStockSet cityStocks;
	
	@Mock
	BuildingBuild buildingBuild;
	
	@Mock
	ObjectBuildQueue objectBuildQueue;
	
	@Mock
	IBuildingModel buildingModel;
	final int wholeBuildTimeNs = 50;
	
	@Mock
	IRequirementNode reqNodeOfBuildingModel;
	
	@Before
	public void setUp() {
		
		cb = new CityBuildingStub();
		cb.init(myCity, new EntityFactoryForTests(), buildingsId);
		when( myCity.getMyCivilization() ).thenReturn(civil);
		when( civil.getById( buildingsId, IBuildingModel.class)).thenReturn(buildingModel);
		when( buildingModel.getPrice(myCity) ).thenReturn(buildingPrice);
		when( myCity.getCityTimeNs_()).thenReturn( msToNs(baseCityTimeMs) );
		when( buildingModel.getRequirementNode() ).thenReturn(reqNodeOfBuildingModel);
		when( myCity.getCityStocks()).thenReturn(cityStocks);		
	}
	
	@After
	public void tearDown() {
		
	}

	@RunWith( MockitoJUnitRunner.class )
	public static class NewlyCreatedBuilding extends CityBuildingTests {
		
		@Test
		public void rightAfterConstruction() throws Exception {
			
			assertFalse(cb.isCurrentlyUpgrading());
			assertEquals(buildingsId, cb.getBuildingId());
			assertEquals(myCity, cb.getCity());
			assertEqualTimes(baseCityTimeMs, cb.getCityTime_());
			assertEquals(0, cb.getLevel());
			assertEquals(civil, cb.getPlayersCivilization());
		}

	}
	
	@RunWith( MockitoJUnitRunner.class )
	public static class RecursionEventsTests extends CityBuildingTests {
		
		@Ignore
		@Test
		public void OneUnitBuildRightAfterAddingUnitBuild() throws Exception {
			
			
		}

	}
	
	@RunWith( MockitoJUnitRunner.class )
	public static class SuccessfulUpgrade extends CityBuildingTests {
	
		@Before
		public void setUp_SuccessfulUpgrade() throws PayException, BuildingCurrentlyUpgradingException, IRequirementNode.RequirementNotFullfilledException, QueueOfBuildIsActiveException {
			
			mockForBuilding_reqNode_canBuild(true);
			mockForBuilding_cityStocks_canPay(true);
			mockForBuilding_objectBuildQueue_isActive(false);
			mock_isBuildingBuildActive(false);
		}

		@After
		public void tearDown_SuccessfulUpgrade() throws PayException {
			
			verify_buildingBuild_startUpgradeInvokedOnce();
			verify_cityStocks_doPayInvokedOnce( buildingPrice );
		}
		
		@Test
		public void simpleUpgrade() throws PayException, BuildingCurrentlyUpgradingException, IRequirementNode.RequirementNotFullfilledException, QueueOfBuildIsActiveException {
			
			cb.upgrade();
		}
		
		@Test
		public void upgradeAndProcessBuilding() throws PayException, BuildingCurrentlyUpgradingException, IRequirementNode.RequirementNotFullfilledException, QueueOfBuildIsActiveException {
			
			cb.upgrade();
			mock_isBuildingBuildActive(true);
			cb.process(wholeBuildTimeNs);
			
			verify_buildingBuild_process(wholeBuildTimeNs);
		}
		
		@Test
		public void upgradeAndProcessHalf() throws PayException, BuildingCurrentlyUpgradingException, IRequirementNode.RequirementNotFullfilledException, QueueOfBuildIsActiveException {

			cb.upgrade();
			mock_isBuildingBuildActive(true);
			cb.process( wholeBuildTimeNs / 2);
			
			verify_buildingBuild_process( wholeBuildTimeNs / 2);
		}
		
		@Test
		public void upgradeAndProcessOvertime() throws PayException, BuildingCurrentlyUpgradingException, IRequirementNode.RequirementNotFullfilledException, QueueOfBuildIsActiveException {

			cb.upgrade();
			mock_isBuildingBuildActive(true);
			int overflowTimeMs = 30;
			cb.process( wholeBuildTimeNs + overflowTimeMs );
			
			verify_buildingBuild_process( wholeBuildTimeNs + overflowTimeMs );
		}
	}
	
	@RunWith( MockitoJUnitRunner.class )
	public static class UnsuccessfulUpgrade extends CityBuildingTests {
		
		@Before
		public void setUp_UnsuccessfulUpgrade() {
			
		}

		@After
		public void tearDown_UnsuccessfulUpgrade() throws PayException {
			
			verify_cityStocks_doPayNotInvoked();
			verify_buildingBuild_startUpgradeNotInvoked();
		}
		
		@Test
		public void requirementsNotSatisfied() throws PayException, BuildingCurrentlyUpgradingException, IRequirementNode.RequirementNotFullfilledException, QueueOfBuildIsActiveException {
			
			mockForBuilding_reqNode_canBuild(false);
			mockForBuilding_cityStocks_canPay(true);
			
			catchException( cb ).upgrade();
			
			assertThat(caughtException()).isInstanceOf( IRequirementNode.RequirementNotFullfilledException.class);
		}
		
		@Test
		public void upgradeBuildingRightAfterBuildingUnitsStarted() throws Exception {
			
			mockForBuilding_objectBuildQueue_isActive(true);
			
			catchException(cb).upgrade();
			
			assertThat(caughtException()).isInstanceOf( QueueOfBuildIsActiveException.class);
		}
		
		@Test
		public void notEnoughStocksInCity() throws Exception {
			
			mockForBuilding_reqNode_canBuild(true);
			mockForBuilding_cityStocks_canPay(false);
			
			catchException( cb ).upgrade();
			
			assertThat(caughtException()).isInstanceOf( PayException.class );
		}
		
		@Test
		public void buildingCurrentlyUpgrading() throws PayException, BuildingCurrentlyUpgradingException, IRequirementNode.RequirementNotFullfilledException, QueueOfBuildIsActiveException  {
			
			mockForBuilding_reqNode_canBuild(true);
			mockForBuilding_cityStocks_canPay(true);
			
			mock_isBuildingBuildActive(true); // mock upgrading state in building
			catchException( cb ).upgrade(); // second upgrade
			
			assertThat(caughtException()).isInstanceOf( BuildingCurrentlyUpgradingException.class);
		}
		
		@Test
		public void processNotUpgradingBuilding() throws Exception {
			
			cb.process(wholeBuildTimeNs);
			
			verify_buildingBuild_processNotInvoked();
			verify_objectBuildQueue_processNotInvoked();
		}

	}
	
	@RunWith( MockitoJUnitRunner.class )
	public static class BuildingUnitsTests extends CityBuildingTests {
		
		@Mock
		IUnitModel unitToBuild;
		final int unitId = 14;
		final int unitBuildTimeMs = 100;
		@Mock
		PriceStockSet unitsPrice;
		@Mock
		PriceStockSet unitsPriceMultipliedByZero;
		@Mock
		PriceStockSet unitsPriceMultipliedByOne;
		@Mock
		PriceStockSet unitsPriceMultipliedByTwo;
		@Mock
		PriceStockSet unitsPriceMultipliedByThree;
		@Mock
		IRequirementNode unitsNode;
		
		@Before
		public void BuildingUnitsSetup() {
			
			when( civil.getById(unitId, IUnitModel.class)).thenReturn(unitToBuild);
			when( unitToBuild.getPrice(myCity)).thenReturn(unitsPrice);
			when(unitToBuild.getRequirementNode()).thenReturn(unitsNode);
			when(unitsNode.canBuild(Mockito.any(UsersCityConfig.class))).thenReturn(true);
			
			when( unitsPrice.cloneAsMultiplied(0)).thenReturn( unitsPriceMultipliedByZero );
			when( unitsPrice.cloneAsMultiplied(1)).thenReturn( unitsPriceMultipliedByOne );
			when( unitsPrice.cloneAsMultiplied(2)).thenReturn( unitsPriceMultipliedByTwo );
			when( unitsPrice.cloneAsMultiplied(3)).thenReturn( unitsPriceMultipliedByThree );
		}
		
		@Test
		public void buildUnitsIfBuildingUpgrading_shouldFail() throws Exception {
			
			mock_isBuildingBuildActive(true);
			catchException( cb ).buildUnit(unitId, 2); 
			
			assertThat(caughtException()).isInstanceOf( BuildingCurrentlyUpgradingException.class);
		}
		
		@Test
		public void buildTwoUnits_NoResourcesInCity() throws Exception {
			
			when( cityStocks.canPay( unitsPriceMultipliedByZero ) ).thenReturn(false);
			when( cityStocks.canPay( unitsPriceMultipliedByOne ) ).thenReturn(false);
			when( cityStocks.canPay( unitsPriceMultipliedByTwo ) ).thenReturn(false);
			when( cityStocks.canPay( unitsPriceMultipliedByThree ) ).thenReturn(false);
			
			int builtUnits = cb.buildUnit(unitId, 2);			
			
			assertEquals(0, builtUnits);
			verify_objectBuildQueue_addUnitBuild_notInvoked();
			verify_cityStocks_doPayInvokedOnce( unitsPriceMultipliedByZero );
		}
		
		@Test
		public void requestThreeUnitsToBuild_haveResourcesOnlyForTwo() throws Exception {
			
			when( cityStocks.canPay( unitsPriceMultipliedByOne ) ).thenReturn(true);
			when( cityStocks.canPay( unitsPriceMultipliedByTwo ) ).thenReturn(true);
			when( cityStocks.canPay( unitsPriceMultipliedByThree ) ).thenReturn(false);
			
			int builtUnits = cb.buildUnit(unitId, 3);
			
			assertEquals(2, builtUnits);
			Mockito.verify( objectBuildQueue ).addUnitBuild(unitId, 2);
			verify_cityStocks_doPayInvokedOnce( unitsPriceMultipliedByTwo );
		}
		
		@Test
		public void requestTwoUnitsToBuild_haveResourcesForThree_shouldBuildOnlyTwo() throws Exception {
			
			when( cityStocks.canPay( unitsPriceMultipliedByOne ) ).thenReturn(true);
			when( cityStocks.canPay( unitsPriceMultipliedByTwo ) ).thenReturn(true);
			when( cityStocks.canPay( unitsPriceMultipliedByThree ) ).thenReturn(true);
			
			int builtUnits = cb.buildUnit(unitId, 2);
			
			assertEquals(2, builtUnits);
			Mockito.verify( objectBuildQueue ).addUnitBuild(unitId, 2);
			verify_cityStocks_doPayInvokedOnce( unitsPriceMultipliedByTwo );
		}

		@Test
		public void requestTwoUnitsToBuild_haveResourcesForTwo_shouldBuildTwoUnits() throws Exception {
			
			when( cityStocks.canPay( unitsPriceMultipliedByOne ) ).thenReturn(true);
			when( cityStocks.canPay( unitsPriceMultipliedByTwo ) ).thenReturn(true);
			when( cityStocks.canPay( unitsPriceMultipliedByThree ) ).thenReturn(false);
			
			int builtUnits = cb.buildUnit(unitId, 2);
			
			assertEquals(2, builtUnits);
			Mockito.verify( objectBuildQueue ).addUnitBuild(unitId, 2);
			verify_cityStocks_doPayInvokedOnce( unitsPriceMultipliedByTwo );
		}
		
		@Test
		public void processCityBuilding_queueNotActive_buildingBuildNotActive_shouldDoNothing() throws Exception {
			
			cb.process( msToNs(unitBuildTimeMs) ) ;
			
			Mockito.verify( objectBuildQueue, times(0)).processQueues( Mockito.anyInt());
			Mockito.verify( buildingBuild, times(0)).process(Mockito.anyInt());
			
		}
		
		@Test
		public void unitsRequirementNotFulfilled_buildShouldFail() throws Exception {
			
			when(unitsNode.canBuild(Mockito.any(UsersCityConfig.class))).thenReturn(false);
			catchException( cb ).buildUnit(unitId, 2);
			assertThat(caughtException()).isInstanceOf( RequirementNotFullfilledException.class);
			
		}
		
		private void verify_objectBuildQueue_addUnitBuild_notInvoked() {
			Mockito.verify( objectBuildQueue, times(0) ).addUnitBuild( Mockito.anyInt(), Mockito.anyInt() );
		}
	}

	protected void mockForBuilding_objectBuildQueue_isActive(boolean isActive) {
		when( objectBuildQueue.isActive()).thenReturn(isActive);
	}

	protected void mock_isBuildingBuildActive(boolean isActive) {
		when( buildingBuild.isActive() ).thenReturn( isActive );
	}
	
	protected void mockForBuilding_cityStocks_canPay(boolean canPay) {
		when( cityStocks.canPay(buildingPrice) ).thenReturn( canPay );
	}

	protected OngoingStubbing<Boolean> mockForBuilding_reqNode_canBuild(boolean canBuild) {
		return when(reqNodeOfBuildingModel.canBuild(myCity)).thenReturn( canBuild );
	}
	
	protected void verify_buildingBuild_startUpgradeInvokedOnce() {
		Mockito.verify( buildingBuild , times(1) ).startUpgrade();
	}
	protected void verify_buildingBuild_startUpgradeNotInvoked() {
		Mockito.verify( buildingBuild , times(0) ).startUpgrade();
	}
	
	protected void verify_buildingBuild_processNotInvoked() {
		Mockito.verify( buildingBuild , times(0) ).process(Mockito.anyInt());
	}
	public void verify_objectBuildQueue_process(long processTimeNs) {
		Mockito.verify( objectBuildQueue, times(1)).processQueues( processTimeNs );	
	}
	public void verify_objectBuildQueue_processNotInvoked() {
		Mockito.verify( objectBuildQueue, times(0)).processQueues( Mockito.anyInt() );	
	}
	
	protected void verify_buildingBuild_process( long... consecutiveProcessTimesNs) {
		InOrder inOrder = Mockito.inOrder( buildingBuild );
		for(long i : consecutiveProcessTimesNs)
			inOrder.verify( buildingBuild ).process( i );			
	}

	protected void verify_cityStocks_doPayInvokedOnce(PriceStockSet price) throws PayException {
		
			Mockito.verify( cityStocks , times(1) ).doPay( price );
	}
	protected void verify_cityStocks_doPayNotInvoked( ) throws PayException {
		
			Mockito.verify( cityStocks , times(0) ).doPay( Mockito.any(PriceStockSet.class) );
	}
	
	@Ignore
	public static class CityBuildingStub extends CityBuilding{

		private int level;
		private int buildingId;
		private UsersCityConfig myCity;
		private ObjectBuildQueue obq;
		private BuildingBuild bb;
		
		@Override
		public int getLevel() {
			return level;
		}

		@Override
		public int getBuildingId() {
			return buildingId;
		}

		@Override
		protected UsersCityConfig getCity() {
			return myCity;
		}

		@Override
		protected ObjectBuildQueue getObjectBuildQueue() {
			return obq;
		}

		@Override
		protected BuildingBuild getBuildingBuild() {
			return bb;
		}

		@Override
		protected void setLevel(int level) {
			this.level = level;
		}

		@Override
		protected void setMyCity(UsersCityConfig city) {
			this.myCity = city;
		}

		@Override
		protected void setObjectBuildQueue(ObjectBuildQueue obq) {
			this.obq = obq;
		}

		@Override
		protected void setBuildingBuild(BuildingBuild bb) {
			this.bb = bb;
		}

		@Override
		protected void setBuildingId(int buildingId) {
			this.buildingId = buildingId;
			
		}
		
	}
	
	private class EntityFactoryForTests extends EntityFactory {

		@Override
		public CityStockSet createCityStocks(UsersCityConfig city) {
			return cityStocks;
		}

		@Override
		public CityArmy createCityArmy() {
			return null;
		}

		@Override
		public ObjectBuildQueue createObjectBuildQueue(CityBuilding cb) {
			return objectBuildQueue;
		}

		@Override
		public BuildingBuild createBuildBuild(CityBuilding cb) {
			return buildingBuild;
		}

		@Override
		protected ExtractCityBuilding instantiateExtractCityBuilding() {
			return null;
		}

		@Override
		protected CityBuilding instantiateCityBuilding() {
			return null;
		}

		@Override
		protected BuildingBuild instantiateBuildBuild() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected CityResource instantiateCityResource() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected TaxStock instantiateTaxStock() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected CityArmy instantiateCityArmy() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected ObjectBuildQueue instantiateObjectBuildQueue() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected CityStockSet instantiateCityStockSet() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected CityPeople instantiateCityPeople() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected CityFood instantiateCityFood() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected UsersCityConfig instantiateCity() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	
}

