package entities.buildings;

import static entities.buildings.TestingUtils.assertEqualTimes;
import static entities.buildings.TestingUtils.fractionOfBuildTimeInMs;
import static entities.buildings.TestingUtils.incrementCityTime;
import static entities.buildings.TestingUtils.mock_ObjectBuildQueue_getIdx;
import static entities.buildings.TestingUtils.msToNs;
import static entities.buildings.TestingUtils.processQueueBuild;
import static org.mockito.Mockito.when;
import game_model_impl.BuildModelException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;

@RunWith(Enclosed.class)
public class QueueBuildTests{

	@Mock
	protected ObjectBuildQueue obq;
	protected final int initialCityTimeMs_ = 10;
	
	@Before
	public void setupObjectBuildQueue(){
		TestingUtils.mock_ObjectBuildQueue_getCityTime(obq, initialCityTimeMs_ );
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class TwoConsecutiveQueueBuilds extends QueueBuildTests {
		
		private int firstBuildTimeMs = 50;
		private int secondBuildTimeMs = 70;
		private QueueBuild first;
		private QueueBuild second;
		
		@Before
		public void setUp() throws BuildModelException{
			first = createQueueBuild(firstBuildTimeMs);
			second = createQueueBuild(secondBuildTimeMs);
			mock_ObjectBuildQueue_getIdx(obq, first , second );
		}

		@Test
		public void queueBuildsAtStart(){
			
			assertEqualTimes(initialCityTimeMs_ + firstBuildTimeMs ,first.getFinishTime_(0));
			assertEqualTimes(firstBuildTimeMs ,first.getRemainingTime(0));
			
			assertEqualTimes(initialCityTimeMs_ + firstBuildTimeMs + secondBuildTimeMs ,second.getFinishTime_(1));
			assertEqualTimes(firstBuildTimeMs + secondBuildTimeMs ,second.getRemainingTime(1));
		}
		
		@Test
		public void processFourFifthOfQueueBuild() throws Exception{
			
			int four_fifth = TestingUtils.fractionOfBuildTimeInMs(first, 4, 5);
			processQueueBuild(first, four_fifth );
			incrementCityTime(obq, four_fifth ); // synchronize city time with queue build
			
			assertEqualTimes(initialCityTimeMs_ + firstBuildTimeMs, first.getFinishTime_(0));
			assertEqualTimes(firstBuildTimeMs - four_fifth ,first.getRemainingTime(0));
			
			assertEqualTimes(initialCityTimeMs_ + firstBuildTimeMs + secondBuildTimeMs ,second.getFinishTime_(1));
			assertEqualTimes(firstBuildTimeMs + secondBuildTimeMs - four_fifth ,second.getRemainingTime(1));
		}
		
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class SingleQueueBuild extends QueueBuildTests {
		
		private QueueBuild ub;
		private final int queueBuildTimeMs = 60;
		
		@Before
		public void singleQueueBuildSetup(){
			ub = createQueueBuild(queueBuildTimeMs);
		}
		
		@Test
		public void queueBuildAtStartUp() throws Exception {
			assertEqualTimes( initialCityTimeMs_ + queueBuildTimeMs, ub.getFinishTime_(0) );
			assertEqualTimes( queueBuildTimeMs, ub.getRemainingTime(0) );
		}
		
		@Test
		public void processQuaterOfQueueBuild() throws Exception{
			
			int quaterOfQueueBuild = fractionOfBuildTimeInMs(ub, 1, 4);
			processQueueBuild(ub, quaterOfQueueBuild);
			incrementCityTime(obq, quaterOfQueueBuild); // synchronize city time with processed queue build

			assertEqualTimes( queueBuildTimeMs - quaterOfQueueBuild, ub.getRemainingTime(0) );
			assertEqualTimes( initialCityTimeMs_ + queueBuildTimeMs, ub.getFinishTime_(0) );
		}
		
		@Test
		public void processOneThirdOfQueueBuild() throws Exception{
			
			int oneThirdOfQueueBuild = fractionOfBuildTimeInMs(ub, 1, 3);
			processQueueBuild(ub, oneThirdOfQueueBuild);
			incrementCityTime(obq, oneThirdOfQueueBuild); //synchronize city time with processed QueueBuild

			assertEqualTimes(queueBuildTimeMs - oneThirdOfQueueBuild, ub.getRemainingTime(0));
			assertEqualTimes(initialCityTimeMs_ + queueBuildTimeMs, ub.getFinishTime_(0));
		}
		
		@Test
		public void processWholeBuild() throws Exception{
			
			int wholeQueueBuildTimeMs = fractionOfBuildTimeInMs(ub, 1, 1);
			
			processQueueBuild(ub, wholeQueueBuildTimeMs);
			incrementCityTime(obq, wholeQueueBuildTimeMs); // synchronize city time with processed queue build
			
			assertEqualTimes(0 , ub.getRemainingTime(0));
			assertEqualTimes(initialCityTimeMs_ + wholeQueueBuildTimeMs, ub.getFinishTime_(0));
		}
	}
	
	public QueueBuild createQueueBuild(int buildTimeToSpy) {
		//UnitBuildStub(ObjectBuildQueue obq, int unitId, int unitsToBuild){
		QueueBuild ub = PowerMockito.spy(new QueueBuildStub( obq));
		mock_QueueBuild_BuildTime(ub, buildTimeToSpy);
		return ub;
	}
	
	private void mock_QueueBuild_BuildTime(QueueBuild qb, int buildTimeMs){
		when( qb.getBuildTime()).thenReturn( msToNs( buildTimeMs));
	}
	
	@Ignore
	public static class QueueBuildStub extends QueueBuild {

		private ObjectBuildQueue objectBuildQueue;
		private long doneTimeNs=0;
		
		public QueueBuildStub( ObjectBuildQueue obq ) {
			this.objectBuildQueue = obq;
		}

		@Override
		public long getBuildTime() {
			return 0; // this method is mocked in each unit test
		}

		@Override
		public int getObjectId() {
			return 88; //useless id
		}

		@Override
		protected ObjectBuildQueue getObjectBuildQueue() {
			return objectBuildQueue;
		}

		@Override
		protected long getDoneTime() {
			
			return doneTimeNs;
		}

		@Override
		protected void setDoneTime(long doneTimeNs) {
			
			this.doneTimeNs = doneTimeNs;
		}

		@Override
		public boolean impactsRecursion() {
			return true;
		}

		
	}
	
}

