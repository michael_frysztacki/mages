package entities.buildings;

import game_model_interfaces.ICivilization;
import game_model_interfaces.IGameModel;
import entities.UsersCityConfig;


public abstract class QueueBuild{

	public abstract int getObjectId();
	public long getRemainingTime(int myIdx) {
		
		return getFinishTime_( myIdx ) - getObjectBuildQueue().getCityTime_();
	}
	/*
	 * @param myIdxInQueue - the behaviour of this method in undefinded while passing wrong myIdxInQueue parameter.
	 */
	public long getFinishTime_(int myIdxInQueue) {
		
		if( myIdxInQueue == 0)
			return getBuildTime() + getObjectBuildQueue().getCityTime_() - getDoneTime();
		return getFinishTimeOfPreviousBuild(myIdxInQueue-1) + getBuildTime();
	}
		private long getFinishTimeOfPreviousBuild(int idxOfPrevious) {
			
			return getObjectBuildQueue().get( idxOfPrevious ).getFinishTime_(idxOfPrevious);
		}
	/*
	 * method returns how long durates this build no matter how much is already processed.
	 */	
	public abstract long getBuildTime();
	public abstract boolean impactsRecursion();
	public void processBuild(long nsTime){
		
		incrementDoneTime( nsTime );
	}
		private void incrementDoneTime(long timeNs){
			
			setDoneTime(getDoneTime() + timeNs);
		}
	protected abstract void setDoneTime(long doneTimeNs);
	protected abstract ObjectBuildQueue getObjectBuildQueue();
	protected abstract long getDoneTime();
	protected UsersCityConfig getUcc(){
		return getObjectBuildQueue().getCity();
	}
	protected final int getMyObjectsBuildTimeMs() {
		
		return getQueueBuildsGameModel().getBuildTime(getUcc());
	}
	protected final IGameModel getQueueBuildsGameModel() {
		
		ICivilization myCivil = getObjectBuildQueue().getPlayersCivilization();
		return myCivil.getById( getObjectId(),IGameModel.class);
	}
	
	/*
	 * metoda zwraca czy ten build jest znacz�cy dla rekurencji.
	 * Nie jest znacz�cy, je�li jest zwyk�ym badaniem, np kartografia, kt�ra nie wp�ywa na rekurencj�.
	 */
	/*
	public boolean isMeaningfulBuild() throws UnexpectedException{
		Civilization myCivil = this.getUc().getMyCivilization();
		IGameModel gameModel =  myCivil.getById( this.objectId, IGameModel.class);
		if(!gameModel.canCast(IUnitModel.class)) return true;
		try{
			IImprovementModel rModel = gameModel.cast(IImprovementModel.class);
			if(rModel.isExtractionImprovement())return true;
			else return false;
		}
		catch(MyClassCastException e){
			
		}
		try{
			gameModel.cast(IResearchModel.class);
			return false;
		}
		catch(MyClassCastException e){
			throw new UnexpectedException();
		}
		
	}
	*/
	
	/*
	public void assignToProperQueue(UsersCityConfig city) {
		// TODO Auto-generated method stub
		Civilization myCivil = city.getUserConfig().getMyCivilization();
		IGameModel resB = myCivil.getById(this.getObjectId(), IGameModel.class);
		IGameModel mother = resB.getRequirementNode().getMotherBuilding();
		this.setObjectBuildQueue(city.getBuilding(CityBuilding.class,mother.getId()).getObjectBuildQueue());
		city.getBuilding(CityBuilding.class,mother.getId()).getObjectBuildQueue().addBuild(this);
	}
	*/

}
