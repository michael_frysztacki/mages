package entities.buildings;

import static entities.buildings.TestingUtils.fractionOfBuildTimeInMs;
import static entities.buildings.TestingUtils.mock_ObjectBuildQueue_getCityTime;
import static entities.buildings.TestingUtils.mock_QueueBuild_getMyObjectsBuildTimeMs;
import static entities.buildings.TestingUtils.msToNs;
import static entities.buildings.TestingUtils.processQueueBuild;
import static org.junit.Assert.assertEquals;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import entities.UserResearch;

@RunWith(PowerMockRunner.class)
@PrepareForTest(UnitBuild.class)
public class ResearchBuildTests{

	@Mock
	private ObjectBuildQueue obq;
	
	@Mock
	UserResearch userResearch;
	
	private final int researchBuildTimeMs = 20;
	private final int cityTimeMs = 10;
	private final int researchId = 10;
	
	private ResearchBuild ub;
	
	@Before
	public void setUp() throws Exception{
		
		mock_ObjectBuildQueue_getCityTime(obq, cityTimeMs);
		mock_ObjectBuildQueue_GetUserResearch( researchId, userResearch);
		ub = PowerMockito.spy( new ResearchBuildStub(obq, researchId) );
		mock_QueueBuild_getMyObjectsBuildTimeMs(ub, researchBuildTimeMs); // mock QueueBuild super class
	}
	
	@Test
	public void SimplyResearchBuildTimeTest() throws Exception{
		
		assertTimeEqual( researchBuildTimeMs , ub.getBuildTime());
	}
	
	@Test
	public void processHalfResearchBuild_ResearchShouldNotUpgrade() throws Exception{
		
		int halfMs = fractionOfBuildTimeInMs(ub, 1, 2);
		processQueueBuild(ub, halfMs);
		Mockito.verify( userResearch, Mockito.times(0) ).incrementLevel();
	}
	
	@Test
	public void processWholeResearchBuild_ShouldUpgrade() throws Exception{
		
		processQueueBuild(ub, researchBuildTimeMs);
		Mockito.verify( userResearch, Mockito.times(1) ).incrementLevel();
	}
	
	@Test
	public void processWholeResearchBuildInTwoSteps_ShouldUpgrade() throws Exception{
		
		int halfMs = fractionOfBuildTimeInMs(ub, 1, 2);
		processQueueBuild(ub, halfMs);
		processQueueBuild(ub, halfMs);
		Mockito.verify( userResearch, Mockito.times(1) ).incrementLevel();
	}
	
	public static class ClassOrSubclassMatcher<T> extends BaseMatcher<Class<T>> {

	    private final Class<T> targetClass;

	    public ClassOrSubclassMatcher(Class<T> targetClass) {
	        this.targetClass = targetClass;
	    }

	    @SuppressWarnings("unchecked")
	    public boolean matches(Object obj) {
	        if (obj != null) {
	            if (obj instanceof Class) {
	                return targetClass.isAssignableFrom((Class<T>) obj);
	            }
	        }
	        return false;
	    }

	    public void describeTo(Description desc) {
	        desc.appendText("Matches a class or subclass");
	    }       
	}
	
	private void assertTimeEqual(int expectedMs, long computedNs) {
		assertEquals( msToNs( expectedMs) , computedNs);
	}
	
	private void mock_ObjectBuildQueue_GetUserResearch(int researchId, UserResearch research) throws Exception {
		Mockito.when( obq.getUserResearch(researchId)).thenReturn(userResearch);
	}
	
	private static class ResearchBuildStub extends ResearchBuild {

		private ObjectBuildQueue obq;
		private long doneTimsNs = 0;
		private int researchId = 0;
		
		public ResearchBuildStub(ObjectBuildQueue obq, int researchId){
			this.obq = obq;
			this.researchId = researchId;
		}
		
		@Override
		public int getObjectId() {
			return researchId;
		}

		@Override
		protected ObjectBuildQueue getObjectBuildQueue() {
			return obq;
		}

		@Override
		protected long getDoneTime() {
			return doneTimsNs;
		}

		@Override
		protected void setDoneTime(long timeNs) {
			doneTimsNs = timeNs;
		}
		
	}
	
}

