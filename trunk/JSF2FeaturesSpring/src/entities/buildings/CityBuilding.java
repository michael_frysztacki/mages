package entities.buildings;

import economycomputation.recursion_events.RecursionEvent;
import entities.EntityFactory;
import entities.UsersCityConfig;
import entities.buildings.BuildingBuild.BuildingCurrentlyUpgradingException;
import entities.stocks.CityStockSet;
import entities.PayException;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IRequirementNode;
import game_model_interfaces.IRequirementNode.RequirementNotFullfilledException;
import game_model_interfaces.IUnitModel;
import game_model_interfaces.PriceStockSet;

public abstract class CityBuilding {

	public abstract int getLevel();
	public abstract int getBuildingId();
	public void init(UsersCityConfig myCity, EntityFactory factory, int buildingId) {
		setLevel(0);
		setMyCity(myCity);
		setObjectBuildQueue(factory.createObjectBuildQueue(this));
		setBuildingBuild(factory.createBuildBuild(this));
		setBuildingId(buildingId);
	}
	protected abstract void setBuildingId(int buildingId);
	protected abstract void setMyCity(UsersCityConfig city);
	protected abstract void setObjectBuildQueue(ObjectBuildQueue obq);
	protected abstract void setBuildingBuild(BuildingBuild bb);
	@Override
	public String toString() {
		
		return Integer.toString(getBuildingId());
	}
	public void process(long buildTimsNs) {
		
		if(getBuildingBuild().isActive())
			getBuildingBuild().process(buildTimsNs);
		if(getObjectBuildQueue().isActive())
			getObjectBuildQueue().processQueues(buildTimsNs);
	}
	public void upgrade() throws PayException, BuildingCurrentlyUpgradingException, RequirementNotFullfilledException, QueueOfBuildIsActiveException {
		
		if(isCurrentlyUpgrading())
			throw new BuildingCurrentlyUpgradingException();
		if(getObjectBuildQueue().isActive())
			throw new QueueOfBuildIsActiveException();
		if(!fulfilRequirements())
			throw new IRequirementNode.RequirementNotFullfilledException();
		if(!haveEnoughResourcesForUpgrade())
			throw new PayException();
		getCityStocks().doPay(getBuildingsPrice());
		getBuildingBuild().startUpgrade();
	}
	public boolean isCurrentlyUpgrading() {	
		return getBuildingBuild().isActive();
	}
	public boolean haveEnoughResourcesForUpgrade(){
		return getCityStocks().canPay(getBuildingsPrice());
	}
		private CityStockSet getCityStocks() {
			return getCity().getCityStocks();
		}
		private PriceStockSet getBuildingsPrice() {
			return getBuildingModel().getPrice(getCity());
		}
	public boolean fulfilRequirements() {
		return getBuildingModel().getRequirementNode().canBuild( getCity());
	}
		private IBuildingModel getBuildingModel() {
			return getMyCivilization().getById( getBuildingId(),IBuildingModel.class);
		}
			private ICivilization getMyCivilization() {
				return getCity().getMyCivilization();
			}
	public int buildUnit(int unitId , int requestedAmount) throws BuildingCurrentlyUpgradingException, RequirementNotFullfilledException {
		
		if(isCurrentlyUpgrading())throw new BuildingCurrentlyUpgradingException();
		if(!getUnitModel(unitId).getRequirementNode().canBuild(getCity()))throw new RequirementNotFullfilledException();
		int unitsToBuild = Math.min(calculateMaxOfUnitsToBuildRegardingCityResources(unitId), requestedAmount);
		doPay(unitId, unitsToBuild );
		if( unitsToBuild > 0)
			getObjectBuildQueue().addUnitBuild(unitId, unitsToBuild);
		return unitsToBuild;
	}
		private int calculateMaxOfUnitsToBuildRegardingCityResources(int unitId) {
			int multiplier = 1;
			PriceStockSet multiplied = mulipliedUnitsPrice(unitId, multiplier);
			while( canPay( multiplied ) )
				multiplied = mulipliedUnitsPrice(unitId, ++multiplier);
			return --multiplier;
		}
			private boolean canPay(PriceStockSet multiplied) {
				return getCity().getCityStocks().canPay( multiplied );
			}
			private PriceStockSet mulipliedUnitsPrice(int unitId, int multiplier) {
				return getUnitModel(unitId).getPrice(getCity()).cloneAsMultiplied(multiplier);
			}
				private IUnitModel getUnitModel(int unitId) {
					return getMyCivilization().getById( unitId , IUnitModel.class);
				}
		private void doPay(int unitId, int multiplier) {
			getCity().getCityStocks().doPay(mulipliedUnitsPrice(unitId, multiplier));
		}
	public RecursionEvent getNextRecursionEvent() {
		return getObjectBuildQueue().getNextRecursionEvent();
	}
	protected abstract UsersCityConfig getCity();
	protected abstract ObjectBuildQueue getObjectBuildQueue();
	protected abstract BuildingBuild getBuildingBuild();
	protected abstract void setLevel(int level);
	void incrementLevel() {
		setLevel(getLevel()+1);
	}
	long getCityTime_() {
		return getCity().getCityTimeNs_();
	}
	ICivilization getPlayersCivilization() {
		return getCity().getMyCivilization();
	}
	@SuppressWarnings("serial")
	public static class QueueOfBuildIsActiveException extends Exception {
		
	}
	/*
	@Override
	public boolean equals(Object o) {
		
		CityBuilding cb = (CityBuilding) o;
		return this.getBuildingId() == cb.getBuildingId();
	}
	*/

}
