package entities.buildings;

import static helpers.CollectionUtil.filterWhichMatchSubclass;

import java.util.ArrayList;
import java.util.List;

import game_model_interfaces.ICivilization;

import economycomputation.recursion_events.RecursionEvent;
import entities.UserResearch;
import entities.UsersCityConfig;
import entities.stocks.CityPeople;
import entities.units.CityUnitGroup;

/*
 * This class is strongly related with CityBuilding class.
 * Using methods without checking if ObjectBuildQueue isActive is undefined.
 */
public abstract class ObjectBuildQueue {

	public void init(CityBuilding cityBuilding) {
		setCityBuilding(cityBuilding);
		setBuilds(new ArrayList<QueueBuild>());
	}
	protected abstract void setCityBuilding(CityBuilding cb);
	protected abstract void setBuilds(List<QueueBuild> builds);
	public int getBuildsAmount(){
		return getBuilds().size();
	}
	/*
	 * process empty is undefined, always check if isActive.
	 */
	public void processQueues(long processTimeNs){
		ObjectBuildQueueProcessor queueProcessor = new ObjectBuildQueueProcessor(processTimeNs);
		queueProcessor.process();
	}
		private class ObjectBuildQueueProcessor {
			
			private long remainingProcessTimeNs;
			
			public ObjectBuildQueueProcessor(long wholeProcessTimeNs){
				this.remainingProcessTimeNs = wholeProcessTimeNs;
			}
			public void process(){
				
				while(remainingProcessTimeNs > 0) {
					if(noBuildsLeftInQueue()) break; 
					long firstQueueBuildTime = buildTimeOfFirstQueueBuild();
					processNextQueueBuild(firstQueueBuildTime);
					remainingProcessTimeNs -= firstQueueBuildTime;
				}
			}
				private boolean noBuildsLeftInQueue() {
					return getBuilds().size() == 0;
				}
				private void processNextQueueBuild(long nextQueueBuildTime) {
					
					if( remainingProcessTimeNs >= nextQueueBuildTime )
						processAndRemoveFirstQueueBuild();
					else
						processPartialyFirstQueueBuild(remainingProcessTimeNs);
				}
				private void processAndRemoveFirstQueueBuild() {
					
					processWholeFirstQueueBuild();
					removeFirstQueueBuild();
				}
				private long buildTimeOfFirstQueueBuild() {
					
					return get(0).getBuildTime();
				}
				private void processWholeFirstQueueBuild() {
					
					get(0).processBuild( buildTimeOfFirstQueueBuild() );
				}
				private void removeFirstQueueBuild() {
					
					getBuilds().remove(0);
				}
				private void processPartialyFirstQueueBuild(long processTimeNs) {
					
					get(0).processBuild(processTimeNs);
				}
		}
	
	public void addResearchBuild(int researchId) {
		
		getBuilds().add(makeResearchBuild(researchId));
	}
	public void addUnitBuild(int unitId, int amount) {
		
		if ( isActive() && lastQueueBuildIsUnitBuild(unitId) )
			getLastUnitBuild().addUnitsToBuild(amount);
		else
			addNewUnitBuild(unitId, amount);
	}
		private boolean lastQueueBuildIsUnitBuild(int unitId) {

			return getBuilds().get(0).getObjectId() == unitId;
		}
		private UnitBuild getLastUnitBuild() {
			
			return (UnitBuild) getBuilds().get(getBuilds().size()-1);
		}
		private void addNewUnitBuild(int unitId, int amount) {
			
			getBuilds().add(makeUnitBuild(unitId, amount));
		}
	public double getTrainedPeople() {
		
		double ret = 0.0d;
		for(UnitBuild ub : filterWhichMatchSubclass(getBuilds(), UnitBuild.class ))
			ret += ub.getTrainedPeopleAmount();
		return ret;
	}
	
	public boolean isActive() {
		
		return this.getBuilds().size() != 0;
	}
	
	public ObjectBuildQueueRecursionEvent getNextRecursionEvent() {
		if(timeToNextRecursionEvent() == null)
			return null;
		return new ObjectBuildQueueRecursionEvent();
	}
	/*
	 * przyk�ad kolejki:
	 * 
	 * unit1 |(t1) unit2 |(t2) research1 |(t3) research 2 wydobywczy |(t4) reserach3 |(t5) unit3
	 * 
	 * metoda zwraca czas w milisekundach do nast�pnej znacz�cej rekurencji.
	 * nieznacz�cymi obiektami w kolejce s� research1 i research3 , dlatego ze nie wp�ywaj� one na rekurencj�.
	 * znacz�ce rekurencje : t1, t2, t4, t5.
	 * t5 dlatego ze zaczynaja sie budowac nowe unity i ma to znaczenie dla getEatFactors()
	 */
	Long timeToNextRecursionEvent() {
		for(int idx=0; idx<getBuilds().size(); idx++) {
			if( impactsRecursion(idx))
				return getRemainingTime(idx);
			if(!isLastQueueBuild(idx))
				if(impactsRecursion(idx+1))
					return getRemainingTime(idx);
		}
		return null;
	}
		private boolean isLastQueueBuild(int queueBuildIdx) {
			return queueBuildIdx == getBuilds().size() - 1;
		}
		private long getRemainingTime(int queueBuildsIdx) {
			return getBuilds().get(queueBuildsIdx).getRemainingTime(queueBuildsIdx);
		}
		private boolean impactsRecursion(int queueBuildsIdx) {
			return getBuilds().get(queueBuildsIdx).impactsRecursion();
		}
	
	protected abstract CityBuilding getCityBuilding();
	protected abstract List<QueueBuild> getBuilds();
	
	protected abstract UnitBuild makeUnitBuild(int unitId, int amount);
	
	protected abstract ResearchBuild makeResearchBuild(int researchId);
	
	long getCityTime_(){
		return getCityBuilding().getCityTime_();
	}
	UsersCityConfig getCity(){
		return getCityBuilding().getCity();
	}
	CityUnitGroup getCityUnitGroup(int unitGroupId){
		return null;
		//return getCity().getMyArmy().getUnitGroupById(unitGroupId);
	}
	UserResearch getUserResearch(int researchId){
		return getCity().getUserConfig().getResearch(researchId);
	}
	QueueBuild get(int idx){
		return getBuilds().get(idx);
	}
	ICivilization getPlayersCivilization(){
		return getCityBuilding().getPlayersCivilization();
	}
	CityPeople getCityPeopleStock() {
		return getCity().getCityStocks().getPeopleStock();
	}
	
	public class ObjectBuildQueueRecursionEvent extends RecursionEvent {

		@Override
		public Long recursionTimeNs() {
			return ObjectBuildQueue.this.timeToNextRecursionEvent();
		}

		@Override
		public void process() {
			ObjectBuildQueue.this.processQueues(recursionTimeNs());
		}

	}
	
	/*
	@Override
	public boolean equals(Object o)
	{
		ObjectBuildQueue ubq = (ObjectBuildQueue)o;
		if(ubq.getCityBuilding().getBuildingId() == this.getCityBuilding().getBuildingId() &&
				ubq.getCityBuilding().getCity().getId() == this.getCityBuilding().getCity().getId()
				)return true;
		else return false;
	}
	@Override
	public int hashCode() {
		int ret = 37;
		ret *= this.getCityBuilding().getBuildingId();
		ret *= this.getCityBuilding().getCity().getId();
		return ret;
	}
	*/

}
