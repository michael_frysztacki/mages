package entities.buildings;

import static entities.buildings.TestingUtils.msToNs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import game_model_impl.BuildModelException;

import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;

@RunWith(Enclosed.class)
public class ObjectBuildQueueTests{

	protected ObjectBuildQueue obq;
	protected final int initialCityTimeMs_ = 10;
	
	@Mock
	private CityBuilding cb;
	
	@Before
	public void setUp() throws BuildModelException {
		
		obq = PowerMockito.spy( new ObjectBuildQueueStub() );
		obq.init(cb);
	}

	@RunWith(MockitoJUnitRunner.class)
	public static class EmptyObjectBuildQueue extends ObjectBuildQueueTests {
		 
		@Mock
		private ResearchBuild rb;
		private final int rbId = 10;
		@Mock
		private ResearchBuild rb2;
		private final int rb2Id = 11;
		@Mock
		private UnitBuild ub;
		private final int ubId = 12;
		@Mock
		private UnitBuild ub2;
		private final int ub2Id = 13;

		private final int unitBuildAmount = 4;
		
		@Before
		public void setUpSimpleTests() throws BuildModelException {
			
			Mockito.doReturn(rb).when(obq).makeResearchBuild(rbId);
			Mockito.doReturn(rb2).when(obq).makeResearchBuild(rb2Id);
			Mockito.doReturn(ub).when(obq).makeUnitBuild(ubId, unitBuildAmount);
			Mockito.doReturn(ub2).when(obq).makeUnitBuild(ub2Id, unitBuildAmount);
			Mockito.when(ub.getObjectId()).thenReturn(ubId);
			Mockito.when(ub2.getObjectId()).thenReturn(ub2Id);
			Mockito.when(rb.getObjectId()).thenReturn(rbId);
			Mockito.when(rb2.getObjectId()).thenReturn(rb2Id);
		}
		
		@Test
		public void emptyOnCreation() {
			
			assertEquals(0, obq.getBuildsAmount());
			assertFalse(obq.isActive());
			obq.getBuilds();
		}
		
		@Test
		public void addOneUnitBuild() throws Exception {
			
			obq.addUnitBuild(ubId , unitBuildAmount);
			assertEquals(1, obq.getBuildsAmount());
			assertTrue(obq.isActive());
		}
		
		@Test
		public void addTwoUnitBuildsWithsDifferentIds() {
			
			obq.addResearchBuild(rbId);
			obq.addResearchBuild(rb2Id);
			assertEquals(2, obq.getBuildsAmount());
			assertTrue(obq.isActive());
		}
		
		@Test
		public void addResearchBuild_UnitBuild_ResearchBuild() {
			
			obq.addResearchBuild(rbId);
			obq.addUnitBuild(ubId, unitBuildAmount);
			obq.addResearchBuild(rb2Id);
			assertEquals(3, obq.getBuildsAmount());
			assertTrue(obq.isActive());
		}
		
		@Test
		public void addTwoUnitBuildsWithSameId_shouldBeOneQueueBuild() {
			
			obq.addUnitBuild(ubId, unitBuildAmount);
			obq.addUnitBuild(ubId, unitBuildAmount);
			assertEquals(1, obq.getBuildsAmount());
			Mockito.verify(ub).addUnitsToBuild(unitBuildAmount );
		}
	
		@Test
		public void getAllTrainedPeople() {
			
			Mockito.when(ub.getTrainedPeopleAmount()).thenReturn(2.0d);
			Mockito.when(ub2.getTrainedPeopleAmount()).thenReturn(3.0d);
			obq.addUnitBuild(ubId, unitBuildAmount);
			obq.addResearchBuild(rbId);
			obq.addUnitBuild(ub2Id, unitBuildAmount);
			assertEquals(5.0d, obq.getTrainedPeople(), 0.0000000001d);
		}
		
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class ObjectBuildQueueWithOneQueueBuild extends ObjectBuildQueueTests {
	
		@Mock
		protected ResearchBuild qb;
		protected final int researchId = 10;
		protected final int researchBuildTimeMs = 40;
		private int remainingTimeOfFirst = researchBuildTimeMs/2;
		
		@Before
		public void setUpResearchBuild() throws BuildModelException {
			
			Mockito.doReturn(qb).when(obq).makeResearchBuild(researchId);
			Mockito.when(qb.getBuildTime()).thenReturn( msToNs( researchBuildTimeMs ));
			Mockito.when(qb.getRemainingTime(0)).thenReturn( msToNs(remainingTimeOfFirst ));
			obq.addResearchBuild( researchId );
		}
		
		@Test
		public void oneQueueBuildJustAfterAdding() throws Exception {
			
			assertEquals(1, obq.getBuildsAmount());
			assertEquals( qb , obq.get(0));
			assertTrue(obq.isActive());
		}
		
		@Test
		public void processHalfOfResearchBuild() throws Exception {
			
			processObjectBuildQueue(researchBuildTimeMs / 2);
			assertEquals(1, obq.getBuildsAmount());
			assertTrue(obq.isActive());
			verify_queueBuild_processBuild_invoked(qb, researchBuildTimeMs / 2 );
		}

		@Test
		public void processOneQueueBuild() throws Exception {
			
			processObjectBuildQueue(researchBuildTimeMs);
			assertEquals(0, obq.getBuildsAmount());
			assertFalse(obq.isActive());
			verify_queueBuild_processBuild_invoked(qb, researchBuildTimeMs );
		}
		
		@Test
		public void timeToNextMeaningfulQueueBuild() throws Exception {
			
			mock_QueueBuilds_impactsRecursion(true, remainingTimeOfFirst);
			mock_QueueBuilds_impactsRecursion(false, null);
		}
		
		private void mock_QueueBuilds_impactsRecursion(boolean first_, Integer expectedTimeMs) {
			
			Mockito.when(qb.impactsRecursion()).thenReturn(first_);
			if(expectedTimeMs != null)
				assertEquals( msToNs(expectedTimeMs), obq.timeToNextRecursionEvent().longValue());
			else
				assertNull(obq.timeToNextRecursionEvent());
		}
	}
	
	@RunWith(MockitoJUnitRunner.class)
	public static class ObjectBuildQueueWithThreeQueueBuilds extends ObjectBuildQueueTests {
	
		@Mock
		protected ResearchBuild first;
		protected final int researchIdOfFirst = 10;
		protected final int researchBuildTimeMsOfFirst = 40;
		private int remainigTimeOfFirst = researchBuildTimeMsOfFirst/2;
		@Mock
		protected ResearchBuild second;
		protected final int researchIdOfSecond = 11;
		protected final int researchBuildTimeMsOfSecond = 70;
		private int remainigTimeOfSecond = researchBuildTimeMsOfFirst/2 + researchBuildTimeMsOfSecond;
		@Mock
		protected ResearchBuild third;
		protected final int researchIdOfThird = 12;
		protected final int researchBuildTimeMsOfThird = 90;
		private int remainigTimeOfThird = researchBuildTimeMsOfFirst/2 + researchBuildTimeMsOfSecond + researchBuildTimeMsOfThird;
		
		@Before
		public void setUpResearchBuild() throws BuildModelException {
			
			Mockito.doReturn(first).when(obq).makeResearchBuild(researchIdOfFirst);
			Mockito.doReturn(second).when(obq).makeResearchBuild(researchIdOfSecond);
			Mockito.doReturn(third).when(obq).makeResearchBuild(researchIdOfThird);
			
			Mockito.when( first.getBuildTime()).thenReturn( msToNs( researchBuildTimeMsOfFirst ));
			Mockito.when( second.getBuildTime()).thenReturn( msToNs( researchBuildTimeMsOfSecond ));
			Mockito.when( third.getBuildTime()).thenReturn( msToNs( researchBuildTimeMsOfThird ));
			Mockito.when( first.getRemainingTime(0)).thenReturn( msToNs(remainigTimeOfFirst ));
			Mockito.when( second.getRemainingTime(1)).thenReturn( msToNs(remainigTimeOfSecond));
			Mockito.when( third.getRemainingTime(2)).thenReturn( msToNs(remainigTimeOfThird));
			obq.addResearchBuild( researchIdOfFirst );
			obq.addResearchBuild( researchIdOfSecond );
			obq.addResearchBuild( researchIdOfThird );
		}
		
		@Test
		public void rigthAfterObjectQueueBuildCreation() throws Exception {
			
			assertEquals(3, obq.getBuildsAmount());
			assertTrue(obq.isActive() );
			queueBuild_process_Notinvoked(first);
			queueBuild_process_Notinvoked(second);
			queueBuild_process_Notinvoked(third);
		}
		
		@Test
		public void processOneFourthOfFirstQueueBuild() throws Exception {
			
			processObjectBuildQueue( researchBuildTimeMsOfFirst / 4 );
			assertEquals(3, obq.getBuildsAmount());
			verify_queueBuild_processBuild_invoked(first, researchBuildTimeMsOfFirst / 4);
			queueBuild_process_Notinvoked(second);
			queueBuild_process_Notinvoked(third);
			assertTrue(obq.isActive() );
		}
		
		@Test
		public void processExactlyOneFirstQueueBuild() throws Exception {
			
			processObjectBuildQueue( researchBuildTimeMsOfFirst);
			assertEquals(2, obq.getBuildsAmount());
			verify_queueBuild_processBuild_invoked(first, researchBuildTimeMsOfFirst );
			queueBuild_process_Notinvoked(second);
			queueBuild_process_Notinvoked(third);
			assertTrue(obq.isActive() );
		}
		
		@Test
		public void processOneAndHalfOfQueueBuilds() throws Exception {
			
			processObjectBuildQueue(researchBuildTimeMsOfFirst + researchBuildTimeMsOfSecond / 2 );
			assertEquals(2, obq.getBuildsAmount());
			verify_queueBuild_processBuild_invoked(first, researchBuildTimeMsOfFirst);
			verify_queueBuild_processBuild_invoked(second, researchBuildTimeMsOfSecond / 2);
			queueBuild_process_Notinvoked(third);
			assertTrue(obq.isActive() );
		}
		
		@Test
		public void processTwoAndHalfOfQueueBuild() throws Exception {
			
			processObjectBuildQueue(researchBuildTimeMsOfFirst + researchBuildTimeMsOfSecond + researchBuildTimeMsOfThird / 2 );
			assertEquals(1, obq.getBuildsAmount());
			verify_queueBuild_processBuild_invoked(first, researchBuildTimeMsOfFirst);
			verify_queueBuild_processBuild_invoked(second, researchBuildTimeMsOfSecond);
			verify_queueBuild_processBuild_invoked(third, researchBuildTimeMsOfThird / 2);
			assertTrue(obq.isActive() );
		}
		
		@Test
		public void processAllQueueBuild() throws Exception {
			
			processObjectBuildQueue(researchBuildTimeMsOfFirst + researchBuildTimeMsOfSecond + researchBuildTimeMsOfThird );
			assertEquals(0, obq.getBuildsAmount());
			verify_queueBuild_processBuild_invoked(first, researchBuildTimeMsOfFirst);
			verify_queueBuild_processBuild_invoked(second, researchBuildTimeMsOfSecond);
			verify_queueBuild_processBuild_invoked(third, researchBuildTimeMsOfThird );
			assertFalse(obq.isActive() );
		}
		
		@Test
		public void processAllWithExceedingTime() throws Exception {
			
			int overflowTimeMs = 40;
			processObjectBuildQueue(researchBuildTimeMsOfFirst + researchBuildTimeMsOfSecond + researchBuildTimeMsOfThird + overflowTimeMs);
			assertEquals(0, obq.getBuildsAmount());
			verify_queueBuild_processBuild_invoked(first, researchBuildTimeMsOfFirst);
			verify_queueBuild_processBuild_invoked(second, researchBuildTimeMsOfSecond);
			verify_queueBuild_processBuild_invoked(third, researchBuildTimeMsOfThird );
			assertFalse(obq.isActive() );
			
		}
		
		@Test
		public void timeToNextMeaningfulQueueBuild() throws Exception {
			
			mock_QueueBuilds_impactsRecursion(true, false, false, remainigTimeOfFirst);
			mock_QueueBuilds_impactsRecursion(true, true, false, remainigTimeOfFirst);
			mock_QueueBuilds_impactsRecursion(false, true, false, remainigTimeOfFirst);
			mock_QueueBuilds_impactsRecursion(false, false, true, remainigTimeOfSecond);
			mock_QueueBuilds_impactsRecursion(false, false, false, null);
		}
		
		@Test
		public void objectBuildQueueRecursionEvent() throws Exception {
			
		}
		
		private void mock_QueueBuilds_impactsRecursion(boolean first_, boolean second_, boolean third_, Integer expectedTimeMs) {
			
			Mockito.when(first.impactsRecursion()).thenReturn(first_);
			Mockito.when(second.impactsRecursion()).thenReturn(second_);
			Mockito.when(third.impactsRecursion()).thenReturn(third_);
			if(expectedTimeMs != null)
				assertEquals( msToNs(expectedTimeMs), obq.timeToNextRecursionEvent().longValue());
			else
				assertNull(obq.timeToNextRecursionEvent());
		}
		
	}

	protected void processObjectBuildQueue(int msTime) {
		obq.processQueues( msToNs(msTime) );
	}

	protected void queueBuild_process_Notinvoked(QueueBuild queueBuild) {
		Mockito.verify( queueBuild , times(0) ).processBuild( Mockito.anyInt());
	}
	
	protected void verify_queueBuild_processBuild_invoked(QueueBuild queueBuild, int queueBuildProcessTimeMs) {
		Mockito.verify( queueBuild , times(1) ).processBuild( msToNs( queueBuildProcessTimeMs ) );
	}
	
	@Ignore
	public static class ObjectBuildQueueStub extends ObjectBuildQueue{

		private List<QueueBuild> builds;
		private CityBuilding cityBuilding;

		@Override
		protected CityBuilding getCityBuilding() {
			
			return cityBuilding;
		}

		@Override
		protected List<QueueBuild> getBuilds() {
			
			return builds;
		}

		@Override
		long getCityTime_() {
			
			return 0;
		}

		@Override
		protected UnitBuild makeUnitBuild(int unitId, int amount) {
			return null;
		}

		@Override
		protected ResearchBuild makeResearchBuild(int researchId) {
			return null;
		}

		@Override
		protected void setBuilds(List<QueueBuild> builds) {
			this.builds = builds;
		}

		@Override
		protected void setCityBuilding(CityBuilding cb) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
}

