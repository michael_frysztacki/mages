package entities.buildings;

import helpers.TimeTranslation;

public abstract class ResearchBuild extends QueueBuild{

	public long getBuildTime(){
		return TimeTranslation.msToNs( this.getMyObjectsBuildTimeMs() );
	}
	@Override
	public boolean impactsRecursion() {
		return true;
	}
	@Override
	public void processBuild(long nsTime)
	{
		super.processBuild(nsTime);
		if( getDoneTime() == this.getBuildTime() )
			getObjectBuildQueue().
			getUserResearch(getObjectId()).
			incrementLevel();
	}
	/*
	 * @Override
	public boolean processBuild(){
		
		if(this.getObjectBuildQueue().idxOf(this) == 0)
		{
			FreeHandForReasearch fh = new FreeHandForReasearch();
			fh.setResearchLvl(this.getUc().getResearch( this.getObjectId()), this.getDestLevel());
			this.getObjectBuildQueue().removeBuild(this);
			this.getObjectBuildQueue().addNsTime(this.getBuildTime());
			return true;
		}
		else return false;
	}
	 */
	
}
