package entities.buildings;

import static entities.buildings.TestingUtils.msToNs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import entities.UsersCityConfig;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.ICivilization;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class BuildingBuildTests {

	protected BuildingBuild bb;
	
	@Mock
	protected CityBuilding cb;
	protected final int cityBaseTimeMs = 10;
	
	@Mock
	protected ICivilization civil;
	
	@Mock
	protected IBuildingModel buildingModel;
	protected final int buildingBuildTimeMs = 30;
	protected final int buildingId = 5;
	
	@Mock
	UsersCityConfig myCity;
	
	@Before
	public void setUp() throws Exception{
		
		bb = new BuildingBuildStub();
		bb.init(cb);
		Mockito.when(cb.getPlayersCivilization()).thenReturn(civil);
		Mockito.when(cb.getCity()).thenReturn(myCity);
		Mockito.when(cb.getBuildingId()).thenReturn(buildingId);
		Mockito.when(cb.getCityTime_()).thenReturn(msToNs(cityBaseTimeMs));
		Mockito.when(civil.getById(buildingId, IBuildingModel.class) ).thenReturn(buildingModel);
		Mockito.when(buildingModel.getBuildTime(myCity)).thenReturn(buildingBuildTimeMs);
	}

		@Test
		public void afterCreationAsserts() throws Exception {
			
			assertFalse(bb.isActive());
		}
		
		@Test
		public void afterUpgrade() throws Exception {
			
			bb.startUpgrade();
			assertTrue(bb.isActive());
			assertEquals( msToNs( cityBaseTimeMs + buildingBuildTimeMs) , bb.getFinishTime_());
			Mockito.verify( cb , Mockito.times(0) ).incrementLevel();
		}
		
		@Test
		public void processActiveWithBuildingBuildTime_shouldUpgradeBuilding() throws Exception {
			
			bb.startUpgrade();
			bb.process(msToNs(buildingBuildTimeMs));
			
			assertFalse(bb.isActive());
			
			Mockito.verify( cb , Mockito.times(1) ).incrementLevel();
		}
		
		@Test
		public void processActiveWithHalfOfBuildingBuildTime_buildingShouldNotUpgrade() throws Exception {
			
			bb.startUpgrade();
			bb.process(msToNs(buildingBuildTimeMs) / 2);
			
			assertTrue(bb.isActive());
			
			Mockito.verify( cb , Mockito.times(0) ).incrementLevel();
		}
		
		@Test
		public void processActiveWithOverTime_buildingShouldUpgrade() throws Exception {
			
			bb.startUpgrade();
			bb.process(msToNs(buildingBuildTimeMs) + 1000);
			
			assertFalse(bb.isActive());
			Mockito.verify(cb, Mockito.times(1)).incrementLevel();
		}
		
		@Test
		public void processWholeActiveForTwoTimes_shouldUpgrade() throws Exception {
			
			bb.startUpgrade();
			int halfBuild = fraction(buildingBuildTimeMs,1,2);
			bb.process(msToNs(halfBuild));
			bb.process(msToNs(halfBuild));
			
			assertFalse(bb.isActive());
			
			Mockito.verify(cb, Mockito.times(1)).incrementLevel();
		}
		
		@Test
		public void processPartialyForTwoTimesJustBeforeUpgrade_shouldNotUpgrade() throws Exception {
			
			bb.startUpgrade();
			int oneSecond = fraction(buildingBuildTimeMs,1,2);
			bb.process(msToNs(oneSecond));
			bb.process(msToNs(oneSecond) - 1);
			
			assertTrue(bb.isActive());
			
			Mockito.verify(cb ,Mockito.times(0)).incrementLevel();
		}
		
		@Test
		public void processOvertimeForThreeTimes_shouldUpgrade() throws Exception {
			
			bb.startUpgrade();
			int oneSecond = fraction(buildingBuildTimeMs,1,2);
			int oneThird = fraction(buildingBuildTimeMs,1,3);
			int twoThird = fraction(buildingBuildTimeMs,2,3);
			bb.process(msToNs(oneSecond));
			bb.process(msToNs(oneThird));
			bb.process(msToNs(twoThird));//overtime
			
			assertFalse(bb.isActive());
			
			Mockito.verify( cb , Mockito.times(1) ).incrementLevel();
		}
		
		@Test
		public void remainingTimeAtStartUp() throws Exception {
			
			bb.startUpgrade();
			assertEquals(msToNs(30), bb.getRemainingTime());
			assertEquals(msToNs(cityBaseTimeMs + buildingBuildTimeMs) ,bb.getFinishTime_());
		}
		
		@Test
		public void remainingTimeAfterHalfProcessing() throws Exception {
			
			bb.startUpgrade();
			int halfBuild = fraction(buildingBuildTimeMs,1,2);
			bb.process(msToNs(halfBuild));
			assertEquals(msToNs(halfBuild), bb.getRemainingTime());
			assertEquals(msToNs(cityBaseTimeMs + buildingBuildTimeMs) ,bb.getFinishTime_());
		}
		
		@Test
		public void remainingTimeAfterOneThirdProcessing() throws Exception {
			
			bb.startUpgrade();
			int oneThird = fraction(buildingBuildTimeMs,1,3);
			int twoThird = fraction(buildingBuildTimeMs,2,3);
			bb.process(msToNs(oneThird));
			assertEquals(msToNs(twoThird), bb.getRemainingTime());
			assertEquals(msToNs(cityBaseTimeMs + buildingBuildTimeMs) ,bb.getFinishTime_());
		}
		
		private int fraction(int time, int numerator,int denominator) {
			return time * numerator / denominator;
		}
		
		public static class BuildingBuildStub extends BuildingBuild {

			private CityBuilding cb;
			private long doneTimeNs;

			@Override
			protected CityBuilding getCityBuilding() {
				return cb;
			}

			@Override
			protected long getDoneTimeNs() {
				
				return doneTimeNs;
			}

			@Override
			protected void setDoneTimeNs(long doneTime) {
				
				this.doneTimeNs = doneTime;
			}

			@Override
			protected void setCityBuilding(CityBuilding myBuilding) {
				this.cb = myBuilding;
				
			}
			
		}
}

