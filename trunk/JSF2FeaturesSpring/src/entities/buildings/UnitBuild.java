package entities.buildings;

import org.jscience.mathematics.number.Rational;

import entities.stocks.CityPeople;
import game_model_interfaces.PriceStock;
import game_model_interfaces.PriceStockSet;
import helpers.TimeTranslation;

public abstract class UnitBuild extends QueueBuild{

	@Override
	public long getBuildTime() {
		
		return TimeTranslation.msToNs( getTotalUnitsToBuildAmount() * getMyObjectsBuildTimeMs() );
	}
	public int getRemainingUnitsToBuild() {
		
		long unitsBuildInDoneTime = getDoneTime() / TimeTranslation.msToNs( getMyObjectsBuildTimeMs() );
		return getTotalUnitsToBuildAmount() - (int)unitsBuildInDoneTime;
	}
	@Override
	public void processBuild(long nsTime) {
		
		super.processBuild( nsTime );
		Rational unitsProduced = divideProcessedTimeByUnitBuildTime(nsTime, getMyObjectsBuildTimeMs());
		increaseUnitAmountInCity(unitsProduced);
	}
		private Rational divideProcessedTimeByUnitBuildTime(long processedTimeNs, int unitBuildTimeMs) {
			
			return Rational.valueOf(processedTimeNs, TimeTranslation.msToNs(unitBuildTimeMs));
		}
		
		private void increaseUnitAmountInCity(Rational increase) {
			
			getObjectBuildQueue().getCityUnitGroup(getObjectId()).increaseUnitAmount(increase);
		}
	public void addUnitsToBuild(int amount) {
		
		incrementUnitAmountInBuild(amount); 
	}
		private void incrementUnitAmountInBuild(int amount) {
			
			setTotalUnitsToBuildAmount(getTotalUnitsToBuildAmount() + amount);
		}
	public double getTrainedPeopleAmount() {
		
		if(gameModelHasPeopleStock())
			return peopleAmountInGameModel() * getRemainingInPercent() * getTotalUnitsToBuildAmount();
		else return 0.0d;
	}
		private Integer peopleAmountInGameModel() {
			
			return getPeopleStock( priceOfMyGameModel()).getVisibleAmount();
		}
		private boolean gameModelHasPeopleStock() {
			
			return getPeopleStock( priceOfMyGameModel() ) != null;
		}
		private PriceStock getPeopleStock(PriceStockSet price) {
			
			return price.getStockById( getCityPeopleStock().getStockId() );
		}
			private CityPeople getCityPeopleStock() {
				
				return getObjectBuildQueue().getCityPeopleStock();
			}
		private PriceStockSet priceOfMyGameModel() {
			
			return getQueueBuildsGameModel().getPrice( getObjectBuildQueue().getCity());
		}
		private double getRemainingInPercent() {
			
			return 1.0d - getDoneInPercent();
		}
		private double getDoneInPercent() {
			
			return ( (double)(int)getDoneTime()/(double)(int)getBuildTime() );
		}
		
	@Override
	public boolean impactsRecursion() {
		return true;
	}
	protected abstract void setTotalUnitsToBuildAmount(int newAmount); 
	protected abstract int getTotalUnitsToBuildAmount();
	
	
}
