package entities.buildings;

import static entities.buildings.TestingUtils.fractionOfBuildTimeInMs;
import static entities.buildings.TestingUtils.mock_QueueBuild_getMyObjectsBuildTimeMs;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import org.jscience.mathematics.number.Rational;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import entities.UsersCityConfig;
import entities.stocks.CityPeople;
import entities.units.CityUnitGroup;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IUnitModel;
import game_model_interfaces.PriceStock;
import game_model_interfaces.PriceStockSet;
import helpers.TimeTranslation;

@RunWith(Enclosed.class)
public class UnitBuildTests {

	@Mock
	protected ObjectBuildQueue obq;
	
	@Mock
	protected CityUnitGroup cityUnitGroup;
	
	protected final int unit10BuildTimeMs = 10;
	protected final int cityTimeMs = 10;
	protected final int fourUnitsToBuild = 4;
	protected final int unitId = 10;
	protected UnitBuild fourUnits;
	
	@Before
	public void setUp() throws Exception{
		fourUnits = PowerMockito.spy ( new UnitBuildStub(obq, unitId, fourUnitsToBuild ) );
		//mock_ObjectBuildQueue_getCityTime(cityTimeMs);
		mock_QueueBuild_getMyObjectsBuildTimeMs(fourUnits, unit10BuildTimeMs);
		mock_ObjectQueueBuild_GetCityUnitGroup_WithUnit10();
	}
	
	@RunWith(PowerMockRunner.class)
	@PrepareForTest(UnitBuild.class)
	public static class GetTrainedPeopleTests extends UnitBuildTests{
		
		@Mock
		ICivilization civil;
		/*
		 * peopleAmountInUnit - how many people contains a unit eg. a regular swordsman is build from 1 people,
		 * but a catapult can be built from 4 peoples.
		 */
		@Mock
		IUnitModel unitModel;
		private final int peopleAmountInUnit = 2;
		
		@Mock
		PriceStockSet priceOfUnit10;
		
		@Mock
		PriceStock peopleStock;
		
		@Mock
		CityPeople cityPeople;
		private final String peopleStockId = "PEOPLE_STOCK";
		
		private double delta = 0.00000000001d;
		@Before
		public void GetTrainedPoepleTestSetup() throws Exception {
			
			PowerMockito.doReturn( unitModel ).when( fourUnits, "getQueueBuildsGameModel");
			when( unitModel.getPrice( any(UsersCityConfig.class) )).thenReturn( priceOfUnit10 );
			when( priceOfUnit10.getStockById( peopleStockId )).thenReturn( peopleStock );
			when( peopleStock.getVisibleAmount() ).thenReturn(peopleAmountInUnit);
			when ( obq.getCityPeopleStock()).thenReturn(cityPeople);
			when( cityPeople.getStockId() ).thenReturn(peopleStockId) ;
			PowerMockito.doReturn( cityPeople ).when(fourUnits, "getCityPeopleStock");
			PowerMockito.doReturn( peopleStockId ).when(cityPeople).getStockId();
//			mock_ObjectBuildQueue_getPlayersCivilization();
//			when( civil.getById(unitId, IUnitModel.class) ).thenReturn(unitModel);
		}
		
		@Test
		public void getTrainedPeopleAfterCreation() throws Exception {
			
			assertEquals( fourUnitsToBuild * peopleAmountInUnit, fourUnits.getTrainedPeopleAmount(), delta);
		}
		
		@Test
		public void getTrainedPeopleAfterThreeEighthProcessing() throws Exception {
			
			int threeEight = fractionOfBuildTimeInMs(fourUnits, 3, 8);
			int fiveEight = fractionOfBuildTimeInMs(fourUnits, 5, 8);
			processUnitBuild(fourUnits, threeEight);
			
			assertEquals(fourUnitsToBuild * peopleAmountInUnit * divide(fiveEight,fiveEight + threeEight) , fourUnits.getTrainedPeopleAmount(), delta);
		}
		
		@Test
		public void getTrainedPeopleAfterWholeProcessing() throws Exception {
			
			int wholeProcess = fractionOfBuildTimeInMs(fourUnits, 1, 1);
			processUnitBuild(fourUnits, wholeProcess);
			
			assertEquals( 0.0d, fourUnits.getTrainedPeopleAmount(), delta);
		}
		
		private double divide(int divisor, int divider) {
			return (double)(divisor)/(double)(divider);
		}
		
	}
	
	@RunWith(PowerMockRunner.class)
	@PrepareForTest(UnitBuild.class)
	public static class JustAfterUnitBuildCreation extends UnitBuildTests{
	
		@Test
		public void SimplyUnitBuildTimeTest() throws Exception {
			
			assertTimeEqual(
					fourUnitsToBuild * unit10BuildTimeMs, 
					fourUnits.getBuildTime());
		}
	
		@Test
		public void remainingUnitsToBuildAfterCreation_shouldReturnInitialAmountOfUnits() throws Exception {
			
			assertEquals(
					fourUnitsToBuild,
					fourUnits.getRemainingUnitsToBuild());
		}
		
		@Test
		public void remainingUnitsToBuildAfterHalfProcess_shouldReturnSecondHalf() throws Exception {
			
			processUnitBuild(fourUnits, unit10BuildTimeMs * 2);
			assertEquals( 2 ,fourUnits.getRemainingUnitsToBuild());
			verify_CityUnit_AmountIncrementation(Rational.valueOf(2, 1));
		}
			
		@Test
		public void remainingUnitsToBuildAfterUnitPartialyProcessed_shouldReturnRoofValue() throws Exception {
			
			int fiveEights = fractionOfBuildTimeInMs(fourUnits, 5, 8);
			processUnitBuild(fourUnits, fiveEights);
			assertEquals( 2 ,fourUnits.getRemainingUnitsToBuild());
			verify_CityUnit_AmountIncrementation(Rational.valueOf(5, 2));
		}
			
		@Test
		public void remainingUnitsJustBeforeSecondUnitFinishesProcessing() throws Exception
		{
			int oneSecond = fractionOfBuildTimeInMs(fourUnits, 1, 2);
			processUnitBuild(fourUnits, oneSecond - 10);
			assertEquals( 3 ,fourUnits.getRemainingUnitsToBuild());
			verifyCallTimes_CityUnit_increaseUnitAmount(1);
		}
			
		@Test
		public void process_whole_unit_build() throws Exception {
			
			processUnitBuild(fourUnits, unit10BuildTimeMs * fourUnitsToBuild); 
			assertEquals( 0, fourUnits.getRemainingUnitsToBuild() );
			verifyCallTimes_CityUnit_increaseUnitAmount(1);
			verify_CityUnit_AmountIncrementation(Rational.valueOf(4, 1));
		}
	
		@Test
		public void addUnitsToExistingUnitBuild() throws Exception {
			
			int additionalUnits = 5;
			
			fourUnits.addUnitsToBuild(additionalUnits);
			
			assertEquals(fourUnitsToBuild + additionalUnits, fourUnits.getRemainingUnitsToBuild());
			assertUnitBuildTime(( additionalUnits + fourUnitsToBuild) * unit10BuildTimeMs );
		}
		
		@Test
		public void addUnitsToExistingUnitBuildAfterPartialProcessing() throws Exception {
			
			int additionalUnits = 5;
			int fiveEights = fractionOfBuildTimeInMs(fourUnits, 5, 8);
			
			processUnitBuild(fourUnits, fiveEights);
			int remainingUnitsAfterUnitBuildProcess = fourUnits.getRemainingUnitsToBuild();
			fourUnits.addUnitsToBuild(additionalUnits);
			
			assertRemainingUnitsAmount( remainingUnitsAfterUnitBuildProcess + additionalUnits );
			assertUnitBuildTime( (fourUnitsToBuild + additionalUnits) * unit10BuildTimeMs );
			
			verify_CityUnit_AmountIncrementation(Rational.valueOf(5, 2));
		}
	
			private void assertUnitBuildTime(int buildTimeMs) {
				assertTimeEqual(buildTimeMs, fourUnits.getBuildTime());
			}
		
			private void assertRemainingUnitsAmount(int remainingUnits ) {
				assertEquals(remainingUnits, fourUnits.getRemainingUnitsToBuild());
			}
		
		@Test
		public void integrationTest_processPartialy_addUnits_processWholeRest() throws Exception {
			int threeUnitsToAdd = 3;
			int half = fractionOfBuildTimeInMs(fourUnits, 1, 2);
			
			processUnitBuild(fourUnits, half);
			fourUnits.addUnitsToBuild(threeUnitsToAdd);
			processUnitBuild(fourUnits, half + buildTimeOfUnitAmount(threeUnitsToAdd));
			
			assertEquals(0, fourUnits.getRemainingUnitsToBuild());
			verifyCallTimes_CityUnit_increaseUnitAmount(2);
			verify_CityUnit_AmountIncrementation(Rational.valueOf(2, 1), Rational.valueOf(5, 1));
		}
	
	}
	
	protected int buildTimeOfUnitAmount(int unitsToAdd) {
		return unitsToAdd * unit10BuildTimeMs;
	}


	protected void verify_CityUnit_AmountIncrementation(Rational... consecutiveAddAmountCalls) {
		verifyCallTimes_CityUnit_increaseUnitAmount(consecutiveAddAmountCalls.length);
		InOrder inOrder = Mockito.inOrder(cityUnitGroup);
		for(int i=0 ; i < consecutiveAddAmountCalls.length ; i++)
			inOrder.verify(cityUnitGroup).increaseUnitAmount( consecutiveAddAmountCalls[i] );
	}
	protected void verifyCallTimes_CityUnit_increaseUnitAmount(int times) {
		Mockito.verify( cityUnitGroup , times(times) ).increaseUnitAmount( any(Rational.class) );
	}

	protected void processUnitBuild(UnitBuild qb, int processTimeMs) {
		qb.processBuild( msToNs(processTimeMs) );
	}
	
	protected long msToNs(int ms){
		return TimeTranslation.msToNs(ms);
	}
	protected void mock_ObjectBuildQueue_getCityTime(int cityTimeMs) {
		when ( obq.getCityTime_()).thenReturn( msToNs(cityTimeMs) );
	}
	
	protected void mock_ObjectQueueBuild_GetCityUnitGroup_WithUnit10() throws Exception {
		PowerMockito.doReturn(cityUnitGroup).when(obq, "getCityUnitGroup", 10);
	}
	
	
	protected void assertTimeEqual(int expectedMs, long computedNs) {
		assertEquals( msToNs( expectedMs) , computedNs);
	}
	
	@Ignore
	public static class UnitBuildStub extends UnitBuild {
		
		private int totalUnitToBuildAmount;
		private ObjectBuildQueue obq;
		private long doneTimsNs = 0;
		private int unitId;
		
		public UnitBuildStub(ObjectBuildQueue obq, int unitId, int unitsToBuild){
			this.obq = obq;
			this.unitId = unitId;
			this.totalUnitToBuildAmount = unitsToBuild;
		}
		
		@Override
		public int getObjectId() {
			return unitId;
		}

		@Override
		protected ObjectBuildQueue getObjectBuildQueue() {
			return obq;
		}

		@Override
		protected long getDoneTime() {
			return doneTimsNs;
		}

		@Override
		protected void setDoneTime(long timeNs) {
			doneTimsNs = timeNs;
		}

		@Override
		protected int getTotalUnitsToBuildAmount() {
			return totalUnitToBuildAmount;
		}

		@Override
		protected void setTotalUnitsToBuildAmount(int newAmount) {
			totalUnitToBuildAmount = newAmount;
		}
		
	}
}

