package entities.buildings;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import entities.EntityFactory;
import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding.SetPeopleException;
import entities.stocks.CityFood;
import entities.stocks.CityPeople;
import entities.stocks.CityResource;
import entities.stocks.CityStockSet;
import entities.stocks.TaxStock;
import entities.units.CityArmy;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IExtractBuildingModel;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith( Enclosed.class )
public class ExtractCityBuildingTests {

	protected ExtractCityBuilding ecb;
	
	@Mock
	IExtractBuildingModel buildingModel;
	private final int buildingId = 10;
	
	@Mock
	ICivilization civil;
	
	@Mock
	UsersCityConfig myCity;
	//private int freePeople;
	
	@Mock
	private CityStockSet cityStocks;
	
	@Mock
	private CityPeople cityPeopleStock;
	
	@Before
	public void setUp() {
		ecb = new ExtractCityBuildingStub();
		ecb.init(myCity, new EntityFactoryForTests(), buildingId);
		Mockito.when(myCity.getMyCivilization()).thenReturn(civil);
		Mockito.when(civil.getById(buildingId, IExtractBuildingModel.class)).thenReturn(buildingModel);
		Mockito.when(myCity.getCityStocks()).thenReturn(cityStocks);
		Mockito.when(cityStocks.getPeopleStock()).thenReturn(cityPeopleStock);
		
	}
	
	@RunWith( MockitoJUnitRunner.class )
	public static class ExtractBuildingOnStartUp extends ExtractCityBuildingTests {
		
		@Before
		public void ExtractBuildingOnStartUpSetUp() {
			
		}
		@Test
		public void ZeroPeopleAfterBuildingCreation() throws Exception {
			assertEquals(0 , ecb.getPopulation());
		}
		@Test
		public void setPeople_onStartup1() throws Exception {
			setPeopleExpectException(40, 40, 30);
		}
		@Test
		public void setPeople_onStartup2() throws Exception {
			setPeopleOnStartup(40, 40, 100);
		}
		@Test
		public void setPeople_onStartup3() throws Exception {
			setPeopleOnStartup(30, 30, 30);
		}
		@Test
		public void setPeople_onStartup4() throws Exception {
			setPeopleOnStartup(0, 40, 100);
		}
		@Test
		public void setPeople_onStartup5() throws Exception {
			setPeopleExpectException(60, 40, 100);
		}
		@Test
		public void setPeople_onStartup6() throws Exception {
			setPeopleExpectException(40, 40, 30);
		}

	}
		protected void setPeopleOnStartup(int peopleToSet, int modelsLimitation, int availableInCity) throws SetPeopleException {
			
			mock_freePeopleAmount(availableInCity);
			mock_modelsMaxPeople(modelsLimitation);
			ecb.setPeople(peopleToSet);
			assertEquals(peopleToSet, ecb.getPopulation());
		}
		
		protected void setPeopleExpectException(int peopleToSet, int modelsLimitation, int available) throws SetPeopleException {
		
			mock_freePeopleAmount(available);
			mock_modelsMaxPeople(modelsLimitation);
			catchException(ecb).setPeople(peopleToSet);
			assertThat(caughtException()).isInstanceOf(SetPeopleException.class);
			assertEquals(0, ecb.getPopulation());
		}
	
	@RunWith( MockitoJUnitRunner.class )
	public static class ExtractBuildingWith20PeopleAlreadySet extends ExtractCityBuildingTests {
		
		@Before
		public void  ExtractBuildingWith20PeopleAlreadySetSetUp() throws SetPeopleException {
			setPeopleOnStartup(20, 60, 21);
			assertEquals(20, ecb.getPopulation());
		}
		/*
		 * test what happend if there are two recursrion event - obq and buildingBuild.
		 * lets say building build is shortest, the internal algorithm should determine which recursion event will occur first.
		 */
		@Ignore
		@Test
		public void testIfObjectBuildQueueAndBuildingBuild_BuildingBuildRecursionEventWillOccurfirst() {
			
		}
		@Test
		public void setPeople_usePeopleThatAreAlreadyInBuilding() throws SetPeopleException {
			ecb.setPeople(40);
			assertEquals(40, ecb.getPopulation());
		}
		
		@Test
		public void setPeople_ToLowPeopleAmount_shouldFail() throws Exception {
			catchException(ecb).setPeople(50);
			assertThat(caughtException()).isInstanceOf(SetPeopleException.class);
		}
		
		@Test
		public void setToPeopleToZero() throws Exception {
			ecb.setPeople(0);
			assertEquals(0, ecb.getPopulation());
		}
	}
	
		private void mock_freePeopleAmount(int peopleAmount) {
			Mockito.when(cityPeopleStock.getVisibleAmount()).thenReturn(peopleAmount);
		}
		
		private void mock_modelsMaxPeople(int p) {
			Mockito.when(buildingModel.getMaxPeople(myCity)).thenReturn(p);
		}
	
	@Ignore
	public static class ExtractCityBuildingStub extends ExtractCityBuilding {

		private int level;
		private int population;
		private int buildingId;
		private UsersCityConfig myCity;
		@Override
		public int getLevel() {
			return level;
		}

		@Override
		public int getBuildingId() {
			return buildingId;
		}

		@Override
		protected UsersCityConfig getCity() {
			return myCity;
		}

		@Override
		protected ObjectBuildQueue getObjectBuildQueue() {
			return null;
		}

		@Override
		protected BuildingBuild getBuildingBuild() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected void setLevel(int level) {
			this.level = level;
		}

		@Override
		public int getPopulation() {
			return population;
		}

		@Override
		protected void setPopulation(int population) {
			this.population = population;
		}

		@Override
		protected void setBuildingId(int buildingId) {
			this.buildingId = buildingId;
		}

		@Override
		protected void setMyCity(UsersCityConfig city) {
			this.myCity = city;
			
		}

		@Override
		protected void setObjectBuildQueue(ObjectBuildQueue obq) {
			
			
		}

		@Override
		protected void setBuildingBuild(BuildingBuild bb) {
			// TODO Auto-generated method stub
			
		}

	}
	
	private class EntityFactoryForTests extends EntityFactory {

		@Override
		public CityStockSet createCityStocks(UsersCityConfig city) {
			return cityStocks;
		}

		@Override
		public CityArmy createCityArmy() {
			return null;
		}

		@Override
		public ObjectBuildQueue createObjectBuildQueue(CityBuilding cb) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public BuildingBuild createBuildBuild(CityBuilding cb) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected ExtractCityBuilding instantiateExtractCityBuilding() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected CityBuilding instantiateCityBuilding() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected BuildingBuild instantiateBuildBuild() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected CityResource instantiateCityResource() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected TaxStock instantiateTaxStock() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected CityArmy instantiateCityArmy() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected ObjectBuildQueue instantiateObjectBuildQueue() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected CityStockSet instantiateCityStockSet() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected CityPeople instantiateCityPeople() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected CityFood instantiateCityFood() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected UsersCityConfig instantiateCity() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
}
