package entities.buildings;

import game_model_interfaces.IBuildingModel;
import game_model_interfaces.ICivilization;
import helpers.TimeTranslation;

/*
 * This class is strongly related with CityBuilding class.
 * Using methods when BuildingBuild is in-active is undefined.
 */
public abstract class BuildingBuild {
	
	protected abstract CityBuilding getCityBuilding();
	protected abstract void  setCityBuilding(CityBuilding myCity);
	protected abstract long getDoneTimeNs();
	protected abstract void setDoneTimeNs(long doneTime);
	private final int inactiveState = -1;
	public void init(CityBuilding cityBuilding) {
		setDoneTimeNs(inactiveState);
		setCityBuilding(cityBuilding);
	}
	
	public long getRemainingTime() {
		return TimeTranslation.msToNs(getBuildingsModelBuildTime()) - getDoneTimeNs();
	}
	
	public long getFinishTime_(){
		return getCityBuilding().getCityTime_() + TimeTranslation.msToNs(getBuildingsModelBuildTime());
	}
		private Integer getBuildingsModelBuildTime() {
			return getBuildingModel().getBuildTime(getCityBuilding().getCity());
		}
			private IBuildingModel getBuildingModel() {
				return getPlayersCivilization().getById( getBuildingId(), IBuildingModel.class);
			}
				private ICivilization getPlayersCivilization() {
					return getCityBuilding().getPlayersCivilization();
				}
				private int getBuildingId() {
					return getCityBuilding().getBuildingId();
				}
	
	public long getStartTime_(){
		return getCityBuilding().getCityTime_();
	}
	/*
	 * processing inactive buildingBuild is undefined. Always Check if is active.
	 */
	public void process(long buildTimsNs) {
		setDoneTimeNs(getDoneTimeNs() + buildTimsNs);
		if(getStartTime_() + getDoneTimeNs() >= getFinishTime_()) {
			getCityBuilding().incrementLevel();
			setDoneTimeNs(inactiveState);
		}
	}

	public boolean isActive() {
		return getDoneTimeNs() != inactiveState;
	}
	
	public void startUpgrade() {
		setDoneTimeNs(0);
	}
	
	public static class BuildingCurrentlyUpgradingException extends Exception{
		
	}

}
