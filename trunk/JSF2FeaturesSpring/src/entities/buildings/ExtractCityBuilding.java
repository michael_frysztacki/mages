package entities.buildings;

import entities.EntityFactory;
import entities.UsersCityConfig;
import game_model_interfaces.IExtractBuildingModel;

public abstract class ExtractCityBuilding extends CityBuilding{
	
	@Override
	public void init(UsersCityConfig city, EntityFactory factory, int buildingId) {
		super.init(city, factory, buildingId);
		setPopulation(0);
	}
	public abstract int getPopulation();
	protected abstract void setPopulation(int population);
	protected abstract void setBuildingId(int buildingId);

	public void setPeople(int count) throws SetPeopleException {
		if(!haveEnoughPlaceInBuilding(count) || !haveEnoughFreePeopleInCity(count))
			throw new SetPeopleException();
		setPopulation(count);
	}
	
		private boolean haveEnoughPlaceInBuilding(int peopleAmount) {
			int buildingPeopleLImit =  getModel().getMaxPeople(getCity());
			return peopleAmount <= buildingPeopleLImit;
		}
		private boolean haveEnoughFreePeopleInCity(int peopleAmount) {
			return peopleAmount <= availablePeopleInCity() + getPopulation();
		}
		private int availablePeopleInCity() {
			return getCity().getCityStocks().getPeopleStock().getVisibleAmount();
		}
		private IExtractBuildingModel getModel() {
			return getPlayersCivilization().getById(getBuildingId(), IExtractBuildingModel.class);
		}
	/*
	@Override
	public RecursionEvent getNextRecursionEvent() {
		if(super.getNextRecursionEvent() != null)
			return super.getNextRecursionEvent();
		if(getBuildingBuild().isActive())
			return new ExtractBuildingBuildRecursionEvent();
		return null;
	}
	*/
	public class SetPeopleException extends Exception {
	
	}
	/*
	public class ExtractBuildingBuildRecursionEvent extends RecursionEvent {

		@Override
		public Long recursionTimeNs() {			
			return getBuildingBuild().getRemainingTime();
		}

		@Override
		public void process() {
			getBuildingBuild().process(recursionTimeNs());
		}
		@Override
		public boolean equals(Object o) {
			if(!super.equals(o))return false;
			BuildingBuild bb = (BuildingBuild)o;
			return bb.equals(this);
		}

	}
	*/
	public boolean isFull() {
		return true;
	}
	
}
