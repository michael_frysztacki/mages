package entities;

import helpers.CollectionUtil;
import helpers.DefaultUnitGroupBinder;
import helpers.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import entities.stocks.TripStockSet;
import entities.units.CityArmy;
import entities.units.CityUnitGroup;
import entities.units.IntegerUnitGroup;
import entities.units.ReportArmy;
import entities.units.UnitGroupChanger;

public class FightReport extends Report{

	public FightReport(long nsTime_) {
		super(nsTime_);
		// TODO Auto-generated constructor stub
	}

	public FightReport()
	{
		super();
	}
	@Override
	public Object clone()
	{
		FightReport ret = (FightReport)super.clone();
		for(FightRound fr : this.getRounds())
		{
			FightRound newFr = new FightRound(fr);
			ret.getRounds().add(newFr);
		}
		//ret.setResult(this.getResult());
		ret.setNsTime_(this.getNsTime_());
		//ret.setFightPlace(this.getFightPlace());
		
		ret.ucc = this.ucc;
		return ret;
	}
	//private UsersCityConfig fightPlace = new UsersCityConfig();
	private TripStockSet tripStockSet;// = new TripStockSet();
	
	private List<FightRound> rounds = new ArrayList<FightRound>();
	private Result result;
	
	private ReportArmy getLastDeffenderState()
	{
		ReportArmy lastDefState = this.getRounds().get(this.getRounds().size()-1).getDeffender();
		return lastDefState;
	}
	
	ReportArmy getLastAggressorState()
	{
		ReportArmy lastAggState = this.getRounds().get(this.getRounds().size()-1).getAggressor();
		return lastAggState;
	}
	public boolean fightReportToCityArmy(CityArmy updateThis)
	{
		ReportArmy army = updateThis.getMyCity().getId() == this.getUcc().getId() ? this.getLastDeffenderState() : this.getLastAggressorState();
		Set<Pair<CityUnitGroup,IntegerUnitGroup>> joined = CollectionUtil.joinCollections(updateThis.getUnits(), army.getUnits(), new DefaultUnitGroupBinder());
		for(Pair<CityUnitGroup,IntegerUnitGroup> pair : joined)
		{
			int floorCount = (int)Math.floor(pair.first.getCount());
			int diff = floorCount - pair.second.getCount();
			UnitGroupChanger freeHand = new UnitGroupChanger();
			if(!freeHand.freeHand(pair.first, pair.first.getCount() - diff ))return false;
		}
		return true;
	}
	public List<FightRound> getRounds() {
		return rounds;
	}

	public void setRounds(List<FightRound> rounds) {
		this.rounds = rounds;
	}
	
	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	@Override
	public TripStockSet getTripStockSet() {
		return tripStockSet;
	}

	void setTripStockSet(TripStockSet tripStockSet) {
		this.tripStockSet = tripStockSet;
	}



//	public UsersCityConfig getFightPlace() {
//		return fightPlace;
//	}
//
//	public void setFightPlace(UsersCityConfig fightPlace) {
//		this.fightPlace = fightPlace;
//	}

	
}