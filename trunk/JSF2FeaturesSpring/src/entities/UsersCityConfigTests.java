package entities;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import map.CityPoint;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import entities.buildings.BuildingBuild;
import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.buildings.ObjectBuildQueue;
import entities.buildings.BuildingBuildTests.BuildingBuildStub;
import entities.buildings.CityBuildingTests.CityBuildingStub;
import entities.buildings.ExtractCityBuildingTests.ExtractCityBuildingStub;
import entities.buildings.ObjectBuildQueueTests.ObjectBuildQueueStub;
import entities.stocks.CityStockSet;
import entities.units.CityArmy;
import entities.units.CityArmyTests.CityArmyStub;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IExtractBuildingModel;


@RunWith(MockitoJUnitRunner.class)
public class UsersCityConfigTests {

	private UsersCityConfig city = new UsersCityConfigStub();
	
	@Mock
	UserConfig player;
	
	@Mock
	ICivilization playersCivil;

	@Mock
	CityPoint point;
	
	@Mock
	IBuildingModel buildingInCityModel;
	CityBuilding buildingInCity;
	
	@Mock
	IBuildingModel buildingInCityModel2;
	CityBuilding buildingInCity2;
	
	@Mock
	IExtractBuildingModel eBuildingInCityModel;
	ExtractCityBuilding eBuildingInCity;
	
	@Mock
	IExtractBuildingModel eBuildingInCityModel2;
	ExtractCityBuilding eBuildingInCity2;
	
	@Mock
	EntityFactory factory;
	
	@Before
	public void setup() {
		
		Mockito.when(factory.createCityBuilding(buildingInCityModel, city)).thenReturn(buildingInCity);
		Mockito.when(factory.createCityBuilding(buildingInCityModel2, city)).thenReturn(buildingInCity2);
		Mockito.when(factory.createExtractCityBuilding(eBuildingInCityModel, city)).thenReturn(eBuildingInCity);
		Mockito.when(factory.createCityBuilding(eBuildingInCityModel2, city)).thenReturn(eBuildingInCity2);
		List<IBuildingModel> civilizationBuildings = new ArrayList<IBuildingModel>();
		civilizationBuildings.add(buildingInCityModel);
		civilizationBuildings.add(buildingInCityModel2);
		List<IExtractBuildingModel> civilizationExtractBuildings = new ArrayList<IExtractBuildingModel>();
		civilizationExtractBuildings.add(eBuildingInCityModel);
		civilizationExtractBuildings.add(eBuildingInCityModel2);
		Mockito.when(playersCivil.getGameModelList(IBuildingModel.class, false, true)).thenReturn(civilizationBuildings);
		Mockito.when(playersCivil.getGameModelList(IExtractBuildingModel.class, true, true)).thenReturn(civilizationExtractBuildings);
		Mockito.when(player.getMyCivilization()).thenReturn(playersCivil);
		city.init(player, point, 0, factory);
	}
	
	@Test
	public void cityAfterCreation() throws Exception {
		
		assertEquals(2, city.getBuildings().size());
		assertEquals(2, city.getExtractBuildings().size());
	}
	
	@Test
	public void processCity() throws InterruptedException {
		
		
		while(true) {
			
			Thread.sleep(1000);
		}
		
	}
	
	@Ignore
	public class UsersCityConfigStub extends UsersCityConfig
	{
		private UserConfig uc;
		private Point point;
		private List<CityBuilding> cityBuildings;
		private List<ExtractCityBuilding> extractCityBuildings;
		private int taxPerHour;
		private CityArmy cityArmy;
		private long cityTimsNs_;
		private CityStockSet stocks;
		@Override
		public UserConfig getUserConfig() {
			return uc;
		}

		@Override
		public List<CityBuilding> getBuildings() {
			return cityBuildings;
		}

		@Override
		public int getTaxSatisfaction() {
			return taxPerHour;
		}

		@Override
		protected void setUserConfig(UserConfig player) {
			
			this.uc = player;
		}

		@Override
		protected void setCityArmy(CityArmy cityArmy) {
			this.cityArmy = cityArmy;
		}

		@Override
		protected void setCityTimeNs_(long cityTimeNs_) {
			
			this.cityTimsNs_ = cityTimeNs_;
		}

		@Override
		protected void setCityStocks(CityStockSet cityStocks) {
			this.stocks = cityStocks;
		}

		@Override
		protected void setTaxSatisfaction(int taxPerHour) {
			this.taxPerHour = taxPerHour;
			
		}

		@Override
		public CityStockSet getCityStocks() {
			return stocks;
		}

		@Override
		public long getCityTimeNs_() {
			return cityTimsNs_;
		}

		@Override
		protected void setBuildings(List<CityBuilding> buildings) {
			this.cityBuildings = buildings;
			
		}

		@Override
		public List<ExtractCityBuilding> getExtractBuildings() {
			return extractCityBuildings;
		}

		@Override
		protected void setExtractBuildings(List<ExtractCityBuilding> buildings) {
			this.extractCityBuildings = buildings;
		}

		@Override
		protected int getPointId() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		protected void setPointId(int pointId) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
}
