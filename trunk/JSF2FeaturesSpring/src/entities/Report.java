package entities;

import entities.stocks.TripStockSet;

public abstract class Report extends Message{
	
	public Report(long nsTime_)
	{
		super(nsTime_);
	}
	public Report()
	{
		super();
	}
	@Override
	public Object clone()
	{
		return super.clone();
	}
	public UsersCityConfig getUcc() {
		return ucc;
	}
	public void setUcc(UsersCityConfig ucc) {
		this.ucc = ucc;
	}
	protected UsersCityConfig ucc;
	
	public abstract TripStockSet getTripStockSet();
	
}




