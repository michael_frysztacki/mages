package entities;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import game_model_interfaces.Civilization;
import helpers.TimeTranslation;

import pojo.stocks.CityStock;
import pojo.stocks.CityStockSet;
import pojo.stocks.IntegerStock;
import pojo.stocks.TripStockSet;
import pojo.units.CityArmy;
import pojo.units.StationedArmy;
import pojo.units.TripArmy;

import junittest.old_tests.Testing;

import services.EconomyService;

import com.jsfsample.managedbeans.SpringJSFUtil;

import citys_interaction.Action;
import citys_interaction.FightComp;
import citys_interaction.RobberyAction;
import economycomputation.CityCompUtil;

public class RobberyTrip extends Trip{

	public RobberyTrip(UsersCityConfig startCity, UsersCityConfig endCity, long nsStartTime_,TripArmy army, TripStockSet resources,long msTravelTime)
	{
		super(startCity,endCity,nsStartTime_,army,resources, msTravelTime);
	}

	private FightReport fReport = null;
	public RobberyTrip(){};
//	@Override
//	public Object clone()
//	{
//		RobberyTrip ret = (RobberyTrip)super.clone();
//		if(this.getfReport() != null)
//		{
//			ret.fReport = (FightReport) this.getfReport().clone();
//		}
//		return ret;
//	}
	@Override
	public Long getReturnTime_()
	{
		long nsRet_ = this.getNsStartTime() + TimeTranslation.msToNs(this.getMsTravelTime()) * 2l;
		return nsRet_;
	}
	@Override
	public Trip createTripAfter(long nsNow_, boolean forAggressor){
		// TODO Auto-generated method stub
		//RobberyAction action = (RobberyAction)super.createAction(now);
		//RobberyTrip rTrip = (RobberyTrip)this.clone();
		FightReport fReport = null;
		TripStockSet currentTripResources = this.getTripStocks();
		Civilization aggCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(this.getStartCity().getUserConfig().getCivil());
		/*
		 * Problem zwiazany z r�wnym czasem policzenia rekurencji i zmiany ChangeTime
		 * Dla jedzenia - je�li chcemy policzyc stan miasta w czasie , kiedy akurat jakis surowiec 
		 * zmienia swoj stan,jedzenie zmienia swoj stan w nowo obliczonym miescie(uccAfter)
		 * Nie dotyczy to natomiast innych zmian�w stan�w jak np. wybudowanie budynku,
		 * zakonczenie budowy armi,
		 * atak miasta.
		 * Je�li poczliczymy stan miasta dokladnie w czasie wykonywania sie powyzej wymienionych akcji
		 * (opr�cz jedzenia) to te akcje nie beda uwzglednione.
		 */
		if(!this.isUpToDate(nsNow_))
		{
			if(forAggressor)
			{
				long nsTimeForEndCity =  this.getArriveTime_() - this.getEndCity().getNsTime_();
				SpringJSFUtil.getCityCompUtil().getFoodAT(this.getEndCity(),nsTimeForEndCity,new Integer(0));
				this.setEndCity(this.getEndCity());
			}
			fReport = SpringJSFUtil.getFightComp().computeFight(Collections.singleton(this.getTripArmy()) ,
					Collections.singleton(new StationedArmy(this.getEndCity().getMyArmy()))
					, 
					this.getArriveTime_());
			if(fReport.getResult() == Result.aggWon)
			{
				/*
				 * sprawdzamy ile jest calego jedzenia i surowcow u przeciwnika
				 */
				double sumOfAll = this.getEndCity().getCityStocks().getSum(CityStock.class);
				//sprawdzamy czy nasza pojemnosc armi jest w stanie pomie�ci� tyle, ile mozna zabrac podczas grabie�y
				int attackArmyCapacity = this.getTripArmy().getAllCapacity() - 
						SpringJSFUtil.getTripUtil().getFoodCountNeedForTravel(this.getMsTravelTime(),this.getTripArmy().getUnits(), this.getTripArmy().getOwner());
				
				double minus = aggCivil.getPercentRobberySteal();
				int availableResourcesForRobbery = (int)Math.floor(sumOfAll * (minus));
				/*
				 * teraz w zaleznosci od tego, czy wiecej jest surowcow dostepnych do zabrania,
				 * czy wiecej mamy pojemnosc armi, to przekazujemy tyle do howMuch.
				 * przekazujemy oczywiscie mniejsz� z tych dwoch wartosci
				 */
				int howMuch = availableResourcesForRobbery > attackArmyCapacity ? 
						attackArmyCapacity : availableResourcesForRobbery;
				Set<IntegerStock> stocks = SpringJSFUtil.getTripUtil().
						getFoodAndResourcesFromProvider(this.getEndCity().getCityStocks().getStocks(), howMuch );
				TripStockSet stolenStocks = TripStockSet.createTripStockSet(this.getEndCity().getCityStocks(), stocks);
//				System.out.print("\ndostepne jedzenie do zrabowania jedzenie: " + 
//						this.getEndCity().getMyFood() +
//						"dostepne surowce do zrabowania: " +
//						this.getEndCity().getMyResources() +
//						"\nhowMuch: " + howMuch + "\n"
//						+ "zrabowanie jedzenie:" +
//						stolenFood +
//						"zrabowane surowce:"+
//						stolenResources
//						);
				/*
				 * dodajemy skradzione surowce do surowcow armi
				 */
				fReport.setTripStockSet(stolenStocks);
			}
			this.setfReport(fReport);
		}
		if(!forAggressor)
		{
			boolean isSynchronized = isReportSynchronized(this.getEndCity());
			if(!isSynchronized)
			{
				TripStockSet.createTripStockSet( this.getEndCity().getCityStocks(), this.getfReport().getTripStockSet().getStocks());
				
				fReport.fightReportToCityArmy(this.getEndCity().getMyArmy());
				
			}
		}
		return this;
	}
	private boolean isReportSynchronized(UsersCityConfig ucc)
	{
		if(ucc.getNsTime_() > this.getfReport().getNsTime_())
		{
			return true;
		}
		else return false;
	}

	public FightReport getfReport() {
		return fReport;
	}

	public void setfReport(FightReport fReport) {
		this.fReport = fReport;
	}
	@Override
	public long getCurrentTime_() {
		/*
		 * je�eli walka sie jeszcze nieodby�a( nie ma wyenerowanego raportu), zwroc czas jako czas startowy
		 * w przeciwym wypadku zwroc czas przybycia armi do celu.
		 */
		if(getfReport() != null)
		{
			return this.getArriveTime_();
		}
		else return this.getNsStartTime();
	}
	/*
	 * wyja�nie zwiazene z '>=' czy '>'.
	 * Mysle ze powinno byc >. Po pierwsze zapobiega to zapetleniu kieduy
	 * this.getArriveTime().getTimeInMillis() == now.getTimeInMillis() (zwracane jest caly czas zero w petli rekurencyjnej).
	 * Ten problem - co jesli miasto ma stan w czasie now.getTimeInMillis() , jak zostanie wtedy policzona ta inreakcja:
	 * Poczatkowy stan miasta tak czy inaczej musi miec swoj pierwotny stan przed wyslaniem tripu przez atakujacego.
	 * Jak miasto broniace bedzie chcialo zapisac jaka� akcje, np zmiana pasku ludzi w miescie w czasie
	 * this.getArriveTime() , tzn wtedy kiedy this.getArriveTime().getTimeInMillis() == now.getTimeInMillis(), 
	 * to intarkcja zostanie wtedy policzona, nie zostanie ona pomini�ta. 
	 * Dlatego �e: tak jak wspomnia�em wy�ej, stan miasta musi przed na poczatku zapisany wczesniej, przed arrive time.
	 * I teraz chce zapisac miasto broniace w czasie arriveTime. metoda  
	 * public Double timeLeftToNextInteraction(GregorianCalendar now)
	 * zwroci mi jakis dodatni czas, i w rekurencji zostanie to obliczone.Przechodzimy do nastepnej rekurencji i
	 * mamy juz ta interakcje policzona.
	 */
	@Override
	public Long timeLeftToNextInteraction(long nsTime_)
	{	
		if(this.getArriveTime_() > nsTime_)
		{
			long ms = this.getArriveTime_() - nsTime_;
			return ms;
		}
		else return null;
	}
	/*
	 * (non-Javadoc)
	 * @see pojo.Trip#isUpToDate(long)
	 * metoda mowiaca, czy trip w bazie danych jest aktualny.
	 * Trip nie jest aktualny, je�li czas now przekroczy� lub jest r�wny czasowi walki, oraz nie posiadamy z tego raportu.
	 * 
	 */
	@Override
	protected boolean isUpToDate(long nsNow_) {
		
		/*
		 * sprawdzamy czy jestesmy przed czy po walce.
		 * Je�li jeste�my przed walk�, trip jest aktualny.
		 * UWAGA z '>=' : tutaj mo�emy sprawdza� czy czasy s� r�wne. Je�li s� r�wne, walka si� odbywa.
		 * Je�li przechodzimy w rekurecnji to czasy w�asnie b�d� r�wne.
		 * Je�li atakuj�cy wylicza zaraz bo bitwie raport, czas b�dzie >=(atakuj�cy nie musi wk�ada� do rekurecnji bitw�).
		 * Za pu�apk� z zap�tleniem w czasie 'createTripAfter' odpowiada metoda 'timeToNextInteraction'.
		 */
		if(nsNow_ >= this.getArriveTime_())
		{
			/*
			 * je�li po walce(lub w czasie walki), sprawdzamy czy mamy juz wygenerowany raport, je�li tak, trip jest 'upToDate'.
			 * Je�li nie mamy raportu, trip jest stary i zwracamy false (nie jest upToDate)
			 */
			if(this.getfReport() != null)
			{
				return true;
			}
			else return false;
		}
		return true;
	}
	@Override
	public boolean canRemove(boolean isAggressor,long nsNow_) {
		if(isAggressor)
		{
			/*
			 * jesli aktualny czas jest wiekszy badz rowny przybyciu armi mozna wykasowac trip.
			 */
			if(nsNow_ >= this.getReturnTime_())
			{
				return true;
			}
			else return false;
		}
		/*
		 * miasto broniace moze wykasowac trip dopiero po istatniej interakcji.
		 * w przypadku oblezenia bedzie to ostatnia walka.
		 * W przypadku robbery bedzie to po prostu po rabunku
		 */
		else
		{
			if(this.getfReport()!=null)return true;
			else return false;
		}
	}
	@Override
	public List<Report> getTripReports() {
		List<Report> ret = new ArrayList<Report>();
		ret.add(this.getfReport());
		return ret;
	}
	@Override
	public void syncReturnedTripWithCity() {
		FightReport tripReport = this.getfReport();
		tripReport.getTripStockSet().putTransportStockSet(this.getStartCity());
		tripReport.getLastAggressorState().
		this.getStartCity().getMyArmy().addArmy(tripReport.getLastAggressorState());
	}
	
}
