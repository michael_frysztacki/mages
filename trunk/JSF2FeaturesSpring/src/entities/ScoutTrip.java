package entities;

import helpers.TimeTranslation;

import java.lang.reflect.InvocationTargetException;
import java.util.GregorianCalendar;




import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.stocks.CityStockSet;
import entities.units.CityArmy;

/*
 * scout trip dziedziczy po robbery trip dlatego ze w zasadzie niczym sie nie rozni od robbery trip tzn
 * idziemy do miasta, na miejsciu robimy cos i od razu wracamy.
 * R�ni sie tylko tym co tam robimy i to musimy zaimpementowac w metodzie
 * createTripAfter.
 * createTripAfter powinno zwrocic trip razem z raportem szpiegowskim.
 */
public class ScoutTrip extends RobberyTrip{

	@Override
	public Trip createTripAfter(long nsNow_, boolean forAggressor){
		// TODO Auto-generated method stub
		//RobberyAction action = (RobberyAction)super.createAction(now);
		ScoutTrip rTrip = (ScoutTrip)this.clone();
		FightReport fReport = null;
		CityStockSet currentTripFood = rTrip.getFood();
		NotFoodSet_dead currentTripResources = rTrip.getResources();
		UsersCityConfig aggCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(rTrip.getStartCity().getUserConfig().getCivil());
		/*
		 * Problem zwiazany z r�wnym czasem policzenia rekurencji i zmiany ChangeTime
		 * Dla jedzenia - je�li chcemy policzyc stan miasta w czasie , kiedy akurat jakis surowiec 
		 * zmienia swoj stan,jedzenie zmienia swoj stan w nowo obliczonym miescie(uccAfter)
		 * Nie dotyczy to natomiast innych zmian�w stan�w jak np. wybudowanie budynku,
		 * zakonczenie budowy armi,
		 * atak miasta.
		 * Je�li poczliczymy stan miasta dokladnie w czasie wykonywania sie powyzej wymienionych akcji
		 * (opr�cz jedzenia) to te akcje nie beda uwzglednione.
		 */
		if(rTrip.isUpToDate(nsNow_))
		{
			if(forAggressor)
			{
				long hTimeForEndCity =  rTrip.getArriveTime_() - rTrip.getEndCity().getNsTime_();
				SpringJSFUtil.getCityCompUtil().getFoodAT(this.getEndCity(),hTimeForEndCity,new Integer(0));
				this.setEndCity(this.getEndCity());
			}
			fReport = SpringJSFUtil.getFightComp().computeFight(rTrip.getTripArmy(), rTrip.getEndCity().getArmy(), 
					this.getArriveTime_());
			if(fReport.getResult() == Result.aggWon)
			{
				/*
				 * sprawdzamy ile jest calego jedzenia i surowcow u przeciwnika
				 */
				double sumOfAll = this.getEndCity().getMyFood().getSum() + rTrip.getEndCity().getMyFood().getSum();
				//sprawdzamy czy nasza pojemnosc armi jest w stanie pomie�ci� tyle, ile mozna zabrac podczas grabie�y
				double attackArmyCapacity = 0;
				
				double minus = aggCivil.getPercentRobberySteal();
				double availableResourcesForRobbery = sumOfAll * (minus);
				/*
				 * teraz w zaleznosci od tego, czy wiecej jest surowcow dostepnych do zabrania,
				 * czy wiecej mamy pojemnosc armi, to przekazujemy tyle do howMuch.
				 * przekazujemy oczywiscie mniejsz� z tych dwoch wartosci
				 */
				double howMuch = availableResourcesForRobbery > attackArmyCapacity ? 
						attackArmyCapacity : availableResourcesForRobbery;
				NotFoodSet_dead stolenResources = new NotFoodSet_dead();
				CityStockSet stolenFood = new CityStockSet();
				SpringJSFUtil.getTripUtil().getFoodAndResourcesFromProvider(
						this.getEndCity().getMyResources(),
						this.getEndCity().getMyFood(), howMuch, stolenResources, stolenFood);
				System.out.print("\ndostepne jedzenie do zrabowania jedzenie: " + 
						this.getEndCity().getMyFood() +
						"dostepne surowce do zrabowania: " +
						this.getEndCity().getMyResources() +
						"\nhowMuch: " + howMuch + "\n"
						+ "zrabowanie jedzenie:" +
						stolenFood +
						"zrabowane surowce:"+
						stolenResources
						);
				/*
				 * dodajemy skradzione surowce do surowcow armi
				 */
				currentTripFood.addFoodSet(stolenFood);
				currentTripResources.addNotFoodSet(stolenResources);
				if(!forAggressor)
				{
					rTrip.getEndCity().getMyFood().subFoodSet(stolenFood);
					rTrip.getEndCity().getMyResources().subNotFoodSet(stolenResources);
				}
			}
			//this.setTripArmy(lastAggState);// ustaw stan wojska w tripie taki jak po bitwie
			rTrip.getEndCity().setArmy(fReport.getRounds().get(fReport.getRounds().size()-1).getDeffender());
			CityArmy lastAggState = fReport.getRounds().get(fReport.getRounds().size()-1).getAggressor();
			rTrip.setfReport(fReport);
			rTrip.setFood(currentTripFood);
			rTrip.setResources(currentTripResources);
			rTrip.setTripArmy(lastAggState);
			//action = new RobberyAction(this,fReport,now,returnTime,arriveTime,lastAggState,currentTripResources,currentTripFood);
		}
		return rTrip;
	}

}
