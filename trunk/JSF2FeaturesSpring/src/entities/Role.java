package entities;

public class Role {

	private String authority;
	private int id;
	
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	public int getId() {
		return id;
	}
	public void setId(int role_id) {
		this.id = role_id;
	}
}
