package entities;

import java.util.GregorianCalendar;

public abstract class Message implements Cloneable{

	protected long nsTime_;
	public long getNsTime_() {
		return nsTime_;
	}
	public void setNsTime_(long nsTime_) {
		this.nsTime_ = nsTime_;
	}
	private int id;
	
	@Override
	public Object clone()
	{
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}
	public Message(){};
	public Message(long nsTime_)
	{
		this.nsTime_ = nsTime_;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
