package entities;

import map.CityPoint;
import entities.buildings.BuildingBuild;
import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.buildings.ObjectBuildQueue;
import entities.stocks.CityFood;
import entities.stocks.CityPeople;
import entities.stocks.CityResource;
import entities.stocks.CityStockSet;
import entities.stocks.TaxStock;
import entities.units.CityArmy;
import framework.IStock;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IWorldDefinition;

public abstract class EntityFactory {

	public UserConfig createUserConfig(IWorldDefinition wd, String nick, String password) {
		return null;
	}
	public UsersCityConfig createCity(UserConfig owner, CityPoint point, long nsCityTime) {
		UsersCityConfig newCity = instantiateCity();
		newCity.init(owner, point, nsCityTime, this);
		return newCity;
	}
	public CityBuilding createCityBuilding(IBuildingModel buildingModel, UsersCityConfig city) {
		CityBuilding newCityBuilding = instantiateCityBuilding();
		newCityBuilding.init(city, this, buildingModel.getId());
		return newCityBuilding;
	}
	public ExtractCityBuilding createExtractCityBuilding(IExtractBuildingModel buildingModel, UsersCityConfig city) {
		ExtractCityBuilding newExtractCityBuilding = instantiateExtractCityBuilding();
		newExtractCityBuilding.init(city, this, buildingModel.getId());
		return newExtractCityBuilding;
	}
	public BuildingBuild createBuildBuild(CityBuilding cb) {
		BuildingBuild newBuildingBuild = instantiateBuildBuild();
		newBuildingBuild.init(cb);
		return newBuildingBuild;
	}
	public CityResource createCityResource(CityStockSet cityStockSet, String resourceId) {
		CityResource newCityResource = instantiateCityResource();
		newCityResource.init(resourceId, cityStockSet, getInitialStockAmount(cityStockSet, resourceId));
		return newCityResource;
	}
	public CityFood createCityFood(CityStockSet cityStockSet, String foodId) {
		CityFood newCityFood = instantiateCityFood();
		newCityFood.init(foodId, cityStockSet, getInitialStockAmount(cityStockSet, foodId));
		return newCityFood;
	}
	public TaxStock createTaxStock(CityStockSet cityStocks) {
		TaxStock newTaxStock = instantiateTaxStock();
		newTaxStock.init("", cityStocks, cityStocks.getCityInitializator().initialTaxSatisfaction());
		return newTaxStock;
	}
	public CityPeople createPeopleStock(CityStockSet cityStocks) {
		CityPeople cityPeople = instantiateCityPeople();
		cityPeople.init(cityStocks, getInitialStockAmount(cityStocks, cityStocks.getGameParameters().getPeopleStockId()));
		return cityPeople;
	}
	public CityStockSet createCityStocks(UsersCityConfig city) {
		CityStockSet newCityStockSet = instantiateCityStockSet();
		newCityStockSet.init(city, this);
		newCityStockSet.initStorage();
		return newCityStockSet;
	}
	public ObjectBuildQueue createObjectBuildQueue(CityBuilding cb) {
		ObjectBuildQueue newObq = instantiateObjectBuildQueue();
		newObq.init(cb);
		return newObq;
	}
		private int getInitialStockAmount(CityStockSet cityStocks, String stockId) {
			for(IStock s : cityStocks.getCityInitializator().initialCityStocks())
				if(s.getStockId().equals(stockId)) return s.getVisibleAmount();
			return 0;
		}
	
	// TODO
	public CityArmy createCityArmy() {
		//CityArmy newCityArmy = instantiateCityArmy();
		//newCityArmy.i
		return null;
	}
	protected abstract UsersCityConfig instantiateCity();
	protected abstract CityPeople instantiateCityPeople();
	protected abstract CityArmy instantiateCityArmy();
	protected abstract ObjectBuildQueue instantiateObjectBuildQueue();
	protected abstract CityStockSet instantiateCityStockSet();
	protected abstract CityResource instantiateCityResource();
	protected abstract CityFood instantiateCityFood();
	protected abstract TaxStock instantiateTaxStock();
	protected abstract BuildingBuild instantiateBuildBuild();
	protected abstract ExtractCityBuilding instantiateExtractCityBuilding();
	protected abstract CityBuilding instantiateCityBuilding();
}
