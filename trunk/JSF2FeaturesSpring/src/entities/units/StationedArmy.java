package entities.units;

import helpers.CollectionUtil;
import helpers.DefaultUnitGroupBinder;
import helpers.Pair;

import java.util.HashSet;
import java.util.Set;

import entities.UserConfig;


public class StationedArmy implements IArmy<IntegerUnitGroup>{

	private UserConfig owner;
	private Set<IntegerUnitGroup> units = new HashSet<IntegerUnitGroup>();

	public StationedArmy(CityArmy cityArmy)
	{
		this.owner = cityArmy.getOwner();
		for(CityUnitGroup cug : cityArmy.getUnits())
		{
			int floorCount = (int)Math.floor(cug.getCount());
			this.units.add(new IntegerUnitGroup(cug.getUnitId(), floorCount));
			cug.amount -= (double)floorCount;
		}
	}
	public StationedArmy(TripArmy tripArmy)
	{
		this.owner = tripArmy.getOwner();
		for(TripUnitGroup cug : tripArmy.getUnits())
		{
			this.units.add(new IntegerUnitGroup(cug.getUnitId(), cug.getCount()));
			cug.setCount(0);
		}
	}
	
	public boolean putHere(CityArmy cityArmy)
	{
		Set<Pair<CityUnitGroup, IntegerUnitGroup>> joined = CollectionUtil.joinCollections(cityArmy.getUnits(), this.getUnits(), new DefaultUnitGroupBinder());
		for(Pair<CityUnitGroup, IntegerUnitGroup> pair : joined )
		{
			pair.first.amount += (double)pair.second.getCount();
			pair.second.setCount(0);
		}
		return true;
	}
	
	public boolean putHere(TripArmy tripArmy)
	{
		Set<Pair<TripUnitGroup, IntegerUnitGroup>> joined = CollectionUtil.joinCollections(tripArmy.getUnits(), this.getUnits(), new DefaultUnitGroupBinder());
		for(Pair<TripUnitGroup, IntegerUnitGroup> pair : joined )
		{
			pair.first.count += pair.second.getCount();
			pair.second.setCount(0);
		}
		return true;
	}
	
	@Override
	public UserConfig getOwner() {
		return owner;
	}

	void setOwner(UserConfig owner) {
		this.owner = owner;
	}

	void setUnits(Set<IntegerUnitGroup> units) {
		this.units = units;
	}

	@Override
	public Set<IntegerUnitGroup> getUnits() {
		// TODO Auto-generated method stub
		return units;
	}

}
