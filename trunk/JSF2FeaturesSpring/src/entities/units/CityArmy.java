package entities.units;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;





import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.UserConfig;
import entities.UsersCityConfig;
import game_model_impl.WorldDefinition;
import game_model_interfaces.ILandUnitModel;
import game_model_interfaces.IUnitModel;

public class CityArmy implements IArmy<CityUnitGroup>{

	private Integer id;
	private UsersCityConfig myCity;
	private Set<CityUnitGroup> units = new HashSet<CityUnitGroup>();
	CityArmy(){};
//	public CityArmy(CityArmy army)
//	{
//		Iterator i = army.getUnits().iterator();
//		//this.id = army.getId();
//		while(i.hasNext())
//		{
//			CityUnitGroup ug = (CityUnitGroup)i.next();
//			CityUnitGroup newUg = new CityUnitGroup(ug,this);
//			this.addUnitGroup(newUg);
//		}
//		this.userConfig = army.getUserConfig();
//	}
	protected UserConfig getUc()
	{
		return this.getMyCity().getUserConfig();
	}
	@Override
	public String toString()
	{
		String ret = "";
		ret += "\n id:" + this.getId();
		for(CityUnitGroup ug : this.getUnits())
		{
			ret += ug.toString();
			ret += "\n";
		}
		return ret;
	}
	
	public CityArmy(UsersCityConfig forCity)
	{
		WorldDefinition ch = forCity.getUserConfig().getMyWorldDefinition();
		this.myCity = forCity;
		List<IUnitModel> units = ch.getCivilization(this.getMyCity().getUserConfig().getCivil()).getGameModelList(IUnitModel.class, true,true);
		for(IUnitModel b : units)
		{
			this.getUnits().add(new CityUnitGroup(b.getId(),0.0d,this));
		}
		this.myCity = forCity;
	}

	/*
	 * metoda zwraca ile jednostek jedzenia je wojsko na godzine
	 */
	public Double getEatPerHour() 
	{
		UsersCityConfig myCivil = this.getMyCity().getUserConfig().getMyWorldDefinition().getCivilization(this.getMyCity().getUserConfig().getCivil());
		double sum = 0.0d;
		for(CityUnitGroup ug : this.getUnits())
		{
			ILandUnitModel unit = myCivil.getById(ug.getUnitId(),ILandUnitModel.class);
			sum += unit.getEatPerHour(this.getMyCity().getUserConfig() ) * ug.getCount();
		}
		return sum;
	}
	public boolean isEmpty()
	{
		Iterator i = this.getUnits().iterator();
		while(i.hasNext())
		{
			CityUnitGroup ug = (CityUnitGroup)i.next();
			if(ug.getCount() != 0)return false;
		}
		return true;
	}
	public CityUnitGroup getUnitGroupById(int unitId)
	{
		for(CityUnitGroup ug : this.getUnits())
		{
			if(ug.getUnitId() == unitId)return ug;
		}
		return null;
	}

	public Set<CityUnitGroup> getUnits() {
		return units;
	}
	void setUnits(Set<CityUnitGroup> units) {
		this.units = units;
	}
	
	public UsersCityConfig getMyCity() {
		return myCity;
	}
	void setMyCity(UsersCityConfig myCity) {
		this.myCity = myCity;
	}
	public Integer getId() {
		return id;
	}
	void setId(Integer id) {
		this.id = id;
	}
	@Override
	public UserConfig getOwner() {
		// TODO Auto-generated method stub
		return this.getMyCity().getUserConfig();
	}
}
