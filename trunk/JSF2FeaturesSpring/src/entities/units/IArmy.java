package entities.units;

import java.util.Set;

import entities.UserConfig;


public interface IArmy<T extends IUnitGroup<?>> {

	public Set<T> getUnits();
	public UserConfig getOwner();
}
