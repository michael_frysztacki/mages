package entities.units;

import java.util.Set;

import entities.UserConfig;


public class ReportArmy implements IArmy<IntegerUnitGroup>{

	private int id;
	private Set<IntegerUnitGroup> units;
	private UserConfig owner;
	public ReportArmy(ReportArmy army)
	{
		this.owner = army.owner;
		for(IntegerUnitGroup unit : army.units )
		{
			IntegerUnitGroup local = new IntegerUnitGroup(unit);
			this.units.add(local);
		}
	}
	
	void setUnits(Set<IntegerUnitGroup> units) {
		this.units = units;
	}
	public int getId() {
		return id;
	}
	void setId(int id) {
		this.id = id;
	}
	@Override
	public UserConfig getOwner() {
		// TODO Auto-generated method stub
		return owner;
	}
	void setOwner(UserConfig owner) {
		this.owner = owner;
	}

	@Override
	public Set<IntegerUnitGroup> getUnits() {
		// TODO Auto-generated method stub
		return units;
	}



}
