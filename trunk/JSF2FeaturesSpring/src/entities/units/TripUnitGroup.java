package entities.units;

public class TripUnitGroup implements IUnitGroup<Integer>{

	private int id;
	private int unitId;
	int count;
	private TripUnitGroup(){};
	private TripUnitGroup(int unitId, int count){
		this.count = count;
		this.unitId = unitId;
	};
	static TripUnitGroup createTripUnitGroup(CityUnitGroup source, IUnitGroup<Integer> unit)
	{
		if(unit.getUnitId() != source.getUnitId() ) return null;
		if(source.getCount() < unit.getCount() )return null;
		source.amount  -= unit.getCount();
		return new TripUnitGroup(unit.getUnitId(),unit.getCount());
	}
	
	boolean putTripUnitGroup(CityUnitGroup putHere)
	{
		if(putHere.getUnitId() != this.unitId)return false;
		putHere.amount += this.count;
		this.count = 0;
		return true;
	}
	
	public int getId() {
		return id;
	}
	void setId(int id) {
		this.id = id;
	}
	@Override
	public int getUnitId() {
		// TODO Auto-generated method stub
		return this.unitId;
	}

	@Override
	public Integer getCount() {
		// TODO Auto-generated method stub
		return this.count;
	}
	
	void setCount(int count) {
		this.count = count;
	}

}
