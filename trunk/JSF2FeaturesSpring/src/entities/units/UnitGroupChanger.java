package entities.units;

/*
 * klasa umo�liwiaj�ca zmian� ilo�ci jednostek w dowolny spos�b.
 * Mo�e byc np wykorzsyatana przez Raport Wojenny w celu ustawienia ilo�ci jednostek na tak� jak w raporcie.
 * Je�li u�ywasz tej klasy, musisz by� ca�kowicie pewny co chcesz zrobi�.
 */
public class UnitGroupChanger {

	public boolean freeHand(CityUnitGroup ug, double value)
	{
		if(value < 0.0d)return false;
		ug.amount = value;
		return true;
	}

}
