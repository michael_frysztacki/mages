package entities.units;

public interface IUnitGroup<T extends Number> {

	public int getUnitId();
	public T getCount();
	
}
