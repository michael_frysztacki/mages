package entities.units;

/*
 * pomocnica kasa przedstawiająca jednostke o ilosci całkowietej. np przy wyswietlanie raportu.
 */
public class IntegerUnitGroup implements IUnitGroup<Integer>{

	private int unitId,count;
	void setUnitId(int unitId) {
		this.unitId = unitId;
	}
	void setCount(int count) {
		this.count = count;
	}
	IntegerUnitGroup(IUnitGroup<Integer> ug)
	{
		this.count = ug.getCount();
		this.unitId = ug.getUnitId();
	}
	IntegerUnitGroup(int unitId, int count)
	{
		this.count = count;
		this.unitId = unitId;
	}
	@Override
	public int getUnitId() {
		return unitId;
	}

	@Override
	public Integer getCount() {
		return count;
	}


}
