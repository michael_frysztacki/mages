package entities.units;

import game_model_interfaces.IUnitModel;
import helpers.CollectionUtil;
import helpers.DefaultUnitGroupBinder;
import helpers.Pair;

import java.util.HashSet;
import java.util.Set;



import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Trip;
import entities.UserConfig;
import entities.UsersCityConfig;


public class TripArmy implements IArmy<TripUnitGroup>{

	private int id;
	private Trip trip;
	private UserConfig owner;
	Set<TripUnitGroup> units = new HashSet<TripUnitGroup>();
	public int getId() {
		return id;
	}

	void setId(int id) {
		this.id = id;
	}

	public Trip getTrip() {
		return trip;
	}

	void setTrip(Trip trip) {
		this.trip = trip;
	}

	@Override
	public Set<TripUnitGroup> getUnits() {
		// TODO Auto-generated method stub
		return units;
	}
	public boolean putTripArmy(UsersCityConfig putHere)
	{
		Set<Pair<TripUnitGroup, CityUnitGroup>> joined = helpers.CollectionUtil.joinCollections(this.units, putHere.getMyArmy().getUnits(), new DefaultUnitGroupBinder());
		for(Pair<TripUnitGroup, CityUnitGroup> pair: joined)
		{
			if(!pair.first.putTripUnitGroup(pair.second))return false;
		}
		return true;
	}
	/*
	 * TODO - u�y� CollectionUtil.joinCollectionsAndPerformOpperation().
	 */
	
	public static TripArmy createTripArmy(CityArmy source, Set<? extends IUnitGroup<Integer>> units)
	{
		Set<?> joined = CollectionUtil.joinCollections(source.getUnits(), units , new DefaultUnitGroupBinder() );
		TripArmy ret = new TripArmy();
		for(Object unit : joined)
		{
			Pair<?,?> pair = (Pair<?, ?>) unit;
			
			ret.units.add( TripUnitGroup.createTripUnitGroup((CityUnitGroup)pair.second, (IUnitGroup<Integer>)pair.first)  );
		}
		return ret;
	}
	public int getAllCapacity()
	{
		UsersCityConfig myCivil = this.getOwner().getMyCivilization();
		int sum = 0;
		for(IUnitGroup<Integer> ug : this.getUnits() )
		{
			IUnitModel unit = myCivil.getById(ug.getUnitId(), IUnitModel.class );
			sum += unit.getCapacity(this.getOwner()) * ug.getCount();
		}
		return sum;
	}

	public UserConfig getOwner() {
		return owner;
	}

	void setOwner(UserConfig owner) {
		this.owner = owner;
	}
	

}
