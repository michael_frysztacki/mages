package entities.units;

import org.jscience.mathematics.number.Rational;

import dto.UnitDTO;


public class CityUnitGroup implements IUnitGroup<Rational>{

	Rational amount;
	private CityArmy myArmy;
	private int unitId;
	public String toString(){
		return "unitId:" + this.getUnitId() + " count:" + this.getCount() + "\n";
	}
	CityUnitGroup(int unitId, double count, CityArmy myArmy)
	{
		this.unitId = unitId;
		this.myArmy = myArmy;
		this.myArmy.getUnits().add(this);
		this.amount = count;
	}

	@Override
	public Rational getCount() {
		return amount;
	}
	public int getUnitId() {
		return this.unitId;
	}
	void setUnitId(int unitId) {
		this.unitId = unitId;
	}
	public void increaseUnitAmount(Rational r) {
		amount = amount.plus(r);
	}
	public CityArmy getMyArmy() {
		return myArmy;
	}
	void setMyArmy(CityArmy myArmy) {
		this.myArmy = myArmy;
	}
	

}
