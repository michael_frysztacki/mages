package citys_interaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import entities.UsersCityConfig;



/*
 * component springowy
 * action holder przetrzymuje akcje obliczone juz wczesniej, tak 
 * zeby nie liczy� obliczonych juz akcji wiecej razy.
 * Przyklad
 * Miasto A atakuje miasto B
 * Zaraz po ataku gracz miasta A odswieza przegladarke zeby zobaczyc wynik bitwy.
 * Dokonuje sie w tym momencie obliczanie wyniku bitwy.
 * Gracz B odswieza przegladarke 5 min pozniej i zeby nie liczyc tej samej bitwy drugi raz,
 * pobierana jest juz wczesniej policzona akcja przez gracza A z tej wlasnie klasy
 */
@Component("cityHolder")
public class CityHolder {

	private Map<Integer,UsersCityConfig> computedCities = new HashMap<Integer,UsersCityConfig>();
	public void addCityState(UsersCityConfig ucc)
	{
		computedCities.put(ucc.getId(), ucc);
	}
	public UsersCityConfig getCityState(int cityId)
	{
		return computedCities.get(cityId);
	}
	
	
	
}
