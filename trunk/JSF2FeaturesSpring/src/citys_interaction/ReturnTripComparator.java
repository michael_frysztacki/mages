package citys_interaction;

import java.util.Comparator;
import java.util.GregorianCalendar;

import entities.Trip;


public class ReturnTripComparator implements Comparator<Trip>{

	public ReturnTripComparator(){};
	@Override
	public int compare(Trip arg0, Trip arg1) {
	      return (arg0.getReturnTime_() < arg1.getReturnTime_() ?
	    		  -1 : (arg0.getReturnTime_()==arg1.getReturnTime_() ? 0 : 1));
	}
}
