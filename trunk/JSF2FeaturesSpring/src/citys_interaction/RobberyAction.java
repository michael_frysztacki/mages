package citys_interaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;



import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.FightReport;
import entities.Trip;
import entities.UsersCityConfig;
import entities.stocks.CityStockSet;
import entities.stocks.TripStock;
import entities.units.CityArmy;
import entities.units.CityUnitGroup;


public class RobberyAction extends Action{

	public RobberyAction(Trip trip,FightReport fightReport,GregorianCalendar actionCal ,GregorianCalendar returnTime, GregorianCalendar arriveTime,CityArmy currArmy ,NotFoodSet_dead nfs, CityStockSet fs)
	{
		super(trip,actionCal,returnTime,arriveTime,currArmy,nfs,fs);
		this.fightReport = fightReport;
	}
	public FightReport getFightReport() {
		return fightReport;
	}
	private FightReport fightReport = new FightReport();
	
	
}