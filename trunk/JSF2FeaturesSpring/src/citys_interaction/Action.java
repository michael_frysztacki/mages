package citys_interaction;

import java.util.GregorianCalendar;

import entities.Trip;
import entities.stocks.CityStockSet;
import entities.units.CityArmy;



/*
 * klasa odpowiadaj�ca za ca�� akcje okreslonego tripu
 * Trzyma ona takie zmienne jak jedzenie , surowce i armie w aktualnym czasie tripu
 * W klasie trip nie mozemy zmieniac armi, surowcow i jedzenia w zaleznosci od rozegranej 
 * bitwy, dlatego ze potrzbujemy informacji poczatkowych o tripe tzn. ile armi wyruszylo i z jakimi surowcami 
 * ia jakim jedzeniem. Potrzebujemy tego w rekurencji.
 */
public abstract class Action {

	protected Trip trip;
	private GregorianCalendar returnTime;
	private GregorianCalendar arriveTime;
	/*
	 * czas kiedy zosta�a policzona akcja
	 */
	private GregorianCalendar actionCalculatedTime;
	private NotFoodSet_dead currentTripResources;
	private CityStockSet currentTripFood;
	private CityArmy currentArmy;
	public int getId()
	{
		return trip.getId();
	}
	public GregorianCalendar getStartTime()
	{
		return trip.getStartTime();
	}
	public Action(){};
	public Action(Trip trip,GregorianCalendar actionCal ,GregorianCalendar returnTime, GregorianCalendar arriveTime,CityArmy currArmy,NotFoodSet_dead nfs,CityStockSet fs)
	{
		this.actionCalculatedTime = actionCal;
		this.currentArmy = currArmy;
		this.setCurrentTripFood(fs);
		this.setCurrentTripResources(nfs);
		this.arriveTime = arriveTime;
		this.returnTime = returnTime;
		this.trip = trip;
	}
	public Trip getTrip() {
		return trip;
	}
	public GregorianCalendar getArriveTime()
	{
		return arriveTime;
	}
	public GregorianCalendar getReturnTime() {
		return returnTime;
	}
	public NotFoodSet_dead getCurrentTripResources() {
		return currentTripResources;
	}
	public void setCurrentTripResources(NotFoodSet_dead currentTripResources) {
		this.currentTripResources = currentTripResources;
	}
	public CityStockSet getCurrentTripFood() {
		return currentTripFood;
	}
	public void setCurrentTripFood(CityStockSet currentTripFood) {
		this.currentTripFood = currentTripFood;
	}
	public CityArmy getCurrentArmy() {
		return currentArmy;
	}
	public void setCurrentArmy(CityArmy currentArmy) {
		this.currentArmy = currentArmy;
	}
	public GregorianCalendar getActionCalculatedTime() {
		return actionCalculatedTime;
	}
}
