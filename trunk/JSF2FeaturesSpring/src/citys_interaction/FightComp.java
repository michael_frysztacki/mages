package citys_interaction;

import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import entities.FightReport;
import entities.FightRound;
import entities.Result;
import entities.UsersCityConfig;
import entities.units.CityArmy;
import entities.units.CityUnitGroup;
import entities.units.StationedArmy;
import entities.units.TripArmy;
import game_model_impl.CommonCivilization;
import game_model_impl.WorldDefinition;



@Component("fightComp")
public class FightComp {

	@Autowired WorldDefinition ch;
	
	public void setCh(WorldDefinition ch) {
		this.ch = ch;
	}
	public FightReport computeFight(Set<TripArmy> aggressors, Set<StationedArmy> defenders, long nsFightTime_)
	{
		FightReport ret = new FightReport();
		ret.setNsTime_(nsFightTime_);
		UsersCityConfig defendersCivil = ch.getCivilization(defender.getUserConfig().getCivil());
		CityArmy roundAgg = new CityArmy(aggressor);
		roundAgg.setId(null);
		CityArmy roundDef = new CityArmy(defender);
		roundDef.setId(null);
		for(int i=0; i < defendersCivil.getMaxFightRounds() ; i++)
		{
			FightRound fr = new FightRound(roundAgg,roundDef);
			//fr.setAggressor(roundAgg);
			//fr.setDeffender(roundDef);
			ret.getRounds().add(fr);
			CityArmy nextRoundAgg = new CityArmy(roundAgg);
			CityArmy nextRoundDef = new CityArmy(roundDef);
			if(nextRoundAgg.isEmpty())
			{
				ret.setResult(Result.defWon);
				break;
			}
			else if(nextRoundDef.isEmpty())
			{
				ret.setResult(Result.aggWon);
				break;
			}
			computeRound(nextRoundAgg, nextRoundDef);
			roundAgg = new CityArmy(nextRoundAgg);
			roundDef = new CityArmy(nextRoundDef);
		}
		if( !roundAgg.isEmpty() && !roundDef.isEmpty() )
		{
			ret.setResult(Result.remis);
		}
		return ret;
	}
	
	/*
	 * TODO - zrobi� algorytm na bitwy
	 * listy agg i def musimy modyfikowac i zwrocic w nich wynik
	 * na razie jest zrobiony prosty algorytm na rundy
	 * zawsze wygrywa ten , kogo jest wiecej.
	 * ten kto ma wiecej zolnierzy wygrywa w pierwszej rundzie zabijajac wszytkich 
	 * zolnierzy przeciwnika.
	 * jak zo�nierzy jest tyle samo to jest remis, nikt nie zabija nikogo
	 */
	public void computeRound(CityArmy agg , CityArmy def)
	{
		int aggSum = 0;
		for(CityUnitGroup ug : agg.getUnits())
		{
			aggSum += ug.getCount();
		}
		int defSum = 0;
		for(CityUnitGroup ug : def.getUnits())
		{
			defSum += ug.getCount();
		}
		if(aggSum < defSum)
		{
			for(CityUnitGroup ug : agg.getUnits())
			{
				ug.setCount(0);
			}
		}
		else
		{
			for(CityUnitGroup ug : def.getUnits())
			{
				ug.setCount(0);
			}
		}
	}
	
}
