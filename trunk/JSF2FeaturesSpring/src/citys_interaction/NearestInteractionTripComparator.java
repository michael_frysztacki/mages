package citys_interaction;

import java.util.Comparator;
import java.util.GregorianCalendar;

import entities.Trip;
import entities.stocks.CityResource;


public class NearestInteractionTripComparator implements Comparator<Trip>{

	private NearestInteractionTripComparator(){};
	public NearestInteractionTripComparator(long nsNow_)
	{
		this.nsNow_ = nsNow_;
	}
	private long nsNow_;
	@Override
	public int compare(Trip arg0, Trip arg1) {
	      return (arg0.timeLeftToNextInteraction(nsNow_)<arg1.timeLeftToNextInteraction(nsNow_) ?
	    		  -1 : (arg0.timeLeftToNextInteraction(nsNow_)==arg1.timeLeftToNextInteraction(nsNow_) ? 0 : 1));
	}

	
}
