package citys_interaction;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import entities.FightReport;
import entities.Mission;
import entities.Trip;
import entities.units.CityUnitGroup;


public class ActionFactory {

	@Autowired FightComp fightComp;
	@Autowired ModelHolder modelHolder;
	public RobberyAction createAttackAction(Trip trip, GregorianCalendar now) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		RobberyAction ret = new RobberyAction();
		ret.setTrip(trip);
		Class deffenderModel = modelHolder.getModel(trip.getEndCity().getUserConfig());
		Method m_getCityLayers = null;
		m_getCityLayers = deffenderModel.getMethod("getCityLayers");
		List<CityLayer> defLayers = (List<CityLayer>)m_getCityLayers.invoke(null);
		long fightTime = trip.getStartTime().getTimeInMillis();
		for(CityLayer layer : defLayers)
		{
			if(layer.getOrder() == 1)
			{
				fightTime += trip.getTravelTime();
				if(now.getTimeInMillis() < fightTime)break;
				FightReport fReport = fightComp.computeFight((Set<IUnitGroup>)trip.getUnitsOnTrip(), (Set<IUnitGroup>)trip.getEndCity().getUnits(null), fightTime);
				ret.getFights().addEdgeAndReversedEdge(fReport);
			}
			else
			{
				fightTime += layer.getWaitTime();
				if(now.getTimeInMillis() < fightTime)break;
				FightReport lastFight = ret.getFights().get(ret.getFights().size()-1);
				Set agg = lastFight.getAggressorStates().get(lastFight.getAggressorStates().size()-1);
				Set def = lastFight.getDefenderStates().get(lastFight.getDefenderStates().size()-1);
				FightReport fReport = fightComp.computeFight((Set<IUnitGroup>)agg, (Set<IUnitGroup>)def, fightTime);
				ret.getFights().addEdgeAndReversedEdge(fReport);
			}
		}
		return ret;
	}
}
