package framework;

public interface IStock {
	
	public int getVisibleAmount();
	public String getStockId();
}
