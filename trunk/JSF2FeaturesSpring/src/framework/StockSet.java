package framework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import game_model_impl.StockType;

public abstract class StockSet<T extends IStock> {

	protected abstract List<T> getStocks();
	@Override
	public String toString()
	{
		String ret = "";
		for(IStock stock : getStocks())
			ret += stock.getStockId() + ": " +stock.getVisibleAmount()+ "\n";
		return ret;
	}
	@SuppressWarnings("unchecked")
	protected <U extends IStock> List<U> getStocksByClass(Class<U> clazz)
	{
		List<U> ret = new ArrayList<U>();
		for(IStock cs : getStocks())
			if(clazz.isAssignableFrom(cs.getClass()))
				ret.add((U)cs);
		return Collections.unmodifiableList(ret);
	}
	public IStock getStockByType(String stockId)
	{
		for(IStock s : this.getStocks())
			if(s.getStockId()==stockId)return s;
		return null;
	}

}
