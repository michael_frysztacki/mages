package framework;

public class Casting {

	public static <T> T cast(Object obj, Class<T> clazz, boolean regardInherited){
		if(regardInherited) {
			if(clazz.isAssignableFrom(obj.getClass()))
				return (T) obj;
		}
		else {
			if(clazz.getClass()==obj.getClass())
				return (T) obj;
		}
		return null;
	}
	
	public static <T> boolean canCast(Object obj, Class<T> clazz, boolean regardInherited){
		if(regardInherited) {
			if(clazz.isAssignableFrom(obj.getClass()))
				return true;
		}
		else {
			if(clazz.getClass()==obj.getClass())
				return true;
		}
		return false;
	}
}
