package framework;

public class IntegerStock implements IStock {
	protected int amount;
	protected String stockId;
	public IntegerStock(String stockId, int amount) {
		this.stockId = stockId;
		this.amount = amount;
	}
	
	@Override
	public int getVisibleAmount() {
		return amount;
	}

	@Override
	public String getStockId() {
		return stockId;
	}

}
