package map;

import java.util.List;

public interface UserGraph {

	/*
	 * this method is called as soon, as at leas one of these parameters are known.
	 */
	public void doPrecalculations(List<Unit> units, Integer startPointId, Integer endPoint, List<Integer> blockedGroups);
	/*
	 * getting the result of 'doPrecalculations' method. It is guaranteed,that before calling this method,
	 * the doPrecalculation method is called with all three parameters which are not-null. So there is no need to checking
	 * if doPrecalculation() has already performed the calculations. Just return the result and assume that the result is calculated.
	 */
	public List<Point> getShortestPath();
}
