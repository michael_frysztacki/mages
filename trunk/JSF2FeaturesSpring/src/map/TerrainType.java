package map;

public enum TerrainType {
	Water, Plain, Steppe, Jungle, Mountain, Desert
}
