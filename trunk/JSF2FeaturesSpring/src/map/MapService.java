package map;

public interface MapService {

	public WorldMap getWorldMap(int worldId);
	public int getGroupIdOfPolarPoints();
	
}
