package map;

public interface CapitalPoint extends CityPoint {
	public String getCityName(String languageId);
	public int getCivilId();
}
