package map;

public interface CityPoint extends Point {
	public int getLodeFactor(String stockId);
	public boolean isWaterCity();
}
