package map;

import java.util.List;

public interface WorldMap {
	
	CityPoint getCityPoint(int pointId);
	CapitalPoint randomizePoint(List<Integer> civilsIds);
	UserGraph createInitialGraph(int radiusKm, int capitalPointId); // thread
	UserGraph rebuildGraph(int previousRadiusKm, UserGraph oldPlayersGraph, int newRadiusKm, List<Integer> pointIds); // thread

}
