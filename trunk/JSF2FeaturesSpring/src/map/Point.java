package map;

/*
 * do we realy need this interface ?
 */
public interface Point {

	public int getPointId();
	public double getLatitude();
	public double getLongtitude();
	
}
