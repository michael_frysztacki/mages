package com.jsfsample.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;
import com.jsfsample.model.Bike;
import com.jsfsample.services.BikeDataProvider;

/* 
 * Spring defined service for performing all operations on bikes:
 * - loading all bikes from given category
 * - loading selected bike details
 * - adding new bike
 * Bikes object are mocked up in this service. In real application bikes objects will be retrieved from repository (database) by DAOs.  
 */

@Service("bikeDataProvider")
public class BikeDataProviderImpl implements BikeDataProvider {

	private List<Bike> bikes;
	private Integer currentBikeId;
	
	@SuppressWarnings("unused")
	@PostConstruct
	private void prepareData(){
		bikes = new ArrayList<Bike>();
		
		// MTB
		Bike mtb1 = new Bike();
		mtb1.setId(1);
		mtb1.setName("Kellys Mobster");
		mtb1.setDescription("Kellys Mobster, lorem ipsut...");
		mtb1.setPrice(6500);
		mtb1.setCategory(1);
		
		
		Bike mtb2 = new Bike();
		mtb2.setId(2);
		mtb2.setName("Scott Scale");
		mtb2.setDescription("Scott Scale, lorem ipsut...");
		mtb2.setPrice(18900);
		mtb2.setCategory(1);
		
		Bike mtb3 = new Bike();
		mtb3.setId(3);
		mtb3.setName("Author Magnum");
		mtb3.setDescription("Author Magnum, lorem ipsut...");
		mtb3.setPrice(17200);
		mtb3.setDiscountPrice(15500);
		mtb3.setCategory(1);
		
		// Trekking
		Bike trek1 = new Bike();
		trek1.setId(4);
		trek1.setName("Giant Accend");
		trek1.setDescription("Giant Accend, lorem ipsut...");
		trek1.setPrice(5000);
		trek1.setDiscountPrice(4600);
		trek1.setCategory(2);
		
		Bike trek2 = new Bike();
		trek2.setId(5);
		trek2.setName("Merida Freeway");
		trek2.setDescription("Merida Freeway, lorem ipsut...");
		trek2.setPrice(2400);
		trek2.setDiscountPrice(2100);
		trek2.setCategory(2);
		
		Bike trek3 = new Bike();
		trek3.setId(6);
		trek3.setName("Mbike Massive");
		trek3.setDescription("Mbike Massive, lorem ipsut...");
		trek3.setPrice(1900);		
		trek3.setCategory(2);
		
		// Cross
		Bike cross1 = new Bike();
		cross1.setId(7);
		cross1.setName("Giant Roam XR 1");
		cross1.setDescription("Giant Roam XR 1, lorem ipsut...");
		cross1.setPrice(3900);		
		cross1.setCategory(3);
		
		Bike cross2 = new Bike();
		cross2.setId(8);
		cross2.setName("Cannondale Quick Cx");
		cross2.setDescription("Cannondale Quick Cx, lorem ipsut...");
		cross2.setPrice(4999);		
		cross2.setCategory(3);
		
		Bike cross3 = new Bike();
		cross3.setId(9);
		cross3.setName("Cube Cross");
		cross3.setDescription("Cube Cross, lorem ipsut...");
		cross3.setPrice(4500);		
		cross3.setDiscountPrice(4200);
		cross3.setCategory(3);
		
		bikes.add(mtb1);
		bikes.add(mtb2);
		bikes.add(mtb3);
		bikes.add(trek1);
		bikes.add(trek2);
		bikes.add(trek3);
		bikes.add(cross1);
		bikes.add(cross2);
		bikes.add(cross3);
		currentBikeId = 9; // 9 bikes by default
		
	}
	
	/* (non-Javadoc)
	 * @see com.jsfsample.services.impl.BikeDataProvider#getBikesByCategory(java.lang.Integer, boolean)
	 */
	public List<Bike> getBikesByCategory(Integer categoryId, boolean onlyWithDiscount) {
		
		List<Bike> result = new ArrayList<Bike>();
		
		for (Bike bike : bikes) {
			if (bike.getCategory().equals(categoryId)) {
				if (onlyWithDiscount) {
					if (bike.getDiscountPrice()!=null) {
						result.add(bike);	
					}					
				} else {
					result.add(bike);
				}
			}
		}		
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jsfsample.services.impl.BikeDataProvider#getBikeById(java.lang.Integer)
	 */
	public Bike getBikeById(Integer id){
		Bike result = null;
		for (Bike bike : bikes) {
			if (bike.getId()==id) {
				result = bike;
			}
		}
		return result;
	}

	@Override
	public void add(Bike newBike)
	{
		newBike.setId(currentBikeId++);
		bikes.add(newBike);
	}
	
}
