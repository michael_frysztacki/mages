package com.jsfsample.application.impl;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import services.LoginService;

import com.jsfsample.managedbeans.LoginBean;


@Service("authenticationService")
public class AuthenticationServiceImpl implements com.jsfsample.application.AuthenticationService {

	@Resource(name = "authenticationManager")
	private AuthenticationManager authenticationManager; // specific for Spring Security

	@Autowired LoginService loginService;
	@Override
	public boolean login(String username, String password) {
		try {
			Authentication authenticate = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(
							username, password));
			if (authenticate.isAuthenticated()) {
				SecurityContextHolder.getContext().setAuthentication(
						authenticate);
				return true;
			}
		} catch (AuthenticationException e) {			
		}
		return false;
	}
	
	@Override
	public void logout() {
		SecurityContextHolder.getContext().setAuthentication(null);
		LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance().
				getExternalContext().getSessionMap().get("loginBean");
		//currentUser.unauthenticate();
	}

	
}
