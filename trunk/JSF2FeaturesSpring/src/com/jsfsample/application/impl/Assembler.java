package com.jsfsample.application.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import entities.Role;
import entities.UserConfig;


@Service("assembler")
public class Assembler {

  @Transactional(readOnly = true)
  User buildUserFromUserEntity(UserConfig userEntity) {

    String username = userEntity.getNick();
    String password = userEntity.getPass();
    Boolean enabled = userEntity.isEnabled();
    Boolean accountNonExpired = userEntity.isEnabled();
    Boolean credentialsNonExpired = userEntity.isEnabled();
    Boolean accountNonLocked = userEntity.isEnabled();

    Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
    Set roles = userEntity.getUserRoles();
    for (Object role : roles) {
      authorities.addEdgeAndReversedEdge(new GrantedAuthorityImpl(((Role)role).getAuthority()));
    }

    User user = new User(username, password, enabled,
      accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    return user;
  }
}