package com.jsfsample.managedbeans;

import game_model_interfaces.initializations.DefaultCityInitializator;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import junittest.model_test.text.world_definition_naming.TestCityInitializator;

import dto.ConcreteResourceDTO;
import entities.Civil;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.stocks.CityResource;

import services.CityService;
import services.EconomyService;
import services.LoginService;
import test.main;

@ManagedBean(name = "gameBean")
@SessionScoped
public class GameBean{
	
	@ManagedProperty(value = "#{testBean}")
	private InitBean test;
	
	public InitBean getTest() {
		return test;
	}
	public void setTest(InitBean test) {
		this.test = test;
	}
	@ManagedProperty(value = "#{economyService}")
	private EconomyService economyService;
	
	public EconomyService getEconomyService() {
		return economyService;
	}
	public void setEconomyService(EconomyService economyService) {
		this.economyService = economyService;
	}
	@ManagedProperty(value = "#{loginService}")
	private LoginService loginService;
	
	public LoginService getLoginService() {
		return loginService;
	}
	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}
	@ManagedProperty(value = "#{cityService}")
	private CityService cityService;
	
	public CityService getCityService() {
		return cityService;
	}
	public void setCityService(CityService cityService) {
		this.cityService = cityService;
	}
	private int cityId;
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	public int getCityId()
	{
		return cityId;
	}
	@PostConstruct
	public void loadUser()
	{
		
		this.cityId = 1;
	}
	
}
