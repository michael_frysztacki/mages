package com.jsfsample.managedbeans;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.GregorianCalendar;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;

import dto.DetailedExtractBuildingDTO;
import dto.DetailedBuildingDTO;
import dto.DetailedUnitDTO;
import dto.UnitDTO;

import services.BuildingService;
import services.EconomyService;
import services.UnitService;

import com.jsfsample.model.Bike;
import com.jsfsample.services.BikeDataProvider;

import dto.ExtractBuildingDTO;
import entities.UsersCityConfig;
import entities.units.CityUnitGroup;

@ManagedBean(name="cityBuildingDetails")
@ViewScoped
public class BuildingDetailBean implements Serializable{

	private Integer buildingId;
	private DetailedBuildingDTO cityBuilding;
	@PostConstruct
	public void pre()
	{
		System.out.print("\n utworzono extract building detail view\n");
	}
	private static EconomyService economyService() {
        return SpringJSFUtil.getBean("economyService");
    }
	
	private static GameBean gameBean() {
        return SpringJSFUtil.getBean("gameBean");
    }
	
	private static UnitService unitService() {
        return SpringJSFUtil.getBean("unitService");
    }
	
	private static BuildingService buildingService() {
        return SpringJSFUtil.getBean("buildingService");
    }
	
	public void loadCityBuilding(int id) throws Exception
	{
		long msNow = System.currentTimeMillis();
		UsersCityConfig uccAfter = economyService().getEconomy(gameBean().getCityId(), msNow);
		setCityBuilding(buildingService().getCityBuilding(uccAfter, id));
	}
	
	public void upgradeBuilding() throws Exception
	{
		System.out.print("upgrade building");
		long msNow = System.currentTimeMillis();
		int cityId = gameBean().getCityId();
		buildingService().upgradeBuilding(this.cityBuilding.getId(), cityId, msNow);
	}

	public Integer getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(Integer buildingId) {
		this.buildingId = buildingId;
	}
	public DetailedBuildingDTO getCityBuilding() {
		return cityBuilding;
	}

	public void setCityBuilding(DetailedBuildingDTO cityBuilding) {
		this.cityBuilding = cityBuilding;
	}

}
