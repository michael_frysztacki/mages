package com.jsfsample.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.google.gson.Gson;

import dto.ConcreteFoodDTO;
import dto.ConcreteResourceDTO;
import entities.UsersCityConfig;
import entities.stocks.CityResource;
import entities.stocks.TripStock;


import services.EconomyService;

@ManagedBean(name = "requestBean")
@RequestScoped
public class RequestBean implements Serializable{

	private double hTime;
	@ManagedProperty(value = "#{economyService}")
	private EconomyService economyService;
	
	private String eco; 
	
	public void setEco(String eco) {
		this.eco = eco;
	}

	public void setEconomyService(EconomyService economyService) {
		this.economyService = economyService;
	}

	@ManagedProperty(value = "#{gameBean}")
	private GameBean gameBean;
	
	UsersCityConfig cityState;
	public GameBean getGameBean() {
		return gameBean;
	}

	@ManagedProperty(value= "#{economyBean}")
	private EconomyBean economyBean;

	public void setEconomyBean(EconomyBean economyBean) {
		this.economyBean = economyBean;
	}

	public void setGameBean(GameBean gameBean)
	{
		this.gameBean = gameBean;
	}
	private Map<String,Object> ecoVariables =  new HashMap<String,Object>();
	
	public Map<String,Object> getEcoVariables()
	{
		return ecoVariables;
	}
	public void loadEcoVars() throws Exception
	{
		long msNow = System.currentTimeMillis();
		long start = System.currentTimeMillis();
		this.cityState = economyService.getEconomy(this.getGameBean().getCityId(),msNow,ecoVariables);
		long stop = System.currentTimeMillis();
		System.out.print("\n czas policzenia miasta w loadEcoVars (selecty + rekurencja):" + (stop - start) + "ms\n");
		
		Gson gson = new Gson();
		this.eco =  gson.toJson(ecoVariables);
		
		System.out.print("\neco vars loaded\n" + this.eco);
		
	}
	public void loadCityState() throws Exception
	{
		boolean ajaxRequest = FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest();
		if(!ajaxRequest)
		{
			long msNow = System.currentTimeMillis();
			
			this.cityState = economyService.getEconomy(this.getGameBean().getCityId(),msNow,ecoVariables);
			Gson gson = new Gson();
			this.eco =  gson.toJson(ecoVariables);
		}
	}

	public String getEco() {
		return eco;
	}

}
