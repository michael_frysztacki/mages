package com.jsfsample.managedbeans;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.PhaseEvent;


import org.primefaces.event.SlideEndEvent;



import services.BuildingService;
import services.EconomyService;

import dto.ConcreteFoodDTO;
import dto.ConcreteResourceDTO;
import dto.FoodConsumptionDTO;
import entities.UsersCityConfig;
import entities.stocks.TripStock;
import entities.units.CityUnitGroup;
import game_model_impl.StockType;
import game_model_interfaces.IUnitModel;

@ManagedBean(name = "ecoBean")
@RequestScoped
public class EconomyBean {

	@ManagedProperty(value = "#{gameBean}")
	private GameBean gameBean;
	@ManagedProperty(value = "#{economyService}")
	private EconomyService economyService;
	@ManagedProperty(value = "#{buildingService}")
	private BuildingService buildingService;

	public BuildingService getBuildingService() {
		return buildingService;
	}

	public void setBuildingService(BuildingService buildingService) {
		this.buildingService = buildingService;
	}

	private double freePeopleCount;
	private double civilian;
	private double workPeople;
	private double trainPeople;
	
	private double hiredPeople;
	private double satisfaction;
	private double birthRate;
	private int currentPeopleLimit;
	private int newPeopleCount;
	
	
	private List<FoodConsumptionDTO> consumption = new ArrayList<FoodConsumptionDTO>();
	
	
	public List<FoodConsumptionDTO> getConsumption() {
		return consumption;
	}

	public void setConsumption(List<FoodConsumptionDTO> consumption) {
		this.consumption = consumption;
	}
	public void test() {
		FacesContext facesContext = FacesContext.getCurrentInstance().getCurrentInstance();
        Map params = facesContext.getExternalContext().getRequestParameterMap();
        boolean isAjaxRequest = params.containsKey("ajaxSourceJQuery");
        
		System.out.print("\neconomy bean: test end, is ajax:\n" + isAjaxRequest);
	}
	public void testEnd(ActionEvent event) {
		System.out.print("\neconomy bean: test end\n");
	}
	public void onSlideEnd() {
		
		long msNow = System.currentTimeMillis();
		
		FacesContext facesContext = FacesContext.getCurrentInstance().getCurrentInstance();
		Map<String,String> requestMap = facesContext.getExternalContext().getRequestParameterMap();
		String s_buildingId = (String) requestMap.get("buildingId");
		String s_peopleC = (String) requestMap.get("peopleC");
		int buildingId = Integer.parseInt(s_buildingId);
		int peopleC = Integer.parseInt(s_peopleC);
		System.out.print("\neconomy bean: on slide end\n");
		UsersCityConfig uccAfter = null;
		try {
			long start = System.currentTimeMillis();
			uccAfter = economyService.getEconomy(gameBean.getCityId(), msNow);
			long stop = System.currentTimeMillis();
			System.out.print("\n czas policzenia rekurencji (selecty + rekurecnja):" + (stop - start) + "ms\n");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int peopleFloor = (int)Math.floor(uccAfter.getCityStocks().getPeopleStock().available());
		if(peopleC <= peopleFloor)
		{
			try {
				long start = System.currentTimeMillis();
				buildingService.setPeopleInBuilding(uccAfter, buildingId, peopleC);
				long stop = System.currentTimeMillis();
				System.out.print("\n czas wsadzenia ludzi do budynku (merge, inserty):" + (stop - start) + "ms\n");
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
        FacesMessage msg = new FacesMessage("Slide Ended", "Value: " + peopleC);  
        FacesContext.getCurrentInstance().addMessage(null, msg);  
        
    }
	public void setPeopleLimit() throws Exception
	{
		System.out.print("setPeopleLimit");
		long msNow = System.currentTimeMillis();
		UsersCityConfig uccAfter = economyService.getEconomy(gameBean.getCityId(),msNow);
		economyService.setNewPeopleCount(uccAfter, newPeopleCount);
	}

	public String showEconomy()
	{		
		return "/test/economy.xhtml?faces-redirect=true";
	}
	public void loadFood() throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	{
		System.out.print("\nLOAD FOOD\n");
//		List<ConcreteFoodDTO> food = new ArrayList<ConcreteFoodDTO>();
//		for(ConcreteFood cf : gameBean.getCurrentCityState().getMyFood().getFood())
//		{
//			food.add((ConcreteFoodDTO)cf.createCommonDTO());
//		}
//		this.food = food;
	}
	public double getPeopleInHouses()
	{
		return this.freePeopleCount + this.hiredPeople;
	}
	public void loadEconomy() throws Exception
	{
		boolean ajaxRequest = FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest();
		if(!ajaxRequest)
		{
			ELContext elContext = FacesContext.getCurrentInstance().getELContext();
			RequestBean rBean = (RequestBean) FacesContext.getCurrentInstance().getApplication()
				    .getELResolver().getValue(elContext, null, "requestBean");
			UsersCityConfig ucc = rBean.cityState;
			
			setWorkPeople(ucc.getCityStocks().getPeopleStock().getHiredPeople() );
			setTrainPeople(ucc.getTrainedPeople() );
			civilian = ucc.getCityStocks().getPeopleStock().getVisibleAmount();
			freePeopleCount = ucc.getCityStocks().getPeopleStock().getFreePeople();
			currentPeopleLimit = ucc.getMaxLudzi();
			hiredPeople = ucc.getCityStocks().getPeopleStock().getHiredPeople();
			satisfaction = ucc.calculateSatisfaction();
			birthRate = ucc.getBirthRatePerHour();
			
			double eph = (Double)rBean.getEcoVariables().get("eph");
			FoodConsumptionDTO people = new FoodConsumptionDTO("cywile:", civilian, eph*civilian);
			consumption.add(people);
			FoodConsumptionDTO trained = new FoodConsumptionDTO("ludzie szkoleni:", ucc.getTrainedPeople(), eph*ucc.getTrainedPeople());
			consumption.add(trained);
			UsersCityConfig myCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
			for(CityUnitGroup ug : ucc.getMyArmy().getUnits())
			{
				if(ug.getCount() != 0.0d)
				{
					IUnitModel unitB = myCivil.getByIdInInternalMap(ug.getUnitId(), IUnitModel.class);
					FoodConsumptionDTO unit = new FoodConsumptionDTO(unitB.getName(ucc.getUserConfig()), ug.getCount(), ug.getCount()* unitB.getEatPerHour(ucc.getUserConfig()));
					consumption.add(unit);
				}
			}
			
			
			System.out.print("\nLOAD ECONOMY\n");
		}
	}
	
	public EconomyService getEconomyService() {
		return economyService;
	}
	public void setEconomyService(EconomyService economyService) {
		this.economyService = economyService;
	}
	public GameBean getGameBean() {
		return gameBean;
	}
	public void setGameBean(GameBean gameBean) {
		this.gameBean = gameBean;
	}
	public double getFreePeopleCount() {
		return freePeopleCount;
	}
	public void setFreePeopleCount(double peopleCount) {
		this.freePeopleCount = peopleCount;
	}
	public double getSatisfaction() {
		return satisfaction;
	}
	public void setSatisfaction(double satisfaction) {
		this.satisfaction = satisfaction;
	}
	public double getBirthRate() {
		return birthRate;
	}
	public void setBirthRate(double birthRate) {
		this.birthRate = birthRate;
	}
	public int getNewPeopleCount() {
		return newPeopleCount;
	}
	public void setNewPeopleCount(int newPeopleCount) {
		this.newPeopleCount = newPeopleCount;
	}
	public int getCurrentPeopleLimit() {
		return currentPeopleLimit;
	}
	public void setCurrentPeopleLimit(int currentPeopleLimit) {
		this.currentPeopleLimit = currentPeopleLimit;
	}
	
	public double getWorkPeople() {
		return workPeople;
	}
	public void setWorkPeople(double workPeople) {
		this.workPeople = workPeople;
	}
	public double getTrainPeople() {
		return trainPeople;
	}
	public void setTrainPeople(double trainPeople) {
		this.trainPeople = trainPeople;
	}
	public double getCivilian() {
		return civilian;
	}
	public void setCivilian(double civilian) {
		this.civilian = civilian;
	}
	
}


