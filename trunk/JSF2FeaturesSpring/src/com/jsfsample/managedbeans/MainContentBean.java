package com.jsfsample.managedbeans;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import dto.ConcreteResourceDTO;
import dto.PriceDTO;
import dto.ResearchDTO;
import dto.TimeDTO;
import dto.UnitDTO;

import services.EconomyService;

@ManagedBean(name="mainContentBean")
@ViewScoped
public class MainContentBean  implements Serializable{

	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		
		this.content = content;
	}
	private static ExtractBuildingDetailBean extractBuildingDetailsBean() {
        return SpringJSFUtil.getBean("buildingDetails");
    }
	private static ResearchDetailBean researchDetailsBean() {
        return SpringJSFUtil.getBean("researchDetailsBean");
    }
	private static BuildingDetailBean cityBuildingBean() {
        return SpringJSFUtil.getBean("cityBuildingDetails");
    }
	private static UnitDetailBean unitDetailsBean() {
        return SpringJSFUtil.getBean("unitDetailsBean");
    }
	public int getResearchesSize()
	{
		if(content != null)
		{
			if(content.equals("extractBuilding"))
				return extractBuildingDetailsBean().getBuilding().getResearches().size();
			else if(content.equals("cityBuilding"))
				return cityBuildingBean().getCityBuilding().getResearches().size();
		}
		return 0;
	}
	public int getUnitsSize()
	{
		if(content != null)
		{
			if(content.equals("extractBuilding"))
				return extractBuildingDetailsBean().getBuilding().getUnits().size();
			else if(content.equals("cityBuilding"))
				return cityBuildingBean().getCityBuilding().getUnits().size();
		}
		return 0;
	}
	public List<ResearchDTO> getResearches()
	{
		if(content != null)
		{
			if(content.equals("extractBuilding"))
				return extractBuildingDetailsBean().getBuilding().getResearches();
			else if(content.equals("cityBuilding"))
				return cityBuildingBean().getCityBuilding().getResearches();
		}
		return null;
	}
	public List<UnitDTO> getUnits()
	{
		if(content != null)
		{
			if(content.equals("extractBuilding"))
				return extractBuildingDetailsBean().getBuilding().getUnits();
			else if(content.equals("cityBuilding"))
				return cityBuildingBean().getCityBuilding().getUnits();
		}
		return null;
	}
	private int id,level;
	public Integer getId()
	{
		if(content != null)
		{
			if(content.equals("extractBuilding"))
				return extractBuildingDetailsBean().getBuilding().getId();
			else if(content.equals("research"))
				return researchDetailsBean().getResearch().getId();
			else if(content.equals("cityBuilding"))
				return cityBuildingBean().getCityBuilding().getId();
			else if(content.equals("unit"))
				return unitDetailsBean().getUnit().getId();
		}
		return null;
	}
	public void setId(int id)
	{
		return;
	}
	public void setLevel(int lvl)
	{
		return;
	}
	public String getName()
	{
		if(content != null)
		{
			if(content.equals("extractBuilding"))
				return extractBuildingDetailsBean().getBuilding().getName();
			else if(content.equals("research"))
				return researchDetailsBean().getResearch().getName();
			else if(content.equals("cityBuilding"))
				return cityBuildingBean().getCityBuilding().getName();
			else if(content.equals("unit"))
				return unitDetailsBean().getUnit().getName();
		}
		return null;
	}
	public Integer getLevel()
	{
		if(content != null)
		{
			if(content.equals("extractBuilding"))
				return extractBuildingDetailsBean().getBuilding().getLevel();
			else if(content.equals("research"))
				return researchDetailsBean().getResearch().getLevel();
			else if(content.equals("cityBuilding"))
				return cityBuildingBean().getCityBuilding().getLevel();
		}
		return null;
	}
	public String getDescription()
	{
		if(content != null)
		{
			if(content.equals("extractBuilding"))
				return extractBuildingDetailsBean().getBuilding().getDescription();
			else if(content.equals("research"))
				return researchDetailsBean().getResearch().getDescription();
			else if(content.equals("cityBuilding"))
				return cityBuildingBean().getCityBuilding().getDescription();
			else if(content.equals("unit"))
				return unitDetailsBean().getUnit().getDescription();
		}
		return null;
	}
	public TimeDTO getBuildTime()
	{
		if(content != null)
		{
			if(content.equals("extractBuilding"))
				return extractBuildingDetailsBean().getBuilding().getTime();
			else if(content.equals("research"))
				return researchDetailsBean().getResearch().getTime();
			else if(content.equals("cityBuilding"))
				return cityBuildingBean().getCityBuilding().getTime();
			else if(content.equals("unit"))
				return unitDetailsBean().getUnit().getTime();
		}
		return null;
	}
	public void test()
	{
		System.out.print("test method");
	}
	public void upgrade()
	{
		System.out.print("upgrade\n");
		try
		{
			if(content != null)
			{
				if(content.equals("extractBuilding"))
					
					extractBuildingDetailsBean().upgradeBuilding();
					 
				else if(content.equals("research"))
					researchDetailsBean().upgradeResearch();
				else if(content.equals("cityBuilding"))
					cityBuildingBean().upgradeBuilding();
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public List<PriceDTO> getCost()
	{
		if(content != null)
		{
			if(content.equals("extractBuilding"))
				return extractBuildingDetailsBean().getBuilding().getCost();
			else if(content.equals("research"))
				return researchDetailsBean().getResearch().getCost();
			else if(content.equals("cityBuilding"))
				return cityBuildingBean().getCityBuilding().getCost();
			else if(content.equals("unit"))
				return unitDetailsBean().getUnit().getPrice();
		}
		return null;
	}
	@PostConstruct
	public void pre()
	{
		System.out.print("\nutworzono main content bean\n");
		//loadBuildings();
	}
	@PreDestroy
	public void dest()
	{
		System.out.print("\nusuniÍto main content bean\n");
	}

}
