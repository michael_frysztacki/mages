package com.jsfsample.managedbeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.el.ELContext;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;


import dto.ConcreteResourceDTO;
import entities.stocks.CityResource;
import game_model_impl.StockType;

@ManagedBean(name="resourceBean")
@RequestScoped
public class ResourceBean {

	private List<ConcreteResourceDTO> resources = new ArrayList<ConcreteResourceDTO>();

	public List<ConcreteResourceDTO> getResources() {
		return resources;
	}

	public void setResources(List<ConcreteResourceDTO> resources)
	{
		this.resources = resources;
	}
	
	@PostConstruct
	public void init()
	{
		
	}
	@PreDestroy
	public void destroy()
	{
		
	}
	@ManagedProperty(value = "#{gameBean}")
	private GameBean gameBean;
	public GameBean getGameBean() {
		return gameBean;
	}

	public void setGameBean(GameBean gameBean) {
		this.gameBean = gameBean;
	}

	public void loadResources()
	{
		boolean ajaxRequest = FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest();
		if(!ajaxRequest)
		{
			List<ConcreteResourceDTO> localResources = new ArrayList<ConcreteResourceDTO>();
			ELContext elContext = FacesContext.getCurrentInstance().getELContext();
			RequestBean rBean = (RequestBean) FacesContext.getCurrentInstance().getApplication()
				    .getELResolver().getValue(elContext, null, "requestBean");
			for(CityResource cr : rBean.cityState.getCityStocks().getResources() )
			{
				localResources.add(new ConcreteResourceDTO(cr));	
			}
			localResources.add(new ConcreteResourceDTO(rBean.cityState.getCityStocks().getStockByType(StockType.peopleCount) ));
	//		gameBean.getCurrentCityState().getArmy().getUnitGroupById(10).createDetailDTO();
			this.resources = localResources;
			System.out.print("\nLOAD RESOURCES\n");
		}
	}
	
}
