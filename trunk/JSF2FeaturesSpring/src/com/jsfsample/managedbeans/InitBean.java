package com.jsfsample.managedbeans;

import game_model_interfaces.initializations.DefaultCityInitializator;
import game_model_interfaces.initializations.TestPointInitializator;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import junittest.model_test.text.world_definition_naming.TestCityInitializator;

import dto.ConcreteResourceDTO;
import entities.Civil;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.stocks.CityResource;

import services.CityService;
import services.EconomyService;
import services.LoginService;
import test.main;

@ManagedBean(name = "testBean")
@ApplicationScoped
public class InitBean{
	
	@ManagedProperty(value = "#{loginService}")
	private LoginService loginService;
	
	public LoginService getLoginService() {
		return loginService;
	}
	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}
	
	@PostConstruct
	public void loadUser()
	{
		System.out.print("gameBean PostConstruct - loadUser");
		long msNow = System.currentTimeMillis();
		try {
			loginService.register("friko", "friko", Civil.korea, msNow*1000l*1000l, new DefaultCityInitializator(), new TestPointInitializator());
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
