package com.jsfsample.managedbeans;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.GregorianCalendar;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;

import dto.DetailedExtractBuildingDTO;
import dto.DetailedBuildingDTO;
import dto.DetailedUnitDTO;
import dto.UnitDTO;

import services.BuildingService;
import services.EconomyService;
import services.UnitService;

import com.jsfsample.model.Bike;
import com.jsfsample.services.BikeDataProvider;

import dto.ExtractBuildingDTO;
import entities.UsersCityConfig;
import entities.units.CityUnitGroup;

@ManagedBean(name="unitDetailsBean")
@ViewScoped
public class UnitDetailBean implements Serializable{

	@PostConstruct
	public void pre()
	{
		System.out.print("\n utworzono unit detail view\n");
	}
	private static EconomyService economyService() {
        return SpringJSFUtil.getBean("economyService");
    }
	
	private static GameBean gameBean() {
        return SpringJSFUtil.getBean("gameBean");
    }
	
	private static UnitService unitService() {
        return SpringJSFUtil.getBean("unitService");
    }
	private DetailedUnitDTO unit;
	/*
	 * ajax method
	 */
	public void loadUnit(int unitId) throws Exception
	{
		long msNow = System.currentTimeMillis();
		UsersCityConfig uccAfter =  economyService().getEconomy(gameBean().getCityId(), msNow);
		CityUnitGroup ug = uccAfter.getMyArmy().getUnitGroupById(unitId);
		//setUnit( new DetailedU );
	}
	public void buildUnit() throws Exception
	{
		long msNow = System.currentTimeMillis();
		int cityId = gameBean().getCityId();
		unitService().buildArmy(unit, cityId, msNow);
		
	}
	public DetailedUnitDTO getUnit() {
		return unit;
	}
	public void setUnit(DetailedUnitDTO unit) {
		this.unit = unit;
	}
	
//	public void buildArmy() throws Exception
//	{
//		long msNow = System.currentTimeMillis();
//		UsersCityConfig uccAfter = economyService().getEconomy(gameBean().getCurrentCityState(), msNow);
//		for(UnitDTO unit : militaryBuilding.getUnits())
//		{
//			if(unit.getBuildCount() > 0)
//			{
//				unitService().buildArmy(unit, uccAfter);
//			}
//		}
//	}
//	
//	public void upgradeBuilding() throws Exception
//	{
//		System.out.print("upgrade building");
//		buildingService().upgradeBuilding(militaryBuilding.getId(),gameBean().getCurrentCityState());
//	}

}
