package com.jsfsample.managedbeans;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.security.RolesAllowed;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;




import services.CityService;
import services.LoginService;
import services.impl.LoginServiceImpl;



import com.jsfsample.application.AuthenticationService;


import dao.SpringSessionFactory;
import dao.UserConfigHomeImpl;
import dto.ExtractBuildingDTO;
import entities.UserConfig;
import entities.UsersCityConfig;
import game_model_impl.Building;

@ManagedBean(name = "loginBean")
@RequestScoped
public class LoginBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String login;
	private String password;
//	private UserConfig uc = null;

	@ManagedProperty(value = "#{loginService}")
	private LoginService loginService;
	public LoginService getLoginService() {
		return loginService;
	}
	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	@ManagedProperty(value = "#{gameBean}")
	private GameBean gameBean;
	@ManagedProperty(value = "#{cityService}")
	private CityService cityService;
	public GameBean getGameBean() {
		return gameBean;
	}
	public void setGameBean(GameBean gameBean) {
		this.gameBean = gameBean;
	}

	@ManagedProperty(value = "#{authenticationService}")
	private AuthenticationService authenticationService; // injected Spring defined service for bikes
	
	public String login() {

		boolean success = authenticationService.login(login, password);
		
		if (success){
			UserConfig uc = loginService.findByName(SecurityContextHolder.getContext().getAuthentication().getName());
			Set cities = cityService.getCities(uc);
			gameBean.setCityId(((UsersCityConfig)cities.iterator().next()).getId());
			return "bikesShop.xhtml"; // return to application but being logged now 
		}
		else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Login or password incorrect."));			
			return "login.xhtml";
		}
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setAuthenticationService(AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}

	
	public CityService getCityService() {
		return cityService;
	}
	public void setCityService(CityService cityService) {
		this.cityService = cityService;
	}
	
}
