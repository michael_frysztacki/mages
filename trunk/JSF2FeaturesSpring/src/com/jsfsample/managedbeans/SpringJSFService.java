package com.jsfsample.managedbeans;

import game_model_impl.AllWorlds;

public interface SpringJSFService {

	public AllWorlds getAllWorlds();
}
