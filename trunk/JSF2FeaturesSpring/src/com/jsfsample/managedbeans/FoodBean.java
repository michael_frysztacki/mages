package com.jsfsample.managedbeans;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;


import org.primefaces.event.SlideEndEvent;


import services.BuildingService;
import services.EconomyService;

import dto.ConcreteFoodDTO;
import dto.ConcreteResourceDTO;
import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;
import entities.stocks.CityFood;
import entities.stocks.CityResource;
import entities.stocks.TripStock;
import game_model_impl.StockType;


@ManagedBean(name="foodBean")
@RequestScoped
public class FoodBean {

	private List<ConcreteFoodDTO> food = new ArrayList<ConcreteFoodDTO>();
	@ManagedProperty(value = "#{gameBean}")
	private GameBean gameBean;
	@ManagedProperty(value = "#{economyService}")
	private EconomyService economyService;
	private int buildingId;
	public int getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(int buildingId) {
		this.buildingId = buildingId;
	}

	public EconomyService getEconomyService() {
		return economyService;
	}

	public void setEconomyService(EconomyService economyService) {
		this.economyService = economyService;
	}

	@ManagedProperty(value = "#{buildingService}")
	private BuildingService buildingService;
	public BuildingService getBuildingService() {
		return buildingService;
	}

	public void setBuildingService(BuildingService buildingService) {
		this.buildingService = buildingService;
	}

	public GameBean getGameBean() {
		return gameBean;
	}

	public void setGameBean(GameBean gameBean) {
		this.gameBean = gameBean;
	}

	public void onSlideEnd(SlideEndEvent event) throws Exception {  
		long msNow = System.currentTimeMillis();
		UsersCityConfig uccAfter = economyService.getEconomy(gameBean.getCityId(), msNow);
		int peopleFloor = (int)Math.floor(uccAfter.getCityStocks().getPeopleStock().getVisibleAmount() );
		if(event.getValue() <= peopleFloor)
		{
			buildingService.setPeopleInBuilding(uccAfter, this.buildingId, event.getValue());
		}
        FacesMessage msg = new FacesMessage("Slide Ended", "Value: " + event.getValue());  
        FacesContext.getCurrentInstance().addMessage(null, msg);  
        
    }
	
	public void loadFood()
	{
		boolean ajaxRequest = FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest();
		if(!ajaxRequest)
		{
			ELContext elContext = FacesContext.getCurrentInstance().getELContext();
			RequestBean rBean = (RequestBean) FacesContext.getCurrentInstance().getApplication()
				    .getELResolver().getValue(elContext, null, "requestBean");
			Map<String,Object> ecoVar = rBean.getEcoVariables();
			//Map<StockType,Double> foodOph = (Map<StockType,Double>)ecoVar.get("startFoodOph");
			UsersCityConfig ucc = rBean.cityState;
			//Civilization civil = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		
			for(CityFood cf : rBean.cityState.getCityStocks().getFood() )
			{
				food.add(new ConcreteFoodDTO(cf, rBean.cityState));
			}
			System.out.print("\nLOAD Food\n");
		}
	}

	public List<ConcreteFoodDTO> getFood() {
		return food;
	}

	public void setFood(List<ConcreteFoodDTO> food) {
		this.food = food;
	}
	
}
