package com.jsfsample.managedbeans;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.TabChangeEvent;

import services.BuildingService;
import services.CityService;
import services.EconomyService;

import dto.BuildingDTO;
import dto.ConcreteResourceDTO;
import dto.DetailedExtractBuildingDTO;
import dto.ExtractBuildingDTO;
import entities.UsersCityConfig;

@ManagedBean(name = "buildingBean")
@ViewScoped
public class BuildingBean implements Serializable{

	private static GameBean gameBean() {
        return SpringJSFUtil.getBean("gameBean");
    }

	private static EconomyService economyService() {
        return SpringJSFUtil.getBean("economyService");
    }
	
	private static BuildingService buildingService() {
        return SpringJSFUtil.getBean("buildingService");
    }
	
	private static CityService cityService() {
        return SpringJSFUtil.getBean("cityService");
    }
	
	private ExtractBuildingDTO peopleSetBuilding;

	private int peopleCount;
	
	private List<ConcreteResourceDTO> resources = new ArrayList<ConcreteResourceDTO>();

	public List<ConcreteResourceDTO> getResources() {
		return resources;
	}

	public void setResources(List<ConcreteResourceDTO> resources) {
		this.resources = resources;
	}

	private List<DetailedExtractBuildingDTO> foodExtractBuildings;

	public List<DetailedExtractBuildingDTO> getFoodExtractBuildings() {
		return foodExtractBuildings;
	}

	public void setFoodExtractBuildings(
			List<DetailedExtractBuildingDTO> foodExtractBuildings) {
		this.foodExtractBuildings = foodExtractBuildings;
	}

	private List<DetailedExtractBuildingDTO> resourceExtractBuildings;
	
	public List<DetailedExtractBuildingDTO> getResourceExtractBuildings() {
		return resourceExtractBuildings;
	}

	public void lvlup()
	{
		String s_bid = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("buildingId");
		String test = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("test");
		System.out.print("inc Level");
		int buildingId = Integer.parseInt(s_bid);
		for(DetailedExtractBuildingDTO b : resourceExtractBuildings)
		{
			if(buildingId == b.getId())
			{
				b.setLevel(b.getLevel() + 1);
				return;
			}
		}
		for(BuildingDTO b : militaryBuildings)
		{
			if(buildingId == b.getId())
			{
				b.setLevel(b.getLevel() + 1);
				return;
			}
		}
		for(DetailedExtractBuildingDTO b : foodExtractBuildings)
		{
			if(buildingId == b.getId())
			{	
				b.setLevel(b.getLevel() + 1);
				return;
			}
		}
	}
	public void setResourceExtractBuildings(
			List<DetailedExtractBuildingDTO> resourceExtractBuildings) {
		this.resourceExtractBuildings = resourceExtractBuildings;
	}

	private DetailedExtractBuildingDTO selectedBuilding; 
	
	private List<BuildingDTO> militaryBuildings;
	
	private List<BuildingDTO> cityBuildings;
	//private List<BuildingDTO> extractBuildings;

	/*
	 * ajax call for building tab change
	 */
	 public void onTabChange(TabChangeEvent event) throws Exception {
		 	String id = event.getTab().getTitle();
//		 	if(id.equals("jedzenie"))
//		 	{
//		 		loadExtractAndConvertBuildingsPage();
//		 	}
//		 	else if(id.equals("surowce"))
//		 	{
//		 		loadExtractAndConvertBuildingsPage();
//		 	}
//		 	else if(id.equals("militarne"))
//		 	{
//		 		this.loadMilitaryBuildings();
//		 	}
		 	
	        FacesMessage msg = new FacesMessage("Tab Changed", "Active Tab: " + event.getTab().getTitle());  
	  
	        //FacesContext.getCurrentInstance().addMessage(null, msg);  
	} 
	 
	@PostConstruct
	public void pre()
	{
		System.out.print("\nUTWORZONO Building Bean VIEW SCOPE\n");
		//loadBuildings();
	}
	@PreDestroy
	public void dest()
	{
		System.out.print("\nUSUNIETO Building Bean VIEW SCOPE\n");
	}
	
	public void loadCityBuildings() throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	{
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		RequestBean rBean = (RequestBean) FacesContext.getCurrentInstance().getApplication()
		    .getELResolver().getValue(elContext, null, "requestBean");
		UsersCityConfig uccAfter = rBean.cityState;
		//UsersCityConfig uccAfter = gameBean().getCurrentCityState();
		setCityBuildings(buildingService().getCityBuildings(uccAfter));
	}
	public void loadMilitaryBuildings() throws Exception
	{
		boolean ajaxRequest = FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest();
		if(!ajaxRequest)
		{
//			long msNow = System.currentTimeMillis();
			ELContext elContext = FacesContext.getCurrentInstance().getELContext();
			RequestBean rBean = (RequestBean) FacesContext.getCurrentInstance().getApplication()
				    .getELResolver().getValue(elContext, null, "requestBean");
			UsersCityConfig uccAfter =  rBean.cityState;//economyService().getEconomy(gameBean().getCityId(), msNow);
//		for(ConcreteResource cr : uccAfter.getMyResources().getResources())
//		{
//			resources.add((ConcreteResourceDTO)cr.createCommonDTO());
//		}
		militaryBuildings = buildingService().getMilitaryBuildings(uccAfter);
		}
	}
	public void loadExtractAndConvertBuildingsPage() throws Exception
	{	
		boolean ajaxRequest = FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest();
		if(!ajaxRequest)
		{
			long msNow = System.currentTimeMillis();
			ELContext elContext = FacesContext.getCurrentInstance().getELContext();
			RequestBean rBean = (RequestBean) FacesContext.getCurrentInstance().getApplication()
				    .getELResolver().getValue(elContext, null, "requestBean");
			UsersCityConfig uccAfter = rBean.cityState;//economyService().getEconomy(gameBean().getCityId(), msNow);
			List<DetailedExtractBuildingDTO> local;// = new ArrayList<ExtractBuildingDTO>();
			List<DetailedExtractBuildingDTO> food = new ArrayList<DetailedExtractBuildingDTO>();
			List<DetailedExtractBuildingDTO> resource = new ArrayList<DetailedExtractBuildingDTO>();
			local = buildingService().getExtractAndConvertBuildings(uccAfter);
			//List<ConcreteResourceDTO> resources = null;
	//		for(ConcreteResource cr : uccAfter.getMyResources().getResources())
	//		{
	//			resources.add((ConcreteResourceDTO)cr.createCommonDTO());
	//		}
			//resourceEconomyBean.setResources(resources);
			for(DetailedExtractBuildingDTO eb : local)
			{
				if(!eb.getType().isFood() )
				{
					resource.add(eb);
				}
				else food.add(eb);
			}
			foodExtractBuildings = food;
			resourceExtractBuildings = resource;
		}
	}
//	public String save() throws Exception
//	{
//		long msNow = System.currentTimeMillis();
//		UsersCityConfig uccAfter = economyService().getEconomy(gameBean().getCityId(),msNow);
//		for(DetailedExtractBuildingDTO b : foodExtractBuildings)
//		{
//			int newPeople = b.getActualPeople();
//			//System.out.print("\nnew People count" + newPeople + "\n");
//			buildingService().setPeopleInBuilding(uccAfter, b.getId(), newPeople);
//		}
//		for(DetailedExtractBuildingDTO b : resourceExtractBuildings)
//		{
//			int newPeople = b.getActualPeople();
//			//System.out.print("\nnew People count" + newPeople + "\n");
//			buildingService().setPeopleInBuilding(uccAfter, b.getId(), newPeople);
//		}
//		cityService().mergeCity(uccAfter);
//		return null;
//	}
	

	public ExtractBuildingDTO getPeopleSetBuilding() {
		return peopleSetBuilding;
	}

	public void setPeopleSetBuilding(ExtractBuildingDTO peopleSetBuilding) {
		this.peopleSetBuilding = peopleSetBuilding;
	}

	public int getPeopleCount() {
		return peopleCount;
	}

	public void setPeopleCount(int peopleCount) {
		this.peopleCount = peopleCount;
	}


	public List<BuildingDTO> getMilitaryBuildings() {
		return militaryBuildings;
	}

	public void setMilitaryBuildings(List<BuildingDTO> militaryBuildings) {
		this.militaryBuildings = militaryBuildings;
	}

	public List<BuildingDTO> getCityBuildings() {
		return cityBuildings;
	}

	public void setCityBuildings(List<BuildingDTO> cityBuildings) {
		this.cityBuildings = cityBuildings;
	}

	public DetailedExtractBuildingDTO getSelectedBuilding() {
		return selectedBuilding;
	}

	public void setSelectedBuilding(DetailedExtractBuildingDTO selectedBuilding) {
		this.selectedBuilding = selectedBuilding;
	}




//	public GameBean getGameBean() {
//		return gameBean;
//	}
//	public void setGameBean(GameBean gameBean) {
//		this.gameBean = gameBean;
//	}
//	public BuildingService getBuildingService() {
//		return buildingService;
//	}
//	public void setBuildingService(BuildingService buildingService) {
//		this.buildingService = buildingService;
//	}
	
}
