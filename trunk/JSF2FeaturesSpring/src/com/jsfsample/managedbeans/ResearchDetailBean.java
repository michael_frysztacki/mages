package com.jsfsample.managedbeans;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.GregorianCalendar;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.Flash;

import dto.DetailResearchDTO;
import dto.DetailedExtractBuildingDTO;
import dto.DetailedBuildingDTO;
import dto.DetailedUnitDTO;
import dto.ResearchDTO;
import dto.UnitDTO;

import services.BuildingService;
import services.EconomyService;
import services.ResearchService;
import services.UnitService;

import com.jsfsample.model.Bike;
import com.jsfsample.services.BikeDataProvider;

import dto.ExtractBuildingDTO;
import entities.UserResearch;
import entities.UsersCityConfig;
import entities.units.CityUnitGroup;

@ManagedBean(name="researchDetailsBean")
@ViewScoped
public class ResearchDetailBean implements Serializable{

	
//	@ManagedProperty(value = "#{gameBean}")
//	private GameBean gameBean;
//	public GameBean getGameBean() {
//		return gameBean;
//	}
//
//	public void setGameBean(GameBean gameBean) {
//		this.gameBean = gameBean;
//	}
	@PostConstruct
	public void pre()
	{
		System.out.print("\n utworzono unit detail view\n");
	}
	private static EconomyService economyService() {
        return SpringJSFUtil.getBean("economyService");
    }
	
	private static GameBean gameBean() {
        return SpringJSFUtil.getBean("gameBean");
    }
	
	private static UnitService unitService() {
        return SpringJSFUtil.getBean("unitService");
    }
	
	private static ResearchService researchService() {
        return SpringJSFUtil.getBean("researchService");
    }
	
	private Integer researchId;
	
	private DetailResearchDTO research;
	public void loadResearch(int rId) throws Exception
	{
		long msNow = System.currentTimeMillis();
		UsersCityConfig uccAfter = economyService().getEconomy(gameBean().getCityId(), msNow);
		UserResearch ur = uccAfter.getUserConfig().getResearch(rId);
		this.setResearch( new DetailResearchDTO(ur, uccAfter) );
	}
	public void upgradeResearch() throws Exception
	{
		long msNow = System.currentTimeMillis();
		researchService().upgradeResearch(this.getResearch().getId(), gameBean().getCityId(), msNow);
	}
	public Integer getResearchId() {
		return researchId;
	}
	public void setResearchId(Integer researchId) {
		this.researchId = researchId;
	}
	public DetailResearchDTO getResearch() {
		return research;
	}
	public void setResearch(DetailResearchDTO research) {
		this.research = research;
	}
	
	
	
//	public void buildArmy() throws Exception
//	{
//		long msNow = System.currentTimeMillis();
//		UsersCityConfig uccAfter = economyService().getEconomy(gameBean().getCurrentCityState(), msNow);
//		for(UnitDTO unit : militaryBuilding.getUnits())
//		{
//			if(unit.getBuildCount() > 0)
//			{
//				unitService().buildArmy(unit, uccAfter);
//			}
//		}
//	}
//	
//	public void upgradeBuilding() throws Exception
//	{
//		System.out.print("upgrade building");
//		buildingService().upgradeBuilding(militaryBuilding.getId(),gameBean().getCurrentCityState());
//	}

}
