package com.jsfsample.managedbeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.SlideEndEvent;

import dto.DetailedExtractBuildingDTO;
import entities.UsersCityConfig;

import services.BuildingService;
import services.EconomyService;
import services.UnitService;

@ManagedBean(name="buildingDetails")
@ViewScoped
public class ExtractBuildingDetailBean implements Serializable{

	private Integer buildingId;
	private DetailedExtractBuildingDTO building;
	

	public void onSlideEnd(SlideEndEvent event) throws Exception {  
		long msNow = System.currentTimeMillis();
		UsersCityConfig uccAfter = economyService().getEconomy(gameBean().getCityId(), msNow);
		int peopleFloor = (int)Math.floor(uccAfter.getCityStocks().getPeopleStock().getVisibleAmount() );
		if(event.getValue() <= peopleFloor)
		{
			buildingService().setPeopleInBuilding(uccAfter, this.building.getId(), event.getValue());
		}
        FacesMessage msg = new FacesMessage("Slide Ended", "Value: " + event.getValue());  
        FacesContext.getCurrentInstance().addMessage(null, msg);  
        
    }  
	@PostConstruct
	public void pre()
	{
		System.out.print("\n utworzono building detail view\n");
	}
	@PreDestroy
	public void destroy()
	{
		System.out.print("\n zniszczono building detail view\n");
	}
	private static EconomyService economyService() {
        return SpringJSFUtil.getBean("economyService");
    }
	
	private static GameBean gameBean() {
        return SpringJSFUtil.getBean("gameBean");
    }
	
	private static UnitService unitService() {
        return SpringJSFUtil.getBean("unitService");
    }
	
	private static BuildingService buildingService() {
        return SpringJSFUtil.getBean("buildingService");
    }
	public void loadExtractBuilding(int buildingId) throws Exception
	{
		long msNow = System.currentTimeMillis();
		UsersCityConfig uccAfter = economyService().getEconomy(gameBean().getCityId(), msNow);
		setBuilding(buildingService().getExtractBuilding(uccAfter, buildingId));
	}
	public void upgradeBuilding() throws Exception
	{
		System.out.print("upgrade building");
		long msNow = System.currentTimeMillis();
		int cityId = gameBean().getCityId();
		buildingService().upgradeBuilding(this.building.getId(),cityId,msNow);
	}

	public Integer getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(Integer buildingId) {
		this.buildingId = buildingId;
	}

	public DetailedExtractBuildingDTO getBuilding() {
		return building;
	}

	public void setBuilding(DetailedExtractBuildingDTO building) {
		this.building = building;
	}

}
