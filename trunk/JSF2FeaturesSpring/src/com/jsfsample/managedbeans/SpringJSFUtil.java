package com.jsfsample.managedbeans;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;



import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import citys_interaction.CityHolder;
import citys_interaction.FightComp;

import economycomputation.CityCompUtil;
import economycomputation.ConvertBuildingUtil;
import economycomputation.ProductionUtil;
import game_model_impl.AllWorlds;
import game_model_impl.WorldDefinition;
import game_utilities.TripUtil;

import junittest.old_tests.Testing;

@Component
public class SpringJSFUtil implements ApplicationContextAware{

    public <T> T getBean(String beanName) {
        if (beanName == null) {
            return null;
        }
        return getValue("#{" + beanName + "}");
    }
    
    @SuppressWarnings("unchecked")
    private <T> T getValue(String expression) {
        FacesContext context = FacesContext.getCurrentInstance();
        return (T) context.getApplication().evaluateExpressionGet(context, expression, Object.class);
    }
    public Flash flashScope (){
    	return (FacesContext.getCurrentInstance().getExternalContext().getFlash());
    }
    //private static ApplicationContext ctx = new ClassPathXmlApplicationContext("file:WebContent/WEB-INF/applicationContext.xml"); //get Spring context
    private static ApplicationContext ctx;

    /**
     * This method is called from within the ApplicationContext once it is 
     * done starting up, it will stick a reference to itself into this bean.
     * @param context a reference to the ApplicationContext.
     */
    public void setApplicationContext(ApplicationContext context) throws BeansException {
      ctx = context;
    }
   
//    @Autowired
//    private ConvertBuildingUtil convertBuildingUtil;
//    @Autowired
//    private CivilizationHolder civilizationHolder;
//    @Autowired
//    private CityCompUtil cityCompUtil;
//    @Autowired
//    private TripUtil tripUtil;
//    @Autowired FightComp fightComp;
    public ConvertBuildingUtil getConvertBuildingUtil() {
		return ctx.getBean(ConvertBuildingUtil.class);
	}

	public AllWorlds getAllWorlds() {
		return ctx.getBean(AllWorlds.class);
	}

	public ProductionUtil getProductionUtil() {
		return ctx.getBean(ProductionUtil.class);
	}
	
	public CityCompUtil getCityCompUtil() {
		return ctx.getBean(CityCompUtil.class);
	}

	public TripUtil getTripUtil() {
		return ctx.getBean(TripUtil.class);
	}

	public FightComp getFightComp() {
		return ctx.getBean(FightComp.class);
	}
    
}
