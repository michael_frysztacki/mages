package com.jsfsample.model;

import java.io.Serializable;

public class Bike implements Serializable {
	
	private static final long serialVersionUID = 7362384441098790471L;
	private Integer id;
	private String name;
	private String description;
	private Integer price;	
	private Integer discountPrice;
	private Integer category;
	
	public Integer getCategory() {
		return category;
	}
	public void setCategory(Integer category) {
		this.category = category;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Integer getDiscountPrice() {
		return discountPrice;
	}
	public void setDiscountPrice(Integer discountPrice) {
		this.discountPrice = discountPrice;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public boolean isDiscount(){
		return !(discountPrice==null);
	}

}
