package comparators;

import java.util.Comparator;

import net.sf.cglib.core.CollectionUtils;

import com.sun.faces.util.CollectionsUtils;

import entities.Trip;
import entities.UsersCityConfig;


public class TripInteractionComparator implements Comparator<Trip>{

	public TripInteractionComparator(UsersCityConfig ucc)
	{
		this.ucc = ucc;
	}
	private UsersCityConfig ucc;
	@Override
	public int compare(Trip o1, Trip o2) {
		// TODO Auto-generated method stub
		return (int)(o1.timeLeftToNextInteraction(this.ucc.getNsTime_()) - o2.timeLeftToNextInteraction(this.ucc.getNsTime_()));
	}
	
}
