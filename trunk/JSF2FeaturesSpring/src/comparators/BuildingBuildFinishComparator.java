package comparators;

import java.util.Comparator;

import entities.UsersCityConfig;
import entities.buildings.BuildingBuild;

public class BuildingBuildFinishComparator implements Comparator<BuildingBuild>{

	public BuildingBuildFinishComparator(UsersCityConfig ucc)
	{
		this.ucc = ucc;
	}
	private UsersCityConfig ucc;
	@Override
	public int compare(BuildingBuild o1, BuildingBuild o2) {
		return (int)(o1.getFinishTime_() - o2.getFinishTime_());
		
	}

}
