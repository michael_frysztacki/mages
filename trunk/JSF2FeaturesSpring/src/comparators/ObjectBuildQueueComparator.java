package comparators;

import java.util.Comparator;

import entities.UsersCityConfig;
import entities.buildings.BuildingBuild;
import entities.buildings.CityBuilding;
import entities.buildings.ObjectBuildQueue;



public class ObjectBuildQueueComparator implements Comparator<CityBuilding>{

	@Override
	public int compare(CityBuilding arg0, CityBuilding arg1) {
		// TODO Auto-generated method stub
		long ret = arg0.getObjectBuildQueue().timeToNextRecur(null) - arg1.getObjectBuildQueue().timeToNextRecur(null);
		return (int) ret ;
	}


}
