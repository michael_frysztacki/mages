package comparators;

import java.util.Comparator;

import economycomputation.recursion_events.RecursionEvent;

public class RecursionEventComparator implements Comparator<RecursionEvent>{

	@Override
	public int compare(RecursionEvent arg0, RecursionEvent arg1) {
		
		long result = arg0.recursionTimeNs().longValue() - arg1.recursionTimeNs().longValue();
		if(result == 0)
			return 0;
		if(result < 0)
			return -1;
		else 
			return 1;
	}

}
