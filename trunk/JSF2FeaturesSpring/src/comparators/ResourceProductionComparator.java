package comparators;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;

import economycomputation.ProductionUtil;
import entities.UsersCityConfig;
import game_model_impl.StockType;


public class ResourceProductionComparator implements Comparator<StockType>{

	public ResourceProductionComparator(UsersCityConfig ucc, ProductionUtil pu)
	{
		this.ucc = ucc;
		this.pu = pu;
	}
	private UsersCityConfig ucc;
	private ProductionUtil pu;
	@Override
	public int compare(StockType o1, StockType o2) {
		// TODO Auto-generated method stub
		double o1Prod = pu.getFoodProduction(o1, ucc);
		double o2Prod = pu.getFoodProduction(o2, ucc);
		
		double result = o1Prod - o2Prod;
		if(result == 0.0d)return 0;
		return (result > 0.0d ? 1 : -1);
	}
	
}
