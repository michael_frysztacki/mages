package exceptions;

public class RepeatedIdException extends Exception
{
	private int repeatedId;
	public int getRepeatedId()
	{
		return this.repeatedId;
	}
	public RepeatedIdException(int rid)
	{
		this.repeatedId = rid;
	}
	@Override
	public String toString()
	{
		return "powtarzaj�ce si� id:" + this.repeatedId;
	}
}
