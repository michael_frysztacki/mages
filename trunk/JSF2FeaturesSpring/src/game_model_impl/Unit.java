package game_model_impl;

import entities.UserConfig;
import entities.UsersCityConfig;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IFortyficationModel;
import game_model_interfaces.IUnitModel;

public abstract class Unit extends GameModel implements IUnitModel{

	
	protected final int hp;
	protected final int armor;
	protected final int attack;
	
	protected final AttackType attackType;

	private final Integer capacity;
	private final ArmorType armorType;
	
	@Override
	public int getCapacity(UserConfig uc) {
		return capacity;
	}
	public Unit(int id, int hp, int attack, AttackType attackType, int armor,
			ArmorType armorType ,
			Double eatPerHour ,
			Integer capacity , 
			int costPattern,
			int buildTimePattern,
			PriceStockSetImpl cost,
			long baseBuildTime) /* Tablica pr�dko�ci. Zawsze nale�y poda� pr�dko�� dla TerainType.plain, reszta jest opcjonalna. Je�li nie poda si� opcjonalnych pr�dko�ci, s� one takie same jak plain.*/
	{
		super(id,costPattern,cost, baseBuildTime, buildTimePattern);
		this.hp = hp;
		this.attack = attack;
		this.armorType = armorType;
		this.armor = armor;
		this.capacity = capacity;
		this.attackType = attackType;
		
		
	}
	@Override
	public Integer getBuildTime(UsersCityConfig uc)
	{
		ICivilization config = uc.getUserConfig().getMyCivilization();
		int buildTimePattern = config.getUnitBuildTimePatternById(getId());
		return config.getUnitBuildTimeByPattern(this.baseBuildTime, uc.getLevel( this.getRequirementNode().getMotherBuilding().getId() ), buildTimePattern);
	}
	
	@Override
	public PriceStockSetImpl getPrice(UsersCityConfig ucc)
	{
		return this.cost;
	}
	@Override
	public int getHP(UserConfig uc)
	{
		return hp;
	}
	@Override
	public int getArmorOutsideCity(UserConfig uc)
	{
		return armor;
	}
	@Override
	public int getAttackOutsideCity(UserConfig uc) {
		return attack;
	}
	@Override
	public ArmorType getArmorType(UserConfig uc) {
		return armorType;
	}
	/*
	@Override
	public String getName(UsersCityConfig uc) {
	
		return this.mtb.getString(uc);
	}
	*/
	@Override
	public AttackType getAttackType(UserConfig uc) {
		return this.attackType;
	}
	@Override
	public int getAttackInsideCity(UsersCityConfig uc) {
		ICivilization civil = uc.getUserConfig().getMyCivilization();
		Integer reqLevel = civil.fortyficationProtectionLevel(this.getId());
		IUnitModel thisUnit = this.getRequirementNode().getMyGameModel().cast(IUnitModel.class);
		IFortyficationModel fortyficationModel = civil.getGameModelList(IFortyficationModel.class, false, true).get(0);
		int fortyficationLevel = uc.getLevel(fortyficationModel.getId());
		if(reqLevel==null)reqLevel = civil.fortyficationProtectionLevel(thisUnit.getAttackType(uc.getUserConfig()));
		if(reqLevel!=null && reqLevel <= fortyficationLevel){
			return civil.fortyficationAttackBoost(fortyficationLevel, this.attack);
		}
		return this.attack;
	}
	@Override
	public int getArmorInsideCity(UsersCityConfig uc) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
