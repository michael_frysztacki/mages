package game_model_impl;

import entities.Civil;
import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IResearchModel;

public class Research extends GameModel implements IResearchModel{

	private final int maxLvl;
	public Research(int id,int maxLvl, int costPattern, PriceStockSetImpl cost, int baseTime ,int timePattern)
	{
		super(id,costPattern, cost, baseTime ,timePattern);
		//internalId = r_counter++;
		this.maxLvl = maxLvl;
	}
	
	@Override
	public PriceStockSetImpl getPrice(UsersCityConfig ucc)
	{
		PriceStockSetImpl uccCost = null;
		IBuildingModel mother = this.getRequirementNode().getMotherBuilding();
		Civil motherBuildingsCivil = null;
		/*
		 * gdy badanie to tier..
		 */
		IExtractBuildingModel m = mother.cast(IExtractBuildingModel.class);
		if(m != null)
			motherBuildingsCivil = ucc.getBuilding(ExtractCityBuilding.class, mother.getId()).getBuildingsCivil() ;
		else motherBuildingsCivil = ucc.getUserConfig().getCivil();
		Civilization config =  this.getCivilizationHolder().getCivilization(motherBuildingsCivil);
		int costPattern = config.getCostPatternById(getId());
		uccCost = config.getCostByPattern(this.cost, costPattern, ucc.getUserConfig().getLevel(this.getId()), null);
		return uccCost;
	}
	
	@Override
	public Integer getBuildTime(UsersCityConfig uc)
	{
		IBuildingModel mother = getRequirementNode().getMotherBuilding();
		Civil motherBuildingsCivil = null;
		IExtractBuildingModel m = mother.cast(IExtractBuildingModel.class);
		if(m != null) motherBuildingsCivil = uc.getBuilding(ExtractCityBuilding.class, mother.getId()).getBuildingsCivil() ;
		else motherBuildingsCivil = uc.getUserConfig().getCivil();
		Civilization config =  getCivilizationHolder().getCivilization(motherBuildingsCivil);
		int buildingsId = getRequirementNode().getMotherBuilding().getId();
		int buildingsLevel = uc.getLevel(buildingsId);
		if( buildingsLevel == 0)return null;
		int buildTimePattern = config.getResearchBuildTimePatternById( getId() );
		return config.getResearchBuildTimeByPattern( this.baseBuildTime, uc.getUserConfig().getLevel(this.getId()), buildingsLevel, buildTimePattern );
	}
	
	@Override
	public String dump(Language lang, Civil civil)
	{
		return "\nBADANIE\n" + super.dump(lang, civil);
	}
	/*
	@Override
	public String getName(UsersCityConfig uc) {
		return mtb.getString( uc );
	}
	*/
	@Override
	public int getMaxLevel() {
		return maxLvl;
	}
	

}
