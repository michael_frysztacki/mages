package game_model_impl;

import entities.Civil;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IGameModel;
import game_model_interfaces.IResearchModel;
import game_model_interfaces.IUnitModel;

import java.util.ArrayList;
import java.util.List;


public class ExtractBuilding extends Building implements IExtractBuildingModel{

	protected final int maxPeople;
	protected final StockType typ;
	protected final int productionPattern;
	protected final int maxPeoplePattern;
	
	public ExtractBuilding(int id, int maxPeople,
			int timePattern, int costPattern, int ExtractionPattern,
			int maxPeoplePattern, PriceStockSetImpl cost, long baseBuildTime, StockType typ) {
		super(id, timePattern, costPattern, cost, baseBuildTime);
		this.typ = typ;
		this.productionPattern = ExtractionPattern;
		this.maxPeoplePattern = maxPeoplePattern;
		this.maxPeople = maxPeople;
	}
	
	
	@Override
	public List<IResearchModel> getResearches(UsersCityConfig user){
		ResearchFilter filter = new ResearchFilter(user);
		return filter.doFiltering();
	}
	
	@Override
	public List<IUnitModel> getUnits(UserConfig user){
		List<IUnitModel> children = this.getRequirementNode().getChildren(IUnitModel.class);
		List<IUnitModel> ret = new ArrayList<IUnitModel>();
		Civilization civil = (Civilization) user.getMyCivilization();
		for(IUnitModel unit : children){
			if(civil.getByIdInInternalMap(unit.getId(), IUnitModel.class) != null){
				ret.add(unit);
			}
		}
		return ret;
	}
	@Override
	public int getMaxPeople(UsersCityConfig uc)
	{	
		Civil buildingsCivil = uc.getBuilding(ExtractCityBuilding.class, this.getId()).getBuildingsCivil();
		Civilization config =  this.getCivilizationHolder().getCivilization(buildingsCivil);
		int maxPeoplePattern = config.getMaxPeoplePatternById(getId());
		return config.getMaxPeopleByPattern(maxPeople, uc.getLevel(this.getId()), maxPeoplePattern);
	}

	@Override
	public Integer getBuildTime(UsersCityConfig uc){
		Civil buildingsCivil = uc.getBuilding(ExtractCityBuilding.class, this.getId()).getBuildingsCivil();
		Civilization config =  this.getCivilizationHolder().getCivilization(buildingsCivil);
		int buildTimePattern = config.getBuildingBuildTimePatternById(getId());
		return config.getBuildingBuildTimeByPattern(buildTimePattern, uc.getLevel(this.getId()), this.baseBuildTime);
	}
	
	@Override
	public int getOutputPerHour(UsersCityConfig uc){
		ExtractCityBuilding ecb  = uc.getBuilding(ExtractCityBuilding.class, getId());
		Civilization config =  this.getCivilizationHolder().getCivilization(ecb.getBuildingsCivil());
		int productionPattern = config.getExtractionPatternById(getId());
		int production = config.getExtractionByPattern( ecb.getPopulation() , uc.getPoint().getCityLode(this.typ).getValue(), productionPattern);
		return production;
	}
	
	@Override
	public String dump(Language lang, Civil civil){
		String ret=  super.dump(lang, civil).replace("BUDYNEK", "BUDYNEK WYDOBYWAJACY")
				+ "\n   PRODUKOWANY SUROWIEC: " + this.typ +
				"\n   ILO�� LUDZI ORAZ WYDOBYCIE PRZY Z�O�U = 1:\n" + "      level  ilo��_ludzi produkcja\n";
		for(int i=0; i <=30; i++)
		{
			int people = this.getMyCivilization().getMaxPeopleByPattern(maxPeople, i, maxPeoplePattern);
			ret += "         " + i + "      " + people + "           " +
			this.getMyCivilization().getExtractionByPattern(people, 1, productionPattern) + "/h\n";
		}
		return ret;
	}
	
	@Override
	public StockType getResourceType(){
		return this.typ;
	}
	
	private class ResearchFilter{
		
		private UsersCityConfig city;
		private List<IResearchModel> children = ExtractBuilding.this.getRequirementNode().getChildren(IResearchModel.class);
		private List<IResearchModel> ret = new ArrayList<IResearchModel>();
		
		public ResearchFilter(UsersCityConfig city){
			this.city = city;
		}
		
		public List<IResearchModel> doFiltering(){
			for(IResearchModel r : children){
				if(this.canBuildResearch(r))ret.add(r);
			}
			return ret;
		}
		
		private boolean canBuildResearch(IResearchModel r){
			for(RequirementLink link : r.getRequirementNode().getParents()){
				if(!haveParentRequirement( link ))return false;
			}
			return true;
		}
		
		private boolean haveParentRequirement(RequirementLink link){
			if(parentIsBuilding(link))
				return cityHasParentBuilding(link);
			if( parentIsTierResearch(link) )
				return true;
			return this.canBuildResearch(castParentToResearch(link));
		}
		
				
		private boolean parentIsTierResearch(RequirementLink link){
			return castParentToResearch(link).getRequirementNode().getParents().size()==0;
		}
		
		private boolean cityHasParentBuilding(RequirementLink link){
			if(city.getBuilding(CityBuilding.class, getParentFromRequirementLink(link).getId()) == null)return false;
			return true;
		}
		
		private IGameModel getParentFromRequirementLink(RequirementLink link){
			return link.getParentNode().getMyGameModel();
		}
		
		private IResearchModel castParentToResearch(RequirementLink link){
			return getParentFromRequirementLink(link).cast(IResearchModel.class);
		}
		
		private boolean parentIsBuilding(RequirementLink link){
			return link.getParentNode().getMyGameModel().canCast(IBuildingModel.class);
		}
		
	}

}
