package game_model_impl;

import game_model_interfaces.civilization_defining_interfaces.ICivilizationConfigProvider;
import helpers.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/*
 * klasa buduj�ca powi�zania miedzy w�z�ami wymaga�.
* klasy reprezentuj�ce Civilizacje (ICivilizationProvider) musz� by� zainicjalizowanie, zanim
* zostanie wywo�ana metoda build (wszytkie obiekty typu 'Requirement' musz� zosta� potworzone.
 * metoda 'build' wywo�ywana jest w trybie 'lazy', tzn przy pierwszym skorzystaniu z metody getRequirementNode.
 * w build budowane s� tylko powi�zania mi�dzy w�z�ami, czyli powi�zania w�ze�<->w�ze�.
 * Powi�zania w�ze�<->IGameModel NIE s� tutaj robione, s� one robione podczas budowania cywilizacji.
 */
public class RequirementNodeBuilder {
	
	public RequirementNodeBuilder(ICivilizationConfigProvider commonConfig, ICivilizationConfigProvider... extended)
	{
		Map<Integer,GameModel> fullMap = new HashMap<Integer,GameModel>();
		if(commonConfig.gameModels()!=null)
		for(GameModel gm : commonConfig.gameModels())
		{
			fullMap.put(gm.getId(), gm);
		}
		for(ICivilizationConfigProvider ext : extended)
		{
			if(ext.gameModels() != null)
			for(GameModel gm : ext.gameModels() )
			{
				fullMap.put(gm.getId(), gm);
			}
		}
		
		if(commonConfig.getRequirements()!=null)
		for(Requirement req : commonConfig.getRequirements())
		{
			this.addRequirements(req,fullMap.get(req.getChildId()));
		}
		
		for(ICivilizationConfigProvider extendedCivil : extended)
		{
			if(extendedCivil.getRequirements()!=null)
			for(Requirement req : extendedCivil.getRequirements())
			{
				this.addRequirements(req,fullMap.get(req.getChildId()));
			}
		}
		this.build();
	}
	private List<Pair<Requirement,GameModel>> reqs = new ArrayList<Pair<Requirement,GameModel>>();
	private void addRequirements(Requirement req, GameModel childModel)
	{
		reqs.add(new Pair<Requirement,GameModel>(req,childModel));
	}
	private boolean isBuilt = false;
	private Map<Integer,RequirementNode> nodes = new HashMap<Integer,RequirementNode>();
	public RequirementNode getRequirementNode(int objectId)
	{
		return this.nodes.get(objectId);
	}
	private static class RequirementLinkFactory
	{
		public static RequirementLink createRequirementLink(RequirementNode parentNode, RequirementNode childNode, GameModel childModel, Requirement req)
		{
			return new RequirementLink(parentNode, childNode, req.getPatternId(), req.isMotherObject());
		}
		
	}
	public void build()
	{
		if(isBuilt)return;
		Map<Integer,Object> helper = new HashMap<Integer,Object>();
		for(Pair<Requirement,GameModel> req : reqs)
		{
			if(helper.get(req.first.getChildId() )==null)
			{
				helper.put(req.first.getChildId(), new Object() );
				List<Pair<Requirement,GameModel>> l_reqs = findAllRequirementsFromObject(req.first.getChildId());
				RequirementNode rNode = null;
				if(nodes.get(req.first.getChildId()) == null)
				{
					rNode = new RequirementNode();
					nodes.put(req.first.getChildId(), rNode);
				}
				else
				{
					rNode = nodes.get(req.first.getChildId());
				}
				for(Pair<Requirement,GameModel> l_req : l_reqs)
				{
					RequirementNode parentNode = nodes.get(l_req.first.getParentId() );
					if(parentNode == null)
					{
						parentNode = new RequirementNode();
						nodes.put(l_req.first.getParentId() , parentNode);
					}
					RequirementLink reqLink	= RequirementLinkFactory.createRequirementLink(parentNode, rNode, l_req.second, l_req.first); 
					parentNode.addChild(reqLink);
					rNode.addParent(reqLink);
				}
			}
		}
		isBuilt=true;
	}
	private List<Pair<Requirement,GameModel>> findAllRequirementsFromObject(int id)
	{
		List<Pair<Requirement,GameModel>> ret = new ArrayList<Pair<Requirement,GameModel>>();
		for(Pair<Requirement,GameModel> req : reqs)
		{
			if(req.first.getChildId()==id)ret.add(req);
				
		}
		return ret;
	}
	

}
