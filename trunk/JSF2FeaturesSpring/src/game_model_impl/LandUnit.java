package game_model_impl;

import entities.UserConfig;
import game_model_interfaces.ILandUnitModel;

import java.util.Arrays;
import java.util.List;

public class LandUnit extends Unit implements ILandUnitModel{
	
	Double eatPerHour;
	protected final List<TerainSpeedPair> speed; /* km/h */
	
	//new LandUnit(503,100,100,AttackType.normal,10, ArmorType.light, null,10,0,0,null,10, new TerainSpeedPair(TerainType.plain , 50))
	public LandUnit(int id, int hp, int attack, AttackType attackType,
			int armor, ArmorType armorType, Double eatPerHour,
			Integer capacity, int costPattern, int buildTimePattern,
			PriceStockSetImpl cost, long baseBuildTime, TerainSpeedPair... speed) {
		super(id, hp, attack, attackType, armor, armorType, eatPerHour, capacity,
				costPattern, buildTimePattern, cost, baseBuildTime);
		this.speed = Arrays.asList(speed);
	}

	@Override
	public int getSpeed(UserConfig uc, TerainType type) {
		TerainSpeedPair temp = new TerainSpeedPair(type, -1);
		int idx = speed.indexOf(temp);
		if(idx == -1)
		{
			int plainIdx = speed.indexOf(new TerainSpeedPair(TerainType.plain, -1));
			return speed.get(plainIdx).speed;
		}
		else return speed.get(idx).speed;
	}
	
	public static class TerainSpeedPair
	{
		public TerainSpeedPair(TerainType terain, int speed)
		{
			this.terain = terain;
			this.speed = speed;
		}
		@Override
		public boolean equals(Object obj)
		{
			TerainSpeedPair tsp = (TerainSpeedPair) obj;
			if(this.terain == tsp.terain)return true;
			else return false;
		}
		public final TerainType terain;
		public final int speed;
	}

	@Override
	public double getEatPerHour(UserConfig uc) {
		
		return this.eatPerHour;
	}
	
}
