package game_model_impl;

public class Requirement {

	public Requirement(int childId, int patternId, boolean motherObject, int parentId) 
	{
		this.childId = childId;
		this.patternId = patternId;
		this.motherObject = motherObject;
		this.parentId = parentId;
	}
	private int childId;
	public int getChildId()
	{
		return childId;
	}
	
	public boolean isMotherObject() {
		return motherObject;
	}
	private boolean motherObject;
	private int patternId;
	public int getPatternId()
	{
		return patternId;
	}
	public int getParentId()
	{
		return parentId;
	}
	private int parentId; 
}
