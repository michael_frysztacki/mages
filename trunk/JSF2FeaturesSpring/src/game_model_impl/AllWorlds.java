package game_model_impl;

import game_model_interfaces.IAllWorlds;

import java.util.HashMap;
import java.util.Map;
/*
git test
*/
public class AllWorlds implements IAllWorlds{

	private Map<Integer,WorldDefinition> allWorlds = new HashMap<Integer,WorldDefinition>();
	public AllWorlds(WorldDefinition... worlds) {
		
		for(WorldDefinition wd : worlds)
			allWorlds.put(wd.getWorldId(), wd);
	}
	
	@Override
	public WorldDefinition getWorldModel(int id) {
		
		return allWorlds.get(id);
	}
}
