package game_model_impl;

import game_model_impl.Text.Text;
import game_model_impl.boosters.NameBooster;
import game_model_impl.boosters.convert_building.ConvertBuildingSubstractor;
import game_model_impl.city_model.CityModelBooster;
import game_model_impl.visitors.civilization_visitors.CollectCivilizationsInChainUpwards;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IConvertBuildingModel;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IGameModel;
import game_model_interfaces.IWorldDefinition;
import game_model_interfaces.civilization_defining_interfaces.ICivilizationConfigProvider;
import game_model_interfaces.civilization_defining_interfaces.ICommonParameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class WorldDefinition implements IWorldDefinition{

	private int worldId;
	private List<Civilization> allCivilizations = new ArrayList<Civilization>();
	private HashMap<StockType, IExtractBuildingModel> producersMap = new HashMap<StockType, IExtractBuildingModel>();
	private ICommonParameters commonParameters;
	
	public WorldDefinition(int worldId, ICivilizationConfigProvider commonConfig,ICommonParameters commonParameters, ICivilizationConfigProvider... extendedCivils) throws BuildModelException{
		this.worldId = worldId;
		this.commonParameters = commonParameters;
		this.build(commonConfig, commonParameters, extendedCivils);
	}
		private void build(ICivilizationConfigProvider commonConfig,
				ICommonParameters commonParameters, 
				ICivilizationConfigProvider... extendedCivils) throws BuildModelException {
			
			WorldBuilder wb = new WorldBuilder(commonConfig, commonParameters, extendedCivils);
			wb.build();
		}
	public int getWorldId() {
		
		return worldId;
	}
	public List<ICivilization> getAllCivilizations() {
		
		return new ArrayList<ICivilization>( allCivilizations );
	}
	public Civilization getCivilization(Civil civil) {
		
		for(Civilization civ : allCivilizations)
			if(civ.getCivilization() == civil)return civ;
		return null;
	}
	public ICivilization findCivilizationInChain(Civil lookForThis, Civil startHere) {
		
		ICivilization civilNode = this.getCivilization(startHere);
		boolean firstCheck = true;
		do{
			if(!firstCheck)civilNode = civilNode.getParentCivilization();
			firstCheck = false;
			if(civilNode.getCivilization() == lookForThis)
				return civilNode;
			if(civilNode.getParentCivilization() == null) return null;
		}
		while(civilNode.getParentCivilization() != null);
		return null;
	}
	
	public IExtractBuildingModel getProducer(StockType stock) {
		
		return producersMap.get(stock);
	}
	void addProducer(IExtractBuildingModel producer) {
		
		this.producersMap.put(producer.getResourceType(), producer);
	}
	private HashMap<StockType, List<IConvertBuildingModel>> spongersMap = new HashMap<StockType, List<IConvertBuildingModel>>();
	public List<IConvertBuildingModel> getSpongers(StockType stock) {
		
		return spongersMap.get(stock);
	}
	void addSponger(IConvertBuildingModel sponger) {
		
		for(StockType stock : sponger.getInputTypes()){
			List<IConvertBuildingModel> list = spongersMap.get(stock);
			if(list == null){
				list = new ArrayList<IConvertBuildingModel>();
				spongersMap.put(stock, list);
			}
			if(!list.contains(sponger))
				list.add(sponger);
		}
	}
	void addCivilization(Civilization civil) {
		
		allCivilizations.add(civil);
	}
	
	private class WorldBuilder{
		
		private ICivilizationConfigProvider commonConfig;
		private ICommonParameters commonParameters;
		private ICivilizationConfigProvider[] extendedCivils;
		private Map<Civil,Civilization> civilizationsMap = new HashMap<Civil,Civilization>();
		private CivilizationConfigValidator validator = new CivilizationConfigValidator();
		private Map<Integer,IGameModel> allDecoratedGameModels = new HashMap<Integer,IGameModel>();
		private Map<Integer,GameModel> allGameModelsHelper = new HashMap<Integer,GameModel>();
		private RequirementNodeBuilder reqBuilder;
		private List<ICivilizationConfigProvider> allConfigs = new ArrayList<ICivilizationConfigProvider>();
		private final CivilizationConfigSorter sorter = new CivilizationConfigSorter();
		public WorldBuilder(ICivilizationConfigProvider commonConfig,ICommonParameters commonParameters, ICivilizationConfigProvider... extendedCivils){
			this.commonConfig = commonConfig;
			this.commonParameters = commonParameters;
			this.extendedCivils = extendedCivils;
			reqBuilder = new RequirementNodeBuilder ( commonConfig , extendedCivils );
			allConfigs.add(commonConfig);
			for(ICivilizationConfigProvider extended : extendedCivils)
				allConfigs.add(extended);
		}
		public void build() throws BuildModelException {
			
			validator.validate(commonConfig,extendedCivils);
			createEmptyCivilizations();
			buildAllGameModels();
			fillCivilizationsWithGameModels();
			fillProducersAndSpongersInWorldDefinition();
		}
			private void createEmptyCivilizations() {
				
				createEmptyAndNotConnectedCivilizations();
				putAllCivilizationsToHelperMap();
				connectCivilization();
			}
				private void createEmptyAndNotConnectedCivilizations() {
					
					instantiateCommonCivilization();
					for(ICivilizationConfigProvider civ : extendedCivils)
						instantiateExtendedCivilization(civ);
				}
					private void instantiateCommonCivilization() {
						
						Civilization common = new CommonCivilization(commonConfig);
						WorldDefinition.this.allCivilizations.add(common);
					}
					private void instantiateExtendedCivilization( ICivilizationConfigProvider civ) {
						
						ConcreteCivilization conCiv = new ConcreteCivilization(civ);
						WorldDefinition.this.allCivilizations.add(conCiv);
					}
				private void putAllCivilizationsToHelperMap() {
					
					for(Civilization civil : WorldDefinition.this.allCivilizations)
						this.civilizationsMap.put(civil.getCivilization(), civil);
				}
				private void connectCivilization() {
					
					assignParentCivilization();
					assignChildCivilizations();
				}
					private void assignParentCivilization() {
						for(ICivilization civil : WorldDefinition.this.allCivilizations){
							try{
								ConcreteCivilization ext = (ConcreteCivilization) civil;
								ext.setExtendingCivilization( civilizationsMap.get(ext.getConfig().extendsCivilization()) );
							}
							catch(ClassCastException e){};
						}
					}
					private void assignChildCivilizations()
				{
					for(Civilization civ : WorldDefinition.this.allCivilizations){
						Civilization parent = civ.getParentCivilization();
						if( parent != null ) parent.addChildCivilizations(civ);
					}
				}
			private void buildAllGameModels(){
				collectAllGameModelsFromCivilizationsConfigs();
				initGameModels();
				decorateGameModels();
				setRequirementNodesForGameModels();
			}
				private void collectAllGameModelsFromCivilizationsConfigs() {
					collectFromCommonConfig();
					collectFromExtendedConfigs();
				}
					private void collectFromExtendedConfigs() {
						for(ICivilizationConfigProvider config : extendedCivils){
							if(config.gameModels() != null)
							for(GameModel gm : config.gameModels() ){
								collectAndSetCivil( gm, config );
							}
						}
					}
					private void collectFromCommonConfig() {
						if(commonConfig.gameModels() != null)
						for(GameModel gm : commonConfig.gameModels()){
							collectAndSetCivil(gm, commonConfig);
						}
					}
						private void collectAndSetCivil(GameModel gm, ICivilizationConfigProvider config) {
							gm.setCivil(config.getCivilization());
							this.allDecoratedGameModels.put(gm.getId(), gm);
							this.allGameModelsHelper.put(gm.getId(), gm);
						}
				private void initGameModels() {
					for( Entry<Integer, GameModel> gm : allGameModelsHelper.entrySet() ){
						gm.getValue().setCivilizationHolder(WorldDefinition.this);
						ifLandUnitInitEatPerHour(gm.getValue());
					}
				}
					private void ifLandUnitInitEatPerHour(GameModel unit){
						try{
							LandUnit landUnit = (LandUnit)unit;
							if( landUnit.eatPerHour == null)
								landUnit.eatPerHour = this.commonParameters.getEatPerHour();
						}
						catch(ClassCastException e){};
					}
				private void decorateGameModels() {
					decorateWithNameBoosters();
					decorateWithConvertBuildingSubstractors();
					decorateWithBoosters();
					decorateCityModelWithCityModelBoosters( (CommonCivilization) WorldDefinition.this.getCivilization(Civil.common));
				}
					private void decorateWithNameBoosters(){
						for(ICivilizationConfigProvider civilProvider : sorter.sortCivilizationConfigs() )
							decorateWithNameBoosters(civilProvider);
					}
					private void decorateWithNameBoosters( ICivilizationConfigProvider civilProvider ) {
						List<TextGroupAttacher> mtb_list = this.createTextGroupAttachersFromCivilConfig(civilProvider);
						for(TextGroupAttacher mtb : mtb_list)
							attachNameBoosterToGameModel( mtb);
					}
						private void attachNameBoosterToGameModel( TextGroupAttacher mtb) {
							IGameModel gm = this.allDecoratedGameModels.get( mtb.getModelTextBunchOwnerId() );
							NameBooster nameBooster = NameBooster.createNameBooster( mtb.textGroup , this.civilizationsMap.get(mtb.modelTextsBunchCivilization()));
							gm = nameBooster.dekoruj(gm);
							this.updateIGameModel(gm);
						}
						private List<TextGroupAttacher> createTextGroupAttachersFromCivilConfig(ICivilizationConfigProvider civilProvider)
						{
							Text[] mts = civilProvider.getNames();
							List<TextGroupAttacher> ret = new ArrayList<TextGroupAttacher>();
							if(mts == null)return ret;
							Arrays.sort(mts);
							if(mts.length < 1)return null;
							Text prior = mts[0];
							List<Text> local = new ArrayList<Text>();
							for(Text mt : mts){
								if(prior.compareTo(mt) == 0) local.add(mt);
								else {
									ret.add(new TextGroupAttacher(local,civilProvider.getCivilization()));
									local = new ArrayList<Text>();
									prior = mt;
									local.add(mt);
								}	
							}
							ret.add(new TextGroupAttacher(local,civilProvider.getCivilization()));
							return ret;
						}
					private void decorateWithConvertBuildingSubstractors() {
						
						for(ICivilizationConfigProvider civilProvider : this.allConfigs)
							decorateWithSubstractorsFromConfig(civilProvider);
					}
					private void decorateWithSubstractorsFromConfig(ICivilizationConfigProvider civilProvider) {
						
						if(civilProvider.convertBuildingSubstractors() != null){
							for(ConvertBuildingSubstractor ebooster : civilProvider.convertBuildingSubstractors() ){
								ebooster.setBoostersCivilization(this.civilizationsMap.get(civilProvider.getCivilization()));
								IGameModel convertBuilding = this.allDecoratedGameModels.get(ebooster.getMyObjectId());
								convertBuilding = ebooster.dekoruj( convertBuilding.cast(IConvertBuildingModel.class) );
								this.updateIGameModel(convertBuilding);
							}
						}
					}
						private void updateIGameModel(IGameModel gm) {
							
							this.allDecoratedGameModels.put(gm.getId(), gm);
						}
					private void decorateWithBoosters() {
						
						for(ICivilizationConfigProvider config : allConfigs)
							buildBoostersFromCivilizationConfig(config);
					}
						private void buildBoostersFromCivilizationConfig(ICivilizationConfigProvider config) {
						if(isCivilizationContainingBoosters(config)){
							for(BoosterPrototype bp : config.boosters()) {
								bp.setBoostersCivilization(WorldDefinition.this.getCivilization(config.getCivilization()));
								bp.decorateAllMatchingGameModels(allDecoratedGameModels);
							}
						}
					}
							private boolean isCivilizationContainingBoosters(
								ICivilizationConfigProvider config) {
							return config.boosters() != null;
						}
					private void decorateCityModelWithCityModelBoosters( CommonCivilization common ) {
						
						for(ICivilizationConfigProvider config : allConfigs){
							if(config.cityModelBoosters()!=null)
								for( CityModelBooster cmb : config.cityModelBoosters()){
									cmb.setBoostersCivilization(WorldDefinition.this.getCivilization(config.getCivilization()));
									common.dekorujCityModel(cmb);
								}
						}
					}
				private void setRequirementNodesForGameModels() {
					
					for(Entry<Integer,GameModel> en : allGameModelsHelper.entrySet() )
						connectRequirementNodeWithGameModel(reqBuilder, en.getValue());
				}
					private void connectRequirementNodeWithGameModel( RequirementNodeBuilder reqBuilder, GameModel en) {
						
						RequirementNode reqNode = reqBuilder.getRequirementNode(en.getId() );
						if(reqNode == null) reqNode = new RequirementNode();
						reqNode.setMyObject(allDecoratedGameModels.get(en.getId()));
						en.setRequirementNode(reqNode);
					}
			private void fillCivilizationsWithGameModels() {
				for(Entry<Integer,IGameModel> entry : allDecoratedGameModels.entrySet()){
					
					Civilization civil = civilizationsMap.get(entry.getValue().getMyCivilization().getCivilization() );
					civil.addIGameModel(entry.getValue());
				}
			}
			private void fillProducersAndSpongersInWorldDefinition() {
				
				for(Entry<Integer,IGameModel> entry : allDecoratedGameModels.entrySet()){
					addToProducersMapIfExtractBuilding(entry.getValue());
					addToProducersMapIfConvertBuilding(entry.getValue());
				}
			}
				private void addToProducersMapIfExtractBuilding( IGameModel entry) {
					
					if(entry.canCast(IExtractBuildingModel.class))
						WorldDefinition.this.addProducer( entry.cast(IExtractBuildingModel.class) );
				}	
				private void addToProducersMapIfConvertBuilding( IGameModel entry) {
					
					if(entry.canCast(IConvertBuildingModel.class ))
						WorldDefinition.this.addSponger( entry.cast(IConvertBuildingModel.class) );
				}
			/*
			 * To Decorate GameModels With NameBoosters, CivilizationConfigs have to be sorted in particular order.
			 * Consider below civilization tree:
			 * 
			 * 	1.			A
			 * 			   / \
			 *  2.        B   C
			 *           / \   \
			 *  3.      D   E   F
			 *         / \   \
			 *  4.    G   H   I
			 *  
			 *  The list has to be sorted in particular order, that each civilization in sorted list has their parent before and children
			 *  after this civilization occurance. Example:
			 *  One of sorting solutions:
			 *  List = { A , B , D , G , E , I , H , C , F }.
			 *  Lets take under consideration the 'E' civilization. It has all it's parent's (B and A)on the left side, and children( I ) 
			 *  on the right side. And this works for all civilization in list. The goal is, that the NameBoosters from civilization e.g. 'B',
			 *  were not decorated after civilization e.g. 'D' decorates the gameModel with boosters from 'D' civil. 
			 *  
			 */
			private class CivilizationConfigSorter {

				private List<Civilization> sorted = new ArrayList<Civilization>();
				private List<ICivilizationConfigProvider> ret = new ArrayList<ICivilizationConfigProvider>();
				private CollectCivilizationsInChainUpwards collector = new CollectCivilizationsInChainUpwards();
				public List<ICivilizationConfigProvider> sortCivilizationConfigs(){
					for( Civilization civil : WorldDefinition.this.allCivilizations )
						if(civilizationIsLastInChain( civil )) fillSortedList( civil);
					makeConfigListFromCivilizationList();
					return ret;
				}
					private boolean civilizationIsLastInChain( ICivilization civil) {
						if(civil.getChildCivilizations().size() == 0) return true;
						else return false;
					}
					private void fillSortedList( Civilization civil) {
						List<Civilization> chain = collectCivilizationsInChain_fromTopToBottom(civil);
						addToSortedList(sorted, chain);
					}
						private List<Civilization> collectCivilizationsInChain_fromTopToBottom(Civilization civil) {
							civil.accept(collector);
							return collector.getChainInReversedOrder();
						}
						private void addToSortedList(List<Civilization> sorted, List<Civilization> chain) {
							for(Civilization civ : chain)
								if(!sorted.contains(civ))sorted.add(civ);
						}
					private void makeConfigListFromCivilizationList() {
						for(ICivilization civil : sorted )
							findConfigForCivilizationAndAddToList(ret, civil);
					}
						private void findConfigForCivilizationAndAddToList(List<ICivilizationConfigProvider> ret, ICivilization civil) {
							for(ICivilizationConfigProvider config : allConfigs)
								if(config.getCivilization() == civil.getCivilization()) ret.add(config);
						}
			}
	}


	@Override
	public int getFurtherAdditionSatisfaction() {
		return this.commonParameters.getFurtherAdditionSatisfaction();
	}
	@Override
	public int getFurtherCarboSatisfaction() {
		return this.commonParameters.getFurtherCarboSatisfaction();
	}
	@Override
	public int getFurtherMeatSatisfaction() {
		return this.commonParameters.getFurtherMeatSatisfaction();
	}
	@Override
	public int getMajorCarboSatisfaction() {
		return this.commonParameters.getMajorCarboSatisfaction();
	}
	@Override
	public int getMajorMeatSatisfaction() {
		return this.commonParameters.getMajorMeatSatisfaction();
	}
	@Override
	public int getWaterSatisfaction() {
		return this.commonParameters.getWaterSatisfaction();
	}
	@Override
	public int satisfactionCooldownSec() {
		return this.commonParameters.satisfactionCooldownSec();
	}
	@Override
	public int getFirstCitizensAmount() {
		return this.commonParameters.getFirstCitizensAmount();
	}
	@Override
	public int getBirthMultiplier() {
		
		return this.commonParameters.getBirthMultiplier();
	}
	@Override
	public int getEatPerHour() {
		return this.commonParameters.getEatPerHour();
	}
	@Override
	public int getMajorAdditionSatisfaction() {
		
		return this.commonParameters.getMajorAdditionSatisfaction();
	}
	@Override
	public double chanceForTakingOverExtractBuilding() {
		
		return this.commonParameters.chanceForTakingOverExtractBuilding();
	}
	@Override
	public int getMaxFightRounds() {
		
		return this.commonParameters.getMaxFightRounds();
	}
	@Override
	public Double getPeopleFallRate() {
		return this.commonParameters.getPeopleFallRate();
	}
}
