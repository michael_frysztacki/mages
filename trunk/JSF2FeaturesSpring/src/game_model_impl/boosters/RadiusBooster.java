package game_model_impl.boosters;

import entities.UserConfig;
import game_model_impl.Civilization;
import game_model_interfaces.IGameModel;
import game_model_interfaces.IRadialResearchModel;

public class RadiusBooster extends BoosterDecorator<IRadialResearchModel>
{

	public RadiusBooster(int myObjectId, int boostPatternId) {
		super(myObjectId, boostPatternId);
	}
	
	@Override
	public int getRadius(UserConfig uc)
	{
		if(uc.getResearch(myObjectId)== null)return element.getRadius(uc);
		Civilization config = this.getBoostersCivilization();
		IGameModel gm = this.element.getInternalGameModel();
		IRadialResearchModel internalResearch = gm.cast(IRadialResearchModel.class);
		int result = config.boostRadius(internalResearch.getRadius(uc), uc.getLevel(this.myObjectId), this.boostPatternId);
		int added = result - internalResearch.getRadius(uc);
		return added + this.element.getRadius(uc);
	}
	
}
