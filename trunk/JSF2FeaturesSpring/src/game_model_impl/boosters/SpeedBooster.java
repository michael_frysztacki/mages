package game_model_impl.boosters;

import entities.UserConfig;
import game_model_impl.Civilization;
import game_model_interfaces.IGameModel;
import game_model_interfaces.ILandUnitModel;
import game_model_interfaces.IUnitModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SpeedBooster extends BoosterDecorator<IUnitModel>{

	/*
	 * je�li nie podany �adnego array type, oznacza to �e ulepszenie dzia�a na wszystkich terenach.
	 */
	public SpeedBooster(int myObjectId, int boostPatternId, TerainType... terain) {
		super(myObjectId, boostPatternId);
		if(terain.length == 0){
			boostTypes = new ArrayList<TerainType>();
			for(TerainType type : TerainType.values())
				boostTypes.add(type);
		}
		else this.boostTypes = Arrays.asList(terain) ;
	}
	private final List<TerainType> boostTypes;
	@Override
	public int getSpeed(UserConfig uc, TerainType terain)
	{
		ILandUnitModel unitModel = element.cast(ILandUnitModel.class);
		if(boostTypes.contains(terain))
		{
			if(uc.getResearch(myObjectId)== null)return unitModel.getSpeed(uc, terain);
			Civilization config = this.getBoostersCivilization();
			IGameModel gm = this.element.getInternalGameModel();
			ILandUnitModel internalUnit = gm.cast(ILandUnitModel.class);
			int result = config.boostSpeed(internalUnit.getSpeed(uc,terain) , uc.getLevel(this.myObjectId), this.boostPatternId);
			int added = result - internalUnit.getSpeed(uc,terain);
			return added + unitModel.getSpeed(uc,terain);
		}
		return unitModel.getSpeed(uc, terain);
		
	}

}
