package game_model_impl.boosters;

import game_model_impl.Civilization;
import game_model_impl.Text.LanguagePair;
import game_model_impl.Text.Text;
import game_model_impl.Text.Text.Language;
import game_model_impl.Text.Text.ModelTextType;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IGameModel;
import helpers.LevelHelper;

import java.util.ArrayList;
import java.util.List;

import entities.UsersCityConfig;

public class NameBooster extends BoosterDecorator<IGameModel>{
	
	protected NameBooster(Civilization civilization){
		super(-1 ,-1);
		this.setBoostersCivilization(civilization);
	}
	
	public static NameBooster createNameBooster(List<Text> textGroup , Civilization boostersCivilization){
		if(textGroup.get(0).type == ModelTextType.regular )
			return new RegularNameBooster( boostersCivilization, textGroup.get(0) );
		else 
			return new ImprovementNameBooster(boostersCivilization, textGroup);
	}
	
	private class ModelText {
		public final List<LanguagePair> langPair;
		public final int reqLvl;
		public ModelText(int reqLvl, List<LanguagePair> langPair){
			this.reqLvl = reqLvl;
			this.langPair = langPair;
		}
		protected String getNameForLanguage(Language lang){
			for(LanguagePair lp : langPair)
				if(lp.first == lang) return lp.second;
			return null;
		}
	}
	
	public static class ImprovementNameBooster extends NameBooster{

		private List<ModelText> modelTexts = new ArrayList<ModelText>();
		private int changerId;
		public ImprovementNameBooster(Civilization civilization, List<Text> textGroup) {
			super(civilization);
			changerId = textGroup.get(0).changerId;
			for(Text text :  textGroup){
				modelTexts.add(new ModelText(text.reqLvl,  text.getLangPairs() ) );
			}
		}
		
		@Override
		public String getName(UsersCityConfig uc)
		{
			ICivilization civilNode = this.getCivilizationHolder().findCivilizationInChain(this.getBoostersCivilization().getCivilization() , uc.getUserConfig().getCivil() );
			if(civilNode == null)return element.getName(uc);
			/*
			 * pobieramy zewn�trzny obiekt, kt�ry wp�ywa na nazw� obiektu w tym �a�cuchu.
			 */
			//IGameModel changer = uc.getUserConfig().getMyCivilization().getById(myObjectId, IGameModel.class);
			/*
			 * je�li chager jest null, tzn. �e myObjectId jest ustawione na "-1", a to znaczy �e ten obiekt patrzy na sw�j w�asny level;
			 */
			//if(changer == null) changer = this;
			int changerLvl = LevelHelper.getBuildingOrResearchLvl( uc, uc.getUserConfig().getMyCivilization().getById(changerId, IGameModel.class) );
			ModelText mt = getModelTextByChangerLevel(modelTexts.toArray(new ModelText[modelTexts.size()]), changerLvl);
			if(mt == null) return element.getName(uc);
			String mtb_string = mt.getNameForLanguage(uc.getUserConfig().getLanguage());
			/*
			 * mo�e si� zdarzy�, �e mtb.getString zwr�ci null. je�li mtb jest typu ImprovementModelTextBunch,
			 * wtedy przegl�dana jest tablica wymaganych level i rozpatrywane jest kt�r� nazw� wybra�.
			 * np. 	od lvl 3 - "nazwa 1"
			 * 		od lvl 7 - "nazwa 2"
			 * 
			 * je�li level obiektu myObjectId jest r�wny 5, wtedy zwracana nazwa to "nazwa 1", je�li level jest 7 i wy�ej
			 * wtedy zwracana nazwa to "nazwa 2". Je�li lvl jest poni�ej 3 lvl, to znaczy ze w tabeli nie ma odpowiedniej nazwy
			 * i trzeba szuka� nazwy w kolejnym elemencie. tzn. w tym boosterze nie ma odpowiedniej nazwy dla lvl 2.
			 */
			 return mtb_string;
			
		}
		
		private ModelText getModelTextByChangerLevel(ModelText[] array, Integer changerIdLvl){
			for(int i = array.length-1 ; i>=0 ; i--)	
				if(changerIdLvl >= array[i].reqLvl )return array[i];
			return null;
		}
		
	}
	
	public static class RegularNameBooster extends NameBooster{
		private ModelText modelText;
		public RegularNameBooster(Civilization civilization, Text text) {
			super(civilization);
			modelText = new ModelText(text.reqLvl, text.getLangPairs() );
		}
		
		@Override
		public String getName(UsersCityConfig uc)
		{
			ICivilization civilNode = this.getCivilizationHolder().findCivilizationInChain(this.getBoostersCivilization().getCivilization() , uc.getUserConfig().getCivil() );
			if(civilNode == null)return element.getName(uc);
			/*
			 * pobieramy zewn�trzny obiekt, kt�ry wp�ywa na nazw� obiektu w tym �a�cuchu.
			 */
			//IGameModel changer = uc.getUserConfig().getMyCivilization().getById(myObjectId, IGameModel.class);
			/*
			 * je�li chager jest null, tzn. �e myObjectId jest ustawione na "-1", a to znaczy �e ten obiekt patrzy na sw�j w�asny level;
			 */
			//if(changer == null) changer = this;
			String mtb_string = modelText.getNameForLanguage(uc.getUserConfig().getLanguage());
			/*
			 * mo�e si� zdarzy�, �e mtb.getString zwr�ci null. je�li mtb jest typu ImprovementModelTextBunch,
			 * wtedy przegl�dana jest tablica wymaganych level i rozpatrywane jest kt�r� nazw� wybra�.
			 * np. 	od lvl 3 - "nazwa 1"
			 * 		od lvl 7 - "nazwa 2"
			 * 
			 * je�li level obiektu myObjectId jest r�wny 5, wtedy zwracana nazwa to "nazwa 1", je�li level jest 7 i wy�ej
			 * wtedy zwracana nazwa to "nazwa 2". Je�li lvl jest poni�ej 3 lvl, to znaczy ze w tabeli nie ma odpowiedniej nazwy
			 * i trzeba szuka� nazwy w kolejnym elemencie. tzn. w tym boosterze nie ma odpowiedniej nazwy dla lvl 2.
			 */
			if(mtb_string != null) return mtb_string;
			else return element.getName(uc);
			
		}

	}
	
	@Override
	public String printDecoratorChain(){
		return this.element.printDecoratorChain() + 
				"<- " + this.getClass().getName()+"(civil: "+ this.getMyCivilization().getCivilization();
	}


}
