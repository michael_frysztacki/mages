package game_model_impl.boosters.convert_building;

import java.util.List;

import entities.UsersCityConfig;
import game_model_impl.Civilization;
import game_model_impl.StockType;
import game_model_impl.boosters.BoosterDecorator;
import game_model_interfaces.IConvertBuildingModel;

public class ConvertBuildingInputBooster extends BoosterDecorator<IConvertBuildingModel>{
	/*
	 * baseSponging - jest to ilosc, jak� zabiera budynek conwertuj�cy, gdy jego level jest = 1,
	 * innym s�owy jest to warto�� startowa z jak� budynek konwertuj�cy ma zabiera� surowiec.
	 */
	public ConvertBuildingInputBooster(int myObjectId, int boostPatternId, StockType stock) {
		super(myObjectId, boostPatternId);
		this.stock = stock;
	}
	
	private final StockType stock; 
	
	public int getInputPerHour(UsersCityConfig ucc, StockType resource)
	{
		int elementsInput = element.getInputPerHour(ucc, resource);
		if(resource == stock)
		{
			Civilization civil = this.getBoostersCivilization();
			return civil.getImprovedInputByPattern( elementsInput, ucc.getUserConfig().getLevel(myObjectId), this.boostPatternId);
		}
		else return elementsInput;
	}
	
	@Override
	public List<StockType> getInputTypes()
	{
		List<StockType> list = element.getInputTypes();
		list.add(stock);
		return list;
	}
	
}
