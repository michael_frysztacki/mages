package game_model_impl.boosters.convert_building;

import java.util.List;

import entities.UsersCityConfig;
import game_model_impl.Civilization;
import game_model_impl.StockType;
import game_model_impl.boosters.BoosterDecorator;
import game_model_interfaces.IConvertBuildingModel;

public class ConvertBuildingSubstractor extends BoosterDecorator<IConvertBuildingModel>{
	/*
	 * baseSponging - jest to ilosc, jak� zabiera budynek conwertuj�cy, gdy jego level jest = 1,
	 * innym s�owy jest to warto�� startowa z jak� budynek konwertuj�cy ma zabiera� surowiec.
	 */
	public ConvertBuildingSubstractor(int myObjectId, int boostPatternId, StockType stock) {
		super(myObjectId, boostPatternId);
		this.stock = stock;
	}
	
	private final StockType stock; 
	
	public int getInputPerHour(UsersCityConfig ucc, StockType resource)
	{
		if(resource == stock)
		{
			//Civilization civil = element.getCivilizationHolder().getCivilization( this.getCivil() );
			Civilization civil = this.getBoostersCivilization();
			IConvertBuildingModel convertBuilding=null;
				convertBuilding = this.getRequirementNode().getMyGameModel().cast(IConvertBuildingModel.class);
			System.out.print("\n"+this.getId()+stock.toString()+"\n");
			int patternId = civil.getSpongingPatternById(this.getId()+"_"+stock.toString());
			return civil.getSpongingByPattern( convertBuilding.getPureOutputPerHour(ucc),  patternId);
		}
		else return element.getInputPerHour(ucc, resource);
	}
	
//	@Override
//	public int potentiallyHighestInputPerHour(UsersCityConfig ucc, StockType resource)
//	{
//		if(resource == stock)
//		{
//			Civilization civil = element.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
//			IConvertBuildingModel convertBuilding = civil.getById( element.getId() , IConvertBuildingModel.class);
//			return civil.getSpongingByPattern( convertBuilding.potentiallyHighestOutputPerHour(ucc) , this.boostPatternId);
//		}
//		else return element.potentiallyHighestInputPerHour(ucc, resource);
//	}
	
	@Override
	public List<StockType> getInputTypes()
	{
		List<StockType> list = element.getInputTypes();
		list.add(stock);
		return list;
	}
	
}
