package game_model_impl.boosters;

import entities.UserConfig;
import entities.UsersCityConfig;
import game_model_impl.Civilization;
import game_model_interfaces.IGameModel;
import game_model_interfaces.IUnitModel;

public class AttackBooster extends BoosterDecorator<IUnitModel>{

	public AttackBooster(int myObjectId, int boostPatternId) {
		super(myObjectId, boostPatternId);
		
	}
	@Override 
	public int getAttackInsideCity(UsersCityConfig ucc){
		int attack = this.getAttackOutsideCity(ucc.getUserConfig());
		IGameModel internal = this.getInternalGameModel();
		IUnitModel unit = internal.cast(IUnitModel.class);
		attack -= unit.getAttackOutsideCity(ucc.getUserConfig());
		attack += unit.getAttackInsideCity(ucc);
		return attack;
	}
	
	@Override
	public int getAttackOutsideCity(UserConfig uc)
	{
		if(uc.getResearch(myObjectId)== null)return element.getAttackOutsideCity(uc);
		Civilization config = this.getBoostersCivilization();
		IGameModel gm = this.element.getInternalGameModel();
		IUnitModel internalUnit = gm.cast(IUnitModel.class);
		int result = config.boostAttack(  this.boostPatternId, uc.getLevel(this.myObjectId), internalUnit.getAttackOutsideCity(uc) );
		int added = result - internalUnit.getAttackOutsideCity(uc);
		return added + this.element.getAttackOutsideCity(uc);
	}
	
}
