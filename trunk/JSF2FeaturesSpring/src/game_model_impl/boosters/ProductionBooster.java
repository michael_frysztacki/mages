package game_model_impl.boosters;

import game_model_impl.Civilization;
import game_model_interfaces.IConvertBuildingModel;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IGameModel;
import helpers.LevelHelper;
import entities.UsersCityConfig;

public class ProductionBooster extends BoosterDecorator<IConvertBuildingModel>{

	public ProductionBooster(int myObjectId, int boostPatternId) {
		super(myObjectId, boostPatternId);
		
	}

	@Override
	public int getOutputPerHour(UsersCityConfig uc)
	{
		Civilization config = this.getBoostersCivilization();
		IGameModel changer = config.getById(myObjectId, IGameModel.class);
		if( LevelHelper.getBuildingOrResearchLvl(uc, changer)== null)return element.getOutputPerHour(uc);
		IGameModel gm = this.element.getInternalGameModel();
		IExtractBuildingModel internalEBuilding = gm.cast(IExtractBuildingModel.class);
		//System.out.print("\n" + internalEBuilding.getOutputPerHour(uc) +","+ uc.getLevel(this.myObjectId)+","+ this.boostPatternId + "\n") ;
		int result = config.getImprovedProductionByPattern(internalEBuilding.getOutputPerHour(uc) , LevelHelper.getBuildingOrResearchLvl(uc, changer), this.boostPatternId);
		int added = result - internalEBuilding.getOutputPerHour(uc);
		return added + this.element.getOutputPerHour(uc);
	}

}
