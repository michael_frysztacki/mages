package game_model_impl.boosters;

import entities.Civil;
import entities.UserConfig;
import entities.UsersCityConfig;
import game_model_impl.Civilization;
import game_model_impl.GameModel;
import game_model_impl.PriceStockSetImpl;
import game_model_impl.RequirementNode;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IConvertBuildingModel;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IFortyficationModel;
import game_model_interfaces.IGameModel;
import game_model_interfaces.IHousesModel;
import game_model_interfaces.IImprovementModel;
import game_model_interfaces.ILandUnitModel;
import game_model_interfaces.IRadialResearchModel;
import game_model_interfaces.IRequirementNode;
import game_model_interfaces.IResearchModel;
import game_model_interfaces.IUnitModel;
import game_model_interfaces.PriceStockSet;
import helpers.Three;

import java.util.List;

public abstract class BoosterDecorator<T extends IGameModel> implements IBuildingModel, IHousesModel, IResearchModel, IUnitModel, ILandUnitModel,IExtractBuildingModel ,IConvertBuildingModel, IImprovementModel, IFortyficationModel, IRadialResearchModel, Cloneable{

	protected T element;
	@Override
	public final GameModel getInternalGameModel()
	{
		return this.element.getInternalGameModel();
	}
	@Override
	public String toString(){
		return "id: " + this.element.getId();
	}
	
	public GameModel getGameModel(){
		try{
			BoosterDecorator<?> next = (BoosterDecorator<?>) element;
			return next.getGameModel();
		}
		catch(ClassCastException e){
			return (GameModel)element;
		}
	}
	
	@Override
	public boolean equals(Object o){
		IGameModel gm = (IGameModel) o;
		return element.equals(gm);
	}
	/*
	 * param:
	 * 	element - obiekt, kt�ry dekorujemy
	 */
	public IGameModel dekoruj(IGameModel element){
		this.element =  (T) element;
		return this;
	}
	protected final int myObjectId;
	public int getMyObjectId()
	{
		return myObjectId;
	}
	protected final int boostPatternId;
	@Override
	public ICivilization getMyCivilization()
	{
		return element.getMyCivilization();
	}
	protected Civilization getBoostersCivilization()
	{
		return boostersCivil;
	}
	private Civilization boostersCivil;
	public void setBoostersCivilization(Civilization civil)
	{
		this.boostersCivil = civil;
	}
	@Override
	public Object clone()
	{
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	@Override
	public int getId()
	{
		return element.getId();
	}
	@Override
	public int getPeopleLimit(UsersCityConfig ucc)
	{
		return ((IHousesModel)element).getPeopleLimit(ucc);
	}
	@Override
	public AttackType getAttackType(UserConfig uc)
	{
		return ((IUnitModel)element).getAttackType(uc);
	}
	@Override
	public WorldDefinition getCivilizationHolder()
	{
		return element.getCivilizationHolder();
	}
	@Override
	public String printDecoratorChain()
	{
		return this.element.printDecoratorChain() + "<- " + this.getClass().getName();
	}
	
	@Override
	public String dump(Language lang, Civil civil)
	{
		return this.element.dump(lang, civil);
	}
	
	@Override
	public String dumpBody(Language lang, Civil civil)
	{
		return this.element.dumpBody(lang, civil);
	}
	
	@Override
	public String getSimpleName(Language lang)
	{
		return element.getSimpleName(lang);
	}
	
	@Override
	public List<Three<Integer, String, String>> dumpHeader(Language lang, Civil civil)
	{
		return this.element.dumpHeader(lang, civil);
	}
	@Override
	public Class<? extends IGameModel> getObjectsClass()
	{
		return element.getObjectsClass() ;
	}
	public BoosterDecorator(int myObjectId, int boostPatternId)
	{
		this.myObjectId = myObjectId;
		this.boostPatternId = boostPatternId;
	}
	@Override
	public int getInputPerHour(UsersCityConfig ucc, StockType resource) {
		
		return ((IConvertBuildingModel)element).getInputPerHour(ucc, resource);
	}
	@Override
	public int getPureOutputPerHour(UsersCityConfig ucc)
	{
		return ((IConvertBuildingModel)element).getPureOutputPerHour(ucc);
	}
	@Override
	public Boolean isExtractionImprovement()
	{
		return ((IImprovementModel)element).isExtractionImprovement();
	}
	@Override
	public Double getProductionRate(UsersCityConfig ucc)
	{
		return ((IConvertBuildingModel)element).getProductionRate(ucc) ;
	}
	@Override
	public int getSpeed(UserConfig uc, TerainType type)
	{
		return ((ILandUnitModel)element).getSpeed(uc, type);
	}
	@Override
	public double getEatPerHour(UserConfig uc)
	{
		return ((ILandUnitModel)element).getEatPerHour(uc);
	}
	@Override
	public List<StockType> getInputTypes()
	{
		return ((IConvertBuildingModel)element).getInputTypes();
	}
	@Override
	public StockType getResourceType()
	{
		return ((IExtractBuildingModel)element).getResourceType();
	}
	@Override
	public List<IUnitModel> getUnits(UserConfig city)
	{
		return ((IBuildingModel)element).getUnits(city);
	}
	@Override
	public List<IResearchModel> getResearches(UsersCityConfig city)
	{
		return ((IBuildingModel)element).getResearches(city);
	}
	//@SuppressWarnings("unchecked")// w bloku try sprawdzam najpeirw czy da sie skastowa�
	@SuppressWarnings("unchecked")
	@Override
	public <E extends IGameModel> E cast(Class<E> clazz){
		if( element.cast(clazz) != null)
			return (E) this;
		return null;
	}
	public <E extends IGameModel> boolean canCast(Class<E> clazz){
		return element.canCast(clazz);
	}
	@Override
	public String getName(UsersCityConfig uc)
	{
		return ((IGameModel)element).getName(uc);
	}
	@Override
	public String getDescription(UsersCityConfig uc)
	{
		return ((IGameModel)element).getDescription(uc);
	}
	@Override
	public int getMaxPeople(UsersCityConfig uc)
	{
		return ((IExtractBuildingModel)element).getMaxPeople(uc);
	}
	
	@Override
	public int getFortyficationHP(UsersCityConfig ucc ){
		return ((IFortyficationModel)element).getFortyficationHP(ucc);
	}
	@Override
	public int getRepairRate(UsersCityConfig ucc){
		return ((IFortyficationModel)element).getRepairRate(ucc);
	}
	
	@Override
	public int getRadius(UserConfig uc){
		return ((IRadialResearchModel)element).getRadius(uc);
	}
	
	@Override
	public int getHP(UserConfig uc)
	{
		return ((IUnitModel)element).getHP(uc);
	}
	@Override
	public int getArmorOutsideCity(UserConfig uc)
	{
		return ((IUnitModel)element).getArmorOutsideCity(uc);
	}
	@Override
	public int getArmorInsideCity(UsersCityConfig ucc)
	{
		return ((IUnitModel)element).getArmorInsideCity(ucc);
	}
	@Override
	public int getCapacity(UserConfig uc)
	{
		return ((IUnitModel)element).getCapacity(uc);
	}
	
	@Override
	public IRequirementNode getRequirementNode()
	{
		return element.getRequirementNode();
	}
	@Override
	public int getOutputPerHour(UsersCityConfig ucc)  
	{
		return ((IExtractBuildingModel)element).getOutputPerHour(ucc);
	}
	/*
	 * cena dla budynku.
	 * Cena budynku jest zale�na od miasta, dlatego �e w r�nych miastach level budynku mo�e by� inny.
	 */
	@Override
	public PriceStockSet getPrice(UsersCityConfig ucc) {
		return element.getPrice(ucc);
	}
	/*
	 * cena dla jednostki i badania. Cena jednostki i badania jest zale�na tylko od bada� gracza.
	 */
//	@Override
//	public PriceStockSet getPrice(UsersCityConfig uc) {
//	
//		try{
//			IResearchModel research = (IResearchModel)element;
//			return research.getPrice(uc);
//		}
//		catch(ClassCastException e)
//		{
//			IUnitModel unit = (IUnitModel)element;
//			return unit.getPrice(uc);
//		}
//	}
	@Override
	public Integer getBuildTime(UsersCityConfig uc)  {
		return element.getBuildTime(uc);
	}
	
	@Override
	public int getMaxLevel() {
		return ((IResearchModel)element).getMaxLevel();
	}
	@Override
	public int getAttackOutsideCity(UserConfig uc) {
		return ((IUnitModel)element).getAttackOutsideCity(uc);
	}
	@Override
	public int getAttackInsideCity(UsersCityConfig ucc) {
		return ((IUnitModel)element).getAttackInsideCity(ucc);
	}
	@Override
	public ArmorType getArmorType(UserConfig uc) {
		return ((IUnitModel)element).getArmorType(uc);
	} 

}
