package game_model_impl.boosters;

import entities.UsersCityConfig;
import game_model_impl.Civilization;
import game_model_interfaces.IFortyficationModel;
import game_model_interfaces.IGameModel;
import helpers.LevelHelper;

public class FortyficationRepairBooster extends BoosterDecorator<IFortyficationModel>{

	public FortyficationRepairBooster(int myObjectId, int boostPatternId) {
		super(myObjectId, boostPatternId);
	}
	
	@Override
	public int getRepairRate(UsersCityConfig ucc)
	{
		Civilization config = this.getBoostersCivilization();
		IGameModel changer = config.getById(myObjectId, IGameModel.class);
		if( LevelHelper.getBuildingOrResearchLvl(ucc, changer)== null)return element.getRepairRate(ucc);
		IGameModel gm = this.element.getInternalGameModel();
		IFortyficationModel internalModel = gm.cast(IFortyficationModel.class);
		int result = config.boostFortyficationRepairRate(internalModel.getRepairRate(ucc), LevelHelper.getBuildingOrResearchLvl(ucc, changer),  this.boostPatternId);
		int added = result - internalModel.getRepairRate(ucc);
		return added + this.element.getRepairRate(ucc);
		
	}

}
