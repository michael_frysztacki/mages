package game_model_impl.boosters;

import game_model_impl.Civilization;
import game_model_interfaces.IGameModel;
import helpers.LevelHelper;
import entities.UsersCityConfig;

public class BuildTimeBooster extends BoosterDecorator<IGameModel>{

	public BuildTimeBooster(int myObjectId, int boostPatternId) {
		super(myObjectId, boostPatternId);
	}
	
	@Override
	public Integer getBuildTime(UsersCityConfig ucc)
	{
		Civilization config = this.getBoostersCivilization();
		IGameModel changer = config.getById(myObjectId, IGameModel.class);
		if( LevelHelper.getBuildingOrResearchLvl(ucc, changer)== null)return element.getBuildTime(ucc);
		IGameModel gm = this.element.getInternalGameModel();
		IGameModel internalModel = gm.cast(IGameModel.class);
		int result = config.boostBuildTimePattern(internalModel.getBuildTime(ucc), LevelHelper.getBuildingOrResearchLvl(ucc, changer), this.boostPatternId);
		int added = result - internalModel.getBuildTime(ucc);
		return added + this.element.getBuildTime(ucc);
		
	}

}
