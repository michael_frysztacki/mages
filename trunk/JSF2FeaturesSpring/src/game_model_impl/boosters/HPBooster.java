package game_model_impl.boosters;

import entities.UserConfig;
import game_model_interfaces.IGameModel;
import game_model_interfaces.IUnitModel;

public class HPBooster extends BoosterDecorator<IUnitModel>
{

	public HPBooster(int myObjectId, int boostPatternId) {
		super(myObjectId, boostPatternId);
	}
	@Override
	public int getHP(UserConfig uc){
		if(!haveResearch(uc))return element.getHP(uc);
		int addedHP = boostHPUponBaseHP(uc) - getBaseHP(uc);
		return addedHP + this.element.getHP(uc);
	}
		private boolean haveResearch(UserConfig uc) {
			return uc.getResearch(myObjectId)!= null;
		}
		private int boostHPUponBaseHP(UserConfig uc){
			return getBoostersCivilization().boostHP(getBaseHP(uc) , uc.getLevel(this.myObjectId), this.boostPatternId);
		}
		private int getBaseHP(UserConfig uc) {
			IGameModel gm = this.element.getInternalGameModel();
			IUnitModel internalUnit = gm.cast(IUnitModel.class);
			return internalUnit.getHP(uc);
		}
	
}
