package game_model_impl.boosters;

import entities.UsersCityConfig;
import game_model_impl.Civilization;
import game_model_interfaces.IFortyficationModel;
import game_model_interfaces.IGameModel;
import helpers.LevelHelper;

public class FortyficationHPBooster extends BoosterDecorator<IFortyficationModel>{

	public FortyficationHPBooster(int myObjectId, int boostPatternId) {
		super(myObjectId, boostPatternId);
	}
	
	@Override
	public int getFortyficationHP(UsersCityConfig ucc) {
		Civilization config = this.getBoostersCivilization();
		IGameModel changer = config.getById(myObjectId, IGameModel.class);
		if( LevelHelper.getBuildingOrResearchLvl(ucc, changer)== null)return element.getFortyficationHP(ucc);
		IGameModel gm = this.element.getInternalGameModel();
		IFortyficationModel internalModel = gm.cast(IFortyficationModel.class);
		int result = config.boostFortyficationHP(internalModel.getFortyficationHP(ucc), LevelHelper.getBuildingOrResearchLvl(ucc, changer),  this.boostPatternId);
		int added = result - internalModel.getFortyficationHP(ucc);
		return added + this.element.getFortyficationHP(ucc);
		
	}

}
