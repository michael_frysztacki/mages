package game_model_impl.boosters;

import entities.UserConfig;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IUnitModel;

public class ArmorTypeBooster extends BoosterDecorator<IUnitModel>{

	public ArmorTypeBooster(int myObjectId, int boostPatternId) {
		super(myObjectId, boostPatternId);
	}
	
	@Override
	public ArmorType getArmorType(UserConfig uc)
	{
		ICivilization civil = uc.getMyCivilization();
		return civil.boostArmorType(this.boostPatternId, uc.getLevel(myObjectId), element.getArmorType(uc));
		
	}
	

}
