package game_model_impl.boosters;

import entities.UserConfig;
import game_model_impl.Civilization;
import game_model_interfaces.IGameModel;
import game_model_interfaces.IUnitModel;

public class CapacityBooster extends BoosterDecorator<IUnitModel>{

	public CapacityBooster(int myObjectId, int boostPatternId) {
		super(myObjectId, boostPatternId);
	}
	
	@Override
	public int getCapacity(UserConfig uc){
		if(uc.getResearch(myObjectId)== null)return element.getCapacity(uc);
		Civilization config = this.getBoostersCivilization();
		IGameModel gm = this.element.getInternalGameModel();
		IUnitModel internalUnit= null;
		internalUnit = gm.cast(IUnitModel.class);
		int result = config.boostCapacity(internalUnit.getCapacity(uc) , uc.getLevel(this.myObjectId), this.boostPatternId);
		int added = result - internalUnit.getCapacity(uc);
		return added + this.element.getCapacity(uc);
	}

}
