package game_model_impl.boosters;

import entities.UserConfig;
import entities.UsersCityConfig;
import game_model_impl.Civilization;
import game_model_interfaces.IGameModel;
import game_model_interfaces.IUnitModel;


public class ArmorBooster extends BoosterDecorator<IUnitModel>{

	public ArmorBooster(int myObjectId, int boostPatternId) {
		super(myObjectId, boostPatternId);
	}
	@Override 
	public int getArmorInsideCity(UsersCityConfig uc){
		int armor = this.getArmorInsideCity(uc);
		IGameModel internal = this.getInternalGameModel();
		IUnitModel unit = internal.cast(IUnitModel.class);
		armor -= unit.getArmorOutsideCity(uc.getUserConfig());
		armor += unit.getArmorInsideCity(uc);
		return armor;
	}
	
	@Override
	public int getArmorOutsideCity(UserConfig uc)
	{
		if(uc.getResearch(myObjectId)== null)return element.getArmorOutsideCity(uc);
		Civilization config = this.getBoostersCivilization();
		IGameModel gm = this.element.getInternalGameModel();
		IUnitModel internalUnit= null;
		internalUnit = gm.cast(IUnitModel.class);
		int result = config.ArmorBoostPattern(internalUnit.getArmorOutsideCity(uc) , uc.getLevel(this.myObjectId), this.boostPatternId);
		int added = result - internalUnit.getArmorOutsideCity(uc);
		return added + this.element.getArmorOutsideCity(uc);
		
	}
	
	
	

}
