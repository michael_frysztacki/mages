package game_model_impl.boosters;

import entities.UserConfig;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IUnitModel;

public class AttackTypeBooster extends BoosterDecorator<IUnitModel>{

	public AttackTypeBooster(int myObjectId, int boostPatternId) {
		super(myObjectId, boostPatternId);
	}
	
	@Override
	public AttackType getAttackType(UserConfig uc)
	{
		ICivilization civil = uc.getMyCivilization();
		return civil.boostAttackType(this.boostPatternId, uc.getLevel(myObjectId), element.getAttackType(uc));
	}
	

}
