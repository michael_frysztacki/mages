package game_model_impl.boosters;

import game_model_impl.Civilization;
import game_model_interfaces.IExtractBuildingModel;
import game_model_interfaces.IGameModel;
import helpers.LevelHelper;
import entities.UsersCityConfig;

public class PeopleBooster extends BoosterDecorator<IExtractBuildingModel>{

	public PeopleBooster(int myObjectId, int boostPatternId) {
		super(myObjectId, boostPatternId);
		
	}
	@Override
	public int getMaxPeople(UsersCityConfig uc){
		Civilization config = this.getBoostersCivilization();
		IGameModel changer = config.getById(myObjectId, IGameModel.class);
		if( LevelHelper.getBuildingOrResearchLvl(uc, changer)== null)return element.getMaxPeople(uc);
		IGameModel gm = this.element.getInternalGameModel();
		IExtractBuildingModel internalEBuilding = gm.cast(IExtractBuildingModel.class);
		System.out.print("\n" + internalEBuilding.getMaxPeople(uc) +","+ uc.getLevel(this.myObjectId)+","+ this.boostPatternId + "\n") ;
		int result = config.maxPeopleBoostPattern(internalEBuilding.getMaxPeople(uc) , LevelHelper.getBuildingOrResearchLvl(uc, changer), this.boostPatternId);
		int added = result - internalEBuilding.getMaxPeople(uc);
		return added + this.element.getMaxPeople(uc);
	}
	

}
