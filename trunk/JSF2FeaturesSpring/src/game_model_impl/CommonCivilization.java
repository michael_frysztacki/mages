package game_model_impl;

import entities.Civil;
import entities.UserConfig;
import game_model_impl.city_model.CityModel;
import game_model_impl.city_model.CityModelBooster;
import game_model_interfaces.ICityModel;
import game_model_interfaces.IGameModel;
import game_model_interfaces.IHousesModel;
import game_model_interfaces.IUnitModel.ArmorType;
import game_model_interfaces.IUnitModel.AttackType;
import game_model_interfaces.civilization_defining_interfaces.ICivilizationConfigProvider;
import game_model_interfaces.initializations.ICityInitializator;

import java.util.ArrayList;
import java.util.List;


public class CommonCivilization extends Civilization{
	
	private ICityModel cityModel = new CityModel();
	CommonCivilization( ICivilizationConfigProvider civilProvider ){
		this.civilProvider = civilProvider;
	}
	
	void dekorujCityModel(CityModelBooster booster){
		cityModel = booster.dekoruj(cityModel);
	}

	@Override
	public String getCivilization(){
		return Civil.common.toString();
	}
	@Override
	public Integer getSpongingByPattern(int productionOfOutcomeResource, int patternId) {
		//patternId = CivilBinaryConverter.getPatternIdFromMergedPatternId(patternId);
		//Integer ret = this.civilProvider.getSpongingByPrivatePattern(productionOfOutcomeResource, patternId);
		//if(ret==null)
			return this.civilProvider.getSpongingByPattern(productionOfOutcomeResource, patternId);
		//return ret;
	}
	
	@Override
	public PriceStockSetImpl getCostByPattern(PriceStockSetImpl baseCost, int patternId, int lvl, UserConfig uc) {
		/*
		patternId = CivilBinaryConverter.getPatternIdFromMergedPatternId(patternId);
		PriceStockSet ret = this.civilProvider.getCostByPrivatePattern(baseCost, patternId, lvl, uc);
		if(ret==null)
		return ret;
		*/
		return this.civilProvider.getCostByPattern(baseCost, patternId, lvl, uc);
	}
	@Override
	public Integer getProductionByPattern(int peopleCount, int patternId) {
		/*
		patternId = CivilBinaryConverter.getPatternIdFromMergedPatternId(patternId);
		Integer ret= this.civilProvider.getProductionByPrivatePattern(peopleCount, patternId);
		if(ret==null)
		return ret;
		*/
		return this.civilProvider.getProductionByPattern(peopleCount, patternId);
	}
	@Override
	public Integer getExtractionByPattern(Integer peopleCount, Integer lodeFactor, int patternId) {
		/*
		patternId = CivilBinaryConverter.getPatternIdFromMergedPatternId(patternId);
		Integer ret = this.civilProvider.getExtractionByPrivatePattern(peopleCount, lodeFactor, patternId);
		if(ret==null)
		return ret;
		*/
		return this.civilProvider.getExtractionByPattern(peopleCount, lodeFactor, patternId);
		
	}
	@Override
	public Integer getMaxPeopleByPattern(int baseAmount, int buildingLvl, int patternId) {
		/*
		patternId = CivilBinaryConverter.getPatternIdFromMergedPatternId(patternId);
		Integer ret = this.civilProvider.getMaxPeopleByPrivatePattern(baseAmount, buildingLvl, patternId);
		if(ret==null)
		return ret;
		*/
		return this.civilProvider.getMaxPeopleByPattern(baseAmount, buildingLvl, patternId);
	}
	@Override
	public Integer getResearchBuildTimeByPattern(Long timeFactor, int rlvl, int blvl, int patternId) {
		/*
		patternId = CivilBinaryConverter.getPatternIdFromMergedPatternId(patternId);
		Integer ret = this.civilProvider.getResearchBuildTimeByPrivatePattern(timeFactor, rlvl, blvl, patternId);
		if(ret==null)return this.civilProvider.getResearchBuildTimeByPattern(timeFactor, rlvl, blvl, patternId);
		return ret;
		*/
		return this.civilProvider.getResearchBuildTimeByPattern(timeFactor, rlvl, blvl, patternId);
	}
	@Override
	public Integer getUnitBuildTimeByPattern(Long baseBuildTime, int blevel, int patternId) {
		/*
		patternId = CivilBinaryConverter.getPatternIdFromMergedPatternId(patternId);
		Integer ret = this.civilProvider.getUnitBuildTimeByPrivatePattern(baseBuildTime, blevel, patternId);
		if(ret==null)return this.civilProvider.getUnitBuildTimeByPattern(baseBuildTime, blevel, patternId);
		return ret;
		*/
		return this.civilProvider.getUnitBuildTimeByPattern(baseBuildTime, blevel, patternId);
	}
	@Override
	public Integer getBuildingBuildTimeByPattern(int patternId, int currLvl, Long baseBuildTime) {
		/*
		patternId = CivilBinaryConverter.getPatternIdFromMergedPatternId(patternId);
		Integer ret = this.civilProvider.getBuildingBuildTimeByPrivatePattern(patternId, currLvl, baseBuildTime);
		if(ret==null)return this.civilProvider.getBuildingBuildTimeByPattern(patternId, currLvl, baseBuildTime);
		return ret;
		*/
		return this.civilProvider.getBuildingBuildTimeByPattern(patternId, currLvl, baseBuildTime);
	}
	@Override
	public Integer getMaxLvlByPattern(Integer myLevel, int patternId) {
		return this.civilProvider.getMaxLvlByPattern(myLevel, patternId);
	}
	@Override
	public Integer maxPeopleBoostPattern(int currentMaxPeople, int improvementLevel, int patternId) {
		return this.civilProvider.maxPeopleBoostPattern(currentMaxPeople, improvementLevel, patternId);
	}
	@Override
	public PriceStockSetImpl PriceBoostPattern(PriceStockSetImpl current,
			int improvementLevel, int patternId) {
		return this.civilProvider.PriceBoostPattern(current, improvementLevel, patternId);
	}
	@Override
	public Integer ArmorBoostPattern(int current, int improvementLevel,
			int patternId) {
		return this.civilProvider.ArmorBoostPattern(current, improvementLevel, patternId);
	}
	@Override
	public Integer getImprovedProductionByPattern(int currentProduction,
			int improvementLevel, int patternId) {
		return this.civilProvider.getImprovedProductionByPattern(currentProduction, improvementLevel, patternId);
	}
	@Override
	public Double getPercentRobberySteal() {
		return this.civilProvider.getPercentRobberySteal();
	}
	@Override
	public String getMainResource() {
		return this.civilProvider.getMainResource();
	}
	
	@Override
	public Civilization getParentCivilization() {
		return null;
	}
	@Override
	public Integer boostSatisfaction(int currentSatisf, int objectLvl, int patternId) {
		return this.civilProvider.boostSatisfaction(currentSatisf, objectLvl, patternId);
	}
	@Override
	public Integer boostCityRadius(int currentRadius, int objectLvl, int patternId) {
		return this.civilProvider.boostCityRadius(currentRadius, objectLvl, patternId);
	}
	@Override
	public Integer boostMoralFactor(int currentMoral, int objectLvl,
			int patternId) {
		return this.civilProvider.boostMoralFactor(currentMoral, objectLvl, patternId);
	}
	@Override
	public Integer boostFortificationRepairPerHour(int currentRepairPerHour,
			int objectLvl, int patternId) {
		return this.civilProvider.boostFortificationRepairPerHour(currentRepairPerHour, objectLvl, patternId);
	}
	@Override
	public Integer boostHP(int currentHP, int objectLvl, int patternId) {
		return this.civilProvider.boostHP(currentHP, objectLvl, patternId);
	}
	@Override
	public Boolean boostAntarcticaSettlemantAbility(int reserachLvl, int patternId) {
		return this.civilProvider.boostAntarcticaSettlemantAbility(reserachLvl, patternId);
	}
	@Override
	public Boolean canSettleAntarctica() {
		return this.civilProvider.canSettleAntarctica();
	}
	@Override
	public Integer mapDirectionsAmount() {
		return this.civilProvider.mapDirectionsAmount();
	}
	@Override
	public Integer boostDirectionsAmount(int objectLvl, int patternId) {
		return this.civilProvider.boostDirectionsAmount(objectLvl, patternId);
	}
	@Override
	public String extendsCivilization() {
		return null;
	}
	@Override
	public Integer getImprovedInputByPattern(int currentInput,
			int improvementLevel, int patternId) {
		return this.civilProvider.getImprovedInputByPattern(currentInput, improvementLevel, patternId);
	}
	@Override
	public Integer boostBuildTimePattern(int currentTime, int brLevel,
			int patternId) {
		return this.civilProvider.boostBuildTimePattern(currentTime, brLevel, patternId);
	}
	
	@Override
	public Integer getPeopleLimitInHouses(int housesLevel, int basePeopleLimit, int patternId) {
		return this.civilProvider.getPeopleLimitInHouses(housesLevel, basePeopleLimit, patternId);
	}
	@Override
	public ArmorType boostArmorType(int patternId, int rLevel, ArmorType current) {
		return this.civilProvider.boostArmorType(patternId, rLevel, null);
	}
	@Override
	public AttackType boostAttackType(int patternId, int rLevel,
			AttackType current) {
		return this.civilProvider.boostAttackType(patternId, rLevel, current);
	}
	@Override
	public Integer boostAttack(int patternId, int rLevel, int current) {
		return this.civilProvider.boostAttack(patternId, rLevel, current);
	}
	@Override
	public Integer boostCapacity(int current, int improvementLevel, int patternId) {
		return this.civilProvider.boostCapacity(current, improvementLevel, patternId);
	}
	@Override
	public Integer boostSpeed(int current, int improvementLevel, int patternId) {
		return this.civilProvider.boostSpeed(current, improvementLevel, patternId);
	}

	@Override
	public ICityModel getCityModel() {
		return this.cityModel;
	}

	@Override
	public String getMajorMeat() {
		return this.civilProvider.getMajorMeat();
	}

	@Override
	public String getMajorCarbo() {
		return this.civilProvider.getMajorCarbo();
	}

	@Override
	public String getMajorAddition() {
		return this.civilProvider.getMajorAddition();
	}

	@Override
	public Integer getFortyfiactionHP(int baseHP, Integer fortyficationLevel, int patternId) {
		return this.civilProvider.getFortyfiactionHP(baseHP, fortyficationLevel, patternId);
	}

	@Override
	public Integer getFortyficationRepairRate(int baseRepairRate,Integer fortyficationLevel, int patternId) {
		return this.civilProvider.getFortyficationRepairRate(baseRepairRate, fortyficationLevel, patternId);
	}

	@Override
	public Integer boostFortyficationHP(int current, int improvementLevel,
			int patternId) {
		return this.civilProvider.boostFortyficationHP(current, improvementLevel, patternId);
	}

	@Override
	public Integer boostFortyficationRepairRate(int current,
			int improvementLevel, int patternId) {
		return this.civilProvider.boostFortyficationRepairRate(current, improvementLevel, patternId);
	}

	@Override
	public Integer getRadius(int baseRadius, Integer radiusResearchLevel, int patternId) {
		/*
		patternId = CivilBinaryConverter.getPatternIdFromMergedPatternId(patternId);
		Integer ret= this.civilProvider.getRadiusPrivate(baseRadius, radiusResearchLevel, patternId);
		if(ret==null)return this.civilProvider.getRadius(baseRadius, radiusResearchLevel, patternId);
		return ret;
		*/
		return this.civilProvider.getRadius(baseRadius, radiusResearchLevel, patternId);
	}


	@Override
	public Integer boostRadius(int current, int improvementLevel, int patternId) {
		return this.civilProvider.boostRadius(current, improvementLevel, patternId);
	}

	@Override
	public Integer getSpongingPatternById(String convBuildId_stockType) {
		int ret =  this.civilProvider.getSpongingPatternById(convBuildId_stockType);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}

	@Override
	public Integer getCostPatternById(int gameModelId) {
		int ret = this.civilProvider.getCostPatternById(gameModelId);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}

	@Override
	public Integer getExtractionPatternById(int extractBuildingId) {
		int ret = this.civilProvider.getExtractionPatternById(extractBuildingId);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}

	@Override
	public Integer getProductionPatternById(int convertBuildingId) {
		int ret = this.civilProvider.getProductionPatternById(convertBuildingId);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}

	@Override
	public Integer getMaxPeoplePatternById(int extractBuildingId) {
		int ret = this.civilProvider.getMaxPeoplePatternById(extractBuildingId);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}

	@Override
	public Integer getResearchBuildTimePatternById(int reseachId) {
		int ret = this.civilProvider.getResearchBuildTimePatternById(reseachId);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}

	@Override
	public Integer getUnitBuildTimePatternById(int unitId) {
		int ret = this.civilProvider.getUnitBuildTimePatternById(unitId);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}

	@Override
	public Integer getBuildingBuildTimePatternById(int buildingId) {
		int ret = this.civilProvider.getBuildingBuildTimePatternById(buildingId);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}

	@Override
	public Integer getRadiusPatternById(int radiusReseach) {
		int ret = this.civilProvider.getRadiusPatternById(radiusReseach);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}


	@Override
	public Integer fortyficationProtectionLevel(AttackType aType) {
		return this.civilProvider.fortyficationProtectionLevel(aType);
	}

	@Override
	public Integer fortyficationProtectionLevel(int unitId) {
		return this.civilProvider.fortyficationProtectionLevel(unitId);
	}

	@Override
	public Integer fortyficationArmorBoost(int fortyficationLevel,
			int currentArmor) {
		return this.fortyficationArmorBoost(fortyficationLevel, currentArmor);
	}

	@Override
	public Integer fortyficationAttackBoost(int fortyficationLevel,
			int currentAttack) {
		return this.fortyficationAttackBoost(fortyficationLevel, currentAttack);
	}

	@Override
	public List<Integer> excludedObjects() {
		return new ArrayList<Integer>();
	}

	@Override
	protected List<IGameModel> getFullGameModelList() {
		return this.getInternalGameModelList();
	}

	@Override
	public <T extends IGameModel> T getById(int id, Class<T> clazz) {
		return  getByIdInInternalMap(id, clazz);
	}

	@Override
	public IHousesModel getHousesModel() {
		return getGameModelList(IHousesModel.class, false, false).get(0);
	}

	@Override
	public ICityInitializator getCityInitializator() {
		return null;
	}

}
