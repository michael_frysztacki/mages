package game_model_impl;

import entities.UsersCityConfig;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IHousesModel;

public class Houses extends Building implements IHousesModel{

	public Houses(int id, int timePattern, int costPattern, PriceStockSetImpl cost,
			long baseBuildTime, int maxLimitInHousesPatternId, int basePeopleLimit) {
		super(id, timePattern, costPattern, cost, baseBuildTime);
		this.maxLimitInHousesPatternId = maxLimitInHousesPatternId;
		this.basePeopleLimit = basePeopleLimit;
	}

	final int maxLimitInHousesPatternId;
	final int basePeopleLimit;
	@Override
	public int getPeopleLimit(UsersCityConfig ucc) {
		
		ICivilization civil = ucc.getUserConfig().getMyCivilization();
		return civil.getPeopleLimitInHouses( ucc.getLevel(this.id), this.basePeopleLimit, this.maxLimitInHousesPatternId);
	}
	
	


}
