package game_model_impl;

import java.util.List;

import entities.Civil;
import entities.UserConfig;
import entities.UsersCityConfig;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IBuildingModel;
import game_model_interfaces.IResearchModel;
import game_model_interfaces.IUnitModel;
import game_model_interfaces.ICivilization;

public class Building extends GameModel implements IBuildingModel{

	public Building(int id, int timePattern,int costPattern, PriceStockSetImpl cost,  long baseBuildTime){
		super(id, costPattern , cost, baseBuildTime, timePattern);
	}
	
	@Override
	public List<IResearchModel> getResearches(UsersCityConfig city) {
		
		return this.getRequirementNode().getChildren(IResearchModel.class);
	}
	
	@Override
	public List<IUnitModel> getUnits(UserConfig city) {
		
		return this.getRequirementNode().getChildren(IUnitModel.class);
	}
	
	@Override
	public PriceStockSetImpl getPrice(UsersCityConfig ucc) {
		
		ICivilization config =  ucc.getUserConfig().getMyCivilization();
		int costPattern = config.getCostPatternById(getId());
		PriceStockSetImpl uccCost = config.getCostByPattern(this.cost, costPattern, ucc.getLevel(this.getId()), null);
		return uccCost;
	}
	
	@Override
	public Integer getBuildTime(UsersCityConfig uc) {
		
		ICivilization config =  uc.getUserConfig().getMyCivilization();
		int buildTimePattern = config.getBuildingBuildTimePatternById(getId());
		return config.getBuildingBuildTimeByPattern(buildTimePattern, uc.getLevel(this.getId()), this.baseBuildTime);
	}

	@Override
	public String dump(Language lang, Civil civil){
		return "BUDYNEK\n" + super.dump(lang, civil);
	}
	/*
	@Override
	public String getName(UsersCityConfig uc) {
		if(mtb == null)return null;
		return mtb.getString( uc );
	}
	*/
	

}
