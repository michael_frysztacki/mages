package game_model_impl;

import game_model_interfaces.IImprovementModel;

public class Improvement extends Research implements IImprovementModel{
	public Improvement(int id,int maxLvl, int costPattern,PriceStockSetImpl cost, int baseBuildTime, int timePattern, boolean isExtractionIprovement)
	{
		super(id,maxLvl, costPattern ,cost,  baseBuildTime, timePattern);
		this.extractionImprov = isExtractionIprovement;
	}
	private boolean extractionImprov;
	@Override
	public Boolean isExtractionImprovement()
	{
		return extractionImprov;
	}
	
}
