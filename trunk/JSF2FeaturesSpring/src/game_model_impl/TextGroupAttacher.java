package game_model_impl;

import java.util.List;

import entities.Civil;
import game_model_impl.Text.Text;

public class TextGroupAttacher {

	public List<Text> textGroup;

	private Civil civil;
	public TextGroupAttacher(List<Text> local, Civil civilization) {
		this.textGroup = local;
		this.civil = civilization;
	}

	public Object getModelTextBunchOwnerId() {
		return textGroup.get(0).objId;
	}

	public Object modelTextsBunchCivilization() {
		return civil;
	}

}
