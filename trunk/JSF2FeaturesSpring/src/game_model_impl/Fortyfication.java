package game_model_impl;

import entities.UsersCityConfig;
import game_model_interfaces.IFortyficationModel;

public class Fortyfication extends Building implements IFortyficationModel{

	public Fortyfication(int id, int timePattern, int costPattern,
			PriceStockSetImpl cost, long baseBuildTime, int baseHP, int hpPattern, int baseRepairRate, int repairPattern) {
		super(id, timePattern, costPattern, cost, baseBuildTime);
		this.baseHP = baseHP;
		this.hpPattern = hpPattern;
		this.baseRepairRate = baseRepairRate;
		this.repairPattern = repairPattern;
	}

	private final int baseHP,hpPattern,baseRepairRate,repairPattern;
	@Override
	public int getFortyficationHP(UsersCityConfig ucc) {
		
		return ucc.getUserConfig().getMyCivilization().getFortyfiactionHP(baseHP, ucc.getLevel(this.id), hpPattern);
	}

	@Override
	public int getRepairRate(UsersCityConfig ucc) {
		
		return ucc.getUserConfig().getMyCivilization().getFortyficationRepairRate(baseRepairRate, ucc.getLevel(this.id), repairPattern);
	}

}
