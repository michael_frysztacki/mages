package game_model_impl.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

	public class Text implements Comparable<Text>
	{
		public enum Language
		{
			PL,
			ENG,
		};
		public enum ModelTextType
		{
			improvement,
			regular
		}
		public final int changerId, reqLvl;
		public final ModelTextType type;
		public Text(int objId, LanguagePair... pairs)
		{
			this.type = ModelTextType.regular;
			this.objId = objId;
			changerId = -1;
			reqLvl = -1;
			if(pairs != null)
			{	
				for(LanguagePair pair : pairs)
				{
					this.pairs.add(pair);
				}
			}
		}
		
		public Text(int objId, int changerId, int reqLvl, LanguagePair... pairs)
		{
			this.type = ModelTextType.improvement;
			this.objId = objId;
			this.changerId = changerId;
			this.reqLvl = reqLvl;
			if(pairs != null)
			{	
				for(LanguagePair pair : pairs)
				{
					this.pairs.add(pair);
				}
			}
		}
		
		//public abstract void applyToGameModel(IGameModel gm) throws BuildModelException;
		
		@Override
		public String toString()
		{
			String add;
			if(ModelTextType.regular == this.type)add = "1";
			else add = "2";
			return add+"_"+Integer.toString(this.objId)+"_"+Integer.toString(this.changerId);
		}
		public final int objId;
		private final List<LanguagePair> pairs = new ArrayList<LanguagePair>();
		public List<LanguagePair> getLangPairs()
		{
			return Collections.unmodifiableList(pairs);
		}
		/*
		 * umieszcza wszystkie ModelText'y kt�re nie s� zwi�zane z ulepszeniem  tylko z GameModel na pocz�tek.
		 * ModelText'y zwi�zane z ulepszeniami daje na koniec listy.
		 * Sortowanie "wewn�trz" odbywa si� tak ja sortowanie stringu, tzn. obiekty
		 * z id i id_changer b�d� obok siebie.
		 */
		@Override
		public int compareTo(Text arg0) {
			
			String thisS = this.toString();
			String argS = arg0.toString();
			return thisS.compareTo(argS);
			
			
		}
		
	}
	
	
	
