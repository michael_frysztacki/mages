package game_model_impl.Text;

import game_model_impl.Text.Text.Language;
import helpers.Pair;

public class LanguagePair extends Pair<Language,String> 
{
	public LanguagePair(Language lang, String text)
	{
		super(lang,text);
	}
}
