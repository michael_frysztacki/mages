package game_model_impl;

public class MyClassCastException extends Exception{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyClassCastException(Class<?> from, Class<?> to)
	{
		this.from = from;
		this.to = to;
	}
	private Class<?> from,to;
	
	@Override
	public String toString()
	{
		return "nie mo�na castowa� z " + from + " na " + to + "\n";
	}

}
