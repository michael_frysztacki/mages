package game_model_impl.visitors.civilization_visitors;

import game_model_impl.Civilization;
import game_model_impl.Civilization.CivilizationVisitor;

import java.util.ArrayList;
import java.util.List;


/*
 * Consider a civilizations chierarchy:
 *  		A
 *  	   / \
 *  	  B   C
 *       /\
 *      D  E
 *      
 *     A is the common civilization, other are extending 'A' in above manner.
 *     If this vistor (CollectCivilizationsInChainUpwards) is used on civilization "D",
 *     it will collect a list of civilizations:
 *     D , B , A.
 *     Collecting is performed from the current civilization to the most top: A.
 *     If used on 'B' it will collect:
 *     B, A.
 */
public class CollectCivilizationsInChainUpwards implements CivilizationVisitor{

	private List<Civilization> chain = new ArrayList<Civilization>();
	@Override
	public void visit(Civilization civil) {
		while(civil.getParentCivilization()!=null){
			chain.add(civil);
			civil = civil.getParentCivilization();
		}
		chain.add(civil);
	}
	public List<Civilization> getChain(){
		return chain;
	}
	
	public List<Civilization> getChainInReversedOrder(){
		java.util.Collections.reverse( chain );
		return chain;
	}
	
	
	
	

}
