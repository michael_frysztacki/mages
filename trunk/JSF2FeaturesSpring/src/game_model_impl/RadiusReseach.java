package game_model_impl;

import entities.UserConfig;
import game_model_interfaces.IRadialResearchModel;

public class RadiusReseach extends Research implements IRadialResearchModel{

	private final int baseRadius;
	
	public RadiusReseach(int id, int maxLvl, int costPattern,
			PriceStockSetImpl cost, int baseTime, int timePattern, int baseRadius, int radiusPattern) {
		
		super(id, maxLvl, costPattern, cost, baseTime, timePattern);
		this.baseRadius = baseRadius;
	}

	@Override
	public int getRadius(UserConfig uc) {
		
		int radiusPattern = uc.getMyCivilization().getRadiusPatternById(getId());
		return this.getMyCivilization().getRadius(baseRadius, uc.getLevel(id), radiusPattern);
	}

}
