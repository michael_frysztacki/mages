package game_model_impl;

import game_model_impl.boosters.BoosterDecorator;
import game_model_interfaces.IGameModel;

import java.util.Map;
import java.util.Map.Entry;


public class BoosterPrototype {

	private BoosterDecorator<?> booster;
	private DecoratorPattern decoratorPattern = null;
	public BoosterPrototype(BoosterDecorator<?> booster, int... ids){
		this.booster = booster;
		this.decoratorPattern = this.new IdDecoratorPattern(ids);
	}
	public BoosterPrototype(BoosterDecorator<?> booster, Class<?>... classes){
		this.booster = booster;
		this.decoratorPattern = this.new ClassDecoratorPattern(classes);
	}
	void setBoostersCivilization(Civilization boostersCivilization){
		this.booster.setBoostersCivilization(boostersCivilization);
	}
	protected BoosterDecorator<?> cloneBooster(){
		BoosterDecorator<?> cloned = (BoosterDecorator<?>) booster.clone();
		return cloned;
	}
	public void decorateAllMatchingGameModels(Map<Integer, IGameModel> allGameModels){
		this.decoratorPattern.attachToGameModels(allGameModels );
	}
	private abstract class DecoratorPattern {

		public abstract void attachToGameModels(Map<Integer,IGameModel> allGameModels);
		private DecoratorPattern(){};
		protected void decorateWithBooster(Map<Integer, IGameModel> allGameModels, IGameModel entry) {
			BoosterDecorator<?> cloned = BoosterPrototype.this.cloneBooster() ;
			IGameModel gameModel = cloned.dekoruj( entry );
			allGameModels.put(gameModel.getId(), gameModel);
		}
	}
	private class ClassDecoratorPattern extends DecoratorPattern{
		public Class<?>[] classes;
		public ClassDecoratorPattern(Class<?>... gameInterface) {
			this.classes = new Class[gameInterface.length];
			for(int i=0;i<gameInterface.length;i++)
				classes[i] = gameInterface[i];
		}
		@Override
		public void attachToGameModels(Map<Integer, IGameModel> allGameModels) {
			for(Entry<Integer,IGameModel> potentialCandidateForDecoration : allGameModels.entrySet() ){
				attachBoosterIfMatch(allGameModels, potentialCandidateForDecoration.getValue());
			}
		}
			private void attachBoosterIfMatch(Map<Integer, IGameModel> allGameModels, IGameModel gm){
				for( Class<?> clazz : classes ){
					if(classMatchesGameModel(gm, clazz) )
						decorateWithBooster(allGameModels, gm);
				}
			}
				private boolean classMatchesGameModel(IGameModel gm, Class<?> clazz) {
					return clazz == gm.getObjectsClass();
				}
	}
	private class IdDecoratorPattern extends DecoratorPattern{
		private int ids[] = null;
		private Map<Integer, IGameModel> allGameModels;
		public IdDecoratorPattern(int... id) {
			ids = new int[id.length];
			for(int i=0;i<id.length;i++)
				ids[i] = id[i];
		}
		
		@Override
		public void attachToGameModels(Map<Integer, IGameModel> allGameModels){
			this.allGameModels = allGameModels;
			for(Entry<Integer,IGameModel> entry : allGameModels.entrySet() )
				attachBoosterIfMatch(entry.getValue());
		}

		private void attachBoosterIfMatch(IGameModel gm) {
			for(int id : ids)
				if( id == gm.getId() )
					decorateWithBooster(allGameModels, gm);
		}
	}
	
	
	
}
