package game_model_impl;

import entities.Civil;
import entities.UserConfig;
import game_model_interfaces.ICityModel;
import game_model_interfaces.IGameModel;
import game_model_interfaces.IUnitModel.ArmorType;
import game_model_interfaces.IUnitModel.AttackType;
import game_model_interfaces.civilization_defining_interfaces.ICivilizationConfigProvider;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/*
 * Klasa dekorujaca.
 */
public class ConcreteCivilization extends Civilization{

	protected Civilization element;
	
	ConcreteCivilization(ICivilizationConfigProvider civilProvider)
	{
		this.civilProvider = civilProvider;
	}
	void setExtendingCivilization(Civilization civil)
	{
		this.element = civil;
	}
	
	@Override
	boolean updateIGameModel(IGameModel b){
		/*
		 * ReqNode'owi trzeba przypisa� ostatniego dekoratora na GameModelu.
		 */
		IGameModel gm = this.map.get(b.getId());
		if(gm == null)
			return element.updateIGameModel(b);
		else
			return super.updateIGameModel(b);
	}
	/*
	@Override
	protected List<IGameModel> getInternalGameModelList()
	{
		return new ArrayList<IGameModel>( this.map.values() );
	}
	*/
	@Override
	protected List<IGameModel> getFullGameModelList(){
		List<IGameModel> ret = new ArrayList<IGameModel>();
		Set<Integer> helper = new HashSet<Integer>();
		if(this.excludedObjects()!=null)
		for(Integer excl : this.excludedObjects()){
			helper.add(excl);
		}
		for(IGameModel gm : this.element.getFullGameModelList()){
			if(!helper.contains(gm.getId()))ret.add(gm);
		}
		ret.addAll(this.getInternalGameModelList());
		return ret;
	}
	@Override
	public <T extends IGameModel> List<T> getGameModelList(Class<T> objectType, boolean inherit, boolean fullList){
		
		List<T> ret = new ArrayList<T>();
		ret.addAll(super.getGameModelList(objectType, inherit, false));
		if(fullList){
			Set<Integer> helper = new HashSet<Integer>();
			if(this.excludedObjects()!=null)
			for(Integer excl : this.excludedObjects()){
				helper.add(excl);
			}
			for(T gm : element.getGameModelList(objectType, inherit, fullList)){
				if(!helper.contains(gm.getId()))ret.add(gm);
			}
			
		}
		/*
		Iterator
		for(IGameModel gm : ret){
			if(this.excludedObjects())
		}
		*/
		return ret;
	}
	@Override
	public <T extends IGameModel> T getById(int id, Class<T> clazz){
		if(this.excludedObjects().contains(id))return null;
		T ret = super.getByIdInInternalMap(id,clazz);
		if(ret == null){
			return element.getById(id,clazz);
		}
		return ret;
	}
	@Override
	public PriceStockSetImpl getCostByPattern(PriceStockSetImpl baseCost,int patternId, int lvl, UserConfig uc){
		PriceStockSetImpl ret = this.civilProvider.getCostByPattern(baseCost, patternId, lvl, uc);
		if(ret == null)return element.getCostByPattern(baseCost, patternId, lvl, uc);
		return ret;
	}
	
	@Override
	public ArmorType boostArmorType(int patternId, int rLevel, ArmorType current) {
		ArmorType armorType = this.civilProvider.boostArmorType(patternId, rLevel, current);
		if(armorType == null){
			return element.boostArmorType(patternId, rLevel, current);
		}
		return armorType;
	}
	
	@Override
	public Integer boostAttack(int patternId, int rLevel, int current) {
		Integer attack = this.civilProvider.boostAttack(patternId, rLevel, current);
		if(attack == null){
			return element.boostAttack( patternId, rLevel, current );
		}
		return attack;
	}
	
	@Override
	public AttackType boostAttackType(int patternId, int rLevel,
			AttackType current) {
		AttackType attackType = this.civilProvider.boostAttackType(patternId, rLevel, current);
		if(attackType == null){
			return element.boostAttackType(patternId, rLevel, current);
		}
		return attackType;
	}
	
	@Override
	public Integer getPeopleLimitInHouses(int housesLevel, int basePeopleLimit, int patternId) {
		Integer ret = this.civilProvider.getPeopleLimitInHouses(housesLevel,basePeopleLimit, patternId);
		if(ret == null){
			return element.civilProvider.getPeopleLimitInHouses(housesLevel, basePeopleLimit, patternId);
		}
		return ret;
	}
	@Override
	public String getCivilization(){
		return this.civilProvider.getCivilization();
	}
	
	@Override
	public Integer getMaxPeopleByPattern(int baseAmount, int buildingLvl, int patternId)
	{
		Integer ret = this.civilProvider.getMaxPeopleByPattern(baseAmount, buildingLvl, patternId);
		if(ret == null)return element.getMaxPeopleByPattern(baseAmount, buildingLvl, patternId);
		return ret;
	}
	
	@Override
	public Integer getExtractionByPattern(Integer peopleCount, Integer lodeFactor, int patternId) {
		Integer ret = this.civilProvider.getExtractionByPattern(peopleCount, lodeFactor, patternId);
		if(ret==null)return element.getExtractionByPattern(peopleCount, lodeFactor, patternId);
		return ret;
	}
	@Override
	public Integer getProductionByPattern(int peopleCount, int patternId)
	{
		Integer ret = this.civilProvider.getProductionByPattern(peopleCount, patternId);
		if(ret==null)return element.getProductionByPattern(peopleCount, patternId);
		return ret;
	}
	@Override
	public Integer getMaxLvlByPattern(Integer myLevel, int patternId)
	{
		// WZORY NA WYMAGANIA NIE MO�NA PRZECI��A�
		Integer ret = this.civilProvider.getMaxLvlByPattern(myLevel, patternId);
		if(ret == null)
		{
			return element.getMaxLvlByPattern(myLevel, patternId);
		}
		return ret;
	}
	
	@Override
	public Integer getBuildingBuildTimeByPattern(int patternId, int currLvl, Long baseBuildTime)
	{
		Integer ret = this.civilProvider.getBuildingBuildTimeByPattern(patternId, currLvl, baseBuildTime);
		if(ret == null)return element.getBuildingBuildTimeByPattern( patternId, currLvl, baseBuildTime);
		return ret;
	}
	@Override
	public Integer getUnitBuildTimeByPattern(Long baseBuildTime, int blevel, int patternId)
	{
		Integer ret = this.civilProvider.getUnitBuildTimeByPattern(baseBuildTime, blevel, patternId);
		if(ret==null)return element.getUnitBuildTimeByPattern(baseBuildTime, blevel, patternId);
		return ret;
	}
	@Override
	public Integer getResearchBuildTimeByPattern(Long timeFactor, int rlvl, int blvl, int patternId)
	{
		Integer ret = this.civilProvider.getResearchBuildTimeByPattern(timeFactor, rlvl, blvl, patternId);
		if(ret==null)return element.getResearchBuildTimeByPattern(timeFactor, rlvl, blvl, patternId);
		return ret;
	}

	@Override
	public Double getPercentRobberySteal()
	{
		Double ret = this.civilProvider.getPercentRobberySteal();
		if(ret==null)return element.getPercentRobberySteal();
		return ret;
		
	}

	@Override
	public Integer getSpongingByPattern(int productionOfOutcomeResource, int patternId) {
		Integer ret = this.civilProvider.getSpongingByPattern(productionOfOutcomeResource, patternId);
		if(ret==null)return element.getSpongingByPattern(productionOfOutcomeResource, patternId);
		return ret;
	}
	@Override
	public Integer maxPeopleBoostPattern(int currentMaxPeople,
			int improvementLevel, int patternId) {
		Integer ret = this.civilProvider.maxPeopleBoostPattern(currentMaxPeople, improvementLevel, patternId);
		if(ret==null)return element.maxPeopleBoostPattern(currentMaxPeople, improvementLevel, patternId);
		return ret;
	}
	@Override
	public PriceStockSetImpl PriceBoostPattern(PriceStockSetImpl current,
			int improvementLevel, int patternId) {
		PriceStockSetImpl ret = this.civilProvider.PriceBoostPattern(current, improvementLevel, patternId);
		if(ret==null)return element.PriceBoostPattern(current, improvementLevel, patternId);
		return ret;
	}
	@Override
	public Integer ArmorBoostPattern(int current, int improvementLevel,
			int patternId) {
		Integer ret = this.civilProvider.ArmorBoostPattern(current, improvementLevel, patternId);
		if(ret==null)return element.ArmorBoostPattern(current, improvementLevel, patternId);
		return ret;
	}
	@Override
	public Integer getImprovedProductionByPattern(int currentProduction,
			int improvementLevel, int patternId) {
		Integer ret = this.civilProvider.getImprovedProductionByPattern(currentProduction, improvementLevel, patternId);
		if(ret== null)return element.getImprovedProductionByPattern(currentProduction, improvementLevel, patternId);
		return ret;
	}
	@Override
	public String getMainResource()
	{
		String tax = this.civilProvider.getMainResource();
		if(tax == null)return element.getMainResource();
		return tax;
	}

	@Override
	public Civilization getParentCivilization() {
		return this.element;
	}

	@Override
	public Integer boostSatisfaction(int currentSatisf, int objectLvl, int patternId) {
		Integer ret = this.civilProvider.boostSatisfaction(currentSatisf, objectLvl, patternId);
		if(ret == null)return this.element.boostSatisfaction(currentSatisf, objectLvl, patternId);
		return ret;
	}

	@Override
	public Integer boostCityRadius(int currentRadius, int objectLvl,
			int patternId) {
		Integer ret = this.civilProvider.boostCityRadius(currentRadius, objectLvl, patternId);
		if(ret == null)return this.element.boostCityRadius(currentRadius, objectLvl, patternId);
		return ret;
	}

	@Override
	public Integer boostMoralFactor(int currentMoral, int objectLvl,
			int patternId) {
		Integer ret = this.civilProvider.boostMoralFactor(currentMoral, objectLvl, patternId);
		if(ret==null)return this.element.boostMoralFactor(currentMoral, objectLvl, patternId);
		return ret;
	}

	@Override
	public Integer boostFortificationRepairPerHour(int currentRepairPerHour,
			int objectLvl, int patternId) {
		Integer ret = this.civilProvider.boostFortificationRepairPerHour(currentRepairPerHour, objectLvl, patternId);
		if(ret==null)return this.element.boostFortificationRepairPerHour(currentRepairPerHour, objectLvl, patternId);
		return ret;
		
	}

	@Override
	public Integer boostHP(int currentHP, int objectLvl, int patternId) {
		Integer ret = this.civilProvider.boostHP(currentHP, objectLvl, patternId);
		if(ret==null)return this.element.boostHP(currentHP, objectLvl, patternId);
		return ret;
		
	}

	@Override
	public Boolean boostAntarcticaSettlemantAbility(int reserachLvl,
			int patternId) {
		Boolean ret = this.civilProvider.boostAntarcticaSettlemantAbility(reserachLvl, patternId);
		if(ret==null)return this.element.boostAntarcticaSettlemantAbility(reserachLvl, patternId);
		return ret;
	}

	@Override
	public Boolean canSettleAntarctica() {
		Boolean ret = this.civilProvider.canSettleAntarctica();
		if(ret==null)return this.element.canSettleAntarctica();
		return ret;
	}

	@Override
	public Integer mapDirectionsAmount() {
		Integer ret = this.civilProvider.mapDirectionsAmount();
		if(ret==null)return this.element.mapDirectionsAmount();
		return ret;
	}

	@Override
	public Integer boostDirectionsAmount(int objectLvl, int patternId) {
		Integer ret = this.civilProvider.boostDirectionsAmount(objectLvl, patternId);
		if(ret==null)return this.element.boostDirectionsAmount(objectLvl, patternId);
		return ret;
	}

	@Override
	public String extendsCivilization() {
		return getParentCivilization().getCivilization();
	}

	@Override
	public Integer getImprovedInputByPattern(int currentInput,
			int improvementLevel, int patternId) {
		Integer ret = this.civilProvider.getImprovedInputByPattern(currentInput, improvementLevel, patternId);
		if(ret==null)return this.element.getImprovedInputByPattern(currentInput, improvementLevel, patternId);
		return ret;
	}

	@Override
	public Integer boostBuildTimePattern(int currentTime, int brLevel,
			int patternId) {
		Integer ret = this.civilProvider.boostBuildTimePattern(currentTime, brLevel, patternId);
		if(ret==null)return this.element.boostBuildTimePattern(currentTime, brLevel, patternId);
		return ret;
	}
	@Override
	public Integer boostCapacity(int current, int improvementLevel, int patternId) {
		
		Integer ret = this.civilProvider.boostCapacity(current, improvementLevel, patternId);
		if(ret==null)return this.element.boostCapacity(current, improvementLevel, patternId);
		return ret;
	}
	@Override
	public Integer boostSpeed(int current, int improvementLevel, int patternId) {
		
		Integer ret = this.civilProvider.boostSpeed(current, improvementLevel, patternId);
		if(ret==null)return this.element.boostSpeed(current, improvementLevel, patternId);
		return ret;
	}
	@Override
	public ICityModel getCityModel() {
		return element.getCityModel();
	}
	@Override
	public String getMajorMeat() {
		String ret = civilProvider.getMajorMeat();
		if(ret==null)return element.getMajorMeat();
		return ret;
	}
	@Override
	public String getMajorCarbo() {
		String ret = civilProvider.getMajorCarbo();
		if(ret==null)return element.getMajorCarbo();
		return ret;
	}
	@Override
	public String getMajorAddition() {
		String ret = civilProvider.getMajorAddition();
		if(ret==null)return element.getMajorAddition();
		return ret;
	}
	
	@Override
	public Integer getFortyfiactionHP(int baseHP, Integer fortyficationLevel, int patternId) {
		Integer ret = this.civilProvider.getFortyfiactionHP(baseHP, fortyficationLevel, patternId);
		if(ret == null) return this.element.getFortyfiactionHP(baseHP, fortyficationLevel, patternId);
		return ret;
	}
	@Override
	public Integer getFortyficationRepairRate(int baseRepairRate, Integer fortyficationLevel, int patternId) {
		Integer ret = this.civilProvider.getFortyficationRepairRate(baseRepairRate, fortyficationLevel, patternId);
		if(ret == null) return this.element.getFortyficationRepairRate(baseRepairRate, fortyficationLevel, patternId);
		return ret;
	}
	@Override
	public Integer boostFortyficationHP(int current, int improvementLevel,
			int patternId) {
		Integer ret = this.civilProvider.boostFortyficationHP(current, improvementLevel, patternId);
		if(ret == null) return this.element.boostFortyficationHP(current, improvementLevel, patternId);
		return ret;
	}
	@Override
	public Integer boostFortyficationRepairRate(int current,
			int improvementLevel, int patternId) {
		Integer ret = this.civilProvider.boostFortyficationRepairRate(current, improvementLevel, patternId);
		if(ret == null) return this.element.boostFortyficationRepairRate(current, improvementLevel, patternId);
		return ret;
	}
	@Override
	public Integer getRadius(int baseRadius, Integer radiusResearchLevel, int patternId) {
		Integer ret = this.civilProvider.getRadius(baseRadius, radiusResearchLevel, patternId);
		if(ret == null) return this.element.getRadius(baseRadius, radiusResearchLevel, patternId);
		return ret;
	}
	@Override
	public Integer boostRadius(int current, int improvementLevel, int patternId) {
		
		Integer ret = this.civilProvider.boostRadius(current, improvementLevel, patternId);
		if(ret == null) return this.element.boostRadius(current, improvementLevel, patternId);
		return ret;
	}
	@Override
	public Integer getSpongingPatternById(String convBuildId_stockType) {
		Integer ret = this.civilProvider.getSpongingPatternById(convBuildId_stockType);
		if(ret == null) return this.element.getSpongingPatternById(convBuildId_stockType);
	//	ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}
	@Override
	public Integer getCostPatternById(int gameModelId) {
		Integer ret = this.civilProvider.getCostPatternById(gameModelId);
		if(ret == null) return this.element.getCostPatternById(gameModelId);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}
	@Override
	public Integer getExtractionPatternById(int extractBuildingId) {
		Integer ret = this.civilProvider.getExtractionPatternById(extractBuildingId);
		if(ret == null) return this.element.getExtractionPatternById(extractBuildingId);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}
	@Override
	public Integer getProductionPatternById(int convertBuildingId) {
		Integer ret = this.civilProvider.getProductionPatternById(convertBuildingId);
		if(ret==null)return this.element.getProductionPatternById(convertBuildingId);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}
	@Override
	public Integer getMaxPeoplePatternById(int extractBuildingId) {
		Integer ret= this.civilProvider.getMaxPeoplePatternById(extractBuildingId);
		if(ret==null)return this.element.getMaxPeoplePatternById(extractBuildingId);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}
	@Override
	public Integer getResearchBuildTimePatternById(int reseachId) {
		Integer ret=this.civilProvider.getResearchBuildTimePatternById(reseachId);
		if(ret==null)return this.element.getResearchBuildTimePatternById(reseachId);
		///ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}
	@Override
	public Integer getUnitBuildTimePatternById(int unitId) {
		Integer ret=this.civilProvider.getUnitBuildTimePatternById(unitId);
		if(ret==null)return this.element.getUnitBuildTimePatternById(unitId);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}
	@Override
	public Integer getBuildingBuildTimePatternById(int buildingId) {
		Integer ret=this.civilProvider.getBuildingBuildTimePatternById(buildingId);
		if(ret==null)return this.element.getBuildingBuildTimePatternById(buildingId);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}
	@Override
	public Integer getRadiusPatternById(int radiusReseach) {
		Integer ret=this.civilProvider.getRadiusPatternById(radiusReseach);
		if(ret==null)return this.element.getRadiusPatternById(radiusReseach);
		//ret = CivilBinaryConverter.mergePatternWithCivil(this.civilProvider.getCivilization(), ret);
		return ret;
	}

	@Override
	public Integer fortyficationProtectionLevel(AttackType aType) {
		Integer ret = this.civilProvider.fortyficationProtectionLevel(aType);
		if(ret==null)return element.civilProvider.fortyficationProtectionLevel(aType);
		return ret;
	}
	@Override
	public Integer fortyficationProtectionLevel(int unitId) {
		Integer ret = this.civilProvider.fortyficationProtectionLevel(unitId);
		if(ret==null)return element.civilProvider.fortyficationProtectionLevel(unitId);
		return ret;
	}
	@Override
	public Integer fortyficationArmorBoost(int fortyficationLevel,
			int currentArmor) {
		Integer ret = this.civilProvider.fortyficationArmorBoost(fortyficationLevel, currentArmor);
		if(ret==null)return element.civilProvider.fortyficationArmorBoost(fortyficationLevel, currentArmor);
		return ret;
	}
	@Override
	public Integer fortyficationAttackBoost(int fortyficationLevel,
			int currentAttack) {
		Integer ret = this.civilProvider.fortyficationAttackBoost(fortyficationLevel, currentAttack);
		if(ret==null)return element.civilProvider.fortyficationAttackBoost(fortyficationLevel, currentAttack);
		return ret;
	}
	@Override
	public List<Integer> excludedObjects() {
		
		List<Integer> ret = new ArrayList<Integer>();
		if(this.civilProvider.excludedObjects()!=null)
			ret.addAll(this.civilProvider.excludedObjects());
		List<Integer> temp = new ArrayList<Integer>();
		for(Integer i : ret){
			for(IGameModel gm : this.element.getByIdInInternalMap(i, IGameModel.class).getRequirementNode().getChildrenRecursively(IGameModel.class)){
				if(!ret.contains(gm.getId()))temp.add(gm.getId());
			}
		}
		ret.addAll(temp);
		if(this.element.civilProvider.excludedObjects()!=null)
		for(int i : this.element.civilProvider.excludedObjects()){
			if(!ret.contains(i))ret.add(i);
		};
		return ret;
	}

}
