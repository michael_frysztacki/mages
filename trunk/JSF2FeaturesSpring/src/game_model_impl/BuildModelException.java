package game_model_impl;

public class BuildModelException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public enum E_BuildModelException
	{
		repetaed_id;
	}
	public final String msg;
	public BuildModelException(String msg)
	{
		this.msg = msg;
	}
	@Override
	public String toString()
	{
		return msg;
	}
}