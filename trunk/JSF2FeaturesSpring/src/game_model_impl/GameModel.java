package game_model_impl;

import dto.TimeDTO;
import entities.Civil;
import entities.UsersCityConfig;
import game_model_impl.Text.Text.Language;
import game_model_interfaces.IGameModel;
import game_model_interfaces.PriceStock;
import helpers.Three;

import java.util.List;



public abstract class GameModel implements IGameModel{

	final protected int costPattern;
	final protected long baseBuildTime;
	protected WorldDefinition ch;
	private Civil civil;
	final protected int id;
	final protected int buildTimePattern;
	final protected PriceStockSetImpl cost;	
	//protected ModelTextBunch mtb;
	public GameModel(int id, int costPattern, PriceStockSetImpl cost, long baseBuildTime, int buildTimePattern){
		this.buildTimePattern = buildTimePattern;
		this.baseBuildTime = baseBuildTime;
		this.costPattern = costPattern;
		this.cost = cost;
		this.id = id;
	};
	
	@Override
	public boolean equals(Object o){
		IGameModel gm = (IGameModel) o;
		return this.id == gm.getId();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T extends IGameModel> T cast(Class<T> clazz){
		if(clazz.isAssignableFrom(this.getClass()))return (T) this;
		else return null;
	}
	public <T extends IGameModel> boolean canCast(Class<T> clazz){
		T obj = this.cast(clazz);
		if(obj == null)return false;
		else return true;
	}
	@Override
	public final String getName(UsersCityConfig ucc){
		return null;
	}
	@Override
	public WorldDefinition getCivilizationHolder(){
		return ch;
	}
	void setCivilizationHolder(WorldDefinition ch){
		this.ch = ch;
	}
	void setCivil(Civil civil){this.civil = civil;}
	@Override
	public Civilization getMyCivilization() {
		return this.getCivilizationHolder().getCivilization(civil);
	}
    private RequirementNode requirementNode;
    void setRequirementNode(RequirementNode reqNode){
    	this.requirementNode = reqNode;
    }
	
	@Override
	public int getId(){
		return id;
	}
	
	@Override
	public String printDecoratorChain() {
		return "GameModel(id: " + this.id + ")";
	}
	
	@Override
	public String dump(Language lang, Civil civil){
		List<Three<Integer,String,String>> list = this.getRequirementNode().getMyGameModel().dumpHeader(lang, civil);
		String ret = "   NAZWA: \n";
		for(Three<Integer,String,String> three : list) {
			ret += "      " + three.second + "(wymagany level " + three.third + ":" + three.first + ") ->\n";
		}
		ret += " \n ";
		ret += this.getRequirementNode().getMyGameModel().dumpBody(lang, civil);
		return ret;
	}
	
	@Override
	public String getSimpleName(Language lang) {
		return null;
	}
	
	@Override
	public List<Three<Integer, String, String>> dumpHeader(Language lang, Civil civil){
		return null;
	}
	
	@Override
	public String dumpBody(Language lang, Civil civil)
	{
		String ret = "";
		ret += "   WYMAGANIA:\n";
		for(RequirementLink link : this.getRequirementNode().getParents()){
			ret += "      -" + link.getParentNode().getMyGameModel().dumpHeader(lang, civil).get(0).second + ", wymagany poziom: " +
					link.requiredLevelForParent(0);
			ret += "\n";
		}
		ret += "   CENA:\n      level";
		for(PriceStock price : this.cost.getStocks(getMyCivilization()) ){
			ret += "      " + price.getStockId() + "  ";
		}
		ret +="\n";
		for(int i=0 ; i <= 30 ; i++ )
		{
			PriceStockSetImpl pss = this.getMyCivilization().getCostByPattern( this.cost, this.costPattern, i, null);
			ret += "      " + i;
			for(PriceStock price : pss.getStocks(getMyCivilization()) )
			{
				ret += "          " + price.getVisibleAmount() + "  ";
			}
			ret += "\n";
		}
		ret += "   CZAS BUDOWY:\n      level\n";
		for(int i=0 ; i <= 30 ; i++ )
		{
			long timeMs = this.getMyCivilization().getBuildingBuildTimeByPattern(buildTimePattern, i, this.baseBuildTime);
			ret += "      " + i + "    " + new TimeDTO(timeMs).toString() +"\n";
		}
		return ret;
	}
	
	@Override
	public String getDescription(UsersCityConfig uc){
		return null;
	}
	@Override
	public RequirementNode getRequirementNode(){
		return requirementNode;
	}
	@Override
	public GameModel getInternalGameModel(){
		return this;
	}
	@Override
	public Class<? extends IGameModel> getObjectsClass() {
		return (Class<? extends IGameModel>) this.getClass();
	}
	
}
