package game_model_impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import entities.stocks.CityStock;
import entities.stocks.CityStockSet;
import framework.StockSet;
import game_model_interfaces.ICivilization;
import game_model_interfaces.PriceStock;
import game_model_interfaces.PriceStockSet;
import game_model_interfaces.TaxPriceStock;
import helpers.CollectionUtil;
import helpers.DefaultStockBinder;
import helpers.Pair;

public class PriceStockSetImpl extends PriceStockSet{

	/*
	 * (non-Javadoc)
	 * @see pojo.stocks.StockSet#getStocks()
	 * PriceStockSet mo�e zale�e� od gracza, je�li jest w nim ustawiony podatek, tzn dla ka�dego gracza mo�e by� inny.
	 * Je�li priceStockSet zawiera cene-podatek, metoda zwr�ci null, poniewa� nie da sie stwierdzi� ceny bez znajomo�ci dla
	 * kt�rego gracza nale�y ustali� cen�. W tym przypadku nale�y u�yc metody getStocks(UserConfig).
	 */
	@Override
	protected List<PriceStock> getStocks() {
		return this.stocks;
	}
	public List<PriceStock> getStocks(ICivilization civil) {
		return this.adjustPriceStockSetForPlayer(civil).stocks;
	}
	
	public PriceStock getStockByType(StockType type, ICivilization civil)
	{
		PriceStockSetImpl adjusted = this.adjustPriceStockSetForPlayer(civil);
		for(PriceStock s : adjusted.getStocks())
		{
			if(s.getStockType() == type )return s;
		}
		return null;
	}
	
	private List<PriceStock> stocks = new ArrayList<PriceStock>();
	
//	public void multiplyAllStocks(int multiplier)
//	{
//		for(PriceStock ps : stocks)
//		{
//			ps.multiply(multiplier);
//		}
//	}
	
	
	/*
	 * TODO - u�y� CollectionUtil.joinCollectionsAndPerformOpperation().
	 */
	
	
	
	public PriceStockSetImpl(PriceStockSetImpl pss, double multiply)
	{
		this(pss);
		for(PriceStock ps : this.getStocks())
		{
			ps.multiply(multiply);
		}
	}
	private PriceStockSetImpl(PriceStockSetImpl pss)
	{
		this(pss.getStocks().toArray(new PriceStock[pss.getStocks().size()]));
	}
	public PriceStockSetImpl(PriceStock... fees)
	{
		for(int i=0 ; i < fees.length ; i++)
		{
			PriceStock f = fees[i].clone();
			stocks.add(f);
		}
	}

}
