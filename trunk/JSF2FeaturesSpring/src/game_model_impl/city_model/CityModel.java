package game_model_impl.city_model;

import entities.UsersCityConfig;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import game_model_interfaces.ICityModel;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IHousesModel;
import game_model_interfaces.IWorldDefinition;

import java.util.ArrayList;
import java.util.List;


public class CityModel implements ICityModel{

	@Override
	public int getSatisfactionFromObjects(UsersCityConfig ucc) {
		IWorldDefinition civil = ucc.getUserConfig().getMyWorldDefinition();
		ICivilization civ = ucc.getUserConfig().getMyCivilization();
		int satisfaction = 0;
		List<StockType> dobory = ucc.getCityStocks().getAvailableFood();
		
		List<StockType> majorFoods = new ArrayList<StockType>();
		majorFoods.addEdgeAndReversedEdge(civ.getMajorAddition());
		majorFoods.addEdgeAndReversedEdge(civ.getMajorCarbo());
		majorFoods.addEdgeAndReversedEdge(civ.getMajorMeat());
		majorFoods.add(StockType.water);
		for( StockType rt : dobory)
		{
			if(majorFoods.contains(rt)){
				switch(rt.getFoodFamily())
				{
				case water:
					satisfaction += civil.getWaterSatisfaction();
					break;
				case meat:
					satisfaction += civil.getMajorMeatSatisfaction();
					break;
				case carbo:
					satisfaction += civil.getMajorCarboSatisfaction();
					break;
				case addition:
					satisfaction += civil.getMajorAdditionSatisfaction();
					break;
				default:
					break;
				}
			}
			else{
				switch(rt.getFoodFamily())
				{
				case meat:
					satisfaction += civil.getFurtherMeatSatisfaction();
					break;
				case carbo:
					satisfaction += civil.getFurtherCarboSatisfaction();
					break;
				case addition:
					satisfaction += civil.getFurtherAdditionSatisfaction();
					break;
				default:
					break;
				}
			}
		}
		satisfaction -= ucc.getTaxSatisfaction();
		return satisfaction;
		
	}
	
	@Override
	public double getBirthRatePerHour(UsersCityConfig ucc)
	{
		ICivilization config = ucc.getUserConfig().getMyCivilization();
		WorldDefinition wd = ucc.getUserConfig().getMyWorldDefinition();
		IHousesModel houses = config.getGameModelList(IHousesModel.class, false, true).get(0);
		double ret = 0;
		if(ucc.getCityStocks().getNastykStoredFood().size() > 0 )
		{
			if( houses.getPeopleLimit(ucc) < ucc.getCityStocks().getPeopleStock().getVisibleAmount() )
			{
				ret = wd.getPeopleFallRate();
			}
			else ret = 0.0d;
		}
		else if(ucc.getCityStocks().isStarvation())
		{
			if(ucc.getCityStocks().getPeopleStock().getHiredPeople() == ucc.getCityStocks().getPeopleStock().getVisibleAmount() )
			{
				return 0.0d;
			}
			ret = wd.getPeopleFallRate();
		}
		else
		{
			if( houses.getPeopleLimit(ucc) < ucc.getCityStocks().getPeopleStock().getVisibleAmount())
			{
				ret = wd.getPeopleFallRate();
			}
			else if( houses.getPeopleLimit(ucc) == ucc.getCityStocks().getPeopleStock().getVisibleAmount())
			{
				ret = 0;
			}
			else ret = wd.getBirthMultiplier() * this.getSatisfactionFromObjects(ucc);
		}
		return ret;	
	}

}
