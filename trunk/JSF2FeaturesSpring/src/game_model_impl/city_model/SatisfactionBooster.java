package game_model_impl.city_model;

import game_model_interfaces.IGameModel;
import helpers.LevelHelper;
import entities.UsersCityConfig;

public class SatisfactionBooster extends CityModelBooster {

	public SatisfactionBooster(int myObjectId, int boostPattern)
	{
		super(myObjectId,boostPattern);
	}
	@Override
	public int getSatisfactionFromObjects(UsersCityConfig uc)
	{
		int currSatisfaction = element.getSatisfactionFromObjects(uc);
		IGameModel myObject = this.boostersCivilization.getById(myObjectId, IGameModel.class);
		Integer lvl = LevelHelper.getBuildingOrResearchLvl(uc, myObject);
		if(lvl==null)return element.getSatisfactionFromObjects(uc);
		return boostersCivilization.boostSatisfaction(currSatisfaction, lvl, this.boostPattern);
	}
}
