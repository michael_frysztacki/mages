package game_model_impl.city_model;

import entities.UsersCityConfig;
import game_model_impl.Civilization;
import game_model_interfaces.ICityModel;

public abstract class CityModelBooster implements ICityModel{

	protected ICityModel element;
	protected final int myObjectId, boostPattern;
	protected Civilization boostersCivilization;
	public void setBoostersCivilization(Civilization civil){
		this.boostersCivilization = civil;
	}
	public CityModelBooster(int myObjectId, int boostPattern){
		this.myObjectId = myObjectId;
		this.boostPattern = boostPattern;
	}
	public ICityModel dekoruj(ICityModel modeldekorowany){
		this.element = modeldekorowany;
		return this;
	}
	@Override
	public int getSatisfactionFromObjects(UsersCityConfig ucc) {
		return element.getSatisfactionFromObjects(ucc);
	}

	@Override
	public double getBirthRatePerHour(UsersCityConfig ucc) {
		return element.getBirthRatePerHour(ucc);
	}

}
