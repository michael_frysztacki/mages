package game_model_impl;

import game_model_interfaces.ICivilization;
import game_model_interfaces.IRequirementLink;

public class RequirementLink implements IRequirementLink {

	private RequirementNode parentNode,childNode;
	private int patternId;
	private boolean isMotherBuilding;
	
	public RequirementLink(RequirementNode parent, RequirementNode child, int patternId,boolean isMotherBuilding) {
		
		this.parentNode = parent;
		this.childNode = child;
		this.isMotherBuilding = isMotherBuilding;
		this.patternId = patternId;
	}
	public RequirementNode getChildNode() {
		return childNode;
	}
	public RequirementNode getParentNode() {
		return parentNode;
	}
	public boolean isMotherBuilding() {
		return this.isMotherBuilding;
	}
	public int requiredLevelForParent(Integer childsLevel) {
		
		ICivilization myCivil = getChildNode().getMyGameModel().getMyCivilization();
		int reqParentsLevel = myCivil.getMaxLvlByPattern( childsLevel , patternId );
		return reqParentsLevel;
	}
	public int getPatternId() {
		
		return patternId;
	}

}
