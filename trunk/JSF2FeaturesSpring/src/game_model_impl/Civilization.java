package game_model_impl;

import game_model_interfaces.ICityModel;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IGameModel;
import game_model_interfaces.civilization_defining_interfaces.ICivilizationConfigProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Civilization implements ICivilization {
	
	public static interface CivilizationVisitor {
		
		public void visit(Civilization civilization);
	}
	
	private List<Civilization> childCivilizations = new ArrayList<Civilization>();
	protected ICivilizationConfigProvider civilProvider;
	protected Map<Integer,IGameModel> map = new HashMap<Integer,IGameModel>();
	
	void accept(CivilizationVisitor visitor){
		visitor.visit(this);
	}
	/*
	 * down-cast return type from ICivilization to Civilization
	 */
	@Override
	public abstract Civilization getParentCivilization();
	
	@Override
	public abstract <T extends IGameModel> T getById(int id, Class<T> clazz);
	/* (non-Javadoc)
	 * @see model.ICivilization#getCityModel()
	 */
	@Override
	public abstract ICityModel getCityModel();
	/* (non-Javadoc)
	 * @see model.ICivilization#getParentCivilization()
	 */

	/* (non-Javadoc)
	 * @see model.ICivilization#getChildCivilizations()
	 */
	@Override
	public List<ICivilization> getChildCivilizations(){
		return new ArrayList<ICivilization>( this.childCivilizations );
	}
	/* (non-Javadoc)
	 * @see model.ICivilization#getGameModelList(boolean)
	 */
	@Override
	public List<IGameModel> getGameModelList(boolean fullList){
		return this.getGameModelList(IGameModel.class, true, fullList);
	}
	/* (non-Javadoc)
	 * @see model.ICivilization#getGameModelList(java.lang.Class, boolean, boolean)
	 */
	@Override
	public <T extends IGameModel> List<T> getGameModelList(Class<T> objectType, boolean inherit, boolean fullList){
		List<T> ret = new ArrayList<T>();
		List<IGameModel> l_list = null;
		if(fullList)l_list = this.getFullGameModelList();
		else l_list = this.getInternalGameModelList();
		for(IGameModel b : l_list)
		{
			if(!inherit){
				Class<?>[] classes = b.getObjectsClass().getInterfaces();
				for(Class<?> clazz : classes){
					if(clazz == objectType)
						ret.add((T) b);
				}
			}
			else{
				if(objectType.isAssignableFrom(b.getObjectsClass()))
					ret.add((T) b);
			}
		}
		return ret;
	}
	@Override
	public boolean equals(Object obj){
		Civilization civil = (Civilization) obj;
		return civil.getCivilization() == this.getCivilization();
	}
	boolean updateIGameModel(IGameModel gm){
		IGameModel local_gm = map.get(gm.getId());
		if(local_gm != null){
			map.put(gm.getId(), gm);
			return true;
		}
		return false;
	}
	ICivilizationConfigProvider getConfig(){
		return this.civilProvider;
	}
	void addIGameModel(IGameModel gm){
		this.map.put(gm.getId(), gm);
	}
	void addChildCivilizations(Civilization childCivil){
		this.childCivilizations.add(childCivil);
	}
	
	protected List<IGameModel> getInternalGameModelList(){
		return new ArrayList<IGameModel>( map.values());
	}
	protected abstract List<IGameModel> getFullGameModelList();
	protected final <T extends IGameModel> T getByIdInInternalMap(int id, Class<T> clazz) {
		IGameModel gm = map.get(id);
		if(gm != null)
			return gm.cast(clazz);
		return null;
	}
	
	@Override
	public Class<?> getCivilClass(){
		return this.getClass();
	}
}
