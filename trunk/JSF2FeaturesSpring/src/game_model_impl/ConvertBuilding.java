package game_model_impl;
import java.util.ArrayList;
import java.util.List;

import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;
import game_model_interfaces.IConvertBuildingModel;
import game_model_interfaces.IExtractBuildingModel;

public class ConvertBuilding extends ExtractBuilding implements IConvertBuildingModel{

	public ConvertBuilding(int id, int maxPeople, int timePattern, int maxPeoplePattern,
			int costPattern, PriceStockSetImpl cost, int productionPattern,
			long baseBuildTime, StockType typ) {
		super( id,  maxPeople,
				 timePattern, costPattern,  productionPattern,
				 maxPeoplePattern, cost, baseBuildTime, typ);
	}

	@Override
	public Double getProductionRate(UsersCityConfig ucc)
	{
		//Civilization myCivil = ch.getCivilization(ucc.getUserConfig().getCivil());
		Double maxRatio = null;
		IConvertBuildingModel thisCb=null;
		thisCb = this.getRequirementNode().getMyGameModel().cast(IConvertBuildingModel.class);
		for(StockType rt : thisCb.getInputTypes())
		{
			if(!ucc.getCityStocks().getStockByType(rt).isAvailable())
			{
				IExtractBuildingModel producer = this.ch.getProducer(rt);
				int need = thisCb.getInputPerHour(ucc, rt);// potentiallyHighestInputPerHour(ucc, rt);
				int production = producer == null ? 0 : producer.getOutputPerHour(ucc);
				double ratio = (double)need/(double)production;
				if(maxRatio == null || ratio > maxRatio)
				{
					maxRatio = ratio;
				}
			}
		}
		
		/*
		 * je�li minProduction == null, to znaczy ze kazdy rodzaj jedzenia potrzebny do prodoukcji pozywienia przez
		 * ten budynek konwertuj�cy, jest dostarczony. 
		 */
		if(maxRatio == null)return 1.0d;
		double ret = 1.0d/maxRatio;
		return ret > 1.0d ? 1.0d : ret;
		
	}
	@Override
	public int getOutputPerHour(UsersCityConfig uc)
	{		
		ExtractCityBuilding ecb = uc.getBuilding(ExtractCityBuilding.class, this.getId());
		Civilization config =  this.getCivilizationHolder().getCivilization( ecb.getBuildingsCivil() );
		int productionPattern = config.getProductionPatternById(getId());
		Integer production = (int) ( getProductionRate(uc) * config.getProductionByPattern( ecb.getPopulation(), productionPattern));
		return production;
	}
	@Override
	public int getPureOutputPerHour(UsersCityConfig uc)
	{
		ExtractCityBuilding ecb = uc.getBuilding(ExtractCityBuilding.class, this.getId());
		Civilization config =  this.getCivilizationHolder().getCivilization( ecb.getBuildingsCivil() );
		int productionPattern = config.getProductionPatternById(getId());
		Integer production = (int) ( config.getProductionByPattern(ecb.getPopulation(), productionPattern));
		return production;
	}	@Override
	public List<StockType> getInputTypes(){
		return new ArrayList<StockType>();
	}
	/*
	 * metoda zwraca ile surowca 'resource' pobiera budynek konwertuj�cy na godzin�.
	 * w tym obiekcie zwracane jest 0, dlatego �e t� metod� obs�uguj� boostery pod��czone do tego budynku,
	 * kt�re powinny znajdowa� si� na ko�cu "�a�cucha".
	 */
	@Override
	public int getInputPerHour(UsersCityConfig ucc, StockType resource) {
		return 0;
	}
//	@Override
//	public int potentiallyHighestInputPerHour(UsersCityConfig ucc,
//			StockType resource) {
//		return 0;
//	}
	
}



