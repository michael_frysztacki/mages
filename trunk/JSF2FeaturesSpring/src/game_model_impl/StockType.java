package game_model_impl;

import java.util.ArrayList;
import java.util.List;

import game_model_impl.StockType.StockFamily.FoodFamily;


public enum StockType
{
	//food
	water(StockFamily.food,"woda"),
	rice(StockFamily.food,"ry�"),
	pork(StockFamily.food,"wieprzowina"),
	beef(StockFamily.food,"wo�owina"),
	sul(StockFamily.food,"sul"),
	brewery(StockFamily.food,"browar"),
	fish(StockFamily.food,"ryby"),
	wheat(StockFamily.food,"pszenica"),
	soy(StockFamily.food,"soja"),
	poultry(StockFamily.food, "chicken"),
	
	// resources
	stone(StockFamily.resource,"kamien"),
	iron(StockFamily.resource,"zelazo"),
	wood(StockFamily.resource,"drewno"),
	
	// tax
	gold(StockFamily.tax,"z�oto"),
	silver(StockFamily.tax,"srebro"),
	
	//special
	slaves(StockFamily.people,"niewolnicy"),
	peopleCount(StockFamily.people,"ludzie");
	
	public enum StockFamily
	{
		tax,
		food,
		resource,
		people;
		public enum FoodFamily
		{
			water,
			carbo,
			meat,
			addition
		}
	};
	public boolean isFood()
	{
		return this.sf == StockFamily.food;
	}
	
	public FoodFamily getFoodFamily()
	{
		FoodFamily rodzina = null;
		switch(this)
		{
			case sul:
			case soy:
			case fish:
			case brewery:
				rodzina = FoodFamily.addition;
				break;
			case water:
				rodzina = FoodFamily.water;
				break;
			case rice:
			case wheat:
				rodzina = FoodFamily.carbo;
				break;
			case beef:
			case pork:
			case poultry:
				rodzina = FoodFamily.meat;
			default:
				break;
		}
		return rodzina;
	}
	
	private final StockFamily sf;
	private final String str;
	
	public StockFamily getStockFamily()
	{
		return sf;
	}
	
	public static List<StockType> getStockValues(StockFamily sf)
	{
		List<StockType> ret = new ArrayList<StockType>();
		for(StockType st : values())
		{
			if(st.getStockFamily() == sf)ret.add(st);
		}
		return ret;
	}
	StockType(StockFamily sf, String str)
	{
		this.str = str;
		this.sf = sf;
	}
	public static StockType StrindgToRt(String str)
	{
		// TODO trzeba zrobic porzadna translacje ze string na ResourceType
		for(StockType rt : StockType.values()){
			if(rt.str == str)return rt;
		}
		return null;
		
	}
	
}




