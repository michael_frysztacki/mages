package game_model_impl;

import game_model_interfaces.civilization_defining_interfaces.ICivilizationConfigProvider;

import java.util.ArrayList;
import java.util.List;


class CivilizationConfigValidator
{
	/**
	 * 
	 */
	//private final WorldDefinition.WorldBuilder CivilizationConfigValidator;

	/**
	 * @param worldBuilder
	 */
	/*
	CivilizationConfigValidator(
			WorldDefinition.WorldBuilder worldBuilder) {
		CivilizationConfigValidator = worldBuilder;
	}
	*/
	void validate(ICivilizationConfigProvider commonConfig, ICivilizationConfigProvider... extendedCivils) throws BuildModelException{
		this.CONVERT_BUILDING_checkIfInputStocksIsTax(commonConfig, extendedCivils);
		this.COMMON_checkIfIdAreRepeatedOrNegativeNumbers(commonConfig, extendedCivils);
		this.COMMON_checkCivilDefinition(commonConfig, extendedCivils);
		this.REQUIREMENTS_only_one_mother_building(commonConfig, extendedCivils);
		this.REQUIREMENTS_check_if_researches_have_mother_buildings(commonConfig, extendedCivils);
		this.REQUIREMENTS_check_if_units_have_mother_buildings(commonConfig, extendedCivils);
		this.REQUIREMENTS_check_if_requirement_is_unit(commonConfig, extendedCivils);
		this.IMPROVEMENTS_check_if_top_civil_is_improving_object_in_lower_civil(commonConfig, extendedCivils);
	}
	/*
	 * metoda sprawdza czy jakikolwiek budynek konweruj�cy otrzymuje na swoj� wej�cie surowiec - podatek.
	 */
	private void CONVERT_BUILDING_checkIfInputStocksIsTax(ICivilizationConfigProvider commonConfig, ICivilizationConfigProvider... extendedCivils ) throws BuildModelException
	{
		// TODO
	}
	
	private void REQUIREMENTS_check_if_researches_have_mother_buildings(ICivilizationConfigProvider commonConfig, ICivilizationConfigProvider... extendedCivils ) throws BuildModelException
	{
		// TODO
	}
	
	private void REQUIREMENTS_check_if_units_have_mother_buildings(ICivilizationConfigProvider commonConfig, ICivilizationConfigProvider... extendedCivils ) throws BuildModelException
	{
		// TODO
	}
	
	/*
	 * unity nie mog� by� wymaganiem.
	 */
	private void REQUIREMENTS_check_if_requirement_is_unit(ICivilizationConfigProvider commonConfig, ICivilizationConfigProvider... extendedCivils ) throws BuildModelException
	{
		// TODO
	}
	
	/*
	 * badania i unit mog� mie� tylko jeden mother building w wymaganiach.
	 */
	private void REQUIREMENTS_only_one_mother_building(ICivilizationConfigProvider commonConfig, ICivilizationConfigProvider... extendedCivils ) throws BuildModelException
	{
		// TODO
	}
	
	/*
	 * sprawdzenie, czy w cywilizacjach nie ma booster�w, kt�re do��cza�yby si� do obiekt�w w cywilziacjach ni�ej.
	 * Oczywi�cie mowa tutaj o boosterach, kt�re do��czane s� z pomoc� id obiektow ( DecoratorPattern(int)).
	 */
	private void IMPROVEMENTS_check_if_top_civil_is_improving_object_in_lower_civil(ICivilizationConfigProvider commonConfig, ICivilizationConfigProvider... extendedCivils ) throws BuildModelException
	{
		// TODO
	}
	
	private void COMMON_checkIfIdAreRepeatedOrNegativeNumbers(ICivilizationConfigProvider commonConfig, ICivilizationConfigProvider... extendedCivils ) throws BuildModelException
	{
		List<Integer> allIds = new ArrayList<Integer>();
		if(commonConfig.gameModels()!=null)
		for(GameModel gm : commonConfig.gameModels())
		{
			allIds.add(gm.getId());
		}
		for(ICivilizationConfigProvider civ : extendedCivils)
		{
			if(civ.gameModels() != null)
			for(GameModel gm : civ.gameModels())
			{
				allIds.add(gm.getId());
			}
		}
		for(int i=0; i < allIds.size() ; i++)
		{
			if(allIds.get(i)<0)throw new BuildModelException("id GameModelu nie mo�e by� ujemne, zastosowane id:" + allIds.get(i));
		}
		java.util.Collections.sort(allIds);
		for(int i=0; i < allIds.size()-1 ; i++)
		{
			int now = allIds.get(i);
			int next = allIds.get(i+1);
			if(now==next)throw new BuildModelException("powtarzaj�ce si� id w obiektach cywilizacyjnych:" + Integer.toString(now));
		}
		
	}
	
	/*
	 * Are Extended Civilizations Extending Another Civilization And Are They Defining Their Own Civilization
	 */
	private void COMMON_checkCivilDefinition(ICivilizationConfigProvider commonConfig, ICivilizationConfigProvider[] extendedCivils) throws BuildModelException
	{
		
		if(commonConfig.getCivilization() == null) throw new BuildModelException("Cywilizacja: " + commonConfig + "nie definiuje �e jest cywilizacj� wsp�ln�");
		
		for(ICivilizationConfigProvider civil : extendedCivils)
		{
			if(civil.extendsCivilization() == null) throw new BuildModelException("Cywilizacja "+ civil + "nie rozszerza �adnej cywilizacji.");
			if(civil.getCivilization() == null) throw new BuildModelException("Cywilizacja: " + civil + "nie definiuje getCivilization()");
		}
	}
}