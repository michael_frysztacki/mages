package game_model_impl;

public class Spong {

	public int patternId;
	public ConvertBuilding[] spongers;
	public Spong(int patternId, ConvertBuilding... spongers)
	{
		this.patternId = patternId;
		this.spongers = new ConvertBuilding[spongers.length];
		for(int i=0; i < spongers.length ; i++) {
			
			this.spongers[i]=spongers[i];
		}
	}
	
}
