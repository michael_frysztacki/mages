package game_model_impl;

import game_model_interfaces.IBuildingModel;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IGameModel;
import game_model_interfaces.IRequirementLink;
import game_model_interfaces.IRequirementNode;
import helpers.LevelHelper;
import helpers.Pair;

import java.util.ArrayList;
import java.util.List;

import entities.Civil;
import entities.UsersCityConfig;

public class RequirementNode implements IRequirementNode {

	private List<RequirementLink> parents = new ArrayList<RequirementLink>();
	private List<RequirementLink> children = new ArrayList<RequirementLink>();
	private IGameModel myGameModel;
	@Override
	public List<IRequirementLink> getParents() {
		return parents;
	}
	public List<IRequirementLink> getChildren() {
		return children;
	}
	public IGameModel getMyGameModel() {
		
		return myGameModel;
	}
	void setMyObject(IGameModel gameModel){
		this.myGameModel = gameModel;
	}
	
	/* (non-Javadoc)
	 * @see model.IRequirementNode#canBuild(entities.UsersCityConfig)
	 */
	@Override
	public boolean canBuild(UsersCityConfig where) {
		
		Integer myModelsLevel= LevelHelper.getBuildingOrResearchLvl(where, this.getMyGameModel());
		for(RequirementLink link : parents) {
			Integer parentsLevel = LevelHelper.getBuildingOrResearchLvl(where, link.getParentNode().getMyGameModel());
			int requiredParentsLevel = link.requiredLevelForParent(myModelsLevel);
			if(parentsLevel < requiredParentsLevel)return false;
		}
		return true;
	}
	
	public <T extends IGameModel> List<T> getParents(Class<T> clazz)
	{
		List<T> ret = new ArrayList<T>();
		for(RequirementLink reqLink : this.getParents())
		{
			RequirementNode node = reqLink.getParentNode();
			T obj = node.getMyGameModel().cast(clazz);
			if(obj != null) ret.add(obj);
		}
		return ret;
	}
	
	/*
	 * Metoda zwraca list� par, pierwszy cz�on w parze oznacza wymagany level parent, drugi cz�on to parent.
	 */
	public List<Pair<Integer,IGameModel>> getParentRequirements(UsersCityConfig ucc)
	{
		List<Pair<Integer,IGameModel>> ret = new ArrayList<Pair<Integer,IGameModel>>();
		for(RequirementLink link : this.getParents())
		{
			Integer requiredParentsLvl = link.requiredLevelForParent( LevelHelper.getBuildingOrResearchLvl(ucc, this.getMyGameModel()));
			ret.add(new Pair<Integer,IGameModel>(requiredParentsLvl,link.getParentNode().getMyGameModel()) );
		}
		return ret;
	}
	
	
	public List<RequirementNode> whatChildrenCanBeUpgradedForFollowingArguments(int levelOfThisNodesModel, int childLevel, Class<? extends IGameModel> childrenClass, Civil childsCivil) {
		
		List<RequirementNode> ret = new ArrayList<RequirementNode>();
		for(RequirementLink link : this.getChildren()) {
			
			IGameModel gm = link.getChildNode().getMyGameModel().cast(childrenClass);
			if(gm == null)continue;
			IGameModel childGm =link.getChildNode().getMyGameModel(); 
			ICivilization foundCivil = childGm.getCivilizationHolder().findCivilizationInChain(childGm.getMyCivilization().getCivilization(), childsCivil);
			if(foundCivil == null)continue;
			int requiredParentsLevel = link.requiredLevelForParent(childLevel);
			if(requiredParentsLevel == levelOfThisNodesModel)ret.add(link.getChildNode());
		}
		return ret;
	}
	
	public RequirementNode getChildById(int id)
	{
		for(RequirementLink rLink : this.getChildren())
		{
			if(rLink.getChildNode().getMyGameModel().getId() == id)return rLink.getChildNode();
		}
		return null;
	}
	
	public RequirementNode getParentById(int id)
	{
		for(RequirementLink rLink : this.getParents())
		{
			if(rLink.getParentNode().getMyGameModel().getId() == id)return rLink.getParentNode();
		}
		return null;
	}
	
	public <T extends IGameModel> List<T> getChildrenRecursively(Class<T> clazz) {
		List<T> ret = new ArrayList<T>();
		if(this.getChildren(IGameModel.class).size()==0)return ret;
		ret.addAll(this.getChildren(clazz));
		for(IGameModel gm : this.getChildren(IGameModel.class)){
			for(T i : gm.getRequirementNode().getChildrenRecursively(clazz)){
				if(!ret.contains(i))ret.add(i);
			}
		}
		return ret;
	}
	public <T extends IGameModel> List<T> getChildren(Class<T> clazz){
		List<T> ret = new ArrayList<T>();
		for(RequirementLink reqLink : this.getChildren())
		{
			RequirementNode node = reqLink.getChildNode();
			T obj = node.getMyGameModel().cast(clazz);
			if(obj != null)ret.add(obj);
		}
		return ret;
	}
	
	/*
	 * zwraca budynek w kt�rym mozemy wybudowa� ten obiekt.
	 * Je�eli ten obiekt jest budynkiem, zwraca null.
	 */
	public IBuildingModel getMotherBuilding()
	{
		for(RequirementLink link : parents)
			if(link.isMotherBuilding())return link.getParentNode().getMyGameModel().cast(IBuildingModel.class);
		return null;
	}
	
	void addParent(RequirementLink link){
		this.parents.add(link);
	}
	void addChild(RequirementLink link){
		this.children.add(link);
	}
	
}
