package interfaces;

import java.lang.reflect.InvocationTargetException;

public interface IBuilding {

	/*
	 * metoda umozliwiajaca budowanie unitow.
	 * Jesli nie mamy wystarczajaco surowcow na wybodowanie ilsosc count,
	 * zostanie wybudowana najwieksza mozliwa ilosc
	 */
	public int buildUnit(int unitId,int count) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;
	public void destroy();
}
