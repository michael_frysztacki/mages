package interfaces;

import java.lang.reflect.InvocationTargetException;

import game_model_impl.StockType;


public interface IExtractBuilding extends IBuilding{

	public boolean canSetPeople(int count) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException;
	public boolean setPeople(int count) throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException ;
	public int getPopulation();
}
