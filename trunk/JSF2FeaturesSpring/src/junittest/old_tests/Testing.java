package junittest.old_tests;

import java.lang.reflect.InvocationTargetException;
import java.util.GregorianCalendar;

import java.util.List;


import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jsfsample.managedbeans.SpringJSFUtil;

import citys_interaction.CityHolder;
import citys_interaction.FightComp;

import economycomputation.CityCompUtil;
import economycomputation.ConvertBuildingUtil;
import entities.CityLode;
import entities.Point;
import entities.UserConfig;
import entities.UserResearch;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.ConvertCityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.stocks.CityResource;
import entities.stocks.TripStock;
import entities.units.CityUnitGroup;
import game_model_impl.Building;
import game_model_impl.ConvertBuilding;
import game_model_impl.ExtractBuilding;
import game_model_impl.Research;
import game_model_impl.StockType;
import game_model_impl.Unit;
import game_model_impl.WorldDefinition;
import game_utilities.TripUtil;

public class Testing {

	public static boolean testing = false;
	@Autowired public CityCompUtil fc;
	@Autowired public TripUtil tripUtil;
	@Autowired public FightComp fightComp;
	@Autowired public ConvertBuildingUtil cbu;
	@Autowired public WorldDefinition ch;
	@Autowired public TripUtil tu;
	@Autowired public CityHolder cityHolder;
	
	public UsersCityConfig initCity(UserConfig uc,Point point,boolean capital) throws InstantiationException, IllegalAccessException 
	{
		UsersCityConfig ucc = new UsersCityConfig(uc, true, point, 1, ch);
		//Point p = new Point();
		point.setX(1.0d);
		ucc.setTest(2);
		point.setY(1.0d);
		ucc.setPointId(point);
		ucc.setNsTime_((long)System.currentTimeMillis()*1000l*1000l);
		ucc.setPointId(point);
		ucc.setUserConfig(uc);
		ucc.setCapital(capital);
		uc.getUsersCityConfigs().add(ucc);
		ucc.setMaxLudzi(0);
		//ucc.getMyResources().addConcreteResource(new ConcreteResource(ResourceType.peopleCount,0));
		List<Boostable> units = ch.getCivilization(ucc.getUserConfig().getCivil()).getBoostableList(Unit.class, true);
		for(Boostable b : units)
		{
			ucc.getArmy().getUnits(null).addEdgeAndReversedEdge(new CityUnitGroup(b.getId(),0,ucc.getArmy()));
		}
		ucc.getArmy().setUserConfig(uc);
		List<Boostable> buildings = ch.getCivilization(ucc.getUserConfig().getCivil()).getBoostableList(Building.class, true);
		for(Boostable b : buildings)
		{
			if(ExtractBuilding.class == b.getObjectsClass())
			{
				ExtractCityBuilding ecb = new ExtractCityBuilding(b.getId(),0,0, ucc);
				ucc.getBuildings().add(ecb);
			}
			else if(ConvertBuilding.class == b.getObjectsClass())
			{
				ConvertCityBuilding ccb = new ConvertCityBuilding(b.getId(), 0, 0, ucc);
				ucc.getBuildings().addEdgeAndReversedEdge(ccb);
			}
			else
			{
				CityBuilding cb = new CityBuilding(b.getId(), 0,ucc);
				ucc.getBuildings().add(cb);
			}
		}
		List<Boostable> researches = ch.getCivilization(ucc.getUserConfig().getCivil()).getBoostableList(Research.class, true);
		for(Boostable b : researches)
		{
			UserResearch ur = new UserResearch(b.getId(),0, ucc.getUserConfig());
			ucc.getUserConfig().getResearches().add(ur);
		}
		SpringJSFUtil.getCityCompUtil().getSN_SOP2(ucc);
		return ucc;
	}
	public static void initTools()
	{
//		testing = true;
//		cityHolder = new CityHolder();
//		tu = new TripUtil();
//		ch = new CivilizationHolder();
//		fightComp = new FightComp();
//		tripUtil = new TripUtil();
//		fc = new CityCompUtil();
//		fightComp.setCh(ch);
//		KoreaConfig kc = new KoreaConfig();
//		cbu = new ConvertBuildingUtil();
//		cbu.setCh(ch);
//		cbu.setFoodComp(fc);
//		fc.setCh(ch);
//		fc.setConvertBuildingUtil(cbu);
	}
}
