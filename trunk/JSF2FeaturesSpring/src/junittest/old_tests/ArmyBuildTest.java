package junittest.old_tests;

import static org.junit.Assert.assertTrue;

import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import helpers.TimeTranslation;

import java.lang.reflect.InvocationTargetException;



import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Civil;
import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.UnitBuild;

import services.CityService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext.xml")
public class ArmyBuildTest {

	public static UsersCityConfig ucc;
	
	@Autowired CityService cityService;
	@Autowired WorldDefinition ch;
	@Before
	public void setUp()
	{
		Testing.initTools();
		UserConfig friko = new UserConfig();
		friko.setCivil(Civil.korea);
		UserConfig galan = new UserConfig();
		galan.setCivil(Civil.korea);
		Point p1 = new Point();
		Point p2 = new Point();
		long nsNow = (long)System.currentTimeMillis()*1000l*1000l;
		ucc = new UsersCityConfig(friko, true, p1, nsNow, ch);
	}
	@Test
	public void test1() throws Exception
	{
		
		ucc.getBuilding(5).setLevel(1);
		ucc.getBuilding(36).setLevel(1);
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(100);
		ucc.getMyFood().getFood(StockType.beef).setCount(10000.0d);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		NotFoodSet_dead cost = config.getById(10).getPrice(ucc);
		for(int i=0;i<5;i++)
		{
			ucc.getMyResources().addNotFoodSet(cost);
		}
		
		assertTrue(ucc.getBuilding(5).buildUnit(10, 5) == 5);
		
		
		
		Boostable wlocznik = config.getById(10);
		long msWlocznikBuildTime = wlocznik.getBuildTime(ucc);
		
		SpringJSFUtil.getCityCompUtil().getFoodAT(ucc, TimeTranslation.msToNs(msWlocznikBuildTime), new Integer(0));
		
		assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 1.0d);
		assertTrue(((UnitBuild)ucc.getBuilding(5).getObjectBuildQueue().get(0)).getVisibleAmount() == 4.0d );
		
		assertTrue( ucc.getBuilding(36).buildUnit(37, 6) == 6);
		assertTrue( ucc.getBuilding(36).buildUnit(38, 7) == 7);
		assertTrue( ucc.getBuilding(36).buildUnit(39, 8) == 8);
		
		Boostable konnylucznik39 = config.getById(39);
		
		long nsTimeAfterTwoBuild = ucc.getBuilding(36).getObjectBuildQueue().get(0).getBuildTime() + ucc.getBuilding(36).getObjectBuildQueue().get(1).getBuildTime();
		nsTimeAfterTwoBuild +=  TimeTranslation.msToNs(konnylucznik39.getBuildTime(ucc) * 2);
		
		SpringJSFUtil.getCityCompUtil().getFoodAT(ucc, nsTimeAfterTwoBuild, new Integer(0));
		
		/*
		 * ten test nie przchodzi, wychodzi 1.(9).
		 * trzeba bedzie kiedys poprawic metoda getFoodAt zeby przyjmowala msTime a nie hTime
		 */
		assertTrue(ucc.getArmy().getUnitGroupById(39).getVisibleAmount() == 2.0d);
		
		
	}
}
