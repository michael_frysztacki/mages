package junittest.old_tests;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import game_model_impl.CommonCivilization;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import helpers.TimeTranslation;

import java.lang.reflect.InvocationTargetException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;



import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jsfsample.managedbeans.SpringJSFUtil;

import services.CityService;
import economycomputation.ConvertBuildingUtil;
import economycomputation.CityCompUtil;
import economycomputation.DefaultRecursionCdddalculator;
import economycomputation.RecursionCalculator;
import entities.CityLode;
import entities.Civil;
import entities.Point;
import entities.Storage;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.BuildingBuild;
import entities.buildings.CityBuilding;
import entities.buildings.ConvertCityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.stocks.CityFood;
import entities.stocks.CityResource;
import entities.stocks.CityStockSet;
import entities.stocks.TripStock;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext.xml")
public class GetFoodAtTest {

	private UsersCityConfig ucc;
	@Autowired CityService cityService;
	@Autowired CityCompUtil ccu;
	@Autowired WorldDefinition ch;
	@Before
	public void setUp() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	{
		Testing.initTools();
		UserConfig friko = new UserConfig();
		friko.setCivil(Civil.korea);
		Point p = new Point();
		long nsNow = (long)System.currentTimeMillis()*1000l*1000l;
		ucc = new UsersCityConfig(friko, true, p, nsNow, ch);
	}
	@Test
	public void test_getFoodAt21() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100.0d);
		
		ucc.setMaxLudzi(100);
	
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		
		NotFoodSet_dead cost = config.getById(ucc.getBuilding(5).getObjectId()).getPrice(ucc);
		//ustawiamy w mie�cie tyle surowc�w �eby starczylo na budowe budynku
		ucc.getMyResources().addNotFoodSet(cost);
		
		assertTrue(ucc.getBuilding(5).upgrade(null));
//		GregorianCalendar buildTime = new GregorianCalendar();
//		buildTime.setTimeInMillis(time.getTimeInMillis() + 1000);
//		BuildingBuild bb = new BuildingBuild(203,8,buildTime,ucc);
//		ucc.addBuildingBuild(bb);
		
//		GregorianCalendar buildTime2 = new GregorianCalendar();
//		buildTime2.setTimeInMillis(time.getTimeInMillis() + 15000);
//		BuildingBuild bb2 = new BuildingBuild(203,9,buildTime2,ucc);
//		ucc.addBuildingBuild(bb);
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeMs = 120*1000;
		//double afterTimeH = TimeTranslation.msToHours(afterTimeMs);
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeMs*1000*1000, new Integer(0));
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		//System.out.print("rice count:" +fs.getFood(ResourceType.rice).getCount() + "\n");
		//System.out.print("rice storage:" +fs.getFood(ResourceType.rice).getStorage() + "\n");
		
//		assertTrue(SN == 33.0d);
//		assertTrue(SOP == 2);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.water).getStorage() == Storage.tak);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.rice).getStorage() == Storage.nie);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.pork).getStorage() == Storage.tak);
		assertTrue(ucc.getLevel(5) == 1);
		assertTrue(ucc.getBuildingBuilds().size() == 0);
	}
	@Test
	public void test_getFoodAt() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100.0d);
		
		ucc.setMaxLudzi(100);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(34));
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		NotFoodSet_dead waterBuildingPrice = config.getById(203).getPrice(ucc);
		
		/*
		 * dodajemy surowce potrzebne na budowe dostawcy wody
		 */
		ucc.getMyResources().addNotFoodSet(waterBuildingPrice);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		
		assertTrue(ucc.getBuilding(203).upgrade(null));
//		GregorianCalendar buildTime = new GregorianCalendar();
//		buildTime.setTimeInMillis(time.getTimeInMillis() + 1000);
//		BuildingBuild bb = new BuildingBuild(203,8,buildTime,ucc);
//		ucc.addBuildingBuild(bb);
		
//		GregorianCalendar buildTime2 = new GregorianCalendar();
//		buildTime2.setTimeInMillis(time.getTimeInMillis() + 15000);
//		BuildingBuild bb2 = new BuildingBuild(203,9,buildTime2,ucc);
//		ucc.addBuildingBuild(bb);
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeMs = 120*1000;
		double afterTimeH = TimeTranslation.msToHours(afterTimeMs);
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeMs*1000l*1000l, new Integer(0));
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		//System.out.print("rice count:" +fs.getFood(ResourceType.rice).getCount() + "\n");
		//System.out.print("rice storage:" +fs.getFood(ResourceType.rice).getStorage() + "\n");
		
//		assertTrue(SN == 33.0d);
//		assertTrue(SOP == 2);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.water).getStorage() == Storage.tak);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.rice).getStorage() == Storage.nie);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.pork).getStorage() == Storage.tak);
		assertTrue(ucc.getLevel(203) == 2);
	}
	
	@Test
	public void test20_getFoodAt() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100.0d);
		ucc.setMaxLudzi(105);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(34));
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		
		NotFoodSet_dead waterBuildingPrice = config.getById(203).getPrice(ucc);
		/*
		 * dodajemy surowce potrzebne na budowe dostawcy wody
		 */
		ucc.getMyResources().addNotFoodSet(waterBuildingPrice);
		CityBuilding cb = ucc.getBuilding(203);
		assertTrue(cb.upgrade(null));
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeMs = 120*1000;
		double afterTimeH = TimeTranslation.msToHours(afterTimeMs);
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeMs*1000*1000, new Integer(0));
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		//System.out.print("rice count:" +fs.getFood(ResourceType.rice).getCount() + "\n");
		//System.out.print("rice storage:" +fs.getFood(ResourceType.rice).getStorage() + "\n");
		
//		assertTrue(SN == 33.0d);
//		assertTrue(SOP == 2);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.water).getStorage() == Storage.tak);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.rice).getStorage() == Storage.nie);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.pork).getStorage() == Storage.tak);
		assertTrue(ucc.getLevel(203) == 2);
	}
	@Test
	public void test11_getFoodAt() throws Exception{

		/*
		 * opis testu:
		 * jedzenie produkowane jest w sumie 1400 /h.
		 *tzn ze starcza dla 1400 ludzi. 
		 *
		 * Ludzi jest na poczatku 1279, ale pasek jest ustawiony na 1600.
		 * Kiedy ludzie osi�gn� ilo�� 1600, znaczy to �e jedzenia zacznie brakowa� w ko�cu.
		 * a to znaczy ze bedzie g��d
		 * ludzie zaczn� odchodzi� i powinno by� ich na ko�cu 1400  tyle ile jest produkowane jedzenia.
		 * Stan jedzenia powinien by� wtedy 0.0d dlatego �e ludzie wszystko zjadaj� - jest stan na styk.
		 */
		ucc.getPoint().getCityLode(StockType.water).setValue(5);
		ucc.getPoint().getCityLode(StockType.rice).setValue(5);
		ucc.getPoint().getCityLode(StockType.pork).setValue(5);
		
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		
		ucc.getPoint().getCityLode(StockType.gold).setValue(1);
		ucc.getPoint().getCityLode(StockType.stone).setValue(1);
		ucc.getPoint().getCityLode(StockType.iron).setValue(1);
		ucc.getPoint().getCityLode(StockType.wood).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(5.0d);
		ucc.getMyFood().getFood(StockType.beef).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.sul).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.wheat).setCount(10.0d);
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(1279.409d);
		ucc.setMaxLudzi(1600);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//ryz
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(205);//sul
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(200);//gold
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(201);//stona
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(214);//iron
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(206);//wood
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		assertTrue(ucc.getMaxLudzi() == 1600);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		/*
		 * czas po 10 godzinach.
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeNs = 1000l*1000l*1000l*60l*60l*72l;
		double afterTimeH = TimeTranslation.nsToHours(afterTimeNs);
		
		//afterTimeH = 0.12800;
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeNs, new Integer(0));
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		
		assertTrue(ucc.getPopulation(205) == 100);
		System.out.print("\nilosc ludzi w miescie: " + ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount() + "\n");
		//assertTrue(uccAfter.getMyResources().getResource(ResourceType.peopleCount).getCount() == 1400.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getVisibleAmount() == 0.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getVisibleAmount() == 0.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getVisibleAmount() == 0.0d);
		
	}
	/*
	 * test ktory w petli co iles sekund wypisuje stan miasta
	 * uruchamiaj go tylko o monitorowania, on sie nigdy nie konczy.
	 */
	@Test
	public void loopTest_getFoodAt() throws Exception{

		/*
		 * opis testu:
		 * jedzenie produkowane jest w sumie 1400 /h.
		 *tzn ze starcza dla 1400 ludzi. 
		 *
		 * Ludzi jest na poczatki 1279, ale pasek jest ustawiony na 1600.
		 * Kiedy ludzie osi�gn� ilo�� 1600, znaczy to �e jedzenia zacznie brakowa� w ko�cu.
		 * a to znaczy ze bedzie g��d
		 * ludzie zaczn� odchodzi� i powinno by� ich na ko�cu 1400  tyle ile jest produkowane jedzenia.
		 * Stan jedzenia powinien by� wtedy 0.0d dlatego �e ludzie wszystko zjadaj� - jest stan na styk.
		 */
		ucc.getPoint().getCityLode(StockType.water).setValue(5);
		ucc.getPoint().getCityLode(StockType.rice).setValue(5);
		ucc.getPoint().getCityLode(StockType.pork).setValue(5);
		
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.gold).setValue(1);
		ucc.getPoint().getCityLode(StockType.stone).setValue(1);
		ucc.getPoint().getCityLode(StockType.iron).setValue(1);
		ucc.getPoint().getCityLode(StockType.wood).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(11.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(12.0d);
		ucc.getMyFood().getFood(StockType.beef).setCount(12.0d);
		ucc.getMyFood().getFood(StockType.sul).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.wheat).setCount(0.0d);
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(1600.0d);
		ucc.setMaxLudzi(1600);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//ryz
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(205);//sul
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(200);//gold
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(201);//stona
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(214);//iron
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(206);//wood
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		System.out.print("prodkcja sul:" + config.getById(205).getOutputPerHour(ucc, StockType.sul));
		
		/*
		 * loop test - uncomment all if needed
		 */
		
//		long afterTimeMs = 0;
//		double afterTimeH = TimeTranslation.msToHours(afterTimeMs);
//		while(true)
//		{
//			Thread.sleep(1000);
//			
//			long start = System.currentTimeMillis();
//			UsersCityConfig uccAfter = ccu.getFoodAT(ucc, afterTimeMs*1000*1000, new Integer(0));
//			long stop = System.currentTimeMillis();
//			System.out.print("\nfood comp time: " + (stop - start) +" ms\n");
//			System.out.print("stan po godzinach: " + afterTimeH + "\n");
//			System.out.print("ilo�� ludzi: " + uccAfter.getMyResources().getResource(ResourceType.peopleCount).getCount() + "\n");
//			for(CityBuilding cb : uccAfter.getBuildings())
//			{
//				if(cb instanceof ExtractCityBuilding)
//				{
//					Boostable eb = config.getById(cb.getObjectId());
//					if(eb.getResourceType().isFood())
//					{
//						System.out.print(eb.getResourceType() + ":" + 
//						uccAfter.getMyFood().getFood(eb.getResourceType()).getCount() + 
//						", produkcja: " + eb.getOutputPerHour(uccAfter, eb.getResourceType()) + "\n");
//					}	
//				}
//			}
//			afterTimeH += 1.0d/3600.0d;
//		}
		
		
	}
	/*
	 * test ktory w petli co iles sekund wypisuje stan miasta
	 * uruchamiaj go tylko o monitorowania, on sie nigdy nie konczy.
	 */
	@Test
	public void loopTest2_getFoodAt() throws Exception{

		
ucc.getMyResources().getResource(StockType.peopleCount).setCount(100.0d);
		
		ucc.setMaxLudzi(100);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(34));
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		/*
		 * loop test - uncomment all if needed
		 */
		
		long afterTimeMs = 0;
		double afterTimeH = TimeTranslation.msToHours(afterTimeMs);
//		while(true)
//		{
//			Thread.sleep(1000);
//			
//			long start = System.currentTimeMillis();
//			UsersCityConfig uccAfter = ccu.getFoodAT(ucc, afterTimeMs*1000*1000, new Integer(0));
//			long stop = System.currentTimeMillis();
//			System.out.print("\nfood comp time: " + (stop - start) +" ms\n");
//			System.out.print("stan po godzinach: " + afterTimeH + "\n");
//			System.out.print("ilo�� ludzi: " + uccAfter.getMyResources().getResource(ResourceType.peopleCount).getCount() + "\n");
//			for(CityBuilding cb : uccAfter.getBuildings())
//			{
//				if(cb instanceof ExtractCityBuilding)
//				{
//					Boostable eb = config.getById(cb.getObjectId());
//					if(eb.getResourceType().isFood())
//					{
//						System.out.print(eb.getResourceType() + ":" + 
//						uccAfter.getMyFood().getFood(eb.getResourceType()).getCount() + 
//						", produkcja: " + eb.getOutputPerHour(uccAfter, eb.getResourceType()) + "\n");
//					}	
//				}
//			}
//			afterTimeH += 1.0d/3600.0d;
//		}
		
		
	}
	@Test
	public void test10_getFoodAt() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(1279.409d);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(200);//zloto
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(201);//kamien
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(214);//zelazo
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(206);//wood
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ucc.getMyFood().getFood(StockType.water).setCount(5.3316d);
		ucc.getMyFood().getFood(StockType.rice).setCount(4.8316d);
		ucc.getMyFood().getFood(StockType.pork).setCount(4.8316d);
		ucc.getMyFood().getFood(StockType.beef).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.sul).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.wheat).setCount(0.0d);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(5);
		ucc.getPoint().getCityLode(StockType.rice).setValue(5);
		ucc.getPoint().getCityLode(StockType.pork).setValue(5);
		
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.gold).setValue(1);
		ucc.getPoint().getCityLode(StockType.stone).setValue(1);
		ucc.getPoint().getCityLode(StockType.iron).setValue(1);
		ucc.getPoint().getCityLode(StockType.wood).setValue(1);
		
		ucc.setMaxLudzi(1600);
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeMs = 4500*60*60;
		double afterTimeH = TimeTranslation.msToHours(afterTimeMs);		
		Integer i = new Integer(0);
		long start = System.currentTimeMillis();
		afterTimeH = 0.12857;
		UsersCityConfig saveCopy = new UsersCityConfig(ucc);
		ccu.getFoodAT(ucc, afterTimeMs*1000*1000, i);
		afterTimeMs = 4400*60*60;
		afterTimeH = 0.12800;
//		UsersCityConfig uccAfter = ccu.getFoodAT(saveCopy, afterTimeH, i);
//		long stop = System.currentTimeMillis();
//		System.out.print("\nfood comp time: " + (stop - start) +" ms");
////		FoodSet fs = (FoodSet)ret.get("foodSet");
////		double people = (Double)ret.get("people");
//		//assertTrue(uccAfter.getMyResources().getResource(ResourceType.peopleCount).getCount() == 90.0d);
//		System.out.print("people first:" + uccAfter.getMyResources().getResource(ResourceType.peopleCount).getCount()+"\n");
//		System.out.print("people next:" + uccAfter2.getMyResources().getResource(ResourceType.peopleCount).getCount()+"\n");
//		assertTrue(uccAfter2.getMyFood().getFood(ResourceType.rice).getCount() > uccAfter.getMyFood().getFood(ResourceType.rice).getCount());
//		assertTrue(uccAfter2.getMyFood().getFood(ResourceType.water).getCount() > uccAfter.getMyFood().getFood(ResourceType.water).getCount());
//		assertTrue(uccAfter2.getMyFood().getFood(ResourceType.pork).getCount() > uccAfter.getMyFood().getFood(ResourceType.pork).getCount());
		
	}
	
	@Test
	public void test9_getFoodAt() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(345);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(34));
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.gold).setValue(1);
		ucc.getPoint().getCityLode(StockType.stone).setValue(1);
		ucc.getPoint().getCityLode(StockType.iron).setValue(1);
		ucc.getPoint().getCityLode(StockType.wood).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.beef).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.sul).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.wheat).setCount(0.0d);
		
		
		ucc.setMaxLudzi(2000);
		
		/*
		 * sprawdzamy jak dlugo sie bedzie upgradowaj budynek
		 */
		double hBuildTime = config.getById(203).getBuildTime(ucc);
		NotFoodSet_dead waterBuildingPrice = config.getById(203).getPrice(ucc);
		
		/*
		 * dodajemy surowce potrzebne na budowe dostawcy wody
		 */
		ucc.getMyResources().addNotFoodSet(waterBuildingPrice);
		/*
		 * upgradujemy budynek dostawca wody
		 */
		
		assertTrue(ucc.getBuilding(203).upgrade(null));
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		/*
		 * policzymy rekurencje dla miasta po czasie budowy budynku + 100 ns.
		 * Po tym czasie budynek powinien uz byc zupgradowany czyli miec level 2
		 */
		long afterTimeNs = TimeTranslation.hoursToNs(hBuildTime) + 100;
		double afterTimeH = TimeTranslation.nsToHours(afterTimeNs);
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeNs, new Integer(0));
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		
		/*
		 * sprawdzamy czy budynek po upgradzie ma level 2
		 */
		assertTrue(ucc.getLevel(203) == 2);
		/*
		 * sprawdzmy jeszcze , czy 50 ms przed zakonczeniem upgradu ma jeszcze 1 level
		 */
		//afterTimeMs = TimeTranslation.hoursToMs(hBuildTime) - 50;
		//uccAfter = ccu.getFoodAT(ucc, TimeTranslation.msToHours(afterTimeMs), new Integer(0));
		//assertTrue(uccAfter.getLevel(203) == 1);
		
	}
	@Test
	public void test8_getFoodAt() throws Exception{

		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(5);
		ucc.getPoint().getCityLode(StockType.rice).setValue(5);
		ucc.getPoint().getCityLode(StockType.pork).setValue(5);
		
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.gold).setValue(1);
		ucc.getPoint().getCityLode(StockType.stone).setValue(1);
		ucc.getPoint().getCityLode(StockType.iron).setValue(1);
		ucc.getPoint().getCityLode(StockType.wood).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.beef).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.sul).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.wheat).setCount(0.0d);
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(345);
		
		ucc.setMaxLudzi(2000);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(34));
		// 165 + 165 +170 = 500
		
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeMs = 100000*60*60;
		double afterTimeH = TimeTranslation.msToHours(afterTimeMs);
		Integer i = new Integer(0);
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeMs*1000l*1000l, i);
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		assertTrue(Math.abs(ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount() - 500.0d) < ucc.eps);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nastyk);
		
//		assertTrue(fs.getFood(ResourceType.rice).getStorage() == Storage.nastyk);
//		assertTrue(fs.getFood(ResourceType.water).getStorage() == Storage.nastyk);
//		assertTrue(fs.getFood(ResourceType.pork).getStorage() == Storage.nastyk);
	}
	
	@Test
	public void test7_getFoodAt() throws Exception{
		
		/*
		 * opis testu:
		 * produkowane jest w sumie 100 jedzenia / h
		 * poczatkowo w miescie jest 100 ludzi, , ka�dy zjada 1 /h , pasek jest ustawiony na 110.
		 * W mie�cie nie ma w ogole jedzenia. 
		 * Z powy�szych za�o�e� wynika �e ludzie nie powinni przychodzi� do miasta.
		 * Ich ilo�c powinna by� ca�y czas r�wna 100, bo tyle jest produkowane jedzenia,
		 * poza tym ludzie z budynk�w nie odchodza, a w budynkach jest w sumie 100 ludzi.
		 * po godzinie stan surowc�w powinien by� dalej r�wny zero, bo ludzie zjadaja wsyzstko co naproduje miasto,
		 * a ludzi powinno by� dalej 100
		 * 
		 */
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.gold).setValue(1);
		ucc.getPoint().getCityLode(StockType.stone).setValue(1);
		ucc.getPoint().getCityLode(StockType.iron).setValue(1);
		ucc.getPoint().getCityLode(StockType.wood).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.beef).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.sul).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.wheat).setCount(0.0d);
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(110);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(34));
		
		/*
		 * czas po 5-ciu minutach
		 */
		
		/*
		 * TODO doko�czy� test
		 */
		
		long afterTimeMs = 1000*60*60;
		double afterTimeH = TimeTranslation.msToHours(afterTimeMs);
		Integer i = new Integer(0);
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeMs*1000*1000, i);
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		assertTrue(ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount() == 100.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getVisibleAmount() == 0.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getVisibleAmount() == 0.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getVisibleAmount() == 0.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.beef).getVisibleAmount() == 0.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.sul).getVisibleAmount() == 0.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.wheat).getVisibleAmount() == 0.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nastyk);
	}
	
	
	@Test
	public void test6_getFoodAt() throws Exception{
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.beef).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.sul).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.wheat).setCount(0.0d);
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(300);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(5);
		ucc.getPoint().getCityLode(StockType.rice).setValue(5);
		ucc.getPoint().getCityLode(StockType.pork).setValue(5);
		
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		
		ucc.setMaxLudzi(800);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(50));
		
		
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeMs = 100000*60*60;
		double afterTimeH = TimeTranslation.msToHours(afterTimeMs);
		Integer i = new Integer(0);
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeMs*1000*1000, i);
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		
		assertTrue(Math.abs(ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount() - 750.0d) < ucc.eps);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nastyk);
	}
	@Test
	public void test5_getFoodAt() throws Exception{
		

		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(5.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(5.0d);
		ucc.getMyFood().getFood(StockType.beef).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.sul).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.wheat).setCount(0.0d);
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(90);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		
		ucc.setMaxLudzi(100);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(30));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(30));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(30));
		
		GregorianCalendar time = new GregorianCalendar();
		time.setTimeInMillis(System.currentTimeMillis());
		ucc.setNsTime_(System.currentTimeMillis()*1000*1000);
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeMs = 1000;
		double afterTimeH = TimeTranslation.msToHours(afterTimeMs);
		HashMap<String, Object> ret = new HashMap<String,Object>();
		HashMap<String, Storage> ret2 = new HashMap<String,Storage>();
		Integer i = new Integer(0);
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeMs*1000*1000, i);
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		CityStockSet fs = (CityStockSet)ret.get("foodSet");
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.tak);
	}
	@Test
	public void test4_getFoodAt() throws Exception{
		
		/*
		 * opis testu:
		 * W miescie jest :
		 * wody : 10
		 * ry�u : 5
		 * pork : 5
		 * W miescie jest 90 ludzi, pasek ludzi ustawiony jest na 100.
		 */
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(5.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(5.0d);
		ucc.getMyFood().getFood(StockType.beef).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.sul).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.wheat).setCount(0.0d);
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(90);
		ucc.setMaxLudzi(100);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(30));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(30));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(30));

		/*
		 * TODO doko�czy� test
		 */
		long afterTimeMs = 24*60*60*1000;
		double afterTimeH = TimeTranslation.msToHours(afterTimeMs);
		Integer i = new Integer(0);
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeMs*1000*1000,i);
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getVisibleAmount() == 0.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getVisibleAmount() == 0.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getVisibleAmount() == 0.0d);
	}
	@Test
	public void test3_getFoodAt() throws Exception{
		

		/*
		 * opis testu:
		 * w kraju mamy jedzenia:
		 * wody: 0
		 * ryz: 0
		 * pork: 1
		 * produkcje sa:
		 * woda :33
		 * ryz 33
		 * pork 34.
		 * Ludzi w miescie jest 100, pasek jest ustawiony na 100.
		 * Ludzie w sumie zjadaja 100 / h, czyli cale jedzenie jakie produkuje miasto.
		 * Woda i ryz nie wyrabiaja z produkkcja bo ludzie by chcieli jesc wszystkiego po rowno tzn po 33,(3).
		 * tak wiec sa on tylko niedoborywymi fabrykami: 100 - 33 -33 =34.
		 * Ludzie potrzebuja jeszcze 34 jedzenia i to zapewnia im pork factory.
		 * Znaczy to �e ca�e jedzenie w miescie jest zjadane a wspichlerzu zostaje to co zostaje czyli:
		 * pork : 1
		 * rice:0
		 * water :0
		 */
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0d);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(1.0d);
		ucc.getMyFood().getFood(StockType.beef).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.sul).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.wheat).setCount(0.0d);
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		
		ucc.setMaxLudzi(100);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//ryz
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(34));
		
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		/*
		 * po dziesieciu latach (!!!), sprawdzam po tak dlugim czasie po to �eby sprawdzic czy nie beda jakies b�edy
		 * zaokraglen przy mnozeniu tak duzych doubli,
		 * test przechodzi i wychodzi na to ze po 10 latach nadal bedziemy mieli ilosc pork rowne dokladnie 1.0d.
		 */
		long afterTimeMs = 60*60*1000*24*365*10; 
		double afterTimeH = TimeTranslation.msToHours(afterTimeMs);
		Integer i = new Integer(0);
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeMs*1000*1000, i);
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nie);
		
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getVisibleAmount() == 0.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getVisibleAmount() == 1.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getVisibleAmount() == 0.0d);
	}
	@Test
	public void test2_getChangeTime() throws Exception{
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(100);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(33));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(34));
		
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeNs = ucc.getNsTime_() + 5*60*1000*1000*1000;
		double afterTimeH = TimeTranslation.msToHours(afterTimeNs);
		Integer i = new Integer(0);
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		ucc.getSN_SOP2();
		double a = abcFactors.get("A");
		double b = abcFactors.get("B");
		double c = abcFactors.get("C");
		RecursionCalculator recCalc = new DefaultRecursionCdddalculator();
		Double changeTime = recCalc.getFoodChangeTime(ucc, StockType.pork,a,b,c,false);
		System.out.print("\npork change Time:" + changeTime );
	}
	@Test
	public void test55_getChangeTime() throws Exception{
		
		/*
		 * opis testu:
		 * 
		 * w kraju mamy:
		 * woda:  100
		 * rice:  100
		 * pork:  100
		 * sul :    0
		 * 
		 * sul jest 0 , ale fabryka sul produkuje 100/h
		 * Ludzi jest 100, pasek jest na 100, wiec ludzi bedzie caly czas 100.
		 * W kraju s� 3 rodzaje po�ywienia (woda,rice,pork) i produkuj�cy sie sul
		 * Ludzie by chcieli je�li po r�wno tych 4 rodzaj�w jedzenia, czyli po 25/h ka�de.
		 * Wode,rice i pork mog�, bo s� w spichlerzu, sul te� mog�, bo produkcja wynosi 100 czyli wiecej niz 25.
		 * po 48 min. czyli po 4/5 godziny, ludzie zjedz� po 20 ka�dego jedzenia.
		 * Czyli pork b�dzie: 100 - 20 : 80
		 * woda: 100 - 20 - 80(bo tyle sul zu�y� w ci�gu 48 minut) = 0
		 * rice: 100 - 20 - 80(bo tyle sul zu�y� w ci�gu 48 minut) = 0
		 * sul b�dzie 60, bo w ci�gu godziny naprodukuje sie 80, ale 20 zj�dz� ludzie wi�c 80 - 20 = 60.
		 * 
		 */
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(100.0d);
		CityFood ccf = ucc.getMyFood().getFood(StockType.water);
		ccf.setCount(100.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(100.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(100.0d);
		ucc.getMyFood().getFood(StockType.sul).setCount(0.0d);
		
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(100);
		
		ConvertCityBuilding ecb = (ConvertCityBuilding) ucc.getBuilding(205);// sul house
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
//		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
//		ecb.setLevel(1);
//		assertTrue(ecb.setPeople(33));
//		
//		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
//		ecb.setLevel(1);
//		assertTrue(ecb.setPeople(34));
		
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeNs = 48l*60l*1000l*1000l*1000l; //60 min
		double afterTimeH = TimeTranslation.msToHours(afterTimeNs);
		Integer i = new Integer(0);
		ccu.getFoodAT(ucc, afterTimeNs, i);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getVisibleAmount() == 0.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getVisibleAmount() == 0.0d);
		System.out.print("sul: " + ucc.getMyFood().getFood(StockType.sul).getVisibleAmount());
		assertTrue(ucc.getMyFood().getFood(StockType.sul).getVisibleAmount() == 60.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getVisibleAmount() == 80.0d);
		//System.out.print("\npork change Time:" + changeTime );
	}
	
	@Test
	public void test56_getChangeTime() throws Exception{
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.beef).setCount(11.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(12.0d);
		
		NotFoodSet_dead resources = ucc.getMyResources();
		resources.getResource(StockType.peopleCount).setCount(1000);
		resources.getResource(StockType.gold).setCount(10000);
		resources.getResource(StockType.iron).setCount(10000);
		resources.getResource(StockType.wood).setCount(10000);
		ucc.setMaxLudzi(1000);
		
		ucc.getBuilding(5).setLevel(1);
		
		ExtractCityBuilding b = (ExtractCityBuilding)ucc.getBuilding(203);//woda
		b.setLevel(1);
		b.setPeople(100);
//		
		b = (ExtractCityBuilding)ucc.getBuilding(205);//sul
		b.setLevel(1);
		b.setPeople(0);
		
		b = (ExtractCityBuilding)ucc.getBuilding(207);//swinie
		b.setLevel(1);
		b.setPeople(100);
		
		b = (ExtractCityBuilding)ucc.getBuilding(200);//zloto
		b.setLevel(1);
		b.setPeople(100);
		
		b = (ExtractCityBuilding)ucc.getBuilding(201);//kamien
		b.setLevel(1);
		b.setPeople(100);
		
		b = (ExtractCityBuilding)ucc.getBuilding(214);//zelazo
		b.setLevel(1);
		b.setPeople(100);
		
		b = (ExtractCityBuilding)ucc.getBuilding(206);//wood
		b.setLevel(1);
		b.setPeople(100);
		
		long afterTimeNs = 60l*60l*1000l*1000l*1000l; //60 min
		
		ccu.getFoodAT(ucc, afterTimeNs, new Integer(0));
		//assertTrue(uccAfter.getMyFood().getFood(ResourceType.water).getCount() == 0.0d);
		//assertTrue(uccAfter.getMyFood().getFood(ResourceType.rice).getCount() == 0.0d);
		System.out.print("HB water: " + ucc.getMyFood().getFood(StockType.water).getVisibleAmount() + "\n");
		System.out.print("HB rice: " + ucc.getMyFood().getFood(StockType.rice).getVisibleAmount()+ "\n");
		System.out.print("HB sul: " + ucc.getMyFood().getFood(StockType.sul).getVisibleAmount()+ "\n");
		System.out.print("HB pork: " + ucc.getMyFood().getFood(StockType.pork).getVisibleAmount()+ "\n");
		//assertTrue(uccAfter.getMyFood().getFood(ResourceType.sul).getCount() == 60.0d);
		//assertTrue(uccAfter.getMyFood().getFood(ResourceType.pork).getCount() == 80.0d);
		
		
	}
}
