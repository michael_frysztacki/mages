package junittest.old_tests;

import static org.junit.Assert.assertTrue;

import game_model_impl.Boostable;
import game_model_impl.StockType;
import helpers.TimeTranslation;

import java.lang.reflect.InvocationTargetException;



import org.junit.Before;
import org.junit.Test;


import com.jsfsample.managedbeans.SpringJSFUtil;

import dto.TimeDTO;

import entities.Civil;
import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.UnitBuild;


public class TimeDTOtest {

	@Test
	public void test1() throws Exception
	{
		TimeDTO time = new TimeDTO(5000l);
		assertTrue(time.getSeconds()==5);
		assertTrue(time.getMinutes()==0);
		assertTrue(time.getHours()==0);	
	}
	@Test
	public void test2() throws Exception
	{
		TimeDTO time = new TimeDTO(1000l*60 + 20000);
		assertTrue(time.getSeconds()==20);
		assertTrue(time.getMinutes()==1);
		assertTrue(time.getHours()==0);		
	}
	@Test
	public void test3() throws Exception
	{
		TimeDTO time = new TimeDTO(1000l*60*60 + 80000);
		assertTrue(time.getSeconds()==20);
		assertTrue(time.getMinutes()==1);
		assertTrue(time.getHours()==1);		
	}
}
