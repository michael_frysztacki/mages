package junittest.old_tests;

import java.lang.reflect.InvocationTargetException;



import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.aop.TargetClassAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import com.jsfsample.managedbeans.SpringJSFUtil;

import economycomputation.CityCompUtil;
import entities.Civil;
import entities.Mission;
import entities.Point;
import entities.RobberyTrip;
import entities.Trip;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.stocks.CityStockSet;
import entities.stocks.NotFoodSet_dead;
import entities.stocks.TripStock;
import entities.units.CityArmy;
import entities.units.CityUnitGroup;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import game_utilities.TripUtil;
import helpers.TimeTranslation;

import services.CityService;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext.xml")
public class TripTest {

	public static UsersCityConfig ucc;
	public static UsersCityConfig target;
	@Autowired CityService cityService;
	@Autowired CityCompUtil ccu;
	@Autowired TripUtil tripUtil;
	@Autowired WorldDefinition ch;
	@Before
	public void setUp() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	{
		Testing.initTools();
		UserConfig friko = new UserConfig();
		friko.setCivil(Civil.korea);
		UserConfig galan = new UserConfig();
		galan.setCivil(Civil.korea);
		Point p1 = new Point();
		Point p2 = new Point();
		long nsNow = (long)System.currentTimeMillis()*1000l*1000l;
		ucc = new UsersCityConfig(friko, true, p1, nsNow, ch);
		target = new UsersCityConfig(galan, true, p2, nsNow, ch);
	}
	@Test
	public void test4()
	{
	
		boolean isDebug = java.lang.management.ManagementFactory.getRuntimeMXBean().
		    getInputArguments().toString().indexOf("-agentlib:jdwp") > 0;
		    System.out.print("is debug:" + isDebug);
	}
	@Test
	public void test3() throws Exception
	{
		/*
		 * ustawiamy startowe jedzenie w miescie z ktorego wyrusza wojsko
		 */
		
		double te = 15768000000.124324324234d;
		System.out.print("\n"+te+"\n");
		ucc.getMyFood().getFood(StockType.beef).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(15.0d);
		ucc.getMyFood().getFood(StockType.water).setCount(20.0d);
		ucc.getMyFood().getFood(StockType.fish).setCount(20.0d);
		/*
		 * ustawiamy surowce w miescie docelowym po to zeby je zrabowac
		 */
		target.getMyFood().getFood(StockType.beef).setCount(10.0d);
		target.getMyFood().getFood(StockType.rice).setCount(10.0d);
		target.getMyFood().getFood(StockType.water).setCount(10.0d);
		target.getMyFood().getFood(StockType.fish).setCount(10.0d);
		
		target.getMyResources().getResource(StockType.wood).setCount(10.0d);
		target.getMyResources().getResource(StockType.iron).setCount(10.0d);
		target.getMyResources().getResource(StockType.gold).setCount(10.0d);
		target.getMyResources().getResource(StockType.stone).setCount(10.0d);
		/*
		 * ustawiamy wojsko w miescie startujacym
		 */
		ucc.getArmy().getUnitGroupById(10).setCount(20);//w�ocznik
		ucc.getArmy().getUnitGroupById(11).setCount(20);//strzelec
		ucc.getArmy().getUnitGroupById(12).setCount(25);//�o�nierz
		
		/*
		 * ustawiamy wojsko w miescie atakowanym
		 */
	//	target.getArmy().getUnitGroupById(10).setCount(8);//w�ocznik
	//	target.getArmy().getUnitGroupById(11).setCount(8);//strzelec
	//	target.getArmy().getUnitGroupById(12).setCount(8);//�o�nierz
		
		/*
		 * tworzymy wojsko, ktore chcemy wyslac na wyprawe.
		 * wysy�amy ca�e wojsko
		 */
		CityArmy army = new CityArmy();
		army.setUserConfig(ucc.getUserConfig());
		army.addUnitGroup(new CityUnitGroup(10, 20, army));
		army.addUnitGroup(new CityUnitGroup(11, 20, army));
		army.addUnitGroup(new CityUnitGroup(12, 25, army));
		/*
		 * sprawdzimy ile to wojsko ktore chcemy wysla�
		 * bedzie zjada� jedzenia na godzin�
		 */
		double eatPerHour = army.getEatPerHour();
		/*
		 * time - czas w godzinach, na jak dlugo wystarczy nam cale jedzenie dostepne w miescie dla armi
		 * sprawdzamy na ile godzin wystarczy nam jedzenia ktore mamy w miescie dla armi
		 */
		double time = ucc.getMyFood().getSumOfFood() / eatPerHour;
		
		/*
		 * sprawdamy jakies jedzenie dostepne w miescie bedzie potrzebowac nasza wyslana armia.
		 * powinno zwrocic to samo co jest w miescie , dlatego ze na czas 'time'
		 * wojsko bedzie potrzebowalo calego jedzenia.
		 */
		CityStockSet fs = tripUtil.getNeededFoodForTrip(army, ucc.getMyFood(), (int)TimeTranslation.hoursToMs(time));
		
		assertTrue(fs.getFood(StockType.beef).getVisibleAmount() == 10.0d);
		assertTrue(fs.getFood(StockType.rice).getVisibleAmount() == 15.0d);
		assertTrue(fs.getFood(StockType.water).getVisibleAmount() == 20.0d);
		assertTrue(fs.getFood(StockType.fish).getVisibleAmount() == 20.0d);
		
		UsersCityConfig aggCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(this.ucc.getUserConfig().getCivil());
		UsersCityConfig defCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(this.target.getUserConfig().getCivil());
		
		NotFoodSet_dead tripRes = new NotFoodSet_dead();
		CityStockSet tripFood = new CityStockSet();
		
		/*
		 * wysy�amy armie na czas t = t/3, a to znaczy ze nie zabiora calego jedzenia z miasta.		
		 */
		time /= 3.0d;
		long msTravelTime = TimeTranslation.hoursToMs(time);
		tripUtil.sendTrip(tripRes, tripFood, army, ucc, target, ucc.getNsTime_(), Mission.robbery, msTravelTime/2);
		
		/*
		 * polczmy teraz stan miasta atakujacego zaraz po rozegraniu bitwy
		 * powinien on meiec nowy raport z bitwy
		 */
		
		/*
		 * policzymy teraz stan miasta, ktore wys�alo wojsko
		 * po powrocie wojska miasto powinno miec dodatkowe surowce zdybte podczas wyprawy.
		 * Outcome trip powinien zosta� usuniety, i powinna byc nowy raport w tablicy userConfig.messages.
		 */
		
		//UsersCityConfig uccSaveCopy = new UsersCityConfig(ucc);
		//UsersCityConfig targetSaveCopy = new UsersCityConfig(target);
		
		CityStockSet uccFoodSet = new CityStockSet(ucc.getMyFood());
		NotFoodSet_dead uccResources = new NotFoodSet_dead(ucc.getMyResources());
		
		CityStockSet targetFoodSet = new CityStockSet(target.getMyFood());
		NotFoodSet_dead targetResources = new NotFoodSet_dead(target.getMyResources());
		
		long nsTravelTime = (long)msTravelTime * 1000l * 1000l;
		ccu.getFoodAT(ucc, nsTravelTime  , new Integer(0));
		
		/*
		 * trip powinien zostac usuniety, dlatego ze wojsko juz wrocilo.
		 */
		
		assertTrue(ucc.getOutcomeTrips().size() == 0);
		assertTrue(ucc.getUserConfig().getMessages().size() == 1);
		
		System.out.print("a:" + ucc.getMyFood().getFood(StockType.beef).getVisibleAmount()+"\n");
		System.out.print("b:" + uccFoodSet.getFood(StockType.beef).getVisibleAmount()+"\n");
		System.out.print("c:" + targetFoodSet.getFood(StockType.beef).getVisibleAmount() * defCivil.getPercentRobberySteal()+"\n");
		
		assertTrue(ucc.getMyFood().getFood(StockType.beef).getVisibleAmount() ==
				uccFoodSet.getFood(StockType.beef).getVisibleAmount() +
				targetFoodSet.getFood(StockType.beef).getVisibleAmount() * defCivil.getPercentRobberySteal());
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getVisibleAmount() ==
				uccFoodSet.getFood(StockType.rice).getVisibleAmount() +
				targetFoodSet.getFood(StockType.rice).getVisibleAmount() * defCivil.getPercentRobberySteal());
		assertTrue(ucc.getMyFood().getFood(StockType.water).getVisibleAmount() ==
				uccFoodSet.getFood(StockType.water).getVisibleAmount() +
				targetFoodSet.getFood(StockType.water).getVisibleAmount() * defCivil.getPercentRobberySteal());
		assertTrue(ucc.getMyFood().getFood(StockType.fish).getVisibleAmount() ==
				uccFoodSet.getFood(StockType.fish).getVisibleAmount() +
				targetFoodSet.getFood(StockType.fish).getVisibleAmount() * defCivil.getPercentRobberySteal());
		
		assertTrue(ucc.getMyResources().getResource(StockType.wood).getVisibleAmount() ==
				uccResources.getResource(StockType.wood).getVisibleAmount() +
				targetResources.getResource(StockType.wood).getVisibleAmount() * defCivil.getPercentRobberySteal());
		assertTrue(ucc.getMyResources().getResource(StockType.iron).getVisibleAmount() ==
				uccResources.getResource(StockType.iron).getVisibleAmount() +
				targetResources.getResource(StockType.iron).getVisibleAmount() * defCivil.getPercentRobberySteal());
		assertTrue(ucc.getMyResources().getResource(StockType.gold).getVisibleAmount() ==
				uccResources.getResource(StockType.gold).getVisibleAmount() +
				targetResources.getResource(StockType.gold).getVisibleAmount() * defCivil.getPercentRobberySteal());
		assertTrue(ucc.getMyResources().getResource(StockType.stone).getVisibleAmount() ==
				uccResources.getResource(StockType.stone).getVisibleAmount() +
				targetResources.getResource(StockType.stone).getVisibleAmount() * defCivil.getPercentRobberySteal());

	}
	/*
	 * testowanie wyprawy
	 */
	@Test
	public void test6() throws Exception
	{
		/*
		 * ustawiamy startowe jedzenie w miescie z ktorego wyrusza wojsko
		 */
		ucc.getMyFood().getFood(StockType.beef).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(15.0d);
		ucc.getMyFood().getFood(StockType.water).setCount(20.0d);
		ucc.getMyFood().getFood(StockType.fish).setCount(20.0d);
		/*
		 * ustawiamy surowce w miescie docelowym po to zeby je zrabowac
		 */
		target.getMyFood().getFood(StockType.beef).setCount(10.0d);
		target.getMyFood().getFood(StockType.rice).setCount(10.0d);
		target.getMyFood().getFood(StockType.water).setCount(10.0d);
		target.getMyFood().getFood(StockType.fish).setCount(10.0d);
		
		target.getMyResources().getResource(StockType.wood).setCount(10.0d);
		target.getMyResources().getResource(StockType.iron).setCount(10.0d);
		target.getMyResources().getResource(StockType.gold).setCount(10.0d);
		target.getMyResources().getResource(StockType.stone).setCount(10.0d);
		/*
		 * ustawiamy wojsko w miescie startujacym
		 */
		ucc.getArmy().getUnitGroupById(10).setCount(20);//w�ocznik
		ucc.getArmy().getUnitGroupById(11).setCount(20);//strzelec
		ucc.getArmy().getUnitGroupById(12).setCount(25);//�o�nierz
		
		/*
		 * ustawiamy wojsko w miescie atakowanym
		 */
	//	target.getArmy().getUnitGroupById(10).setCount(8);//w�ocznik
	//	target.getArmy().getUnitGroupById(11).setCount(8);//strzelec
	//	target.getArmy().getUnitGroupById(12).setCount(8);//�o�nierz
		
		/*
		 * tworzymy wojsko, ktore chcemy wyslac na wyprawe.
		 * wysy�amy ca�e wojsko
		 */
		CityArmy army = new CityArmy();
		army.setUserConfig(ucc.getUserConfig());
		army.addUnitGroup(new CityUnitGroup(10, 20, army));
		army.addUnitGroup(new CityUnitGroup(11, 20, army));
		army.addUnitGroup(new CityUnitGroup(12, 25, army));
		/*
		 * sprawdzimy ile to wojsko ktore chcemy wysla�
		 * bedzie zjada� jedzenia na godzin�
		 */
		double eatPerHour = army.getEatPerHour();
		/*
		 * time - czas w godzinach, na jak dlugo wystarczy nam cale jedzenie dostepne w miescie dla armi
		 * sprawdzamy na ile godzin wystarczy nam jedzenia ktore mamy w miescie dla armi
		 */
		double time = ucc.getMyFood().getSumOfFood() / eatPerHour;
		
		/*
		 * sprawdamy jakies jedzenie dostepne w miescie bedzie potrzebowac nasza wyslana armia.
		 * powinno zwrocic to samo co jest w miescie , dlatego ze na czas 'time'
		 * wojsko bedzie potrzebowalo calego jedzenia.
		 */
		CityStockSet fs = tripUtil.getNeededFoodForTrip(army, ucc.getMyFood(), (long)TimeTranslation.hoursToMs(time));
		
		assertTrue(fs.getFood(StockType.beef).getVisibleAmount() == 10.0d);
		assertTrue(fs.getFood(StockType.rice).getVisibleAmount() == 15.0d);
		assertTrue(fs.getFood(StockType.water).getVisibleAmount() == 20.0d);
		assertTrue(fs.getFood(StockType.fish).getVisibleAmount() == 20.0d);
		
		UsersCityConfig aggCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(this.ucc.getUserConfig().getCivil());
		UsersCityConfig defCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(this.target.getUserConfig().getCivil());
		
		NotFoodSet_dead tripRes = new NotFoodSet_dead();
		CityStockSet tripFood = new CityStockSet();
		
		/*
		 * wysy�amy armie na czas t = t/3, a to znaczy ze nie zabiora calego jedzenia z miasta.		
		 */
		time /= 3.0d;
		long msTravelTime = (long)TimeTranslation.hoursToMs(time);
		tripUtil.sendTrip(tripRes, tripFood, army, ucc, target, ucc.getNsTime_(), Mission.robbery, msTravelTime/2);

		/*
		 * policzymy teraz stan miasta wysylajacego wojsko 10 sek przed atakiem.
		 * Powinno nie byc zanego raportu
		 */
		//UsersCityConfig uccSaveCopy = new UsersCityConfig(ucc);
		
		long nsTravelTime = (long)msTravelTime * 1000l * 1000l;
		ccu.getFoodAT(ucc, nsTravelTime/2 - 10 , new Integer(0));
		RobberyTrip trip2 = (RobberyTrip)ucc.getOutcomeTrips().iterator().next();
		assertTrue(trip2.getfReport() == null);
		
	}
	/*
	 * testowanie wyprawy
	 */
	@Test
	public void test5() throws Exception
	{
		/*
		 * ustawiamy startowe jedzenie w miescie z ktorego wyrusza wojsko
		 */
		ucc.getMyFood().getFood(StockType.beef).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(15.0d);
		ucc.getMyFood().getFood(StockType.water).setCount(20.0d);
		ucc.getMyFood().getFood(StockType.fish).setCount(20.0d);
		/*
		 * ustawiamy surowce w miescie docelowym po to zeby je zrabowac
		 */
		target.getMyFood().getFood(StockType.beef).setCount(10.0d);
		target.getMyFood().getFood(StockType.rice).setCount(10.0d);
		target.getMyFood().getFood(StockType.water).setCount(10.0d);
		target.getMyFood().getFood(StockType.fish).setCount(10.0d);
		
		target.getMyResources().getResource(StockType.wood).setCount(10.0d);
		target.getMyResources().getResource(StockType.iron).setCount(10.0d);
		target.getMyResources().getResource(StockType.gold).setCount(10.0d);
		target.getMyResources().getResource(StockType.stone).setCount(10.0d);
		/*
		 * ustawiamy wojsko w miescie startujacym
		 */
		ucc.getArmy().getUnitGroupById(10).setCount(20);//w�ocznik
		ucc.getArmy().getUnitGroupById(11).setCount(20);//strzelec
		ucc.getArmy().getUnitGroupById(12).setCount(25);//�o�nierz
		
		/*
		 * ustawiamy wojsko w miescie atakowanym
		 */
	//	target.getArmy().getUnitGroupById(10).setCount(8);//w�ocznik
	//	target.getArmy().getUnitGroupById(11).setCount(8);//strzelec
	//	target.getArmy().getUnitGroupById(12).setCount(8);//�o�nierz
		
		/*
		 * tworzymy wojsko, ktore chcemy wyslac na wyprawe.
		 * wysy�amy ca�e wojsko
		 */
		CityArmy army = new CityArmy();
		army.setUserConfig(ucc.getUserConfig());
		army.addUnitGroup(new CityUnitGroup(10, 20, army));
		army.addUnitGroup(new CityUnitGroup(11, 20, army));
		army.addUnitGroup(new CityUnitGroup(12, 25, army));
		/*
		 * sprawdzimy ile to wojsko ktore chcemy wysla�
		 * bedzie zjada� jedzenia na godzin�
		 */
		double eatPerHour = army.getEatPerHour();
		/*
		 * time - czas w godzinach, na jak dlugo wystarczy nam cale jedzenie dostepne w miescie dla armi
		 * sprawdzamy na ile godzin wystarczy nam jedzenia ktore mamy w miescie dla armi
		 */
		double time = ucc.getMyFood().getSumOfFood() / eatPerHour;
		
		/*
		 * sprawdamy jakies jedzenie dostepne w miescie bedzie potrzebowac nasza wyslana armia.
		 * powinno zwrocic to samo co jest w miescie , dlatego ze na czas 'time'
		 * wojsko bedzie potrzebowalo calego jedzenia.
		 */
		long msTravelTime = (long)TimeTranslation.hoursToMs(time);
		
		CityStockSet fs = tripUtil.getNeededFoodForTrip(army, ucc.getMyFood(), msTravelTime);
		/*
		 * policzymy teraz stan miasta wysylajacego wojsko 10 sek po ataku.
		 * Powinien miec wygenerowany dodatkowy raport z tripu.
		 */

		assertTrue(fs.getFood(StockType.beef).getVisibleAmount() == 10.0d);
		assertTrue(fs.getFood(StockType.rice).getVisibleAmount() == 15.0d);
		assertTrue(fs.getFood(StockType.water).getVisibleAmount() == 20.0d);
		assertTrue(fs.getFood(StockType.fish).getVisibleAmount() == 20.0d);
		
		UsersCityConfig aggCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(this.ucc.getUserConfig().getCivil());
		UsersCityConfig defCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(this.target.getUserConfig().getCivil());
		
		NotFoodSet_dead tripRes = new NotFoodSet_dead();
		CityStockSet tripFood = new CityStockSet();
		
		/*
		 * wysy�amy armie na czas t = t/3, a to znaczy ze nie zabiora calego jedzenia z miasta.		
		 */
		time /= 3.0d;
		msTravelTime = (long)TimeTranslation.hoursToMs(time);
		long nsTravelTime = (long)msTravelTime * 1000l * 1000l;
		tripUtil.sendTrip(tripRes, tripFood, army, ucc, target, ucc.getNsTime_(), Mission.robbery, msTravelTime/2);
		
		UsersCityConfig uccSaveCopy = new UsersCityConfig(ucc);
		ccu.getFoodAT(ucc, nsTravelTime/2 + 10, new Integer(0));
		RobberyTrip trip = (RobberyTrip)ucc.getOutcomeTrips().iterator().next();
		assertTrue(trip.getfReport() != null);
		

		
	}
	/*
	 * testowanie wyprawy
	 */
	@Test
	public void test1() throws Exception
	{
		/*
		 * ustawiamy startowe jedzenie w miescie z ktorego wyrusza wojsko
		 */
		ucc.getMyFood().getFood(StockType.beef).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(15.0d);
		ucc.getMyFood().getFood(StockType.water).setCount(20.0d);
		ucc.getMyFood().getFood(StockType.fish).setCount(20.0d);
		/*
		 * ustawiamy surowce w miescie docelowym po to zeby je zrabowac
		 */
		target.getMyFood().getFood(StockType.beef).setCount(10.0d);
		target.getMyFood().getFood(StockType.rice).setCount(10.0d);
		target.getMyFood().getFood(StockType.water).setCount(10.0d);
		target.getMyFood().getFood(StockType.fish).setCount(10.0d);
		
		target.getMyResources().getResource(StockType.wood).setCount(10.0d);
		target.getMyResources().getResource(StockType.iron).setCount(10.0d);
		target.getMyResources().getResource(StockType.gold).setCount(10.0d);
		target.getMyResources().getResource(StockType.stone).setCount(10.0d);
		/*
		 * ustawiamy wojsko w miescie startujacym
		 */
		ucc.getArmy().getUnitGroupById(10).setCount(20);//w�ocznik
		ucc.getArmy().getUnitGroupById(11).setCount(20);//strzelec
		ucc.getArmy().getUnitGroupById(12).setCount(25);//�o�nierz
		
		/*
		 * ustawiamy wojsko w miescie atakowanym
		 */
	//	target.getArmy().getUnitGroupById(10).setCount(8);//w�ocznik
	//	target.getArmy().getUnitGroupById(11).setCount(8);//strzelec
	//	target.getArmy().getUnitGroupById(12).setCount(8);//�o�nierz
		
		/*
		 * tworzymy wojsko, ktore chcemy wyslac na wyprawe.
		 * wysy�amy ca�e wojsko
		 */
		CityArmy army = new CityArmy();
		army.setUserConfig(ucc.getUserConfig());
		army.addUnitGroup(new CityUnitGroup(10, 20, army));
		army.addUnitGroup(new CityUnitGroup(11, 20, army));
		army.addUnitGroup(new CityUnitGroup(12, 25, army));
		/*
		 * sprawdzimy ile to wojsko ktore chcemy wysla�
		 * bedzie zjada� jedzenia na godzin�
		 */
		double eatPerHour = army.getEatPerHour();
		/*
		 * time - czas w godzinach, na jak dlugo wystarczy nam cale jedzenie dostepne w miescie dla armi
		 * sprawdzamy na ile godzin wystarczy nam jedzenia ktore mamy w miescie dla armi
		 */
		double time = ucc.getMyFood().getSumOfFood() / eatPerHour;
		
		/*
		 * sprawdamy jakies jedzenie dostepne w miescie bedzie potrzebowac nasza wyslana armia.
		 * powinno zwrocic to samo co jest w miescie , dlatego ze na czas 'time'
		 * wojsko bedzie potrzebowalo calego jedzenia.
		 */
		long msTravelTime = TimeTranslation.hoursToMs(time);
		
		CityStockSet fs = tripUtil.getNeededFoodForTrip(army, ucc.getMyFood(), msTravelTime );
		
		assertTrue(fs.getFood(StockType.beef).getVisibleAmount() == 10.0d);
		assertTrue(fs.getFood(StockType.rice).getVisibleAmount() == 15.0d);
		assertTrue(fs.getFood(StockType.water).getVisibleAmount() == 20.0d);
		assertTrue(fs.getFood(StockType.fish).getVisibleAmount() == 20.0d);
		
		UsersCityConfig aggCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(this.ucc.getUserConfig().getCivil());
		UsersCityConfig defCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(this.target.getUserConfig().getCivil());
		NotFoodSet_dead tripRes = new NotFoodSet_dead();
		CityStockSet tripFood = new CityStockSet();
		
		/*
		 * wysy�amy armie na czas t = t/3, a to znaczy ze nie zabiora calego jedzenia z miasta.		
		 */
		time /= 3.0d;
		msTravelTime = TimeTranslation.hoursToMs(time);
		
		tripUtil.sendTrip(tripRes, tripFood, army, ucc, target, ucc.getNsTime_(), Mission.robbery, msTravelTime/2);
		/*
		 * sprawdzamy czy po wyslaniu wojska stan miasta sie zgadza, tzn. wojsko zabralo potrzebne jedzenie.
		 * czy w miescie nie ma zadnej armi
		 */
		assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 0);
		assertTrue(ucc.getArmy().getUnitGroupById(11).getVisibleAmount() == 0);
		assertTrue(ucc.getArmy().getUnitGroupById(12).getVisibleAmount() == 0);
		
		/*
		 * policzymy teraz stan miasta atakowanego zaraz
		 * po przeprowadzonym ataku. Miasta atakujace powinno przegrac i odda� pewien procent swoich surowc�w
		 * armi atakujacej. Wojsko atakujace ma wystarczaja ilosc miejsca (350) zeby zrabowac mozliwie najwiecej surowcow.
		 * Pasek ludzi w miescie rabowanym jest ustawiony na 0 wiec nie ma ludzi w ogole, a to znaczy ze nic nie zostanie 
		 * zjedzone w tym miescie.
		 * Trip powinien rowniez zostac usuniety.
		 */
		
		//UsersCityConfig targetS = new UsersCityConfig(target);
		double targetStartBeefCount = target.getMyFood().getFood(StockType.beef).getVisibleAmount();
		double targetStartRiceCount = target.getMyFood().getFood(StockType.rice).getVisibleAmount();
		double targetStartWaterCount = target.getMyFood().getFood(StockType.water).getVisibleAmount();
		double targetStartFishCount = target.getMyFood().getFood(StockType.fish).getVisibleAmount();
		
		double targetStartWoodCount = target.getMyResources().getResource(StockType.wood).getVisibleAmount();
		double targetStartIronCount = target.getMyResources().getResource(StockType.iron).getVisibleAmount();
		double targetStartGoldCount = target.getMyResources().getResource(StockType.gold).getVisibleAmount();
		double targetStartStoneCount = target.getMyResources().getResource(StockType.stone).getVisibleAmount();
		//UsersCityConfig uccSaveCopy = new UsersCityConfig(ucc);
		
		long nsTravelTime = (long)msTravelTime * 1000l * 1000l;
		ccu.getFoodAT(target, nsTravelTime/2 + 10 , new Integer(0));
		
		/*
		 * sprawdzamy czy trip zostal usuniety
		 */
		assertTrue(target.getIncomeTrips().size() == 0);
		System.out.print("\na:" + target.getMyFood().getFood(StockType.beef).getVisibleAmount());
		System.out.print("\nb:"+ targetStartBeefCount * (1.0d - defCivil.getPercentRobberySteal()));
		assertTrue(target.getMyFood().getFood(StockType.beef).getVisibleAmount() == 
			targetStartBeefCount * (1.0d - defCivil.getPercentRobberySteal()));
		assertTrue(target.getMyFood().getFood(StockType.rice).getVisibleAmount() == 
				targetStartRiceCount * (1.0d - defCivil.getPercentRobberySteal()));
		assertTrue(target.getMyFood().getFood(StockType.water).getVisibleAmount() == 
				targetStartWaterCount * (1.0d - defCivil.getPercentRobberySteal()));
		assertTrue(target.getMyFood().getFood(StockType.fish).getVisibleAmount() == 
				targetStartFishCount * (1.0d - defCivil.getPercentRobberySteal()));
		
		assertTrue(target.getMyResources().getResource(StockType.wood).getVisibleAmount() == 
				targetStartWoodCount * (1.0d - defCivil.getPercentRobberySteal()));
		assertTrue(target.getMyResources().getResource(StockType.iron).getVisibleAmount() == 
				targetStartIronCount * (1.0d - defCivil.getPercentRobberySteal()));
		assertTrue(target.getMyResources().getResource(StockType.gold).getVisibleAmount() == 
				targetStartGoldCount * (1.0d - defCivil.getPercentRobberySteal()));
		assertTrue(target.getMyResources().getResource(StockType.stone).getVisibleAmount() == 
				targetStartStoneCount * (1.0d - defCivil.getPercentRobberySteal()));
		

		
		
		
		
		
		
	}
	
	/*
	 * testowanie wyprawy
	 * TODO - test2 jest taki sam jak test1
	 * zmien tu cos, potestuj cos ciekawego
	 */
	@Test
	public void test2() throws Exception
	{
		/*
		 * ustawiamy startowe jedzenie w miescie z ktorego wyrusza wojsko
		 */
		ucc.getMyFood().getFood(StockType.beef).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(15.0d);
		ucc.getMyFood().getFood(StockType.water).setCount(20.0d);
		ucc.getMyFood().getFood(StockType.fish).setCount(20.0d);
		/*
		 * ustawiamy surowce w miescie docelowym po to zeby je zrabowac
		 */
		target.getMyFood().getFood(StockType.beef).setCount(10.0d);
		target.getMyFood().getFood(StockType.rice).setCount(10.0d);
		target.getMyFood().getFood(StockType.water).setCount(10.0d);
		target.getMyFood().getFood(StockType.fish).setCount(10.0d);
		
		target.getMyResources().getResource(StockType.wood).setCount(10.0d);
		target.getMyResources().getResource(StockType.iron).setCount(10.0d);
		target.getMyResources().getResource(StockType.gold).setCount(10.0d);
		target.getMyResources().getResource(StockType.stone).setCount(10.0d);
		/*
		 * ustawiamy wojsko w miescie startujacym
		 */
		ucc.getArmy().getUnitGroupById(10).setCount(20);//w�ocznik
		ucc.getArmy().getUnitGroupById(11).setCount(20);//strzelec
		ucc.getArmy().getUnitGroupById(12).setCount(25);//�o�nierz
		
		/*
		 * ustawiamy wojsko w miescie atakowanym
		 */
		//target.getArmy().getUnitGroupById(10).setCount(8);//w�ocznik
		//target.getArmy().getUnitGroupById(11).setCount(8);//strzelec
		//target.getArmy().getUnitGroupById(12).setCount(8);//�o�nierz
		
		/*
		 * tworzymy wojsko, ktore chcemy wyslac na wyprawe.
		 * wysy�amy ca�e wojsko
		 */
		CityArmy army = new CityArmy();
		army.setUserConfig(ucc.getUserConfig());
		army.addUnitGroup(new CityUnitGroup(10, 20, army));
		army.addUnitGroup(new CityUnitGroup(11, 20, army));
		army.addUnitGroup(new CityUnitGroup(12, 25, army));
		/*
		 * sprawdzimy ile to wojsko ktore chcemy wysla�
		 * bedzie zjada� jedzenia na godzin�
		 */
		double eatPerHour = army.getEatPerHour();
		/*
		 * time - czas w godzinach, na jak dlugo wystarczy nam cale jedzenie dostepne w miescie dla armi
		 * sprawdzamy na ile godzin wystarczy nam jedzenia ktore mamy w miescie dla armi
		 */
		double time = ucc.getMyFood().getSumOfFood() / eatPerHour;
		
		/*
		 * sprawdamy jakies jedzenie dostepne w miescie bedzie potrzebowac nasza wyslana armia.
		 * powinno zwrocic to samo co jest w miescie , dlatego ze na czas 'time'
		 * wojsko bedzie potrzebowalo calego jedzenia.
		 */
		
		long msTravelTime = TimeTranslation.hoursToMs(time);
		CityStockSet fs = tripUtil.getNeededFoodForTrip(army, ucc.getMyFood(), msTravelTime );
		
		assertTrue(fs.getFood(StockType.beef).getVisibleAmount() == 10.0d);
		assertTrue(fs.getFood(StockType.rice).getVisibleAmount() == 15.0d);
		assertTrue(fs.getFood(StockType.water).getVisibleAmount() == 20.0d);
		assertTrue(fs.getFood(StockType.fish).getVisibleAmount() == 20.0d);
		
		UsersCityConfig aggCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(this.ucc.getUserConfig().getCivil());
		UsersCityConfig defCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(this.target.getUserConfig().getCivil());
		
		NotFoodSet_dead tripRes = new NotFoodSet_dead();
		CityStockSet tripFood = new CityStockSet();
		
		/*
		 * wysy�amy armie na czas t = t/3, a to znaczy ze nie zabiora calego jedzenia z miasta.		
		 */
		time /= 3.0d;
		msTravelTime = TimeTranslation.hoursToMs(time);
		long nsTravelTime = TimeTranslation.msToNs(msTravelTime);
		
		tripUtil.sendTrip(tripRes, tripFood, army, ucc, target, ucc.getNsTime_(), Mission.robbery, msTravelTime/2);
		/*
		 * sprawdzamy czy po wyslaniu wojska stan miasta sie zgadza, tzn. wojsko zabralo potrzebne jedzenie.
		 * czy w miescie nie ma zadnej armi
		 */
		assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 0);
		assertTrue(ucc.getArmy().getUnitGroupById(11).getVisibleAmount() == 0);
		assertTrue(ucc.getArmy().getUnitGroupById(12).getVisibleAmount() == 0);
		
		/*
		 * policzymy teraz stan miasta atakowanego zaraz
		 * po przeprowadzonym ataku. Miasta atakujace powinno przegrac i odda� pewien procent swoich surowc�w
		 * armi atakujacej. Wojsko atakujace ma wystarczaja ilosc miejsca (350) zeby zrabowac mozliwie najwiecej surowcow.
		 * Pasek ludzi w miescie rabowanym jest ustawiony na 0 wiec nie ma ludzi w ogole, a to znaczy ze nic nie zostanie 
		 * zjedzone w tym miescie.
		 */
		double targetStartBeefCount = target.getMyFood().getFood(StockType.beef).getVisibleAmount();
		double targetStartRiceCount = target.getMyFood().getFood(StockType.rice).getVisibleAmount();
		double targetStartWaterCount = target.getMyFood().getFood(StockType.water).getVisibleAmount();
		double targetStartFishCount = target.getMyFood().getFood(StockType.fish).getVisibleAmount();
		
		double targetStartWoodCount = target.getMyResources().getResource(StockType.wood).getVisibleAmount();
		double targetStartIronCount = target.getMyResources().getResource(StockType.iron).getVisibleAmount();
		double targetStartGoldCount = target.getMyResources().getResource(StockType.gold).getVisibleAmount();
		double targetStartStoneCount = target.getMyResources().getResource(StockType.stone).getVisibleAmount();
		
		ccu.getFoodAT(target, nsTravelTime/2 + 10, new Integer(0));
		
		assertTrue(ucc.getOutcomeTrips().size() == 1);
		ucc.getOutcomeTrips().iterator().next().getResources();
		
		System.out.print("\na:" + target.getMyFood().getFood(StockType.beef).getVisibleAmount());
		System.out.print("\nb:"+ target.getMyFood().getFood(StockType.beef).getVisibleAmount() * (1.0d - defCivil.getPercentRobberySteal()));
		
		assertTrue(target.getMyFood().getFood(StockType.beef).getVisibleAmount() == 
			targetStartBeefCount * (1.0d - defCivil.getPercentRobberySteal()));
		assertTrue(target.getMyFood().getFood(StockType.rice).getVisibleAmount() == 
				targetStartRiceCount * (1.0d - defCivil.getPercentRobberySteal()));
		assertTrue(target.getMyFood().getFood(StockType.water).getVisibleAmount() == 
				targetStartWaterCount * (1.0d - defCivil.getPercentRobberySteal()));
		assertTrue(target.getMyFood().getFood(StockType.fish).getVisibleAmount() == 
				targetStartFishCount * (1.0d - defCivil.getPercentRobberySteal()));
		
		assertTrue(target.getMyResources().getResource(StockType.wood).getVisibleAmount() == 
				targetStartWoodCount * (1.0d - defCivil.getPercentRobberySteal()));
		assertTrue(target.getMyResources().getResource(StockType.iron).getVisibleAmount() == 
				targetStartIronCount * (1.0d - defCivil.getPercentRobberySteal()));
		assertTrue(target.getMyResources().getResource(StockType.gold).getVisibleAmount() == 
				targetStartGoldCount * (1.0d - defCivil.getPercentRobberySteal()));
		assertTrue(target.getMyResources().getResource(StockType.stone).getVisibleAmount() == 
				targetStartStoneCount * (1.0d - defCivil.getPercentRobberySteal()));
		

		
		
	}
	@Test
	public void test7() throws Exception
	{
		/*
		 * ustawiamy startowe jedzenie w miescie z ktorego wyrusza wojsko
		 */
		ucc.getMyFood().getFood(StockType.beef).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(15.0d);
		ucc.getMyFood().getFood(StockType.water).setCount(20.0d);
		ucc.getMyFood().getFood(StockType.fish).setCount(20.0d);
		/*
		 * ustawiamy surowce w miescie docelowym po to zeby je zrabowac
		 */
		target.getMyFood().getFood(StockType.beef).setCount(10.0d);
		target.getMyFood().getFood(StockType.rice).setCount(10.0d);
		target.getMyFood().getFood(StockType.water).setCount(10.0d);
		target.getMyFood().getFood(StockType.fish).setCount(10.0d);
		
		target.getMyResources().getResource(StockType.wood).setCount(10.0d);
		target.getMyResources().getResource(StockType.iron).setCount(10.0d);
		target.getMyResources().getResource(StockType.gold).setCount(10.0d);
		target.getMyResources().getResource(StockType.stone).setCount(10.0d);
		/*
		 * ustawiamy wojsko w miescie startujacym
		 */
		ucc.getArmy().getUnitGroupById(10).setCount(20);//w�ocznik
		ucc.getArmy().getUnitGroupById(11).setCount(20);//strzelec
		ucc.getArmy().getUnitGroupById(12).setCount(25);//�o�nierz
		
		/*
		 * ustawiamy wojsko w miescie atakowanym
		 */
		//target.getArmy().getUnitGroupById(10).setCount(8);//w�ocznik
		//target.getArmy().getUnitGroupById(11).setCount(8);//strzelec
		//target.getArmy().getUnitGroupById(12).setCount(8);//�o�nierz
		
		/*
		 * tworzymy wojsko, ktore chcemy wyslac na wyprawe.
		 * wysy�amy ca�e wojsko
		 */
		CityArmy army = new CityArmy();
		army.setUserConfig(ucc.getUserConfig());
		army.addUnitGroup(new CityUnitGroup(10, 20, army));
		army.addUnitGroup(new CityUnitGroup(11, 20, army));
		army.addUnitGroup(new CityUnitGroup(12, 25, army));
		/*
		 * sprawdzimy ile to wojsko ktore chcemy wysla�
		 * bedzie zjada� jedzenia na godzin�
		 */
		double eatPerHour = army.getEatPerHour();
		/*
		 * time - czas w godzinach, na jak dlugo wystarczy nam cale jedzenie dostepne w miescie dla armi
		 * sprawdzamy na ile godzin wystarczy nam jedzenia ktore mamy w miescie dla armi
		 */
		double time = ucc.getMyFood().getSumOfFood() / eatPerHour;
		
		long msTravelTime = TimeTranslation.hoursToMs(time);
		/*
		 * sprawdamy jakies jedzenie dostepne w miescie bedzie potrzebowac nasza wyslana armia.
		 * powinno zwrocic to samo co jest w miescie , dlatego ze na czas 'time'
		 * wojsko bedzie potrzebowalo calego jedzenia.
		 */
		CityStockSet fs = tripUtil.getNeededFoodForTrip(army, ucc.getMyFood(),msTravelTime);
		
		assertTrue(fs.getFood(StockType.beef).getVisibleAmount() == 10.0d);
		assertTrue(fs.getFood(StockType.rice).getVisibleAmount() == 15.0d);
		assertTrue(fs.getFood(StockType.water).getVisibleAmount() == 20.0d);
		assertTrue(fs.getFood(StockType.fish).getVisibleAmount() == 20.0d);
		
		UsersCityConfig aggCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(this.ucc.getUserConfig().getCivil());
		UsersCityConfig defCivil = SpringJSFUtil.getCivilizationHolder().getCivilization(this.target.getUserConfig().getCivil());
		
		NotFoodSet_dead tripRes = new NotFoodSet_dead();
		CityStockSet tripFood = new CityStockSet();
		
		/*
		 * wysy�amy armie na czas t = t/3, a to znaczy ze nie zabiora calego jedzenia z miasta.		
		 */
		time /= 3.0d;
		msTravelTime = TimeTranslation.hoursToMs(time);
		tripUtil.sendTrip(tripRes, tripFood, army, ucc, target, ucc.getNsTime_(), Mission.robbery, msTravelTime/2);
		/*
		 * sprawdzamy czy po wyslaniu wojska stan miasta sie zgadza, tzn. wojsko zabralo potrzebne jedzenie.
		 * czy w miescie nie ma zadnej armi
		 */
		assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 0);
		assertTrue(ucc.getArmy().getUnitGroupById(11).getVisibleAmount() == 0);
		assertTrue(ucc.getArmy().getUnitGroupById(12).getVisibleAmount() == 0);
		
		/*
		 * policzymy teraz stan miasta atakowanego zaraz
		 * po przeprowadzonym ataku. Miasta atakujace powinno przegrac i odda� pewien procent swoich surowc�w
		 * armi atakujacej. Wojsko atakujace ma wystarczaja ilosc miejsca (350) zeby zrabowac mozliwie najwiecej surowcow.
		 * Pasek ludzi w miescie rabowanym jest ustawiony na 0 wiec nie ma ludzi w ogole, a to znaczy ze nic nie zostanie 
		 * zjedzone w tym miescie.
		 */
		
		double uccStartBeefCount = ucc.getMyFood().getFood(StockType.beef).getVisibleAmount();
		double uccStartRiceCount = ucc.getMyFood().getFood(StockType.rice).getVisibleAmount();
		double uccStartWaterCount = ucc.getMyFood().getFood(StockType.water).getVisibleAmount();
		double uccStartFishCount = ucc.getMyFood().getFood(StockType.fish).getVisibleAmount();
		
		double uccStartWoodCount = ucc.getMyResources().getResource(StockType.wood).getVisibleAmount();
		double uccStartIronCount = ucc.getMyResources().getResource(StockType.iron).getVisibleAmount();
		double uccStartGoldCount = ucc.getMyResources().getResource(StockType.gold).getVisibleAmount();
		double uccStartStoneCount = ucc.getMyResources().getResource(StockType.stone).getVisibleAmount();
		
		double targetStartBeefCount = target.getMyFood().getFood(StockType.beef).getVisibleAmount();
		double targetStartRiceCount = target.getMyFood().getFood(StockType.rice).getVisibleAmount();
		double targetStartWaterCount = target.getMyFood().getFood(StockType.water).getVisibleAmount();
		double targetStartFishCount = target.getMyFood().getFood(StockType.fish).getVisibleAmount();
		
		double targetStartWoodCount = target.getMyResources().getResource(StockType.wood).getVisibleAmount();
		double targetStartIronCount = target.getMyResources().getResource(StockType.iron).getVisibleAmount();
		double targetStartGoldCount = target.getMyResources().getResource(StockType.gold).getVisibleAmount();
		double targetStartStoneCount = target.getMyResources().getResource(StockType.stone).getVisibleAmount();
		

		/*
		 * polczmy teraz stan miasta atakujacego zaraz po rozegraniu bitwy
		 * powinien on meiec nowy raport z bitwy
		 */
		
		/*
		 * policzymy teraz san miasta, ktore wys�alo wojsko
		 * po powrocie wojska miasto powinno miec dodatkowe surowce zdybte podczas wyprawy.
		 * Liczymy stan miasta 10 sekund po przybyciu wojska.
		 */
		
		long nsTravelTime = TimeTranslation.msToNs(msTravelTime);
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc,nsTravelTime , new Integer(0));
		long stop = System.currentTimeMillis();
		
		System.out.print("\nczas liczenia rekurencji(ms): " + (stop - start));
		
		assertTrue(ucc.getMyFood().getFood(StockType.beef).getVisibleAmount() ==
				uccStartBeefCount +
				targetStartBeefCount * defCivil.getPercentRobberySteal());
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getVisibleAmount() ==
				uccStartRiceCount +
				targetStartRiceCount * defCivil.getPercentRobberySteal());
		assertTrue(ucc.getMyFood().getFood(StockType.water).getVisibleAmount() ==
				uccStartWaterCount +
				targetStartWaterCount * defCivil.getPercentRobberySteal());
		assertTrue(ucc.getMyFood().getFood(StockType.fish).getVisibleAmount() ==
				uccStartFishCount +
				targetStartFishCount * defCivil.getPercentRobberySteal());
		
		assertTrue(ucc.getMyResources().getResource(StockType.wood).getVisibleAmount() ==
				uccStartWoodCount +
				targetStartWoodCount * defCivil.getPercentRobberySteal());
		assertTrue(ucc.getMyResources().getResource(StockType.iron).getVisibleAmount() ==
				uccStartIronCount +
				targetStartIronCount * defCivil.getPercentRobberySteal());
		assertTrue(ucc.getMyResources().getResource(StockType.gold).getVisibleAmount() ==
				uccStartGoldCount +
				targetStartGoldCount * defCivil.getPercentRobberySteal());
		assertTrue(ucc.getMyResources().getResource(StockType.stone).getVisibleAmount() ==
				uccStartStoneCount +
				targetStartStoneCount * defCivil.getPercentRobberySteal());
	}
			
}
