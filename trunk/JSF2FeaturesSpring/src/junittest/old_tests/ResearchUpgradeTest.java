package junittest.old_tests;

import static org.junit.Assert.assertTrue;

import game_model_impl.Boostable;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import helpers.TimeTranslation;

import java.lang.reflect.InvocationTargetException;



import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import com.jsfsample.managedbeans.SpringJSFUtil;

import economycomputation.CityCompUtil;
import entities.Civil;
import entities.Point;
import entities.UserConfig;
import entities.UserResearch;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;

import services.CityService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext.xml")
public class ResearchUpgradeTest {

	private UsersCityConfig ucc;
	@Autowired CityService cityService;
	@Autowired CityCompUtil ccu;
	@Autowired WorldDefinition ch;
	@Before
	public void setUp() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	{
		Testing.initTools();
		UserConfig friko = new UserConfig();
		friko.setCivil(Civil.korea);
		Point p = new Point();
		long nsNow = (long)System.currentTimeMillis()*1000l*1000l;
		ucc = new UsersCityConfig(friko, true, p, nsNow, ch);
	}
	@Test
	public void test_getFoodAt23() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100.0d);
		ucc.setMaxLudzi(100);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		
		ucc.getMyResources().getResource(StockType.gold).setCount(20000);
		ucc.getBuilding(201).setLevel(2);
		
		Boostable res = config.getById(2);
		assertTrue(res.getBuildTime(ucc) == 5000);
		
		assertTrue(ucc.getUserConfig().getResearch(2).upgrade(ucc));
		
		
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeNs = 5l*1000l*1000l*1000l;
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeNs, new Integer(0));
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		//System.out.print("rice count:" +fs.getFood(ResourceType.rice).getCount() + "\n");
		//System.out.print("rice storage:" +fs.getFood(ResourceType.rice).getStorage() + "\n");
		
//		assertTrue(SN == 33.0d);
//		assertTrue(SOP == 2);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.water).getStorage() == Storage.tak);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.rice).getStorage() == Storage.nie);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.pork).getStorage() == Storage.tak);
		assertTrue(ucc.getUserConfig().getLevel(2) == 1);
		
		assertTrue(ucc.getBuilding(res.getMotherBuilding()).getObjectBuildQueue().getBuildsAmount() == 0);
	}
	@Test
	public void test_getFoodAt22() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100.0d);
		ucc.setMaxLudzi(100);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		
		ucc.getMyResources().getResource(StockType.gold).setCount(20000);
		ucc.getBuilding(201).setLevel(2);
		
		Boostable res = config.getById(2);
		assertTrue(res.getBuildTime(ucc) == 5000);
		
		assertTrue(ucc.getUserConfig().getResearch(2).upgrade(ucc));
		
		
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeNs = 6l*1000l*1000l*1000l;
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeNs, new Integer(0));
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		//System.out.print("rice count:" +fs.getFood(ResourceType.rice).getCount() + "\n");
		//System.out.print("rice storage:" +fs.getFood(ResourceType.rice).getStorage() + "\n");
		
//		assertTrue(SN == 33.0d);
//		assertTrue(SOP == 2);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.water).getStorage() == Storage.tak);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.rice).getStorage() == Storage.nie);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.pork).getStorage() == Storage.tak);
		assertTrue(ucc.getUserConfig().getLevel(2) == 1);
		
		assertTrue(ucc.getBuilding(res.getMotherBuilding()).getObjectBuildQueue().getBuildsAmount() == 0);
	}
	@Test
	public void test_getFoodAt21() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100.0d);
		ucc.setMaxLudzi(100);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		
		ucc.getMyResources().getResource(StockType.gold).setCount(20000);
		ucc.getBuilding(201).setLevel(2);
		
		Boostable res = config.getById(2);
		assertTrue(res.getBuildTime(ucc) == 5000);
		
		assertTrue(ucc.getUserConfig().getResearch(2).upgrade(ucc));
		
		
		/*
		 * czas po 5-ciu minutach
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeNs = 4l*1000l*1000l*1000l;
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeNs, new Integer(0));
		long stop = System.currentTimeMillis();
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		//System.out.print("rice count:" +fs.getFood(ResourceType.rice).getCount() + "\n");
		//System.out.print("rice storage:" +fs.getFood(ResourceType.rice).getStorage() + "\n");
		
//		assertTrue(SN == 33.0d);
//		assertTrue(SOP == 2);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.water).getStorage() == Storage.tak);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.rice).getStorage() == Storage.nie);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.pork).getStorage() == Storage.tak);
		assertTrue(ucc.getUserConfig().getLevel(2) == 0);
		
		assertTrue(ucc.getBuilding(res.getMotherBuilding()).getObjectBuildQueue().getBuildsAmount() == 1);
	}
	@Test
	public void test_getFoodAt25() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100.0d);
		ucc.setMaxLudzi(100);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		
		ucc.getMyResources().getResource(StockType.gold).setCount(20000);
		ucc.getBuilding(5).setLevel(1);
		
		//ulepszeni wlocznikcy - id 1
		// wlocznicy - id 10
		// koszary - id 5
		
		Boostable koszary = config.getById(5);
		Boostable wlocznik = config.getById(10);
		Boostable ulepszeniWlocznicyB = config.getById(1);
		//assertTrue(res.getBuildTime(ucc) == 5000);
		CityBuilding koszarycb = ucc.getBuilding(5);
		
		assertTrue(koszarycb.buildUnit(10, 10) == 10);
		
		UserResearch ulepszeniWlocznicy = ucc.getUserConfig().getResearch(1);
		
		assertTrue(ulepszeniWlocznicy.upgrade(ucc));
		
		//CityBuilding motherBuilding = ucc.getBuilding(res.getMotherBuilding());
		
		assertTrue(koszarycb.getObjectBuildQueue().getBuildsAmount() == 2);
		assertTrue(koszarycb.getObjectBuildQueue().get(0).getBuildTime() == TimeTranslation.msToNs(wlocznik.getBuildTime(ucc) * 10 ));
		
		long afterTimeNs = TimeTranslation.msToNs(wlocznik.getBuildTime(ucc) * 10 ) + TimeTranslation.msToNs(ulepszeniWlocznicyB.getBuildTime(ucc)) - 10;
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeNs, new Integer(0));
		long stop = System.currentTimeMillis();
		
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		
		assertTrue(ucc.getUserConfig().getLevel(1) == 0);
		
		assertTrue(ucc.getBuilding(wlocznik.getMotherBuilding()).getObjectBuildQueue().getBuildsAmount() == 1);
	}
	
	@Test
	public void test_getFoodAt26() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100.0d);
		ucc.setMaxLudzi(100);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		
		ucc.getMyResources().getResource(StockType.gold).setCount(20000);
		ucc.getBuilding(5).setLevel(1);
		
		//ulepszeni wlocznikcy - id 1
		// wlocznicy - id 10
		// koszary - id 5
		
		Boostable koszary = config.getById(5);
		Boostable wlocznik = config.getById(10);
		Boostable ulepszeniWlocznicyB = config.getById(1);
		//assertTrue(res.getBuildTime(ucc) == 5000);
		CityBuilding koszarycb = ucc.getBuilding(5);
		
		assertTrue(koszarycb.buildUnit(10, 10) == 10);
		
		UserResearch ulepszeniWlocznicy = ucc.getUserConfig().getResearch(1);
		
		assertTrue(ulepszeniWlocznicy.upgrade(ucc));
		
		//CityBuilding motherBuilding = ucc.getBuilding(res.getMotherBuilding());
		
		assertTrue(koszarycb.getObjectBuildQueue().getBuildsAmount() == 2);
		assertTrue(koszarycb.getObjectBuildQueue().get(0).getBuildTime() == TimeTranslation.msToNs(wlocznik.getBuildTime(ucc) * 10 ));
		
		long afterTimeNs = TimeTranslation.msToNs(wlocznik.getBuildTime(ucc) * 10 ) + TimeTranslation.msToNs(ulepszeniWlocznicyB.getBuildTime(ucc)) + 10;
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeNs, new Integer(0));
		long stop = System.currentTimeMillis();
		
		System.out.print("\nfood comp time: " + (stop - start) +" ms");
		
		assertTrue(ucc.getUserConfig().getLevel(1) == 1);
		
		assertTrue(ucc.getBuilding(wlocznik.getMotherBuilding()).getObjectBuildQueue().getBuildsAmount() == 0);
	}
	
}
