package junittest.old_tests;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;



import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import com.jsfsample.managedbeans.SpringJSFUtil;


import services.CityService;
import economycomputation.ConvertBuildingUtil;
import economycomputation.CityCompUtil;
import entities.CityLode;
import entities.Civil;
import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;
import entities.stocks.CityResource;
import entities.stocks.CityStockSet;
import entities.stocks.TripStock;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext.xml")
public class BirthRateTest {

	private UsersCityConfig ucc;
	@Autowired CityService cityService;
	@Autowired CityCompUtil ccu;
	@Autowired WorldDefinition ch;
	@Before
	public void setUp() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException
	{
		Testing.initTools();
		UserConfig friko = new UserConfig();
		friko.setCivil(Civil.korea);
		Point p = new Point();
		long nsNow = (long)System.currentTimeMillis()*1000l*1000l;
		ucc = new UsersCityConfig(friko, true, p, nsNow, ch);
	}
	@Test
	public void br_test1() throws InstantiationException, IllegalAccessException
	{
		// przydzielami levele budynkom
//		ucc.getLevelMap().put(203, 1);//woda. budynek dostawca wody ma id = 203
//		ucc.getLevelMap().put(204, 1);//rice. budynek farma ryzu id = 204
//		ucc.getLevelMap().put(207, 1);//pork. budynek pork ma id = 207
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(150.0d);

		ucc.setMaxLudzi(150);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);
		ecb.setLevel(1);
		assertTrue( ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);
		ecb.setLevel(1);
		assertTrue( ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);
		ecb.setLevel(1);
		assertTrue( ecb.setPeople(50));
		//prydzielamy ludzi do budynków. Tyle ile jest ludzi, taka jest produkcja na godzinę.
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		//Testing.mh.getCommonConfig(ucc.getUserConfig()).getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		/*
		 * OPIS:
		 * fabryki produkują po 50. nie mamy jedzenia w spichlerzu
		 * na pewno wszystkie fabryki dostarczją jedzenie, bo ludzie potrzebują po 33.3.
		 * suma niedoborów wa takim razie SN = 0
		 * a suma dostawców SOP = 3
		 * oraz 3 x Storage.tak
		 */
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		
		
		ucc.getSN_SOP2();
		assertTrue(ucc.getBirthRatePerHour() == 0 );
	}
	@Test
	public void br_test2() throws InstantiationException, IllegalAccessException
	{
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(150.0d);
		ucc.setMaxLudzi(140);
		// przydzielami levele budynkom
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);
		ecb.setLevel(1);
		assertTrue( ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);
		ecb.setLevel(1);
		assertTrue( ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);
		ecb.setLevel(1);
		assertTrue( ecb.setPeople(50));
		//prydzielamy ludzi do budynków. Tyle ile jest ludzi, taka jest produkcja na godzinę.
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
	
		/*
		 * OPIS:
		 * fabryki produkują po 50. nie mamy jedzenia w spichlerzu
		 * na pewno wszystkie fabryki dostarczją jedzenie, bo ludzie potrzebują po 33.3.
		 * suma niedoborów wa takim razie SN = 0
		 * a suma dostawców SOP = 3
		 * oraz 3 x Storage.tak
		 */
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		
		
		ucc.getSN_SOP2();
		assertTrue(ucc.getBirthRatePerHour() == 0 );
	}
	@Test
	public void br_test3() throws InstantiationException, IllegalAccessException
	{
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(150.0d);
		ucc.setMaxLudzi(140);
		// przydzielami levele budynkom
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(50));
		//prydzielamy ludzi do budynków. Tyle ile jest ludzi, taka jest produkcja na godzinę.
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		/*
		 * OPIS:
		 * fabryki produkują po 50. nie mamy jedzenia w spichlerzu
		 * na pewno wszystkie fabryki dostarczją jedzenie, bo ludzie potrzebują po 33.3.
		 * suma niedoborów wa takim razie SN = 0
		 * a suma dostawców SOP = 3
		 * oraz 3 x Storage.tak
		 */
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		ucc.getSN_SOP2();
		double br = ucc.getBirthRatePerHour();
		assertTrue(ucc.getBirthRatePerHour() == 0 );
	}
	@Test
	public void br_test4() throws InstantiationException, IllegalAccessException
	{
		// przydzielami levele budynkom
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);
		ecb.setLevel(1);
		ecb.setPeople(50);
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);
		ecb.setLevel(1);
		ecb.setPeople(50);
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);
		ecb.setLevel(1);
		ecb.setPeople(50);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		/*
		 * OPIS:
		 * fabryki produkują po 50. nie mamy jedzenia w spichlerzu
		 * na pewno wszystkie fabryki dostarczją jedzenie, bo ludzie potrzebują po 33.3.
		 * suma niedoborów wa takim razie SN = 0
		 * a suma dostawców SOP = 3
		 * oraz 3 x Storage.tak
		 */
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(160.0d);
		ucc.setMaxLudzi(140);
		
		ucc.getSN_SOP2();
		assertTrue(ucc.getBirthRatePerHour() < 0 );
	}
	
	@Test
	public void br_test5() throws InstantiationException, IllegalAccessException
	{
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		// przydzielami levele budynkom
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);
		ecb.setLevel(1);
		ecb.setPeople(50);
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);
		ecb.setLevel(1);
		ecb.setPeople(50);
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);
		ecb.setLevel(1);
		ecb.setPeople(50);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		/*
		 * OPIS:
		 * fabryki produkują po 50. nie mamy jedzenia w spichlerzu
		 * na pewno wszystkie fabryki dostarczją jedzenie, bo ludzie potrzebują po 33.3.
		 * suma niedoborów wa takim razie SN = 0
		 * a suma dostawców SOP = 3
		 * oraz 3 x Storage.tak
		 */
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(160.0d);
		ucc.setMaxLudzi(170);
		
		ucc.getSN_SOP2();
		assertTrue(ucc.getBirthRatePerHour() < 0 );
	}
	@Test
	public void br_test6() throws InstantiationException, IllegalAccessException
	{
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		// przydzielami levele budynkom
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(140.0d);
		ucc.setMaxLudzi(160);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(40));
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		/*
		 * OPIS:
		 * fabryki produkują po 50. nie mamy jedzenia w spichlerzu
		 * na pewno wszystkie fabryki dostarczją jedzenie, bo ludzie potrzebują po 33.3.
		 * suma niedoborów wa takim razie SN = 0
		 * a suma dostawców SOP = 3
		 * oraz 3 x Storage.tak
		 */
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		
		
		ucc.getSN_SOP2();
		
		assertTrue(ucc.getBirthRatePerHour() == 0 );
	}
	@Test
	public void br_test7() throws InstantiationException, IllegalAccessException
	{
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		// przydzielami levele budynkom
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(140.0d);
		ucc.setMaxLudzi(150);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(40));
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		/*
		 * OPIS:
		 * fabryki produkują po 50. nie mamy jedzenia w spichlerzu
		 * na pewno wszystkie fabryki dostarczją jedzenie, bo ludzie potrzebują po 33.3.
		 * suma niedoborów wa takim razie SN = 0
		 * a suma dostawców SOP = 3
		 * oraz 3 x Storage.tak
		 */
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		
		ucc.getSN_SOP2();
		
		assertTrue(ucc.getBirthRatePerHour() == 0 );
	}
	@Test
	public void br_test8() throws InstantiationException, IllegalAccessException
	{
		// przydzielami levele budynkom
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		// przydzielami levele budynkom
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);
		ecb.setLevel(1);
		ecb.setPeople(50);
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);
		ecb.setLevel(1);
		ecb.setPeople(50);
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);
		ecb.setLevel(1);
		ecb.setPeople(40);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		/*
		 * OPIS:
		 * fabryki produkują po 50. nie mamy jedzenia w spichlerzu
		 * na pewno wszystkie fabryki dostarczją jedzenie, bo ludzie potrzebują po 33.3.
		 * suma niedoborów wa takim razie SN = 0
		 * a suma dostawców SOP = 3
		 * oraz 3 x Storage.tak
		 */
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(160.0d);
		ucc.setMaxLudzi(150);
		ucc.getSN_SOP2();
		
		assertTrue(ucc.getBirthRatePerHour() < 0 );
	}
}
