package junittest.old_tests;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;



import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import com.jsfsample.managedbeans.SpringJSFUtil;

import civil_config.KoreaConfig;

import services.CityService;
import economycomputation.ConvertBuildingUtil;
import economycomputation.CityCompUtil;
import economycomputation.DefaultRecursionCdddalculator;
import economycomputation.RecursionCalculator;
import entities.CityLode;
import entities.Civil;
import entities.Point;
import entities.Storage;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.ExtractCityBuilding;
import entities.stocks.CityResource;
import entities.stocks.CityStockSet;
import entities.stocks.NotFoodSet_dead;
import entities.stocks.TripStock;
import game_model_impl.Boostable;
import game_model_impl.StockType;
import game_model_impl.WorldDefinition;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext.xml")
public class GetSN_SOPTest {

	private UsersCityConfig ucc;
	@Autowired CityService cityService;
	@Autowired CityCompUtil ccu;
	@Autowired WorldDefinition ch;
	@Before
	public void setUp() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException 
	{
		Testing.initTools();
		UserConfig friko = new UserConfig();
		friko.setCivil(Civil.korea);
		Point p = new Point();
		long nsNow = (long)System.currentTimeMillis()*1000l*1000l;
		ucc = new UsersCityConfig(friko, true, p, nsNow, ch);
	}
	@Test
	public void test65_getSN_SOP() throws Exception{
		
		/*
		 * w miescie jest 100 ludzi. pasek ustawion na 100
		 * w miescie mamy tylko ry� (10) a woda produkowana jest w ilosci 50.
		 * Ludzie chcieliby je�� wody i ry�u po r�wno tzn po 50.
		 * ry� daje satysfakcje +2 , ale podatek -2 powoduje ze satysfakcja == 0.
		 * Z tego powodu birthRate powinien byc == 0.
		 * Stan wody powinien by ustawiony na 'nie', dlatego ze tyle ile ludzie jedz� (50) tyle jest produkowane.
		 * Stan ryzu == tak , dlatego ze mamy go w spichlerzu.
		 */
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(110);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.setTaxSatisfaction(2);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(0);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(10.0d);
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		
		
		
		int SOP  = ucc.getMyFood().getSOP();
		
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		
		assertTrue(ucc.getBirthRatePerHour() == 0);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.tak);
	}
	@Test
	public void test66_getSN_SOP() throws Exception{
		
		/*
		 * w miescie jest 100 ludzi. pasek ustawion na 110
		 * w miescie mamy tylko ry� (10) a woda produkowana jest w ilosci 50.
		 * Ludzie chcieliby je�� wody i ry�u po r�wno tzn po 50.
		 * ry� daje satysfakcje +2 , ale podatek -1 powoduje ze satysfakcja == 1.
		 * Z tego powodu birthRate powinien byc > 0.
		 * Stan wody powinien by ustawiony na 'nie', dlatego ze tyle ile ludzie jedz� (50) tyle jest produkowane, dodatkowo ludzie przychodz� do miasta.
		 * Stan ryzu == tak , dlatego ze mamy go w spichlerzu.
		 */
		
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(110);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.setTaxSatisfaction(1);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(0);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(10.0d);
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		
		assertTrue(ucc.getBirthRatePerHour() > 0);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.tak);
	}
	@Test
	public void test67_getSN_SOP() throws Exception{
		
		/*
		 * w miescie jest 100 ludzi. pasek ustawion na 110
		 * w miescie mamy tylko ry� (10) a woda produkowana jest w ilosci 50.
		 * Ludzie chcieliby je�� wody i ry�u po r�wno tzn po 50.
		 * ry� daje satysfakcje +2 , ale podatek -3 powoduje ze satysfakcja == -1.
		 * Z tego powodu birthRate() powinien zwracac 0. Wyja�nienie:
		 * Z tego powodu ze wody produkowane jest 50, oraz zjadanie ludzi jednego surowca jest rownie� 50,
		 * jest to punkt krytyczny. Pocz�tkowo mamy satysfakcje -1 (patrz wy�ej). To znaczy ze ludzie powinni odchodzic z miasta.
		 * Jednak gdy zaczynaj� odchodzi� z miasta, produkcja wody, kt�ra jest w punkcie przej�ciowym, zaczyna produkowa� wode do spichlerza,
		 * a tym samym daje satysfakcje +2. Tak wi�c satysfakcja sumaryczna wynosi teraz +1, co znowu powoduje przychodzenie ludzi do miasta, 
		 * i tak powstaje zamkniete ko�o. Nale�y wi�c wod� ustawi� w stan 'na styk' , aby zatrzyma� hu�tawke.
		 * Mog� by� jeszcze inne produkcje  kt�re wcale nie musz� by� 'na styk'.
		 * Stan wody powinien by ustawiony na 'nie', dlatego ze tyle ile ludzie jedz� (50) tyle jest produkowane, dodatkowo ludzie przychodz� do miasta.
		 * Stan ryzu == tak , dlatego ze mamy go w spichlerzu.
		 */
		
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(110);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.setTaxSatisfaction(3);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(50));
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(0);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(10.0d);
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		
		assertTrue(ucc.getBirthRatePerHour() == 0);
		assertTrue(SN == 50);
		assertTrue(SOP == 1);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.tak);
	}
	@Test
	public void test68_getSN_SOP() throws Exception{
		
		/*
		 * w miescie jest 100 ludzi. pasek ustawion na 110
		 * w miescie mamy tylko ry� (10) , woda produkowana jest w ilosci 40 , pork produkowany jest w ilo�ci 20.
		 * Ludzie chcieliby je�� wody, ry�u i pork po r�wno tzn po 33,(3).
		 * ry� daje satysfakcje +2 , ale podatek -3 powoduje ze satysfakcja == -1.
		 * Analiza krok po kroku:
		 * Pocz�tkowo ludzie chcieliby je�� po 33,(3).
		 * Pork nie wyrabia(produkcja 20). Sprawdzmy w takim razie ile potrzebuj� woda i ry�, gdy wspomaga ich pork:
		 * 100 - 20 / 2 = 40.
		 * Ry� na pewno wyrabia, bo jest w spichlerzu, woda produkuje 40, czyli tyle ile potrzbuj�.
		 * Aktualnie satysfakcja == -1, wie� ludzie chcieliby odchodzi� z miasta.
		 * Ale gdy tylko zaczn� odchodzi�, woda zacznie wyrabia� , dlatego �e jest w 'punkcie r�wnowagi'.
		 * Pojawi si� wtedy dodatkowa satysfakcja z wody co spowoduje sumaryczn� satysfakcje +1.
		 * I pojawia si� zamkni�te ko�o. Dlatego aby zatrzyma� hu�tawk�, ustawiamy wodzie stan 'na styk'.
		 * Mog� by� jeszcze inne produkcje  kt�re wcale nie musz� by� 'na styk'.
		 * Stan pork == nie , dlatego ze jest fabryk� niedomiarow�.
		 * Stan ryzu == tak , dlatego ze mamy go w spichlerzu.
		 */
		
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(110);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.setTaxSatisfaction(3);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(40));
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(0);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(10.0d);
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		
		assertTrue(ucc.getBirthRatePerHour() == 0);
		assertTrue(SN == 60);
		assertTrue(SOP == 1);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nie);
	}
	@Test
	public void test69_getSN_SOP() throws Exception{
		
		/*
		 * w miescie jest 100 ludzi. pasek ustawion na 110
		 * w miescie mamy tylko ry� (10) , woda produkowana jest w ilosci 40 , pork produkowany jest w ilo�ci 20.
		 * Ludzie chcieliby je�� wody, ry�u i pork po r�wno tzn po 33,(3).
		 * ry� daje satysfakcje +2 , ale podatek -3 powoduje ze satysfakcja == -1.
		 * Analiza krok po kroku:
		 * Pocz�tkowo ludzie chcieliby je�� po 33,(3).
		 * Pork nie wyrabia(produkcja 20). Sprawdzmy w takim razie ile potrzebuj� woda i ry�, gdy wspomaga ich pork:
		 * 100 - 20 / 2 = 40.
		 * Ry� na pewno wyrabia, bo jest w spichlerzu, woda produkuje 40, czyli tyle ile potrzbuj�.
		 * Aktualnie satysfakcja == -1, wie� ludzie chcieliby odchodzi� z miasta.
		 * Ale gdy tylko zaczn� odchodzi�, woda zacznie wyrabia� , dlatego �e jest w 'punkcie r�wnowagi'.
		 * Pojawi si� wtedy dodatkowa satysfakcja z wody co spowoduje sumaryczn� satysfakcje +1.
		 * I pojawia si� zamkni�te ko�o. Dlatego aby zatrzyma� hu�tawk�, ustawiamy wodzie stan 'na styk'.
		 * Mog� by� jeszcze inne produkcje  kt�re wcale nie musz� by� 'na styk'.
		 * Stan pork == nie , dlatego ze jest fabryk� niedomiarow�.
		 * Stan ryzu == tak , dlatego ze mamy go w spichlerzu.
		 */
		
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(110);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.setTaxSatisfaction(1);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(40));
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(0);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(10.0d);
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		
		assertTrue(ucc.getBirthRatePerHour() > 0);
		assertTrue(SN == 60);
		assertTrue(SOP == 1);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nie);
	}
	@Test
	public void test70_getSN_SOP() throws Exception{
		
		/*
		 * w miescie jest 100 ludzi. pasek ustawion na 90
		 * w miescie mamy tylko ry� (10) , woda produkowana jest w ilosci 40 , pork produkowany jest w ilo�ci 20.
		 * Ludzie chcieliby je�� wody, ry�u i pork po r�wno tzn po 33,(3).
		 * ry� daje satysfakcje +2 , ale podatek -3 powoduje ze satysfakcja == -1.
		 * Analiza krok po kroku:
		 * Pocz�tkowo ludzie chcieliby je�� po 33,(3).
		 * Pork nie wyrabia(produkcja 20). Sprawdzmy w takim razie ile potrzebuj� woda i ry�, gdy wspomaga ich pork:
		 * 100 - 20 / 2 = 40.
		 * Ry� na pewno wyrabia, bo jest w spichlerzu, woda produkuje 40, czyli tyle ile potrzbuj�.
		 * Pasek ludzi jest ustawiony na 90, wie� ludzie chcieliby odchodzi� z miasta.
		 * Gdy tylko zaczn� odchodzi�, woda zacznie wyrabia� , co spowoduje sumaryczn� satysfakcje == +1.
		 * Pojawi si� wtedy dodatkowa satysfakcja z wody co spowoduje sumaryczn� satysfakcje +1.
		 * Pomimo dodatniej satysf. ludzie nadal powinni odchodzi� z powodu paska ludzi == 90.
		 * Stan pork == nie , dlatego ze jest fabryk� niedomiarow�.
		 * Stan ryzu == tak , dlatego ze mamy go w spichlerzu.
		 * Stan woda == tak, dlatego ze przy punkcie r�wnowagi zaczyna wyrabia�(ludzie odchodz�).
		 */
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(90);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.setTaxSatisfaction(3);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(40));
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(0);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(10.0d);
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		
		assertTrue(ucc.getBirthRatePerHour() < 0);
		assertTrue(SN == 20);
		assertTrue(SOP == 2);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nie);
	}
	@Test
	public void test21_getSN_SOP() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(90);
		ucc.setMaxLudzi(80);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(17);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(16);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(15);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(21.0d);
		
		ucc.getMyFood().getFood(StockType.water).setCount(21.0d);
		
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(15.0d);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		Double riceChTime = ccu.getChangeTime(ucc, StockType.rice, SOP, SN, A,B,C,false);
		Double porkChTime = ccu.getChangeTime(ucc, StockType.pork, SOP, SN, A,B,C,true);
		assertTrue(riceChTime > 0);
		assertTrue(SN == 16.0d);
		assertTrue(SOP == 2);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.tak);
	}
	@Test
	public void test20_getSN_SOP() throws Exception{
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(0);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(0);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(0);
		
		
		ucc.getMyFood().getFood(StockType.water).setCount(15.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(15.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(15.0d);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(90);
		ucc.setMaxLudzi(90);
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		Double waterChTime = ccu.getChangeTime(ucc, StockType.water, SOP, SN, A,B,C,true);
		Double riceChTime = ccu.getChangeTime(ucc, StockType.rice, SOP, SN, A,B,C,true);
		Double porkChTime = ccu.getChangeTime(ucc, StockType.pork, SOP, SN, A,B,C,true);
		assertTrue(waterChTime == 0.5);
		assertTrue(riceChTime == 0.5);
		assertTrue(porkChTime == 0.5);
		assertTrue(SN == 0.0d);
		assertTrue(SOP == 3);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.tak);
	}
	@Test
	public void test_getSN_SOP() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		assertTrue(ucc.setMaxLudzi(90));
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(34);
		
		assertTrue(ucc.getMaxLudzi() == 100);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0d);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		Double waterChTime = ccu.getChangeTime(ucc, StockType.water, SOP, SN, A,B,C,true);
		Double riceChTime = ccu.getChangeTime(ucc, StockType.rice, SOP, SN, A,B,C,false);
		Double porkChTime = ccu.getChangeTime(ucc, StockType.pork, SOP, SN, A,B,C,true);
		assertTrue(waterChTime > 0);
		assertTrue(riceChTime == null);
		assertTrue(porkChTime == null);
		assertTrue(SN == 33.0d);
		assertTrue(SOP == 2);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.tak);
	}
	@Test
	public void test12_getSN_SOP() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(200);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(34);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		
		Double waterChTime = ccu.getChangeTime(ucc, StockType.water, SOP, SN, A,B,C,true);
		Double riceChTime = ccu.getChangeTime(ucc, StockType.rice, SOP, SN, A,B,C,false);
		Double porkChTime = ccu.getChangeTime(ucc, StockType.pork, SOP, SN, A,B,C,true);
		assertTrue(waterChTime > 0.0d);
		assertTrue(riceChTime == null);
		assertTrue(porkChTime > 0.0d);
		assertTrue(SN == 33.0d);
		assertTrue(SOP == 2);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.tak);
	}
	@Test
	public void test11_getSN_SOP() throws Exception{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(100);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(34);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		
		Double waterChTime = ccu.getChangeTime(ucc, StockType.water, SOP, SN, A,B,C,true);
		Double riceChTime = ccu.getChangeTime(ucc, StockType.rice, SOP, SN, A,B,C,false);
		Double porkChTime = ccu.getChangeTime(ucc, StockType.pork, SOP, SN,A,B,C, true);
		assertTrue(waterChTime > 0);
		assertTrue(riceChTime == null);
		assertTrue(porkChTime == null);
		assertTrue(SN == 33.0d);
		assertTrue(SOP == 2);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.tak);
	}
	@Test
	public void test10_getSN_SOP() throws Exception{
		
		/*
		 * opis:
		 * ludzi jest 100 i zjadaj� pocz�tkowo po 33,(3) jedzenia.
		 * mamy:
		 * woda : 10
		 * ryz : 0
		 * pork: 0
		 * 
		 * pasek jest ustawion powyzej, tzn na 101.
		 * 
		 */
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(101);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(34);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		Double waterChTime = ccu.getChangeTime(ucc,StockType.water,SOP, SN, A,B,C,true);
		Double riceChTime = ccu.getChangeTime(ucc, StockType.rice, SOP, SN, A,B,C,false);
		Double porkChTime = ccu.getChangeTime(ucc, StockType.pork, SOP, SN, A,B,C,true);
		/*
		 * Wody 'zjadaj�' 33,3 wiec kiedy sie skonczy, bo jest produkowane tylko 33.
		 * 
		 */
		assertTrue(waterChTime > 0);
		/*
		 * ry�u nigdy nie przybedzie, dlatego ze produkowane jest tylko 33
		 */
		assertTrue(riceChTime == null);
		/*
		 * porku wprawdzie na teraz starcza bo produkowane jest 34, a ludzie zjadaj� tylko 33,(3)
		 * ale z uwagi na to, �e ludzie przychodz� do kraju, w koncu sie skonczy
		 */
		assertTrue(porkChTime > 0);
		assertTrue(SN == 33.0d);
		assertTrue(SOP == 2);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.tak);
	}
	@Test
	public void test9_getSN_SOP() throws Exception
	{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(60);
		ucc.setMaxLudzi(60);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		
		
		ucc.getSN_SOP2();
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		RecursionCalculator recCalc = new DefaultRecursionCdddalculator();
		Double waterChTime = recCalc.getFoodChangeTime(ucc, StockType.water, A,B,C,true);
		Double riceChTime = recCalc.getFoodChangeTime(ucc, StockType.rice, A,B,C,true);
		Double porkChTime = recCalc.getFoodChangeTime(ucc, StockType.pork, A,B,C,true);
		assertTrue(ucc.getSN() == 60.0d);
		assertTrue(ucc.getSOP() == 0);
		assertTrue(waterChTime == null);
		assertTrue(waterChTime == null);
		assertTrue(waterChTime == null);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nastyk);
	}
	@Test
	public void test8_getSN_SOP() throws InstantiationException, IllegalAccessException
	{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(60);
		ucc.setMaxLudzi(70);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(20);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		assertTrue(SN == 60.0d);
		assertTrue(SOP == 0);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nastyk);
	}
	@Test
	public void test6_getSN_SOP() throws InstantiationException, IllegalAccessException
	{
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(61);
		ucc.setMaxLudzi(61);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(21);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(20);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		/*
		 *  
		 */
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		assertTrue(SN == 61.0d);
		assertTrue(SOP == 0);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nastyk);
	}
	@Test
	public void test15_getSN_SOP() throws Exception
	{
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(61);
		ucc.setMaxLudzi(61);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(21);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(20);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		Double waterChTime = ccu.getChangeTime(ucc, StockType.water, SOP, SN, A,B,C,true);
		Double riceChTime = ccu.getChangeTime(ucc, StockType.rice, SOP, SN, A,B,C,false);
		Double porkChTime = ccu.getChangeTime(ucc, StockType.pork, SOP, SN, A,B,C,false);
		assertTrue(waterChTime == null);
		assertTrue(riceChTime == null);
		assertTrue(porkChTime == null);
		assertTrue(SN == 40.0d);
		assertTrue(SOP == 1);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nie);
	}
	@Test
	public void test14_getSN_SOP() throws InstantiationException, IllegalAccessException
	{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(61);
		ucc.setMaxLudzi(61);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(21);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(20);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		
		/*
		 *  
		 */
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		assertTrue(SN == 40.0d);
		assertTrue(SOP == 1);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nie);
	}
	@Test
	public void test18_getSN_SOP() throws Exception
	{
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(80);
		ucc.setMaxLudzi(80);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(21);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(20);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0d);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(3.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(3.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(3.0d);
		
		/*
		 * O FLAGACH tak/nie/nastyk
		 * 
		 * flaga 'tak' jest ustawiona na jedzeniu wtedy, gdy od obecnej chwili (czas startowy)
		 * do pewnej chwili (dop�ki nie nast�pi jaka� zmiana stanu przynajmniej jednego rodzaju jedzenia)
		 * mamy nadmiar tego jedzenia, tzn. odk�ada si� do spichlerza.
		 * 
		 * flaga 'nie' ustawiona jest na jedzeniu wtedy, gdy s� spe�nione dwa warunki:
		 * - w spichlerzu ilo�� jedzenia = 0.0
		 * - fabryka produkuj�ca dany surowiec w najbli�szym czasie nie odk�ada tego surowca do spichlerza,
		 * z tego wzgl�du �e jest on na bierz�co zjadany, a to znaczy �e jego produkcja jest zbyt niska
		 * 
		 * O METODZIE getChangeTime(CityConfig, ResourceType, SOP, SN, isStored);
		 * CityConfig - Config miasta.
		 * ResourceType - typ po�ywienia.
		 * SOP(1*) - sum of providers - suma nadmiarowych rodzaj�w jedze�.
		 * uwaga: je�li jedzenie jest 'nastyk', tzn. nie odk�ada si� do spichlerza, tylko balansuje
		 * na granicy 0.0 to nie jest ono wliczane do SOP
		 * SN(2*) - suma produkcji surowc�w niedomiarowych.
		 * isStored - t� flag� informujemy metod� czy ma policzy� czas 'upadku jedzenia', tzn. kiedy
		 * jego zmiana nast�pi z tak -> nie, lub 'powstania jedzenia (nie ->tak).
		 * Innymi s�owy tutaj podajemy true je�li mamy aktualnie to jedzenie jako nadmiarowe,
		 * je�li nie dajemy false
		 * 
		 * metoda getSN_SOP2 ustawia flagi jedze�( kt�re s� w obiekcie przekazywanym przez argument)
		 * oraz wylicza SOP i SN.
		 * 
		 *	To kiedy nast�pi zmiana stanu surowca z tak -> nie lub nie -> tak, oraz czy w og�le nast�pi 
		 *  informuje na metoda 'getChangeTime'
		 *  je�li zwraca null, to znaczy �e stan badanego jedzenia nigdy si� nie zmieni.
		 *  np. ca�y czas b�dzie si� odk�ada�
		 *  do spilerza, albo np. stan tego jedzenia ca�y czas b�dzie ustawiony na 'nie'. tzn. jedzenie nigdy
		 *  nie zacznie si� odk�ada� do spichlerza.
		 * 
		 * flaga 'nastyk' ustawiona jest wtedy, gdy spe�nione s� nast�puj�ce warunki:
		 * - w spichlerzu nie mamy �adnego jedzenia,tzn. ka�de jest ustawione na 0.0, 
		 * -  
		 * 
		 */
		/*
		 * mamy 80 ludzi w kraju, oraz pasek ustawiony na 80, wi�c ilo�� ludzi sie nie zmienia w czasie, tzn jest sta�a.
		 * 
		 * W kraju mamy rodzaje jedzenia w spichlerzu(patrz 6 wierszy wy�ej ):
		 * water = 3.0
		 * rice = 3.0
		 * pork = 3.0
		 * 
		 * Produkcje s� takie :
		 * woda : 21/h
		 * rice: 20/h
		 * pork: 20/h
		 *
		*/
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		/*
		 * ludzie potrzebuj� przez ca�y czas 80/3 ka�dego jedzenia. Ka�da fabryka, nie da rady z wyrabianiem surowca
		 * bo ka�da produkuje oko�o 20. Tak wi�c ka�de z trzech jedze� w spichlerzu na pewno si� kiedy� sko�czy.
		 * */
		/*
		 * metoda getChangeTime zwraca czas po jakim stan jedzenia ulegnie zmianie( stany : tak/nie/nastyk ).
		 * trzeba jej poda� config miasta(ucc) , typ jedzenia(ResourceType), SOP - sum of providers
		 * czyli liczba dostarczycieli �ywno�ci, w tej chwili jest 3 bo mamy 3 rodzaje nadmiarowego jedzenia.
		 */
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		
		Double waterChTime = ccu.getChangeTime(ucc, StockType.water, SOP, SN, A,B,C,true);
		Double riceChTime = ccu.getChangeTime(ucc, StockType.rice, SOP, SN, A,B,C,true);
		Double porkChTime = ccu.getChangeTime(ucc, StockType.pork, SOP, SN, A,B,C,true);
		assertTrue(waterChTime > 0);
		assertTrue(riceChTime > 0);
		assertTrue(porkChTime > 0);
		assertTrue(SN == 0.0d);
		assertTrue(SOP == 3);
		/*
		 * 
		 * Storage ka�dego jedzenia mamy ustawione na tak, bo w obecnej chwili mamy ka�de jedzenie.
		 */
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.tak);
	}
	@Test
	public void test19_getSN_SOP() throws Exception
	{
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(150);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(34);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.getMyFood().getFood(StockType.beef).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.wheat).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(1.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		
		
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		Double waterChTime = ccu.getChangeTime(ucc, StockType.water, SOP, SN, A,B,C,true);
		Double riceChTime = ccu.getChangeTime(ucc, StockType.rice, SOP, SN, A,B,C, false);
		Double porkChTime = ccu.getChangeTime(ucc, StockType.pork, SOP, SN, A,B,C, true);
		Double wheatChTime = ccu.getChangeTime(ucc, StockType.wheat, SOP, SN,  A,B,C,false);
		Double beefChTime = ccu.getChangeTime(ucc, StockType.beef, SOP, SN, A,B,C, false);
		assertTrue(waterChTime > 0);
		assertTrue(riceChTime == null);
		assertTrue(porkChTime > 0);
		assertTrue(SN == 33.0d);
		assertTrue(SOP == 2);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.beef).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.wheat).getStorage() == Storage.nie);
	}
	@Test
	public void test17_getSN_SOP() throws Exception
	{
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(61);
		ucc.setMaxLudzi(100);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(21);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(20);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(3.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(3.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(3.0d);
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		Double waterChTime = ccu.getChangeTime(ucc, StockType.water, SOP, SN, A,B,C,true);
		Double riceChTime = ccu.getChangeTime(ucc, StockType.rice, SOP, SN, A,B,C,true);
		Double porkChTime = ccu.getChangeTime(ucc, StockType.pork, SOP, SN, A,B,C,true);
		assertTrue(waterChTime > 0);
		assertTrue(riceChTime > 0);
		assertTrue(porkChTime > 0);
		assertTrue(SN == 0.0d);
		assertTrue(SOP == 3);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.tak);
	}
	@Test
	public void test16_getSN_SOP() throws Exception
	{
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(61);
		ucc.setMaxLudzi(70);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(21);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(20);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(3.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(3.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(3.0d);
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		Double waterChTime = ccu.getChangeTime(ucc, StockType.water, SOP, SN,A,B,C, true);
		Double riceChTime = ccu.getChangeTime(ucc, StockType.rice, SOP, SN,A,B,C, true);
		Double porkChTime = ccu.getChangeTime(ucc, StockType.pork, SOP, SN, A,B,C,true);
		assertTrue(waterChTime > 0);
		assertTrue(riceChTime > 0);
		assertTrue(porkChTime > 0);
		assertTrue(SN == 0.0d);
		assertTrue(SOP == 3);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.tak);
	}
	@Test
	public void test13_getSN_SOP() throws Exception
	{
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(61);
		ucc.setMaxLudzi(61);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(21);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(20);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		
		Map<String,Double> abcFactors = ccu.getArmyEatFactors(ucc);
		Double A = abcFactors.get("A");
		Double B = abcFactors.get("B");
		Double C = abcFactors.get("C");
		
		Double waterChTime = ccu.getChangeTime(ucc, StockType.water, SOP, SN,A,B,C, true);
		Double riceChTime = ccu.getChangeTime(ucc, StockType.rice, SOP, SN,A,B,C, false);
		Double porkChTime = ccu.getChangeTime(ucc, StockType.pork, SOP, SN,A,B,C, false);
		assertTrue(waterChTime == null);
		assertTrue(riceChTime == null);
		assertTrue(porkChTime == null);
		assertTrue(SN == 61.0d);
		assertTrue(SOP == 0);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nastyk);
	}
	@Test
	public void test2_getSN_SOP() throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException, InstantiationException
	{
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(100);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(35);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(32);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		
		
		/*
		 *  
		 */
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc); 
		int SOP  = ucc.getMyFood().getSOP(); 
		assertTrue(SN == 100.0d);
		assertTrue(SOP == 0);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nastyk);
	}
	@Test
	public void test7_getSN_SOP() throws InstantiationException, IllegalAccessException, SecurityException, IllegalArgumentException, NoSuchMethodException, InvocationTargetException, NoSuchFieldException
	{
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(61);
		ucc.setMaxLudzi(61);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(20);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(20);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		
		
		/*
		 *  
		 */
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		assertTrue(SN == 60.0d);
		assertTrue(SOP == 0);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nie);
	}
	
	@Test
	public void test5_getSN_SOP() throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, NoSuchFieldException, InstantiationException
	{
		
		/*
		 * opis testu:
		 * 
		 * mamy 100 ludzi, pasek jest ustawiony na 100.
		 * produkowane jest dokladnie tyle jedzenia ile zjadaj� ludzie.
		 * znaczy to tyle, SOP czyli suma dostawc�w jedzenia jest r�wna 0.
		 * a SN - suma niedobor�w jest r�wna 100.
		 * kazde jedzenie jest produkowane 'nastyk'
		 */
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(100);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(35);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(32);
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		/*
		 *  
		 */
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc);
		int SOP  = ucc.getMyFood().getSOP();
		assertTrue(SN == 100.0d);
		assertTrue(SOP == 0);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nastyk);
	}
	
	@Test
	public void test3_getSN_SOP() throws InstantiationException, IllegalAccessException
	{
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100);
		ucc.setMaxLudzi(100);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(33);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		/*
		 * OPIS:
		 * ludzie potrzebuj� 100 jedzenia, a trzy fabryki produkuj� po 33, wi�c dostarczaj� 99 jedzenia.
		 * W dodatku nie mamy jedzenia w spichlerzu. W takim razie nie powinnysmy miec zadnego surowca nadmiarowo,
		 * a sumia produkcja nie dobor�w to 99.
		 */
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc); // SN to suma produkcji niedobor�w tzn, z powy�szej historyjki wynika �e niedoborami s� pork(produkcja 32) i rice(produkcja 32)
		int SOP  = ucc.getMyFood().getSOP(); // SOP to sum of providers , czyli ile rodzaj�w jedzenia mamy nadmiarowo, w tym przypadku tylko 1 - woda.
		assertTrue(SN == 99.0d);
		assertTrue(SOP == 0);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nie);
	}
	@Test
	public void test51_getSN_SOP() throws InstantiationException, IllegalAccessException
	{
		/*
		 * produkcja:
		 * woda: 500 - 100(sul) = 400
		 * rice: 500 - 100(sul) = 400
		 * pork :                 500
		 * sul:                   100
		 */
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(5);
		ucc.getPoint().getCityLode(StockType.rice).setValue(5);
		ucc.getPoint().getCityLode(StockType.pork).setValue(5);
		
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		
		ucc.getPoint().getCityLode(StockType.gold).setValue(1);
		ucc.getPoint().getCityLode(StockType.stone).setValue(1);
		ucc.getPoint().getCityLode(StockType.iron).setValue(1);
		ucc.getPoint().getCityLode(StockType.wood).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.beef).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.sul).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.wheat).setCount(0.0d);
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(1400.0d);
		ucc.setMaxLudzi(1300);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//ryz
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(205);//sul
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(200);//gold
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(201);//stona
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(214);//iron
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(206);//wood
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		assertTrue(ucc.getMaxLudzi() == 1300);
		Boostable sul = config.getById(205);
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		assertTrue(sul.getOutputPerHour(ucc, StockType.sul) == 100);
		
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.sul).getStorage() == Storage.nie);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.tak);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nie);
	}
	@Test
	public void test50_getSN_SOP() throws InstantiationException, IllegalAccessException
	{
		ucc.getPoint().getCityLode(StockType.water).setValue(5);
		ucc.getPoint().getCityLode(StockType.rice).setValue(5);
		ucc.getPoint().getCityLode(StockType.pork).setValue(5);
		
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		
		ucc.getPoint().getCityLode(StockType.gold).setValue(1);
		ucc.getPoint().getCityLode(StockType.stone).setValue(1);
		ucc.getPoint().getCityLode(StockType.iron).setValue(1);
		ucc.getPoint().getCityLode(StockType.wood).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.beef).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.sul).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.wheat).setCount(0.0d);
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(1400.0d);
		ucc.setMaxLudzi(1600);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//ryz
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(205);//sul
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(200);//gold
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(201);//stona
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(214);//iron
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(206);//wood
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		assertTrue(ucc.getMaxLudzi() == 1600);
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.sul).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
	}
	@Test
	public void test4_getSN_SOP() throws InstantiationException, IllegalAccessException
	{
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(150);
		ucc.setMaxLudzi(150);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding)ucc.getBuilding(203);//water
		ecb.setLevel(1);
		ecb.setPeople(50);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		ecb.setPeople(50);
		
		ecb = (ExtractCityBuilding)ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		ecb.setPeople(50);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.pork).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.sul).setValue(1);
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		
		/*
		 * OPIS:
		 * fabryki produkuj� po 50. nie mamy jedzenia w spichlerzu
		 * na pewno wszystkie fabryki dostarczj� jedzenie, bo ludzie potrzebuj� po 33.3.
		 * suma niedobor�w wa takim razie SN = 0
		 * a suma dostawc�w SOP = 3
		 * oraz 3 x Storage.tak
		 */
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(0.0d);
		
		
		HashMap<String, Object> ret = ccu.getSN_SOP2(ucc);
		double SN = ucc.getMyFood().getSN(ucc); // SN to suma produkcji niedobor�w tzn, z powy�szej historyjki wynika �e niedoborami s� pork(produkcja 32) i rice(produkcja 32)
		int SOP  = ucc.getMyFood().getSOP(); // SOP to sum of providers , czyli ile rodzaj�w jedzenia mamy nadmiarowo, w tym przypadku tylko 1 - woda.
		assertTrue(SN == 150.0d);
		assertTrue(SOP == 0);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getStorage() == Storage.nastyk);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getStorage() == Storage.nastyk);
	}
}
