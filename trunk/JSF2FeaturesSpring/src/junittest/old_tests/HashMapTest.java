package junittest.old_tests;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class HashMapTest {

	/**
	 * @param args
	 */
	public int a;
	public static void main(String[] args) {

		Map<Integer,HashMapTest> map = new HashMap<Integer,HashMapTest>();
		for(int i=0; i <10000 ; i++)
		{
			map.put(i, new HashMapTest());
		}
		List<HashMapTest> list = new ArrayList<HashMapTest>();
		for(int i=0; i <10000 ; i++)
		{
			list.add(new HashMapTest());
		}
		
		long start = System.nanoTime();
		Collection<HashMapTest> coll =  map.values();
		 Iterator<?> it = coll.iterator(); 
		while(it.hasNext())
		{
			it.next();
		}
		long stop = System.nanoTime();
		System.out.print("map iteration time: " + (stop - start) + " ns\n");
		
		start = System.nanoTime();
		for( int i=0; i < list.size();i++)
		{
			list.get(i);
		}
		stop = System.nanoTime();
		
		System.out.print("list iteration time: " + (stop - start) + " ns\n");
		
		
		
	}

}
