package junittest.old_tests;

import static org.junit.Assert.assertTrue;

import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import helpers.TimeTranslation;

import java.lang.reflect.InvocationTargetException;




import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import com.jsfsample.managedbeans.SpringJSFUtil;

import entities.Civil;
import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.ConvertCityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.buildings.UnitBuild;

import services.CityService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext.xml")
public class ConvertBuildingTest {

	public static UsersCityConfig ucc;
	@Autowired CityService cityService;
	@Autowired WorldDefinition ch;
	@Before
	public void setUp() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	{
		Testing.initTools();
		UserConfig friko = new UserConfig();
		friko.setCivil(Civil.korea);
		UserConfig galan = new UserConfig();
		galan.setCivil(Civil.korea);
		Point p1 = new Point();
		Point p2 = new Point();
		long nsNow = (long)System.currentTimeMillis()*1000l*1000l;
		ucc = new UsersCityConfig(friko, true, p1, nsNow, ch);
	}
	
	/*
	 * 
	 * uwaga !
	 * przetestuj koniecznie przypadek:
	 * produkcja wody: 0 , ilosc wody: 0:
	 * produkcja ry�u: 0 , ilosc ry�u: 0;
	 * produkcja sul : 41 
	 * i sprawdz produkcje sul, wody i ry�u.
	 * Produkcja wody i ry�y wychodzi�a -41/h !. 
	 * 
	 */
	@Test
	public void test1() throws Exception
	{
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.setMaxLudzi(100);
		ConvertCityBuilding ccb = (ConvertCityBuilding) ucc.getBuilding(205);//sul house
		ccb.setLevel(1);
		assertTrue(ccb.setPeople(100));
		Boostable sulHouse = config.getById(205);
		assertTrue(sulHouse.getOutputPerHour(ucc, StockType.sul) == 0);
		
		
	}
	@Test
	public void test2() throws Exception
	{
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(100.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(1.0d);
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.setMaxLudzi(100);
		ConvertCityBuilding ccb = (ConvertCityBuilding) ucc.getBuilding(205);//sul house
		ccb.setLevel(1);
		assertTrue(ccb.setPeople(100));
		Boostable sulHouse = config.getById(205);
		ucc.initFoodFlags();
		assertTrue(sulHouse.getOutputPerHour(ucc, StockType.sul) == 100);
		
	}
	@Test
	public void test3() throws Exception
	{
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(170.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(70));
		ucc.setMaxLudzi(170);
		ConvertCityBuilding ccb = (ConvertCityBuilding) ucc.getBuilding(205);//sul house
		ccb.setLevel(1);
		assertTrue(ccb.setPeople(100));
		Boostable sulHouse = config.getById(205);
		ucc.initFoodFlags();
		assertTrue(sulHouse.getOutputPerHour(ucc, StockType.sul) == 70);
		
	}
	@Test
	public void test4() throws Exception
	{
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(170.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(70));
		ucc.setMaxLudzi(170);
		ConvertCityBuilding ccb = (ConvertCityBuilding) ucc.getBuilding(205);//sul house
		ccb.setLevel(1);
		assertTrue(ccb.setPeople(100));
		Boostable sulHouse = config.getById(205);
		assertTrue(sulHouse.getOutputPerHour(ucc, StockType.sul) == 0);
		
	}
	@Test
	public void test5() throws Exception
	{
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(300.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(203);//water
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ucc.setMaxLudzi(300);
		ConvertCityBuilding ccb = (ConvertCityBuilding) ucc.getBuilding(205);//sul house
		ccb.setLevel(1);
		assertTrue(ccb.setPeople(100));
		Boostable sulHouse = config.getById(205);
		assertTrue(sulHouse.getOutputPerHour(ucc, StockType.sul) == 100);
		
	}
	@Test
	public void test6() throws Exception
	{
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(300.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getPoint().getCityLode(StockType.rice).setValue(1);
		ucc.getPoint().getCityLode(StockType.water).setValue(1);
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(99));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(203);//water
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ucc.setMaxLudzi(300);
		ConvertCityBuilding ccb = (ConvertCityBuilding) ucc.getBuilding(205);//sul house
		ccb.setLevel(1);
		assertTrue(ccb.setPeople(100));
		Boostable sulHouse = config.getById(205);
		assertTrue(sulHouse.getOutputPerHour(ucc, StockType.sul) == 99);
		
	}
	@Test
	public void test7() throws Exception
	{
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(300.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(0.0d);
		ucc.getPoint().getCityLode(StockType.rice).setValue(5);
		ucc.getPoint().getCityLode(StockType.water).setValue(5);
		ucc.getMyFood().getFood(StockType.water).setCount(0.0d);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(203);//water
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ucc.setMaxLudzi(300);
		ConvertCityBuilding ccb = (ConvertCityBuilding) ucc.getBuilding(205);//sul house
		ccb.setLevel(1);
		assertTrue(ccb.setPeople(100));
		Boostable sulHouse = config.getById(205);
		ucc.initFoodFlags();
		assertTrue(sulHouse.getOutputPerHour(ucc, StockType.sul) == 100);
		
	}
}
