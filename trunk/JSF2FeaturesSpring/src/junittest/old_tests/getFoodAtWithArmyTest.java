package junittest.old_tests;

import static org.junit.Assert.assertTrue;

import game_model_impl.StockType;
import game_model_impl.WorldDefinition;
import helpers.TimeTranslation;

import java.lang.reflect.InvocationTargetException;



import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import com.jsfsample.managedbeans.SpringJSFUtil;

import economycomputation.CityCompUtil;
import entities.Civil;
import entities.Point;
import entities.UserConfig;
import entities.UsersCityConfig;
import entities.buildings.CityBuilding;
import entities.buildings.ExtractCityBuilding;
import entities.stocks.NotFoodSet_dead;

import services.CityService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="file:WebContent/WEB-INF/applicationContext.xml")
public class getFoodAtWithArmyTest {

	private UsersCityConfig ucc;
	@Autowired CityService cityService;
	@Autowired CityCompUtil ccu;
	@Autowired WorldDefinition ch;
	@Before
	public void setUp() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchFieldException
	{
		Testing.initTools();
		UserConfig friko = new UserConfig();
		friko.setCivil(Civil.korea);
		Point p = new Point();
		long nsNow = (long)System.currentTimeMillis()*1000l*1000l;
		ucc = new UsersCityConfig(friko, true, p, nsNow, ch);
	}

	@Test
	public void test11_getFoodAt() throws Exception
	{
	
		/*
		 * opis testu:
		 * jedzenie produkowane jest w sumie 1500 /h.
		 * Ludzi jest 1000. Pasek ustawiony na 1000.
		 * armia:
		 * 500 w��cznik�w, ka�dy zjada 1/h.
		 * stan jedzenia :
		 * woda: 10
		 * ryz: 10
		 * pork: 10.
		 * 
		 * Po 1 h stan jedzenia nie powinien ulec zmianie dlatego, �e ludzie razem z armia
		 * zjadaja w sumie 1500 /h.
		 * 
		 */
		ucc.getPoint().getCityLode(StockType.water).setValue(5);
		ucc.getPoint().getCityLode(StockType.rice).setValue(5);
		ucc.getPoint().getCityLode(StockType.pork).setValue(5);
		
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.gold).setValue(1);
		ucc.getPoint().getCityLode(StockType.stone).setValue(1);
		ucc.getPoint().getCityLode(StockType.iron).setValue(1);
		ucc.getPoint().getCityLode(StockType.wood).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(10.0d);
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(1000.0d);
		ucc.setMaxLudzi(1000);
		
		ucc.getArmy().getUnitGroupById(10).setCount(500);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//woda
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//ryz
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(200);//gold
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(201);//stone
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(214);//iron
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(206);//wood
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		/*
		 * czas po 24 godzinach.
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeNs = 1000l*1000l*1000l*60l*60l*24l;
		//double afterTimeH = TimeTranslation.msToHours(afterTimeMs);
		
		//afterTimeH = 0.12800;
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeNs, new Integer(0));
		long stop = System.currentTimeMillis();
		System.out.print("\nilosc ludzi w miescie: " + ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount() + "\n");
		assertTrue(ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount() == 1000.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getVisibleAmount() == 10.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getVisibleAmount() == 10.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getVisibleAmount() == 10.0d);
			
	}
	
	/* 
	 * mamy 1500 ludzi, pasek ustawiony na 1000.
	 * budujemy 500 wlocznikow. kazdy wlocznik je jak normalny czlowiek,
	 * wiec produkcja wlocznikow nie powinna wplynac na gospodarke jedzenia, tzn.
	 * stan jedzenia powinien sie nie zmienic.
	 * po wyprodukowaniu 500 wlocznikow w miescie powinno byc 1000 ludzi , 500 wlocznik i 
	 * stan jedzenia bez zmian.
	 */
	@Test
	public void test2_getFoodAt() throws Exception
	{
		/*
		 * opis testu:
		 * jedzenie produkowane jest w sumie 1500 /h.
		 * Ludzi jest 1500. Pasek ustawiony na 1000.
		 * armia: 0
		 * stan jedzenia :
		 * woda: 10
		 * ryz: 10
		 * pork: 10.
		 */
		ucc.getPoint().getCityLode(StockType.water).setValue(5);
		ucc.getPoint().getCityLode(StockType.rice).setValue(5);
		ucc.getPoint().getCityLode(StockType.pork).setValue(5);
		
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.gold).setValue(1);
		ucc.getPoint().getCityLode(StockType.stone).setValue(1);
		ucc.getPoint().getCityLode(StockType.iron).setValue(1);
		ucc.getPoint().getCityLode(StockType.wood).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(10.0d);
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(1000.0d);
		ucc.setMaxLudzi(1000);
		
		ucc.getBuilding(5).setLevel(1);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		NotFoodSet_dead cost = config.getById(10).getPrice(ucc);
		for(int i=0;i<500;i++)
		{
			ucc.getMyResources().addNotFoodSet(cost);
		}
		
		assertTrue(ucc.getBuilding(5).buildUnit(10, 500) == 500);
		
		CityBuilding cb = ucc.getBuilding(5);
		cb.setLevel(1);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//water
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(200);//gold
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(201);//stone
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(214);//iron
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(206);//wood
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
	
		
		/*
		 * czas po 24 godzinach.
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeNs = 1000l*1000l*1000l*60l*60l*24l;
		//double afterTimeH = TimeTranslation.msToHours(afterTimeMs);
		
		//afterTimeH = 0.12800;
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeNs, new Integer(0));
		long stop = System.currentTimeMillis();
		System.out.print("\nilosc ludzi w miescie: " + ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount() + "\n");
		assertTrue(ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount() == 1000.0d);
		System.out.print("ilosc ryzu:" +ucc.getMyFood().getFood(StockType.rice).getVisibleAmount() + "\n");
		System.out.print("ilosc armi: " + ucc.getArmy().getUnitGroupById(10).getVisibleAmount()+ "\n");
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getVisibleAmount() == 10.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getVisibleAmount() == 10.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getVisibleAmount() == 10.0d);
		
		assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 500);
		assertTrue(ucc.getTrainedPeople() == 0.0d);
			
	}
	@Test
	public void test3_getFoodAt() throws Exception
	{
		
		ucc.getPoint().getCityLode(StockType.water).setValue(5);
		ucc.getPoint().getCityLode(StockType.rice).setValue(5);
		ucc.getPoint().getCityLode(StockType.pork).setValue(5);
		
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.gold).setValue(1);
		ucc.getPoint().getCityLode(StockType.stone).setValue(1);
		ucc.getPoint().getCityLode(StockType.iron).setValue(1);
		ucc.getPoint().getCityLode(StockType.wood).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(10.0d);
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(1000.0d);
		ucc.setMaxLudzi(1000);
		
		ucc.getBuilding(5).setLevel(1);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		NotFoodSet_dead cost = config.getById(10).getPrice(ucc);
		for(int i=0;i<500;i++)
		{
			ucc.getMyResources().addNotFoodSet(cost);
		}
		
		assertTrue(ucc.getBuilding(5).buildUnit(10, 500) == 500);
		
		CityBuilding cb = ucc.getBuilding(5);
		cb.setLevel(1);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//water
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(200);//gold
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(201);//stone
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(214);//iron
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(206);//wood
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		
		
		/*
		 * czas po 24 godzinach.
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeNs = 1000l*1000l*1000l*60l;
		//double afterTimeH = TimeTranslation.msToHours(afterTimeMs)
		
		//afterTimeH = 0.12800;
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeNs, new Integer(0));
		long stop = System.currentTimeMillis();
		System.out.print("\nilosc ludzi w miescie: " + ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount() + "\n");
		assertTrue(ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount() == 1000.0d);
		System.out.print("ilosc ryzu:" +ucc.getMyFood().getFood(StockType.rice).getVisibleAmount());
		assertTrue(ucc.getMyFood().getFood(StockType.rice).getVisibleAmount() == 10.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.water).getVisibleAmount() == 10.0d);
		assertTrue(ucc.getMyFood().getFood(StockType.pork).getVisibleAmount() == 10.0d);
		System.out.print("ilosc zolnierzy w miescie: " + ucc.getArmy().getUnitGroupById(10).getVisibleAmount() + "\n");
		assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 12);
		System.out.print("szkoleni ludzie:" + ucc.getTrainedPeople() + "\n");
		assertTrue(ucc.getTrainedPeople() == 488.0d);
			
	}
	@Test
	public void test4_getFoodAt() throws Exception
	{
		
		ucc.getPoint().getCityLode(StockType.water).setValue(5);
		ucc.getPoint().getCityLode(StockType.rice).setValue(5);
		ucc.getPoint().getCityLode(StockType.pork).setValue(5);
		
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.gold).setValue(1);
		ucc.getPoint().getCityLode(StockType.stone).setValue(1);
		ucc.getPoint().getCityLode(StockType.iron).setValue(1);
		ucc.getPoint().getCityLode(StockType.wood).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(10.0d);
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(1500.0d);
		ucc.setMaxLudzi(1500);
		
		ucc.getBuilding(5).setLevel(1);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		NotFoodSet_dead cost = config.getById(10).getPrice(ucc);
		for(int i=0;i<10;i++)
		{
			ucc.getMyResources().addNotFoodSet(cost);
		}
		
		assertTrue(ucc.getBuilding(5).buildUnit(10, 10) == 10);
		
		CityBuilding cb = ucc.getBuilding(5);
		cb.setLevel(1);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//water
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(200);//gold
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(201);//stone
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(214);//iron
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(206);//wood
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		
		
		/*
		 * czas po 24 godzinach.
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeNs = 1000l*1000l*1000l*30l;
		double afterTimeH = TimeTranslation.nsToHours(afterTimeNs);
		
		//afterTimeH = 0.12800;
		long start = System.currentTimeMillis();
		ucc.initFoodFlags();
		double startBr = ucc.getBirthRatePerHour();
		double startPeople = ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount();
		
		ccu.getFoodAT(ucc, afterTimeNs, new Integer(0));
		long stop = System.currentTimeMillis();
		System.out.print("\nilosc ludzi w miescie: " + ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount() + "\n");
		
		System.out.print("ucc birthrate: " + ucc.getBirthRatePerHour()+ "\n");
		System.out.print("ucc ilosc ludzi: " + ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount()+ "\n");
		System.out.print("ile powinno byc: " + ((ucc.getBirthRatePerHour() * afterTimeH) + ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount())+ "\n");
		assertTrue(ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount() == startBr * afterTimeH + startPeople);
		System.out.print("ilosc ryzu:" +ucc.getMyFood().getFood(StockType.rice).getVisibleAmount());
//		assertTrue(ucc.getMyFood().getFood(ResourceType.rice).getCount() == 10.0d);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.water).getCount() == 10.0d);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.pork).getCount() == 10.0d);
		System.out.print("ilosc zolnierzy w miescie: " + ucc.getArmy().getUnitGroupById(10).getVisibleAmount() + "\n");
		assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 6);
		System.out.print("szkoleni ludzie:" + ucc.getTrainedPeople() + "\n");
		assertTrue(ucc.getTrainedPeople() == 4.0d);
			
	}
	
	@Test
	public void test5_getFoodAt() throws Exception
	{
		/*
		 * 
		 */
		ucc.getPoint().getCityLode(StockType.water).setValue(5);
		ucc.getPoint().getCityLode(StockType.rice).setValue(5);
		ucc.getPoint().getCityLode(StockType.pork).setValue(5);
		
		ucc.getPoint().getCityLode(StockType.wheat).setValue(1);
		ucc.getPoint().getCityLode(StockType.beef).setValue(1);
		ucc.getPoint().getCityLode(StockType.gold).setValue(1);
		ucc.getPoint().getCityLode(StockType.stone).setValue(1);
		ucc.getPoint().getCityLode(StockType.iron).setValue(1);
		ucc.getPoint().getCityLode(StockType.wood).setValue(1);
		
		ucc.getMyFood().getFood(StockType.water).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.rice).setCount(10.0d);
		ucc.getMyFood().getFood(StockType.pork).setCount(10.0d);
		
		ucc.getMyResources().getResource(StockType.peopleCount).setCount(1500.0d);
		ucc.setMaxLudzi(1490);
		
		ucc.getBuilding(5).setLevel(1);
		
		UsersCityConfig config = SpringJSFUtil.getCivilizationHolder().getCivilization(ucc.getUserConfig().getCivil());
		config.getCivilConfig().setEatPerHour(1.0f);
		
		NotFoodSet_dead cost = config.getById(10).getPrice(ucc);
		for(int i=0;i<10;i++)
		{
			ucc.getMyResources().addNotFoodSet(cost);
		}
		
		assertTrue(ucc.getBuilding(5).buildUnit(10, 10) == 10);
		
		CityBuilding cb = ucc.getBuilding(5);
		cb.setLevel(1);
		
		ExtractCityBuilding ecb = (ExtractCityBuilding) ucc.getBuilding(203);//water
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(204);//rice
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(207);//pork
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(200);//gold
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(201);//stone
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(214);//iron
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		ecb = (ExtractCityBuilding) ucc.getBuilding(206);//wood
		ecb.setLevel(1);
		assertTrue(ecb.setPeople(100));
		
		
		
		/*
		 * czas po 24 godzinach.
		 */
		/*
		 * TODO doko�czy� test
		 */
		long afterTimeNs = 1000l*1000l*1000l*30l;
		double afterTimeH = TimeTranslation.nsToHours(afterTimeNs);
		
		//afterTimeH = 0.12800;
		long start = System.currentTimeMillis();
		ccu.getFoodAT(ucc, afterTimeNs, new Integer(0));
		long stop = System.currentTimeMillis();
		System.out.print("\nilosc ludzi w miescie: " + ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount() + "\n");
		System.out.print("ucc birthrate: " + ucc.getBirthRatePerHour()+ "\n");
		System.out.print("ucc ilosc ludzi: " + ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount()+ "\n");
		System.out.print("ile powinno byc: " + ((ucc.getBirthRatePerHour() * afterTimeH) + ucc.getMyResources().getResource(StockType.peopleCount).getVisibleAmount())+ "\n");
		//assertTrue(ucc.getMyResources().getResource(ResourceType.peopleCount).getCount() == ucc.getBirthRatePerHour() * afterTimeH + ucc.getMyResources().getResource(ResourceType.peopleCount).getCount());
		System.out.print("ilosc ryzu:" +ucc.getMyFood().getFood(StockType.rice).getVisibleAmount());
//		assertTrue(ucc.getMyFood().getFood(ResourceType.rice).getCount() == 10.0d);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.water).getCount() == 10.0d);
//		assertTrue(ucc.getMyFood().getFood(ResourceType.pork).getCount() == 10.0d);
		System.out.print("ilosc zolnierzy w miescie: " + ucc.getArmy().getUnitGroupById(10).getVisibleAmount() + "\n");
		assertTrue(ucc.getArmy().getUnitGroupById(10).getVisibleAmount() == 6);
		System.out.print("szkoleni ludzie:" + ucc.getTrainedPeople() + "\n");
		assertTrue(ucc.getTrainedPeople() == 4.0d);
		
		ucc.getBuilding(36).setLevel(1);
		ucc.getBuilding(36).buildUnit(37, 8);
		assertTrue(ucc.getTrainedPeople() == 12.0d);
		
		//afterTimeH = TimeTranslation.msToHours(1000*40);
		//UsersCityConfig ucc2 = ccu.getFoodAT(ucc, afterTimeH, new Integer(0));
		//assertTrue(ucc2.getArmy().getUnitGroupById(37).getCount() == 8.0d);
			
	}
}
