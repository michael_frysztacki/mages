package use_cases;

import entities.EntityFactory;
import entities.UserConfig;
import game_model_interfaces.IAllWorlds;
import game_model_interfaces.ICivilization;
import game_model_interfaces.IRadialResearchModel;
import game_model_interfaces.IWorldDefinition;

import java.util.List;

import map.CapitalPoint;
import map.MapService;
import map.UserGraph;
import map.WorldMap;

public abstract class GameEntrance {

	protected abstract MapService getMapService();
	protected abstract IAllWorlds getAllWorlds();
	protected abstract EntityFactory getEntityFactory();
	public void registerPlayer(String nick, String password, int worldId, String civilId, String languageId) {
		
		UserConfig newPlayer = createPlayer(worldId, civilId);
		CapitalPoint newCapital = randomizePoint(worldId, civilId);
		IRadialResearchModel landRadialResearch = getRadialLandResearchModel(worldId, civilId);
		UserGraph landGraph = createInitialGraph(worldId, newPlayer, newCapital, landRadialResearch);
		if(newCapital.isWaterCity()) {
			IRadialResearchModel waterRadialResearch = getRadialWaterResearchModel(worldId, civilId);
			UserGraph waterGraph = createInitialGraph(worldId, newPlayer, newCapital, waterRadialResearch);
		}
	}
		private IRadialResearchModel getRadialWaterResearchModel(int worldId, String civilId) {
			return null;
		}
		private UserConfig createPlayer(int worldId, String civilId) {
			return getEntityFactory().createUserConfig(getCivilization(worldId, civilId));
		}
		private UserGraph createInitialGraph(int worldId, UserConfig newPlayer,
				CapitalPoint newCapital, IRadialResearchModel landRadialResearch) {
			return getWorldMap(worldId).createInitialGraph(landRadialResearch.getRadius(newPlayer), newCapital.getPointId());
		}
		private CapitalPoint randomizePoint(int worldId, String civilId) {
			return getWorldMap(worldId).randomizePoint(getRecursivelyAllCivilId(civilId));
		}
			private IRadialResearchModel getRadialLandResearchModel(int worldId, String civilId) {
				return getCivilization(worldId, civilId).getLandRadialResearch();
			}
				private ICivilization getCivilization(int worldId, String civilId) {
					IWorldDefinition wd = getAllWorlds().getWorldModel(worldId);
					ICivilization myCivil = wd.getCivilization(civilId);
					return myCivil;
				}
			private WorldMap getWorldMap(int worldId) {
				return getMapService().getWorldMap(worldId);
			}
		private List<Integer> getRecursivelyAllCivilId(String civilId) {
			return null;
		}
}
