package use_cases;

import entities.UsersCityConfig;
import entities_gateways.CityGateway;
import map.MapService;

public abstract class ResearchInteractor {
	
	public abstract MapService getMapService();
	public abstract CityGateway getCityGateway();
	public void upgradeLandRadialResearch(int cityId) {
		UsersCityConfig city = getCityGateway().fetchCity(cityId);
		
	}

}
