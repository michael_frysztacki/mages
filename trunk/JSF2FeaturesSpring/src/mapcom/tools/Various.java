package mapcom.tools;

import java.util.ArrayList;
import mapcom.math.Auxiliary;
import mapcom.worldstructure.TerrainType;

public class Various
{
	public static boolean areOfTheSameClass(Object A, Object B)
	{		
		return A.getClass() == B.getClass();
	}
	
	public static ArrayList<Double> parseALStringToALDouble(ArrayList<String> stringData)
	{
		ArrayList<Double> doubleData = new ArrayList<Double>();
		for(String str: stringData)
		{
			doubleData.add( Double.parseDouble(str) );
		}
		return doubleData;
	}
	public static ArrayList<Boolean> parseALStringToALBoolean(ArrayList<String> stringData)
	{
		ArrayList<Boolean> booleanData = new ArrayList<Boolean>();
		for(String str: stringData)
		{
			booleanData.add( Boolean.parseBoolean(str)  );
		}
		return booleanData;
	}
	public static ArrayList<Integer> parseALStringToALInteger(ArrayList<String> stringData)
	{
		ArrayList<Integer> integerData = new ArrayList<Integer>();
		for(String str: stringData)
		{
			integerData.add( Integer.parseInt(str)  );
		}
		return integerData;
	}
	public static float[] getInversedFloatArray(float[] Array)
	{
		int i;
		float[] inversedArray = new float[Array.length];
		for(i = 0; i<Array.length; i++)
		{
			inversedArray[i] = Array[Array.length-1-i];
		}
		return inversedArray;
	}
	public static TerrainType[] getInversedTerrainTypeArray(TerrainType[] Array)
	{
		int i;
		TerrainType[] inversedArray = new TerrainType[Array.length];
		for(i = 0; i<Array.length; i++)
		{
			inversedArray[i] = Array[Array.length-1-i];
		}
		return inversedArray;
	}
	public static double[] getInvDoubleArray(double[] Array)
	{
		int i;
		double[] inversedArray = new double[Array.length];
		for(i = 0; i<Array.length; i++)
		{
			inversedArray[i] = Array[Array.length-1-i];
		}
		return inversedArray;
	}
	
	
	//zwraca tablice liczb binarnych od 0 do 2^n  np: dla n=4; (dla przejrzystosci przykladu: false - 0; true - 1)
	// 0 0 0 0 
	// 1 0 0 0 
	// 0 1 0 0
	// 1 1 0 0 
	// 0 0 1 0
	// 1 0 1 0
	// 0 1 1 0
	// 1 1 1 0
	// 0 0 0 1
	// 1 0 0 1
	// 0 1 0 1
	// 1 1 0 1
	// 0 0 1 1
	// 1 0 1 1
	// 0 1 1 1
	// 1 1 1 1
	public static boolean[][] createTableOfNumbersRepresentedInBinarySystem(int n)
	{		
		if( n == 0)
			throw new java.lang.UnsupportedOperationException();
		int i, j, k;
		int temp1, temp2;
		int numberOfRows = (int) Auxiliary.numberToIntegerPower(2.0, n);
		boolean[][] arrayOfBinNumbers = new boolean[numberOfRows][n];
		for(i = 0; i<n; i++)
		{			
			temp1 = (int)Auxiliary.numberToIntegerPower(2,i);
			temp2 = (int)Auxiliary.numberToIntegerPower(2, i+1);
			for(j = 0; j<numberOfRows; j += temp2 )
			{				
				for(k = j; k<j + temp1; k++)
				{
					arrayOfBinNumbers[k][i] = false;
				}
				for(k = j + temp1; k<j + 2*temp1; k++)
				{
					arrayOfBinNumbers[k][i] = true;
				}	
			}			
		}
		return arrayOfBinNumbers;
	}	
	// jako argumenty wchodza 2 tablice przynaleznosci do zbiorow np.
	// {true, true, false ,true}
	// {false, false, false, true}
	public static boolean ifArraysHaveAtLeastOneTrueOnSameIndex(boolean[] array1, boolean[] array2)
	{				
		for(int i = 0; i<array1.length; i++ )		
			if( array1[i] && array2[i] )			
				return true;	
		return false;
	}
}
