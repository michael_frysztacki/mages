package mapcom.tools.fileproc;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

public class TxtFileProcessingTest {

	String pathToTestFiles = "src/mapcom/tools/fileproc/FilePerformerTestFiles/";
	
	// 0010 
	@Test
	public void readFileWithTwoUTF8Lines() throws IOException 
	{
		ArrayList<String> lines = TxtFileProcessing.readLinesFromFile(pathToTestFiles + "0010/testFile.txt");
		
		ArrayList<String> results = new ArrayList<String>();
		results.add("Ж");
		results.add("狗");
		
		assertEquals(lines, results);
	}
	
	// 0020 	
	@Test
	public void readLinesFromEmptyFile() throws IOException 
	{
		ArrayList<String> lines = TxtFileProcessing.readLinesFromFile(pathToTestFiles + "0020/testFile.txt");		
		assertEquals(0, lines.size());
	}

	// 0030 
	@Test
	public void readFileWithPolishSpanishFrenchRussianItalianChineseUTF8Characters() throws IOException 
	{
		ArrayList<String> lines = TxtFileProcessing.readLinesFromFile(pathToTestFiles + "0030/testFile.txt");
		
		ArrayList<String> results = new ArrayList<String>(); 
		results.add("żźąłęć");
		results.add("ñé¿?");
		results.add("ùàçéè");
		results.add("фвачси");
		results.add("òèàùì");
		results.add("我");
		
		assertEquals(lines, results);
	}
	
	// 0040 
	@Test
	public void readFileWithThreeAsciiLines() throws IOException 
	{
		ArrayList<String> lines = TxtFileProcessing.readLinesFromFile(pathToTestFiles + "0040/testFile.txt");
		
		ArrayList<String> results = new ArrayList<String>(); 
		results.add("abcd");
		results.add("efgh");
		results.add("ijkl");		
		
		assertEquals(lines, results);
	}
	
	// 0100 
	@Test
	public void saveEmptyZeroLinesToFile() throws IOException
	{
		String testFileDirectory = pathToTestFiles + "0100/TestResult/testFile.txt";
		ArrayList<String> linesToSave = new ArrayList<String>();
		TxtFileProcessing.saveLinesToFile(linesToSave, testFileDirectory);
		ArrayList<String> readLines = TxtFileProcessing.readLinesFromFile(testFileDirectory);
		assertEquals(0, readLines.size());
	}
	// 0110 
	@Test
	public void savePolishSpanishFrenchRussianItalianChineseUTF8Lines() throws IOException
	{
		String testFileDirectory = pathToTestFiles + "0110/TestResult/testFile.txt";
		ArrayList<String> linesToSave = new ArrayList<String>();
		linesToSave.add("żźąłęć");
		linesToSave.add("ñé¿?");
		linesToSave.add("ùàçéè");
		linesToSave.add("фвачси");
		linesToSave.add("òèàùì");
		linesToSave.add("我");
		TxtFileProcessing.saveLinesToFile(linesToSave, testFileDirectory);
		
		ArrayList<String> readLines = TxtFileProcessing.readLinesFromFile(testFileDirectory);
		assertEquals(linesToSave, readLines);
	}
	// 0120
	@Test
	public void saveAsciiLines() throws IOException
	{
		String testFileDirectory = pathToTestFiles + "0120/TestResult/testFile.txt";
		ArrayList<String> linesToSave = new ArrayList<String>();
		linesToSave.add("abcd");
		linesToSave.add("efgh");
		linesToSave.add("ijkl1234567890");
		TxtFileProcessing.saveLinesToFile(linesToSave, testFileDirectory);
		
		ArrayList<String> readLines = TxtFileProcessing.readLinesFromFile(testFileDirectory);
		assertEquals(linesToSave, readLines);
	}
}
