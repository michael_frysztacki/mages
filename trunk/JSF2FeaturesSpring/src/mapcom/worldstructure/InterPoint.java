package mapcom.worldstructure;

public class InterPoint extends Point 
{

	public InterPoint(double longit, double lat, int id, boolean isWater,
			boolean isPolar) {
		super(longit, lat, id, isWater, isPolar);
	}
	
	@Override
	public String toString()
	{
		return "InterPoint: " + dataToString();
	}

}
