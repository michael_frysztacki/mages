package mapcom.worldstructure;

public class City extends Point
{
	
	private static final long serialVersionUID = 3L;
	
	public City(double longit, double lat, int id, boolean isWater,
			boolean isPolar) {
		super(longit, lat, id, isWater, isPolar);
	}
	
	@Override
	public String toString()
	{
		return "City: " + dataToString();
	}
}
