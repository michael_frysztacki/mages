package mapcom.worldstructure;

import java.io.Serializable;

import mapcom.math.PointOnWorldSphere;

public class Point extends PointOnWorldSphere implements Serializable 
{
	private static final long serialVersionUID = 2L;
	
	final private boolean isWater;
	final private boolean isPolar;
	final private int id;

	public Point(double longit, double lat, int id, boolean isWater, boolean isPolar)
	{
		super(longit, lat);
		this.isWater = isWater;
		this.isPolar = isPolar;
		this.id = id;			
	}
	
	@Override
	public String toString()
	{
		return "Point: " + dataToString();		
	}
	protected String dataToString()
	{
		String s = "id=" + id + "; (" + longitude +", " + latitude + ");";
		s += isWater ? " water; ": " land;";
		s += isPolar ? "polar;": "not polar;";
		return s;
	}
	public boolean isWater()
	{
		return isWater;
	}
	public boolean isPolar()
	{
		return isPolar;
	}
	public int getId()
	{
		return id;
	}
	public double getLat()
	{
		return latitude;
	}
	public double getLongit()
	{
		return longitude;
	}		
	
	public Point createAntipodalPoint()
	{
		return new Point(-longitude, -latitude, id, isWater, isPolar);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + id;
		result = prime * result + (isPolar ? 1231 : 1237);
		result = prime * result + (isWater ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (id != other.id)
			return false;
		if (isPolar != other.isPolar)
			return false;
		if (isWater != other.isWater)
			return false;
		return true;
	}
}

