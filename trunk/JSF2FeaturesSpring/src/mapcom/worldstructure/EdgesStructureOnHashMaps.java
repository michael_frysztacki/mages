package mapcom.worldstructure;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;

import mapcom.tools.EmptyIterator;

public class EdgesStructureOnHashMaps implements EdgesStructure, Serializable
{	
	private static final long serialVersionUID = 111L;
	
	protected HashMap<Integer,HashMap<Integer,Edge>> edges = new HashMap<Integer,HashMap<Integer,Edge>>();
	
	@Override
	public Edge getEdge(int startId, int endId)
	{	
		if( exists(startId, endId) )
			return this.edges.get(startId).get(endId);
		return null;
	}
	
	@Override
	public boolean exists(int startId, int endId)
	{
		if ( edges.containsKey(startId) )
			if ( edges.get(startId).containsKey(endId) )
				return true;
		return false;
	}

	@Override
	public Iterator<Edge> innerIterator(int pointId)
	{
		if(edges.containsKey(pointId))
			return edges.get(pointId).values().iterator();
		return new EmptyIterator<Edge>();
	}

	@Override
	public void addEdgeAndReversedEdge(Edge edge)
	{
		addSingleEdge(edge);
		addSingleEdge(edge.createReversedEdge());
	}
		private void addSingleEdge(Edge edge)
		{
			int startPointId = edge.getStartPoint().getId();
			int endPointId =  edge.getEndPoint().getId();;
			if( !edges.containsKey(startPointId) )
				edges.put( startPointId, new HashMap<Integer,Edge>() );
			edges.get(startPointId).put(endPointId, edge);
		}

	@Override
	public void removeEdgeAndReversedEdge(int startId, int endId)
	{
		removeSingleEdge(startId, endId);
		removeSingleEdge(endId, startId);
	}
		private void removeSingleEdge(int startId, int endId)
		{
			if( exists(startId, endId) )
				edges.get(startId).remove(endId);
			if(edges.containsKey(startId))
				if( edges.get(startId).size() == 0 )
					edges.remove(startId);	
		}
		
	@Override
	public Iterator<Iterator<Edge>> outerIterator()
	{
		return new outerIter();
	}

	private class  outerIter implements Iterator<Iterator<Edge>>
	{
		Iterator<HashMap<Integer, Edge>> outerIter;
		
		private outerIter()
		{
			outerIter = edges.values().iterator();
		}
		@Override
		public boolean hasNext()
		{
			return outerIter.hasNext();
		}
		@Override
		public Iterator<Edge> next()
		{
			HashMap<Integer, Edge> innerHM = outerIter.next();
			return innerHM.values().iterator();			
		}
		@Override
		public void remove()
		{
			
		}		
	}
	

}



