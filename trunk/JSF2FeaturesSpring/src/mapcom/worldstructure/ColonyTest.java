package mapcom.worldstructure;

import static org.junit.Assert.*;
import org.junit.Test;

public class ColonyTest {

	@Test
	public void toStringTesT() {
		Colony colony = new Colony(0.5, 1.5, 2, true, false);
		assertEquals("Colony: id=2; (0.5, 1.5); water; not polar;", colony.toString());
	}
	@Test
	public void compareColoniesWithSlightlyDifferentLongitudesTest(){
		Colony A = new Colony(0.5, 1.5, 2, true, false);
		Colony B = new Colony(0.5000001, 1.5, 2, true, false);
		assertFalse( A.equals(B) );
	}
	@Test
	public void compareColoniesWithSlightlyDifferentLatitudesTest(){
		Colony A = new Colony(0.5, 1.5, 2, true, false);
		Colony B = new Colony(0.5, 1.500000000001, 2, true, false);
		assertFalse( A.equals(B) );
	}
	@Test
	public void compareTheSameColoniesTest(){
		Colony A = new Colony(0.5, 1.5, 2, true, false);
		Colony B = new Colony(0.5, 1.5, 2, true, false);
		assertTrue( A.equals(B) );
	}
}
