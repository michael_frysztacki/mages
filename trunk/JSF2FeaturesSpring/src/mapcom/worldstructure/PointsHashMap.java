package mapcom.worldstructure;


import java.util.HashMap;
import java.util.Iterator;

public class PointsHashMap implements PointsStructure
{
	private HashMap<Integer,Point> allPoints;
	
	@Override
	public PointsStructure createEmptyPointsStructure(){
		return new PointsHashMap();	
	}
	
	public PointsHashMap(){
		allPoints = new HashMap<Integer, Point>();	
	}
	
	@Override
	public PointsStructure[] createArrayOfEmptyPointsStructures(int size){
		return new PointsHashMap[size];	
	}
	
	
	@Override
	public Point getPointWithId(int id)
	{
		if( allPoints.get(id) == null )
			return null;
		else
			return allPoints.get(id);	
	}
		
	@Override
	public int size(){
		return allPoints.size();	
	}
	
	@Override
	public Iterator<Point> iterator(){
		return allPoints.values().iterator();	
	}
	
	@Override
	public void addPoint(Point toAdd){	
		allPoints.put(toAdd.getId(), toAdd);
	}
	
	@Override
	public void remove(int id){
		allPoints.remove(id);	
	}


	


}
