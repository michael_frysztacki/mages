package mapcom.worldstructure;

public abstract class Graph {
	
	PointsStructure points;
	EdgesStructure edges;

	public PointsStructure getPoints() {
		return this.points;
	}
	public EdgesStructure getEdges() {
		return this.edges;
	}
}
