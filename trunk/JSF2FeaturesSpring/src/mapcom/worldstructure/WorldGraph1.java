package mapcom.worldstructure;

import mapcom.mapprojections.MapProjection;
import mapcom.worldconstruction.EdgesStructureBuilder;
import mapcom.worldconstruction.PointsStructureExtractor;
import mapcom.worldconstruction.PointsStructureLoader;

public class WorldGraph1	extends WorldGraph
{

	WorldGraph1(String pointsDirectory, MapProjection proj)
	{	
		super(loadPointsStructure(pointsDirectory), createWaterEdgesFromPoints(ps, proj));						
	}		
	WorldGraph1(PointsStructure ps, EdgesStructure es)
	{	
		super(ps, es);						
	}
	
		private static PointsStructure loadPointsStructure(String pointsDirectory)
		{
			PointsStructureLoader psl = new PointsStructureLoader();
			return psl.load(ps, pointsDirectory);
		}
		
		private static EdgesStructure createWaterEdgesFromPoints(PointsStructure ps, MapProjection proj)
		{
			EdgesStructureBuilder esb = new EdgesStructureBuilder();
			EdgesStructure es = new ImmutableEdgesStructureOnHashMaps();
			return esb.build(es, proj, ps);
		}
		

	
}
