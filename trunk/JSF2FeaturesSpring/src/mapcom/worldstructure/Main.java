package mapcom.worldstructure;

import java.io.IOException;

import mapcom.imgproc.Auxiliary;
import mapcom.imgproc.Colors;
import mapcom.imgproc.WorldDrawing;
import mapcom.mapprojections.Equirectangular;
import mapcom.mapprojections.MapProjection;
import mapcom.worldconstruction.EdgesStructureBuilder;
import mapcom.worldconstruction.EdgesStructureLoader;
import mapcom.worldconstruction.EdgesStructureSaver;
import mapcom.worldconstruction.LandChecking;
import mapcom.worldconstruction.PointsStructurePerformer;
import mapcom.worldconstruction.PointsStructureExtractor;
import mapcom.worldconstruction.PointsStructureLoader;
import mapcom.worldconstruction.PointsStructureSaver;
import mapcom.worldconstruction.PointsVerifier;
import mapcom.worldconstruction.WaterChecking;

public class Main
{

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException
	{
		
		/**************************************************************************************************************
		 *  Initializing
		 **************************************************************************************************************/
		
		MapProjection proj = new Equirectangular("World Graph Files/terrains.png");
		PointsStructureLoader pointsLoader = new PointsStructureLoader();
		PointsHashMap allPoints = new PointsHashMap();
		allPoints = (PointsHashMap) pointsLoader.load(allPoints, "World Graph Files/Points/");
		
//		PointDrawing.drawPoints(allPoints, proj);
//		Auxiliary.saveImage( proj.getImg(), "points.png" );
		
		PointsStructureExtractor pointsBuilder = new PointsStructureExtractor();
		PointsHashMap waterPoints = (PointsHashMap) pointsBuilder.extractPointForWaterGraph(allPoints);
		PointsHashMap landPoints = (PointsHashMap) pointsBuilder.extractPointsForLandGraph(allPoints);
		
		PointsVerifier verifier = new PointsVerifier();
		PointsStructurePerformer performer = new PointsStructurePerformer();

		
		
		PointsStructureSaver pointsSaver = new PointsStructureSaver();
//		pointsSaver.save(allPoints, "World Graph Files/New Points/");
		
		EdgesStructureBuilder edgesBuilder = new EdgesStructureBuilder();
		EdgesStructure landEdges = new ImmutableEdgesStructureOnHashMaps();
		EdgesStructure waterEdges = new ImmutableEdgesStructureOnHashMaps();
		
		
		System.out.println( verifier.isCorrectPointsStructure(allPoints, proj) ); 
		
		long startTime;		
		long endTime;
		long totalTime;
		
		boolean buildLandEdges = false;
		boolean buildWaterEdges = false;
		boolean saveLandEdges = false;		
		boolean saveWaterEdges = false;
		boolean loadLandEdges = false;
		boolean loadWaterEdges = false;
		boolean drawLandEdges = false;
		boolean drawWaterEdges = false;
		
		/**************************************************************************************************************
		 *  CREATING EDGES
		 **************************************************************************************************************/
		if( buildLandEdges )
		{
			startTime = System.currentTimeMillis();		
			
			landEdges = edgesBuilder.build(landEdges, proj, landPoints, new LandChecking());
			endTime   = System.currentTimeMillis();
			totalTime = endTime - startTime;
			System.out.println("Creating Land Edges after: " + (double)totalTime/1000.0/3600.0 + " hours.");
		}
		
		if( buildWaterEdges )
		{
			startTime = System.currentTimeMillis();		
			waterEdges =  edgesBuilder.build(waterEdges, proj, waterPoints, new WaterChecking());	
			endTime   = System.currentTimeMillis();
			totalTime = endTime - startTime;
			System.out.println("Creating Water Edges after: " + (double)totalTime/1000.0/3600.0 + " hours.");
		}

		/**************************************************************************************************************
		 *  EDGES SAVING
		 **************************************************************************************************************/
		
		EdgesStructureSaver edgesSaver = new EdgesStructureSaver();

		if(saveLandEdges)
		{
			startTime = System.currentTimeMillis();		
			edgesSaver.saveEdges(landEdges, "World Graph Files/Edges/Land Edges/");
			endTime   = System.currentTimeMillis();
			totalTime = endTime - startTime;
			System.out.println("Saving Land Edges after " + Math.round((double)totalTime/1000.0) + " seconds." );
		}
		
		if(saveWaterEdges)
		{
			startTime = System.currentTimeMillis();		
			edgesSaver.saveEdges(waterEdges, "World Graph Files/Edges/Water Edges/");
			endTime   = System.currentTimeMillis();
			totalTime = endTime - startTime;
			System.out.println("Saving Water Edges after " + Math.round((double)totalTime/1000.0) + " seconds." );
		}
		
		/**************************************************************************************************************
		 *  EDGES LOADING
		 **************************************************************************************************************/		
		
		EdgesStructureLoader loader = new EdgesStructureLoader();
		
		if(loadLandEdges)
		{
			startTime = System.currentTimeMillis();		
			loader.load(landEdges, "World Graph Files/Edges/Land Edges/");		
			endTime   = System.currentTimeMillis();
			totalTime = endTime - startTime;
			System.out.println("Loading Land Edges after " + Math.round((double)totalTime/1000.0) + " seconds." );
		}
		
		if(loadWaterEdges)
		{
			startTime = System.currentTimeMillis();		
			loader.load(waterEdges, "World Graph Files/Edges/Water Edges/");		
			endTime   = System.currentTimeMillis();
			totalTime = endTime - startTime;
			System.out.println("Loading Water Edges after " + Math.round((double)totalTime/1000.0) + " seconds." );
		}
		
		/**************************************************************************************************************
		 *  EDGES DRAWING
		 **************************************************************************************************************/		
		if(drawLandEdges)
		{
			proj = new Equirectangular("World Graph Files/terrains.png");		
			startTime = System.currentTimeMillis();		
			WorldDrawing.drawEdges(proj, landEdges, landPoints, 0, Colors.violet);		
			endTime   = System.currentTimeMillis();
			totalTime = endTime - startTime;
			System.out.println("Drawing Land Edges after " + Math.round((double)totalTime/1000.0) + " seconds." );		
			Auxiliary.saveImage( proj.getImg(), "landEdges.png" );
		}
		
		if(drawWaterEdges)
		{
			proj = new Equirectangular("World Graph Files/terrains.png");		
			startTime = System.currentTimeMillis();		
			WorldDrawing.drawEdges(proj, waterEdges, waterPoints, 0, Colors.darkblue);		
			endTime   = System.currentTimeMillis();
			totalTime = endTime - startTime;
			System.out.println("Drawing Water Edges after " + Math.round((double)totalTime/1000.0) + " seconds." );		
			Auxiliary.saveImage( proj.getImg(), "waterEdges.png" );
		}
		
	}
}








