package mapcom.worldstructure;

import mapcom.worldconstruction.SurfaceCheckingType;
import mapcom.worldconstruction.WorldGraphBuilder;

final public class WorldGraph extends Graph{
	
	final protected PointsStructure points;
	final protected EdgesStructure edges;
	final SurfaceCheckingType surfaceCheckingType;	
	
	WorldGraph(WorldGraphBuilder graphBuilder){
		points = graphBuilder.getPoints();
		edges = graphBuilder.getEdges();
		surfaceCheckingType = graphBuilder.getSurfaceCheckingType();
	}
}
