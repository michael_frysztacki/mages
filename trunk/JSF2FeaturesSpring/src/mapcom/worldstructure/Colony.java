package mapcom.worldstructure;



public class Colony extends City 
{
	private static final long serialVersionUID = 5L;

	public Colony(double longit, double lat, int id, boolean isWater, boolean isPolar) {
		super(longit, lat, id, isWater, isPolar);
	}
	
	@Override
	public String toString()
	{
		return "Colony: " + dataToString();
	}
	

}
