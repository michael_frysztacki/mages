package mapcom.worldstructure;


public class Capital extends City
{
	private static final long serialVersionUID = 4L;
	private String name;
	
	public Capital(double longit, double lat, int id, String name, boolean isWater, boolean isPolar)
	{
		super(longit, lat, id, isWater, isPolar);
		this.name = name; 
	}
	public String getName()
	{
		return name;
	}
	@Override
	public String toString()
	{
		return "Capital: name=" + name + "; " + dataToString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Capital other = (Capital) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
};
