package mapcom.worldstructure;

import java.util.Iterator;

// przetrzymuje edge skierowane, ale w grafie istnieje pewnego rodzaju symetria, mianowicie
// Jesli struktura zawiera krawedz z pkt A do pkt B, to zawiera rowniez krawedz z pkt B do pkt A
// Nie mniej jednak w nazwie nie widnieje nigdzie slowo symmetric, aby nie mylic z Grafem Symetrycznym, 
// ktorego scisla definicja matematyczna jest inna od powyzszej

public interface EdgesStructure
{			
	// should return null if a given edge does not exist;
	public abstract Edge getEdge(int startId, int endId);	
	
	public abstract Iterator<Edge> innerIterator(int pointId);
	public abstract Iterator<Iterator<Edge>> outerIterator();
	
	public abstract boolean exists(int point1Id, int point2Id);
	
	public abstract int numberOfAllEdges();
	public abstract int numberOfEdgesStartingInPoint(int pointId);
	
}
