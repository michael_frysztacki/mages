package mapcom.worldstructure;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import mapcom.tools.Various;
import mapcom.worldconstruction.PointsStructureExtractor;
import mapcom.worldconstruction.SurfaceCheckingType;
import mapcom.math.Auxiliary;
import mapcom.math.GeometryCalc;


final class UserGraph extends Graph
{	
	WorldGraph worldGraph;
	ArrayList<Point> userCities;
	
	
	UserGraph(WorldGraph worldGraph, Point startingCity, int navRadiusKm){
		this.worldGraph = worldGraph;
		
		extractPoints(worldGraph.getPoints(), new Point[]{startingCity},  navRadiusKm);		
		extendEdges((Point[])userCities.toArray(), navRadiusKm, 0);
	}
		private void extractPoints(PointsStructure worldPoints, Point[] userCities,  int naviRadiusKm)
		{		
			PointsStructureExtractor e = new PointsStructureExtractor();
			this.points = e.extractPointsInUserRange(worldPoints, userCities, naviRadiusKm);
		}

	private void extendEdges(Point[] userCities, int oldNavRadiusKm, int newNavRadiusKm)
	{
		edges = new EdgesStructureOnHashMaps();
		Subsets subsets = new Subsets(points, userCities, newNavRadiusKm);			
	}

	private void addEdgesFromSubsets(Subsets subsets, PointsStructure userCities, double navRadiusKm)
	{		
		// Ponizej dwie petle iterujace po subsetach tak, aby zadna para subsetow nie zostala uwzgledniona dwa razy.
		// Pierwsza petla iterujaca po subsetach
		for(int i = 0; i < subsets.size(); i++)
		{
			// druga petla iterujaca po subsetach
			for(int j = 0; j <= i; j++)
			{					
				// Czy istnieje taki okrag nawigacji, ze obejmuje obydwa subsety...
				if( subsets.existsAtLeastOneNavigationCenterInOfWhichRangeTwoSubsetsAre(i, j) )									
					addEdgesWithEndsInSubsetsIfTheyExistsInTheWorldGraph(subsets.getSubset(i), subsets.getSubset(j), 
							worldGraph.getEdges());
				else 
					addEdgesWithEndsInSubsetsIfTheyExistsInTheWorldGraphAfterCheckingIntersections(subsets.getSubset(i), 
							subsets.getSubset(j), worldGraph.getEdges());
			}
		}
	}	
	private void addEdgesWithEndsInSubsetsIfTheyExistsInTheWorldGraph(PointsStructure sub1, 
			PointsStructure sub2, EdgesStructure edges)
	{		
		Iterator<Point> it1 = sub1.iterator();							
		// dla kazdego punktu pierwszego subsetu.....
		while(it1.hasNext())
		{
			Iterator<Point> it2 = sub2.iterator();
			int id1 = it1.next().getId();
			// ...dla kazdego punktu drugiego subsetu.....
			while(it2.hasNext())
			{
				int id2 = it2.next().getId();
				// ...jesli krawedz istnieje w grafie swiata to trzeba przekopiowac do naszego grafu 
				if( edges.exists(id1, id2) );
					edges.equals( edges.getEdge(id1, id2) );							
			}
		}
	}
	
	private void addEdgesWithEndsInSubsetsIfTheyExistsInTheWorldGraphAfterCheckingIntersections(PointsStructure sub1, 
			PointsStructure sub2, EdgesStructure edges)
	{
		Iterator<Point> it1 = sub1.iterator();		
		// dla kazdego punktu pierwszego subsetu...
		while(it1.hasNext())
		{
			Point point1 = it1.next();
			Iterator<Point> it2 = sub2.iterator();
			// dla kazdego punktu drugiego subsetu...
			while(it2.hasNext())
			{							
				Point point2 = it2.next();
				// ...jesli istnieje w grafie swiata...
				if( edges.exists(point1.getId(), point2.getId()) );
				{				
					// [ponizszy if nie pozwala by szukanie przeciec (metoda najbardziej spowalniajaca dzialanie,
					// wykonala sie dwa razy) odbylo sie dwa razy dla tej samej krawedzi]
					if(point1.getId() < point2.getId())
						// ... to trzeba sprawdzic (metoda przeciec) czy cala miesci sie w zasiegu nawigacji
						if( GeometryCalc.edgeInNavigationCircles(point1, point2, (Point[])userCities.toArray(), navRadiusKm))
							edges.addEdgeAndReversedEdge( edges.getEdge(point1.getId(), point2.getId()) );
				}
			}
		}
	}
	

	// Kazdy zbior n okregow nawigacyjnych moze stworzyc do 2^n rozdzielnych obszarow na mapie swiata, w kazdym
	// z takim obszarow znajduje sie pewna czesc wszystkich punktow na swiecie. Ten podzbior punktow to wlasnie 
	// subset
	private class Subsets
	{
		private PointsStructure[] subsets;
		private boolean[][] memberships;
		
		public Subsets(PointsStructure worldPoints, Point[] userCities, double navRadiusKm)
		{
			int numOfUserCities = userCities.length;
			int numOfSubsets = (int) Auxiliary.numberToIntegerPower(2, numOfUserCities);			
			memberships = createMembershipsArray(numOfUserCities);
			subsets = worldPoints.createArrayOfEmptyPointsStructures(numOfSubsets);
			
			for(int i = 0; i < numOfSubsets; i++)
			{
				subsets[i] = createSubset(worldPoints, userCities, memberships[i], navRadiusKm);
			}
		}
		
		public int size(){
			return subsets.length;			
		}	
		public boolean[] getMembership(int which){	
			return memberships[which];	
		}
		public PointsStructure getSubset(int which){
			return subsets[which];	
		}
		public Iterator<Point> getSubsetIterator(int which){
			return subsets[which].iterator();
		}		
		
		public boolean existsAtLeastOneNavigationCenterInOfWhichRangeTwoSubsetsAre(int whichSubset1, int whichSubset2)
		{
			return Various.ifArraysHaveAtLeastOneTrueOnSameIndex(getMembership(whichSubset1), 
													             getMembership(whichSubset2));
		}
	}

	public boolean[][] createMembershipsArray(int n)
	{
		return Various.createTableOfNumbersRepresentedInBinarySystem(n);
	}

	private PointsStructure createSubset(PointsStructure worldPoints, 
			Point[] userCities, boolean[] membership, double navRadiusKm)
	{				
		PointsStructure subset = worldPoints.createEmptyPointsStructure();
		Iterator<Point> it = worldPoints.iterator();
		while( it.hasNext() )
		{
			Point temp = it.next();
			addPointToSubsetIfItIsMemberOfOnlyGivenCircles(
					subset, temp, userCities, membership, navRadiusKm);							
		}
		return subset;		
	}	
		private PointsStructure addPointToSubsetIfItIsMemberOfOnlyGivenCircles( 
					PointsStructure subset, Point toAdd, Point[] userCities,
					boolean[] membership, double navRadiusKm)
		{			
			boolean ifAdd = true;
			for( int i = 0; i < userCities.length; i++){
				if( GeometryCalc.isPointInNavRange( userCities[i], toAdd, navRadiusKm) != membership[i] ){
					ifAdd = false;
					break;
				}								
			}
			if( ifAdd ){
				subset.addPoint(toAdd);
			}
			return subset;
		}	
}
