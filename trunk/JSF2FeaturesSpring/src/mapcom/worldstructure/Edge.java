package mapcom.worldstructure;

import java.io.Serializable;
import java.util.Arrays;

import mapcom.tools.Various;

public class Edge   // klasa byc moze niepotrzebna, bo wystarczy trzymac sama tablice terenow
{
	private static final long serialVersionUID = 10L;
	
	private final Point startPoint; 	
	private final Point endPoint;
	private final TerrainType[] typesOfTerrainsTraversed;
	private final float[] percentagesOfTerrainsTraversed;
	
	// startPoint and endPoint CAN'T be null
	public Edge(Point startPoint, Point endPoint, TerrainType[] typesOfTerrTrav, float[] percentagesOfTerrTrav){	
		this.startPoint = startPoint;
		this.endPoint = endPoint;
		this.typesOfTerrainsTraversed = typesOfTerrTrav;
		this.percentagesOfTerrainsTraversed = percentagesOfTerrTrav;
	}	
	
	public int numberOfTerrains(){
		return typesOfTerrainsTraversed.length;	
	}
	
	public TerrainType getTerrainType(int which){
		return typesOfTerrainsTraversed[which];	
	}
	
	public float getPercOfTerrain(int which){
		return percentagesOfTerrainsTraversed[which];	
	}	
		
	public Point getStartPoint(){
		return startPoint;	
	}
	
	public Point getEndPoint(){
		return endPoint;	
	}	
	
	public Edge createReversedEdge()
	{
		return new Edge(	
							this.endPoint, 
							this.startPoint, 
							Various.getInversedTerrainTypeArray(this.typesOfTerrainsTraversed),
							Various.getInversedFloatArray( this.percentagesOfTerrainsTraversed)
						);
	}
	
	@Override
	public String toString(){
		String s; 
		s = "Edge:\n";
		s += "start point: [" + startPoint.toString() + "]\n";
		s += "end point: [" + endPoint.toString() + "]\n";
		
		s += "terrain types: [";
		for( int i=0; i < typesOfTerrainsTraversed.length-1; i++){
			s += typesOfTerrainsTraversed[i].toString() + ", ";
		}
		s += typesOfTerrainsTraversed[typesOfTerrainsTraversed.length-1].toString() + "]\n";
		
		s += "percentages: [";
		for( int i=0; i < percentagesOfTerrainsTraversed.length-1; i++){
			s += new Float(percentagesOfTerrainsTraversed[i]).toString() + ", ";
		}
		s += new Float(percentagesOfTerrainsTraversed[percentagesOfTerrainsTraversed.length-1]).toString() + "]\n";
		return s;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((endPoint == null) ? 0 : endPoint.hashCode());
		result = prime * result
				+ Arrays.hashCode(percentagesOfTerrainsTraversed);
		result = prime * result
				+ ((startPoint == null) ? 0 : startPoint.hashCode());
		result = prime * result + Arrays.hashCode(typesOfTerrainsTraversed);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (!startPoint.equals(other.startPoint))
			return false;
		if (!endPoint.equals(other.endPoint))
			return false;
		if (!Arrays.equals(percentagesOfTerrainsTraversed, other.percentagesOfTerrainsTraversed))
			return false;
		if (!Arrays.equals(typesOfTerrainsTraversed, other.typesOfTerrainsTraversed))
			return false;
		return true;
	}
}
