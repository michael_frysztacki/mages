package mapcom.worldstructure;

import static org.junit.Assert.*;

import org.junit.Test;

public class CapitalTest {

	@Test
	public void toStringTesT() {
		Capital capital = new Capital(0.5, 1.5, 2, "Warsaw", true, false);
		assertEquals("Capital: name=Warsaw; id=2; (0.5, 1.5); water; not polar;", capital.toString());
	}

	@Test
	public void compareCapitalsWithSlightlyDifferentLongitudesTest(){
		Capital A = new Capital(0.5, 1.5, 2, "Warsaw", true, false);
		Capital B = new Capital(0.5000001, 1.5, 2, "Warsaw", true, false);
		assertFalse( A.equals(B) );
	}
	@Test
	public void compareCapitalsWithSlightlyDifferentLatitudesTest(){
		Capital A = new Capital(0.5, 1.5, 2, "Warsaw", true, false);
		Capital B = new Capital(0.5, 1.500000000001, 2, "Warsaw", true, false);
		assertFalse( A.equals(B) );
	}
	@Test
	public void compareTheSameCapitalsTest(){
		Capital A = new Capital(0.5, 1.5, 2, "Warsaw", true, false);
		Capital B = new Capital(0.5, 1.5, 2, "Warsaw", true, false);
		assertTrue( A.equals(B) );
	}
	@Test
	public void compareCapitalsWithSlightlyDifferentNames(){
		Capital A = new Capital(0.5, 1.5, 2, "Warsaws", true, false);
		Capital B = new Capital(0.5, 1.5, 2, "Warsaw", true, false);
		assertFalse( A.equals(B) );
	}

}
