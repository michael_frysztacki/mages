package mapcom.worldstructure;

import static org.junit.Assert.*;
import org.junit.Test;

public class PointTest {	
	
	
	double delta = 0.00000000001;

	double longitude = 1.5;
	double latitude = 89.5;
	boolean isWater = true;
	boolean isPolar = true;

	Point point = new Point(longitude, latitude, 3, isWater, isPolar);;
	
	@Test
	public void longitudeGetterTest()
	{		
		assertEquals(longitude, point.getLongit(), delta);
	}
	@Test
	public void latGetterTest()
	{
		assertEquals(latitude, point.getLat(), delta);
	}
	@Test
	public void isCreatedAntipodalPointHasIndeedOppositeCoordinates()
	{
		Point antipodalPoint = point.createAntipodalPoint();
		assertEquals(-longitude, antipodalPoint.getLongit(), delta);
		assertEquals(-latitude, antipodalPoint.getLat(), delta);
	}
	@Test
	public void isCreatedAntipodalPointHasTheSameOtherArguments()
	{
		Point antipodalPoint = point.createAntipodalPoint();
		assertEquals(point.isPolar(), antipodalPoint.isPolar());
		assertEquals(point.isWater(), antipodalPoint.isWater());
		assertEquals(point.getId(), antipodalPoint.getId());
	}
	
	@Test
	public void equalsTestWithDifferentIsPolar()
	{
		Point A = new Point(0.5, 2.7, 1, true, true);
		Point B = new Point(0.5, 2.7, 1, true, false);
		assertFalse( A.equals(B) );
		assertFalse( B.equals(A) );
	}	
	@Test
	public void equalsTestWithDifferentIsWater()
	{
		Point A = new Point(0.5, 2.7, 1, false, false);
		Point B = new Point(0.5, 2.7, 1, true, false);
		assertFalse( A.equals(B) );
		assertFalse( B.equals(A) );
	}	
	@Test
	public void equalsTestWithDifferentLat()
	{
		Point A = new Point(0.5, 2.6999, 1, true, false);
		Point B = new Point(0.5, 2.7, 1, true, false);
		assertFalse( A.equals(B) );
		assertFalse( B.equals(A) );
	}
	@Test
	public void equalsTestWithDifferentLongit()
	{
		Point A = new Point(0.50000001, 2.7, 1, true, false);
		Point B = new Point(0.5, 2.7, 1, true, false);
		assertFalse( A.equals(B) );
		assertFalse( B.equals(A) );
	}
	@Test
	public void equalsTestWithDifferentId()
	{
		Point A = new Point(0.5, 2.7, 2, true, false);
		Point B = new Point(0.5, 2.7, 1, true, false);
		assertFalse( A.equals(B) );
		assertFalse( B.equals(A) );
	}
	@Test
	public void equalsForTheSamePoints()
	{
		Point A = new Point(0.5, 2.7, 1, true, false);
		Point B = new Point(0.5, 2.7, 1, true, false);
		assertTrue( A.equals(B) );
		assertTrue( B.equals(A) );
	}
	
	@Test
	public void toStringTestWithWaterNotPolarPoint()
	{
		Point A = new Point(0.5, 2.7, 1, true, false);
		assertEquals( "Point: id=1; (0.5, 2.7); water; not polar;", A.toString() );
	}
	
	@Test
	public void toStringTestWaterPolarPoint()
	{
		Point A = new Point(0.5, 2.7, 1, true, true);
		assertEquals( "Point: id=1; (0.5, 2.7); water; polar;", A.toString() );
	}
}

