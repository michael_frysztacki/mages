package mapcom.worldstructure;

import java.io.IOException;

import mapcom.imgproc.Colors;

import mapcom.mapprojections.Equirectangular;
import mapcom.worldconstruction.GraphConstruction;

public class World
{
	private Points wPoints;
	private EdgesStructure waterEdges;
	private EdgesStructure landEdges;

	public World(String pointsPath, String edgesPath, String mapPath) throws IOException
	{
		wPoints = new Points(pointsPath);
		Equirectangular proj = new Equirectangular(mapPath);
		
        landEdges = new GraphHashMap1(wPoints.size());
        waterEdges = new GraphHashMap1(wPoints.size());       
        
        
        
        
        
		long startTime;			
		long endTime;
		double period;
        
		System.out.println("Starting creating land edges");
		startTime = System.currentTimeMillis();
		landEdges = GraphConstruction.createLandGraph(proj, landEdges, wPoints);
		endTime   = System.currentTimeMillis();
		period = (double)(endTime - startTime)/1000;
		System.out.println("Creating land edges done after" + " " + period);		
		
		System.out.println("Starting creating water edges");
		startTime = System.currentTimeMillis();
		waterEdges = GraphConstruction.createWaterGraph(proj, waterEdges, wPoints);
		endTime   = System.currentTimeMillis();
		period = (double)(endTime - startTime)/1000;
		System.out.println("Creating water edges done after" + " " + period);
        
        
        
        GraphConstruction.saveGraph(landEdges, edgesPath + "Land Edges", wPoints.size());
        GraphConstruction.saveGraph(waterEdges, edgesPath + "Water Edges", wPoints.size());
	}
	
	public void drawWorld(String path)
	{
//		for(int i = 0; i<wPoints.size(); i++)
//		{
//			wPoints.getPoint(i).showPoint();
//		}
//		Equirectangular proj = new Equirectangular(path);
//		WorldDrawing.drawEdges(proj, landEdges, wPoints, 1, Colors.lightblue);
//		WorldDrawing.drawEdges(proj, waterEdges, wPoints, 1, Colors.red);
//		WorldDrawing.drawCities(proj, wPoints, 15);
//		WorldDrawing.drawColonies(proj, wPoints, 15);
//		WorldDrawing.drawInterPoints(proj, wPoints, 15);
//		proj.saveProjection("swiat.png");
	}
	
	public int getNumOfNotPolarLandEdges()
	{
		// TODO Auto-generated method stub
		return 0;		
	}
	public int getNumOfNotPolarWaterEdges()
	{
		// TODO Auto-generated method stub
		return 0;		
	}	
	public int getNumOfPolarLandEdges()
	{
		// TODO Auto-generated method stub
		return 0;		
	}
	public int getNumOfPolarWaterEdges()
	{
		// TODO Auto-generated method stub
		return 0;		
	}
	public int getNumOfAllLandEdges()
	{
		// TODO Auto-generated method stub
		return 0;		
	}
	public int getNumOfAllWaterEdges()
	{
		// TODO Auto-generated method stub
		return 0;
	}
	public void createEdges()
	{
		// TODO Auto-generated method stub		
	}
	public void createLandEdges()
	{
		// TODO Auto-generated method stub		
	}
	

		
		
	public int getPointId(int id)
	{
		return wPoints.getPoint(id).getId();
	}
	public boolean ifPointIsInterPoint(int id)
	{
		return wPoints.getPoint(id).ifIsInterPoint();
	}
	public boolean ifPointIsColony(int id)
	{
		return wPoints.getPoint(id).ifIsColony();
	}
	public boolean ifPointIsCapital(int id)
	{
		return wPoints.getPoint(id).ifIsCapital();
	}
	public boolean ifPointIsWaterPoint(int id)
	{
		return wPoints.getPoint(id).ifIsWaterPoint();
	}
	public boolean ifPointIsPolarPoint(int id)
	{
		return wPoints.getPoint(id).ifIsPolarPoint();
	}
	public void createWaterEdges()
	{
		// TODO Auto-generated method stub		
	}
	public void loadEdges()
	{
		// TODO Auto-generated method stub		
	}
	public void loadLandEdges()
	{
		// TODO Auto-generated method stub		
	}
	public void loadWaterEdges()
	{
		// TODO Auto-generated method stub		
	}
	
	
	public Points getWPoints()
	{
		return wPoints;
	}
	public void setWPoints(Points wPoints)
	{
		this.wPoints = wPoints;
	}
	public EdgesStructure getWaterEdges()
	{
		return waterEdges;
	}
	public void setWaterEdges(EdgesStructure waterEdges)
	{
		this.waterEdges = waterEdges;
	}
	public EdgesStructure getLandEdges()
	{
		return landEdges;
	}
	public void setLandEdges(EdgesStructure landEdges)
	{
		this.landEdges = landEdges;
	}
}
