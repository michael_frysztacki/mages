package mapcom.worldstructure;

public interface PointsStructure extends Iterable<Point>
{		
	// the method returns point with id pointId, if it doesn't exist
	// the method returns null if such a point does not exist
	public Point getPointWithId(int pointId);		
	public void remove(int pointId);
	public void addPoint(Point toAdd);
	public int size();
	
	public PointsStructure createEmptyPointsStructure();
	public PointsStructure[] createArrayOfEmptyPointsStructures(int size);
}
