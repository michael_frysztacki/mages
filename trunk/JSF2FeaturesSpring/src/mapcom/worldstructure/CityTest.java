package mapcom.worldstructure;

import static org.junit.Assert.*;
import org.junit.Test;

public class CityTest {

	@Test
	public void toStringTesT() {
		City city = new City(0.5, 1.5, 2, true, false);
		assertEquals("City: id=2; (0.5, 1.5); water; not polar;", city.toString());
	}
	@Test
	public void compareColoniesWithSlightlyDifferentLongitudesTest(){
		City A = new City(0.5, 1.5, 2, true, false);
		City B = new City(0.5000001, 1.5, 2, true, false);
		assertFalse( A.equals(B) );
	}
	@Test
	public void compareColoniesWithSlightlyDifferentLatitudesTest(){
		City A = new City(0.5, 1.5, 2, true, false);
		City B = new City(0.5, 1.500000000001, 2, true, false);
		assertFalse( A.equals(B) );
	}
	@Test
	public void compareTheSameColoniesTest(){
		City A = new City(0.5, 1.5, 2, true, false);
		City B = new City(0.5, 1.5, 2, true, false);
		assertTrue( A.equals(B) );
	}
}
