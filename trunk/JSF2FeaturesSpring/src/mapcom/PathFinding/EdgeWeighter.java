package PathFinding;

import mapcom.worldstructure.Edge;

public interface EdgeWeighter
{
	public double edgeWeight(Edge edge, Army army);
}
