package PathFinding;

import java.util.ArrayList;

import mapcom.worldstructure.Point;
import mapcom.worldstructure.WorldGraph1;

public interface PathFinder 
{
	public ArrayList<Point> calcShortestPath(WorldGraph1 wg, Point startPoint, Point endPoint,
			                                      Army army, EdgeWeighter edgeWeight);
	
}
