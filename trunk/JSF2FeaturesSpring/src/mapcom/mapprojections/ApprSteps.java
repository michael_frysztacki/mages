package mapcom.mapprojections;


import mapcom.math.GeometryCalc;
import mapcom.math.PointOnWorldSphere;
import mapcom.math.Transl;

// Statyczna klasa zawierajaca metody, odpowiadajace za podawanie odpowiedniego kroku (w radianach) przechodzenia po krawedzi laczacej dwa 
// dwa punkty w sposob taki, aby zaden piksel w danej projekcji nie zostal pominiety.
public class ApprSteps
{
	
	public static double equirectangular(PointOnWorldSphere A, PointOnWorldSphere B, int imgWidth, int imgHeight)
	{			
//		         Pi*cos(extremeLatitude)                 
//		     ------------------------------
//		        max( imgWidth, imgHeight)
//		double extremeLatitude = Math.abs( GeometryCalc.getTheHighestOrLowestLatitudeOnEdge(A, B) );
//		
//		if( extremeLatitude <=85 )
//			return Math.PI*Math.abs(Math.cos(extremeLatitude)) / Math.max( imgHeight, imgWidth/2.0);
//		else
//			return Math.PI*Math.abs( Math.cos(Math.toRadians(85.0)) ) / Math.max( imgHeight, imgWidth/2.0);
		return 2*Math.PI/imgWidth/100.0;
//		return 0.0001;
	}
	
	public static double equirectangular(double xa, double ya, double za, double xb, double yb, double zb, 
			int imgWidth, int imgHeight)
	{			
		PointOnWorldSphere A = new PointOnWorldSphere(Transl.xyzToLongit(xa, ya), Transl.xyzToLat(za) );
		PointOnWorldSphere B = new PointOnWorldSphere(Transl.xyzToLongit(xb, yb), Transl.xyzToLat(zb) );
		return equirectangular(A, B, imgWidth, imgHeight);
	}
}
