package mapcom.mapprojections;

import mapcom.imgproc.ImgProc;
import mapcom.math.PointOnWorldSphere;
import mapcom.math.Transl;

import java.awt.image.BufferedImage;


public abstract class MapProjection
{
	protected BufferedImage img;

	public MapProjection(String inputFileName)	{
		this.img = ImgProc.loadImage(inputFileName);
	}
	public MapProjection(BufferedImage img)	{
		this.img = img;
	}
	
	public int getLongitDegToPix(PointOnWorldSphere A)	{
		return getLongitDegToPix(A.getLongit(), A.getLat());
	}
	
	public int getLatDegToPix(PointOnWorldSphere A)	{
		return getLatDegToPix(A.getLongit(), A.getLat());
	}
	
	public abstract int getLongitDegToPix(double longitDeg, double latDeg);
	public abstract int getLatDegToPix(double longitDeg, double latDeg);
	public abstract double getLongitPixToDeg(int longitPix, int latPix);
	public abstract double getLatPixToDeg(int longitPix, int latPix);
	
	// zwraca krok w stopniach o jaki powinno sie przesuwac po mapie danej wielkosci podczas konstruowania swiata (szukanie edge'ow)
	public final double apprStep(double xa, double ya, double za, double xb, double yb, double zb)	{
		PointOnWorldSphere A = new PointOnWorldSphere(Transl.xyzToLongit(xa, ya), Transl.xyzToLat(za));
		PointOnWorldSphere B = new PointOnWorldSphere(Transl.xyzToLongit(xb, yb), Transl.xyzToLat(zb));
		return apprStep(A, B);
	}	
	public abstract double apprStep(PointOnWorldSphere A, PointOnWorldSphere B);
	
	public final int getWidth(){ 	
		return img.getWidth();	
	}
	
	public final int getHeight(){ 
		return img.getHeight();	
	}		
	public final BufferedImage getImg(){
		return img;	
	}
	
	
	/**
	 *  RGB GETTERS 
	 */	
	public final int getRGB(double longitDeg, double latDeg)
	{
		return img.getRGB(getLongitDegToPix(longitDeg, latDeg), getLatDegToPix(longitDeg, latDeg));
	}
	public int getRGB(PointOnWorldSphere A)
	{
		return img.getRGB(getLongitDegToPix(A), getLatDegToPix(A));		
	}
	public int getRGB(int x, int y)
	{
		return img.getRGB(x, y);
	}
	
	/**
	 *  RGB SETTERS 
	 */	
	public final void setRGB(double longitDeg, double latDeg, int color)
	{
		img.setRGB(getLongitDegToPix(longitDeg, latDeg), getLatDegToPix(longitDeg, latDeg), color);
	}
	public final void setRGB(PointOnWorldSphere point, int color)
	{
		img.setRGB(getLongitDegToPix(point), getLatDegToPix(point), color);
	}
	public final void setRGB(int x, int y, int color)
	{
		img.setRGB(x, y, color);
	}
	public final void saveProjection(String outputFileName)
	{
		ImgProc.saveImage(this.img, outputFileName);
	}	
}



//package mapcom.mapprojections;
//
//import java.awt.image.BufferedImage;
//
//import mapcom.worldstructure.TerrainType;
//
//public abstract class MapProjection
//{
//	protected BufferedImage img;
//	TerrainType getTerrainType(){
//		return null;
//	}
//	MapProjection(BufferedImage img){
//		
//	}
//	
//		
//	
//}

