package mapcom.mapprojections;

import mapcom.imgproc.ImgProc;
import java.awt.image.BufferedImage;
import mapcom.worldstructure.Point;
import mapcom.math.Transl;

public class Mollweide extends MapProjection
{
	public Mollweide(BufferedImage img)
	{
		super(img);
	}
	// konstruktor, ktory tworzy projekcje na podstawie projekcji dostanej na wejsciu (konwertuje projekcje typu Equirectangular na
	// Mollweide)
	public Mollweide(Equirectangular Equi, int imgWidth, int imgHeight)
	{
		super( new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_RGB) );
		ImgProc.colorImgWithBlack(this.img);
		
		double longitStep = 360.0 / (double)img.getWidth();
		double latStep = 180.0 / (double)img.getHeight();		
		
		for(double i=-180.0; i< 180.0; i+=longitStep)
		{				
			for(double j=-90.0; j< 90.0; j+=latStep)
			{				
				setRGBOfImgPixel(i, j, Equi.getRGBOfImgPixel(i, j));
			}
		}
	}
	
	@Override
	public int getLongitDegToPix(double longitDeg, double latDeg)
	{
		return Transl.longitDegToPixMollweide(longitDeg, latDeg, getWidth());	
	}

	@Override
	public int getLatDegToPix(double longitDeg, double latDeg)
	{
		// metoda ta dziala tak samo jak dla mapy prostokatnej
		return Transl.latDegToPixEquirect(latDeg, img.getHeight() );
	}

	@Override
	public int getLongitDegToPix(Point A)
	{
		return Transl.longitDegToPixMollweide(A.getLongit(), A.getLat(), getWidth());
	}
	@Override
	public int getLatDegToPix(Point A)
	{
		return Transl.latDegToPixEquirect(A.getLat(), img.getHeight() );
	}

	@Override
	public double getLongitPixToDeg(int longitPix, int latPix)
	{
		return Transl.longitPixToDegMollweide(longitPix, latPix, getWidth(), getHeight());
	}
	@Override
	public double getLatPixToDeg(int longitPix, int latPix)
	{
		// metoda ta dziala tak samo jak dla mapy prostokatnej
		return Transl.latPixToDegEquirect(latPix, img.getHeight() );
	}
	@Override
	public double apprStep(Point A, Point B)
	{
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public double apprStep(double xa, double ya, double za, double xb,
			double yb, double zb)
	{
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public double apprStep(double longitA, double latA, double longitB,
			double latB)
	{
		// TODO Auto-generated method stub
		return 0;
	}
}
