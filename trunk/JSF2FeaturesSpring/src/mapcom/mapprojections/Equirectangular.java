package mapcom.mapprojections;

import java.awt.image.BufferedImage;

import mapcom.math.PointOnWorldSphere;
import mapcom.math.Transl;

public class Equirectangular extends MapProjection
{	
	public Equirectangular(String inputFileName)
	{
		super(inputFileName); 
	}
	public Equirectangular(BufferedImage img)
	{
		super(img); 
	}
	
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	@Override
	public int getLongitDegToPix(double longitDeg, double latDeg)
	{
		return Transl.longitDegToPixEquirect(longitDeg, img.getWidth());		
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	@Override
	public int getLatDegToPix(double longitDeg, double latDeg)
	{
		return Transl.latDegToPixEquirect(latDeg, img.getHeight());
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	@Override
	public double getLongitPixToDeg(int longitPix, int latPix)
	{
		return Transl.longitPixToDegEquirect(longitPix, img.getWidth());
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	@Override
	public double getLatPixToDeg(int longitPix, int latPix)
	{
		return Transl.latPixToDegEquirect(latPix, img.getHeight());
	}
	@Override
	public double apprStep(PointOnWorldSphere A, PointOnWorldSphere B)
	{
		return ApprSteps.equirectangular(A, B, img.getWidth(), img.getHeight());		
	}
	
	
}
