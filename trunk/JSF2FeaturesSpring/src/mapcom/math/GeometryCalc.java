package mapcom.math;

import java.util.ArrayList;

import mapcom.math.Transl;
import mapcom.math.WorldConstants;
import mapcom.worldstructure.Point;

public class GeometryCalc
{

	
	public static boolean haveTheSameCoordinates(PointOnWorldSphere A, PointOnWorldSphere B)
	{
		return (A.getLongit() == B.getLongit()) && (A.getLat() == B.getLat());
	}
	
	/**
	 * BARDZO WAZNE ZALOZENIE: metoda zwraca czasami bledne wartosci dla pkt antypodalnych czyli takich ktore znajduja sie po
	 * przeciwleglych stronach kuli
	 */ 
	// TESTED
	public static double sphericalDistance(double long1, double lat1, double long2, double lat2)
	{
		double a,c,d;
		double dLat = Math.toRadians(lat1) - Math.toRadians(lat2);
		double dLon = Math.toRadians(long1)- Math.toRadians(long2);	
		
		a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
		c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		d = WorldConstants.EARTH_RADIUS_LENGTH * c;
		return d;
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double sphericalDistance(PointOnWorldSphere point1, PointOnWorldSphere point2)
	{
		return sphericalDistance( point1.getLongit(), point1.getLat(), point2.getLongit(), point2.getLat() );
	}
	
	// TESTED
	public static double dotProduct(double xa, double ya, double za, double xb, double yb, double zb)
	{
		return xa*xb + ya*yb + za*zb;
	}
	
	// TESTED
	public static double dotProduct(double xa, double ya, double xb, double yb)
	{
		return xa*xb + ya*yb;
	}
	
	// TESTED
	public static double[] crossProduct(double xa, double ya, double za, double xb, double yb, double zb)
	{
		return new double[]{ya*zb-yb*za, za*xb-zb*xa, xa*yb-xb*ya};		
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double[] crossProduct(PointOnWorldSphere A, PointOnWorldSphere B)
	{
		return crossProduct(Transl.coordToCartX(A),
		 					Transl.coordToCartY(A),
		 					Transl.coordToCartZ(A),
		 					Transl.coordToCartX(B),
		 					Transl.coordToCartY(B),
		 					Transl.coordToCartZ(B));
	}
	
	public static double angleBetweenVectorsInRad(double xa, double ya, double xb, double yb)
	{
		return Math.acos( dotProduct(xa, ya, xb, yb) / ((vectorLength(xa, ya)*vectorLength(xb, yb))));
	}

	// 3D vector variant
	// na Wejsciu nie moze wystapic wektor niezerowy
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double vectorLength(double[] vector)
	{
		return (Math.sqrt(vector[0]*vector[0] + vector[1]*vector[1] + vector[2]*vector[2]));		
	}
	// 2D vector variant
	// na Wejsciu nie moze wystapic wektor niezerowy
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double vectorLength(double xa, double ya)
	{
		return (Math.sqrt(xa*xa + ya*ya));		
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static PointOnWorldSphere vectorToPoint(double[] vector)
	{
		return new PointOnWorldSphere( Transl.xyzToLongit(vector[0], vector[1]), Transl.xyzToLat(vector[2]));
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double[] pointToVector(PointOnWorldSphere A)
	{
		return new double[]{ Transl.coordToCartX(A), Transl.coordToCartY(A), Transl.coordToCartZ(A)};
	}
	
	// Prawoskretny uklad
	// TESTED
	public static double[] rotateVectorAroundZAxis(double[] vector, double angleInRad)
	{
		return new double[]{vector[0]*Math.cos(angleInRad) - vector[1]*Math.sin(angleInRad), 
							 vector[0]*Math.sin(angleInRad) + vector[1]*Math.cos(angleInRad), 
							 vector[2]};
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double[] rotateVectorAroundZAxis(PointOnWorldSphere toRotate, double angle)
	{		
		return rotateVectorAroundZAxis( pointToVector(toRotate), angle );
	}


	// Prawoskretny uklad
	// TESTED	
	public static double[] rotateVectorAroundXAxis(double[] vector, double angleInRad)
	{
		return new double[]{
							vector[0],
							vector[1]*Math.cos(angleInRad) - vector[2]*Math.sin(angleInRad),
							vector[1]*Math.sin(angleInRad) + vector[2]*Math.cos(angleInRad) 
							};							 
	}	
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double[] rotateVectorAroundXAxis(PointOnWorldSphere toRotate, double angleInRad)
	{		
		return rotateVectorAroundXAxis( pointToVector(toRotate), angleInRad );
	}
	
	
 
	// Zwraca kat o jaki zostala obrocona plaszczyzna wokol osi OX,
	/**
	 * BARDZO WAZNE ZALOZENIE: PLASZCZYZNA NIE MOZE BYC OBROCONA W INNYCH PLASZCZYZNACH
	 * W PRAKTYCE ZNACZY TO TYLE, ZE WEKTOR NORMALNY MA SKLADOWA X = ~0.0
	 */ 
	// argumentem wejsciowym jest wektor prostopadly do plaszczyzny 
	// (a co za tym idzie moze tez byc wektor normalny plaszczyzny).
	// Argumentem wyjsciowym jest kat w radianach od -Pi/2 do Pi/2 (90 stopni)
	// Orientacja XYZ
	//   
	//	   ^ z      
	//	   |      ^ y
	//	   |     /
	//     |	/
	//	   |   /
	//	   |  /
	//	   | /                                    x
	//	   | -------------------------------------->
    //
	//
	//    Ponizsze przyklady pokazuja trojwymiarowa przestrzen zrzutowana na 2 wymiarowa plaszczyzny OYZ
	//                      ^ z
	// 						|
	//	                \pi | 
	//	                 \  |    
	//	                  \ |   W tym przypadku plaszczyzna pi jest obrocona o kat okolo alfa = -70 stopni	
	//	    			   \|       
	//	-------------------------------------------------------------------> y
	//	                     \ alfa = ~ - 70 stopni
	//	                      \ 
	//	                       \
	//	                        \
	//	                         \             
	//                      ^ z
	// 						|    
	//	                    |   / pi
	//	                    |  /  
	//	                    | /  	
	//	                    |/ alfa = ~ 70 stopni      
	//	-------------------------------------------------------------------> y
	//	                   /   
	//	                  / W tym przypadku plaszczyzna pi jest obrocona o kat okolo alfa = 70 stopni
	//	                 /     
	//	                /        
	//	               /         
	// TESTED
	public static double planeSlopeOXAxis(double[] perpendicularVector)
	{
		if( perpendicularVector[2] == 0 )
		{
			if( perpendicularVector[1] == 0)				
			{				
				throw new java.lang.IllegalArgumentException("Error in PlaneSlopeOXAxis() method: All the elements of the input vector equal 0.");								
			}			
			else				
				return Math.PI/2.0;			
		}
		else
		{
			if( perpendicularVector[1] == 0 )
			{
				return 0.0;
			}			
			else
			{
				return -1*Math.atan(perpendicularVector[1]/perpendicularVector[2]);
			}
		}
	}	
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double planeSlopeOXAxis(double xa, double ya, double za, double xb, double yb, double zb)
	{		
		return planeSlopeOXAxis(crossProduct(xa, ya, za, xb, yb, zb));
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double planeSlopeOXAxis(double longA, double latA, double longB, double latB)
	{
		double xa = Transl.coordToCartX(longA, latA);
		double ya = Transl.coordToCartY(longA, latA);
		double za = Transl.coordToCartZ(latA);
		double xb = Transl.coordToCartX(longB, latB);
		double yb = Transl.coordToCartY(longB, latB);
		double zb = Transl.coordToCartZ(latB);
		
		return planeSlopeOXAxis(xa, ya, za, xb, yb, zb);
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double planeSlopeOXAxis(PointOnWorldSphere A, PointOnWorldSphere B)
	{			
		double xa = Transl.coordToCartX(A);
		double ya = Transl.coordToCartY(A);
		double za = Transl.coordToCartZ(A);
		double xb = Transl.coordToCartX(B);
		double yb = Transl.coordToCartY(B);
		double zb = Transl.coordToCartZ(B);
		
		return planeSlopeOXAxis(xa, ya, za, xb, yb, zb);
	}	

	// Zwraca kat o jaki zostala obrocona plaszczyzna wokol osi OZ, 
	// argumentem wejsciowym jest wektor prostopadly do plaszczyzny 
	// (a co za tym idzie moze tez byc wektor normalny plaszczyzny).
	// Argumentem wyjsciowym jest kat w radianach od -Pi/2 do Pi/2 (90 stopni)
	//
	// Orientacja XYZ	   
	//	   ^ z      
	//	   |     ^ y
	//	   |    /
	//     |   /
	//	   |  /
	//	   | /
	//	   |/                                    x
	//	    -------------------------------------->
    //
	//  Ponizsze przyklady pokazuja trojwymiarowa przestrzen zrzutowana na 2 wymiarowa plaszczyzny OXY	
	//                      ^ y
	// 						|
	//	                \pi | 
	//	                 \  |    
	//	                  \ |   W tym przypadku plaszczyzna pi jest obrocona o kat okolo alfa = ~ -70 stopni	
	//	   				   \|       
	//	-------------------------------------------------------------------> x
	//	                     \  alfa= ~ -70stopni
	//	                      \ 
	//	                       \
	//	                        \
	//	                         \             
	//                      ^ y
	// 						|    
	//	                    |   / pi
	//	                    |  /  
	//	                    | /  	
	//	                    |/ alfa = ~ 70 stopni      
	//	-------------------------------------------------------------------> x
	//	                   /   
	//	                  / W tym przypadku plaszczyzna pi jest obrocona o kat okolo alfa = 70 stopni
	//	                 /     
	//	                /        
	//	                  	
	// TESTED
	public static double planeSlopeOZAxis(double[] perpendicularVector)
	{ 		
		// jesli skladowe X i Y sa niezerowe
		if( (perpendicularVector[0] != 0.0 ) && (perpendicularVector[1] != 0.0) ) 
		{	
			return Math.atan(-perpendicularVector[0]/perpendicularVector[1]);			
		}
		else // jesli conajmniej jedna ze skladowych X lub Y jest rowna zeru
		{
			if( perpendicularVector[0] == 0.0 ) // jesli skladowa X jest rowna 0
			{
				if( perpendicularVector[1] == 0.0)  //i jesli skladowa Y jest rowna 0
				{
					if( perpendicularVector[2] == 0.0) //wszystkie skladowe rowne 0											
						throw new java.lang.IllegalArgumentException("Error in PlaneSlopeOZAxis() method: All the elements of the input vector equal 0.");			
					else // X,Y=0, Z rozne od zera => brak obrotu					
						return 0.0;					
				}
				else  
				{
					// Jesli X=0, Y rozny od zera, to niezaleznie od Wartosci Z nie ma obrotu
					return 0.0;					
				}
			}
			else // skladowa X nierowna zeru => wiec Y musi byc rowny 0
			{
				// Niezaleznie od wartosci Z obrot jest obrot o 90 stopni
				return Math.PI/2;
			}
		}
	}	

	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double planeSlopeOZAxis(double xa, double ya, double za, double xb, double yb, double zb)
	{
		return planeSlopeOZAxis(crossProduct(xa, ya, za, xb, yb, zb));
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double planeSlopeOZAxis(double longA, double latA, double longB, double latB)
	{
		double xa = Transl.coordToCartX(longA, latA);
		double ya = Transl.coordToCartY(longA, latA);
		double za = Transl.coordToCartZ(latA);
		double xb = Transl.coordToCartX(longB, latB);
		double yb = Transl.coordToCartY(longB, latB);
		double zb = Transl.coordToCartZ(latB);
		
		return planeSlopeOZAxis(xa, ya, za, xb, yb, zb);
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double planeSlopeOZAxis(PointOnWorldSphere A, PointOnWorldSphere B)
	{		
		double xa = Transl.coordToCartX(A);
		double ya = Transl.coordToCartY(A);
		double za = Transl.coordToCartZ(A);
		double xb = Transl.coordToCartX(B);
		double yb = Transl.coordToCartY(B);
		double zb = Transl.coordToCartZ(B);
		
		return planeSlopeOZAxis(xa, ya, za, xb, yb, zb);
	}		
	//to samo co powyzej ale przyjmuje wspolrzedne dwoch pkt rozpinajacych plaszcyzne (trzeci pkt to 0,0,0) w ukladzie XYZ
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double[] getPlaneSlopes(double xa, double ya, double za, double xb, double yb, double zb)
	{
		return getPlaneSlopes(crossProduct(xa, ya, za, xb, yb, zb));
	}	
	//zwraca katy nachylenia (w postaci tablicy dwuwymiarowej) plaszczyzny zdefiniowanej za pomoca wektora normalnego
	//pierwszy element tablicy to kat obrotu wokol osi OX
	//drugi element tablicy to kat obrotu wokol osi OZ
	// TESTED
	public static double[] getPlaneSlopes(double[] normalVector)
	{
		double[] angles = new double[2];		
		//beta - obrot wokol osi OZ
		angles[1] = planeSlopeOZAxis(normalVector);
		//aby podejsc do wyznacznia obrotu wokol osi OX najpierw musimy obrocic wektor tak, zeby nie mial skladowej X 
		//(wedlug zalozen metody planeSlopeOXAxis)
		normalVector = rotateVectorAroundZAxis(normalVector, -angles[1]);
		//alpha - obrot wokol osi OX		
		angles[0] = planeSlopeOXAxis(normalVector);
		return angles;
	}
	
	//ta sama metoda co powyzsza, ale przyjmuje pointy, a nie koordynanty
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double[] getPlaneSlopes(PointOnWorldSphere A, PointOnWorldSphere B)
	{
		return getPlaneSlopes(Transl.coordToCartX(A),
							  Transl.coordToCartY(A), 
							  Transl.coordToCartZ(A),
							  Transl.coordToCartX(B),
							  Transl.coordToCartY(B), 
							  Transl.coordToCartZ(B)
							  );
	}	
	// zwraca roznice punktow w dlugosci geograficznej (w radianach) 
	// TESTED
	public static double longitudeDistanceInRad(PointOnWorldSphere point1, PointOnWorldSphere point2)
	{
		double longitDist = Math.toRadians( Math.abs(point1.getLongit() - point2.getLongit()) );
		if(longitDist > Math.PI)
			longitDist = 2*Math.PI - longitDist;
		return longitDist;
	}
	// zwraca roznice punktow w szerokosci geograficznej (w radianach)
	// TESTED
	public static double latitudeDistanceInRad(PointOnWorldSphere point1, PointOnWorldSphere point2)
	{
		double latDist = Math.toRadians( Math.abs(point1.getLat() - point2.getLat()) );		
		return latDist;
	}	
	// zwraca kat pomiedzy wektorami
	// zwraca odleglosc katowa (w radianach) pomiedzy pkt o danych wspolrzednych
	// TESTED, BUT SUPPOSED TO WORK PROPERLY
	//
	// |A|*|B|*cos(teta) =  xa*xb+ya*yb+za*zb       wiemy, ze |A|=|B|=R - promien ziemi  =>
	//
	//                      xa*xb + ya*yb + za*zb
	//  =>  teta = arcCos( ----------------------- ) 
	//                               R*R
	//
	// NOT TESTED
	public static double angleDistanceInRad(double xa, double ya, double za, double xb, double yb, double zb)
	{		
		double teta;
		if(xa==xb && ya==yb && za==zb)   // ten if omija problem, ale nie jest to rozwiazanie optymalne
			teta = 0;
		else
			teta = Math.acos((xa*xb + ya*yb + za*zb )/(WorldConstants.EARTH_RADIUS_LENGTH*WorldConstants.EARTH_RADIUS_LENGTH));				
		return teta;	
	}
	// TESTED
	public static double angleDistanceInRad(double longitA, double latA, double longitB, double latB)
	{		
		double xa = Transl.coordToCartX(longitA, latA);
		double ya = Transl.coordToCartY(longitA, latA);
		double za = Transl.coordToCartZ(latA);
		double xb = Transl.coordToCartX(longitB, latB);
		double yb = Transl.coordToCartY(longitB, latB);
		double zb = Transl.coordToCartZ(latB);
		
		return angleDistanceInRad(xa, ya, za, xb, yb, zb);
	}
	// to samo co powyzej ale argumentem sa punkty
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double angleDistanceInRad(PointOnWorldSphere point1, PointOnWorldSphere point2)
	{
		return angleDistanceInRad(Transl.coordToCartX(point1), Transl.coordToCartY(point1), Transl.coordToCartZ(point1),
								Transl.coordToCartX(point2), Transl.coordToCartY(point2), Transl.coordToCartZ(point2));
	}
	
	// zwraca kat luku ACB w radianach
	// Ta metode mozna zoptymalizowac (poprzez zaprzestanie uzywania funkcji euclideanDistance), ale dla czytelnosci kodu
	// narazie pozostaje ta wersja
	// TESTED
	public static double getInscribedAngle(double xa, double ya, double za, double xb, double yb, double zb, 
			double xc, double yc, double zc)
	{
		double ACx = xa-xc;
		double ACy = ya-yc;
		double ACz = za-zc;
		double BCx = xb-xc;
		double BCy = yb-yc;
		double BCz = zb-zc;
		
		double AC = euclideanDistance(xa, ya, za, xc, yc, zc);
		double BC = euclideanDistance(xb, yb, zb, xc, yc, zc);
		return Math.acos( (ACx*BCx + ACy*BCy + ACz*BCz )/(AC*BC));
	}
	
	public static double getInscribedAngle(PointOnWorldSphere A, PointOnWorldSphere B, PointOnWorldSphere C)
	{
		double xa = Transl.coordToCartX(A);
		double ya = Transl.coordToCartY(A);
		double za = Transl.coordToCartZ(A);
		double xb = Transl.coordToCartX(B);
		double yb = Transl.coordToCartY(B);
		double zb = Transl.coordToCartZ(B);
		double xc = Transl.coordToCartX(C);
		double yc = Transl.coordToCartY(C);
		double zc = Transl.coordToCartZ(C);
		
		return getInscribedAngle(xa, ya, za, xb, yb, zb, xc, yc, zc);
	}
	
	public static PointOnWorldSphere getTheHighestOrLowestPointOnGreatCircle(PointOnWorldSphere A, PointOnWorldSphere B)
	{		
		PointOnWorldSphere extremePoint = new PointOnWorldSphere(180.0, 0.0);
				
		double[] angles = GeometryCalc.getPlaneSlopes(A, B);
		
		double[] vector = GeometryCalc.pointToVector(extremePoint);
				
		vector = GeometryCalc.rotateVectorAroundXAxis(vector, angles[0]);
		vector = GeometryCalc.rotateVectorAroundZAxis(vector, angles[1]);
		
		extremePoint = GeometryCalc.vectorToPoint(vector);
		return extremePoint;
	}
	
	
	// TESTED
	public static PointOnWorldSphere getTheHighestOrLowestPointOnEdge(PointOnWorldSphere A, PointOnWorldSphere B)
	{		
		PointOnWorldSphere extremePoint = getTheHighestOrLowestPointOnGreatCircle(A, B);
		
		if( isPointBetweenPointsOnEdge(A, B, extremePoint) )
		{
			return extremePoint;
		}
		else
		{
			extremePoint = extremePoint.createAntipodalPoint();
			if( isPointBetweenPointsOnEdge(A, B, extremePoint) )			
				return extremePoint;			
			else
				return getPointClosestToAPole(A, B);			
		}		
	}	
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static PointOnWorldSphere getTheHighestOrLowestPointOnEdge(double longitA, double latA, double longitB, double latB)
	{
		return getTheHighestOrLowestPointOnEdge(new PointOnWorldSphere(longitA, latA), new PointOnWorldSphere(longitB, latB));
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static PointOnWorldSphere getTheHighestOrLowestPointOnEdge(double xa, double ya, double za, double xb, double yb, double zb)
	{
		return getTheHighestOrLowestPointOnEdge(
												new PointOnWorldSphere(Transl.xyzToLongit(xa, ya), Transl.xyzToLat(za)),
												new PointOnWorldSphere(Transl.xyzToLongit(xb, yb), Transl.xyzToLat(zb))
											   );
	}		
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double getTheHighestOrLowestLatitudeOnEdge(PointOnWorldSphere A, PointOnWorldSphere B)
	{
		return getTheHighestOrLowestPointOnEdge(A, B).getLat();
	}
	
	// Zwraca ten z pkt, ktory jest usytuowany blizej do ktoregokolwiek z biegunow
	// TESTED
	public static PointOnWorldSphere getPointClosestToAPole(PointOnWorldSphere A, PointOnWorldSphere B)
	{
		// jesli blizej do bieguna polnocnego niz do poludniowego
		if( (Math.min(A.getLat(), B.getLat()) + 90.0 ) <= ( 90.0 - Math.max(A.getLat(), B.getLat()) ) )
		{
			//Jesli to pkt A jest blizej bieguna polnocnego 
			if( Math.min(A.getLat(), B.getLat()) == A.getLat() )			
				return A;			
			else
				return B;
		}
		else  // jesli blizej do bieguna poludniowego
		{
			//Jesli to pkt A jest blizej bieguna poludniowego
			if( Math.max(A.getLat(), B.getLat()) == A.getLat() )			
				return A;			
			else
				return B;
		}
	}       
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static PointOnWorldSphere getPointClosestToAPole(double longitA, double latA, double longitB, double latB)
	{
		return getPointClosestToAPole(new PointOnWorldSphere(longitA, latA), new PointOnWorldSphere(longitB, latB));
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static PointOnWorldSphere getPointClosestToAPole(double xa, double ya, double za, double xb, double yb, double zb)
	{
		return getPointClosestToAPole(
				new PointOnWorldSphere(Transl.xyzToLongit(xa, ya), Transl.xyzToLat(za)),
				new PointOnWorldSphere(Transl.xyzToLongit(xb, yb), Transl.xyzToLat(zb))
									  );
	}
	
	// TESTED
	public static double getGreaterAngleInscribedOnLineSegment(PointOnWorldSphere A, PointOnWorldSphere B)
	{
		return getGreaterAngleInscribedOnLineSegment(euclideanDistance(A, B));
	}	
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double getGreterAngleInscribedOnLineSegment(double xa, double ya, double za, double xb, double yb, double zb )
	{
		return getGreaterAngleInscribedOnLineSegment(euclideanDistance(xa, ya, za, xb, yb, zb));
	}
	//				_<  
	//	          _< a|       a - polowa kata zwracanego przez funkcje
	//	        _<    |       R - promien ziemi
	//	      _<      |       r - polowa zmiennej lineSegmentLength
	//	    _<   r    |
	//	   <----------|R 
	//	     <        |
	//	      R <     |
	//	           <  |	
	// 
	// lineSegment to odleglosc euklidesowa pomiedzy dwoma punktami na sferze
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double getGreaterAngleInscribedOnLineSegment(double lineSegmentLength)
	{
		double r = lineSegmentLength / 2.0;
		return 2*Math.atan( r / (WorldConstants.EARTH_RADIUS_LENGTH - 
			Math.sqrt(WorldConstants.EARTH_RADIUS_LENGTH*WorldConstants.EARTH_RADIUS_LENGTH-r*r)) );
	}
	// ZALOZENIE: na wejsciu mamy 3pkt lezace na jednym wielkim kole
	// TESTED
	public static boolean isPointBetweenPointsOnEdge(PointOnWorldSphere A, PointOnWorldSphere B, PointOnWorldSphere pointBetween)
	{
		// C = pointBetween  (sprawdzamy czy znajduje sie on miedzy pkt A i B)
//		double AB = euclideanDistance(A, B);
//		double AC = euclideanDistance(A, pointBetween);
//		double BC = euclideanDistance(B, pointBetween);		
		
		if( getInscribedAngle(A, B, pointBetween) < Math.PI/2)
			return false;
		return true;		
	}
	/**
	 * BARDZO WAZNE ZALOZENIE: PKT poczatkowy i koncowy nie powinny byc oddalone od siebie o 180 stopni
	 */ 
	// zwraca punkt na luku miedzy punktami A i B, oddalony o kat beta od punktu point1, alpha to odleglosc katowa miedzy punktami
	// beta w radianach
	// TESTED
	public static PointOnWorldSphere getPointBetween(double xa, double ya, double za, double xb, double yb, double zb, 
			double alpha, double betaInRad)
	{										
		double t;
		double s;
		double xp;
		double yp;
		double zp;
		double sinAlpha = Math.sin(alpha);
		double sinBeta = Math.sin(betaInRad);
		
		t = Math.sin(alpha-betaInRad)/sinAlpha;
		s = sinBeta/sinAlpha;
		t = Math.sin(alpha-betaInRad)/sinAlpha;
		s = sinBeta/sinAlpha;
		xp = xa*t+xb*s;
		yp = ya*t+yb*s;
		zp = za*t+zb*s;
		PointOnWorldSphere pointBetween = new PointOnWorldSphere(Transl.xyzToLongit(xp, yp), Transl.xyzToLat(zp));
		
		return pointBetween;
	}
	// to samo co powyzej ale argumentami wejsciowymi sa pointy
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static PointOnWorldSphere getPointBetween(PointOnWorldSphere point1, PointOnWorldSphere point2, double angle, double beta )
	{	
		return getPointBetween(
				Transl.coordToCartX(point1), 
				Transl.coordToCartY(point1),
				Transl.coordToCartZ(point1), 
				Transl.coordToCartX(point2),
				Transl.coordToCartY(point2),
				Transl.coordToCartZ(point2), 
				angle, beta);
	}
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static PointOnWorldSphere getPointBetween(PointOnWorldSphere point1, PointOnWorldSphere point2, double beta)
	{											
		return getPointBetween(point1, point2, angleDistanceInRad(point1, point2), beta);				
	}






	

	
	//znajduje odleglosc(po sferze) punktow przeciecia kol nawigacji od wielkiego kola przechodzacego przez centra punktow nawigacji
	//naviRadius w km
	// NOT TESTED
	public static Double getHalfOfTheDistanceOfTheIntersectionsOfNaviCircles(PointOnWorldSphere point1, PointOnWorldSphere point2, double naviRadiusKm)
	{
		// jesli okregi sa zbyt daleko siebie to nie istnieja pomiedzy nimi punkty przeciecia
		if(sphericalDistance(point1, point2) > naviRadiusKm*2)
		{
			return null;
		}
		else
		{		
			double a; //odleglosc na sferze punktu przeciecia kol nawigacji od najkrotszej linii laczacej punkty point1 i point2  
			double b = sphericalDistance(point1, point2)/2;;
			double c = naviRadiusKm;
			double R = WorldConstants.EARTH_RADIUS_LENGTH;
			/**
			  	cos(a/R)*cos(b/R) = cos(c/R) prawo pitagorasa na sferze
			 */
			a = R*Math.acos(Math.cos(c/R)/Math.cos(b/R));
			return a;
		}
	}	
	
	

	

	
	// ponizsza metoda zwraca odleglosc euklidesowa miedzy pkt w przestrzeni kartezjanskiej
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double euclideanDistance(double xa, double ya, double za, double xb, double yb, double zb)
	{
		return Math.sqrt((xb-xa)*(xb-xa) + (yb-ya)*(yb-ya) + (zb-za)*(zb-za));
	}	
	// to samo co powyzej, ale przyjmuje wspolrzedne geograficzne
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double euclideanDistance(double longit1, double lat1, double longit2, double lat2)
	{
		double xa = Transl.coordToCartX(longit1, lat1);
		double ya = Transl.coordToCartY(longit1, lat1);
		double za = Transl.coordToCartZ(lat1);
		double xb = Transl.coordToCartX(longit2, lat2);
		double yb = Transl.coordToCartY(longit2, lat2);
		double zb = Transl.coordToCartZ(lat2);
		return euclideanDistance(xa, ya, za, xb, yb, zb);
	}	
	// to samo co powyzej, ale przyjmuje punkty
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double euclideanDistance(PointOnWorldSphere A, PointOnWorldSphere B)
	{
		return euclideanDistance(A.getLongit(), A.getLat(), B.getLongit(), B.getLat());
	}
	
	//zwraca kwadrat odleglosci euklidesowej
	//Po co nam ta metoda? moze sie przydac przy, kiedy potrzebujemy porownac dwie odleglosci, a potrzebujemy oszczedzac
	//obliczenia. Dzieki tej metodzie nie musimy wykonywac pierwiastkowania
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double euclideanDistanceSquared(double xa, double ya, double za, double xb, double yb, double zb)
	{
		return (xb-xa)*(xb-xa) + (yb-ya)*(yb-ya) + (zb-za)*(zb-za);
	}
	// to samo co powyzej, ale przyjmuje wspolrzedne geograficzne
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double euclideanDistanceSquared(double longA, double latA, double longB, double latB)
	{
		double xa = Transl.coordToCartX(longA, latA);
		double ya = Transl.coordToCartY(longA, latA);
		double za = Transl.coordToCartZ(latA);
		double xb = Transl.coordToCartX(longB, latB);
		double yb = Transl.coordToCartY(longB, latB);
		double zb = Transl.coordToCartZ(latB);
		return euclideanDistanceSquared(xa, ya, za, xb, yb, zb);
	}	
	// to samo co powyzej, ale przyjmuje punkty
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static double distanceXYZSquared(PointOnWorldSphere A, PointOnWorldSphere B)
	{
		return euclideanDistanceSquared(A.getLongit(), A.getLat(), B.getLongit(), B.getLat());
	}
	
	



	// Zwraca odleg�o�� ko�a nawigacji (zrzutowanego na p�aszczyzn� zawieraj�c� okr�g owego ko�a nawigacji) od �rodka kuli ziemskiej
	//		 
	//	  r	|            
	//		|---------- 
	//		|         /
	//		|        /
	//		|       /
	//	  	|      /
	//		|alpha/ R
	//		|    /
	//		|   /
	//		|  /
	//		| /
	//		|/	
	//
	// TESTED
	public static double distanceOfNaviCircleToSphereCenter(double naviRadiusKm)
	{			
		return Math.cos( naviRadiusKm/WorldConstants.EARTH_RADIUS_LENGTH ) * WorldConstants.EARTH_RADIUS_LENGTH;
	}		
	
	// czy punkt nalezy do zasiegu nawigacji danego punktu
	// TESTED
	public static boolean isPointInNavRange(PointOnWorldSphere navCenter, PointOnWorldSphere toCheck, double naviRadiusKm)
	{
		// a - polowa odleglosci euklidesowej od srodka nawigacji do najodlejszego punktu lezacego w zasiegu nawigacji
		//
		//  a		  naviRadiusKm
		// --- = sin( ------------ )
		//  R		     2*R
		
		double max = 2 * WorldConstants.EARTH_RADIUS_LENGTH
				* Math.sin( naviRadiusKm/( 2 * WorldConstants.EARTH_RADIUS_LENGTH ) );
		return ( euclideanDistance(navCenter, toCheck) < max -0.1 );	
	}
	
//	 Metoda znajduje interesujace nas przeciecia krawedzi z okregiem nawigacji. Najpierw szuka przeciecia plaszczyzny 
//	 zawierajacej okrag nawigacji z plaszczyzna zawierajaca krawedz. Nastepnie szuka przeciecia znalezionej linii ze
//	 sfera (ziemia)
//	 A = (xa, ya, za); B = (xb, yb, zb); C = (xc, yc, zc)
//	 A, B - poczatek i koniec krawedzi; C - centrum okregu nawigacji rozwazanego w metodzie	
	public static PointOnWorldSphere[] findIntersOfEdgeAndNaviCircle(PointOnWorldSphere A, PointOnWorldSphere B, PointOnWorldSphere navCenter, double navRadiusKm)
	{		
		double xa = Transl.coordToCartX(A);
		double ya = Transl.coordToCartY(A);
		double za = Transl.coordToCartZ(A);
		double xb = Transl.coordToCartX(B);
		double yb = Transl.coordToCartY(B);
		double zb = Transl.coordToCartZ(B);
		double xc = Transl.coordToCartX(navCenter);
		double yc = Transl.coordToCartY(navCenter);
		double zc = Transl.coordToCartZ(navCenter);

		double R = WorldConstants.EARTH_RADIUS_LENGTH;		
		
		// r - odleglosc plaszczyzny zawierajacej okrag nawigacji od srodka Ziemi
		double r = distanceOfNaviCircleToSphereCenter(navRadiusKm);
		
		//  x = xc*s	                                               r	
		//	y = yc*s   => x^2 + y^2 + z^2 = r^2  =>	  s = ---------------------------
		//	z = zc*s         							   _  _______________________
		//           							            \/ (xc^2 + yc^2 + zc^2)
		double s = r/Math.sqrt(xc*xc + yc*yc + zc*zc);
		
		// wyznaczenie wektora normalnego do plaszczyzny rozpinanej przez punkty A, B, (0,0,0)
		//  |i  j  k |
		//  |xa ya za| = i(ya*zb- yb*za) + j(za*xb-zb*xa) + k(xa*yb - xb*ya)
		//  |xb yb zb|		
		double[] normVectOfABplane = crossProduct(xa, ya, za, xb, yb, zb);
		
		//wyznaczenie prostej, ktora jest przecieciem plaszczyzny zawierajacej krawedz, oraz plaszczyzny zawierajacej 
		//okrag nawigacji wokol punktu C		
		//  (ya*zb-yb*za)x + (za*xb-zb*xa)y + (xa*yb- xb*ya)z = 0           (*)
		//  xc*x + yc*y + zc*z = xc*xc*s + yc*yc*s + zc*zc*s    inaczej: xc(x - xc*s) + yc(y - yc*s) + zc(z - zc*s) = 0   (*)		
		//  kierunek prostej u (bedacej przecieciem plaszczyzn) to iloczyn wektorowy wektorow normalnych powyzszych plaszczyzn
		double[] lineDirection = crossProduct(normVectOfABplane[0], normVectOfABplane[1], normVectOfABplane[2], xc, yc, zc);

		double a = lineDirection[0];
		double b = lineDirection[1];
		double c = lineDirection[2];
		
		// znalezienie dowolnego punktu zaczepienia nalezacego do rozpatrywanej prostej
		double[] P = findAnyPointOfIntersactionOfTwoPlanes(normVectOfABplane[0], normVectOfABplane[1], normVectOfABplane[2], 0,
				   xc, yc, zc, (xc*xc + yc*yc + zc*zc)*s);
		double Px = P[0];
		double Py = P[1];
		double Pz = P[2];		
		
		// Rownanie prostej u:
		// (x,y,z) = (Px, Py, Pz) + (a*t, b*t, c*t)
		 
		// ZNALEZIENIE PUNKTU PRZECIECIA PROSTEJ ZE SFERA (ZIEMIA)		
		// Podstawienie rownania prostej do rownania sfery:
		//  (Px + a*t)^2 + (Py + b*t)^2 + (Pz + c*t)^2 = earthRadius^2 
		//  (a*a + b*b + c*c)*t^2 + 2(Px*a + Py*b + Pz*c)t + (Px^2 + Py^2 + Pz^2 - earthRadius^2) = 0
		
		// wyliczenie parametru "t" (wyliczenie pierwiastkow rownania kwadratowego)
		EquationSolutions quadrSols = EquationsSolving.solveQuadraticEquation(
				a*a + b*b + c*c,
				2*(Px*a + Py*b + Pz*c), 
				Px*Px + Py*Py + Pz*Pz - R*R);		
		
		double x, y, z;		
		PointOnWorldSphere toAdd;
		ArrayList<PointOnWorldSphere> intersections = new ArrayList<PointOnWorldSphere>();
		for(int i = 0; i < quadrSols.numberOfSolutions(); i++)
		{
			x = Px + a*quadrSols.getSolution(i);
			y = Py + b*quadrSols.getSolution(i);
			z = Pz + c*quadrSols.getSolution(i);
			toAdd = new PointOnWorldSphere(Transl.xyzToLongit(x, y), Transl.xyzToLat(z));
			if( isPointBetweenPointsOnEdge(A, B, toAdd) )			
				intersections.add(toAdd);			
		}
		
		PointOnWorldSphere[] intersectionsArray = new PointOnWorldSphere[intersections.size()];
		for(int i = 0; i<intersections.size(); i++)		
			intersectionsArray[i] = intersections.get(i);
		
		return intersectionsArray;
	}
	
	
	/**
	 *  BARDZO WAZNE ZALOZENIE: PLASZCZYZNY NIE MOGA BYC ROWNOLEGLE DO SIEBIE (CO ZA TYM IDZIE NIE MOGA SIE POKRYWAC)
	 */
	// metoda zwraca jakis punkt (w postaci tablicy trzyelementowej przetrzymujacej wspolrzedne kartezjanskie)
	// zaczepienia prostej (bedacej przecieciem dwoch plaszczyzn)
	// rownania plazczyzn:
	// A1x + B1y + C1z = D1  oraz A2x + B2y + C2z = D2 
	public static double[] findAnyPointOfIntersactionOfTwoPlanes(double A1, double B1, double C1, double D1,
																  double A2, double B2, double C2, double D2)
	{	
		double[][] Amatrix;
		double[] bMatrix;
		double Px, Py, Pz;
		
		// 3 katy bedace katem wzajemnego nachylenia prostych otrzymanych przez wyzerowanie jednej 
		// ze wspolrzednych w obu plaszczyznach
		double angle1;
		double angle2;
		double angle3;
		// jesli prosta jest zdegenerowana, trzeba sztucznie ustawic kat na bardzo wysoki, tak aby ta kombinacja 
		// prostych nie zostala pozniej wybrana
		if( (B1 == 0 && C1 == 0) || (B2 == 0 && C2 == 0) )		
			angle1 = 10;
		else
			angle1 = angleBetweenVectorsInRad(B1, C1, B2, C2);
		if( (A1 == 0 && C1 == 0) || (A2 == 0 && C2 == 0) )		
			angle2 = 10;
		else
			angle2 = angleBetweenVectorsInRad(A1, C1, A2, C2);
		if( (A1 == 0 && B1 == 0) || (A2 == 0 && B2 == 0) )		
			angle3 = 10;
		else
			angle3 = angleBetweenVectorsInRad(A1, B1, A2, B2);				
		
		// sprawdzamy ktora kombinacja prostych jest nachylona do siebie nawzajem pod najwiekszym katem
		// (dla stabilnosci numerycznej)
		// W zaleznosci od tego zerujemy dana wspolrzedna, aby otrzymac uklad dwoch rownan z dwoma niewiadomymi
		int whichVariableReset = Auxiliary.theNearestTo90Degrees(angle1, angle2, angle3);
		if( whichVariableReset == 0)
		{
			Amatrix = new double[][]{ {B1, C1} , {B2, C2} }; 
			bMatrix = new double[]{D1, D2 };
			EquationSolutions linSols = EquationsSolving.solveLinEquations2x2(Amatrix, bMatrix);

			Px = 0; // tak ustalilismy
			Py = linSols.getFirstSolution();
			Pz = linSols.getSecondSolution();		
		}	
		else if( whichVariableReset == 1 )
		{
			Amatrix = new double[][]{ {A1, C1} , {A2, C2} }; 
			bMatrix = new double[]{D1, D2 };
			EquationSolutions linSols = EquationsSolving.solveLinEquations2x2(Amatrix, bMatrix);

			Px = linSols.getFirstSolution();
			Py = 0; // tak ustalilismy
			Pz = linSols.getSecondSolution();		
		}
		else 
		{
			Amatrix = new double[][]{ {A1, B1} , {A2, B2} }; 
			bMatrix = new double[]{D1, D2 };
			EquationSolutions linSols = EquationsSolving.solveLinEquations2x2(Amatrix, bMatrix);

			Px = linSols.getFirstSolution();
			Py = linSols.getSecondSolution();
			Pz = 0; // tak ustalilismy		
		}
		return new double[]{Px, Py, Pz};
	}
		
	// TESTED
	public static double determinant2x2(double a11, double a12, 
			 double a21, double a22)
	{
		return a11*a22 - a21*a12;
	}
	
	// A*A + B*B + C*C > 0
	// plaszczyzna Ax + By + Cz = D
	// TESTED
	public static double distanceOfPointToPlane(double Px, double Py, double Pz, double A, double B, double C, double D)
	{
		return Math.abs(A*Px + B*Py + C*Pz - D) / Math.sqrt(A*A + B*B + C*C);				
	}	
	
	
	
	/**
	 *  BARDZO WAZNE ZALOZENIE: punkt poczatkowy i koncowy krawedzi musi sie znajdowac w terytorium podanym w argumentach
	 */
	public static boolean edgeInNavigationCircles(PointOnWorldSphere point1, PointOnWorldSphere point2, PointOnWorldSphere[] centers, double naviRadiusKm)
	{
		double[] naviRadiusesKm = new double[centers.length];
		for(int i = 0; i < naviRadiusesKm.length; i++){
			naviRadiusesKm[i] = naviRadiusKm;
		}		
		return edgeInNavigationCircles(point1, point2, centers, naviRadiusesKm);
	}
	public static boolean edgeInNavigationCircles(PointOnWorldSphere point1, PointOnWorldSphere point2, PointOnWorldSphere[] centers, double[] naviRadiusesKm)
	{
		ArrayList<PointOnWorldSphere> allCirclesInters = new ArrayList<PointOnWorldSphere>();  // przeciecia ze wszystkimi okregami 
		PointOnWorldSphere[] oneCircleInters;  // przeciecia z jednym z okregow (tymczasowym)
		boolean edgeBelongs = true;	
		
		for( int i = 0; i<centers.length; i++)
		{		
			oneCircleInters = findIntersOfEdgeAndNaviCircle(point1, point2, centers[i], naviRadiusesKm[i]);			
			for(int j = 0; j<oneCircleInters.length; j++)				
				allCirclesInters.add(oneCircleInters[j]);			
		}		
		for(int i = 0; i<allCirclesInters.size(); i++)
		{
			edgeBelongs = false;
			// ....sprawdzymy czy przeciecie jest w zasiegu ktoregokolwiek miasta
			for(int j = 0; j<centers.length; j++)
			{
				if( isPointInNavRange(centers[j], allCirclesInters.get(i), naviRadiusesKm[j]) )
				{
					edgeBelongs = true;		
					break;
				}
			}
			if( !edgeBelongs )			
				break;			
		}		
		return edgeBelongs;
	}
	
	
	
	
}
