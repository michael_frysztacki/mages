package mapcom.math;

import static org.junit.Assert.*;
import mapcom.worldstructure.Point;

import org.junit.Test;

public class PointOnWorldSphereTest {

	double delta = 0.00000000001;
	
	@Test
	public void compareTheSamePoints() 
	{
		PointOnWorldSphere A = new PointOnWorldSphere(60.0, 89.9999);
		PointOnWorldSphere B = new PointOnWorldSphere(60.0, 88.9999);
		
		assertFalse( A.equals(B) );
	}
	
	@Test
	public void comparePointsWithDifferentLatitudes() 
	{
		PointOnWorldSphere A = new PointOnWorldSphere(60.0, 89.0);
		PointOnWorldSphere B = new PointOnWorldSphere(60.0, 88.9999);
		
		assertFalse( A.equals(B) );
	}
	
	@Test
	public void isCreatedAntipodalPointHasIndeedOppositeCoordinates()
	{
		PointOnWorldSphere point = new PointOnWorldSphere(10, 20);
		PointOnWorldSphere antipodalPoint = point.createAntipodalPoint();
		assertEquals(-10, antipodalPoint.getLongit(), delta);
		assertEquals(-20, antipodalPoint.getLat(), delta);
	}

}
