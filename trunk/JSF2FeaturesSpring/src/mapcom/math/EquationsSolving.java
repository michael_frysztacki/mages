package mapcom.math;

public class EquationsSolving
{
	public static EquationSolutions solveQuadraticEquation(double a, double b, double c)
	{
		double firstSolution;
		double secondSolution;
		double delta = b*b - 4*a*c;		
		
		if(delta<0)
		{
			//			System.out.println("rownanie nie ma pierwiastkow rzeczywistych");			
			return new EquationSolutions();			
		}
		else
		{
			if(delta > 0)
			{
				double deltaSqRoot = Math.sqrt(delta);
				firstSolution = (-b - deltaSqRoot)/(2*a);
				secondSolution = (-b + deltaSqRoot)/(2*a);				
				return new EquationSolutions(firstSolution, secondSolution);
			}
			else
			{
				firstSolution = -b/(2*a);				
				return new EquationSolutions(firstSolution);			
			}
		}
	}
	
	public static EquationSolutions solveLinEquations2x2(double[][] A, double[] b)
	{		
		double[] tSolution = new double[2];
		double m;		
		//sprawdzenie w ktorym wierszu jest wiekszy co do modulu element
		if( Math.abs(A[0][0]) >= Math.abs(A[1][0]) )
		{	
			m = A[1][0]/A[0][0];
			A[1][1] = A[1][1] - m*A[0][1];
			tSolution[1] = (b[1] - m*b[0])/A[1][1];
			tSolution[0] = (b[0] - A[0][1]*tSolution[1])/A[0][0];
			return new EquationSolutions(tSolution[0], tSolution[1]);
							
		}
		else
		{
			m = A[0][0]/A[1][0];
			A[0][1] = A[0][1] - m*A[1][1];
			tSolution[1] = (b[0]-m*b[1])/A[0][1];
			tSolution[0] = (b[1] - A[1][1]*tSolution[1])/A[1][0];
			return new EquationSolutions(tSolution[0], tSolution[1]);
		}				
	}
}
