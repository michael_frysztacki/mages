package mapcom.math;

public class EquationSolutions
{
	private double[] solutions;
//	boolean ifInfiniteNumberOfSolutions;
	public EquationSolutions(double... solutions)
	{
		this.solutions = new double[solutions.length];
		for(int i = 0; i<solutions.length; i++)
		{
			this.solutions[i] = solutions[i];
		}
	}
	public int numberOfSolutions()
	{
		return solutions.length;
	}
	public double getSolution(int which)
	{
		return solutions[which];
	}
	public double getFirstSolution()
	{
		return solutions[0];
	}
	public double getSecondSolution()
	{
		return solutions[1];
	}
	public boolean ifHasNoSolutions()
	{
		return solutions.length == 0;
	}
	public boolean ifHasOneSolution()
	{
		return solutions.length == 1;
	}
	public boolean ifHasTwoSolutions()
	{
		return solutions.length == 2;
	}
}
