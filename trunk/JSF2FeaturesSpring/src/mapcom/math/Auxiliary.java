package mapcom.math;

public class Auxiliary
{
	public static double numberToIntegerPower(double number, int n)
	{
		double result = 1.0;
		if( n >= 0)		
			for(int i=0; i<n; i++)
				result = result*number;		
		else
			for(int i=n; i<0; i++)
				result = result/number;		
		return result;	
	}
	
	// returns 0, 1, 2 depending on which element is the nearest to 90 degrees
	public static int theNearestTo90Degrees(double angle1, double angle2, double angle3)
	{		
		double norm1 = Math.abs((angle1 - Math.PI/2));
		double norm2 = Math.abs((angle2 - Math.PI/2));
		double norm3 = Math.abs((angle3 - Math.PI/2));
		if( norm1 < norm2 && norm1 < norm3 )
			return 0;
		else if( norm2 < norm3 )
			return 1;
		else return 2;			
	}
	
}
