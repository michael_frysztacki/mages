package mapcom.math;

import java.io.Serializable;

public class PointOnWorldSphere implements Serializable
{
	private static final long serialVersionUID = 1L;
	final protected double latitude;	// in degrees    70 east - przetrzymywane jako 70, 45 west - przetrzymywane jako -45
	final protected double longitude;	// in degrees	70 North - przetrzymywane jako -70, 45 South - przetrzymywane jako 45
	
	public PointOnWorldSphere(double longitude, double latitude)
	{
		this.longitude = longitude;
		this.latitude = latitude;
	}	
	
	public double getLongit() {
		return longitude;
	}
	
	public double getLat() {
		return latitude;
	}
	
	public PointOnWorldSphere createAntipodalPoint()
	{
		return new PointOnWorldSphere(-longitude, -latitude);
	}
	
	public String toString(){
		String s = "PointOnWorldSphere: " + "(" + longitude +", " + latitude + ");";
		return s;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(latitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(longitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PointOnWorldSphere other = (PointOnWorldSphere) obj;
		if (Double.doubleToLongBits(latitude) != Double
				.doubleToLongBits(other.latitude))
			return false;
		if (Double.doubleToLongBits(longitude) != Double
				.doubleToLongBits(other.longitude))
			return false;
		return true;
	}

}