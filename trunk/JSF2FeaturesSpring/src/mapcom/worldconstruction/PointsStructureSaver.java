package mapcom.worldconstruction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import mapcom.tools.fileproc.TxtFileProcessing;
import mapcom.worldstructure.Capital;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.PointType;
import mapcom.worldstructure.PointsStructure;

public class PointsStructureSaver extends PointsStructureTxtFileProcessor
{
	public void save(PointsStructure ps, String pointsDirPath) throws IOException
	{
		saveAllPointsToFiles(ps, pointsDirPath);
	}

	public void saveAllPointsToFiles(PointsStructure ps, String directoryPath) throws IOException
	{		
		saveInterPointsToFiles(ps, directoryPath);
		saveCapitalsToFiles(ps, directoryPath);
		saveColoniesToFiles(ps, directoryPath);		
	}
		public void saveCapitalsToFiles(PointsStructure ps, String dirPath) throws IOException
		{
			PointsStructureExtractor filter = new PointsStructureExtractor();
			PointsStructure capitals = filter.extractCapitals(ps);
			savePointsToFiles(capitals, dirPath + capitalsDirName);
						
			ArrayList<String> stringNames = new ArrayList<String>();
			Iterator<Point> it = capitals.iterator();
			Capital tempCapital; 
			while(it.hasNext())
			{
				tempCapital = (Capital)it.next();
				stringNames.add( tempCapital.getName() );
			}
			TxtFileProcessing.saveLinesToFile(stringNames, dirPath + capitalsDirName + capitalNamesFileName);
		}
		public void saveColoniesToFiles(PointsStructure ps, String dirPath) throws IOException
		{
			PointsStructureExtractor filter = new PointsStructureExtractor();
			PointsStructure colonies = filter.extractColonies(ps);
			savePointsToFiles(colonies, dirPath + coloniesDirName);
		}
		public void saveInterPointsToFiles(PointsStructure ps, String dirPath) throws IOException
		{
			PointsStructureExtractor filter = new PointsStructureExtractor();
			PointsStructure interPoints = filter.extractInterPoints(ps);
			savePointsToFiles(interPoints, dirPath + interPointsDirName);
		}
		public void savePointsToFiles(PointsStructure ps, String dirPath) throws IOException
		{
			ArrayList<String> stringLongit = new ArrayList<String>();
			ArrayList<String> stringLat = new ArrayList<String>();
			ArrayList<String> stringIds = new ArrayList<String>();
			ArrayList<String> stringIsWater = new ArrayList<String>();
			ArrayList<String> stringIsPolar = new ArrayList<String>();
			Iterator<Point> it = ps.iterator();
			Point tempPoint; 
			while(it.hasNext())
			{
				tempPoint = it.next();
				stringLongit.add( Double.toString(tempPoint.getLongit()) );
				stringLat.add( Double.toString(tempPoint.getLat()) );
				stringIds.add( Integer.toString(tempPoint.getId()) );
				stringIsWater.add( Boolean.toString(tempPoint.isWater()) );
				stringIsPolar.add( Boolean.toString(tempPoint.isPolar()) );
			}
			TxtFileProcessing.saveLinesToFile(stringLongit, dirPath + longitudesFileName);
			TxtFileProcessing.saveLinesToFile(stringLat, dirPath + latitudesFileName);
			TxtFileProcessing.saveLinesToFile(stringIds, dirPath + idsFileName);
			TxtFileProcessing.saveLinesToFile(stringIsWater, dirPath + isWaterFileName);
			TxtFileProcessing.saveLinesToFile(stringIsPolar, dirPath + isPolarFileName);
		}
		
}
