package mapcom.worldconstruction;

import java.util.Iterator;

import mapcom.worldstructure.Edge;
import mapcom.worldstructure.EdgesStructure;

public class EdgesStructureHelper {
	
	public boolean containTheSameEdges(EdgesStructure es1, EdgesStructure es2)
	{
		// analogia do matematycznego rozumowania: 
		// Jesli A zawiera sie w B i licznosc(A) = licznosc(B), to A = B
		return firstEdgesStructureContainsAllEdgesOfTheSecondEdgesStructure(es1, es2) && 
			   edgesHaveTheSameNumberOfEdges(es1, es2);
	}
		private boolean edgesHaveTheSameNumberOfEdges(EdgesStructure es1, EdgesStructure es2)
		{
			return EdgesStructureStatistician.numberOfAllEdges(es1) == EdgesStructureStatistician.numberOfAllEdges(es2);
		}
		private boolean firstEdgesStructureContainsAllEdgesOfTheSecondEdgesStructure(EdgesStructure es1, EdgesStructure es2)
		{
			Iterator<Iterator<Edge>> outerIt = es2.outerIterator();
			Iterator<Edge> innerIt;
			Edge temp;
			int tempId1;
			int tempId2;
			
			while(outerIt.hasNext()){
				innerIt = outerIt.next();	
				while( innerIt.hasNext() ){
					temp = innerIt.next();
					tempId1 = temp.getStartPoint().getId();
					tempId2 = temp.getEndPoint().getId();
					if( !temp.equals( es1.getEdge(tempId1, tempId2)) )
							return false;
				}				 
			}
			return true;
		}
		
	public void copyContent(EdgesStructure source, EdgesStructure emptyTarget){
		EdgesStructure target = emptyTarget;
		Iterator<Iterator<Edge>> outerIt = source.outerIterator();
		Iterator<Edge> innerIt;
		Edge tempEdge;
		
		while(outerIt.hasNext()){
			innerIt = outerIt.next();	
			while( innerIt.hasNext() ){
				tempEdge = innerIt.next();
				target.addEdgeAndReversedEdge(tempEdge);
			}				 
		}
	}

}
