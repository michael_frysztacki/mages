package mapcom.worldconstruction;

import java.util.Iterator;

import mapcom.worldstructure.Edge;
import mapcom.worldstructure.EdgesStructure;

public class EdgesStructureStatistician 
{
	public static int numberOfAllEdges(EdgesStructure es)
	{
		Iterator<Iterator<Edge>> outerIt = es.outerIterator();
		Iterator<Edge> innerIt;
		
		int numberOfAllEdges = 0;		
		while(outerIt.hasNext())
		{
			innerIt = outerIt.next();			
			numberOfAllEdges += numberOfEdgesStartingInPoint(innerIt); 
		}
		return numberOfAllEdges;
	}

	public static int numberOfEdgesStartingInPoint(EdgesStructure es, int pointId)
	{
		return numberOfEdgesStartingInPoint(es.innerIterator(pointId));
	}
		private static int numberOfEdgesStartingInPoint(Iterator<Edge> innerIterator)
		{		
			int counter = 0;
			while(innerIterator.hasNext())
			{
				counter++;
				innerIterator.next();
			}
			return counter;
		}
}
