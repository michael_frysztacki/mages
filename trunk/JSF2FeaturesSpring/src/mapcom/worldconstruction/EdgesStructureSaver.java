
package mapcom.worldconstruction;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import mapcom.worldstructure.EdgesStructure;
import mapcom.worldstructure.EdgesStructureOnHashMaps;
import mapcom.worldstructure.IntermediateEdgesStructure;

public class EdgesStructureSaver {
	
	private static EdgesStructureHelper helper = new EdgesStructureHelper();
	
	public void saveEdges(EdgesStructure edges, String edgesPath) throws IOException{
		EdgesStructure intermediateEdgesStructure = new EdgesStructureOnHashMaps();
		helper.copyContent(edges, intermediateEdgesStructure);
		
		FileOutputStream fos = null;
		ObjectOutputStream out = null;
		try {
			fos = new FileOutputStream(edgesPath);
			out = new ObjectOutputStream(fos);
			out.writeObject(intermediateEdgesStructure);
			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}		
	}
} 



//package mapcom.worldconstruction;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Iterator;
//
//import mapcom.tools.fileproc.TxtFileProcessing;
//import mapcom.worldstructure.Edge;
//import mapcom.worldstructure.EdgesStructure;
//
//public class EdgesStructureSaver
//{
//	public void saveEdges(EdgesStructure edges, String edgesPath) throws IOException
//	{				
//		ArrayList<String> edgesInStrings = new ArrayList<String>();
//		Edge tempEdge = null;
//		Iterator<Iterator<Edge>> outerIt = edges.outerIterator();
//		Iterator<Edge> innerIt;
//		
//		while(outerIt.hasNext()){			
//			innerIt = outerIt.next();
//			edgesInStrings = new ArrayList<String>();
//			
//			while( innerIt.hasNext() ){			
//				tempEdge = innerIt.next();
//				edgesInStrings.add( EdgeProcessing.edgeToString(tempEdge) );				 
//			}
//			TxtFileProcessing.saveLinesToFile(edgesInStrings, edgesPath + tempEdge.getStartPoint().getId() + ".txt");
//		}		
//	} 
//}
