package mapcom.worldconstruction;

import mapcom.mapprojections.MapProjection;
import mapcom.worldstructure.EdgesStructure;
import mapcom.worldstructure.PointsStructure;

public class WorldGraphBuilder
{
	protected PointsStructure points;
	protected EdgesStructure edges;
	final protected SurfaceCheckingType surfaceCheckingType;
		
	public WorldGraphBuilder(MapProjection proj, PointsStructure points, SurfaceCheckingType surfaceCheckingType)
	{
		this.surfaceCheckingType = surfaceCheckingType;
		PointsStructureExtractor psf = new PointsStructureExtractor();
		this.points = psf.extract(points, surfaceCheckingType);
		
		EdgesStructureBuilder edgesStructureBuilder = new EdgesStructureBuilder();
		this.edges = edgesStructureBuilder.build(edges, proj, points, surfaceCheckingType);
		
	}
	public PointsStructure getPoints()
	{
		return this.points;
	}
	public EdgesStructure getEdges()
	{
		return this.edges;
	}
	public SurfaceCheckingType getSurfaceCheckingType() {
		return surfaceCheckingType;
	}
	
	
}
