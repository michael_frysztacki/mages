package mapcom.worldconstruction;

import java.util.ArrayList;
import java.util.Iterator;

import mapcom.imgproc.Colors;
import mapcom.mapprojections.MapProjection;
import mapcom.math.GeometryCalc;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.PointsStructure;

public class PointsVerifier
{
	public boolean isCorrectPointsStructure(PointsStructure points, MapProjection proj)
	{
		return !thereAreAtLeastTwoPointsWithTheSameCoordinants(points) && thereArePointsWithIncorrectSurfaces(points, proj);
	}
	
	public boolean thereAreAtLeastTwoPointsWithTheSameCoordinants(PointsStructure points)
	{
		Iterator<Point> iter1 = points.iterator();
		Iterator<Point> iter2 = points.iterator();
		Point temp1, temp2;
		while(iter1.hasNext())
		{
			temp1 = iter1.next();
			while(iter2.hasNext())
			{
				temp2 = iter2.next();
				// zabezpieczamy, aby zadna para nie byla znaleziona dwa razy
				if(temp1.getId() < temp2.getId())
					if( GeometryCalc.haveTheSameCoordinates(temp1, temp2) )
						return true;						
			}	
		}
		return false;
	}
	
	public boolean thereArePointsWithIncorrectSurfaces(PointsStructure points, MapProjection proj)
	{
		Iterator<Point> it = points.iterator();
		Point tempPoint;
		while( it.hasNext() )
		{
			tempPoint = it.next();
			if( !isSurfaceOfPointCorrect(tempPoint, proj))
				return false;
		}
		return false;
	}				
	public void switchSurfaceTypeOfIncorrectPoints(PointsStructure points, MapProjection proj)
	{
		Iterator<Point> it = points.iterator();
		Point tempPoint;
		while( it.hasNext() )
		{
			tempPoint = it.next();
			if( !isSurfaceOfPointCorrect(tempPoint, proj))
				switchLandTypeOfPoint(tempPoint);
		}		
	}
		private void switchLandTypeOfPoint(Point point)
		{
			if( point.isWater() )
				point.setIsWater(false);
			else 
				point.setIsWater(true);
		}
	public boolean isSurfaceOfPointCorrect(Point point, MapProjection proj)
	{
		int pointRGB = proj.getRGB(point);
		if( point.isWater() )		
			if( !Colors.isWaterColor(pointRGB) )
				return false;
		if( !point.isWater() )		
			if( !Colors.isLandColor(pointRGB) )
				return false;
		return true;
		
	}
	
	
	public ArrayList<Point> pairesOfPointsWithTheSameCoordinants(PointsStructure points)
	{
		ArrayList<Point> incorrectPoints = new ArrayList<Point>();
		
		Iterator<Point> iter1 = points.iterator();
		Iterator<Point> iter2;
		Point temp1, temp2;
		while(iter1.hasNext())
		{
			temp1 = iter1.next();
			iter2 = points.iterator();
			while(iter2.hasNext())
			{
				temp2 = iter2.next();
				// zabezpieczamy, aby zadna para nie byla znaleziona dwa razy
				if(temp1.getId() < temp2.getId())
					if( temp1.hasTheSameCoord(temp2) )
					{
						incorrectPoints.add(temp1);
						incorrectPoints.add(temp2);
					}
					
			}	
		}
		return incorrectPoints;
	}
	public int countPairesOfPointsWithTheSameCoordinants(PointsStructure points)
	{
		return pairesOfPointsWithTheSameCoordinants(points).size()/2;
	}
}
