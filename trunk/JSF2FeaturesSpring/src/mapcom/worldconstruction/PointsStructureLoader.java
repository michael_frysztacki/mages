package mapcom.worldconstruction;

import java.io.IOException;
import java.util.ArrayList;

import mapcom.tools.Various;
import mapcom.tools.fileproc.TxtFileProcessing;
import mapcom.worldstructure.Capital;
import mapcom.worldstructure.Colony;
import mapcom.worldstructure.InterPoint;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.PointsStructure;

public class PointsStructureLoader extends PointsStructureTxtFileProcessor
{	
	public PointsStructure load(PointsStructure ps, String pointsDirPath) throws IOException
	{
		ps = loadAllPointsFromFiles(ps, pointsDirPath);
		return ps;
	}
	
	private PointsStructure loadAllPointsFromFiles(PointsStructure ps,
			String directory) throws IOException
	{
		return loadAllPointsFromFiles(ps, directory + interPointsDirName, directory + coloniesDirName, directory + capitalsDirName);		
	}
		private PointsStructure loadAllPointsFromFiles(PointsStructure ps, 
				String interPointsDirPath, String coloniesDirPath, String capitalsDirPath) throws IOException
		{
			ArrayList<Capital> capitalsToAdd = loadCapitalsFromFiles(capitalsDirPath);			
			ArrayList<Colony> coloniesToAdd = loadColoniesFromFiles(coloniesDirPath);			
			ArrayList<InterPoint> interPointsToAdd = loadInterPointsFromFiles(interPointsDirPath);
			
			addPointsToPointsStructure(ps, capitalsToAdd);
			addPointsToPointsStructure(ps, coloniesToAdd);
			addPointsToPointsStructure(ps, interPointsToAdd);
			return ps;
		}	
			private <T extends Point> void addPointsToPointsStructure(PointsStructure ps, ArrayList<T> pointsToAdd)
			{
				for(Point point: pointsToAdd)				
					ps.addPoint( point );
			}	
	
			private ArrayList<InterPoint> loadInterPointsFromFiles(String interPointsFilePath) throws IOException
			{		
				return loadInterPointsFromFiles(interPointsFilePath + longitudesFileName,
												interPointsFilePath + latitudesFileName,
												interPointsFilePath + idsFileName,
												interPointsFilePath + isWaterFileName, 
												interPointsFilePath + isPolarFileName);
			}
				private ArrayList<InterPoint> loadInterPointsFromFiles(String longitFilePath, String latFilePath, String idsFilePath,
						String ifWatersFilePath, String ifPolarsFilePath) throws IOException
				{					
					ArrayList<InterPoint> interPoints = new ArrayList<InterPoint>();
					ArrayList<Double> longitudes = 
							Various.parseALStringToALDouble( TxtFileProcessing.readLinesFromFile(longitFilePath) );
					ArrayList<Double> latitudes = 
							Various.parseALStringToALDouble( TxtFileProcessing.readLinesFromFile(latFilePath) );
					ArrayList<Integer> ids = 
							Various.parseALStringToALInteger( TxtFileProcessing.readLinesFromFile(idsFilePath) );
					ArrayList<Boolean> ifWaters = 
							Various.parseALStringToALBoolean( TxtFileProcessing.readLinesFromFile(ifWatersFilePath) );
					ArrayList<Boolean> ifPolars = 
							Various.parseALStringToALBoolean( TxtFileProcessing.readLinesFromFile(ifPolarsFilePath) );
					
					for(int i=0; i<longitudes.size(); i++)												
						interPoints.add( new InterPoint(longitudes.get(i), latitudes.get(i), ids.get(i), ifWaters.get(i), ifPolars.get(i) ) );					
					return interPoints;
				}	
			
			private ArrayList<Colony> loadColoniesFromFiles(String coloniesDirPath) throws IOException
			{		
				return loadColoniesFromFiles(coloniesDirPath + longitudesFileName, 
												  coloniesDirPath + latitudesFileName,
   												  coloniesDirPath + idsFileName,
												  coloniesDirPath + isWaterFileName,
												  coloniesDirPath + isPolarFileName);
			}
				
				private ArrayList<Colony> loadColoniesFromFiles(String longitFilePath, String latFilePath, String idsFilePath, String ifWatersFilePath, 
						String ifPolarsFilePath) throws IOException
				{
					ArrayList<InterPoint> interPoints = loadInterPointsFromFiles(longitFilePath, latFilePath, idsFilePath, ifWatersFilePath, ifPolarsFilePath);		
					ArrayList<Colony> colonies = new ArrayList<Colony>();
					for(Point point: interPoints)								
						colonies.add( createAColonyOutOfInterPoint(point) );		
					
					return colonies;		
				}
					private Colony createAColonyOutOfInterPoint(Point interPoint)
					{
						return new Colony( interPoint.getLongit(), interPoint.getLat(), interPoint.getId(), interPoint.isWater(), interPoint.isPolar());
					}
			
			private ArrayList<Capital> loadCapitalsFromFiles(String capitalsDirPath) throws IOException
			{		
				return loadCapitalsFromFiles(capitalsDirPath + longitudesFileName, 
											 capitalsDirPath + latitudesFileName,	
										     capitalsDirPath + idsFileName,
											 capitalsDirPath + isWaterFileName,									 	
											 capitalsDirPath + isPolarFileName, 
											 capitalsDirPath + capitalNamesFileName);
			}	
			
				private ArrayList<Capital> loadCapitalsFromFiles(String longitFilePath, String latFilePath, String idsFilePath, String ifWatersFilePath, 
						String ifPolarsFilePath, String capitalsNamesFilePath) throws IOException
				{
					ArrayList<InterPoint> interPoints = loadInterPointsFromFiles(longitFilePath, latFilePath, idsFilePath, ifWatersFilePath, ifPolarsFilePath);		
					ArrayList<String> capitalsNames = TxtFileProcessing.readLinesFromFile(capitalsNamesFilePath);
					ArrayList<Capital> capitals = new ArrayList<Capital>();
					for(int i=0; i<capitalsNames.size(); i++)			
						capitals.add( createACapitalWithNameOutOfCity(capitalsNames.get(i), interPoints.get(i)) );					
					return capitals;		
				}
					private Capital createACapitalWithNameOutOfCity(String name, Point interPoint)
					{
						return new Capital( interPoint.getLongit(), interPoint.getLat(), interPoint.getId(), name, interPoint.isWater(), interPoint.isPolar());
					}
	


}