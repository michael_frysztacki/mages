package mapcom.worldconstruction;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import mapcom.math.PointOnWorldSphere;
import mapcom.worldstructure.EdgesStructure;
import mapcom.worldstructure.EdgesStructureOnHashMaps;
import mapcom.worldstructure.IntermediateEdgesStructure;

public class EdgesStructureLoader
{
	private static EdgesStructureHelper helper = new EdgesStructureHelper();
	
	public void loadEdges(EdgesStructure edges, String edgesPath) throws IOException{
		EdgesStructure intermediaryEdgesStructure = new EdgesStructureOnHashMaps();
				
		FileInputStream fis = null;
		ObjectInputStream in = null;
	    try {
			fis = new FileInputStream(edgesPath);
			in = new ObjectInputStream(fis);
			intermediaryEdgesStructure = (EdgesStructureOnHashMaps) in.readObject();
			in.close();
	    } catch (Exception ex) {
	      ex.printStackTrace();
	    }
	    
	    helper.copyContent(intermediaryEdgesStructure, edges);
	}
}

//public class EdgesStructureLoader
//{		
//	public EdgesStructure load(EdgesStructure edges, String edgesPath) throws IOException
//	{		
//		ArrayList<String> edgesInStrings;
//		Edge tempEdge;
//
//		File folder = new File(edgesPath);
//		File[] listOfFiles = folder.listFiles();
//
//		String fileName;
//		for (int i = 0; i < listOfFiles.length; i++) 
//		{
//			if (listOfFiles[i].isFile()) 
//		    {
//				fileName = listOfFiles[i].getName();
//				edgesInStrings = TxtFileProcessing.readLinesFromFile(edgesPath + fileName);
//				for(int j=0; j<edgesInStrings.size(); j++)
//				{
//					tempEdge = EdgeProcessing.parseStringToEdge(edgesInStrings.get(j));
//					edges.addEdgeAndReversedEdge(tempEdge);			
//				}		
//		    }
//		}
//		return edges;
//	}
//	
//
//}
