package mapcom.worldconstruction;

import mapcom.imgproc.Colors;
import mapcom.mapprojections.MapProjection;
import mapcom.math.PointOnWorldSphere;
import mapcom.worldconstruction.EdgeBuilder.TerrainInformation;
import mapcom.worldstructure.Edge;
import mapcom.worldstructure.Point;

public class LandChecking implements SurfaceCheckingType
{	
	private static EdgeBuilder builder = new EdgeBuilder();
	
	@Override
	public boolean checkSurfaceOnSpot(MapProjection proj, PointOnWorldSphere spotToCheck){		
		return Colors.isLandColor(proj.getRGB(spotToCheck));		
	}
	
	@Override
	public boolean mightBelongToEdge(Point point){		
		return !(point.getClass() == Point.class && point.isWater());
	}
	
	@Override
	public Edge createEdge(Point start, Point end, MapProjection proj){
		TerrainInformation terrInfo = builder.getTerrainInfo(start, end, proj);
		return new Edge(start, end, terrInfo.typesOfTerrainsTraversed, terrInfo.percentagesOfTerrainsTraversed);
	}
}

