package mapcom.worldconstruction;

import mapcom.mapprojections.MapProjection;
import mapcom.math.PointOnWorldSphere;
import mapcom.worldstructure.Edge;
import mapcom.worldstructure.Point;


// uzywany podczas konstrukcji grafu, narazie posiadamy dwa rodzaje grafow:
// - wodny 
// - ladowy
// Ale byc moze w przyszlosci dojda inne np. polar graph. Wtedy nie bedzie trzeba modyfikowac metod tworzenia grafu, jedynie
// dodac nowy surfaceCheckingType z odpowiednimi metodami

public interface SurfaceCheckingType
{
	public boolean checkSurfaceOnSpot(MapProjection proj, PointOnWorldSphere spotToCheck);
	public boolean mightBelongToEdge(Point A);
	public Edge createEdge(Point A, Point B, MapProjection proj);
}

