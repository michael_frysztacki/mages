package mapcom.worldconstruction;

public class PointsStructureTxtFileProcessor 
{
	protected final String longitudesFileName = "Longitude.txt";
	protected final String latitudesFileName = "Latitude.txt";
	protected final String idsFileName = "Ids.txt";
	protected final String isWaterFileName = "Water.txt";
	protected final String isPolarFileName = "Polar.txt";
	protected final String capitalNamesFileName = "Names.txt";
	
	protected final String interPointsDirName = "InterPoints/";
	protected final String coloniesDirName = "Colonies/";
	protected final String capitalsDirName = "Capitals/";
}
