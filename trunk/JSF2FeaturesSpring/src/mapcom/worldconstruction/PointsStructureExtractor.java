package mapcom.worldconstruction;

import java.util.Iterator;

import mapcom.math.GeometryCalc;
import mapcom.worldstructure.Capital;
import mapcom.worldstructure.Colony;
import mapcom.worldstructure.InterPoint;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.PointsStructure;

public class PointsStructureExtractor
{
	public PointsStructure extractPointsForWaterGraph(PointsStructure points)
	{
		return extractPointsWithForAGivenSurfaceCheckingType(points, new WaterChecking());
	}
	
	public PointsStructure extractPointsForLandGraph(PointsStructure points)
	{
		return extractPointsWithForAGivenSurfaceCheckingType(points, new LandChecking());
	}
		public PointsStructure extractPointsWithForAGivenSurfaceCheckingType(PointsStructure points, SurfaceCheckingType surfType)
		{
			PointsStructure extracted = points.createEmptyPointsStructure();
			Iterator<Point> iter = points.iterator();
			Point tempPoint;
			while( iter.hasNext() )
			{
				tempPoint = iter.next();
				if( surfType.mightBelongToEdge(tempPoint) )
					extracted.addPoint(tempPoint);
			}
			return extracted;
		}

	public PointsStructure extractPointsInUserRange(PointsStructure points, Point[] userCities, int naviRadiusKm)
	{
		PointsStructure extracted = points.createEmptyPointsStructure();
		Iterator<Point> iter = points.iterator();
		Point tempPoint;
		while( iter.hasNext() )
		{			
			tempPoint = iter.next();
			for(int i = 0; i < userCities.length; i++){
				if( GeometryCalc.isPointInNavRange(userCities[i], tempPoint, naviRadiusKm) ){					
					extracted.addPoint(tempPoint);	
					break;
				}
			}				
		}
		return extracted;
	}
	// TODO poprawic trzy ponizsze metody 
	public PointsStructure extractCapitals(PointsStructure ps)
	{
		Capital capital = new Capital(0, 0, 0, "", true, true);
		PointsStructure capitals = ps.createEmptyPointsStructure();
		Iterator<Point> iter = ps.iterator();
		Point tempPoint;
		while( iter.hasNext() )
		{
			tempPoint = iter.next();
			if( tempPoint.getClass() == capital.getClass() )
				capitals.addPoint(tempPoint);
		}
		return capitals;
	}
	public PointsStructure extractColonies(PointsStructure ps)
	{
		Colony colony = new Colony(0, 0, 0, true, true);
		PointsStructure colonies = ps.createEmptyPointsStructure();
		Iterator<Point> iter = ps.iterator();
		Point tempPoint;
		while( iter.hasNext() )
		{
			tempPoint = iter.next();
			if( tempPoint.getClass() == colony.getClass() )
				colonies.addPoint(tempPoint);
		}
		return colonies;
	}		
	public PointsStructure extractInterPoints(PointsStructure ps)
	{
		InterPoint interPoint = new InterPoint(0, 0, 0, true, true);
		PointsStructure interPoints = ps.createEmptyPointsStructure();
		Iterator<Point> iter = ps.iterator();
		Point tempPoint;
		while( iter.hasNext() )
		{
			tempPoint = iter.next();
			if( tempPoint.getClass() == interPoint.getClass() )
				interPoints.addPoint(tempPoint);
		}
		return interPoints;
	}
}
