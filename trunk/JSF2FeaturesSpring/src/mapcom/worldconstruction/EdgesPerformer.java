package mapcom.worldconstruction;

import java.util.Iterator;

import mapcom.worldstructure.Edge;
import mapcom.worldstructure.EdgesStructure;

public class EdgesPerformer
{
	public int countEdges(EdgesStructure edges)
	{
		Edge temp;
		Iterator<Iterator<Edge>> outerIt = edges.outerIterator();
		Iterator<Edge> innerIt;
		int counter = 0; 
		int i = 0;
		while(outerIt.hasNext())
		{
			innerIt = outerIt.next();
			while( innerIt.hasNext() )
			{
				innerIt.next();
				counter++;
			}
		}
		return counter;
	}
}
