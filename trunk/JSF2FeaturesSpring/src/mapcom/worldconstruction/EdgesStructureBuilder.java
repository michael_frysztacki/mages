package mapcom.worldconstruction;

import java.util.Iterator;

import mapcom.mapprojections.MapProjection;
import mapcom.worldstructure.Edge;
import mapcom.worldstructure.EdgesStructure;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.PointsStructure;

public class EdgesStructureBuilder
{

	public EdgesStructure build(EdgesStructure es, MapProjection proj, PointsStructure points, 
			SurfaceCheckingType surf)
	{		
		//comment
		int i = 0;
		Iterator<Point> outerIt = points.iterator();
		while( outerIt.hasNext() )
		{					System.out.println(i);i++;		
			Point tempPoint = outerIt.next();
			createEdgesWithEndsInPoint(es, proj, points, tempPoint, surf);			
		}	
		return es;
	}
	
	public void createEdgesWithEndsInPoint(EdgesStructure es, MapProjection proj, PointsStructure points, Point point,
			SurfaceCheckingType surf)
	{
		Edge temp;
		Point tempPoint;
		Iterator<Point> it = points.iterator();				
		while( it.hasNext() )
		{
			tempPoint = it.next();
			
			// jesli sprawdzimy czy krawedz istnieje dla id "i" oraz "j" to nie trzeba sprawdzac dla "j" oraz "i"
			if( point.getId() < tempPoint.getId()){
				
				// koncowe punkty krawedzi spelniaja zalozenia --> sprawdzamy krawedz bardzo dokladnie
				if( EdgeBuilder.checkIfEdgeIsOfSurfaceType( proj, point, tempPoint, surf) ){
					temp = surf.createEdge( point, tempPoint, proj);
					es.addEdgeAndReversedEdge(temp);
				}
			}
		}	
	}
}
