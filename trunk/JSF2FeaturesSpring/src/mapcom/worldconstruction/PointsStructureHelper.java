package mapcom.worldconstruction;

import java.util.Iterator;

import mapcom.worldstructure.Point;
import mapcom.worldstructure.PointsStructure;

public class PointsStructureHelper {
	public boolean containTheSamePoints(PointsStructure ps1, PointsStructure ps2)
	{
		// analogia do matematycznego rozumowania: 
		// Jesli A zawiera sie w B i licznosc(A) = licznosc(B), to A = B
		return firstPointsStructureContainAllPointsOfTheSecondPointsStructure(ps1, ps2) && 
			   ps1.size() == ps2.size();
	}
		private boolean firstPointsStructureContainAllPointsOfTheSecondPointsStructure(PointsStructure ps1, PointsStructure ps2)
		{
			Iterator<Point> it = ps2.iterator();			
			Point temp;
			
			while( it.hasNext() ){
				temp = it.next();
				if( !temp.equals( ps1.getPointWithId(temp.getId())) )
						return false;
			}			
			return true;
		}		
}
