package mapcom.worldconstruction;

import java.util.ArrayList;

import mapcom.imgproc.Colors;
import mapcom.mapprojections.MapProjection;
import mapcom.math.GeometryCalc;
import mapcom.math.PointOnWorldSphere;
import mapcom.math.Transl;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.TerrainType;

public class EdgeBuilder
{	
	public TerrainInformation getTerrainInfo(Point A, Point B, MapProjection proj){
		return getTerrainInfo(Transl.coordToCartX(A), Transl.coordToCartY(A), Transl.coordToCartZ(A),
					          Transl.coordToCartX(B), Transl.coordToCartY(B), Transl.coordToCartZ(B),
					          proj);
	}
		public TerrainInformation getTerrainInfo(double xa, double ya, double za,
								                        double xb, double yb, double zb, 
								                        MapProjection proj)
		{			
			double step = proj.apprStep(xa, ya, za, xb, yb, zb);
			double alpha = GeometryCalc.angleDistanceInRad(xa, ya, za, xb, yb, zb);		
			
			ArrayList<Integer> quantities = new ArrayList<Integer>();  //ilosc pixeli danego tymczasowego koloru (terenu)  
			ArrayList<Integer> colors = new ArrayList<Integer>();
			
			int previous = Colors.getNeutralColor();
			int tempRGB;		
			PointOnWorldSphere tempPoint;
			int i = 0;
			for(double beta = 0; beta<alpha; beta+=step){
				tempPoint = GeometryCalc.getPointBetween(xa, ya, za, xb, yb, zb, alpha, beta);			
				tempRGB = proj.getRGB(tempPoint);
				
				if(tempRGB != previous){
					quantities.add(new Integer(1)); 
					colors.add(tempRGB);
				}
				else{			
					addOneToLastElementInArrayList(quantities);
				}
				previous = tempRGB;
				i++;
			}
			
			TerrainType[] typesOfTerrTrav = new TerrainType[quantities.size()] ;
			float[] tempPercentagesOfTerrTrav = new float[quantities.size()];		
			int howManyPixelsChecked = i;  
			
			for(i = 0; i<colors.size(); i++){				
				typesOfTerrTrav[i] = Colors.colorToTerrainType( colors.get(i) );
				tempPercentagesOfTerrTrav[i] = quantities.get(i) / howManyPixelsChecked;			
			}	
			return new TerrainInformation(typesOfTerrTrav, tempPercentagesOfTerrTrav);
		}
//			private TerrainType[] convertRGBcolorsToTerrainTypes(ArrayList<Integer> colors){
//				TerrainType[] typesOfTerrTrav = new TerrainType[colors.size()];
//				for(int i = 0; i<colors.size(); i++){				
//					typesOfTerrTrav[i] = Colors.colorToTerrainType( colors.get(i) );			
//				}	
//				return typesOfTerrTrav;
//			}
			
			private void addOneToLastElementInArrayList(ArrayList<Integer> al){
				al.set(al.size()-1, new Integer(al.size()-1) + 1);
			}
		
	public class TerrainInformation
	{
		public final TerrainType[] typesOfTerrainsTraversed;
		public final float[] percentagesOfTerrainsTraversed;
		
		public TerrainInformation(TerrainType[] typesOfTerrainsTraversed, float[] percentagesOfTerrainsTraversed)
		{
			this.typesOfTerrainsTraversed = typesOfTerrainsTraversed;
			this.percentagesOfTerrainsTraversed = percentagesOfTerrainsTraversed;
		}			
	}
	
	public boolean ifLandEdge(MapProjection proj , double xa, double ya, double za,
			double xb, double yb, double zb)
	{
		LandChecking landChecking = new LandChecking();
		return checkIfEdgeIsOfSurfaceType(proj, xa, ya, za, xb, yb, zb, landChecking);
	}	

	public boolean checkIfEdgeIsOfSurfaceType(MapProjection proj, Point A, Point B, SurfaceCheckingType surfaceCheckingType)
	{				
		double xa = Transl.coordToCartX(A);
		double ya = Transl.coordToCartY(A);
		double za = Transl.coordToCartZ(A);
		double xb = Transl.coordToCartX(B);
		double yb = Transl.coordToCartY(B);
		double zb = Transl.coordToCartZ(B);
		
		return checkIfEdgeIsOfSurfaceType(proj, xa, ya, za, xb, yb, zb, surfaceCheckingType);			
	}
	    // ponizsza metoda sprawdza czy wszystkie punkty posrednie miedzy dwoma punktami sa danego typu;
		// jesli tak jest to zwraca prawde.
		// Metoda sprawdza najpierw 20 punktow na linii geodezyjnej, nastepni
		// metoda zaczyna sprawdzac linie geodezyjna bardzo dokladnie.	
		public boolean checkIfEdgeIsOfSurfaceType(MapProjection proj , double xa, double ya, double za,
				double xb, double yb, double zb, SurfaceCheckingType surfaceCheckingType)
		{				
			double angleDistance = GeometryCalc.angleDistanceInRad(xa, ya, za, xb, yb, zb);
			
			if( checkIfEdgeIsOfSurfaceTypeUsingSpecifiedNumberOfChecks(proj, xa, ya, za, xb, yb, zb, 20, angleDistance, surfaceCheckingType) )
			{
				if( checkIfEdgeIsOfSurfaceTypeUsingApprStep(proj, xa, ya, za, xb, yb, zb, angleDistance, surfaceCheckingType) )
				{
					return true;
				}
				else
					return false;
			}
			else
				return false;			
		}
		
		// PONIZEJ KILKA METOD DO EDGES CHECKING 
		// USING GIVEN STEP	
		public boolean checkIfEdgeIsOfSurfaceTypeUsingGivenStep(MapProjection proj , Point A, Point B,
				double angleDistance, double step, SurfaceCheckingType surfaceCheckingType)
		{
			double xa = Transl.coordToCartX(A);
			double ya = Transl.coordToCartY(A);
			double za = Transl.coordToCartZ(A);
			double xb = Transl.coordToCartX(B);
			double yb = Transl.coordToCartY(B);
			double zb = Transl.coordToCartZ(B);
			
			return checkIfEdgeIsOfSurfaceTypeUsingGivenStep(proj, xa, ya, za, xb, yb, zb, angleDistance, step, surfaceCheckingType); 
		}
			public boolean checkIfEdgeIsOfSurfaceTypeUsingGivenStep(MapProjection proj , double xa, double ya, double za,
					double xb, double yb, double zb, double angleDistance, double step, SurfaceCheckingType surfaceCheckingType)
			{		
				PointOnWorldSphere temp;		
				
				double beta;	
				
				for(beta = 0; beta<angleDistance; beta+=step)
				{					
					temp = GeometryCalc.getPointBetween(xa, ya, za, xb, yb, zb, angleDistance, beta);
					if( !surfaceCheckingType.checkSurfaceOnSpot(proj, temp) )
					{
						return false;
					}			
				}
				return true;
			}
	// USING SPECIFIED NUMBER OF CHECKS
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public boolean checkIfEdgeIsOfSurfaceTypeUsingSpecifiedNumberOfChecks(MapProjection proj , double xa, double ya, double za,
																									  double xb, double yb, double zb, 
																									  int numberOfChecks, double angleDistance, 
																									  SurfaceCheckingType surfaceCheckingType)	{
		double step = angleDistance/numberOfChecks;
		return checkIfEdgeIsOfSurfaceTypeUsingGivenStep(proj, xa, ya, za, xb, yb, zb, angleDistance, step, surfaceCheckingType);
	}	
	// USING APPROPRIATE STEP
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public boolean checkIfEdgeIsOfSurfaceTypeUsingApprStep(MapProjection proj , double xa, double ya, double za,
																					   double xb, double yb, double zb, 
																					   double angleDistance, SurfaceCheckingType surfaceCheckingType){
		double step = proj.apprStep(xa, ya, za, xb, yb, zb);
		return checkIfEdgeIsOfSurfaceTypeUsingGivenStep(proj, xa, ya, za, xb, yb, zb, angleDistance, step, surfaceCheckingType);
	}

}
