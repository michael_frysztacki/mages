package mapcom.worldconstruction;

import mapcom.imgproc.Colors;
import mapcom.mapprojections.MapProjection;
import mapcom.math.PointOnWorldSphere;
import mapcom.worldstructure.Edge;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.TerrainType;

public class WaterChecking implements SurfaceCheckingType
{
	@Override
	public boolean checkSurfaceOnSpot(MapProjection proj, PointOnWorldSphere spotToCheck){
		return Colors.isWaterColor(proj.getRGB(spotToCheck));		
	}
	
	@Override
	public boolean mightBelongToEdge(Point A){
		return A.isWater();
	}
	
	@Override
	public Edge createEdge(Point start, Point end, MapProjection proj){
		return new Edge(start, end, new TerrainType[]{TerrainType.Water}, new float[]{1.0f});		
	}
}
