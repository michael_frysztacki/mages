package mapcom.worldconstruction;

import java.util.Iterator;

import mapcom.worldstructure.Point;
import mapcom.worldstructure.PointsStructure;

public class PointsStructurePerformer
{
	
	public int countCapitals(PointsStructure points)
	{
		return countGivenPoints(points, 0, 0, 0, 0, 1);
	}
	
	private int countGivenPoints(PointsStructure points, 
			int polar, int water, int interPoints, int capitals, int colonies)
	{
		int counter = 0;
		Iterator<Point> iter = points.iterator();
		boolean shouldBeAdded;
		Point tempPoint;
		while( iter.hasNext() )
		{
			tempPoint = iter.next();
			shouldBeAdded = true;
			if( polar == -1 )
				if( tempPoint.isPolar())
					shouldBeAdded = false;
			if( polar == 1 )
				if( !tempPoint.isPolar())
					shouldBeAdded = false;
			if( water == -1 )
				if( tempPoint.isWater())
					shouldBeAdded = false;
			if( water == 1 )
				if( !tempPoint.isWater())
					shouldBeAdded = false;
			if( interPoints == -1 )
				if( tempPoint.isInterPoint())
					shouldBeAdded = false;
			if( interPoints == 1 )
				if( !tempPoint.isInterPoint())
					shouldBeAdded = false;
			if( capitals == -1 )
				if( tempPoint.isCapital())
					shouldBeAdded = false;
			if( capitals == 1 )
				if( !tempPoint.isCapital())
					shouldBeAdded = false;
			if( colonies == -1 )
				if( tempPoint.isCapital())
					shouldBeAdded = false;
			if( colonies == 1 )
				if( !tempPoint.isCapital())
					shouldBeAdded = false;			
			
			if(shouldBeAdded)
				counter++;
		}
		return counter;
	}
}
