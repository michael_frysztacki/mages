package mapcom.imgproc;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Auxiliary
{
	public static BufferedImage resizeImg(BufferedImage originalImage, int finalWidth, int finalHeight)
    {
    		BufferedImage resizedImage = new BufferedImage(finalWidth, finalHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = resizedImage.createGraphics();            
            g.drawImage(originalImage, 0, 0, finalWidth, finalHeight, null);
            g.dispose();
            
            return resizedImage;
    }
	
    public static BufferedImage loadImage(String inputFile)
	{
	    BufferedImage src;
	    BufferedImage out = null;
	    
		try
		{
			src = ImageIO.read(new File(inputFile));
			out = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_RGB);
			out.getGraphics().drawImage(src, 0, 0, null);
		} 
		catch (IOException e)
		{
			System.out.println("LoadImage() method: The file " + inputFile + " could not have been read");
		}	
		return out;
	}
    
	public static void saveImage(BufferedImage toSave, String outputFileName)
	{		
		try {
		    File outputfile = new File(outputFileName);
		    ImageIO.write(toSave, "png", outputfile);
		} catch (IOException e) {		
		}
	}
	
	public static BufferedImage binarizeImage(BufferedImage img, int imgWidth, int imgHeight)
	{
		int i,j;
		int threshold=50;
		int pixel;
		for(i=0; i<imgWidth; i++){
			for(j=0; j<imgHeight; j++){
				pixel = img.getRGB(i, j);
				int  red   = (pixel & 0x00ff0000) >> 16;
				int  green = (pixel & 0x0000ff00) >> 8;
				int  blue  =  pixel & 0x000000ff;
				if(red<threshold && green<threshold && blue<threshold){
					img.setRGB(i,j, 0x00000000);
				}				
				else 
				img.setRGB(i,j, 0x00ffffff);
			}
		}
		return img;
	}

	public static boolean ifImageBinarized(BufferedImage img, int imgWidth, int imgHeight)
	{
		int i,j;
		boolean ifBinarized = true;
		outer:
		for(i=0; i<imgWidth; i++)
		{
			for(j=0; j<imgHeight; j++)
			{
				if( !isWhite(img.getRGB(i, j)) && !isBlack(img.getRGB(i, j)) )
				{
					ifBinarized = false;					
					break outer;
				}
			}
		}
		return ifBinarized;
	}
	
	public static void changeOneColorToAnother(BufferedImage img, int initColor, int finalColor)
	{
		for(int i=0; i<img.getWidth(); i++){
			for(int j=0; j<img.getHeight(); j++){				
				if( Colors.areTheSameColors(img.getRGB(i, j), initColor) ){
					img.setRGB(i, j, finalColor);					
				}
			}
		}
	}
		private static boolean isWhite(int color){
			return color == Colors.white;
		}
		private static boolean isBlack(int color){
			return color == Colors.black;
		}
	
	public static void colorImgWithColor(BufferedImage img, int color)
	{
		for(int i=0; i<img.getWidth(); i++)
		{
			for(int j=0; j<img.getHeight(); j++)
			{				
				img.setRGB(i, j, color);
			}
		}
	}
	public static void colorImgWithDarkBlue(BufferedImage img)
	{
		colorImgWithColor(img, Colors.darkblue);
	}
	public static void colorImgWithBlack(BufferedImage img)
	{
		colorImgWithColor(img, Colors.black);
	}
	public static void colorImgWithGreen(BufferedImage img)
	{
		colorImgWithColor(img, Colors.green);
	}
	public static void colorImgWithWhite(BufferedImage img)
	{
		colorImgWithColor(img, Colors.white);
	}
	public static void colorImgWithRed(BufferedImage img)
	{
		colorImgWithColor(img, Colors.red);
	}
}
