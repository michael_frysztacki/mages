package mapcom.imgproc;

import mapcom.mapprojections.MapProjection;
import mapcom.math.GeometryCalc;
import mapcom.math.PointOnWorldSphere;
import mapcom.math.Transl;
import mapcom.math.WorldConstants;

public class LineDrawing
{
	
	//rysowanie linii geodezyjnej, poprzez rownomierne przechodzenie po kacie (metoda Thao: kolega taty)
	// TESTED
	public static void drawGeodesic(MapProjection proj, PointOnWorldSphere point1, PointOnWorldSphere point2, int thickness, int color)
	{
		double xa = Transl.coordToCartX(point1);
		double ya = Transl.coordToCartY(point1);
		double za = Transl.coordToCartZ(point1);
		double xb = Transl.coordToCartX(point2);
		double yb = Transl.coordToCartY(point2);
		double zb = Transl.coordToCartZ(point2);
		
		PointOnWorldSphere temp;
		double beta;
		double step = proj.apprStep(point1, point2);		
		double alpha = GeometryCalc.angleDistanceInRad(point1, point2);
		for(beta = 0; beta<alpha; beta+=step)
		{			
			temp = GeometryCalc.getPointBetween(xa, ya, za, xb, yb, zb, alpha, beta);
			PointDrawing.markPointWithSquareOfColor(temp, thickness, proj, color);		
		}
		PointDrawing.markPointWithSquareOfColor(point2, thickness, proj, color);
	}
	
	// Metoda, ktora zamiast punktu "center" przyjmowalaby dwa katy pochylenia plaszczyzny wzgledem osi OX i OZ nie jest zaimplementowana
	// bo bylaby niejednoznaczna. Dla promienia nawigacji nie bedacego 1/4 rownika nie wiadomo byloby po ktorej stronie narysowac okrag, 	
	// bo istnialyby dwie mozliwosci
	// TESTED	
	public static void drawNavCircle(MapProjection proj, PointOnWorldSphere center, double naviRadiusKm, int thickness, int color)
	{	
		// r - promien nawigacji po zrzutowaniu na plaszczyzne zawierajaca obwod nawigacji
		// R - odleglosc kola nawigacji do punktu (0,0,0)
		
		//	r    					  naviRadiuskm	
		// --- = sin(a)    oraz  a = --------------		               
		//  R						  EarthRadius
				
		double r = Math.sin(naviRadiusKm/(WorldConstants.EARTH_RADIUS_LENGTH))*WorldConstants.EARTH_RADIUS_LENGTH;		
		double R = Math.sqrt(WorldConstants.EARTH_RADIUS_LENGTH*WorldConstants.EARTH_RADIUS_LENGTH-r*r);

		double t = 0;
		
		// sprawdzamy o jakie katy zostal obrocony "center" z pozycji (0, R, 0);
		// znajdujemy wiec najpierw kat obrocenia wokol osi OZ
		double alpha;
		alpha = Math.toRadians( -center.getLat() );
		double beta;
		if( center.getLongit() > 0 ) 
			beta = Math.toRadians( - 180 + center.getLongit() ); 
		else
			beta = Math.toRadians( + 180 + center.getLongit() ); 
				
		double[] pointOnNavCircle = new double[]{r*Math.cos(t), R, r*Math.sin(t)};
		pointOnNavCircle = GeometryCalc.rotateVectorAroundXAxis(pointOnNavCircle, alpha);
		pointOnNavCircle = GeometryCalc.rotateVectorAroundZAxis(pointOnNavCircle, beta);									
		
		PointOnWorldSphere startPoint = GeometryCalc.vectorToPoint(pointOnNavCircle);
		PointOnWorldSphere endPoint = null;
		
		for(t=0.001; t<2*Math.PI; t+=0.001)
		{		
			pointOnNavCircle = new double[]{r*Math.cos(t), R, r*Math.sin(t)};
			pointOnNavCircle = GeometryCalc.rotateVectorAroundXAxis(pointOnNavCircle, alpha);
			pointOnNavCircle = GeometryCalc.rotateVectorAroundZAxis(pointOnNavCircle, beta);																	
			endPoint = GeometryCalc.vectorToPoint(pointOnNavCircle);			
			drawGeodesic(proj, startPoint, endPoint, thickness, color);
			startPoint = endPoint;
		}			
	}	
	
	
	// alpha - obrot wokol OX
	// beta - obrot wokol OZ
	// obydwa katy musza byc podane w radianach
	// TESTED
	public static void drawGreatCircle(MapProjection proj, double alpha, double beta, int thickness, int color)
	{		
		double longit, lat;
		
		if(alpha > 0)
			lat = - 90 + Math.toDegrees(alpha) ;
		else
			lat = - 90 - Math.toDegrees(alpha);
		
		if(alpha > 0)
			longit = Math.toDegrees(beta);
		else 
		{
			if(beta > 0)
				longit = -180 + Math.toDegrees(beta);
			else
				longit = 180 + Math.toDegrees(beta);
		}
				
		PointOnWorldSphere center = new PointOnWorldSphere( longit, lat );
		drawNavCircle(proj, center, WorldConstants.EARTH_RADIUS_LENGTH*Math.PI/2, thickness, color);
	}

	// NOT TESTED
	public static void drawGreatCircle(MapProjection proj, PointOnWorldSphere A, PointOnWorldSphere B, int thickness, int color)
	{		
		double[] slopeAngles = GeometryCalc.getPlaneSlopes(A, B);		
		double alpha = slopeAngles[0];
		double beta = slopeAngles[1];
		drawGreatCircle(proj, alpha, beta, thickness, color);
	}



}
