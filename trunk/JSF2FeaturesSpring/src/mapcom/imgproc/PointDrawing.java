package mapcom.imgproc;

import java.util.Iterator;

import mapcom.mapprojections.MapProjection;
import mapcom.math.PointOnWorldSphere;
import mapcom.worldstructure.Capital;
import mapcom.worldstructure.Colony;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.PointsStructure;

public class PointDrawing
{
	//sprawdza czy dany pixel na obrazku jest blisko morza. Dziala to nastepujaco: jesli jakis pixel w kwadracie 
	//o boku dlugosci (2*pixRadius +1), i o srodku w danym pixelu jest inny niz czarny to znaczy ze jest blisko morza
	// NOT TESTED
	public static boolean isCloseToLand(int lonPix, int latPix, int pixRadius, MapProjection proj)
	{
		int i,j;
		boolean isCloseToLand=false;		
		petla:
		for(i=-pixRadius; i<=pixRadius; i++)
		{			
			for(j=-pixRadius; j<=pixRadius; j++)
			{
				if( !Colors.isWaterColor(proj.getRGB(lonPix+i, latPix+j)) )
				{						
					isCloseToLand = true;					
					break petla;
				}	
			}		
		}
		return isCloseToLand;
	}
	//sprawdza czy dany pixel na obrazku jest blisko morza. Dziala to nastepujaco: jesli jakis pixel w kwadracie 
	//o boku dlugosci (2*pixRadius +1), i o srodku w danym pixelu jest czarny to znaczy ze jest blisko morza
	// NOT TESTED
	public static boolean ifCloseToSea(int lonPix, int latPix, int pixRadius, MapProjection proj)
	{
		int i,j;
		boolean isCloseToSea = false;		
		for(i=-pixRadius; i<=pixRadius; i++)
		{			
			for(j=-pixRadius; j<=pixRadius; j++)
			{
				if( Colors.isWaterColor(proj.getRGB(lonPix+i, latPix+j)) )
				{						
					isCloseToSea = true;					
					break;
				}	
			}			
		}
		return isCloseToSea;		
	}
	
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static void markWithXOfColor(PointOnWorldSphere center, int pixSize, MapProjection proj, int color)	
	{
		markPointWithXOfColor(proj.getLongitDegToPix(center), proj.getLatDegToPix(center) , pixSize, proj, color);
	}
		//Zaznacza dany punkt iksem danego koloru. Dziala wedlug metody: iks(X) o belkach (2*pixSize+1) oraz centrum w
		//podanym punkcie zostaje zamalowany na kolor podany w argumencie
		// TESTED
		public static void markPointWithXOfColor(int x, int y, int pixSize, MapProjection proj, int color)	
		{			
			for(int i = -pixSize; i<=pixSize; i++)
			{
				if( x + i >= 0 && x + i < proj.getWidth() && y + i >= 0 && y + i < proj.getHeight() )
					proj.setRGB(x + i, y + i, color );
				if( x - i >= 0 && x - i < proj.getWidth() && y + i >= 0 && y + i < proj.getHeight() )
					proj.setRGB(x - i, y + i, color );
			}
		}
		
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static void markWithCrossOfColor(PointOnWorldSphere center, int pixSize, MapProjection proj, int color)	
	{
		markPointWithCrossOfColor(proj.getLongitDegToPix(center), proj.getLatDegToPix(center) , pixSize, proj, color);
	}
		//Zaznacza dany punkt krzyzykiem danego koloru. Dziala wedlug metody: krzyz o belkach (2*pixSize+1) oraz centrum w
		//podanym punkcie zostaje zamalowany na kolor podany w argumencie
		// TESTED
		public static void markPointWithCrossOfColor(int x, int y, int pixSize, MapProjection proj, int color)	
		{			
			for(int i = -pixSize; i<=pixSize; i++)
			{
				if( x + i >= 0 && x + i < proj.getWidth() ){
					proj.setRGB(x + i, y, color );
				}
				if( y + i >= 0 && y + i < proj.getHeight() ){
					proj.setRGB(x, y + i, color );
				}
			}
		}
	
	// NOT TESTED, BUT SUPPOSED TO WORK PROPERLY
	public static void markPointWithSquareOfColor(PointOnWorldSphere center, int pixSize, MapProjection proj, int color)
	{
		markPointWithSquareOfColor(proj.getLongitDegToPix(center), proj.getLatDegToPix(center) , pixSize, proj, color);
	}
		//Zaznacza dany punkt kwadratem danego koloru. Dziala wedlug metody: kwadrat o boku (2*pixSize+1) oraz centrum w
		//podanym punkcie zostaje zamalowany na kolor podany w argumencie
		// TESTED
		public static void markPointWithSquareOfColor(int x, int y, int pixSize, MapProjection proj, int color)	
		{
			int i,j;		
		
			int leftConstr = -pixSize;
			int rightConstr = pixSize;
			int downConstr = -pixSize;
			int upConstr = pixSize;
			
			//sprawdzanie czy domniemany kwadrat nie wyjezdzalby za mape
			if(x-pixSize<0)		
				leftConstr = -x;		
			if(x+pixSize >= proj.getWidth())		
				rightConstr = proj.getWidth()-x-1;		
			if(y-pixSize<0)		
				downConstr = -y;		
			if(y+pixSize >= proj.getHeight())		
				upConstr = proj.getHeight()-y-1;				
			
			for(i = leftConstr; i<=rightConstr; i++)
			{
				for(j = downConstr; j<= upConstr; j++)
				{
					proj.setRGB(x+i, y+j, color );				
				}
			}
		}
		
	public static void drawPoints(PointsStructure points, MapProjection proj)
	{
		Iterator<Point> iter = points.iterator();
		while( iter.hasNext() )
		{
			drawPoint(iter.next(), proj);
		}
	}
		public static void drawPoint(Point point, MapProjection proj)
		{
			int color;			
			if( point.isWater() )
				color = Colors.darkblue;
			else
				color = Colors.black;
			if( point.getClass() == Capital.class )
				markWithCrossOfColor(point, 3, proj, color);
			else if( point.getClass() == Colony.class )
				markWithXOfColor(point, 3, proj, color);
			else
				markPointWithSquareOfColor(point, 1, proj, color);
		}
	
	
	
	
}
