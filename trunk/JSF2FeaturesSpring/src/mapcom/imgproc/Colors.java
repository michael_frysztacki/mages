package mapcom.imgproc;

import mapcom.worldstructure.TerrainType;

public class Colors 
{		
	private final static int alphaConst = (255<<24); 
	public final static int white = alphaConst + (255<<16) + (255<<8) + (255);
	public final static int yellow = alphaConst + (255<<16) + (255<<8) + (0);
	public final static int orange = alphaConst + (255<<16) + (88<<8) + (0);
	public final static int red = alphaConst + (255<<16) + (0<<8) + (0);
	public final static int green = alphaConst + (0<<16) + (255<<8) + (0);
	public final static int darkblue = alphaConst + (0<<16) + (0<<8) + (205);
	public final static int lightBlue = alphaConst + (51<<16) + (153<<8) + (204);
	public final static int violet = alphaConst + (134<<16) + (127<<8) + (127);
	public final static int grey = alphaConst + (134<<16) + (127<<8) + (127);
	public final static int brown = alphaConst + (139<<16) + (69<<8) + (19);
	public final static int black = alphaConst + (0<<16) + (0<<8) + (0);
		
	public static int getWaterColor()
	{			
		return lightBlue;
	}
	public static int getPlainColor()
	{
		return white;
	}
	public static int getSteppeColor()
	{
		return red;
	}
	public static int getJungleColor()
	{
		return green;
	}
	public static int getDesertColor()
	{
		return yellow;
	}
	public static int getMountainColor()
	{
		return grey;
	}
	public static int getNeutralColor()
	{
		return black;
	}
	public static boolean areTheSameColors(int RGBtoCheck, int toCompare)
	{
		return RGBtoCheck == toCompare;
	}
	public static boolean areNotTheSameColors(int rgb1, int rgb2)
	{
		return rgb1 != rgb2;
	}
	
	public static boolean isWaterColor(int RGBtoCheck)
	{
		return areTheSameColors(RGBtoCheck, getWaterColor());
	}	
	
	
	
	public static boolean isLandColor(int rgb)
	{
		return areTheSameColors(rgb, getPlainColor()) || 
			   areTheSameColors(rgb, getDesertColor()) || 
			   areTheSameColors(rgb, getJungleColor()) ||
			   areTheSameColors(rgb, getSteppeColor()) ||
			   areTheSameColors(rgb, getMountainColor());
	}	
	
	public static TerrainType colorToTerrainType(int rgb)
	{
		TerrainType terrainType;
		switch( rgb ){
	        case lightBlue:		terrainType = TerrainType.Water; 
	                 break;
	        case white:			terrainType = TerrainType.Plain; 
	                 break;
	        case red: 			terrainType = TerrainType.Steppe;
            		break;         
	        case green:			terrainType = TerrainType.Jungle;
            	     break;
	        case yellow: 		terrainType = TerrainType.Desert;
            		break;
	        case grey: 			terrainType = TerrainType.Mountain;	 
	                 break;
	        default: 			terrainType = TerrainType.Water;
            		break;
		}
		return terrainType;
	}
	public static int terrainTypeToColor(TerrainType terrainType)
	{		
		int color;		
		switch( terrainType ){
	        case Water:		 		color = lightBlue; 
	                 break;
	        case Plain:				color = white; 
	                 break;
	        case Steppe:			color = red;
            	     break;
	        case Jungle: 			color = green;	 
	                 break;
	        case Desert: 			color = yellow; 
	                 break;
	        case Mountain: 			color = grey;
	                 break;
	        default: 				color = lightBlue;
	                 break;
		}
		return color;		
	}
}
