package mapcom.imgproc;

import java.awt.Color;
import java.util.Iterator;

import mapcom.mapprojections.MapProjection;
import mapcom.worldstructure.Edge;
import mapcom.worldstructure.EdgesStructure;
import mapcom.worldstructure.Point;
import mapcom.worldstructure.PointsStructure;

public class WorldDrawing
{

	public static void drawEdges(MapProjection proj, EdgesStructure edges, PointsStructure points, int thickness, Color color)
	{
		Edge temp;
		Iterator<Iterator<Edge>> outerIt = edges.outerIterator();
		Iterator<Edge> innerIt;
		while(outerIt.hasNext())
		{
			innerIt = outerIt.next();
			while(innerIt.hasNext())
			{					
				temp = innerIt.next();
				ImgProc.drawLine(proj, temp.getStartPoint(), temp.getEndPoint(), thickness, color);
			}
		}
	}
	
	public static void drawCitiesCapitalsAndInterPoints(MapProjection proj, PointsStructure points, int thickness)
	{
		drawCities(proj, points, thickness);
		drawColonies(proj, points, thickness);
		drawInterPoints(proj, points, thickness);		
	}
		public static void drawCities(MapProjection proj, PointsStructure points, int thickness)
		{
			drawPoints(proj, points, thickness, Colors.violet, 0, 0, 1, -1, -1);
		}
		public static void drawInterPoints(MapProjection proj, PointsStructure points, int thickness)
		{
			drawPoints(proj, points, thickness, Colors.darkblue, 0, 0, -1, -1, 1);
		}
		public static void drawColonies(MapProjection proj, PointsStructure points, int thickness)
		{
			drawPoints(proj, points, thickness, Colors.black, 0, 0, -1, 1, -1);
		}
	
			// 1 oznacza, ze tak; -1, ze nie; 0, zero ze obojetne 
			public static void drawPoints(MapProjection proj, PointsStructure points, int thickness, Color color, 
					int ifPolar, int ifWater, int ifCapital, int ifColony, int ifInterPoint)
			{		
				for(int i=0; i<points.size(); i++)
				{
					drawPoint(proj, points.getPointWithId(i), thickness, color, ifPolar, ifWater, ifCapital, ifColony, ifInterPoint);
				}
			}
			
				// 1 oznacza, ze tak; -1, ze nie; 0, zero ze obojetne 
				public static void drawPoint(MapProjection proj, Point point, int thickness, Color color, 
						int ifPolar, int ifWater, int ifCapital, int ifColony, int ifInterPoint)
				{
					boolean	ifMark = true;
					if( ifPolar == 1)
					{				
						if(!point.isPolar())
							ifMark = false;
					}
					if( ifPolar == -1 )
					{
						if(point.isPolar())
							ifMark = false;
					}
					if( ifWater == 1)
					{
						if(!point.isWater())
							ifMark = false;
					}
					if( ifWater == -1)
					{
						if(point.isWater())
							ifMark = false;
					}
					if( ifCapital == 1)
					{
						if(!point.isCapital())
							ifMark = false;
					}
					if( ifCapital == -1)
					{
						if(point.isCapital())
							ifMark = false;
					}
					if( ifColony == 1)
					{
						if(!point.isColony())
							ifMark = false;
					}
					if( ifColony == -1)
					{
						if(point.isColony())
							ifMark = false;
					}
					if( ifInterPoint == 1)
					{
						if(!point.isInterPoint())
							ifMark = false;
					}
					if( ifInterPoint == -1)
					{
						if(point.isInterPoint())
							ifMark = false;
					}
					if( ifMark )			
						if( point.isInterPoint() )
							ImgProc.markPointWithSquareOfColor(point, thickness, proj, color);
						else
						{
							if( point.isColony() )
								ImgProc.markPointWithXOfColor(point, thickness, proj, color);
							else
								ImgProc.markPointWithCrossOfColor(point, thickness, proj, color);
						}				
				}
}
