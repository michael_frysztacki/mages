package mapcom.imgproc;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import mapcom.mapprojections.MapProjection;
import mapcom.math.GeometryCalc;
import mapcom.math.PointOnWorldSphere;
import mapcom.math.Transl;
import mapcom.worldstructure.Point;


public class ImgProc
{	
	 // sprawadza czy dany pixel na obrazku img, jest takiego samego koloru jak w argumentach
	public static boolean ifTheSameCol(BufferedImage img, int x, int y, Color color)
	{		 
//		int pixelColor = img.getRGB(x, y);		
//		return((pixelColor & 0x00ffffff) == (color & 0x00fffffff));
		return ( img.getRGB(x, y) == color.getRGB() );
	}
	//sprawadza czy dany pixel na obrazku img jest bialy
	public static boolean ifWhite(BufferedImage img, int x, int y)
	{
		return ifTheSameCol(img, x, y, Colors.white);
	}
	//sprawadza czy dany pixel na obrazku img jest czarny
	public static boolean ifBlack(BufferedImage img, int x, int y)
	{
		return ifTheSameCol(img, x, y, Colors.black);
	}
	public static boolean ifDarkBlue(BufferedImage img, int x, int y)
	{
		return ifTheSameCol(img, x, y, Colors.darkblue);
	}
	public static boolean ifRed(BufferedImage img, int x, int y)
	{
		return ifTheSameCol(img, x, y, Colors.red);
	}
	public static boolean ifGreen(BufferedImage img, int x, int y)
	{
		return ifTheSameCol(img, x, y, Colors.green);
	}
	public static boolean ifYellow(BufferedImage img, int x, int y)
	{
		return ifTheSameCol(img, x, y, Colors.yellow);
	}
	public static boolean ifViolet(BufferedImage img, int x, int y)
	{
		return ifTheSameCol(img, x, y, Colors.violet);
	}
	//sprawdza czy dany pixel na obrazku jest blisko morza. Dziala to nastepujaco: jesli jakis pixel w kwadracie 
	//o boku dlugosci (2*pixRadius +1), i o srodku w danym pixelu jest inny niz czarny to znaczy ze jest blisko morza 
	public static boolean ifCloseToLand(int lonPix, int latPix, int pixRadius, BufferedImage img)  //
	{
		int i,j;
		boolean ifCloseToLand=false;		
		petla:
		for(i=-pixRadius; i<=pixRadius; i++)
		{			
			for(j=-pixRadius; j<=pixRadius; j++)
			{
				if(!ifBlack(img, lonPix+i, latPix+j))
				{						
					ifCloseToLand = true;					
					break petla;
				}	
			}		
		}
		return ifCloseToLand;
	}
	//sprawdza czy dany pixel na obrazku jest blisko morza. Dziala to nastepujaco: jesli jakis pixel w kwadracie 
	//o boku dlugosci (2*pixRadius +1), i o srodku w danym pixelu jest czarny to znaczy ze jest blisko morza	
	public static boolean ifCloseToSea(int lonPix, int latPix, int pixRadius, BufferedImage img)
	{
		int i,j;
		boolean ifCloseToSea = false;		
		for(i=-pixRadius; i<=pixRadius; i++)
		{			
			for(j=-pixRadius; j<=pixRadius; j++)
			{
				if(ifBlack(img, lonPix+i, latPix+j))
				{						
					ifCloseToSea = true;					
					break;
				}	
			}			
		}
		return ifCloseToSea;		
	}
	
	//Zaznacza dany punkt kwadratem danego koloru. Dziala wedlug metody: krzyz o belkach (2*pixSize+1) oraz centrum w
	//podanym punkcie zostaje zamalowany na kolor podany w argumencie
	public static void markPointWithXOfColor(int x, int y, int pixSize, BufferedImage img, Color color)	
	{
		int i,j;		

		int leftConstr = -pixSize;
		int rightConstr = pixSize;
		int downConstr = -pixSize;
		int upConstr = pixSize;
		
		//sprawdzanie czy domniemany krzyz nie wyjezdzalby za mape
		if(x-pixSize<0)		
			leftConstr = -x;		
		if(x+pixSize >= img.getWidth())		
			rightConstr = img.getWidth()-x-1;		
		if(y-pixSize<0)	
			downConstr = -y;		
		if(y+pixSize >= img.getHeight())		
			upConstr = img.getHeight()-y-1;
		
		
		for(i = leftConstr; i<=rightConstr; i++)
		{
			img.setRGB(x+i, y+i, color.getRGB() );	
		}
		for(j = downConstr; j<= upConstr; j++)
		{
			img.setRGB(x-j, y+j, color.getRGB() );				
		}
	}
	public static void markPointWithXOfColor(Point center, int pixSize, MapProjection proj, Color color)	
	{
		markPointWithXOfColor(proj.getLongitDegToPix(center), proj.getLatDegToPix(center) , pixSize, proj.getImg(), color);
	}
	//Zaznacza dany punkt kwadratem danego koloru. Dziala wedlug metody: krzyz o belkach (2*pixSize+1) oraz centrum w
	//podanym punkcie zostaje zamalowany na kolor podany w argumencie
	public static void markPointWithCrossOfColor(int x, int y, int pixSize, BufferedImage img, Color color)	
	{
		int i,j;		

		int leftConstr = -pixSize;
		int rightConstr = pixSize;
		int downConstr = -pixSize;
		int upConstr = pixSize;
		
		//sprawdzanie czy domniemany krzyz nie wyjezdzalby za mape
		if(x-pixSize<0)		
			leftConstr = -x;		
		if(x+pixSize >= img.getWidth())		
			rightConstr = img.getWidth()-x-1;		
		if(y-pixSize<0)	
			downConstr = -y;		
		if(y+pixSize >= img.getHeight())		
			upConstr = img.getHeight()-y-1;
		
		
		for(i = leftConstr; i<=rightConstr; i++)
		{
			img.setRGB(x+i, y, color.getRGB() );	
		}
		for(j = downConstr; j<= upConstr; j++)
		{
			img.setRGB(x, y+j, color.getRGB() );				
		}
	}
	public static void markPointWithCrossOfColor(Point center, int pixSize, MapProjection proj, Color color)	
	{
		markPointWithCrossOfColor(proj.getLongitDegToPix(center), proj.getLatDegToPix(center) , pixSize, proj.getImg(), color);
	}
	//Zaznacza dany punkt kwadratem danego koloru. Dziala wedlug metody: kwadrat o boku (2*pixSize+1) oraz centrum w
	//podanym punkcie zostaje zamalowany na kolor podany w argumencie
	public static void markPointWithSquareOfColor(int x, int y, int pixSize, BufferedImage img, Color color)	
	{
		int i,j;		

		int leftConstr = -pixSize;
		int rightConstr = pixSize;
		int downConstr = -pixSize;
		int upConstr = pixSize;
		
		//sprawdzanie czy domniemany kwadrat nie wyjezdzalby za mape
		if(x-pixSize<0)		
			leftConstr = -x;		
		if(x+pixSize >= img.getWidth())		
			rightConstr = img.getWidth()-x-1;		
		if(y-pixSize<0)		
			downConstr = -y;		
		if(y+pixSize >= img.getHeight())		
			upConstr = img.getHeight()-y-1;				
		
		for(i = leftConstr; i<=rightConstr; i++)
		{
			for(j = downConstr; j<= upConstr; j++)
			{
				img.setRGB(x+i, y+j, color.getRGB() );				
			}
		}
	}
	
	public static void markPointWithSquareOfColor(PointOnWorldSphere center, int pixSize, MapProjection proj, Color color)
	{
		markPointWithSquareOfColor(proj.getLongitDegToPix(center), proj.getLatDegToPix(center) , pixSize, proj.getImg(), color);
	}
	
//	//nieoptymalnie napisane
//	//rysuje linie prosta na img. Jest to linia prosta na obrazku (a nie linia geodezyjna)
//	public static void drawStraightLine(BufferedImage img, double longitStart, double latStart, double longitEnd,
//			double latEnd, int imgWidth, int imgHeight, int color)
//	{
//		if(longitStart>longitEnd || latStart>latEnd)
//		{
//			System.out.println("zle punkty do rysowania linii");
//		}
//		int i;
//		int longitStartPix = Transl.longitDegToPix(longitStart, imgWidth);
//		int latStartPix = Transl.latDegToPix(latStart, imgHeight);
//		int longitEndPix = Transl.longitDegToPix(longitEnd, imgWidth);
//		int latEndPix = Transl.latDegToPix(latEnd, imgHeight);
//		double xInterval = Math.abs(longitEndPix - longitStartPix);
//		double yInterval = Math.abs(latEndPix - latStartPix);
//		double howMany = imgWidth + imgHeight;
//		
//		for(i = 0; i<howMany; i++)
//		{			
//			img.setRGB((int)(longitStartPix+(int)((double)i/howMany*xInterval)), 
//					(int)(latStartPix+(int)((double)i/howMany*yInterval)),  color);			
//		}
//	}
//	public static void drawVioletStraightLine(BufferedImage img, double longitStart, double latStart, double longitEnd,
//			double latEnd, int imgWidth, int imgHeight)
//	{
//		drawStraightLine(img, longitStart, latStart, longitEnd,
//			latEnd, imgWidth, imgHeight, Colors.violet);
//	}
	
	//rysowanie linii geodezyjnej, poprzez rownomierne przechodzenie po kacie (metoda Thao: kolega taty) 
	public static void drawLine(MapProjection proj, PointOnWorldSphere point1, PointOnWorldSphere point2, int thickness, Color color)
	{
		double xa = Transl.coordToCartX(point1);
		double ya = Transl.coordToCartY(point1);
		double za = Transl.coordToCartZ(point1);
		double xb = Transl.coordToCartX(point2);
		double yb = Transl.coordToCartY(point2);
		double zb = Transl.coordToCartZ(point2);
		
		PointOnWorldSphere temp = null;
		double beta;
		double step = proj.apprStep(point1, point2);		
		double alpha = GeometryCalc.angleDistanceInRad(point1, point2);
		for(beta = 0; beta<alpha; beta+=step)
		{			
			temp = GeometryCalc.getPointBetween(xa, ya, za, xb, yb, zb, alpha, beta);
			markPointWithSquareOfColor(temp, thickness, proj, color);		
		}		
	}
	//rysowanie linii
	public static void drawDarkBlueLine(MapProjection proj, PointOnWorldSphere point1, PointOnWorldSphere point2, int thickness)
	{
		drawLine(proj, point1, point2, thickness, Colors.darkblue);
	}
	public static void drawRedLine(MapProjection proj, PointOnWorldSphere point1, PointOnWorldSphere point2, int thickness)
	{
		drawLine(proj, point1, point2, thickness, Colors.red);
	}
	public static void drawGreenLine(MapProjection proj, PointOnWorldSphere point1, PointOnWorldSphere point2, int thickness)
	{
		drawLine(proj, point1, point2, thickness, Colors.green);
	}
	public static void drawVioletLine(MapProjection proj, PointOnWorldSphere point1, PointOnWorldSphere point2, int thickness)
	{
		drawLine(proj, point1, point2, thickness, Colors.violet);
	}
	
	//rysuje kolo (chodzi o kolo nawigacji) o srodku w punkcie na sferze kuli, i promieniu nawigacji podanym w argumencie
	public static void drawCircle(MapProjection proj, int imgWidth, int imgHeight, 
			PointOnWorldSphere center, double naviRadiusKm, int thickness, int color)
	{						
		double t = 0; //parametr
		double alpha = Math.toRadians(90+center.getLat());    // obrot wokol osi x
		double beta = Math.toRadians(center.getLongit());   // obrot wokol osi z
		
		// r - promien nawigacji po zrzutowaniu na plaszczyzne zawierajaca okrag nawigacji
		// R - odleglosc kola nawigacji do punktu (0,0,0)
		
		//	r    					  naviRadiuskm	
		// --- = sin(a)    oraz  a = --------------		               
		//  R						  EarthRadius
				
		double r = Math.sin(naviRadiusKm/(Transl.earthRadiusLength))*Transl.earthRadiusLength;		
		double R = Math.sqrt(Transl.earthRadiusLength*Transl.earthRadiusLength-r*r);

		double coordX;
		double coordY; 			
		double coordZ;		

		double sinAlpha = Math.sin(alpha);
		double cosAlpha = Math.cos(alpha);
		double sinBeta = Math.sin(beta);
		double cosBeta = Math.cos(beta);
		
		//	coordX = r*cosBeta*Math.cos(t)+(r*cosAlpha*Math.sin(t)+R*sinAlpha)*sinBeta;				
		//	coordY = cosBeta*(r*cosAlpha*Math.sin(t)+R*sinAlpha)-r*Math.cos(t)*sinBeta;
		//	coordZ = R*cosAlpha-r*Math.sin(t)*sinAlpha;
		
		coordX = r*cosBeta*Math.cos(t) + sinBeta*(-r*cosAlpha*Math.sin(t) + R*sinAlpha);		
		coordY = cosBeta*(r*cosAlpha*Math.sin(t) - R*sinAlpha) + r*sinBeta*Math.cos(t);
		coordZ = R*cosAlpha + r*sinAlpha*Math.sin(t);

		//	{cost R Cos[b] + (-R sint Cos[a] + r Sin[a]) Sin[b], 
		//	 R sint Cos[a] Cos[b] - r Cos[b] Sin[a] + cost R Sin[b], 
		//	 r Cos[a] + R sint Sin[a]}
		
		PointOnWorldSphere endPoint = null;					
		PointOnWorldSphere startPoint = new PointOnWorldSphere(Transl.xyzToLongit(coordX, coordY), Transl.xyzToLat(coordZ));							

		for(t=0.001; t<2*Math.PI; t+=0.001)
		{											
			coordX = r*cosBeta*Math.cos(t) + sinBeta*(-r*cosAlpha*Math.sin(t) + R*sinAlpha);		
			coordY = cosBeta*(r*cosAlpha*Math.sin(t) - R*sinAlpha) + r*sinBeta*Math.cos(t);
			coordZ = R*cosAlpha + r*sinAlpha*Math.sin(t);																
			
			endPoint = new PointOnWorldSphere(Transl.xyzToLongit(coordX, coordY), Transl.xyzToLat(coordZ));
			drawLine(img, startPoint, endPoint, imgWidth, imgHeight, thickness, color);
			startPoint = endPoint;																	
		}			
	}	
	public static void drawCircleDarkBlue(BufferedImage img, int imgWidth, int imgHeight, Point center, double km, int thickness)
	{
		drawCircle(img, imgWidth, imgHeight, center, km, thickness, Colors.darkblue);
	}
	public static void drawCircleRed(BufferedImage img, int imgWidth, int imgHeight, Point center, double km, int thickness)
	{
		drawCircle(img, imgWidth, imgHeight, center, km, thickness, Colors.red);
	}	
	public static void drawCircleYellow(BufferedImage img, int imgWidth, int imgHeight, Point center, double km, int thickness)
	{
		drawCircle(img, imgWidth, imgHeight, center, km, thickness, Colors.yellow);
	}
	public static void drawCircleGreen(BufferedImage img, int imgWidth, int imgHeight, Point center, double km, int thickness)
	{
		drawCircle(img, imgWidth, imgHeight, center, km, thickness, Colors.green);
	}
	public static void drawCircleViolet(BufferedImage img, int imgWidth, int imgHeight, Point center, double km, int thickness)
	{
		drawCircle(img, imgWidth, imgHeight, center, km, thickness, Colors.violet);
	}
	
	//METODY WIELKIEGO KOLA	
	//rysuje wielkie kolo obrocone o alpha w osi OX i o beta w osi OZ 
	//minimalna thickness = 0
	public static void drawGreatCircle(BufferedImage img, int imgWidth, int imgHeight, double alpha, double beta, int color, int thickness)
	{		
		double t;
		double x;
		double y;
		double z;
		Point temp;
		
		double cosA = Math.cos(alpha);
		double cosB = Math.cos(beta);
		double sinA = Math.sin(alpha);
		double sinB = Math.sin(beta);
		double cosT, sinT;
		double R = Transl.earthRadiusLength;
		double step = Transl.apprStep(alpha);
		
		for(t=0; t<2*Math.PI; t+=step)
		{
			sinT = Math.sin(t);
			cosT = Math.cos(t);
			x = R*cosB*cosT - R*cosA*sinB*sinT;
			y = R*sinB*cosT + R*cosA*cosB*sinT;
			z = R*sinA*sinT;
			temp = new Point(Transl.xyzToLongit(x, y), Transl.xyzToLat(z));
			markPointWithColor(temp, thickness, img, imgWidth, imgHeight, color);
		}		
	}
	public static void drawGreatCircle(BufferedImage img, int imgWidth, int imgHeight, double longA, double latA, 
			double longB, double latB, int color, int thickness)
	{
		double[] angles = new double[2];
		angles = Transl.getPlaneSlopes(longA, latA, longB, latB);		
		drawGreatCircle(img, imgWidth, imgHeight, angles[0], angles[1], color, thickness);
	}
	//rysuje wielkie kolo przechodzace przez dwa punkty
	public static void drawGreatCircle(BufferedImage img, int imgWidth, int imgHeight, Point A, Point B, int color, int thickness)
	{
		drawGreatCircle(img, imgWidth, imgHeight, A.getLongitude(), A.getLatitude(), B.getLongitude(), B.getLatitude(), color, thickness);
	}
	//rysowanie wielkich kol
	public static void drawGreatCircleViolet(BufferedImage img, int imgWidth, int imgHeight, double alpha, double beta, int thickness)
	{
		drawGreatCircle(img, imgWidth, imgHeight, alpha, beta, Colors.violet, thickness);
	}
	public static void drawGreatCircleViolet(BufferedImage img, int imgWidth, int imgHeight, Point A, Point B, int thickness)
	{
		drawGreatCircle(img, imgWidth, imgHeight, A, B, Colors.violet, thickness);
	}
	public static void drawGreatCircleViolet(BufferedImage img, int imgWidth, int imgHeight, double longA, double latA, double longB, double latB, int thickness)
	{
		drawGreatCircle(img, imgWidth, imgHeight, longA, latA, longB, latB, Colors.violet, thickness);
	}
	public static void drawGreatCircleDarkBlue(BufferedImage img, int imgWidth, int imgHeight, double alpha, double beta, int thickness)
	{
		drawGreatCircle(img, imgWidth, imgHeight, alpha, beta, Colors.darkblue, thickness);
	}
	public static void drawGreatCircleDarkBlue(BufferedImage img, int imgWidth, int imgHeight, Point A, Point B, int thickness)
	{
		drawGreatCircle(img, imgWidth, imgHeight, A, B, Colors.darkblue, thickness);
	}
	public static void drawGreatCircleDarkBlue(BufferedImage img, int imgWidth, int imgHeight, double longA, double latA, double longB, double latB, int thickness)
	{
		drawGreatCircle(img, imgWidth, imgHeight, longA, latA, longB, latB, Colors.darkblue, thickness);
	}
	public static void drawGreatCircleGreen(BufferedImage img, int imgWidth, int imgHeight, double alpha, double beta, int thickness)
	{
		drawGreatCircle(img, imgWidth, imgHeight, alpha, beta, Colors.green, thickness);
	}
	public static void drawGreatCircleGreen(BufferedImage img, int imgWidth, int imgHeight, Point A, Point B, int thickness)
	{
		drawGreatCircle(img, imgWidth, imgHeight, A, B, Colors.green, thickness);
	}
	public static void drawGreatCircleGreen(BufferedImage img, int imgWidth, int imgHeight, double longA, double latA, double longB, double latB, int thickness)
	{
		drawGreatCircle(img, imgWidth, imgHeight, longA, latA, longB, latB, Colors.green, thickness);
	}
	public static void drawGreatCircleRed(BufferedImage img, int imgWidth, int imgHeight, double alpha, double beta, int thickness)
	{
		drawGreatCircle(img, imgWidth, imgHeight, alpha, beta, Colors.red, thickness);
	}
	public static void drawGreatCircleRed(BufferedImage img, int imgWidth, int imgHeight, Point A, Point B, int thickness)
	{
		drawGreatCircle(img, imgWidth, imgHeight, A, B, Colors.red, thickness);
	}
	public static void drawGreatCircleRed(BufferedImage img, int imgWidth, int imgHeight, double longA, double latA, double longB, double latB, int thickness)
	{
		drawGreatCircle(img, imgWidth, imgHeight, longA, latA, longB, latB, Colors.red, thickness);
	}
	
	

	// resizuje obrazek do danych rozmiarow
	public static BufferedImage resizeImg(BufferedImage originalImage, int finalWidth, int finalHeight)
    {
    		BufferedImage resizedImage = new BufferedImage(finalWidth, finalHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = resizedImage.createGraphics();            
            g.drawImage(originalImage, 0, 0, finalWidth, finalHeight, null);
            g.dispose();
            
            return resizedImage;
    }
	
    public static BufferedImage loadImage(String inputFileName)
	{
	    BufferedImage src;
	    BufferedImage out = null;
	    
		try
		{
			src = ImageIO.read(new File(inputFileName));
			out = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_RGB);
			out.getGraphics().drawImage(src, 0, 0, null);
		} 
		catch (IOException e)
		{
			System.out.println("LoadImage() method: The file " + inputFileName + " could not have been read");
		}	
		return out;
	}
    
//	public static BufferedImage loadImage(String inputFile)
//	{	
//		BufferedImage img = null;
//		try
//		{
//			img = ImageIO.read(new File(inputFile));
//		} 
//		catch (IOException e)
//		{
//			System.out.println("LoadImage() method: The file " + inputFile + " could not have been read");
//		}	
//		return img;
//	}	
	//zapisuje dany obraz na dysk. Obraz ma nazwe podana w argumencie, a jego rozszerzenie to gif 
	public static void saveImage(BufferedImage toSave, String outputFileName)
	{		
		try 
		{
		    File outputfile = new File(outputFileName);
		    ImageIO.write(toSave, "png", outputfile);
		} 
		catch (IOException e)
		{		
		}
	}
	
	
	// binaryzuje dany obraz. Funkcja do naprawy, robia sie jakies bledy, ze obrazek na obrazku nie mozna zapisywac innych kolorow
	// niz czarny i bialy
	public static BufferedImage binarization(BufferedImage img, int imgWidth, int imgHeight)
	{
		int i,j;
		int threshold=50;
		int pixel;
		for(i=0; i<imgWidth; i++)
		{
			for(j=0; j<imgHeight; j++)
			{
				pixel = img.getRGB(i, j);
				int  red   = (pixel & 0x00ff0000) >> 16;
				int  green = (pixel & 0x0000ff00) >> 8;
				int  blue  =  pixel & 0x000000ff;
				if(red<threshold && green<threshold && blue<threshold)
				{
					img.setRGB(i,j, 0x00000000);
				}				
				else 
				img.setRGB(i,j, 0x00ffffff);
			}
		}
		return img;
	}
	//sprawdza czy dany obraz jest zbinaryzowany
	public static boolean ifBinarized(BufferedImage img, int imgWidth, int imgHeight)
	{
		int i,j;
		boolean ifBinarized = true;
		outer:
		for(i=0; i<imgWidth; i++)
		{
			for(j=0; j<imgHeight; j++)
			{
				if(!ifWhite(img, i, j) && !ifBlack(img, i, j))
				{
					ifBinarized = false;					
					break outer;
				}
			}
		}
		return ifBinarized;
	}
	//binaryzuje dany obraz i zapisuje go na dysku;
	public static void createBinarizedMap(String inputFileName, int imgWidth, int imgHeight, String outputFileName)
	{
		BufferedImage img = loadImage(inputFileName);
		binarization(img, imgWidth, imgHeight);
		saveImage(img, outputFileName);
	}
	
	public static void changeOneColorToAnother(BufferedImage img, Color initColor, Color finalColor)
	{
		for(int i=0; i<img.getWidth(); i++)
		{
			for(int j=0; j<img.getHeight(); j++)
			{				
				if( (ifTheSameCol(img, i, j, initColor) ) )
				{
					img.setRGB(i, j, finalColor.getRGB());					
				}
			}
		}
	}
	
	public static void colorImgWithColor(BufferedImage img, Color color)
	{
		int rgb = color.getRGB();
		for(int i=0; i<img.getWidth(); i++)
		{
			for(int j=0; j<img.getHeight(); j++)
			{				
				img.setRGB(i, j, rgb);
			}
		}
	}
	public static void colorImgWithDarkBlue(BufferedImage img)
	{
		colorImgWithColor(img, Colors.darkblue);
	}
	public static void colorImgWithBlack(BufferedImage img)
	{
		colorImgWithColor(img, Colors.black);
	}
	public static void colorImgWithGreen(BufferedImage img)
	{
		colorImgWithColor(img, Colors.green);
	}
	public static void colorImgWithWhite(BufferedImage img)
	{
		colorImgWithColor(img, Colors.white);
	}
	public static void colorImgWithRed(BufferedImage img)
	{
		colorImgWithColor(img, Colors.red);
	}
}
